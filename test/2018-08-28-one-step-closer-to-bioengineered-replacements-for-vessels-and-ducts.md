---
layout: post
title: "One step closer to bioengineered replacements for vessels and ducts"
date: 2018-08-28
categories: Science
author: unknown author
tags: [3D bioprinting, Tissue (biology), Biology, Medicine, Medical specialties, Clinical medicine]
---





>(Brigham and Women's Hospital) Researchers bioprint complex tubular tissues to replace dysfunctional vessels and ducts in the body....



<hr>[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/bawh-osc082418.php)


