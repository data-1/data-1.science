---
layout: post
title: "First open-access data from large collider confirm subatomic particle patterns"
date: 2017-10-04
categories:
author: "Jennifer Chu, Massachusetts Institute Of Technology"
tags: [Compact Muon Solenoid,Particle physics,Collider,Large Hadron Collider,Strong interaction,Gluon,Jet (particle physics),Physics,Quark,Science]
---


In our field of particle physics, there isn't the tradition of making data public, says Thaler. In particular, physicists can use an equation, known as an evolution equation or splitting function, to predict the pattern of particles that spray out from an initial collision, and therefore the overall structure of the jet produced. Collider legacy  In 2014, the CMS released a preprocessed form of the detector's 2010 raw data that contained an exhaustive listing of particle flow candidates, or the types of subatomic particles that are most likely to have been released, given the energies measured in the detector after a collision. The team was able to reveal the splitting function, or evolution equation, by combining information from all 750,000 jets they studied, showing that the equation—a fundamental feature of the strong force—can indeed predict the overall structure of a jet and the energies of particles produced from the collision of two protons. Thaler hopes his and others' analysis of the CMS open data will spur other large particle physics experiments to release similar information, in part to preserve their legacies.

<hr>

[Visit Link](https://phys.org/news/2017-09-open-access-large-collider-subatomic-particle.html){:target="_blank" rel="noopener"}


