---
layout: post
title: "Hubble's deep field images of the early universe are postcards from billions of years ago"
date: 2016-04-23
categories:
author: Mark Sullivan, The Conversation
tags: [Hubble Ultra-Deep Field,Hubble Space Telescope,Milky Way,Physical sciences,Outer space,Space science,Science,Astronomical objects,Astronomy]
---


The Hubble Space Telescope began observing deep fields in 1995. Careful planning was required for the deep field images. Credit: Digitised Sky Survey (DSS), STScI/AURA, Palomar/Caltech, and UKSTU/AAO  Finally, advances in data processing techniques, some new infrared data and more images of Ultra Deep Field area were in 2012 combined to create the Hubble eXtreme Deep Field (HXDF), the main image above, and the same image with ultraviolet included, below – humanity's most sensitive images of the cosmos ever taken. All the other objects in the image are galaxies. This is about 45 billion light years from the Earth: light from more distant objects has not yet had time to reach us.

<hr>

[Visit Link](http://phys.org/news349075733.html){:target="_blank" rel="noopener"}


