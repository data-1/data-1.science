---
layout: post
title: "Toward mass-producible quantum computers: Process for positioning quantum bits in diamond optical circuits could work at large scales"
date: 2017-09-20
categories:
author: "Massachusetts Institute of Technology"
tags: [Quantum computing,Diamond,Quantum superposition,Light,Electron,Nitrogen-vacancy center,Qubit,Silicon,Atom,Computing,Technology,Theoretical physics,Science,Physical chemistry,Physical sciences,Chemistry,Quantum mechanics,Applied and interdisciplinary physics,Physics]
---


But practical, diamond-based quantum computing devices will require the ability to position those defects at precise locations in complex diamond structures, where the defects can function as qubits, the basic units of information in quantum computing. The dream scenario in quantum information processing is to make an optical circuit to shuttle photonic qubits and then position a quantum memory wherever you need it, says Dirk Englund, an associate professor of electrical engineering and computer science who led the MIT team. In fact, the light particles emitted by diamond defects can preserve the superposition of the qubits, so they could move quantum information between quantum computing devices. advertisement  In their new paper, the MIT, Harvard, and Sandia researchers instead use silicon-vacancy centers, which emit light in a very narrow band of frequencies. In the process described in the new paper, the MIT and Harvard researchers first planed a synthetic diamond down until it was only 200 nanometers thick.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/05/170526084521.htm){:target="_blank" rel="noopener"}


