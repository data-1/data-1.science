---
layout: post
title: "English Channel to host wind farm"
date: 2015-05-19
categories:
author: ""    
tags: [Energy development,Renewable energy,Wind power,Wind turbine,Energy,Nature,Physical quantities,Sustainable technologies,Sustainable energy,Sustainable development,Energy and the environment,Natural environment,Renewable resources]
---


English Channel to host wind farm    by Daniel J. Graeber    Dusseldorf, Germany (UPI) May 18, 2015    German energy company E.ON said Monday it's continuing its legacy as the largest European investor in wind by building a wind farm in the English Channel. E.ON said it made a final investment decision on the Rampion wind farm in the English Channel. Backed by a $374 million commitment by the U.K. Green Investment Bank, the project is expected to generate enough energy to meet the annual demand of 300,000 average households. The German company is the third-largest offshore wind energy operator, with 1.2 gigawatts of capacity on the regional grid. Because wind may need a fossil fuel buffer, a 2014 study from the Adam Smith Institute finds the only benefit the U.K. wind fleet brings to the United Kingdom is that of reduced dependency on fossil-fuel imports.

<hr>

[Visit Link](http://www.winddaily.com/reports/English_Channel_to_host_wind_farm_999.html){:target="_blank" rel="noopener"}


