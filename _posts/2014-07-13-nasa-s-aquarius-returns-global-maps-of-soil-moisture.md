---
layout: post
title: "NASA's Aquarius returns global maps of soil moisture"
date: 2014-07-13
categories:
author: "$author" 
tags: [Aquarius (SAC-D instrument),Soil Moisture Active Passive,Soil Moisture and Ocean Salinity,Earth sciences,Physical geography,Nature,Applied and interdisciplinary physics]
---


Satellite readings of soil moisture will help scientists better understand the climate system and have potential for a wide range of applications, from advancing climate models, weather forecasts, drought monitoring and flood prediction to informing water management decisions and aiding in predictions of agricultural productivity. Tom Jackson, principal investigator for the Aquarius soil moisture measurements and a hydrologist with USDA, said that his agency uses soil wetness information to improve crop forecasts. One of the things that having Aquarius and SMOS in orbit before SMAP has allowed us to do is to do inter-comparison studies between the sensors, O'Neill said. This approach will provide a footprint of 5.6 miles (9 kilometers), and it will produce worldwide soil moisture maps every three days. Soil moisture data from NASA's Aquarius microwave radiometer are now available at the National Snow and Ice Data Center.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/nsfc-nar070714.php){:target="_blank" rel="noopener"}


