---
layout: post
title: "Exogenous microRNAs in maternal food pass through placenta, regulate fetal gene expression"
date: 2016-05-02
categories:
author: Nanjing University School Of Life Sciences
tags: [MicroRNA,Placenta,RNA silencing,RNA,Fetus,Gene expression,Small interfering RNA,Non-coding RNA,Gene,Alpha-fetoprotein,Molecular biology,Genetics,Biotechnology,Life sciences,Biochemistry,Medical specialties,Biology]
---


In a new study published in the Protein & Cell, Chen-Yu Zhang's group at Nanjing University reports that small non-coding RNAs in maternal food can transfer through placenta to regulate fetal gene expression. In support of this new concept, they have also found a plant microRNA, MIR2911, which is enriched in honeysuckle, directly targets influenza A viruses (IAV) including H1N1, H5N1 and H7N9. Here, they report another surprising finding that exogenous plant miRNAs and artificial synthetic small influence RNAs (siRNAs) can transfer through the placenta and directly regulate fetus gene expression. When pregnant mice were administrated honeysuckle soup (the exogenous plant microRNAs are physiological concentration in food), the plant MIR2911 was detected in fetus liver with significant level. This work is important for the following reasons:  1) This is the first time to demonstrate that small RNAs can pass through mammalian placenta and directly regulate foetus gene, consequently may also influence foetus development.

<hr>

[Visit Link](http://phys.org/news350647882.html){:target="_blank" rel="noopener"}


