---
layout: post
title: "Watch 2,600 years of culture spread across the world in 5 minutes"
date: 2015-10-09
categories:
author: Carl Franzen, Aug
tags: [The Verge,Digital media,Computing,Technology,Websites,Web 20,Mass media,Communication,Internet,Cyberspace,World Wide Web]
---


Cookie banner  We use cookies and other tracking technologies to improve your browsing experience on our site, show personalized content and targeted ads, analyze site traffic, and understand where our audiences come from. To learn more or opt-out, read our Cookie Policy. Please also read our Privacy Notice and Terms of Use, which became effective December 20, 2019. By choosing I Accept, you consent to our use of cookies and other tracking technologies.

<hr>

[Visit Link](http://www.theverge.com/2014/8/1/5958903/watch-2600-years-of-culture-spread){:target="_blank" rel="noopener"}


