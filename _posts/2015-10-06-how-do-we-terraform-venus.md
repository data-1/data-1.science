---
layout: post
title: "How do we terraform Venus?"
date: 2015-10-06
categories:
author: Matt Williams
tags: [Venus,Terraforming,Terraforming of Venus,Atmosphere,Earth,Planet,Sun,Runaway greenhouse effect,Greenhouse effect,Mars,Solar System,Circumstellar habitable zone,Atmosphere of Earth,Sky,Physical sciences,Nature,Planetary science,Outer space,Astronomy,Space science,Planets]
---


As such, if humans want to live there, some serious ecological engineering – aka. terraforming – is needed first. Proposed Methods:  The first proposed method of terraforming Venus was made in 1961 by Carl Sagan. In addition, this shade would also serve to block the solar wind, thus reducing the amount of radiation Venus' surface is exposed to (another key issue when it comes to habitability). Here's The Definitive Guide To Terraforming, Could We Terraform the Moon?, Should We Terraform Mars?, How Do We Terraform Mars?

<hr>

[Visit Link](http://phys.org/news325497132.html){:target="_blank" rel="noopener"}


