---
layout: post
title: "What will the future look like? Elon Musk speaks at TED2017"
date: 2017-09-19
categories:
author: "Brian Greene"
tags: [Tesla Inc,Hyperloop,Tesla Semi,Spacecraft,Elon Musk,Space vehicles,Outer space,Flight,Spaceflight,Vehicles,Technology,Transport]
---


In conversation with TED’s Head Curator Chris Anderson, serial entrepreneur and future-builder Elon Musk discusses his new project digging tunnels under LA, Hyperloop, Tesla, SpaceX and his dreams for what the world could look like. “With the Tesla Semi, we want to show that an electric truck actually can out-torque any diesel semi. Musk thinks we’ll need about 100 such factories to power the world in a future where we don’t feel guilty about using and producing energy, and Tesla plans to announce locations for another four Gigafactories late this year. Showing plans for a massive rocket that’s the size of a 40-story building, Musk talks about what it’ll take to get to Mars. It only improves if a lot of people work very hard to make it better.”  What’s your motivation?

<hr>

[Visit Link](http://blog.ted.com/what-will-the-future-look-like-elon-musk-speaks-at-ted2017/){:target="_blank" rel="noopener"}


