---
layout: post
title: "Scientists grow multiple brain structures and make connections between them: Model of neuronal connectivity using stem cells"
date: 2016-05-08
categories:
author: IOS Press BV
tags: [Brain,Neuron,News aggregator,Medicine,Neuroscience]
---


Human stem cells can be differentiated to produce other cell types, such as organ cells, skin cells, or brain cells. While organ cells, for example, can function in isolation, brain cells require synapses, or connectors, between cells and between regions of the brain. Freed, PhD, of the Intramural Research Program, National Institute on Drug Abuse, National Institutes of Health, Baltimore, MD. However, studying mDA neurons and neocortical neurons in isolation does not reveal much data about how these cells actually interact in these conditions. This new capability to grow and interconnect two types of neurons in vitro now provides researchers with an excellent model for further study.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150615125252.htm){:target="_blank" rel="noopener"}


