---
layout: post
title: "Play an Instrument? Musical Training Improves Brain Function"
date: 2014-06-22
categories:
author: Science World Report
tags: [Executive functions,Functional magnetic resonance imaging,Brain,Medical imaging,Music,Intelligence quotient,Interdisciplinary subfields,Branches of science,Psychological concepts,Neuropsychology,Mental processes,Behavioural sciences,Cognitive psychology,Psychology,Cognition,Cognitive science,Neuroscience]
---


Researchers at Boston Children's Hospital discovered, with the help of functional MRI brain imaging, that playing an instrument early in life supports brain growth and function. All participants who were musically trained had actively played an instrument for at least two years through private music lessons. Findings revealed that for adult musicians and musically trained children, they showed enhanced performance on several of the executive functioning tests. With future studies, researchers hope to determine if certain children may be predisposed to study music and already equipped with certain executive functioning abilities. Do you play an instrument?

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15504/20140618/play-an-instrument-musical-training-improves-brain-function.htm){:target="_blank" rel="noopener"}


