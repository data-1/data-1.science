---
layout: post
title: "How spacetime is built by quantum entanglement"
date: 2016-05-03
categories:
author: University of Tokyo
tags: [Quantum mechanics,General relativity,Quantum gravity,Theory of everything,Physics,Quantum entanglement,Theory of relativity,Universe,Mechanics,Dimension,Gravity,Science,Theoretical physics,Scientific theories,Physical sciences,Applied and interdisciplinary physics,Scientific method,Epistemology of science]
---


A collaboration of physicists and a mathematician has made a significant step toward unifying general relativity and quantum mechanics by explaining how spacetime emerges from quantum entanglement in a more fundamental theory. The holographic principle states that gravity in a three-dimensional volume can be described by quantum mechanics on a two-dimensional surface surrounding the volume. The importance of quantum entanglement has been suggested before, but its precise role in emergence of spacetime was not clear until the new paper by Ooguri and collaborators. ###  Paper  Authors: Jennifer Lin (1), Matilde Marcolli (2), Hirosi Ooguri (3,4), Bogdan Stoica (3)  Author affiliations: (1) Enrico Fermi Institute and Department of Physics, University of Chicago  (2) Department of Mathematics, California Institute of Technology  (3) Walter Burke Institute for Theoretical Physics, California Institute of Technology  (4) Kavli Institute for the Physics and Mathematics of the Universe (WPI), University of Tokyo  Title: Locality of Gravitational Systems from Entanglement of Conformal Field Theories  Journal: Physical Review Letters  Links  The University of Tokyo http://www.u-tokyo.ac.jp/en/  Kavli Institute for the Physics and Mathematics of the Universe http://www.ipmu.jp/  Research contact  Hirosi Ooguri  Principal Investigator  Kavli Institute for the Physics and Mathematics of the Universe, The University of Tokyo  Email: h.ooguri@gmail.com  Press officer contact  Motoko Kakubayashi  Project Specialist  Kavli Institute for the Physics and Mathematics of the Universe, The University of Tokyo  Tel: +81-4-7136-5980 (office)  Email: press@ipmu.jp  About the Kavli IPMU  Kavli IPMU (Kavli Institute for the Physics and Mathematics of the Universe) is an international research institute with English as its official language. The Institute for the Physics and Mathematics of the Universe (IPMU) was established in October 2007 under the World Premier International Research Center Initiative (WPI) of the Ministry of Education, Sports, Science and Technology in Japan with the University of Tokyo as the host institution.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/uot-hsi052715.php){:target="_blank" rel="noopener"}


