---
layout: post
title: "Astrophysicists model the formation of the oldest-known star in our galaxy"
date: 2015-09-13
categories:
author: Institute For Astrophysics Göttingen
tags: [Star,Astrophysics,Metallicity,Abundance of the chemical elements,Sun,Chemical element,Supernova,Stellar astronomy,Natural sciences,Astronomical objects,Physical sciences,Astronomy,Space science,Nature,Physics]
---


When these stars died during the first supernova explosions, the heavy elements have been ejected, and stars of the second generation could form. The results therefore strongly suggest that the transition to the second generation already occurred after the first supernova explosion. The heavy elements provide additional mechanisms for the gas to cool, and it is very important to follow their chemical evolution, explains Dr. Tommaso Grassi from the Center for Star and Planet Formation at the University of Copenhagen. While this star has a tiny amount of heavy elements, it has a relatively higher carbon abundance. In the future, the scientists plan to explore a wide range of possible conditions to understand the formation of the most metal-poor stars observed in our Milky Way galaxy.

<hr>

[Visit Link](http://phys.org/news325264792.html){:target="_blank" rel="noopener"}


