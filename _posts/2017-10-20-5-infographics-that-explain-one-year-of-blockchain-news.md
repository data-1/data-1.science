---
layout: post
title: "5 Infographics That Explain One Year Of Blockchain News"
date: 2017-10-20
categories:
author:  
tags: [Blockchain,General Electric,Technology,Analytics,Innovation,News,Computer network,Electrical grid,Culture,Energy development,Infrastructure,3D printing,Economy]
---


Last spring, the biggest headline in the blockchain world was that an Australian entrepreneur, Craig Wright, finally    A Quid timeline maps 3,409 stories about blockchain, colored by cluster, from global news sources between 20 April 2016-20 April 2017. In the graph above, Quid sampled the data to show which stories generated the most volume in relation to each other. Last fall Ripple, which develops a payment protocol and exchange network, announced it had raised $50 million and partnered with major banks. In the timeline below, we identified several other events that generated significant news volume. Sorting all that data into a network, major themes emerge.

<hr>

[Visit Link](http://www.gereports.com/5-infographics-explain-one-year-blockchain-news/){:target="_blank" rel="noopener"}


