---
layout: post
title: "How Agile helps non-technical teams get things done"
date: 2018-08-15
categories:
author: "Michelle Thong"
tags: [Agile software development,Scrum (software development),Business,Technology]
---


What is Agile? Team Kanban board for tracking contracts (Photo credit: Alvina Nishimoto)  Manage interruptions. Team members benefit from having a visual reminder of their priorities and getting to share their progress with the team. After a while, the team decided that daily stand-up meetings would be useful so they could share progress at a finer-grained level. Next, they decided to start regular planning meetings as a team to establish priorities.

<hr>

[Visit Link](https://opensource.com/article/18/8/agile-helps-non-technical-teams){:target="_blank" rel="noopener"}


