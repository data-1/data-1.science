---
layout: post
title: "Watch Keynote Videos from OS Summit and ELC Europe 2017 Including a Conversation with Linus Torvalds"
date: 2018-02-18
categories:
author: "The Linux Foundation"
tags: [Linux kernel,Linux Foundation,Linux,Software development,Linus Torvalds,System software,Free software,Linux kernel programmers,Finnish computer programmers,Computer architecture,Technology,Torvalds family,Linux people,Finnish computer scientists,Unix variants,Computers,Computer science,Linux websites,Linux organizations,Software engineering,Open-source movement,Unix,Operating system families,Software,Computing,Intellectual works,Free system software,Computer engineering,Kernel programmers]
---


[vc_row type=”in_container” full_screen_row_position=”middle” scene_position=”center” text_color=”dark” text_align=”left” overlay_strength=”0.3″][vc_column column_padding=”no-extra-padding” column_padding_position=”all” background_color_opacity=”1″ background_hover_color_opacity=”1″ column_shadow=”none” width=”1/1″ tablet_text_alignment=”default” phone_text_alignment=”default” column_border_width=”none” column_border_style=”solid”][vc_column_text]  If you weren’t able to attend Open Source Summit and Embedded Linux Conference (ELC) Europe last week, don’t worry! We’ve recorded keynote presentations from both events and all the technical sessions from ELC Europe to share with you here. Check out the on-stage conversation with Linus Torvalds and VMware’s Dirk Hohndel, opening remarks from The Linux Foundation’s Executive Director Jim Zemlin, and a special presentation from 11-year-old CyberShaolin founder Reuben Paul. You can watch these and other ELC and OS Summit keynotes below for insight into open source collaboration, community and technical expertise on containers, cloud computing, embedded Linux, Linux kernel, networking, and much more. And, you can watch all 55+ technical sessions from Embedded Linux Conference here.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/watch-keynotes-and-session-videos-from-os-summit-and-elc-europe-2017/){:target="_blank" rel="noopener"}


