---
layout: post
title: "Saudi Arabia announces plan for $500B megacity powered by renewables"
date: 2017-10-26
categories:
author: Lacy Cooke
tags: []
---


Saudi Arabia just announced plans for a futuristic megacity that will span 10,000 square miles and three countries near the Red Sea. NEOM will be an independent economic zone owned by the Public Investment Fund of Saudi Arabia – and the nation is calling out to innovators, scientists, and dreamers to form an aspirational society to pioneer next-gen technologies. The future of food, energy, and biotechnology, to name a few, will be explored in this global community to be backed by over $500 billion. They want to transform NEOM into a global hub to pursue innovation in nine investment sectors: advanced manufacturing, food, biotech, mobility, media, technological and digital sciences, entertainment, energy and water, and livability. Related: The world’s first “Tesla Town” with solar roofs and Powerwalls is coming to Australia  Join Our Newsletter Receive the latest in global news and designs building a better future.

<hr>

[Visit Link](https://inhabitat.com/saudi-arabia-announces-plan-for-500b-megacity-powered-by-renewables){:target="_blank" rel="noopener"}


