---
layout: post
title: "Study provides strongest evidence oxygen levels were key to early animal evolution"
date: 2017-03-19
categories:
author: "University College London"
tags: [Oxygen,Cloudinidae,Animal,Fossil,Earth,Earth sciences,Nature,Science]
---


Rosalie Tostevin in Namibia. It shows that poorly oxygenated waters did not support the complex life that evolved immediately prior to the Cambrian period, suggesting the presence of oxygen was a key factor in the appearance of these animals. Lead author Dr Rosalie Tostevin completed the study analyses as part of her PhD with UCL Earth Sciences, and is now in the Department of Earth Sciences at Oxford University. By teasing apart waters with high and low levels of oxygen, and demonstrating that early skeletal animals were restricted to well-oxygenated waters, we have provided strong evidence that the availability of oxygen was a key requirement for the development of these animals. The team, which included other geochemists, palaeoecologists and geologists from UCL and the universities of Edinburgh, Leeds and Cambridge, as well as the Geological Survey of Namibia, analysed the chemical elemental composition of rock samples from the ancient seafloor in the Nama Group – a group of extremely well-preserved rocks in Namibia that are abundant with fossils of early Cloudina, Namacalathus and Namapoikia animals.

<hr>

[Visit Link](http://phys.org/news/2016-09-ocean-oxygen-key-animal-evolution.html){:target="_blank" rel="noopener"}


