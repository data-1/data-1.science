---
layout: post
title: "Human genes adapted to life in the Arctic"
date: 2015-10-01
categories:
author: University of Copenhagen - Faculty of Science 
tags: [Genetics,Biology,DNA,Novo Nordisk,Mutation,Inuit,Gene,Fat,University of Copenhagen,Cell (biology),Life sciences,Biotechnology,Biochemistry,Molecular biology,Nature]
---


By using genetic information from 4500 Greenlanders, researchers investigated which genes have changed the most over the roughly 20,000 years since the Greenlanders' most ancient ancestors separated from their nearest East Asian relatives, the Han Chinese. By studying the concentration of fatty acids in the cell membranes of Greenlanders, we found out that the genetic changes allows the Greenlandic Inuit to compensate for their traditionally high intake of certain omega-3 and omega-6 fatty acids from fish, among other things, explains Associate Professor Anders Albrechtsen of the Section for Computational and RNA Biology at the University of Copenhagen's Department of Biology. The research also showed that one of the genetic changes, a specific mutation, has had a large effect on height as well. This clearly shows the advantage of studying small and historically isolated populations, says Professor Torben Hansen of the Novo Nordisk Foundation Center for Basic Metabolic Research at the University of Copenhagen. ###  Collaboration  The research study is the result of collaboration between research groups from the Department of Biology at the University of Copenhagen, the Novo Nordisk Foundation Center for Basic Metabolic Research at the University of Copenhagen, the University of California, Berkeley, University College London, the Steno Diabetes Center and the University of Southern Denmark.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/fos--hga091715.php){:target="_blank" rel="noopener"}


