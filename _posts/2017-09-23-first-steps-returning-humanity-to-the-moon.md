---
layout: post
title: "First steps: returning humanity to the Moon"
date: 2017-09-23
categories:
author: ""
tags: [Moon,European Space Agency,Exploration of the Moon,Apollo program,Lander (spacecraft),Spaceflight,Planetary science,Astronomy,Space exploration,Discovery and exploration of the Solar System,Spacecraft,Aerospace,Solar System,Sky,Space research,Space programs,Human spaceflight,Flight,Space science,Astronautics,Outer space]
---


Science & Exploration First steps: returning humanity to the Moon 20/09/2017 20892 views 297 likes  In the first act of lunar exploration, Neil Armstrong and Buzz Aldrin were major characters. But for our return to the Moon to be truly sustainable, we must make use of lunar resources. So in addition to transportation and communication, we are looking to invest in the development and pay for the use of technology that can turn indigenous lunar material into oxygen and water, critical resources for sustaining future human operations in deep space. But while we learned a lot about the Moon from Apollo, we literally just scratched the surface of Earth’s eighth continent. If you are a commercial enterprise ready to take on the challenge and build on the legacy of Neil and Buzz, then we want to hear from you.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/First_steps_returning_humanity_to_the_Moon){:target="_blank" rel="noopener"}


