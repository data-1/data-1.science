---
layout: post
title: "Quantum phase transition observed for the first time"
date: 2017-09-17
categories:
author: "Institute of Science and Technology Austria"
tags: [Phase transition,Photon,Quantum mechanics,Temperature,Superconductivity,Phase (matter),Quantum simulator,Resonator,Physics,Science,Theoretical physics,Physical chemistry,Physical sciences,Chemistry,Applied mathematics,Applied and interdisciplinary physics]
---


A group of scientists led by Johannes Fink from the Institute of Science and Technology Austria (IST Austria) reported the first experimental observation of a first-order phase transition in a dissipative quantum system. But if the photon flux increases to a critical level, a quantum phase transition has been predicted to occur: The photon blockade breaks down, and the state of the system changes from opaque to transparent. To produce a flux of photons, the researchers then sent a continuous microwave tone to the input of the resonator on the chip. Our experiment took exactly 1.6 milliseconds to complete for any given input power. This gives an idea why these systems could be useful for quantum simulations, Fink explains.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/iosa-qpt020117.php){:target="_blank" rel="noopener"}


