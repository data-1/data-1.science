---
layout: post
title: "Photosynthesis in plants key to speedy evolution"
date: 2018-04-26
categories:
author: "University Of Oxford"
tags: [Plant,Photosynthesis,Evolution,Gene,Privacy,Biology,Technology]
---


Credit: Oxford Science Blog  In a study of 11 different plant species, published in Molecular Biology and Evolution, researchers at the University of Oxford have shown that the speed at which plants evolve is linked to how good they are at photosynthesis. Plants need nitrogen to do photosynthesis. What the study found is that plants that invest lots of nitrogen in photosynthesis use cheaper letters to build their genes. For example, when atmospheric CO2 concentration goes up, plants don't need to invest as much nitrogen in trying to capture it, and so more of the nitrogen budget in the cell can be spent on making genes. That means when atmospheric CO2 concentration goes up plant genes evolve faster.

<hr>

[Visit Link](https://phys.org/news/2018-04-photosynthesis-key-speedy-evolution.html){:target="_blank" rel="noopener"}


