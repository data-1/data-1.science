---
layout: post
title: "Full house for EDRS"
date: 2018-08-16
categories:
author: ""
tags: [European Data Relay System,Copernicus Programme,Satellite,European space programmes,Space policy of the European Union,Science,Space agencies,Outer space,Spaceflight,Satellites,Space science,Astronautics,Technology,Space vehicles,Flight,Spacecraft,Bodies of the Solar System]
---


Applications Full house for EDRS 13/03/2018 6093 views 100 likes  The EDRS–SpaceDataHighway has now begun regularly relaying Earth images from Sentinel-2A, which marks the last of four Copernicus satellites in orbit being brought under the EDRS service. After several months of rigorous testing, the system has added the last ‘colour vision’ Sentinel to the list of Sentinels it serves, bringing the satellite’s vibrant images to Earth faster than ever and completing the full set of four. The European Data Relay System (EDRS) will be a unique system of satellites permanently fixed over a network of ground stations, with the first – EDRS-A – already in space. These nodes lock on to low-orbiting satellites with lasers and collect their data as they travel thousands of kilometres below, scanning Earth. That way, it can send down more data, more quickly.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Telecommunications_Integrated_Applications/EDRS/Full_house_for_EDRS){:target="_blank" rel="noopener"}


