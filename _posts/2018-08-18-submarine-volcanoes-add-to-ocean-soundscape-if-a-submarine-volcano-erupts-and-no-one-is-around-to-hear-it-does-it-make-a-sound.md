---
layout: post
title: "Submarine volcanoes add to ocean soundscape: If a submarine volcano erupts and no one is around to hear it, does it make a sound?"
date: 2018-08-18
categories:
author: "Acoustical Society of America"
tags: [Volcano,Submarine volcano,Types of volcanic eruptions,Earth sciences,Volcanism,Geology,Volcanology]
---


Most volcanoes erupt beneath the ocean, but scientists know little about them compared to what they know about volcanoes that eject their lava on dry land. Gabrielle Tepp of the Alaska Volcano Observatory and the U.S. Geological Survey thinks that with improved monitoring, scientists can learn more about these submarine eruptions, which threaten travel and alter the ocean soundscape. In 2016 and 2017, Bogoslof had more sustained eruptions, lasting minutes to hours, which occurred every few days. Evidence of these eruptions showed up on distant seismometers, which measure waves passing through the ground to record earthquakes, and hydrophone arrays that pick up underwater sound to detect covert nuclear detonations. Eruptions that create a loud enough sound, in the right location, can travel pretty far, even from one ocean to another, Tepp said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/12/171204091205.htm){:target="_blank" rel="noopener"}


