---
layout: post
title: "Discovery of new code makes reprogramming of cancer cells possible"
date: 2016-05-31
categories:
author: "Mayo Clinic"
tags: [Cadherin-1,MicroRNA,PLEKHA7,Cell membrane,Carcinogenesis,Neoplasms,Cell biology,Biochemistry,Molecular biology,Cancer,Biology]
---


Now, researchers on Mayo Clinic's Florida campus have discovered a way to potentially reprogram cancer cells back to normalcy. The investigators showed, in laboratory experiments, that restoring the normal miRNA levels in cancer cells can reverse that aberrant cell growth. However, we and other researchers had found that this hypothesis didn't seem to be true, since both E-cadherin and p120 are still present in tumor cells and required for their progression, Dr. Anastasiadis says. The investigators discovered that PLEKHA7 maintains the normal state of the cells, via a set of miRNAs, by tethering the microprocessor to E-cadherin and p120. However, when this apical adhesion complex was disrupted after loss of PLEKHA7, this set of miRNAs was misregulated, and the E-cadherin and p120 switched sides to become oncogenic, Dr. Anastasiadis says.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150824064916.htm){:target="_blank" rel="noopener"}


