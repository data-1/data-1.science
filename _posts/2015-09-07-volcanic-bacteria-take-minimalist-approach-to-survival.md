---
layout: post
title: "Volcanic bacteria take minimalist approach to survival"
date: 2015-09-07
categories:
author: University of Otago 
tags: [Soil,Bacteria,Ecosystem,Metabolism,American Association for the Advancement of Science,Nutrient,Microorganism,Nature,Natural environment,Physical sciences,Chemistry,Earth sciences]
---


It is well-established that the majority of bacteria in soil ecosystems live in dormant states due to nutrient deprivation, but the metabolic strategies that enable their survival have not yet been shown. Following in-depth studies on the genetic and biochemical capabilities of the organism, the authors uncovered that the bacterium actually took a highly minimalistic approach to survival. When its preferred carbohydrate nutrient sources are exhausted, it is able to scavenge trace amounts of the fuel hydrogen from the air. It is 'living on thin air', so to speak. This publication is the latest in a trilogy of PNAS papers authored by Professor Cook and Dr Greening.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/uoo-vbt080315.php){:target="_blank" rel="noopener"}


