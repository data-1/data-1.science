---
layout: post
title: "Red Hat Announces 2018 Women in Open Source Award Winners"
date: 2018-05-26
categories:
author: ""
tags: [Open source,Red Hat,Automated insulin delivery system,Risk,Forward-looking statement,Open-source-software movement,Cloud computing,Intellectual property,Technology]
---


SAN FRANCISCO - RED HAT SUMMIT 2018 - May 8, 2018 —  Red Hat, Inc. (NYSE: RHT), the world's leading provider of open source solutions, today announced Dana Lewis, founder of the Open Artificial Pancreas System (OpenAPS) movement, and Zui Dighe, a Duke University student, as the 2018 Women in Open Source Award winners. DeLisa Alexander executive vice president and chief people officer, Red Hat  In its fourth year, the Women in Open Source Awards were created and sponsored by Red Hat to honor women who make important contributions to open source projects and communities, or those making innovative use of open source methodology. Lewis, who was recognized in the community category, is the founder of the OpenAPS movement and creator of the DIY Artificial Pancreas System. We're passionate about promoting the sizable impact that women are making in open source and I am inspired not only by their work, but by their efforts as mentors and advocates for others.”  Dana Lewis, founder of the OpenAPS movement  “I’m honored to be part of the group of amazing women who were nominated for the Red Hat Women in Open Source Award, and appreciate everyone who learned about all the finalists’ commitment to open source. I am inspired by the stories of the finalists for the Red Hat Women in Open Source Award and learning about the diverse projects that they have been working on.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/red-hat-announces-2018-women-open-source-award-winners){:target="_blank" rel="noopener"}


