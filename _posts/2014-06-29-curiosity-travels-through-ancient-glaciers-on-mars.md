---
layout: post
title: "Curiosity travels through ancient glaciers on Mars"
date: 2014-06-29
categories:
author: ""        
tags: []
---


3,500 million years ago the Martian crater Gale, through which the NASA rover Curiosity is currently traversing, was covered with glaciers, mainly over its central mound. NASA’s Mars Curiosity Rover has completed a Martian year –687 Earth days– this week. However, at that time there were also rivers and lakes with very cold liquid water in the lower-lying areas within the crater, adds the researcher, who highlights the fact that ancient Mars was capable of maintaining large quantities of liquid water (an essential element for life) at the same time that giant ice sheets covered extensive regions of its surface. To carry out the study, the team has used images captured with the HiRISE and CTX cameras from NASA's Mars Reconnaissance Orbiter, together with the HRSC onboard the Mars Express probe managed by the European Space Agency (ESA). For example, there is a glacier on Iceland –known as Breiðamerkurjökull– which shows evident resemblances to what we see on Gale crater, and we suppose that is very similar to those which covered Gale's central mound in the past, says Fairén.

<hr>

[Visit Link](http://www.pddnet.com/news/2014/06/developer-turns-manure-renewable-energy){:target="_blank" rel="noopener"}


