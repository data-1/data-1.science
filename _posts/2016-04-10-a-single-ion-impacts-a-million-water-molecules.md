---
layout: post
title: "A single ion impacts a million water molecules"
date: 2016-04-10
categories:
author: Ecole Polytechnique Fédérale de Lausanne
tags: [Molecule,Water,Chemical bond,Ion,Atom,American Association for the Advancement of Science,Hydrogen,Applied and interdisciplinary physics,Chemistry,Physical sciences,Physical chemistry,Materials,Nature,Atoms,Materials science,Physics,Atomic molecular and optical physics,Condensed matter physics]
---


Yet the collective behavior of water molecules is unique and continues to amaze us. Deciphering the interactions among water, salt and ions is thus fundamentally important for understanding life. According to their multi-scale analyses, a single ion has an influence on millions of water molecules, i.e. 10,000 times more than previously thought. In an article appearing in Science Advances, they explain how a single ion can twist the bonds of several million water molecules over a distance exceeding 20 nanometers causing the liquid to become stiffer. When an ion, which is an electrically charged atom, comes into contact with water, the network of hydrogen bonds is perturbed.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/epfd-asi040816.php){:target="_blank" rel="noopener"}


