---
layout: post
title: "Crops evolved 10 millennia earlier than thought"
date: 2017-10-23
categories:
author: University Of Warwick
tags: [Domestication,Wheat,Evolution,Agriculture,Einkorn wheat]
---


Professor Robin Allaby, in Warwick's School of Life Sciences, has discovered that human crop gathering was so extensive, as long ago as the last Ice Age, that it started to have an effect on the evolution of rice, wheat and barley - triggering the process which turned these plants from wild to domesticated. In Tell Qaramel, an area of modern day northern Syria, the research demonstrates evidence of einkorn being affected up to thirty thousand years ago, and rice has been shown to be affected more than thirteen thousand years ago in South, East and South-East Asia. When a plant begins to be gathered on a large scale, human activity alters its evolution, changing this gene and causing the plant to retain its seeds instead of spreading them – thus adapting it to the human environment, and eventually agriculture. Professor Allaby and his colleagues made calculations from archaeobotanical remains of crops mentioned above that contained 'non-shattering' genes - the genes which caused them to retain their seeds – and found that human gathering had already started to alter their evolution millennia before previously accepted dates. The study shows that crop plants adapted to domestication exponentially around eight thousand years ago, with the emergence of sickle farming technology, but also that selection changed over time.

<hr>

[Visit Link](https://phys.org/news/2017-10-crops-evolved-millennia-earlier-thought.html){:target="_blank" rel="noopener"}


