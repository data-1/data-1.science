---
layout: post
title: "Researchers create inside-out plants to watch how cellulose forms"
date: 2015-10-09
categories:
author: University of British Columbia 
tags: [Plant,Cellulose,Cell (biology),Botany,American Association for the Advancement of Science]
---


Researchers have been able to watch the interior cells of a plant synthesize cellulose for the first time by tricking the cells into growing on the plant's surface. The bulk of the world's cellulose is produced within the thickened secondary cell walls of tissues hidden inside the plant body, says University of British Columbia Botany PhD candidate Yoichiro Watanabe, lead author of the paper published this week in Science. So we've never been able to image the cells in high resolution as they produce this all-important biological material inside living plants. Cellulose, the structural component of cell walls that enables plants to stay upright, is the most abundant biopolymer on earth. It's a critical resource for pulp and paper, textiles, building materials, and renewable biofuels.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/uobc-rci100815.php){:target="_blank" rel="noopener"}


