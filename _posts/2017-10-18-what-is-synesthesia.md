---
layout: post
title: "What Is Synesthesia?"
date: 2017-10-18
categories:
author: Alina Bradford
tags: [Synesthesia,Cognitive psychology,Cognitive science,Neuroscience,Psychology,Cognition,Psychological concepts,Mental processes,Interdisciplinary subfields,Behavioural sciences,Branches of science,Neuropsychological assessment,Neuropsychology]
---


Most people with the condition experience at least two types of synesthesia. Some other types of synesthesia include the following:  Smelling certain scents when hearing certain sounds. Seeing sign language as colors. However, Peter Grossenbacher, a psychologist at Naropa University in Colorado, thinks that rather than rearranging the architecture of the brain, synesthesia happens when single-sense areas of the brain get feedback from multisensory areas. Have a perception that is the same each time.

<hr>

[Visit Link](https://www.livescience.com/60707-what-is-synesthesia.html){:target="_blank" rel="noopener"}


