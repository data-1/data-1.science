---
layout: post
title: "Phytoplankton like it hot: Warming boosts biodiversity and photosynthesis in phytoplankton: Warmer temperatures increase biodiversity and photosynthesis in phytoplankton, researchers have found"
date: 2015-12-18
categories:
author: University of Exeter
tags: [Phytoplankton,Climate change,Plankton,Biodiversity,Ecosystem,Biogeography,Ecology,Physical geography,Environmental science,Biogeochemistry,Oceanography,Earth phenomena,Environmental social science,Applied and interdisciplinary physics,Environmental conservation,Environmental chemistry,Chemical oceanography,Systems ecology,Hydrography,Nature,Natural environment,Earth sciences,Environmental technology]
---


Warmer temperatures increase biodiversity and photosynthesis in phytoplankton, researchers at the University of Exeter and Queen Mary University of London (QMUL) have found. The groundbreaking study, published in the journal PLOS Biology, was carried out over five years using artificially warmed ponds that simulated the increases in temperature expected by the end of the century. The researchers found that phytoplankton in ponds that had been warmed by four degrees, had 70% more species and higher rates of photosynthesis, and as a result, have the potential to remove more carbon dioxide from the atmosphere. Phytoplankton were counted, measured and identified under a microscope, and the production or consumption of oxygen was measured to determine rates of photosynthesis and respiration. In contrast to previous work conducted in small scale, short-term laboratory experiments, these findings demonstrate that future global warming could actually lead to increases in biodiversity and photosynthesis in some locations.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/12/151217151533.htm){:target="_blank" rel="noopener"}


