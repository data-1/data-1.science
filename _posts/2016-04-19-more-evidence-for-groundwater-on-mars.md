---
layout: post
title: "More evidence for groundwater on Mars"
date: 2016-04-19
categories:
author: Geological Society Of America
tags: [Equatorial layered deposits,Earth sciences,Geology,Planetary science,Mars]
---


(B) High Resolution Stereo Camera (HRSC) mosaic of the mapped area. Topographic contours (in white, 500 m spacing) are indicated. Credit: Pondrelli et al. and GSA Bulletin  Monica Pondrelli and colleagues investigated the Equatorial Layered Deposits (ELDs) of Arabia Terra in Firsoff crater area, Mars, to understand their formation and potential habitability. Pondrelli and colleagues also note that the ELDs inside the craters would likely have originated by fluid upwelling through the fissure ridges and the mounds, and that lead to evaporite precipitation. As a basis for their research, Pondrelli and colleagues produced a detailed geological map of the Firsoff crater area.

<hr>

[Visit Link](http://phys.org/news346682837.html){:target="_blank" rel="noopener"}


