---
layout: post
title: "Pushing the boundaries of magnet design"
date: 2017-10-07
categories:
author: "Springer"
tags: [Magnet,Samarium,Magnetism,Rare-earth element,Magnetization,Atom,American Association for the Advancement of Science,Metal,Chemistry,Nature,Condensed matter physics,Physical chemistry,Materials science,Electromagnetism,Physical sciences,Materials,Physics,Applied and interdisciplinary physics]
---


They have developed methods to counter the spontaneous loss of magnetisation, based on their understanding of the underlying physical phenomenon. Roman Morgunov from the Institute of Problems of Chemical Physics at the Russian Academy of Sciences and colleagues have now developed a simple additive-based method for ensuring the stability of permanent magnets over time, with no loss to their main magnetic characteristics. To design magnets that retain their magnetic stability, the authors altered the chemical composition of a RE-TM-B magnet. The authors believe this result is linked to Samarium's symmetry. As a result, spontaneous magnetisation no longer takes place.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/s-ptb101816.php){:target="_blank" rel="noopener"}


