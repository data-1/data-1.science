---
layout: post
title: "New results from world's most sensitive dark matter detector"
date: 2015-12-22
categories:
author: Lawrence Berkeley National Laboratory
tags: [Large Underground Xenon experiment,XENON,Sanford Underground Research Facility,Dark matter,Weakly interacting massive particles,Radioactive decay,Neutron,Physical sciences,Physics,Nature,Particle physics,Science]
---


LUX improvements, coupled to advanced computer simulations at the U.S. Department of Energy's Lawrence Berkeley National Laboratory's (Berkeley Lab) National Energy Research Scientific Computing Center (NERSC) and Brown University's Center for Computation and Visualization (CCV), have allowed scientists to test additional particle models of dark matter that now can be excluded from the search. The nature of the interaction between neutrons and xenon atoms is thought to be very similar to the interaction between dark matter and xenon. The neutron experiments help to calibrate the detector for interactions with the xenon nucleus. And so the search continues, McKinsey said. LZ would have a 10-ton liquid xenon target, which will fit inside the same 72,000-gallon tank of pure water used by LUX.

<hr>

[Visit Link](http://phys.org/news/2015-12-results-world-sensitive-dark-detector.html){:target="_blank" rel="noopener"}


