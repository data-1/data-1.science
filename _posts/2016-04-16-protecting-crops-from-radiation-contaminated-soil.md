---
layout: post
title: "Protecting crops from radiation-contaminated soil"
date: 2016-04-16
categories:
author: RIKEN
tags: [Plant,Caesium,Soil,Potassium,Caesium-137,Plant nutrition,Physical sciences,Botany,Nature,Materials,Chemistry]
---


First, they used seedlings from the model plant Arabidopsis thaliana and tested 10,000 synthetic compounds to determine if any could reverse the harmful effects of cesium. The effects of each compound were quantified with a scoring scale, and after several screenings, they had found five compounds that made plants highly tolerant to cesium. Importantly, the concentration of CsTolen A needed for this effect did not prevent the plants from absorbing the potassium that they need to grow. Not only will the current findings help plants, but by reducing the amount of radiocesium that enters them, it should also ensure the safety of agricultural products grown in contaminated soil. Selective chemical binding enhances cesium tolerance in plants through inhibition of cesium uptake.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/r-pcf030215.php){:target="_blank" rel="noopener"}


