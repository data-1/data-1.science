---
layout: post
title: "Chimpanzee intelligence determined by genes"
date: 2015-10-03
categories:
author: Georgia State University
tags: [Intelligence,Human intelligence,Animal cognition,Chimpanzee,Intelligence quotient,Cognitive psychology,Neuroscience,Branches of science,Psychological concepts,Psychology,Behavioural sciences,Interdisciplinary subfields,Cognition,Cognitive science]
---


A chimpanzee's intelligence is largely determined by its genes, while environmental factors may be less important than scientists previously thought, according to a Georgia State University research study. The suggestion here is that genes play a really important role in their performance on tasks while non-genetic factors didn't seem to explain a lot. Genes were found to play a role in overall cognitive abilities, as well as the performance on tasks in several categories. Hopkins also studied the structure of chimpanzee intelligence to determine whether there were any similarities to the structure of human intelligence. He would also like to pursue studies to determine which genes are involved in intelligence and various cognitive abilities as well as how genes are linked to variation in the organization of the brain.

<hr>

[Visit Link](http://phys.org/news324211096.html){:target="_blank" rel="noopener"}


