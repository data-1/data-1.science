---
layout: post
title: "China builds world's first space-ground integrated quantum communication network"
date: 2017-10-04
categories:
author: "Chinese Academy of Sciences Headquarters"
tags: [Quantum Experiments at Space Scale,Quantum key distribution,Quantum network,Quantum teleportation,Telecommunications,Quantum information science,Technology,Theoretical physics,Physics,Quantum mechanics,Theoretical computer science,Science,Applied mathematics]
---


A cross-disciplinary multi-institutional team of scientists from the Chinese Academy of Sciences, led by Professor Jian-Wei Pan, has spent more than ten years in developing a sophisticated satellite, named Micius, dedicated for quantum science experiments (for the project timeline, see Appendix), which was successfully launched on 16th August 2016, from Jiuquan, China, orbiting at an altitude of ~500 km . Within a year after the launch, three key milestones that will be central to a global-scale quantum internet have been achieved: satellite-to-ground decoy-state QKD with kHz rate over a distance of ~1200 km (Liao et al. 2017, Nature 549, 43); satellite-based entanglement distribution to two locations on the Earth separated by ~1200 km and Bell test (Yin et al. 2017, Science 356, 1140), and ground-to-satellite quantum teleportation (Ren et al. 2017, Nature 549, 70). For example, the Xinglong station has now been connected to the metropolitan multi-node quantum network in Beijing via optical fibers. Very recently, the largest fiber-based quantum communication backbone has been built in China by Professor Pan's team, linking Beijing to Shanghai (going through Jinan and Hefei, and 32 trustful relays) with a fiber length of 2000 km. By performing another QKD between the satellite and Nanshan station, and using one-time-pad encoding, secure key between Xinglong and Nanshan were then established.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-09/caos-cbw092717.php){:target="_blank" rel="noopener"}


