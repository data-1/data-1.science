---
layout: post
title: "Estimating species loss from habitat clearing"
date: 2018-06-17
categories:
author: "National University Of Singapore"
tags: [Habitat fragmentation,Extinction,Deforestation,Species,Biodiversity,Conservation biology,Ecology,Natural environment,Biogeochemistry,Branches of science]
---


How many species are lost when a forest is cleared? This formula can also be used to calculate how many species are lost when a habitat area shrinks. But Arrhenius' formula assumes that habitat area is the only spatial variable of importance for species richness; it ignores the spatial pattern of habitat fragmentation. A research team led by Prof Ryan CHISHOLM from Department of Biological Sciences, NUS have developed efficient mathematical formulas that incorporate the effect of habitat fragmentation to provide better estimates of species loss from land-clearing activities. Prof Chisholm said, When we applied the new formulas to estimate tree species loss in Singapore over the last 200 years, we found that the lower bound from our formulas, which assumes maximum fragmentation, was close to independent estimates of tree species loss from Singapore Botanic Gardens herbarium data.

<hr>

[Visit Link](https://phys.org/news/2018-05-species-loss-habitat.html){:target="_blank" rel="noopener"}


