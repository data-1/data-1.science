---
layout: post
title: "Transfering magnetic fields across long distances"
date: 2014-06-25
categories:
author: University Of Innsbruck
tags: [Superconductivity,Spintronics,Physical sciences,Applied and interdisciplinary physics,Science,Electrical engineering,Technology,Physics,Electromagnetism]
---


A new technology transfers magnetic fields to arbitrary long distances, which is comparable to transmitting and routing light in optical fibers. Oriol Romero-Isart and his colleagues have theoretically proposed and already tested this new device experimentally. Magnetic hose  Our theoretical studies have shown that we need a material with extreme anisotropic properties to transfer and route static magnetic fields, explains theoretical physicist Romero-Isart. Even though our technical set-up wasn't perfect, we could show that the static magnetic field is transferred well by the hose, says Prof. Sanchez, the Catalan group leader of Oriol Romero-Isart's collaborators. The work of the physicists from the Universitat Autonoma de Barcelona, the Max-Planck-Institute of Quantum Optics, the Institute for Quantum Optics and Quantum Information of the Austrian Academy of Sciences and the Institute for Theoretical Physics of the University of Innsbruck has been published in the renowned journal Physical Review Letters.

<hr>

[Visit Link](http://phys.org/news322900211.html){:target="_blank" rel="noopener"}


