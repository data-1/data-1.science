---
layout: post
title: "Research sheds light on what causes cells to divide"
date: 2016-04-12
categories:
author: University Of California - San Diego
tags: [Bacteria,Cell (biology),Escherichia coli,Biology,Technology,Branches of science,Science]
---


Or is the real trigger the time period over which the cell keeps growing ever larger? How cells control their size and maintain stable size distributions is one of the most fundamental, unsolved problems in biology, said Suckjoon Jun, an assistant professor of physics and molecular biology at UC San Diego, who headed the research study with Massimo Vergassola, a professor of physics. In our study, we monitored the growth and division of hundreds of thousands of two kinds of bacterial cells, E. coli and B. subtilis, under a wide range of tightly controlled steady-state growth conditions, said Jun. But they also found to their surprise that cell size or the time between cell divisions had little to do with when the cells divided. Instead, to keep the distribution of different sized cells within a population constant, the cells followed what the researchers termed an extraordinarily simple quantitative principle of cell-size control.

<hr>

[Visit Link](http://phys.org/news338621110.html){:target="_blank" rel="noopener"}


