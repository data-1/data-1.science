---
layout: post
title: "The world's first photonic router"
date: 2014-07-16
categories:
author: Weizmann Institute of Science 
tags: [Quantum mechanics,Photon,Photonics,Science,Atom,Physics,Optics,American Association for the Advancement of Science,Research,Atomic molecular and optical physics,Chemistry,Theoretical physics,Electromagnetism,Physical chemistry,Electromagnetic radiation,Applied and interdisciplinary physics,Branches of science,Physical sciences]
---


The state is set just by sending a single particle of light – or photon – from the right or the left via an optical fiber. For example, in one state, a photon coming from the right continues on its path to the left, whereas a photon coming from the left is reflected backwards, causing the atomic state to flip. Yet superposition can only last as long as nothing observes or measures the system otherwise it collapses to a single state. In the current demonstration a single atom functions as a transistor – or a two-way switch – for photons, but in our future experiments, we hope to expand the kinds of devices that work solely on photons, for example new kinds of quantum memory or logic gates. The Weizmann Institute of Science in Rehovot, Israel, is one of the world's top-ranking multidisciplinary research institutions.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/wios-twf071414.php){:target="_blank" rel="noopener"}


