---
layout: post
title: "Cell-sized robots can sense their environment"
date: 2018-07-28
categories:
author: "David L. Chandler"
tags: [Particle,Colloid,Robotics,Robot,Nanotechnology,Integrated circuit,Massachusetts Institute of Technology,Liquid,Pipeline transport,Applied and interdisciplinary physics,Chemistry,Technology,Materials,Materials science,Physical sciences]
---


MIT postdoc Volodymyr Koman is the paper’s lead author. Researchers produced tiny electronic circuits, just 100 micrometers across,on a substrate material which was then dissolved away to leave the individual devices floating freely in solution. (Courtesy of the researchers)  Strano says that while other groups have worked on the creation of similarly tiny robotic devices, their emphasis has been on developing ways to control movement, for example by replicating the tail-like flagellae that some microbial organisms use to propel themselves. A simple photodiode provides the trickle of electricity that the tiny robots’ circuits require to power their computation and memory circuits. Other efforts at nanoscale robotics “haven’t reached that level” of creating complex electronics that are sufficiently small and energy efficient to be aerosolized or suspended in a colloidal liquid.

<hr>

[Visit Link](http://news.mit.edu/2018/cell-sized-robots-sense-their-environment-0723){:target="_blank" rel="noopener"}


