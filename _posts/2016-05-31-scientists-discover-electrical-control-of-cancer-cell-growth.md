---
layout: post
title: "Scientists discover electrical control of cancer cell growth"
date: 2016-05-31
categories:
author: "University of Texas Health Science Center at Houston"
tags: [KRAS,News aggregator,Cell biology,Biology]
---


The research focused on a molecular switch called K-Ras. When K-Ras is locked in the on position, it drives cell division, which leads to the production of a cancer, said John Hancock, M.B., B.Chir, Ph.D., ScD, the study's senior author and chairman of the Department of Integrative Biology and Pharmacology at UTHealth Medical School. The study focused on the tiny electrical charges that all cells carry across their limiting (plasma) membrane. advertisement  With the aid of a high-powered electron microscope, the investigators observed that certain lipid molecules in the plasma membrane respond to an electrical charge, which in turn amplifies the output of the Ras signaling circuit. Hancock and Venkatachalam are on the faculty of The University of Texas Graduate School of Biomedical Sciences at Houston.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150824130820.htm){:target="_blank" rel="noopener"}


