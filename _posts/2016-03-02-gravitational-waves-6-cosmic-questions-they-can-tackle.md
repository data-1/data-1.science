---
layout: post
title: "Gravitational waves: 6 cosmic questions they can tackle"
date: 2016-03-02
categories:
author: Castelvecchi, Davide Castelvecchi, You Can Also Search For This Author In
tags: []
---


The announcement vindicates Albert Einstein’s prediction of gravitational waves, which he made almost exactly 100 years ago1 as part of his general theory of relativity — but it also has much further significance. Do black holes actually exist? Astronomers had plenty of circumstantial evidence for black holes, but until now, that had come from observations of the stars and super-heated gas that orbit black holes, not of black holes themselves. LIGO detected the characteristic sound of these waves, called a chirp, which allowed scientists to measure the masses of the two objects involved in the event the observatory spotted: one about 36 times the mass of the Sun, and the other 29 solar masses. If, like photons, these particles have no mass, then gravitational waves would travel at the speed of light, matching the prediction of the speed of gravitational waves in classical general relativity.

<hr>

[Visit Link](http://www.nature.com/news/gravitational-waves-6-cosmic-questions-they-can-tackle-1.19337){:target="_blank" rel="noopener"}


