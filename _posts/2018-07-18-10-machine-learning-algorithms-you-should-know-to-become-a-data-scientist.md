---
layout: post
title: "10 Machine Learning Algorithms You Should Know to Become a Data Scientist"
date: 2018-07-18
categories:
author: "Mar."
tags: []
---


SVMs can be used to train a classifier (even regressors). Use RNNs for any sequence modeling task especially text classification, machine translation, and language modeling. Library:  https://sklearn-crfsuite.readthedocs.io/en/latest/  Introductory Tutorials  http://blog.echen.me/2012/01/03/introduction-to-conditional-random-fields/  https://www.youtube.com/watch?v=GF3iSJkgPbA  Decision Trees  Let’s say I am given an Excel sheet with data about various fruits and I have to tell which look like Apples. Decision Trees can be used to classify data points (and even regression). This type of Machine Learning is called Reinforcement Learning.

<hr>

[Visit Link](https://dzone.com/articles/ten-machine-learning-algorithms-you-should-know-to?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


