---
layout: post
title: "Machine learning approach could aid the design of industrial processes for drug manufacturing"
date: 2017-09-15
categories:
author: "Larry Hardesty, Massachusetts Institute Of Technology"
tags: [Chemical reaction,Molecule,Chemical substance,Research,Machine learning,Engineering,Atom,Branches of science,Technology]
---


A new computer system predicts the products of chemical reactions. Credit: MIT News  When organic chemists identify a useful chemical compound—a new drug, for instance—it's up to chemical engineers to determine how to mass-produce it. But MIT researchers are trying to put this process on a more secure empirical footing, with a computer system that's trained on thousands of examples of experimental reactions and that learns to predict what a reaction's major products will be. In tests, the system was able to predict a reaction's major product 72 percent of the time; 87 percent of the time, it ranked the major product among its three most likely results. The model might declare, for instance, that if molecule A has reaction site X, and molecule B has reaction site Y, then X and Y will react to form group Z—unless molecule A also has reaction sites P, Q, R, S, T, U, or V.  It's not uncommon for a single model to require more than a dozen enumerated exceptions.

<hr>

[Visit Link](https://phys.org/news/2017-06-machine-approach-aid-industrial-drug.html){:target="_blank" rel="noopener"}


