---
layout: post
title: "Physicists build engine consisting of one atom"
date: 2016-04-16
categories:
author: Johannes Gutenberg Universitaet Mainz
tags: [Engine,Heat,Atom,Heat engine,Energy,Thermodynamics,Science,Nanotechnology,Laser,Technology,Nature,Chemistry,Physical chemistry,Physical sciences,Physics,Applied and interdisciplinary physics]
---


An article in the latest edition of the journal Science describes an innovative form of heat engine that operates using only one single atom. Heat engines have played an important role in shaping society ever since the Industrial Revolution. As in the case of motor vehicle engines, they transform thermal energy into mechanical force, and our modern lifestyle would be impossible without them. A team of researchers led by Professor Kilian Singer, head of the project at Mainz University and now Professor at the University of Kassel, used a Paul trap to capture a single electrically charged calcium atom. As a result, the atom is subjected to a thermodynamic cycle.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/jgum-pbe041516.php){:target="_blank" rel="noopener"}


