---
layout: post
title: "How DNA protects itself from UV light"
date: 2016-05-08
categories:
author: Us Department Of Energy
tags: [Ultrafast laser spectroscopy,Ultraviolet,Chemistry,Physical sciences,Physical chemistry,Atomic molecular and optical physics,Atomic physics,Physics,Applied and interdisciplinary physics,Molecular physics,Electromagnetism,Optics]
---


The experimental findings give new insight on how the nucleobases inside DNA protect themselves from damage induced by ultraviolet light. In addition, the experimental scheme developed will be useful for probing the ultrafast dynamics of other classes of molecules in biology, chemistry and physics. Researchers at SLAC approached this problem by investigating how DNA, which absorbs light very strongly, protects itself by dissipating the UV energy as heat instead of breaking the chemical bonds that hold the DNA together. Measurement of the kinetic energy of the Auger electrons reveals information about the dynamics. Explore further Scientists use X-rays to look at how DNA protects itself from UV light

<hr>

[Visit Link](http://phys.org/news353661547.html){:target="_blank" rel="noopener"}


