---
layout: post
title: "Arachnid Rapunzel: Researchers spin spider silk proteins into artificial silk"
date: 2016-04-13
categories:
author: Biophysical Society
tags: [Spider silk,Protein domain,Protein,Chemistry,Biology,Biotechnology]
---


Jan Rainey's group at Dalhousie University used nuclear magnetic resonance (NMR) spectroscopy to analyze the structure of AcSp1's repeat sequence at very high resolution, producing one of the first spider silk repeat unit structure sequences to be reported. They determined that although in some cases silk proteins can link into fibers without the c-terminal domain, the region in general helped with fiber formation—fibers made of proteins with c-terminal domains tended to be tougher and stronger. In addition, the researchers found replacing the aciniform silk c-terminal domains with c-terminal domains from other types of spider silk also improved fiber formation. The findings suggested that the c-terminal domain could potentially be manipulated to adjust the strength and toughness of the fibers. Artificial spider silk remains far from commercially viable, but advances in understanding of the relationship between spider silk's structure and its function are helping scientists inch closer to creating an alternative in the lab.

<hr>

[Visit Link](http://phys.org/news342799867.html){:target="_blank" rel="noopener"}


