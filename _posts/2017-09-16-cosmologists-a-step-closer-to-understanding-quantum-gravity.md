---
layout: post
title: "Cosmologists a step closer to understanding quantum gravity"
date: 2017-09-16
categories:
author: "University Of Portsmouth"
tags: [Physical cosmology,Quantum mechanics,Cosmology,Universe,Chronology of the universe,Physics,Quantum gravity,Privacy,Inflation (cosmology),Astronomy,Physical sciences,Science,Astrophysics]
---


Dr Vincent Vennin. Credit: University of Portsmouth  Cosmologists trying to understand how to unite the two pillars of modern science – quantum physics and gravity – have found a new way to make robust predictions about the effect of quantum fluctuations on primordial density waves, ripples in the fabric of space and time. Researchers from the University of Portsmouth have revealed quantum imprints left on cosmological structures in the very early Universe and shed light on what we may expect from a full quantum theory of gravity. Yet both play a crucial role in the very early Universe where the expansion of space is driven by gravity and cosmological structures that arise from quantum fluctuations. Structures we see today such as galaxies, stars, planets and people can be traced back to these primordial fluctuations.

<hr>

[Visit Link](https://phys.org/news/2017-01-cosmologists-closer-quantum-gravity.html){:target="_blank" rel="noopener"}


