---
layout: post
title: "Unraveling the light of fireflies"
date: 2016-04-12
categories:
author: Ecole Polytechnique Federale De Lausanne
tags: [Bioluminescence,Firefly,Technology]
---


This detailed microimage shows larger channels branching into smaller ones, supplying oxygen for the firefly's light emission. We know that this reaction needs oxygen, but what we don't know is how fireflies actually supply oxygen to their light-emitting cells. Using state-of-the-art imaging techniques, scientists from Switzerland and Taiwan have determined how fireflies control oxygen distribution to light up their cells. The function of these tubes, called, is to supply oxygen to the cells of the lantern, which contain luciferase and can produce light. The study is the first to ever show the firefly's lantern in such detail, while also providing clear evidence that it is optimized for light emission thanks to the state-of-the-art techniques used by the scientists.

<hr>

[Visit Link](http://phys.org/news338011163.html){:target="_blank" rel="noopener"}


