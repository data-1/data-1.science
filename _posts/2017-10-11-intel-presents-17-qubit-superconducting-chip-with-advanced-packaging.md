---
layout: post
title: "Intel presents 17-qubit superconducting chip with advanced packaging"
date: 2017-10-11
categories:
author: ""
tags: [Semiconductor device fabrication,Integrated circuit,Computing,Quantum computing,Intel,Quantum mechanics,Superconducting quantum computing,Qubit,Neuromorphic engineering,Computer engineering,Electronics,Branches of science,Electrical engineering,Information Age,Computer science,Technology]
---


Credit: Intel Corporation  Today, Intel announced the delivery of a 17-qubit superconducting test chip for quantum computing to QuTech, Intel's quantum research partner in the Netherlands. The delivery of this chip demonstrates the fast progress Intel and QuTech are making in researching and developing a working quantum computing system. Since that time, the collaboration has achieved many milestones – from demonstrating key circuit blocks for an integrated cryogenic-CMOS control system to developing a spin qubit fabrication flow on Intel's 300mm process technology and developing this unique packaging solution for superconducting qubits. Advancing the Quantum Computing System  Intel and QuTech's work in quantum computing goes beyond the development and testing of superconducting qubit devices. While quantum computers promise greater efficiency and performance to handle certain problems, they won't replace the need for conventional computing or other emerging technologies like neuromorphic computing.

<hr>

[Visit Link](https://phys.org/news/2017-10-intel-qubit-superconducting-chip-advanced.html){:target="_blank" rel="noopener"}


