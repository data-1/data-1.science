---
layout: post
title: "Observation of anisotropic magneto-Peltier effect"
date: 2018-07-01
categories:
author: "National Institute For Materials Science"
tags: [Thermoelectric effect,Thermoelectric generator,Electricity,Technology,Electrical engineering,Electromagnetism]
---


Credit: NIMS  NIMS and Tohoku University have jointly observed an anisotropic magneto-Peltier effect—a thermoelectric conversion phenomenon in which simple redirection of a charge current in a magnetic material induces heating and cooling. Although the anisotropic magneto-Peltier effect is a fundamental thermoelectric conversion phenomenon, it has never before been observed. It has been previously observed that the Seebeck effect—a phenomenon in which a temperature difference between a conductor produces a charge current—changes in relation to the direction of magnetization; this is called the anisotropic magneto-Seebeck effect. Application of the anisotropic magneto-Peltier effect may enable thermoelectric temperature control of a magnetic material by simply redirecting a charge current in the material and creating a non-uniform magnetization configuration within it, rather than forming a junction between two different electrical conductors. In future studies, we will attempt to identify and develop magnetic materials that exhibit large anisotropic magneto-Peltier effects and apply them to the development of thermal management technologies that make electronic devices energy-efficient.

<hr>

[Visit Link](https://phys.org/news/2018-06-anisotropic-magneto-peltier-effect.html){:target="_blank" rel="noopener"}


