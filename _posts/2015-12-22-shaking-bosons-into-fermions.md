---
layout: post
title: "Shaking bosons into fermions"
date: 2015-12-22
categories:
author: Joint Quantum Institute
tags: [BoseEinstein condensate,Boson,Fermion,Matter,Optical lattice,Theoretical physics,Physics,Quantum mechanics,Applied and interdisciplinary physics,Physical sciences,Physical chemistry,Condensed matter,Condensed matter physics,Chemistry]
---


This transmutation is an example of emergent behavior, specifically what's known as quasiparticle excitations—one of the concepts that make condensed matter systems so interesting. The experiment harnesses the strengths of atom-optical systems, such as using bosons (which are easier to work with), a relatively simple lattice geometry (made from lasers that are the workhorses of atomic physics), and established measurement techniques. Not much changes when a typical optical lattice is turned on—the bosons will still collect into the lowest energy state and still be in this condensate form. The particles themselves do not actually change from bosons to fermions. In the new band structure, the bosons do not form a condensate.

<hr>

[Visit Link](http://phys.org/news/2015-12-bosons-fermions.html){:target="_blank" rel="noopener"}


