---
layout: post
title: "Scientists mix matter and anti-matter to resolve decade-old proton puzzle"
date: 2016-05-04
categories:
author: Oak Ridge National Laboratory
tags: [Electron,Proton,Photon,Thomas Jefferson National Accelerator Facility,Antimatter,CLAS detector,Nuclear physics,Physical sciences,Science,Applied and interdisciplinary physics,Particle physics,Physics,Quantum mechanics,Atomic physics,Nature]
---


Nuclear physicists have used two different methods to measure the proton's electric form factor. But sometimes, the electron interacts with the proton differently; it may conjure up two virtual photons that it passes on to the proton. But that few percent effect could be big enough to explain this huge difference between the measurements of the proton's electric form factor. It turns out that, while measuring the two-photon effect directly may be too difficult to do now, the scientists could instead measure a different quantity that relates to the effect. The difference between electron and positron interactions calibrates the strength of the two photon effect and its effect on the form factor measurements.

<hr>

[Visit Link](http://phys.org/news352372357.html){:target="_blank" rel="noopener"}


