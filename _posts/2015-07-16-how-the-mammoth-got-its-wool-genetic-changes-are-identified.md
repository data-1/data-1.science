---
layout: post
title: "How the mammoth got its wool: Genetic changes are identified"
date: 2015-07-16
categories:
author: Penn State 
tags: [Woolly mammoth,Genetics,Gene,DNA sequencing,Genome,Mutation,Bioinformatics,American Association for the Advancement of Science,ENCODE,Molecular biology,Biological engineering,Biology,Biotechnology,Life sciences]
---


In a study published in Cell Reports on July 2, 2015, researchers determined the whole-genome sequence of two woolly mammoths and three modern Asian elephants, predicted the function of genetic changes found only in the mammoths, and then experimentally validated the function of a woolly mammoth gene reconstructed in the lab. This project represents a departure from previous efforts to study ancient genomes, said Webb Miller, project co-leader and professor of biology and of computer science and engineering at Penn State. Of these shared, derived variants, the scientists identified 2,020 that resulted in a change to the amino-acid sequence of a protein encoded by a gene and 26 that caused the loss of a gene's function. The collection of 2,020 amino-acid variants that we discovered in mammoths, but that our analysis showed are not present in living elephants, represents a treasure trove that can be mined for the genetic causes of mammoth-specific characteristics and whose functions can be tested in further experiments, said Miller. Of the amino-acid variants shared between the two mammoths, but not found in the living elephants, the research team found a disproportionate number of changes in genes involved in hair development, how the body stores and processes fats, and how the body senses temperature.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/ps-htm070515.php){:target="_blank" rel="noopener"}


