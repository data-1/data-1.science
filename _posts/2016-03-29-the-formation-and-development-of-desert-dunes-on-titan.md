---
layout: post
title: "The formation and development of desert dunes on Titan"
date: 2016-03-29
categories:
author:  
tags: [Titan (moon),Saturn,Sediment,Dune,Wind,CassiniHuygens,Astronomy,Physical sciences,Planets of the Solar System,Nature,Space science,Earth sciences,Planetary science]
---


This growth mechanism, also seen in some terrestrial deserts and on the planet Mars, appears to be the dominant mechanism in the deserts of Titan, explaining not only the shape of the dunes, but also their orientation and direction of growth, together with their confinement to the tropical belt around the moon. The Cassini probe arrived in the Saturn system in 2004 with the objective of studying the giant planet and its moons, including Titan. Their orientation suggests that they have formed over a non-erodable substrate consisting of either solid material or sediment with grains too large to be transported, obtaining sufficient material for their growth from a local source of sediment. These extreme conditions occur at the equinoxes and have also been observed by the Cassini probe. The researchers have also shown that the atmospheric currents on the surface of the moon converge towards the equator, a region where the climatic conditions are the most arid, explaining both the sedimentary deposit and the concentration of dune fields in the intertropical zone.

<hr>

[Visit Link](http://phys.org/news333277284.html){:target="_blank" rel="noopener"}


