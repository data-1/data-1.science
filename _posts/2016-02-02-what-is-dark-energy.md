---
layout: post
title: "What is dark energy?"
date: 2016-02-02
categories:
author: Thomas Kitching, Ucl, The Conversation
tags: [Universe,Euclid (spacecraft),Physical cosmology,Dark energy,Square Kilometre Array,Telescope,Vera C Rubin Observatory,Galaxy,Multiverse,Higgs boson,Astronomy,Weak gravitational lensing,Nature,Physical sciences,Science,Space science,Physics,Cosmology,Astrophysics]
---


One way to do this is to measure weak lensing, an effect where the light from distant galaxies is distorted by matter on its way to us, which unveils the scaffolding of matter in the universe. While we don't yet know what dark energy is, there are three main contenders for it:  Energy of the vacuum . Cosmologists were very happy when the Higgs boson was discovered, partly because it's a manifestation of a Higgs field – the first fundamental scalar field observed in nature. A wind map, on the other hand, isn't a scalar field as it has speed and overall direction. It has been theorised that, like Higgs, dark energy could be another example of a scalar field.

<hr>

[Visit Link](http://phys.org/news/2016-02-dark-energy.html){:target="_blank" rel="noopener"}


