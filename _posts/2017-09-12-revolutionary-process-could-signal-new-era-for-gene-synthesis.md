---
layout: post
title: "Revolutionary process could signal new era for gene synthesis"
date: 2017-09-12
categories:
author: "University of Southampton"
tags: [DNA,Gene,Artificial gene synthesis,Epigenetics,Biology,Life sciences,Genetics,Biotechnology,Biochemistry,Molecular biology,Chemistry,Molecular genetics,Branches of genetics]
---


Current methods for synthesizing genes make extensive use of enzymes (naturally occurring biological catalysts) to connect short strands of DNA to form the larger strands that make up genes. Epigenetic information plays an important role in several biological processes, including diseases such as cancer, meaning that access to epigenetically modified genes is crucial to a better understanding of the disease and its potential treatments. In a study published today [Monday 11 September] in the journal Nature Chemistry, scientists at the University of Southampton, in collaboration with partners at the University of Oxford and ATDBio (a DNA synthesis company based in Southampton and Oxford), demonstrate a purely chemical method for gene assembly which overcomes the limitations of existing methods. Ali Tavassoli, Professor of Chemical Biology at the University of Southampton, who led the study, said: Our approach is a significant breakthrough in gene synthesis. Not only have we demonstrated assembly of a gene using click-chemistry, we have also shown that the resulting strand of DNA is fully functional in bacteria, despite the scars formed by joining fragments.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-09/uos-rpc091117.php){:target="_blank" rel="noopener"}


