---
layout: post
title: "Gene circuits in live cells can perform complex computations"
date: 2016-06-13
categories:
author: "Massachusetts Institute of Technology"
tags: [Synthetic biology,Electronic circuit,Digital electronics,Electronics,Computer,Signal,Massachusetts Institute of Technology,Comparator,Technology]
---


But now a team of researchers at MIT has developed a technique to integrate both analogue and digital computation in living cells, allowing them to form gene circuits capable of carrying out complex processing operations. The synthetic circuits, presented in a paper published today in the journal Nature Communications, are capable of measuring the level of an analogue input, such as a particular chemical relevant to a disease, and deciding whether the level is in the right range to turn on an output, such as a drug that treats the disease. However, since digital systems are based on a simple binary output such as 0 or 1, performing complex computational operations requires the use of a large number of parts, which is difficult to achieve in synthetic biological systems. So this is how we take an analogue input, such as a concentration of a chemical, and convert it into a 0 or 1 signal, Lu says. The team has already built an analogue-to-digital converter circuit that implements ternary logic, a device that will only switch on in response to either a high or low concentration range of an input, and which is capable of producing two different outputs.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/miot-gci060216.php){:target="_blank" rel="noopener"}


