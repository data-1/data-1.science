---
layout: post
title: "Luck plays a role in how language evolves, Penn team finds"
date: 2017-11-19
categories:
author: "University of Pennsylvania"
tags: [Regular and irregular verbs,Linguistics,Question,Language,English language,Parsing,Evolution,Cognitive science,Cognition,Branches of linguistics,Human communication,Branches of science,Interdisciplinary subfields,Culture]
---


In a new study published in Nature, researchers in these two academic fields have joined forces at the University of Pennsylvania to solve an essential problem of how languages evolve: determining whether language changes occur by random chance or by a selective force. Linguists usually assume that when a change occurs in a language, there must have been a directional force that caused it, said Joshua Plotkin, professor of biology in Penn's School of Arts and Sciences and senior author on the paper. Whereas we propose that languages can also change through random chance alone. These corpora are the result of generations of work, much of it by Penn linguists, to parse written texts and annotate parts of speech. They found that the first stage of the rising periphrastic do use is consistent with random chance.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/uop-lpa103017.php){:target="_blank" rel="noopener"}


