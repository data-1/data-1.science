---
layout: post
title: "Construction set of magnon logic extended: Magnon spin currents can be controlled via spin valve structure"
date: 2018-07-01
categories:
author: "Universität Mainz"
tags: [Spintronics,Magnon,Magnetism,Ferromagnetism,Materials,Technology,Phases of matter,Chemical product engineering,Condensed matter,Electromagnetism,Condensed matter physics,Physics,Applied and interdisciplinary physics,Electrical engineering,Materials science,Electricity]
---


In the emerging field of magnon spintronics, researchers investigate the possibility to transport and process information by means of so-called magnon spin currents. In a so-called spin valve structure, which amongst others comprises several ferromagnets, it was possible to demonstrate that the detection efficiency of magnon currents depends on the magnetic configuration of the device. The research work has been published in the online journal Nature Communications with a fellow of the JGU-based Graduate School of Excellence Materials Science in Mainz (MAINZ) as first author. The result of our experiment is an effect which might find application in prospective magnon logic operations, thus yielding an essential contribution to the field of magnon spintronic, Cramer added. Hence, I would like to thank the group of Professor Saitoh and the Institute for Materials Research at Tohoku University for their hospitality and the excellent collaboration, added Kläui.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/03/180314092809.htm){:target="_blank" rel="noopener"}


