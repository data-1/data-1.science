---
layout: post
title: "Heatwaves and Hospital Admissions for Mental Disorders in Northern Vietnam"
date: 2016-05-21
categories:
author: Phan Minh Trang, Department Of Public Health, Clinical Medicine, Unit Of Epidemiology, Global Health, Umeå University, Joacim Rocklöv, Kim Bao Giang, Institute For Preventive Medicine, Public Health
tags: []
---


Thus, in the present study, a definition of heatwaves similar to that of other studies was used to estimate the effect of heatwaves in relation to hospital admissions for mental diseases. When comparing the relative risk associated with the same groups at only one day of extreme temperature with extreme temperature of at least three or seven consecutive days, there were increased trends with the longer heat events, particularly among F70-71, with relative risks 1.18 (0.86–1.62), 1.75 (1.11–2.76) and 2.69 (0.9–8.14), respectively ( Table 3 ). Overall, the results indicated that admissions followed a pattern of increased relative risk when the duration of the heatwave was from one day to seven days, and that ages over 60 years and residents in rural areas accounted for the highest relative risk increases ( Table 3 ). During heatwaves of at least three consecutive days, admissions for mental disorders among rural populations and among men estimated a relative risk of 1.26 (1.04–1.52) and 1.15 (1–1.33), respectively. The number of admissions for mental disorders (F00-79) in the older group of over 60 years of age had an increased trend with longer exposure to heat extremes.

<hr>

[Visit Link](http://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0155609){:target="_blank" rel="noopener"}


