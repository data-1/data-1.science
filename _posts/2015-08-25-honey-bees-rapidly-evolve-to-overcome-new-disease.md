---
layout: post
title: "Honey bees rapidly evolve to overcome new disease"
date: 2015-08-25
categories:
author: Okinawa Institute of Science and Technology - OIST 
tags: [Evolution,Bee,Genetics,Varroa destructor,Reproduction,Mite,Pollination,Gene,Honey bee,Evolutionary biology,Biology]
---


Mikheyev and his collaborators at OIST and Cornell University studied the population genetics of the wild colony by comparing the DNA of specimens collected in 1977 with bees collected from the same forest in 2010. By comparing bees from the same colony only a few decades a part, the team was able to see this natural selection in action. First, mitochondrial DNA, the genetic material stored in cells' power plants, changed significantly from the older generation to the newer generation. Genetic diversity is the raw material for evolution, and high genetic diversity increases the chance for successful adaptation. The researchers also found many changes in genes associated with development.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150819083650.htm){:target="_blank" rel="noopener"}


