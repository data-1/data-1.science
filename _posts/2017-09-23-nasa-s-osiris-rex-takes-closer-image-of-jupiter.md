---
layout: post
title: "NASA's OSIRIS-REx takes closer image of Jupiter"
date: 2017-09-23
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Moons of Jupiter,OSIRIS-REx,Jupiter,Ganymede (moon),Io (moon),Natural satellite,Callisto (moon),Space science,Outer space,Astronomy,Solar System,Planetary science,Bodies of the Solar System,Spaceflight,Astronomical objects,Astronautics,Planets of the Solar System,Space exploration,Spacecraft,Astronomical objects known since antiquity,Flight]
---


During Earth-Trojan asteroid search operations, the PolyCam imager aboard NASA's OSIRIS-REx spacecraft captured this image of Jupiter (center) and three of its moons, Callisto (left), Io, and Ganymede. PolyCam is OSIRIS-REx's longest range camera, capable of capturing images of the asteroid Bennu from a distance of two million kilometers. NASA's Goddard Space Flight Center in Greenbelt, Maryland provides overall mission management, systems engineering and the safety and mission assurance for OSIRIS-REx. Dante Lauretta of the University of Arizona, Tucson, is the principal investigator, and the University of Arizona also leads the science team and the mission's observation planning and processing. OSIRIS-REx is the third mission in NASA's New Frontiers Program.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/nsfc-not021517.php){:target="_blank" rel="noopener"}


