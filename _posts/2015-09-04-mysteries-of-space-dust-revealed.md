---
layout: post
title: "Mysteries of space dust revealed"
date: 2015-09-04
categories:
author: "$author" 
tags: [Stardust (spacecraft),Cosmic dust,Astronomy,Nature,Outer space,Space science,Science,Physical sciences]
---


The first analysis of space dust collected by a special collector onboard NASA's Stardust mission and sent back to Earth for study in 2006 suggests the tiny specks, which likely originated from beyond our solar system, are more complex in composition and structure than previously imagined. The analysis, completed at a number of facilities including the U.S. Department of Energy's Lawrence Berkeley National Lab (Berkeley Lab) opens a door to studying the origins of the solar system and possibly the origin of life itself. Westphal, who is also affiliated with Berkeley Lab's Advanced Light Source, a DOE Office of Science User Facility where some of the research was conducted, and his 61 co-authors found and analyzed a total of seven grains of possible interstellar dust and presented preliminary findings. The analysis of these particles captured by Stardust is our first glimpse into the complexity of interstellar dust, and the surprise is that each of the particles are quite different from each other. Three of the particles found in the aluminum foil were also complex, and contain sulfur compounds, which some astronomers believe should not occur in interstellar dust particles.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/dbnl-mos081314.php){:target="_blank" rel="noopener"}


