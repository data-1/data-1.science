---
layout: post
title: "Urbanization costs five billion years of evolutionary history"
date: 2017-09-11
categories:
author: "Helmholtz Centre For Environmental Research - UFZ"
tags: [Biodiversity,News aggregator,Species,Evolution,Ecology,Biogeography,Environmental protection,Environmental science,Conservation biology,Natural environment,Earth sciences,Biogeochemistry,Nature,Environmental conservation,Environmental social science,Nature conservation,Systems ecology]
---


All over the globe, the urbanization of landscapes is increasing. The researchers drew on lists of species published by botanists since the 17th century as well as data from herbaria. In the centuries that followed, during which the city's population increased more than tenfold, more than 20 botanists recorded the flora of Halle. Overall, 4.7 billion years of evolutionary history have therefore been lost in the Halle region, so great is the loss of evolutionary diversity -- calculated on the basis of plant pedigrees. The team calculated how the current evolutionary diversity of Halle's flora would change if, firstly, the plants found in Halle listed on the Red List of endangered species disappeared and, secondly, the most common introduced species in Germany which are not yet found in Halle were to migrate there.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/04/170426093154.htm){:target="_blank" rel="noopener"}


