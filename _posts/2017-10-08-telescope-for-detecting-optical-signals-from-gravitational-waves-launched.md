---
layout: post
title: "Telescope for detecting optical signals from gravitational waves launched"
date: 2017-10-08
categories:
author: "University Of Warwick"
tags: [Gravitational wave,Astronomy,Science,Space science,Physical sciences,Physics,Astrophysics,Physical phenomena,Scientific observation]
---


The Gravitational-wave Optical Transient Observer (GOTO) was inaugurated at Warwick's astronomical observing facility in the Roque de los Muchachos Observatory of the Instituto de Astrofísica de Canarias on La Palma, Canary Islands, on 3 July 2017. GOTO is an autonomous, intelligent telescope, which will search for unusual activity in the sky, following alerts from gravitational wave detectors - such as the Advanced Laser Interferometer Gravitational-Wave Observatory (Adv-LIGO), which recently secured the first direct detections of gravitational waves. First predicted over a century ago by Albert Einstein, they have only been directly detected in the last two years, and astronomers' next challenge is to associate the signals from these waves with signatures in the electromagnetic spectrum, such as optical light. Dr. Duncan Galloway, from the School of Physics & Astronomy at Monash University, comments:  GOTO is very significant for the Monash Centre for Astrophysics. We've invested strongly in gravitational wave astronomy over the last few years, leading up to the first detection announced last year, and the telescope project represents a fundamentally new observational opportunity.

<hr>

[Visit Link](https://phys.org/news/2017-07-telescope-optical-gravitational.html){:target="_blank" rel="noopener"}


