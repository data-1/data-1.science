---
layout: post
title: "Linux for lettuce"
date: 2015-06-02
categories:
author: "Lisa M. Hamilton"
tags: [Open Source Seed Initiative,Patent,Plant breeding]
---


Thus the need for a protected commons—open source seed. If seeds are software, then protecting them as intellectual property is a natural, even essential, requirement for their technological development. If you’re in plant breeding, you know you can’t do it on your own, Michaels told me. No patent would be necessary. “Patents on naturally occurring biodiversity in plant breeding are an abuse of patent law,” the opposition statement read, “because instead of protecting inventions they become an instrument for the misappropriation of natural resources.” [1]  Their argument centers around a single line in the European Patent Convention, Article 53b, which states that patents shall not be granted on “plant or animal varieties or essentially biological processes for the production of plants or animals.” Recent objections to similar claims (one on a different broccoli, another on a tomato) led the European Patent Office’s board of appeals to clarify that a new variety created by simply crossing plants and selecting their offspring—exactly the work of Myers and the Seminis broccoli breeders alike—was considered essentially biological and so not patentable.

<hr>

[Visit Link](http://opensource.com/life/14/11/linux-lettuce-open-food-seeds){:target="_blank" rel="noopener"}


