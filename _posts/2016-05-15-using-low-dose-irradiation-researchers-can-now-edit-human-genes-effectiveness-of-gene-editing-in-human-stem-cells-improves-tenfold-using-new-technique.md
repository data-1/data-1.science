---
layout: post
title: "Using low-dose irradiation, researchers can now edit human genes: Effectiveness of gene editing in human stem cells improves tenfold using new technique"
date: 2016-05-15
categories:
author: Cedars-Sinai Medical Center
tags: [CRISPR gene editing,Genome editing,Stem cell,Regenerative medicine,News aggregator,Radiation therapy,Genetics,Life sciences,Molecular biology,Branches of genetics,Biotechnology,Clinical medicine,Medicine,Health sciences,Biology]
---


This method, developed by researchers in the Cedars-Sinai Board of Governors Regenerative Medicine Institute, is 10 times more effective than techniques currently in use. This novel technique allows for far more efficient gene editing of stem cells and will increase the speed of new discoveries in the field, said co-senior author Clive Svendsen, PhD, director of the Board of Governors Regenerative Medicine Institute. Gene editing allows scientists to correct irregular mutations and, theoretically, cure the disease in the petri dish. When using this form of gene editing, Cedars-Sinai scientists can more efficiently insert reporter genes that glow when a stem cell turns into a specific cell of the body. This work allows scientists to test novel drugs on human cells that carry disease-causing genes.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150722150920.htm){:target="_blank" rel="noopener"}


