---
layout: post
title: "Global Renewable Energy Industry Adds 147 GW In 2015, Invests $328 Billion"
date: 2016-06-12
categories:
author: "Joshua S Hill, Zachary Shahan, James Ayre, Guest Contributor, Written By"
tags: [Renewable energy,REN21,Renewable energy commercialization,Wind power,Sustainable technologies,Technology,Electric power,Natural resources,Economy,Climate change mitigation,Power (physics),Sustainable development,Energy and the environment,Environmental technology,Renewable resources,Energy,Nature,Physical quantities,Sustainable energy,Economy and the environment]
---


The global renewable energy industry added a record 147 GW of new capacity in 2015, according to the latest annual status report from REN21. Wind increased from its total cumulative capacity from 370 GW in 2014 to 433 GW in 2015, while solar added 50 GW, growing from 177 GW to 227 GW. In total, renewable energy’s share of the global electricity production sat at 23.7%, with hydropower responsible for the lion’s share of that. More investment means more jobs, too, with 8.1 million people now working in the global renewable energy sector — a number confirmed last week by the International Renewable Energy Agency. Or follow us on Google News Have a tip for CleanTechnica, want to advertise, or want to suggest a guest for our CleanTech Talk podcast?

<hr>

[Visit Link](http://cleantechnica.com/2016/06/01/global-renewable-energy-industry-adds-147-gw-2015-invests-328-billion/){:target="_blank" rel="noopener"}


