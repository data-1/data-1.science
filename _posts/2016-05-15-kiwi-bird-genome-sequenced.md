---
layout: post
title: "Kiwi bird genome sequenced"
date: 2016-05-15
categories:
author: Max Planck Society
tags: [Kiwi (bird),Evolution,Bird,Species,Sense of smell,Flightless bird,Biology]
---


Researchers of the University of Leipzig and the Max Planck Institute for Evolutionary Anthropology in Leipzig, Germany, have now sequenced the genetic code of this endangered species and have identified several sequence changes that underlie the kiwi's adaptation to a nocturnal lifestyle: They found several genes involved in colour vision to be inactivated and the diversity of odorant receptors to be higher than in other birds - suggesting an increased reliance on their sense of smell rather than vision for foraging. The study was published in the journal Genome Biology. They are mainly nocturnal with a low basal metabolic rate and the lowest body temperature among birds. Already French botanist and zoologist Jean Baptiste de Lamarck, who lived in the 18th century, hypothesized that evolution works in accordance with a 'use it or lose it' principle. Following human migration to New Zealand around 800 years ago, many of the local bird species became extinct.

<hr>

[Visit Link](http://phys.org/news/2015-07-kiwi-bird-genome-sequenced.html){:target="_blank" rel="noopener"}


