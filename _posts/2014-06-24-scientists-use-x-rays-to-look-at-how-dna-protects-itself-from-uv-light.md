---
layout: post
title: "Scientists use X-rays to look at how DNA protects itself from UV light"
date: 2014-06-24
categories:
author: Slac National Accelerator Laboratory
tags: [Ultraviolet,SLAC National Accelerator Laboratory,Light,Molecule,DNA,Electron,Chemistry,Physics,Physical sciences,Science,Applied and interdisciplinary physics]
---


It’s such a strong absorber of ultraviolet light that the UV in sunlight should deactivate it, yet this does not happen. In a study reported in Nature Communications, researchers used an X-ray laser at SLAC National Accelerator Laboratory to make detailed observations of a “relaxation response” that protects these molecules, and the genetic information they encode, from UV damage. The experiment at the Department of Energy's SLAC National Accelerator Laboratory focused on thymine, one of four DNA building blocks. Researchers hit thymine with a short pulse of ultraviolet light and used a powerful X-ray laser to watch the molecule's response: A single chemical bond stretched and snapped back into place within 200 quadrillionths of a second, setting off a wave of vibrations that harmlessly dissipated the destructive UV energy. While protecting the genetic information encoded in DNA is vitally important, the significance of this result goes far beyond DNA chemistry, said Philip Bucksbaum, director of the Stanford PULSE Institute and a co-author of the report.

<hr>

[Visit Link](http://phys.org/news322753553.html){:target="_blank" rel="noopener"}


