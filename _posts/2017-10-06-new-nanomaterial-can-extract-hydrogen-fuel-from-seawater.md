---
layout: post
title: "New nanomaterial can extract hydrogen fuel from seawater"
date: 2017-10-06
categories:
author: "University of Central Florida"
tags: [Photocatalysis,Solar energy,Hydrogen,Seawater,Fuel,Water,Hybrid vehicle,Energy,Chemistry,Nature,Physical sciences,Materials,Technology,Sustainable energy,Sustainable technologies,Energy technology,Chemical substances]
---


It's possible to produce hydrogen to power fuel cells by extracting the gas from seawater, but the electricity required to do it makes the process costly. It's done using a photocatalyst - a material that spurs a chemical reaction using energy from light. When he began his research, Yang focused on using solar energy to extract hydrogen from purified water. We can absorb much more solar energy from the light than the conventional material, Yang said. Yang's team is continuing its research by focusing on the best way to scale up the fabrication, and further improve its performance so it's possible to split hydrogen from wastewater.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uocf-nnc100417.php){:target="_blank" rel="noopener"}


