---
layout: post
title: "Magnetic reconnection: Magnetic explosions in Northern lights and solar flares"
date: 2015-12-22
categories:
author: NASA/Goddard Space Flight Center
tags: [Magnetosphere,Magnetic reconnection,Sun,Solar flare,Plasma (physics),Interplanetary medium,Aurora,Solar wind,Magnetopause,Earth,Energy,Electrical engineering,Phases of matter,Space plasmas,Astrophysics,Plasma physics,Applied and interdisciplinary physics,Physical sciences,Nature,Astronomy,Electromagnetism,Physics,Space science]
---


Just under four months into the science phase of the mission, NASA's Magnetospheric Multiscale, or MMS, is delivering promising early results on a process called magnetic reconnection -- a kind of magnetic explosion that's related to everything from the northern lights to solar flares. All of this magnetic and electric energy means that magnetic reconnection plays a huge role in shaping the environment wherever plasma exists -- whether that's on the sun, in interplanetary space, or at the boundaries of Earth's magnetic system. This suite of instruments, the FIELDS suite -- duplicated on each of the four MMS spacecraft -- contains six sensors that work together to form a three-dimensional picture of the electric and magnetic fields near the spacecraft. Goodrich presented observations from MMS that showed how the FIELDS suite can spot examples of parallel electric fields at time scales down to half a second. Cohen presented MMS observations that are clearly able to distinguish between the directions the particles are moving, which will help scientists better understand what mechanisms drive magnetic reconnection.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/12/151218161458.htm){:target="_blank" rel="noopener"}


