---
layout: post
title: "A programming language for living cells"
date: 2016-04-10
categories:
author: Massachusetts Institute of Technology
tags: [Computer programming,Bacteria,Cell (biology),Genetics,DNA,Sensor,DNA sequencing,Yeast,Biology,Life sciences,Technology]
---


Using this language, anyone can write a program for the function they want, such as detecting and responding to certain environmental conditions. It is literally a programming language for bacteria, says Christopher Voigt, an MIT professor of biological engineering. Users of the new programming language, however, need no special knowledge of genetic engineering. To create a version of the language that would work for cells, the researchers designed computing elements such as logic gates and sensors that can be encoded in a bacterial cell's DNA. Now you just hit the button and immediately get a DNA sequence to test, Voigt says.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/miot-apl032916.php){:target="_blank" rel="noopener"}


