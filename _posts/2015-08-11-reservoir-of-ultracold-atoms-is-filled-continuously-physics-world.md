---
layout: post
title: "Reservoir of ultracold atoms is filled continuously – Physics World"
date: 2015-08-11
categories:
author: Hamish Johnston
tags: [Magneto-optical trap,Science,Nature,Theoretical physics,Chemistry,Applied and interdisciplinary physics,Physical chemistry,Physical sciences,Physics]
---


This uses laser beams and magnetic fields to cool and guide about 10 billion rubidium-87 atoms at temperatures as low as 25 μK. Others will not lose energy (or will even gain energy) and these will be able to escape from the trap. An important feature of the device is that the reservoir is a magnetic trap that does not contain any laser light. The advantage of using a reservoir that is continuously pumped is that hot atoms escaping the reservoir will be replaced by colder ones, thereby enhancing the cooling process. This would also involve lowering the energy of the entrance barrier so that only very cold atoms remain in the trap.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/jul/30/reservoir-of-ultracold-atoms-is-filled-continuously){:target="_blank" rel="noopener"}


