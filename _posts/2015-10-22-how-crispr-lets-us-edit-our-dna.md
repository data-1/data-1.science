---
layout: post
title: "How CRISPR lets us edit our DNA"
date: 2015-10-22
categories:
author: Jennifer Doudna
tags: [CRISPR gene editing,Designer baby,Jennifer Doudna,Biological engineering,Branches of genetics,Molecular biology,Life sciences,Biotechnology,Biology,Genetics,Biochemistry,Molecular genetics,Modification of genetic information,Genomics]
---


Geneticist Jennifer Doudna co-invented a groundbreaking new technology for editing genes, called CRISPR-Cas9. The tool allows scientists to make precise edits to DNA strands, which could lead to treatments for genetic diseases ... but could also be used to create so-called designer babies. Doudna reviews how CRISPR-Cas9 works -- and asks the scientific community to pause and discuss the ethics of this new tool.

<hr>

[Visit Link](http://www.ted.com/talks/jennifer_doudna_we_can_now_edit_our_dna_but_let_s_do_it_wisely){:target="_blank" rel="noopener"}


