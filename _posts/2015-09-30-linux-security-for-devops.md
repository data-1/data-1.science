---
layout: post
title: "Linux Security for DevOps"
date: 2015-09-30
categories:
author: Michael Boelen
tags: [Linux,Lynis,Puppet (software),Patch (computing),System administrator,System software,Digital media,Computer engineering,Information Age,Systems engineering,Software development,Computing,Information technology,Software engineering,Technology,Information technology management,Software,Computer science]
---


We are especially interested in Linux security for DevOps and what they can apply. From the previously mentioned CMDB, used software components, documentation tools or developer repositories. Simply trusting your configuration management solution is not enough. Keep on improving in steps and don’t forget the “Check”. Security audits will give new insights and room for improvement.

<hr>

[Visit Link](http://linux-audit.com/linux-security-for-devops/){:target="_blank" rel="noopener"}


