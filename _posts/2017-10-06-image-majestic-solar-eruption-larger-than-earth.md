---
layout: post
title: "Image: Majestic solar eruption larger than Earth"
date: 2017-10-06
categories:
author: "European Space Agency"
tags: [Sun,Stellar corona,Coronal mass ejection,Solar and Heliospheric Observatory,Physical sciences,Space science,Physics,Phases of matter,Plasma physics,Astrophysics,Astronomical objects known since antiquity,Stellar astronomy,Electromagnetism,Astronomy,Nature,Space plasmas,Outer space,Solar System,Bodies of the Solar System,Astronomical objects]
---


This remarkable image was captured on 27 July 1999 by SOHO, the Solar and Heliospheric Observatory. Earth is superimposed for comparison and shows that from top to bottom the loop of gas, or prominence, extends about 35 times the diameter of our planet into space. A prominence is an extension of gas that arches up from the surface of the Sun. Prominences are sculpted by magnetic fields that are generated inside the Sun, and then burst through the surface, propelling themselves into the solar atmosphere. These eruptive prominences fling out a huge quantity of plasma that solar physicists call a coronal mass ejection.

<hr>

[Visit Link](http://phys.org/news/2016-08-image-majestic-solar-eruption-larger.html){:target="_blank" rel="noopener"}


