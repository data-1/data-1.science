---
layout: post
title: "These Are The Top 10 Emerging Technologies Of 2016"
date: 2016-07-09
categories:
author: ""
tags: [General Electric,Technology,Computer network,Sustainable energy,Innovation,Electrical grid,Energy development,World Economic Forum,Infrastructure,Analytics,Economic growth,Hybrid power,Sustainability,Engine,Culture,Branches of science,Economy]
---


The World Economic Forum's annual list of emerging technologies, released this summer, include both familiar and unfamiliar discoveries. A diverse range of breakthrough technologies, including batteries capable of providing power to whole villages, “socially aware” artificial intelligence and new generation solar panels, could soon be playing a role in tackling the world’s most pressing challenges, according to    A diverse range of breakthrough technologies, including batteries capable of providing power to whole villages, “socially aware” artificial intelligence and new generation solar panels, could soon be playing a role in tackling the world’s most pressing challenges, according to a list published by the World Economic Forum. With related venture investment exceeding $1 billion in 2015 alone, the economic and social impact of blockchain’s potential to fundamentally change the way markets and governments work is only now emerging. To compile this list, the World Economic Forum’s        (Top image: A graphene tube. To compile this list, the World Economic Forum’s Meta-Council on Emerging Technologies , a panel of global experts, drew on the collective expertise of the Forum’s communities to identify the most important recent technological trends.

<hr>

[Visit Link](http://www.gereports.com/these-are-the-top-10-emerging-technologies-of-2016/){:target="_blank" rel="noopener"}


