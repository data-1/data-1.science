---
layout: post
title: "Antibiotic-resistant microbes date back to 450 million years ago, well before the age of dinosaurs: Survival of mass extinctions helps to explain near indestructible properties of hospital superbugs"
date: 2017-10-06
categories:
author: "Massachusetts Eye and Ear Infirmary"
tags: [Antimicrobial resistance,Antibiotic,Evolution,Bacteria,Medical specialties,Biology,Nature,Microbiology]
---


Leading hospital superbugs, known as the enterococci, arose from an ancestor that dates back 450 million years -- about the time when animals were first crawling onto land (and well before the age of dinosaurs), according to a new study led by researchers from Massachusetts Eye and Ear, the Harvard-wide Program on Antibiotic Resistance and the Broad Institute of MIT and Harvard. By analyzing the genomes and behaviors of today's enterococci, we were able to rewind the clock back to their earliest existence and piece together a picture of how these organisms were shaped into what they are today said co-corresponding author Ashlee M. Earl, Ph.D., group leader for the Bacterial Genomics Group at the Broad Institute of MIT and Harvard. Some bacteria protect and serve the animals, as the healthy microbes in our intestines do today; others live in the environment, and still others cause disease. The authors of the Cell study found that all species of enterococci, including those that have never been found in hospitals, were naturally resistant to dryness, starvation, disinfectants and many antibiotics. From sea animals, like fish, intestinal microbes are excreted into the ocean, which usually contains about 5,000 mostly harmless bacteria per drop of water.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/05/170511142012.htm){:target="_blank" rel="noopener"}


