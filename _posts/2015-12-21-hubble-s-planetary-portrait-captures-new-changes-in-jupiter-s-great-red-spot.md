---
layout: post
title: "Hubble's planetary portrait captures new changes in Jupiter's Great Red Spot"
date: 2015-12-21
categories:
author: NASA/Goddard Space Flight Center
tags: [Jupiter,Great Red Spot,Planet,Hubble Space Telescope,Goddard Space Flight Center,Solar System,Voyager 2,NASA,Physical sciences,Science,Planets of the Solar System,Sky,Bodies of the Solar System,Planets,Astronomical objects,Outer space,Planetary science,Space science,Astronomy]
---


Scientists using NASA's Hubble Space Telescope have produced new maps of Jupiter -- the first in a series of annual portraits of the solar system's outer planets. Already, the Jupiter images have revealed a rare wave just north of the planet's equator and a unique filamentary feature in the core of the Great Red Spot not seen previously. The new images confirm that the Great Red Spot continues to shrink and become more circular, as it has been doing for years. Hubble will dedicate time each year to this special set of observations, called the Outer Planet Atmospheres Legacy program. The long-term value of the Outer Planet Atmospheres Legacy program is really exciting, said co-author Michael H. Wong of the University of California, Berkeley.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/nsfc-hpp101315.php){:target="_blank" rel="noopener"}


