---
layout: post
title: "Industrial Medicine: Cell Therapy Scales Up"
date: 2018-04-20
categories:
author: ""
tags: [General Electric,Technology,Cell therapy,Innovation,Electrical grid,Computer network,Energy development,Sustainable energy,3D printing,Health care,Culture,Dose (biochemistry),Cancer,Medicine,Engine,Infrastructure,Hybrid power,Gene therapy]
---


Above: GE Healthcare is working with the Centre for Commercialization of Regenerative Medicine (CCRM) in Toronto to commercialize cell therapy. Top: As medical science advances and increasing numbers of modified cell therapies are approved for general use, handcrafting doses will be too slow to scale. “It’s relatively easy to do 15 or 20 doses by hand, but it’s nearly impossible to efficiently make thousands,” says GE Healthcare’s Aaron Dulgar-Tulloch, director of cell therapy research and development at the Centre for Advanced Therapeutic Cell Technologies (CATCT) in Toronto.One way to speed the process is GE Healthcare's FlexFactory for cell therapy. “FlexFactory for cell therapy allows a company to manufacture multiple (doses) for multiple, unique patients at a faster speed,” Dulgar-Tulloch says.The FlexFactory is also easier to install than traditional methods. It can be operational in nine months, about half the time it would require a company to make a traditional lab ready for industrial-scale cell manufacturing.

<hr>

[Visit Link](https://www.ge.com/reports/industrial-medicine-cell-therapy-scales/){:target="_blank" rel="noopener"}


