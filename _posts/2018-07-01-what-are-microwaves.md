---
layout: post
title: "What Are Microwaves?"
date: 2018-07-01
categories:
author: "Jim Lucas"
tags: [Electromagnetic spectrum,Microwave,Radar,Electromagnetic radiation,Radio,Waves,Science,Physical phenomena,Radiation,Physics]
---


Microwaves have a range of applications, including communications, radar and, perhaps best known by most people, cooking. Prior to World War II, British radio engineers found that short-wavelength radio waves could be bounced off distant objects like ships and aircraft, and the returning signal could be detected with highly sensitive directional antennas so the presence and locations of those objects could be determined. Conversely, return waves from objects moving away are elongated and have a longer wavelength and lower frequency. Microwave heating systems are also used in a number of industrial applications, including food, chemical and materials processing in both batch and continuous operations. The Bell Lab scientists soon realized that they had serendipitously discovered the cosmic microwave background radiation.

<hr>

[Visit Link](https://www.livescience.com/50259-microwaves.html){:target="_blank" rel="noopener"}


