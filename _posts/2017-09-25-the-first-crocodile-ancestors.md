---
layout: post
title: "The first crocodile ancestors"
date: 2017-09-25
categories:
author: "Jon Tennant, Public Library Of Science"
tags: [Archosaur,Crocodilia,Carnufex,Pseudosuchia,Dinosaur,Animals,Taxa]
---


Importantly, it is from around the time when this dinosaur-crocodile split occurred, and therefore should hold important clues to the evolutionary history of these groups. The teeth of Carnufex, perfect for piercing and slicing flesh  By analysing the anatomy of Carnufex along with a large range of other similar animals, they were able to work out its evolutionary relationships. What this implies is that Carnufex is actually one of the earliest diverging crocodylomorphs, and therefore was highly important in determining the early fate of this ancient group. This is quite exceptional, as other crocodylomorphs at the time were by no means top tier predators, with this role usually taken on by other now extinct archosaurs. Explore further Crocodile ancestor was top predator before dinosaurs roamed North America

<hr>

[Visit Link](http://phys.org/news/2016-08-crocodile-ancestors.html){:target="_blank" rel="noopener"}


