---
layout: post
title: "An ocean lies a few kilometers beneath Saturn's moon Enceladus's icy surface"
date: 2016-06-23
categories:
author: "CNRS"
tags: [Enceladus,Astronomical objects known since antiquity,Physical sciences,Planets,Planemos,Ancient astronomy,Science,Astronomical objects,Outer space,Solar System,Bodies of the Solar System,Planets of the Solar System,Space science,Planetary science,Astronomy]
---


An international team including researchers from the Laboratoire de Planétologie Géodynamique de Nantes (CNRS/Université de Nantes/Université d'Angers), Charles University in Prague, and the Royal Observatory of Belgium[1] recently proposed a new model that reconciles different data sets and shows that the ice shell at Enceladus's south pole may be only a few kilometers thick. This suggests that there is a strong heat source in the interior of Enceladus, an additional factor supporting the possible emergence of life in its ocean. Initial interpretations of data from Cassini flybys of Enceladus estimated that the thickness of its ice shell ranged from 30 to 40 km at the south pole to 60 km at the equator. According to this study, Enceladus is made up successively of a rocky core with a radius of 185 km, and an internal ocean approximately 45 km deep, isolated from the surface by an ice shell with a mean thickness of around 20 km, except at the south pole where it is thought to be less than 5 km thick. Since a thinner ice shell retains less heat, the tidal effects caused by Saturn on the large fractures in the ice at the south pole are no longer enough to explain the strong heat flow affecting this region.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/06/160621115743.htm){:target="_blank" rel="noopener"}


