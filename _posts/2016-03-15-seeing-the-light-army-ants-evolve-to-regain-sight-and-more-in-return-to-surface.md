---
layout: post
title: "Seeing the light: Army ants evolve to regain sight and more in return to surface"
date: 2016-03-15
categories:
author: Frank Otto, Drexel University
tags: [Brain,Evolution,Ecology,Ant,Visual perception,Neuroscience,Cognitive science]
---


But some of the ants that O'Donnell and his research partners studied appeared to grow back parts of the brain used in seeing. It appeared to be a rare example of a species' brain tissue increasing over time following a move to a more complex environment. In their study, the Drexel researchers found that the ants that lived mostly on the surface—Eciton hamatum, Eciton mexicanum and Eciton burchellii parvispinum—had significantly larger optical lobes, which are parts of the brain used in sight. A diagram comparing the optical lobes in the brains of above and below-ground genera of army ants. The data suggest the underground is a simpler world, one that is less cognitively challenging, overall, than the above-ground world.

<hr>

[Visit Link](http://phys.org/news/2016-03-army-ants-evolve-regain-sight.html){:target="_blank" rel="noopener"}


