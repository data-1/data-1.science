---
layout: post
title: "United Nations recognizes first-ever carbon-neutral soccer club"
date: 2018-08-01
categories:
author: "Luciana Pricop"
tags: []
---


The Forest Green Rovers, a Gloucestershire-based team in the English Football League that prides itself as “the world’s greenest football club,” has been recognized by the United Nations as carbon-neutral – a world first. The Climate Neutral Now initiative was developed in the wake of the Paris Agreement to encourage climate action around the world. Related: Adidas unveils a Manchester United jersey created with ocean plastic  Join Our Newsletter Receive the latest in global news and designs building a better future. SIGN UP I agree to receive emails from the site. I can withdraw my consent at anytime by unsubscribing.

<hr>

[Visit Link](https://inhabitat.com/united-nations-recognizes-first-ever-carbon-neutral-soccer-club){:target="_blank" rel="noopener"}


