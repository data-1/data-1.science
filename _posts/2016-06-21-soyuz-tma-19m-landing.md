---
layout: post
title: "Soyuz TMA-19M landing"
date: 2016-06-21
categories:
author: ""
tags: [Tim Peake,European Space Agency,Astronaut,Outer space,Astronautics,Life in space,Flight,Spacecraft,Space exploration,Spaceflight technology,Human spaceflight programs,Crewed spacecraft,Space vehicles,Aerospace,Space agencies,Space program of the United States,Space-based economy,Scientific exploration,Space research,Space industry,Human spaceflight,Space programs,Spaceflight,Space science,NASA,Astronauts,Space program of Russia]
---


ESA astronaut Tim Peake, NASA astronaut Tim Kopra and commander Yuri Malenchenko landed in the steppe of Kazakhstan on Saturday, 18 June in their Soyuz TMA-19M spacecraft. The trio spent 186 days on the International Space Station. The landing brings Tim Peake’s Principia mission to an end but the research continues. Tim is the eighth ESA astronaut to complete a long-duration mission in space. He is the third after Alexander Gerst and Andreas Mogensen to fly directly to ESA’s astronaut home base in Cologne, Germany, for medical checks and for researchers to collect more data on how Tim’s body and mind have adapted to living in space.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2016/06/Soyuz_TMA-19M_landing){:target="_blank" rel="noopener"}


