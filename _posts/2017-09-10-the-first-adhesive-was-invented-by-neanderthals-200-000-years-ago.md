---
layout: post
title: "The First Adhesive Was Invented by Neanderthals 200,000 Years Ago"
date: 2017-09-10
categories:
author: "Jen Viegas"
tags: [Early modern human,Neanderthal,Human,Homo]
---


(Image credit: Paul Kozowyk)  The second method demonstrated by the researchers, pit roll,” required placing hot embers directly on top of a birch bark roll placed over a pit, which produced tar. My personal favorite is the pit roll method, because it's simple, but still produced reasonable quantities of tar.”  Tar collected in a birch bark container from the pit roll experiment, a technique which uses glowing embers placed over a roll of bark in a small pit. (Image credit: Paul Kozowyk)  The earliest evidence so far for adhesive production by anatomically modern humans dates to around 70,000 years ago, according to the researchers. Neanderthals and anatomically modern humans diverged long before then. “Now, I think that anatomically modern humans are only a sub-group within the species Homo sapiens, and that we should recognize the diversity of forms within early Homo sapiens, some of which probably went extinct.”  Approximately 0.3 ounces of birch bark tar produced using the raised structure technique being prepared for analysis in the lab.

<hr>

[Visit Link](https://www.livescience.com/60302-first-adhesive-invented-by-neanderthals.html){:target="_blank" rel="noopener"}


