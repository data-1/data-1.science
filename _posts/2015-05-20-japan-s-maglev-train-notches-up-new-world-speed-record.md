---
layout: post
title: "Japan's maglev train notches up new world speed record"
date: 2015-05-20
categories:
author: ""    
tags: [Maglev,Train,High-speed rail,Rail transport,Transportation engineering,Transport,Sustainable transport,Land transport]
---


Japan's maglev train notches up new world speed record    by Staff Writers    Tokyo (AFP) Apr 21, 2015    Japan's state-of-the-art maglev train clocked a new world speed record Tuesday in a test run near Mount Fuji, smashing through the 600 kilometre (373 miles) per hour mark, as Tokyo races to sell the technology abroad. The faster the train runs, the more stable it becomes -- I think the quality of the train ride has improved, Yasukazu Endo, who heads the maglev test centre southwest of Tokyo, told reporters Tuesday. JR Central wants to have a train in service in 2027, plying the route between Tokyo and the central city of Nagoya, a distance of 286 kilometres. The service, which would run at a top speed of 500 kph, is expected to connect the two cities in only 40 minutes, less than half the present journey time in Japan's already speedy bullet trains. He is due in the United States this weekend, where he will be touting the technology for a high-speed rail link between New York and Washington.

<hr>

[Visit Link](http://www.terradaily.com/reports/Japans_maglev_train_notches_up_new_world_speed_record_999.html){:target="_blank" rel="noopener"}


