---
layout: post
title: "SpaceX launches AI robot, strong coffee for station crew"
date: 2018-06-30
categories:
author: "Marcia Dunn"
tags: [SpaceX Dragon,SpaceX,Falcon 9,International Space Station,NASA,Cape Canaveral Space Force Station,Outer space,Spaceflight technology,Human spaceflight,Aerospace,Spaceflight,Space industry,Space program of the United States,Space programs,Spacecraft,Space vehicles,Flight,Astronautics,Rocketry]
---


A SpaceX Falcon 9 rocket's exhaust plume is illuminated during a launch just before dawn Friday, June 29, 2018 at Launch Complex 40 at Cape Canaveral, Fla. (Malcolm Denemark/Orlando Sentinel via AP)  A SpaceX rocket that flew just two months ago with a NASA satellite roared back into action Friday, launching the first orbiting robot with artificial intelligence and other station supplies. The shipment—packed into a Dragon capsule that's also recycled—should reach the station Monday. (Red Huber/Orlando Sentinel via AP)  In this image made from video, the Falcon 9 rocket carrying the SpaceX CRS-15 Dragon launches from Cape Canaveral, Florida, to the international space station, Friday, June 29, 2018. The round, artificial intelligence robot is part of SpaceX's latest delivery to the International Space Station.

<hr>

[Visit Link](https://phys.org/news/2018-06-spacex-ai-robot-strong-coffee.html){:target="_blank" rel="noopener"}


