---
layout: post
title: "NASA's MMS spacecraft achieve tightest flying formation ever"
date: 2015-10-20
categories:
author: Karen C. Fox
tags: [Magnetospheric Multiscale Mission,Sky,Space science,Outer space,Astronomy,Spaceflight,Bodies of the Solar System,Science,Spacecraft,Solar System,Flight,Astronautics]
---


On Oct. 15, 2015, the four spacecraft of NASA's Magnetospheric Multiscale, or MMS, mission entered the tightest flying formation ever achieved (shown here in an artist concept) – a pyramid shape, with each satellite just six miles from each other. Credit: NASA's Goddard Space Flight Center  On Oct. 15, 2015, a NASA mission broke its own record: the four satellites of its Magnetospheric Multiscale mission are now flying at their smallest separation, the tightest multi-spacecraft formation ever flown in orbit. The four spacecraft are just six miles apart, flying in what's called a tetrahedral formation, with each spacecraft at the tip of a four-sided pyramid. MMS gathers data to study a phenomenon called magnetic reconnection, which occurs when the magnetic field surrounding Earth connects and disconnects from the magnetic field carried by the solar wind, reconfiguring the very shape of Earth's magnetic environment. The goal of the STP Program is to understand the fundamental physical processes of the space environment from the sun to Earth, other planets, and the extremes of the solar system boundary.

<hr>

[Visit Link](http://phys.org/news/2015-10-nasa-mms-spacecraft-tightest-formation.html){:target="_blank" rel="noopener"}


