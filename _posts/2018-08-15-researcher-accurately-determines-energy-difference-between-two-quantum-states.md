---
layout: post
title: "Researcher accurately determines energy difference between two quantum states"
date: 2018-08-15
categories:
author: "Anne Beston, University Of Auckland"
tags: [Atom,Particle physics,Universe,Physics,Matter,Quantum mechanics,Standard Model,Dodd-Walls Centre,Helium,Nature,Theoretical physics,Physical sciences,Science]
---


Our understanding of the universe and the forces that govern it relies on the Standard Model of particle physics. This is done in an experiment that could fit on a table top with ultra-cold gas using an ultra-stable laser, accurate to a million times a million or, if you were using this level of measurement to measure the distance from Earth to the moon, it would be accurate to within a fraction of a millimetre. The fact the transition occurred is rare, and a milestone for quantum physics research. It advances our knowledge of the way atoms are put together and hence contributes to our understanding of space-time, Dr. Hoogerland says  This new result is a great test for our understanding of the Model and also allows us to determine the size of the helium nucleus and of the helium atom. The Large Hadron Collider is the largest machine ever built and a major international project involving hundreds of scientists looking for effects that cannot be explained by the Standard Model directly and for new particles at very high energy that do not fit the model.

<hr>

[Visit Link](https://phys.org/news/2018-08-accurately-energy-difference-quantum-states.html){:target="_blank" rel="noopener"}


