---
layout: post
title: "Bacteria self-organize to build working sensors"
date: 2017-10-09
categories:
author: "Duke University"
tags: [Bacteria,Biology,Cell (biology),Nanoparticle,Protein,Nature,Physical sciences,Technology,Chemistry]
---


When supplied with gold nanoparticles by researchers, the system forms a golden shell around the bacterial colony, the size and shape of which can be controlled by altering the growth environment. The result is a device that can be used as a pressure sensor, proving that the process can create working devices. Nature is full of examples of life combining organic and inorganic compounds to make better materials. To show how their system could be used to manufacture working devices, the researchers used these hybrid organic/inorganic structures as pressure sensors. Bacteria can create complex branching patterns, we just don't know how to make them do that ourselves -- yet.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/du-bst100517.php){:target="_blank" rel="noopener"}


