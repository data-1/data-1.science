---
layout: post
title: "Spelling mutations and evolutionary advantages"
date: 2016-03-23
categories:
author: National Centre for Biological Sciences
tags: [Genetic code,Mutation,Gene,Evolution,Synonymous substitution,Bacteria,DNA,Organism,Protein,Natural selection,Molecular biology,Chemistry,Biology,Genetics,Biological evolution,Molecular genetics,Nature,Evolutionary biology,Branches of genetics,Biochemistry,Life sciences,Biotechnology]
---


Similarly, DNA codes carrying instructions for creating a protein can sometimes be 'spelt' differently, although they specify the exact same sequence information to create that protein. Deepa Agashe at NCBS and her team of collaborators have reinforced a growing body of evidence that synonymous variants of a gene affect an organism's fitness. When grown in conditions where methanol was provided as the sole carbon source, all bacterial populations with the 'synonymous' fae gene variants performed poorly when compared to bacteria carrying the normal gene. Many of these mutations were again synonymous. Until now, synonymous mutations and gene variants were considered relatively unimportant for such studies on adaptation, due to a lack of information about their effects on organism fitness.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/ncfb-sma031816.php){:target="_blank" rel="noopener"}


