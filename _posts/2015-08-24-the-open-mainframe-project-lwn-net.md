---
layout: post
title: "The Open Mainframe Project [LWN.net]"
date: 2015-08-24
categories:
author: Posted August, Corbet
tags: [Finnish families of Swedish ancestry,Operating system families,Software,Linux organizations,Open-source movement,Software development,Kernel programmers,System software,Free software websites,Linux websites,Free content,Free software programmers,Computer architecture,Software engineering,Computers,Computer science,Free software people,Unix people,Computing,Technology,Linux,Finnish computer programmers,Linux kernel programmers,Torvalds family,Finnish computer scientists,Unix variants,Linus Torvalds,Linux people,Linux Foundation,Free software,Free system software,Unix]
---


[Announcements] Posted Aug 17, 2015 23:31 UTC (Mon) by corbet  The Linux Foundation has announced the launch of the Open Mainframe Project. In just the last few years, demand for mainframe capabilities have drastically increased due to Big Data, mobile processing, cloud computing and virtualization. Linux excels in all these areas, often being recognized as the operating system of the cloud and for advancing the most complex technologies across data, mobile and virtualized environments. Linux on the mainframe today has reached a critical mass such that vendors, users and academia need a neutral forum to work together to advance Linux tools and technologies and increase enterprise innovation. Comments (15 posted)

<hr>

[Visit Link](http://lwn.net/Articles/654775/rss){:target="_blank" rel="noopener"}


