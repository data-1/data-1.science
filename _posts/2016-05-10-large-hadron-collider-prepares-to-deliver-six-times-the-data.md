---
layout: post
title: "Large hadron collider prepares to deliver six times the data"
date: 2016-05-10
categories:
author: Karen Mcnulty Walsh, Brookhaven National Laboratory
tags: [Large Hadron Collider,Standard Model,Particle physics,Collider,Matter,Higgs boson,ATLAS experiment,Universe,Physics beyond the Standard Model,Physics,Nature,Physical sciences,Science]
---


The LHC will run around the clock for the next six months and produce roughly 2 quadrillion high-quality proton collisions, six times more than in 2015 and just shy of the total number of collisions recorded during the nearly three years of the collider's first run. The four particle detectors located on the LHC's ring allow scientists to record and study the properties of these building blocks and look for new fundamental particles and forces. Almost everything we know about matter is summed up in the Standard Model of Particle Physics, an elegant map of the subatomic world. During the first run of the LHC, scientists on the ATLAS and CMS experiments discovered the Higgs boson, the cornerstone of the Standard Model that helps explain the origins of mass. So far the Standard Model seems to explain matter, but we know there has to be something beyond the Standard Model, said Denise Caldwell, director of the Physics Division of the National Science Foundation.

<hr>

[Visit Link](http://phys.org/news/2016-05-large-hadron-collider.html){:target="_blank" rel="noopener"}


