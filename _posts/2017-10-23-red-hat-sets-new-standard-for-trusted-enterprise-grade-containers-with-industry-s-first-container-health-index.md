---
layout: post
title: "Red Hat Sets New Standard for Trusted, Enterprise-Grade Containers with Industry’s First Container Health Index"
date: 2017-10-23
categories:
author:  
tags: [Red Hat,OpenShift,Linux,Open source,Forward-looking statement,Couchbase Server,Information technology,Software,Technology,Computing]
---


BOSTON – RED HAT SUMMIT 2017 - May 2, 2017 —  Red Hat, Inc. (NYSE:RHT), the world’s leading provider of open source solutions, today introduced the industry’s first Container Health Index, setting a new standard for enterprise-grade Linux containers. Based upon Red Hat’s track record of delivering enterprise-grade open source technologies, including the world’s leading enterprise Linux platform, the Container Health Index provides the most comprehensive image detail of any enterprise container service. Healthy containers, backed by leading software security expertise  Building upon the extensive expertise of Red Hat’s Product Security team in investigating, tracking, and explaining security issues to customers, the Container Health Index expands this work to include the challenges of container maintenance. Red Hat Container Catalog  The Container Health Index is an integrated part of the Red Hat Container Catalog, a service for discovering, distributing and consuming commercially-curated Linux container images. To join the webcast or view the replay after the event, visit: https://vts.inxpo.com/Launch/Event.htm?ShowKey=39441  Supporting Quotes  Matthew Hicks, vice president, Engineering, OpenShift and Management, Red Hat  “While public registries and uncurated repositories are acceptable for some cloud-native development and proof-of-concept projects, they do not always provide content that is fit for production consumption; enterprise workloads require enterprise-ready tools.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/red-hat-sets-new-standard-trusted-enterprise-grade-containers-industry%E2%80%99s-first-container-health-index){:target="_blank" rel="noopener"}


