---
layout: post
title: "5 reasons the i3 window manager makes Linux better"
date: 2018-08-12
categories:
author: "Ricardo Gerardi
(Red Hat)"
tags: [Workspace,Desktop environment,Keyboard shortcut,Computer keyboard,Xfce,Red Hat,Tiling window manager,Software development,Humancomputer interaction,Computer architecture,Computers,User interfaces,Technology,System software,Computing,Computer science,Software engineering,Software,Graphical user interfaces]
---


I3 is a tiling window manager. Window managers are often used as part a full-featured desktop environment (such as GNOME or Xfce), but some can also be used as standalone applications. In i3, you can define shortcuts for everything. Here are some examples:  Open terminal  Open browser  Change layouts  Resize windows  Control music player  Switch workspaces  Now that I am used to this workflow, I can't see myself going back to a regular desktop environment. If you get into the habit of always assigning applications/groups of windows to the same workspace, you can quickly switch between them, which makes workspaces a very useful feature.

<hr>

[Visit Link](https://opensource.com/article/18/8/i3-tiling-window-manager){:target="_blank" rel="noopener"}


