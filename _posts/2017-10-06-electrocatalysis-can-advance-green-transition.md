---
layout: post
title: "Electrocatalysis can advance green transition"
date: 2017-10-06
categories:
author: "Technical University Of Denmark"
tags: [Catalysis,Electrocatalyst,Renewable energy,Solar energy,Fuel,Hydrogen,Fertilizer,Carbon dioxide,World energy supply and consumption,Water,Chemical substances,Physical sciences,Energy,Nature,Chemistry]
---


Demand is not likely to fall, so we need to find a way to make the fuels and chemicals that are an integrated part of our everyday lives in a sustainable and fossil-free way. Electrocatalysis could be the way forward  One way to escape dependence on oil is through electrocatalysis, which can transform molecules in our atmosphere (such as water, CO2, and nitrogen) into more expensive and useful products, such as hydrogen or methanol (fuel), and chemicals—like ammonia (used in fertilizers). To get these sources to play a greater role, we will take electricity from renewable sources and basic molecules that exist in the atmosphere—such as water, CO2 or nitrogen. As a third example, researchers are looking for a good catalyst that can make ammonia, used in artificial fertilizers. Platinum is currently the best catalyst for the process.

<hr>

[Visit Link](https://phys.org/news/2017-01-electrocatalysis-advance-green-transition.html){:target="_blank" rel="noopener"}


