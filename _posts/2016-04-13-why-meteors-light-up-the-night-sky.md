---
layout: post
title: "Why meteors light up the night sky"
date: 2016-04-13
categories:
author: Jonti Horner, Donna Burton And Tanya Hill, The Conversation
tags: [Meteoroid,Meteor shower,Comet,Meteorite,Leonids,Outer space,Physical sciences,Local Interstellar Cloud,Meteoroids,Nature,Astronomical objects,Solar System,Space science,Astronomy,Planetary science,Bodies of the Solar System]
---


Meteorite: fall vs find  When scientists study newly recovered meteorites, they break them up into two types – falls and finds. Because the debris is moving in the same direction as it hits the Earth, the meteors in a given shower will appear to radiate from a small area on the night sky, known as the radiant. Showers and storms, young and old  With each swing around the sun, the parent of a meteor shower adds more material to its debris stream, which continues to spread and disperse to space. The debris of that disintegration continued to orbit the sun and provided a spectacular epitaph for the comet with the Andromedid meteor storms. Credit: E. Weiß/Wikimedia  Others streams rotate in, birthing new showers, and fresh streams are born as comets are flung onto new orbits.

<hr>

[Visit Link](http://phys.org/news342690788.html){:target="_blank" rel="noopener"}


