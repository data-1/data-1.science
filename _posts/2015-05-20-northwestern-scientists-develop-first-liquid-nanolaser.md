---
layout: post
title: "Northwestern scientists develop first liquid nanolaser"
date: 2015-05-20
categories:
author: ""    
tags: [Laser,Nanolaser,Materials science,Technology,Electromagnetic radiation,Optics,Chemistry,Electromagnetism,Physical chemistry,Applied and interdisciplinary physics,Atomic molecular and optical physics,Materials,Physical sciences]
---


The liquid nanolaser in this study is not a laser pointer but a laser device on a chip, Odom explained. The laser's color can be changed in real time when the liquid dye in the microfluidic channel above the laser's cavity is changed. Odom's research team has found a way to integrate liquid gain materials with gold nanoparticle arrays to achieve nanoscale plasmon lasing that can be tuned dynamical, reversibly and in real time. Thus, the dielectric environment around the nanoparticle arrays can be tuned, which also tunes the lasing wavelength. Thus, the same fixed nanocavity structure (the same gold nanoparticle array) can exhibit lasing wavelengths that can be tuned over 50 nanometers, from 860 to 910 nanometers, simply by changing the solvent the dye is dissolved in.

<hr>

[Visit Link](http://www.spacedaily.com/reports/Northwestern_scientists_develop_first_liquid_nanolaser_999.html){:target="_blank" rel="noopener"}


