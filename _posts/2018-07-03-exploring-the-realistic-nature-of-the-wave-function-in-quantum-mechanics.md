---
layout: post
title: "Exploring the realistic nature of the wave function in quantum mechanics"
date: 2018-07-03
categories:
author: "Science China Press"
tags: [MachZehnder interferometer,Quantum mechanics,Waveparticle duality,Wheelers delayed-choice experiment,Wave function,Wave interference,Photon,Applied and interdisciplinary physics,Physical sciences,Scientific theories,Science,Theoretical physics,Physics]
---


The REIN states that the wave function of a quantum object is an actual state rather than a mere mathematical description—in other words, the quantum object in space exists in the form of the wave function. The single photon exists in the form of the two resultant sub-waves. In the experiment, the second BS is inserted or not when the two sub-waves traveling simultaneously along the two arms of the MZI have an encounter, as shown in Fig. In the EDC case, the parts, subject to the second BS, of the two-sub waves, will interfere and their forms change according to the relative phase. 1(b), the sub-waves having left from the MZI can be divided into two parts, one from the wave nature and the other from the particle nature.

<hr>

[Visit Link](https://phys.org/news/2018-01-exploring-realistic-nature-function-quantum.html){:target="_blank" rel="noopener"}


