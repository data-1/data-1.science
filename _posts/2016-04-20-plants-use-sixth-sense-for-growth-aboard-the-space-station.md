---
layout: post
title: "Plants use sixth sense for growth aboard the space station"
date: 2016-04-20
categories:
author: Laura Niles
tags: [Arabidopsis thaliana,Plant,Sense,JAXA,NASA,Micro-g environment,Gravity]
---


Researchers with the Japan Aerospace Exploration Agency will conduct a second run of the Plant Gravity Sensing study after new supplies are delivered by the sixth SpaceX commercial resupply mission to theInternational Space Station. The research team seeks to determine how plants sense their growth direction without gravity. The investigation examines the cellular process of formation in thale cress, or Arabidopsis thaliana, a small flowering plant related to cabbage. The research team does this to determine if the plants sense changes in gravitational acceleration and adapt the levels of calcium in their cells. We may design plants that respond to gravity vector changes more efficiently than wild ones, said Tatsumi.

<hr>

[Visit Link](http://phys.org/news347518745.html){:target="_blank" rel="noopener"}


