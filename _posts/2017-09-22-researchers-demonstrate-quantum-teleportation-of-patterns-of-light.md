---
layout: post
title: "Researchers demonstrate quantum teleportation of patterns of light"
date: 2017-09-22
categories:
author: "University of the Witwatersrand"
tags: [Quantum entanglement,Quantum teleportation,Photon,Quantum network,Physics,Physical sciences,Scientific theories,Applied and interdisciplinary physics,Science,Theoretical physics,Quantum mechanics]
---


Quantum communication over long distances is integral to information security and has been demonstrated in free space and fibre with two-dimensional states, recently over distances exceeding 1200 km between satellites. But using only two states reduces the information capacity of the photons, so the link is secure but slow. An integral part of a quantum repeater is the ability to entangle two photons that have never interacted - a process referred to as entanglement swapping. They showed that quantum correlations could be established between previously independent photons, and that this could be used to send information across a virtual link. To overcome this, entanglement swapping may be used to generate remote quantum correlations between particles that have not interacted, the core ingredient of a quantum repeater, akin to repeaters in optical fibre networks.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-09/uotw-rdq092117.php){:target="_blank" rel="noopener"}


