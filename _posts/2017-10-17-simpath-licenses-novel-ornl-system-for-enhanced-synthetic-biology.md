---
layout: post
title: "SimPath licenses novel ORNL system for enhanced synthetic biology"
date: 2017-10-17
categories:
author: DOE/Oak Ridge National Laboratory
tags: [Oak Ridge National Laboratory,Office of Science,Synthetic biology,Biology,Cloning,Science,Technology,Biobased economy,Gene,Genetics,Life sciences,Biotechnology]
---


OAK RIDGE, Tenn., Oct. 16, 2017--SimPath has licensed a novel cloning system developed by the Department of Energy's Oak Ridge National Laboratory that generates and assembles the biological building blocks necessary to synthetically bioengineer new medicines and fuels. By providing them flexible DNA assembly tools, we can further enable the plant synthetic biology community to introduce new products to the market faster. ###  Co-inventors of the cloning system include ORNL's Xiaohan Yang, a lead researcher on the DOE CAM biodesign project; Henrique C. De Paoli who is a senior research associate at the DOE Joint Genome Institute at Lawrence Berkeley National Laboratory; and ORNL's Jerry Tuskan, chief executive officer of the new Center for Bioenergy Design. The research was funded by DOE's Office of Science (Biological and Environmental Research, Genomic Science Program) and the technology matured under ORNL's Technology Innovation Program. CBI is supported by DOE's Office of Science Biological and Environmental Research.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/drnl-sln101617.php){:target="_blank" rel="noopener"}


