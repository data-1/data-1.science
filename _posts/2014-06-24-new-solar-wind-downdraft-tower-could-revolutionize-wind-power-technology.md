---
layout: post
title: "New Solar Wind Downdraft Tower Could Revolutionize Wind Power Technology"
date: 2014-06-24
categories:
author: Colin Payne
tags: []
---


The Solar Wind Downdraft Tower, created by Maryland-based Solar Wind Energy Inc. turns the traditional wind turbine design on its head by putting turbines at the base of a tubular tower that generates its own wind throughout the year. How does it work? Continue reading below Our Featured Videos  A tower at the center of the system creates a downdraft by using a series of pumps to carry water to the top of the 2,250-foot structure. Related: CSIRO Sets World Record with Solar-Generated Supercritical Steam  Solar Wind Energy claims this tower can generate electricity 24 hours per day throughout the year when it’s located in a hot, dry area – through production would be lower during the winter months. And depending on where it’s located and the weather conditions there, the towers could generate additional energy via vertical “wind vanes” that would capture prevailing winds and direct them into the tower.

<hr>

[Visit Link](http://inhabitat.com/new-solar-wind-downdraft-tower-could-revolutionize-wind-power-technology/){:target="_blank" rel="noopener"}


