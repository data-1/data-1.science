---
layout: post
title: "Stephen Hawking on What Makes a Good Theory and the Quest for a Theory of Everything"
date: 2018-08-22
categories:
author: "Maria Popova"
tags: [Theory,A Brief History of Time,Stephen Hawking,Science,Karl Popper,Physics,Physical sciences]
---


He transmuted all the remaining equations into a scintillating scientific narrative and completed the book just before he rallied Cambridge into a celebration of the 300th anniversary of Newton’s Principia — perhaps the most paradigm-shifting book in the history of science, introducing the theory of gravitation to the world. On the other hand, you can disprove a theory by finding even a single observation that disagrees with the predictions of the theory. Some theories are good boats. Of course, some new thought or discovery might come along and threaten to sink the boat. In the penultimate chapter of A Brief History of Time, Hawking considers this unholy grail of physics against the backdrop of the history of science:  The prospects for finding such a theory seem to be much better now because we know so much more about the universe.

<hr>

[Visit Link](https://www.brainpickings.org/2018/08/20/stephen-hawking-a-brief-history-of-time-theory/){:target="_blank" rel="noopener"}


