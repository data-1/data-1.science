---
layout: post
title: "Aspen Stands Tall As Third US City Achieves 100% Renewable Electricity"
date: 2015-09-15
categories:
author: Glenn Meyers, Michael Barnard, Jennifer Sensiba, Carolyn Fortuna, U.S. Energy Information Administration, Written By
tags: [Renewable energy,Clean technology,Energy and the environment,Environment,Climate change mitigation,Economy and the environment,Sustainable technologies,Natural resources,Energy,Nature,Natural environment,Sustainable development,Sustainable energy,Renewable resources,Physical quantities,Economy]
---


Aspen, the Colorado skiing-Mecca, now stands tall as a renewable energy visionary, having become one of three US cities to run on 100% renewable electricity. This news was recently reported by The Aspen Times, citing staff members at Aspen’s environmental and project departments. The plan for shifting to renewable energy and meeting the challenges of climate change dates back to 2005. According to Chris Menges and Will Dolan, from Aspen’s Sustainability and Utility departments:  “In 2005, the City created the Canary Initiative, which identifies Aspen and other mountain communities as “canaries in the coal mine” with respect to their sensitivity to the effects of climate change. Aspen relies on a stable climate and thriving natural environment for its economic viability and quality of life.

<hr>

[Visit Link](http://cleantechnica.com/2015/09/15/aspen-stands-tall-third-us-city-achieves-100-renewable-energy/){:target="_blank" rel="noopener"}


