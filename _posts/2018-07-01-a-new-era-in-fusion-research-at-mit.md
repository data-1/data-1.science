---
layout: post
title: "A new era in fusion research at MIT"
date: 2018-07-01
categories:
author: "Francesca Mccaffrey"
tags: [Fusion power,Nuclear fusion,Eni,Chemistry,Sustainable technologies,Physical quantities,Technology,Energy technology,Nature,Energy]
---


This week the Italian energy company Eni, a founding member of the MIT Energy Initiative (MITEI), announced it has reached an agreement with MIT to fund fusion research projects run out of the MIT Plasma Science and Fusion Center (PSFC)’s newly created Laboratory for Innovation in Fusion Technologies (LIFT). Eni will fund research projects at LIFT that are a continuation of this research and focus on fusion-specific solutions. One of the key elements of the fusion pilot plant currently being studied at LIFT is the liquid immersion blanket, essentially a flowing pool of molten salt that completely surrounds the fusion energy core. A history of innovation  During the 23 years MIT’s Alcator C-Mod tokamak fusion experiment was in operation, it repeatedly advanced records for plasma pressure in a magnetic confinement device. Just last year, they used C-Mod data to create a new method of heating fusion plasmas in tokamaks which could result in the heating of ions to energies an order of magnitude greater than previously reached.

<hr>

[Visit Link](http://news.mit.edu/2018/new-era-fusion-research-mit-eni-0309){:target="_blank" rel="noopener"}


