---
layout: post
title: "Alphabet's DeepMind forms ethics unit for artificial intelligence"
date: 2017-10-04
categories:
author: ""
tags: [DeepMind,Artificial intelligence,Technology,Computing,Branches of science,Information technology,Cyberspace]
---


Deepmind is setting up an ethic unit to allay growing fears that artificial intelligence could slip out of human control  DeepMind, the Google sibling focusing on artificial intelligence, has announced the launch of an ethics and society unit to study the impact of new technologies on society. Understanding what this means in practice requires rigorous scientific inquiry into the most sensitive challenges we face. The post said the focus would be on ensuring truly beneficial and responsible uses for artificial intelligence. Google and DeepMind are members of an industry-founded Partnership of AI to Benefit People and Society which includes Facebook, Amazon, Microsoft and other tech firms. DeepMind, acquired by Google in 2014, gained notoriety for becoming the first machine to beat a grandmaster in the Asian board game Go last year.

<hr>

[Visit Link](https://phys.org/news/2017-10-alphabet-deepmind-ethics-artificial-intelligence.html){:target="_blank" rel="noopener"}


