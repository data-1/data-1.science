---
layout: post
title: "Quantum physicists achieve entanglement record"
date: 2018-04-19
categories:
author: "University Of Innsbruck"
tags: [Quantum entanglement,Quantum mechanics,Rainer Blatt,Applied and interdisciplinary physics,Branches of science,Theoretical physics,Applied mathematics,Physics,Science,Physical sciences]
---


Some of the new quantum technologies ranging from extremely precise sensors to universal quantum computers require a large number of quantum bits in order to exploit the advantages of quantum physics. Now, a research team led by Ben Lanyon and Rainer Blatt at the Institute of Quantum Optics and Quantum Information (IQOQI) of the Austrian Academy of Sciences, together with theorists from the University of Ulm and the Institute of Quantum Optics and Quantum Information in Vienna, has achieved controlled multi-particle entanglement in a system of 20 quantum bits. The researchers were able to detect genuine multi-particle entanglement between all neighbouring groups of three, four and five quantum bits. Genuine multi-particle entanglement  Physically, entangled particles cannot be described as individual particles with defined states, but only as a complete system. At the Institute of Quantum Optics and Quantum Information in Innsbruck, the team of physicists used laser light to entangle 20 calcium atoms in an ion trap experiment and observed the dynamic propagation of multi-particle entanglement in this system.

<hr>

[Visit Link](https://phys.org/news/2018-04-quantum-physicists-entanglement.html){:target="_blank" rel="noopener"}


