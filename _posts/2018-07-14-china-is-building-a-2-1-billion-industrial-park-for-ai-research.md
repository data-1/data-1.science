---
layout: post
title: "China Is Building a $2.1 Billion Industrial Park for AI Research"
date: 2018-07-14
categories:
author: ""
tags: [Artificial intelligence,China,Technology,Branches of science]
---


Building An AI Industry  Over the past year, more nations have come to realize the importance of artificial intelligence (AI) in shaping the economics of the future. Click to View Full Infographic  The government is investing some $2.12 billion (13.8 billion yuan) to build the industrial park, located in west Beijing, according to state press agency Xinhua and as first reported by Reuters. This national AI research center of sorts is also expected to produce $7.7 billion (50 billion yuan) a year from the 400 enterprises that would be housed in the AI research park. Previous reports have shown that China has purportedly been investing time and effort in AI research, even more than the United States. With their clearly laid-out program, China is expected to soon surpass the U.S. in AI development.

<hr>

[Visit Link](https://futurism.com/china-building-2-1-billion-industrial-park-ai-research/){:target="_blank" rel="noopener"}


