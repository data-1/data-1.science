---
layout: post
title: "Animals built reefs 550 million years ago, fossil study finds"
date: 2014-06-29
categories:
author: University of Edinburgh 
tags: [Cloudinidae,Fossil,Reef,Animal,Physical geography,Earth sciences,Nature,Natural environment,Oceanography,Systems ecology,Environmental science,Ecology]
---


It is a remarkable survivor of an ancient aquatic world – now a new study sheds light on how one of Earth's oldest reefs was formed. Scientists say it was at this point that tiny aquatic creatures developed the ability to construct hard protective coats and build reefs to shelter and protect them in an increasingly dangerous world. Findings from the study – led by scientists at the University of Edinburgh – support previous research which suggested that environmental pressures caused species to develop new features and behaviours in order to survive. Researchers say animals may have developed the ability to build reefs to protect themselves against increased threats from predators. Scientists say the development of hard biological structures – through a process called biomineralisation – sparked a dramatic increase in the biodiversity of marine ecosystems.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/uoe-abr062414.php){:target="_blank" rel="noopener"}


