---
layout: post
title: "CRISPR Can Now Be Used to Edit Individual Bases"
date: 2017-10-27
categories:
author:  
tags: [CRISPR gene editing,Molecular genetics,Branches of genetics,Biology,Life sciences,Biochemistry,Molecular biology,Biotechnology,Genetics,Nucleic acids,Chemistry]
---


Crisper Than CRISPR  CRISPR-Cas9 has been widely heralded as one of the most important scientific developments of recent years, thanks to its capacity to make edits to the human genome. Now, a more precise version of the tool has been developed by a team of scientists from the Massachusetts Institute of Technology and Harvard. Sometimes, only one base pair in a length of DNA is abnormal in some way – this is called a point mutation, and it accounts for 32,000 of the 50,000 changes in the human genome that have been associated with diseases. A study published in Nature looked at changing an A base into a G; w process that could address around half of those mutations. DNA & RNA  While one study was looking into how base editing could be used to modify DNA, another group of researchers have been looking into how it could be used to tweak RNA.

<hr>

[Visit Link](https://futurism.com/crispr-can-now-be-used-to-edit-individual-bases/){:target="_blank" rel="noopener"}


