---
layout: post
title: "Liftoff – Alexander Gerst returns to space"
date: 2018-08-16
categories:
author: ""
tags: [Spacecraft,International Space Station,European Space Agency,Astronaut,Space vehicles,Rocketry,Astronomy,Space exploration,Spaceflight technology,Life in space,Space science,Aerospace,Space programs,Human spaceflight,Flight,Outer space,Astronautics,Spaceflight]
---


Science & Exploration Liftoff – Alexander Gerst returns to space 06/06/2018 10183 views 92 likes  Today at 11:12 GMT (13:12 CEST) ESA astronaut Alexander Gerst was launched into space alongside NASA astronaut Serena Auñón-Chancellor and Roscosmos commander Sergei Prokopyev in the Soyuz MS-09 spacecraft from Baikonur cosmodrome in Kazakhstan. The German astronaut is a returning visitor to the International Space Station, the first of ESA’s 2009 class of astronauts to be sent into space for a second time. During the second part of his mission Alexander will take over as commander of the International Space Station, only the second time an ESA astronaut will take on this role so far. While in space, Alexander will work on over 50 European experiments. The permanent weightless laboratory allows for long-term studies with humans in microgravity and ESA’s Columbus research module is celebrating its 10th anniversary this year.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/International_Space_Station/Liftoff_Alexander_Gerst_returns_to_space_on_Horizons_mission){:target="_blank" rel="noopener"}


