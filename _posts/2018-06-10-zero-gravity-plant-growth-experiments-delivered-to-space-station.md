---
layout: post
title: "Zero gravity plant growth experiments delivered to space station"
date: 2018-06-10
categories:
author: "University of Wisconsin-Madison"
tags: [Plant,Plants in space,Life in space,Spacecraft,Human spaceflight,Flight,Astronautics,Outer space,Spaceflight]
---


His lab will grow identical seedlings on Earth during the month-long experiment to compare their growth with and without gravity. In another first for Gilroy's team, astronauts will take images of living plants as they grow using microscopes on the space station. In their current experiment, the researchers are growing mutant Arabidopsis that responds strongly to low-oxygen stress, at least on Earth. Then, several days before the scheduled launch date, the researchers flew down with enough seed to handle six delayed launches. The issue is how well the plants grow.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/uow-zgp121817.php){:target="_blank" rel="noopener"}


