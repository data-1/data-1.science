---
layout: post
title: "Neutron stars cast light on quark matter"
date: 2018-07-03
categories:
author: "Ana Lopes"
tags: [Matter,QCD matter,Neutron star,Quark,Subatomic particle,Neutron,Hadron,Virgo interferometer,Nature,Theoretical physics,Nuclear physics,Science,Physical sciences,Particle physics,Physics]
---


Credit: University of Warwick/Mark Garlick  Quark matter – an extremely dense phase of matter made up of subatomic particles called quarks – may exist at the heart of neutron stars. Kurkela and colleagues used a neutron-star property deduced from the first observation by the LIGO and Virgo scientific collaborations of gravitational waves – ripples in the fabric of spacetime – emitted by the merger of two neutron stars. To describe the collective behaviour of quark matter, physicists generally employ equations of state, which relate the pressure of a state of matter to other state properties. By plugging tidal-deformability values of the neutron stars observed by LIGO and Virgo into a derivation of a family of equations of state for neutron-star quark matter, Kurkela and colleagues were able to dramatically reduce the size of that equation family. Such a reduced family provides more stringent limits on the collective properties of quark matter, and more generally on nuclear matter at high densities, than were previously available.

<hr>

[Visit Link](https://phys.org/news/2018-06-neutron-stars-quark.html){:target="_blank" rel="noopener"}


