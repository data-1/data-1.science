---
layout: post
title: "In AI, radiology finds a new ally"
date: 2018-07-13
categories:
author: ""
tags: [Radiology,Artificial intelligence,Clinical medicine,Health,Health sciences,Medical specialties,Technology,Medicine,Branches of science]
---


It was trained on over 1.5 million X-rays to detect 15 chest abnormalities, ranging from tuberculosis to potentially cancerous lung nodules. Overcoming the ‘black box’  The results of the trial were very promising, says Shalini Govil, senior adviser and quality controller at Columbia Asia Radiology Group; the AI was calling the X-rays correctly around 90% of the time. In qXR’s case, these were chest X-rays and radiologist interpretations of them. When the network is exposed to millions of such X-rays and interpretations, it builds its own rules for translating the images into interpretations. But there will be substantial challenges in deploying qXR across rural India, given the lack of digitisation in hospitals.

<hr>

[Visit Link](https://www.thehindu.com/sci-tech/health/in-ai-radiology-finds-a-new-ally/article24362309.ece){:target="_blank" rel="noopener"}


