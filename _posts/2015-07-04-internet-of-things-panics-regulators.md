---
layout: post
title: "Internet of Things panics regulators"
date: 2015-07-04
categories:
author: Thursday, January
tags: [Internet of things,Technology,Computing,Information technology,Cyberspace]
---


UK and US regulators are becoming increasingly concerned about how to manage the Internet of Things  For those unfamiliar with the term, the Internet of Things (IoT) refers to the ability of everyday objects to connect to the internet, allowing them to send and receive data. Nowadays, we all possess an abundance of personal gadgets that are constantly connected to the net, with practically everyone carrying a smartphone around in their pocket. But the rise in other forms of technology that are constantly connected to the web is growing. This might be home automation systems that turn on your kettle when you wake up in the morning, to activity trackers that share with your friends how many miles you’ve run that day. Ofcom plans to work closely with the UK government, along with other regulators and the technology industry in order to strike a balance that will ensure that IoT products can be developed without barriers, but at the same time ensure that issues, such as data protection are protected.

<hr>

[Visit Link](http://www.theneweconomy.com/home/internet-of-things-panics-regulators){:target="_blank" rel="noopener"}


