---
layout: post
title: "Space-based tracker to give scientists a beyond-bird's-eye-view of wildlife"
date: 2018-08-15
categories:
author: "Yale University"
tags: [Biodiversity,ICARUS Initiative,American Association for the Advancement of Science,Conservation biology,Science]
---


Thanks to the recently founded Max Planck-Yale Center (MPYC) for Biodiversity Movement and Global Change, Yale and U.S.-based biodiversity researchers will be among the first to make use of the big data that this groundbreaking scientific instrument will be collecting by early 2019. For the past 16 years, ICARUS has been simultaneously developing the tiniest transmitters (by 2025, the team hopes to scale down solar-powered backpacks enough to fit them on desert locusts) and some of the most massive antennae (the equipment that the cosmonauts will be installing). The system represents a quantum leap for the study of animal movements and migration, and will enable real-time biodiversity monitoring at a global scale, said Walter Jetz, professor of ecology and evolutionary biology at Yale and co-director of the MPYC. Even with the limited tracking technology available, biodiversity researchers have already been able to predict volcanic eruptions by tracking the movements of goat herds and understand impacts of climate change by following migration changes in birds. (Fruit bats have antibodies against, but do not transmit, this deadly disease.)

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/yu-stt081418.php){:target="_blank" rel="noopener"}


