---
layout: post
title: "Bioengineers grow living bone for facial reconstruction"
date: 2017-03-17
categories:
author: "National Institute of Biomedical Imaging and Bioengineering"
tags: [Tissue engineering,Bone grafting,Bone,Dental implant,Gordana Vunjak-Novakovic,Medical specialties,Clinical medicine,Medicine,Health sciences,Medical treatments,Biotechnology,Life sciences,Biology]
---


The team led by researchers from Columbia University, New York, grafted customized implants into pig jaws that resulted in integration and function of the engineered graft into the recipient's own tissue. Replacement bones must be perfectly sculpted to satisfactorily match the features of the person's face. They then implanted the engineered bone grafts into the pigs and monitored the growth for the next six months. Six pigs received engineered implants, while six others received the cow bone without any stem cells. Several key features enhanced the clinical prospects of this research, according to Hunziker.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/08/160804172402.htm){:target="_blank" rel="noopener"}


