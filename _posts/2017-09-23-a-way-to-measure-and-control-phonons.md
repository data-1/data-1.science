---
layout: post
title: "A way to measure and control phonons"
date: 2017-09-23
categories:
author: "Bob Yirka"
tags: [Photon,Quantum mechanics,Light,Physics,Applied and interdisciplinary physics,Science,Theoretical physics,Physical sciences]
---


Those photons were then reflected back to a photon detector and were subsequently analyzed using Hanbury Brown and Twiss interferometry. The researchers used the state of the photons to determine the non-classical state of the phonons in the device. The team showed that individual phonons moving in a crystal follow the laws of quantum mechanics as opposed to classical physics. More information: Sungkun Hong et al. Hanbury Brown and Twiss interferometry of single phonons from an optomechanical resonator, Science (2017). Our result establishes purely optical quantum control of a mechanical oscillator at the single phonon level.

<hr>

[Visit Link](https://phys.org/news/2017-09-phonons.html){:target="_blank" rel="noopener"}


