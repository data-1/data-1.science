---
layout: post
title: "Genome's tale of 'conquer and enslave'"
date: 2016-04-14
categories:
author: University Of Toronto
tags: [Gene,Evolution,DNA,Selfish genetic element,Genome,Virus,Transcription (biology),Transcription factor,Mutation,Protein,Genetics,Biology,Life sciences,Biotechnology,Molecular biology,Branches of genetics,Biochemistry,Biological evolution,Evolutionary biology]
---


Credit: University of Toronto  Toronto scientists uncovered how viral remnants helped shape control of our genes. Genes are switched on or off by proteins called transcription factors. Almost half the human genome is made of selfish DNA, which probably came from ancient retro-viruses which, similar to modern counterparts, inserted their DNA into the host's genome. What I think was not appreciated until this study is that retro-elements are really a driving force in the evolution of transcription factors themselves. One C2H2-ZF family member, a transcription factor called ZNF189 evolved to silence an ancient retro-element, known as LINE L2, which is a staggering 100 million years old.

<hr>

[Visit Link](http://phys.org/news343660743.html){:target="_blank" rel="noopener"}


