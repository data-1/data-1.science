---
layout: post
title: "Oxygen-creating instrument selected to fly on the upcoming Mars 2020 mission"
date: 2015-10-06
categories:
author: Maia Weinstock, Massachusetts Institute Of Technology
tags: [Mars Oxygen ISRU Experiment,Mars 2020,Mars,Spaceflight,Science,Flight,Space science,Astronautics,Outer space]
---


Credit: NASA  Whenever the first NASA astronauts arrive on Mars, they will likely have MIT to thank for the oxygen they breathe—and for the oxygen needed to burn rocket fuel that will launch them back home to Earth. Human exploration of Mars will be a seminal event for the next generation, the same way the moon landing mission was for my generation, says Michael Hecht, principal investigator of the MOXIE instrument and assistant director for research management at the MIT Haystack Observatory. To that end, the MOXIE instrument will attempt to make oxygen out of native resources in order to demonstrate that it could be done on a larger scale for future missions. When will humans actually get to Mars? An independent mission known as Mars One aims to send humans on a one-way trip to the Red Planet in 2024—but critically, the explorers who have signed up for that mission know they won't be returning.

<hr>

[Visit Link](http://phys.org/news326099009.html){:target="_blank" rel="noopener"}


