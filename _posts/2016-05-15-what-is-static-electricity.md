---
layout: post
title: "What Is Static Electricity?"
date: 2016-05-15
categories:
author: Jim Lucas
tags: [Static electricity,Electrostatic discharge,Electric charge,Capacitor,Technology,Nature,Electromagnetism,Electrical engineering,Electricity]
---


According to the University of Hawaii, “When two objects are rubbed together to create static electricity, one object gives up electrons and becomes more positively charged while the other material collects electrons and becomes more negatively charged.” This is because one material has weakly bound electrons, and the other has many vacancies in its outer electron shells, so electrons can move from the former to the latter creating a charge imbalance after the materials are separated. If that fluid is flammable — such as gasoline — a spark from a sudden discharge could result in a fire or explosion. This dissipates the static charge continuously and keeps it from building up enough to create a spark. Applications of static electricity  While static electricity can be a nuisance or even a danger, as in the case of static cling or static shock, in other cases it can be quite useful. If the capacitor is charged and the voltage is switched off, it can retain the charge for some time.

<hr>

[Visit Link](http://www.livescience.com/51656-static-electricity.html){:target="_blank" rel="noopener"}


