---
layout: post
title: "New insights into the impacts of ocean acidification"
date: 2017-10-26
categories:
author: National Oceanography Centre
tags: [Ocean acidification,Sea,Ocean,Carbon dioxide,Exoskeleton,Hydrology,Physical sciences,Natural environment,Chemistry,Applied and interdisciplinary physics,Earth sciences,Oceanography,Physical geography,Environmental science,Hydrography,Nature]
---


This collaborative research, involving contributions from the National Oceanography Centre and led by the Institute of Geosciences at Kiel University, (CAU), found that almost a quarter of the deep sea, shell-forming species analyzed already live in seawater chemically unfavorable to the maintenance of their calcareous skeletons and shells. Increasing atmospheric CO2 is also increasing oceanic CO2. Chemical parameters change when carbon dioxide dissolves in sea water. Some of those deep-sea organisms already exist in undersaturated conditions, not favorable to the formation of shells. The scientists hope to gain more insight into this by exploring how past changes in seawater pH have impacted these organisms, but also through further field and laboratory studies testing the effect of ocean acidification on calcifiers.

<hr>

[Visit Link](http://phys.org/news/2016-09-insights-impacts-ocean-acidification.html){:target="_blank" rel="noopener"}


