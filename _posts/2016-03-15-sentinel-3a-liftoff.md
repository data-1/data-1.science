---
layout: post
title: "Sentinel-3A liftoff"
date: 2016-03-15
categories:
author:  
tags: [Sentinel-3,Outer space,Earth sciences,Spaceflight,Space science,Spacecraft,Satellites,Bodies of the Solar System,Physical geography]
---


Replay of the Sentinel-3A liftoff on a Rockot launcher from the Plesetsk Cosmodrome in northern Russia at 17:57 GMT (18:57 CET) on 16 February 2016. Sentinel-3A is the third satellite to be launched for Europe’s Copernicus environment monitoring programme. Designed as a two-satellite constellation – Sentinel-3A and -3B – the Sentinel-3 mission carries a series of cutting-edge instruments for systematic measurements of Earth’s oceans, land, ice and atmosphere. Over oceans, Sentinel-3 measures the temperature, colour and height of the sea surface as well as the thickness of sea ice. Watch the replay of the Sentinel-3A launch event

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2016/02/Sentinel-3A_liftoff){:target="_blank" rel="noopener"}


