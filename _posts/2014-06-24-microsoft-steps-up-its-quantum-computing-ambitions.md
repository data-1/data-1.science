---
layout: post
title: "Microsoft steps up its quantum computing ambitions"
date: 2014-06-24
categories:
author: Russell Brandom, Jun
tags: [The Verge,D-Wave Systems,Computer science,Information Age,Theoretical physics,Technology,Quantum mechanics,Computing]
---


Last week saw some bad news for Google's quantum computing lab, but this week Microsoft seems to be picking up the slack. The company first launched its quantum research wing back in 2006, but according to a new report in The New York Times, Microsoft has made some crucial hires in recent days, hiring Intel's Douglas Carmean to work on the growing quantum hardware design group. The efforts are still very preliminary, but the hope is that, by the time researchers have a working model for an anyon-based qubit, Microsoft will have a hardware design that can hold them. The core of the research is a qubit based on the experimental anyon particle, which engineers have been working on since 2006. But while the physics behind the move is still being worked out, Microsoft's ambitions are already coming into focus.

<hr>

[Visit Link](http://www.theverge.com/2014/6/23/5836208/microsoft-steps-up-its-quantum-computing-ambitions){:target="_blank" rel="noopener"}


