---
layout: post
title: "Virgin Hyperloop One's System Just Broke Elon Musk's Speed Record"
date: 2018-06-10
categories:
author: ""
tags: [Hyperloop,Virgin Hyperloop,Transport,Technology,Elon Musk,Vehicles]
---


Hyperloop Speed Record  On December 18, Virgin Hyperloop One announced the completion of third phase testing on the DevLoop, the world's first full-scale hyperloop test site. By combining magnetic levitation, extremely low aerodynamic drag, and the level of air pressure experienced at 200,000 feet above sea level, the system proved that it is capable of reaching airline speeds over long distances. The recent phase three testing continues to prove the incredible persistence and determination of our DevLoop team — the close to 200 engineers, machinists, welders, and fabricators who collaborated to make hyperloop a reality today, Josh Giegel, Virgin Hyperloop One’s co-founder and chief technology officer, stated in a press release announcing the new hyperloop speed record. The continued support from our existing investors Caspian Venture Capital and DP World highlight their adamant belief in our ability to execute,” said Giegel. “I am excited by the latest developments at Virgin Hyperloop One and delighted to be its new Chairman, said Branson in the press release.

<hr>

[Visit Link](https://futurism.com/virgin-hyperloop-ones-system-broke-elon-musks-speed-record/){:target="_blank" rel="noopener"}


