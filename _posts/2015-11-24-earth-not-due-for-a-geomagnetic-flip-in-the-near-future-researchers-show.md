---
layout: post
title: "Earth not due for a geomagnetic flip in the near future, researchers show"
date: 2015-11-24
categories:
author: Massachusetts Institute Of Technology
tags: [Earths magnetic field,Geomagnetic reversal,Earth,Paleomagnetism,Magnetism,Magnet,Physical sciences,Earth sciences,Electromagnetism,Physics,Space science,Applied and interdisciplinary physics,Nature]
---


This drop in intensity is associated with periodic geomagnetic field reversals, in which the Earth's North and South magnetic poles flip polarity, and it could last for several thousand years before returning to a stable, shielding intensity. But according to a new MIT study in the Proceedings of the National Academy of Sciences, the geomagnetic field is not in danger of flipping anytime soon: The researchers calculated Earth's average, stable field intensity over the last 5 million years, and found that today's intensity is about twice that of the historical average. Wang reasoned that knowing the paleomagnetic field intensity at the equator and the poles would therefore give an accurate estimate of the planet's average historical intensity. A rock's remanent magnetization is proportional to the magnetic field in which it cooled. From there, the field intensity may go up again.

<hr>

[Visit Link](http://phys.org/news/2015-11-earth-due-geomagnetic-flip-future.html){:target="_blank" rel="noopener"}


