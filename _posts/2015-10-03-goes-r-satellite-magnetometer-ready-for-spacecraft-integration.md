---
layout: post
title: "GOES-R satellite Magnetometer ready for spacecraft integration"
date: 2015-10-03
categories:
author: Nasa'S Goddard Space Flight Center
tags: [GOES-16,Space weather,Space science,Outer space,Spaceflight,Sky,Astronomy,Technology,Science,Spacecraft,Flight,Astronautics]
---


Credit: ATK/Goleta  The Magnetometer instrument that will fly on NOAA's GOES-R satellite when it is launched in early 2016 has completed the development and testing phase and is ready to be integrated with the spacecraft. This allows the sensor to be much more perceptive of the space magnetic environment, resulting in even better forecasting of space weather. The electronics units were installed on the spacecraft panels and the sensors and the boom will be integrated onto the satellite in the fall. The Magnetometer is the fifth of six total instruments to be completed for the GOES-R satellite. The advanced spacecraft and instrument technology on the GOES-R, or Geostationary Operational Environmental Satellite – R, series will result in more timely and accurate weather forecasts.

<hr>

[Visit Link](http://phys.org/news324693922.html){:target="_blank" rel="noopener"}


