---
layout: post
title: "Root microbiome engineering improves plant growth"
date: 2016-08-01
categories:
author: "Cell Press"
tags: [Microbiome,Plant,Selective breeding,Genetics,Root,Genetic engineering,Agriculture,Internet privacy,Biology]
---


Humans have been breeding crops until they're bigger and more nutritious since the early days of agriculture, but genetic manipulation isn't the only way to give plants a boost. In a review paper published on September 25 in Trends in Microbiology, two integrative biologists present how it is possible to engineer the plant soil microbiome to improve plant growth, even if the plants are genetically identical and cannot evolve. These artificially selected microbiomes, which can also be selected in animals, can then be passed on from parents to offspring. Only a few published studies have looked at the effects of artificially selecting microbiomes. Credit: Ulrich Mueller/UT Austin/Trends in Microbiology 2015  Explore further Unearthing cornerstones in root microbiomes  More information: Trends in Microbiology, Mueller and Sachs: Engineering Microbiomes to Improve Plant and Animal Health Trends in Microbiology, Mueller and Sachs: Engineering Microbiomes to Improve Plant and Animal Health dx.doi.org/10.1016/j.tim.2015.07.009

<hr>

[Visit Link](http://phys.org/news/2015-09-root-microbiome-growth.html){:target="_blank" rel="noopener"}


