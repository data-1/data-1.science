---
layout: post
title: "What are aerosols?"
date: 2016-04-05
categories:
author: Brookhaven National Laboratory
tags: [Greenhouse effect,Climate change,Greenhouse gas,Climate system,Atmosphere of Earth,Nature,Atmosphere,Applied and interdisciplinary physics,Climate variability and change,Physical geography,Natural environment,Physical sciences,Climate,Chemistry,Atmospheric sciences,Earth phenomena,Earth sciences]
---


Art Sedlacek, an atmospheric scientist at the U.S. Department of Energy's Brookhaven National Laboratory, has gone to extreme lengths to study aerosols -- tiny particles emitted from factories, forest fires, car exhaust, and sometimes from natural sources. Sedlacek's goal is to understand the impact aerosols have on Earth's climate system. From their research, atmospheric scientists have determined that the effects clouds and aerosols have on the climate system is offsetting warming from greenhouse gases -- which ultimately explains why scientists haven't seen as much warming as expected from the levels of greenhouse gases. advertisement  The challenges of studying aerosols  The biggest challenge scientists face when studying how aerosols impact climate is that this impact is such a small fraction of the overall energy Earth receives from the sun. The effect of greenhouse gases on Earth's climate is only around one percent of that amount, and the effect of aerosols, through scattering and absorbing the sun's radiant energy, is an even smaller fraction.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/04/160401145037.htm){:target="_blank" rel="noopener"}


