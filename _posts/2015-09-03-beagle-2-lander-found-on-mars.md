---
layout: post
title: "Beagle-2 lander found on Mars"
date: 2015-09-03
categories:
author: "$author"   
tags: [Beagle 2,Lander (spacecraft),Mars Express,Mars,ExoMars,Astronautics,Exploration of Mars,Spaceflight,Missions to the planets,Planets of the Solar System,Outer space,Astronomical objects known since antiquity,Space probes,Space science,Solar System,Space vehicles,Astronomy,Discovery and exploration of the Solar System,Space exploration,Spacecraft,Flight,Aerospace,Spaceflight technology,Space research,Space missions,Terrestrial planets,Uncrewed spacecraft]
---


Science & Exploration Beagle-2 lander found on Mars 16/01/2015 81377 views 278 likes  The UK-led Beagle-2 Mars lander, which hitched a ride on ESA’s Mars Express mission and was lost on Mars since 2003, has been found in images taken by a NASA orbiter at the Red Planet. Now, over a decade later, the lander has been identified in images taken by the high-resolution camera on NASA’s Mars Reconnaissance Orbiter. The lander is seen partially deployed on the surface, showing that the entry, descent and landing sequence worked and it did indeed successfully land on Mars on Christmas Day 2003. The images show the lander in what appears to be a partially deployed configuration, with only one, two or at most three of the four solar panels open, and with the main parachute and what is thought to be the rear cover with its pilot/drogue parachute still attached close by. The project was a partnership between the Open University, the University of Leicester and EADS Astrium UK (now Airbus Defence and Space).

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Mars_Express/Beagle-2_lander_found_on_Mars){:target="_blank" rel="noopener"}


