---
layout: post
title: "Schiaparelli readied for Mars landing"
date: 2017-09-22
categories:
author: ""
tags: [Schiaparelli EDM,Spaceflight technology,Spaceflight,Outer space,Astronautics,Flight,Spacecraft,Space exploration,Space vehicles,Space science,Discovery and exploration of the Solar System,Space probes,Astronomy,Solar System,Planets of the Solar System,Space program of the United States,Aerospace,Uncrewed spacecraft,Exploration of Mars,Mars,Space research,Astronomical objects known since antiquity,Missions to the planets,Space missions,Space programs]
---


This week’s uploading was conducted by the Orbiter team working at ESA’s mission control in Darmstadt, Germany, and marked a significant milestone in readiness for arrival. Schiaparelli’s operations are governed by time-tagged stored commands, ensuring that the lander can conduct its mission even when out of contact with any of the Mars orbiters that will serve as data relays. The second, containing the rest of the mission command sequence, was uploaded to the module on 7 October. Schiaparelli’s descent to Mars  The science activities are designed to make the most of the limited energy available from the batteries, so they will be performed in set windows rather than continuously – typically, for six hours each day. These relay slots include 32 by NASA craft: 18 by the Mars Reconnaissance Orbiter, eight by Odyssey and six by Maven.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Operations/Schiaparelli_readied_for_Mars_landing){:target="_blank" rel="noopener"}


