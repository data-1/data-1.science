---
layout: post
title: "Probing the Association between Early Evolutionary Markers and Schizophrenia"
date: 2017-03-22
categories:
author: "Saurabh Srinivasan, Norment, Kg Jebsen Centre For Psychosis Research, Institute Of Clinical Medicine, University Of Oslo, Oslo, Division Of Mental Health, Addiction, Oslo University Hospital, Francesco Bettella"
tags: []
---


Polygenic enrichment plots suggest a higher prevalence of schizophrenia associations in human accelerated regions, segmental duplications and ohnologs. While SNPs associated with schizophrenia are enriched in HAR, Ohno and SD regions, the enrichment seems to be mediated by affiliation to known genomic enrichment categories. The human SD score indicates a SNP’s affiliation to SD regions in humans. Discussion We used polygenic enrichment methods to investigate whether SNPs in the HAR, SD and Ohno regions of the human genome, related to early evolution are more likely to be associated with schizophrenia. Our results suggest that brain SNPs in evolutionary salient regions show enrichment of associations with schizophrenia compared to just any brain genes or non-brain genes within the same regions.

<hr>

[Visit Link](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0169227){:target="_blank" rel="noopener"}


