---
layout: post
title: "The Rights of Synthetic Lifeforms is the Next Great Civil Rights Controversy"
date: 2017-10-27
categories:
author:  
tags: [Robot,Artificial intelligence,Ethics of artificial intelligence,Technology,Self-driving car,Law,Issues in ethics,Cognitive science,Applied ethics,Branches of science]
---


As Kaevats pointed out, right now, self-aware artificial intelligences are so far off that there's no reason to rush into giving robots similar rights to humans. When Bryson spoke to Futurism, she warned against the establishment of robot rights, relating the situation to the way the legal personhood of corporations has been abused in the past. Corporations are legal persons, but it's a legal fiction. How should we tackle synthetic personhood for those entities? Traditionally, under the law, you’re either a person or you are property.

<hr>

[Visit Link](https://futurism.com/rights-synthetic-lifeforms-civil-rights-controversy/){:target="_blank" rel="noopener"}


