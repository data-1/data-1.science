---
layout: post
title: "The Alps are home to more than 3,000 lichens"
date: 2018-06-17
categories:
author: "Pensoft Publishers"
tags: [Lichen,Organisms,Ecology,Earth sciences,Natural environment]
---


Nevertheless, while the Alps are one of the best studied parts of the world in terms of their biogeography, no overview of the Alpine lichens had been provided up until recently, when an international team of lichenologists, led by Prof. Pier Luigi Nimis, University of Trieste, Italy, concluded their 15-year study with a publication in the open access journal MycoKeys. Credit: Dr Peter O. Bilovitz  They point out that such catalogue has been missing for far too long, hampering research all over the world. The scientists point out that this has been particularly annoying, since the data from the Alps could have been extremely useful for comparisons between mountainous lichen populations from around the globe. Thus, it could become a catalyst for new, more intensive investigations and turn into a benchmark for comparisons between mountains systems worldwide. Explore further When it comes to genes, lichens embrace sharing economy  More information: Pier Luigi Nimis et al, The lichens of the Alps – an annotated checklist, MycoKeys (2018).

<hr>

[Visit Link](https://phys.org/news/2018-03-alps-home-lichens.html){:target="_blank" rel="noopener"}


