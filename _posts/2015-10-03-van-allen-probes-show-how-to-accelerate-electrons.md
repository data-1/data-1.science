---
layout: post
title: "Van Allen Probes show how to accelerate electrons"
date: 2015-10-03
categories:
author: Karen C. Fox, Nasa'S Goddard Space Flight Center
tags: [Van Allen Probes,Van Allen radiation belt,Electromagnetic radiation,Space science,Outer space,Physics,Astronomy,Science,Physical sciences,Spaceflight,Nature]
---


NASA's Van Allen Probes orbit through two giant radiation belts surrounding Earth. Recent data from the Van Allen Probes suggests that it is a two-fold process: One mechanism gives the particles an initial boost and then a kind of electromagnetic wave called Whistlers does the final job to kick them up to such intense speeds. After two years in space, the Van Allen Probes data has largely pointed to the latter. Together the one-two punch is a mechanism that can effectively accelerate particles up to the intense speeds, which have for so long mysteriously appeared in the Van Allen belts. This is the first time we can truly explain how the electrons are accelerated up to nearly the speed of light.

<hr>

[Visit Link](http://phys.org/news324693971.html){:target="_blank" rel="noopener"}


