---
layout: post
title: "Atom interferometer"
date: 2018-08-16
categories:
author: ""
tags: [Quantum mechanics,Space,Physics,Wave interference,Science,Theoretical physics,Physical sciences,Applied and interdisciplinary physics,Scientific theories,Scientific method]
---


A prototype atom interferometer chip in a vacuum chamber, harnessing the quantum behaviour of atoms to perform ultra-precise measurements of gravity. “Quantum physics and space travel are two of the greatest scientific achievements of the last century,” comments ESA’s Bruno Leone, among the organisers of the latest Agency workshop on quantum technologies. “We now see great promise in bringing them together: many quantum experiments can be performed much more precisely in space, away from terrestrial perturbations. In addition, the new generation of quantum devices offer huge improvements to space-related technology. “Potential is there for the use of quantum technologies in areas such as Earth observation, planetary exploration, secure communications, fundamental physics, microgravity research and navigation.”  This Earth gravity meter is being developed by RAL Space in the UK and IQO Hannover in Germany, with ESA support.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2017/11/Atom_interferometer){:target="_blank" rel="noopener"}


