---
layout: post
title: "NASA's Van Allen Probes spot man-made barrier shrouding Earth"
date: 2017-09-21
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Very low frequency,Van Allen radiation belt,Radiation,Space weather,Earth,Van Allen Probes,Space science,Outer space,Astronomy,Nature,Physical sciences,Science]
---


Humans have long been shaping Earth's landscape, but now scientists know we can shape our near-space environment as well. A certain type of communications -- very low frequency, or VLF, radio communications -- have been found to interact with particles in space, affecting how and where they move. At times, these interactions can create a barrier around Earth against natural high energy particle radiation in space. These results, part of a comprehensive paper on human-induced space weather, were recently published in Space Science Reviews. This bubble is even seen by spacecraft high above Earth's surface, such as NASA's Van Allen Probes, which study electrons and ions in the near-Earth environment.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-05/nsfc-nva051717.php){:target="_blank" rel="noopener"}


