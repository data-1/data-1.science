---
layout: post
title: "Git 2.0.1 Version Control System Available for Download"
date: 2014-06-27
categories:
author: Silviu Stahie, Jun
tags: [Git,Computing,Software,Technology,Software development,Software engineering,Computer science,Computers,Information technology management,Computer engineering,System software,Digital media,Computer architecture,Information Age,Intellectual works,Utility software]
---


Git 2.0.1, a free and open source distributed version control system designed to handle everything from small to very large projects with speed and efficiency, is now available for download. The new Git 2.0.x branch continues the trend of large releases, integrating a big number of changes and fixes. This latest update is just for maintenance, but it’s packed with fixes. According to the developers, the tools that read diagnostic output in the standard error stream no longer see the terminal control sequence, the error message given upon a failure to open an existing loose object file due to permission issues is now phrased better, the mailmap.file configuration option now supports the tilde expansion, and the --ignore-space-change option of git apply no longer ignores the spaces at the beginning of the line. Also, the git blame no longer miscounts the number of columns needed to show localized timestamps, git commit --allow-empty-message -C $commit now works when the commit doesn’t have any log message, and the git format-patch now enforces the rule that the --follow option from the log/diff family of commands must be used with exactly one pathspec.

<hr>

[Visit Link](http://news.softpedia.com/news/Git-2-0-1-Version-Control-System-Available-for-Download-448534.shtml){:target="_blank" rel="noopener"}


