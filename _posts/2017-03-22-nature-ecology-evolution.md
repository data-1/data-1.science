---
layout: post
title: "Nature Ecology & Evolution"
date: 2017-03-22
categories:
author: "Lässig, Institute Of Theoretical Physics, University Of Cologne, Mustonen, Wellcome Trust Sanger Institute, Cambridge, Present Address, Department Of Biosciences, University Of Helsinki, Po Box"
tags: []
---


Evolutionary paths to antibiotic resistance under dynamically sustained drug selection. A. G. M. Predicting the evolution of antibiotic resistance. Levin, B. R., Perrot, V. & Walker, N. Compensatory mutations, antibiotic resistance and the population genetics of adaptive evolution in bacteria. The Genetical Theory of Natural Selection. Evolution 66, 3815–3824 (2012).

<hr>

[Visit Link](http://www.nature.com/articles/s41559-017-0077?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


