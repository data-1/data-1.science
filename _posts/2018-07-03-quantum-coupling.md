---
layout: post
title: "Quantum coupling"
date: 2018-07-03
categories:
author: "Universitaet Tübingen"
tags: [Quantum mechanics,Quantum computing,Superconductivity,Electromagnetism,Branches of science,Computing,Computer science,Theoretical physics,Electrical engineering,Physics,Electronics,Technology,Science,Applied and interdisciplinary physics]
---


The linking of these two building blocks is a significant step towards the construction of a hybrid quantum system of atoms and superconductors which will enable the further development of quantum processors and quantum networks. Instead of the conventional signals used in today's technology -- bits -- which can only be a one or a zero, the new hardware will have to process far more complex quantum entangled states. The hybrid quantum system combines nature's smallest quantum electronic building blocks -- atoms -- with artificial circuits -- the superconducting microwave resonators. The new hybrid system for future quantum processors and their networks forms a parallel with today's technology, which is also a hybrid, as a look at your computer hardware shows: Calculations are made by microelectronic circuits; information is stored on magnetic media, and data is carried through fiber-optic cables via the internet. Future quantum computers and their networks will operate on this analogy -- requiring a hybrid approach and interdisciplinary research and development for full functionality, Fortágh says.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/12/171221122720.htm){:target="_blank" rel="noopener"}


