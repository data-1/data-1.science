---
layout: post
title: "Researchers review the rapid progress in machine learning for the chemical sciences"
date: 2018-08-01
categories:
author: "University Of Bath"
tags: [Artificial intelligence,Machine learning,Research,Science,Technology,Chemical substance,Intelligence,Learning,Computer,Cognitive science,Computing,Branches of science]
---


In a new paper published in Nature, researchers review the rapid progress in machine learning for the chemical sciences. Historically, the discovery of materials has involved a combination of chance, intuition, and trial and error—but this could all be set to change thanks to artificial intelligence. An international team of scientists from the UK and the USA, including Ph.D. student Daniel Davies from the Centre for Sustainable Chemical Technologies and Department of Chemistry, published a review on the growing potential of machine learning for chemical design. Daniel said: Machine learning is a branch of artificial intelligence where computers are programmed by learning from data. But scientists are now starting to turn to artificial intelligence driven solutions to accelerate their own materials discovery and optimisation processes.

<hr>

[Visit Link](https://phys.org/news/2018-07-rapid-machine-chemical-sciences.html){:target="_blank" rel="noopener"}


