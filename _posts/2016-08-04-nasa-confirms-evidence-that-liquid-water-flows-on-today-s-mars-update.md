---
layout: post
title: "NASA confirms evidence that liquid water flows on today's Mars (Update)"
date: 2016-08-04
categories:
author: ""
tags: [Seasonal flows on warm Martian slopes,Mars,Mars Reconnaissance Orbiter,Discovery and exploration of the Solar System,Exploration of Mars,Science,Outer space,Planets of the Solar System,Planetary science,Spaceflight,Space science,Astronomical objects known since antiquity,Terrestrial planets,Solar System,Astronomy]
---


Using an imaging spectrometer on MRO, researchers detected signatures of hydrated minerals on slopes where mysterious streaks are seen on the Red Planet. These downhill flows, known as recurring slope lineae (RSL), often have been described as possibly related to liquid water. Credit: NASA/JPL/University of Arizona  The spectrometer observations show signatures of hydrated salts at multiple RSL locations, but only when the dark features were relatively wide. However, this study of RSL detected perchlorates, now in hydrated form, in different areas than those explored by the landers. These dark streaks flowing downhill on warm Martian slopes have been inferred to be contemporary flowing liquid water on Mars.

<hr>

[Visit Link](http://phys.org/news/2015-09-evidence-brine-mars.html){:target="_blank" rel="noopener"}


