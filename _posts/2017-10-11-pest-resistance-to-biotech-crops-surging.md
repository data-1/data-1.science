---
layout: post
title: "Pest resistance to biotech crops surging"
date: 2017-10-11
categories:
author: "University of Arizona"
tags: [Pesticide resistance,Bacillus thuringiensis,Genetically modified crops,Biotechnology,Agriculture,Biology]
---


This will be critically important information as more crops are engineered to produce Bt toxins. As expected from evolutionary theory, factors favoring sustained efficacy of Bt crops were recessive inheritance of resistance in pests and abundant refuges, Carriere said. Although India similarly required a refuge strategy, farmer compliance was low. Same pest, same crop, same Bt proteins, but very different outcomes, said Tabashnik. All other Bt proteins in genetically engineered crops are in another group, called crystalline, or Cry, proteins.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uoa-prt100517.php){:target="_blank" rel="noopener"}


