---
layout: post
title: "Development Release: Oracle Linux 7 RC (DistroWatch.com News)"
date: 2014-06-27
categories:
author: ""        
tags: [Oracle Linux,Linux distribution,Linux,Application software,Technology,Open-source movement,Information technology management,Digital media,Software,Computing,Software development,System software,Free content,Software engineering,Computer science,Free software,Operating system families,Computer engineering,Computer architecture,Computers,Operating system technology]
---


Distribution Release: Oracle Linux 8.6 Simon Coter has announced the release of version 8.6 of Oracle Linux, an enterprise-class Linux distribution supported by Oracle and built from source packages for Red Hat Enterprise Linux (RHEL):  Oracle is pleased to announce the availability of the Oracle Linux 8 update 6 for the 64-bit Intel, 64-bit AMD (x86_64) and 64-bit Arm (aarch64) platforms. Distribution Release: Oracle Linux 8.4 Simon Coter has announced the release of Oracle Linux 8.4, the latest update from the project that develops an enterprise-class Linux distribution supported by Oracle and built from source packages for Red Hat Enterprise Linux (RHEL):  Oracle is pleased to announce the availability of the Oracle Linux 8 Update 4 for the 64-bit Intel and AMD (x86_64) and 64-bit Arm (aarch64) platforms. Oracle Linux 8 Update 3 release includes: improved support for NVDIMM devices; improved support for IPv6 static configurations; installation program uses the default LUKS2 version for an encrypted container....  Read the release announcement and the release notes for more information. Distribution Release: Oracle Linux 7.9 Simon Coter has announced the release of Oracle Linux 7 Update 9, a new build of the company's enterprise-class Linux distribution's legacy branch, compiled from the source code of Red Hat Enterprise Linux (RHEL) 7.9:  Oracle is pleased to announce the general availability of Oracle Linux 7 Update 9, which includes Unbreakable Enterprise Kernel (UEK) Release 6 as the default kernel. This release is the first in the 8.x series that installs Oracle's Unbreakable Enterprise Kernel (UEK) by default:  Oracle is pleased to announce the general availability of Oracle Linux 8 Update 2.

<hr>

[Visit Link](http://distrowatch.com/8500){:target="_blank" rel="noopener"}


