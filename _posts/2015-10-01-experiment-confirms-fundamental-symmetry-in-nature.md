---
layout: post
title: "Experiment confirms fundamental symmetry in nature"
date: 2015-10-01
categories:
author: Fundação de Amparo à Pesquisa do Estado de São Paulo 
tags: [High-energy nuclear physics,ALICE experiment,Large Hadron Collider,Particle physics,Relativistic Heavy Ion Collider,Proton,Nuclear physics,Quantum mechanics,Science,Nature,Physics,Quantum field theory,Physical sciences,Theoretical physics]
---


The measurements were made possible by the ALICE experiment's capacity to track and identify particles produced in high-energy collisions of heavy ions in the Large Hadron Collider  Scientists working with ALICE (A Large Ion Collider Experiment), a heavy-ion detector on the Large Hadron Collider (LHC) ring, have made precise measurements of particle mass and electric charge that confirm the existence of a fundamental symmetry in nature. These measurements of particles produced in high-energy collisions of heavy ions in the LHC were made possible by the ALICE experiment's high-precision tracking and identification capabilities, as part of an investigation designed to detect subtle differences between the ways in which protons and neutrons join in nuclei while their antiparticles form antinuclei. In particle physics, a very important question is whether all the laws of physics display a specific kind of symmetry known as CPT, and these measurements suggest that there is indeed a fundamental symmetry between nuclei and antinuclei, said Marcelo Gameiro Munhoz, a professor at USP's Physics Institute (IF) and a member of the Brazilian team working on ALICE. This allows ALICE to make a detailed comparison of the properties of the nuclei and antinuclei that are most copiously produced. The high precision of the time-of-flight detector, which determines the arrival time of particles and antiparticles with a resolution of 80 picoseconds and is associated with the energy-loss measurement provided by the time-projection chamber, allows the scientists involved to measure a clear signal for deuterons/antideuterons and helium-3/antihelium-3, the particles studied in the similarity experiment.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/fda-ecf092115.php){:target="_blank" rel="noopener"}


