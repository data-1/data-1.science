---
layout: post
title: "CaloriSMART test system succeeds in magnetocaloric cooling"
date: 2018-07-01
categories:
author: "Ames Laboratory"
tags: [Magnetic refrigeration,Refrigeration,Technology]
---


The CaloriSMART system is a benchtop model system for testing small sample of magnetocaloric, elastocaloric and electrocaloric materials. Credit: Ames Laboratory  Researchers at the U.S. Department of Energy's Ames Laboratory have designed and built an advanced model system that successfully uses very small quantities of magnetocaloric materials to achieve refrigeration level cooling. The initial test subjected a sample of gadolinium to sequential magnetic fields, causing the sample to alternate between heating up and cooling down. Magnetic refrigeration near room temperature has been broadly researched for 20 years, but this is one of the best systems that has been developed. The plan is to upgrade the system to work with elastocaloric materials—that reversibly heat up and cool down when subjected to cyclic tension or compression—and electrocaloric materials—that do the  The 3-D printed manifold holding a gadolinium sample.

<hr>

[Visit Link](https://phys.org/news/2018-03-calorismart-magnetocaloric-cooling.html){:target="_blank" rel="noopener"}


