---
layout: post
title: "Scientists sequence genome of worm that can regrow body parts, seeking stem cell insights: Worm's genome could lead to better understanding of its regenerative prowess, advance stem cell biology"
date: 2016-07-11
categories:
author: "Cold Spring Harbor Laboratory"
tags: [Regeneration (biology),News aggregator,Biology,Biotechnology,Life sciences]
---


He was studying an important pathway in mammalian reproductive tissues when he became interested in Macrostomum. This and other regenerating flatworms have the same kind of pathway operating in stem cells that is responsible for their remarkable regenerative capabilities. The researchers used the worm's genomic information to study how gene expression changed during regeneration. We think this is going to be a very important species for stem cell research. Well before its genome was available, M. lignano was already being studied for its insights into stem cells and tissue differentiation.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150921153459.htm){:target="_blank" rel="noopener"}


