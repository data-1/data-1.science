---
layout: post
title: "How bird evolution swapped snouts for beaks"
date: 2015-08-25
categories:
author: Peter Reuell, Harvard University
tags: [Bird,Beak,FGF8,Evolution,Wnt signaling pathway,Gene regulatory network,Cellular differentiation,Biology]
---


“Because we can get live alligator embryos with snouts, we can actually compare them with beaked modern birds to see how they develop their faces, and what genes are activating during that process,” said Arkhat Abzhanov, who led a team of researchers who found that bird beaks are the result of skeletal changes controlled by two genetic pathways. The immediate ancestors of birds had very long fingers and a somewhat opposable thumb, he said. Because we can get live alligator embryos with snouts, we can actually compare them with beaked modern birds to see how they develop their faces, and what genes are activating during that process, Abzhanov said. For insight into beak development, Abzhanov and colleagues interrupted the pathways in chickens, producing embryos with facial bones that looked remarkably like their snouted dinosaur ancestors. We suggest paleontologists will be able to find fossils in the future which will bridge this gap between beaked modern birds and their snouted ancestors, Abzhanov said.

<hr>

[Visit Link](http://phys.org/news/2015-08-bird-evolution-swapped-snouts-beaks.html){:target="_blank" rel="noopener"}


