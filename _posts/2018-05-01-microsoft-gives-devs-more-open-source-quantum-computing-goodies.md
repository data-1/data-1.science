---
layout: post
title: "Microsoft Gives Devs More Open Source Quantum Computing Goodies"
date: 2018-05-01
categories:
author: ""
tags: []
---


Further, the kit will be interoperable with the Python computing language. Microsoft announced the Quantum Development Kit at its Ignite conference last fall. Python Interoperability  Developers have been clamoring for the kit to be made available for Linux and macOS, according to Microsoft. Supporting interoperability for Python allows developers to access their existing libraries from Q# without having to port it, Friedman pointed out. IBM rolled out its open source QISKit earlier last year, making quantum computers available to run programs on hardware or online quantum simulators.

<hr>

[Visit Link](http://www.linuxinsider.com/story/85173.html?rss=1){:target="_blank" rel="noopener"}


