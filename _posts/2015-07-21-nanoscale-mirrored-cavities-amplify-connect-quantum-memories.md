---
layout: post
title: "Nanoscale mirrored cavities amplify, connect quantum memories"
date: 2015-07-21
categories:
author: ""   
tags: [Quantum memory,Nitrogen-vacancy center,Electron,Atom,Photon,Chemistry,Physics,Electromagnetic radiation,Materials,Physical chemistry,Materials science,Electromagnetism,Applied and interdisciplinary physics,Technology,Atomic molecular and optical physics,Physical sciences,Optics]
---


Layers of diamond and air keep light trapped within these cavities long enough to interact with the nitrogen atom's spin state and transfer that information via the emitted light. Impurities trapped in diamond  The memory elements described in this research are the spin states of electrons in nitrogen-vacancy (NV) centers in diamond. It is already possible to transfer information about the electron spin state via photons, but we have to make the interface between the photons and electrons more efficient. Light and mirrors  These cavities, nanofabricated at Brookhaven by MIT graduate student Luozhou Li with the help of staff scientist Ming Lu of the CFN, consist of layers of diamond and air tightly spaced around the impurity atom of an NV center. This increases the efficiency of information transfer between photons and the NV center's electron spin state.

<hr>

[Visit Link](http://www.nanodaily.com/reports/Nanoscale_mirrored_cavities_amplify_connect_quantum_memories_999.html){:target="_blank" rel="noopener"}


