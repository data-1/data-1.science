---
layout: post
title: "Magnetic stem cells for gene engineering"
date: 2017-10-08
categories:
author: "Tomsk Polytechnic University"
tags: [Mesenchymal stem cell,Stem cell,Mesenchyme,Cell biology,Medical specialties,Health sciences,Medicine,Life sciences,Biology,Biotechnology,Clinical medicine]
---


Scientists from the Tomsk Polytechnic University's Laboratory of Novel Dosage are developing a technology to control mesenchymal stem cells of patients. The novel technology implies that mesenchymal stem cells (MSCs) of the patient's body with the size of about 10 microns are internationalized with magnetic controlled microcapsules with drug inside. Mesenchymal stem cells are inherently able to migrate toward tumors. For the first time, the scientists have demonstrated the efficiency of internationalization of magnetic microcapsules by MSCs to functionalize cells and to design magnetic controlled cells and tissue engineering systems. As a result of magnetic capsules internalization by MSCs we create a new cell engineering platform which is responsive to external magnetic field to control cell migration.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-12/tpu-msc122816.php){:target="_blank" rel="noopener"}


