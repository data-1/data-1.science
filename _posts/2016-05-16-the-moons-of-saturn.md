---
layout: post
title: "The moons of Saturn"
date: 2016-05-16
categories:
author: Matt Williams
tags: [Moons of Saturn,Saturn,Natural satellite,Titan (moon),Planets,Planemos,Planetary science,Bodies of the Solar System,Solar System,Moons,Planets of the Solar System,Astronomy,Outer planets,Astronomical objects known since antiquity,Gas giants,Outer space,Astronomical objects,Space science]
---


Inner Large Moons:  Saturn's Inner Large Moons, which orbit within the E Ring (see below), includes the larger satellites Mimas, Enceladus, Tethys, and Dione. At 1066 km in diameter, Tethys is the second-largest of Saturn's inner moons and the 16th-largest moon in the solar system. Credit: NASA/JPL/Space Science Institute  Large Outer Moons:  The Large Outer Moons, which orbit outside of the Saturn's E Ring, are similar in composition to the Inner Moons – i.e. composed primarily of water ice and rock. All have prograde orbits that range from 11.1 to 17.9 million km, and from 7 to 40 km in diameter. This group is also sometimes referred to as the Phoebe group, due to the presence of a single larger moon in the group – which measures 240 km in diameter.

<hr>

[Visit Link](http://phys.org/news/2015-08-moons-saturn.html){:target="_blank" rel="noopener"}


