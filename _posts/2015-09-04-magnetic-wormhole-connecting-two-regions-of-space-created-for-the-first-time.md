---
layout: post
title: "Magnetic wormhole connecting two regions of space created for the first time"
date: 2015-09-04
categories:
author: Universitat Autonoma De Barcelona
tags: [Wormhole,Superconductivity,Magnetism,Magnetic field,Ferromagnetism,Magnetic resonance imaging,Physics,Science,Applied and interdisciplinary physics,Physical sciences,Electromagnetism]
---


(Right) In terms of magnetism the wormhole is undetectable, which means that the magnetic field seems to disappear on the right only to reappear on the left in the form of a magnetic monopole. Scientists in the Department of Physics at the Universitat Autònoma de Barcelona have designed and created in the laboratory the first experimental wormhole that can connect two regions of space magnetically. This consists of a tunnel that transfers the magnetic field from one point to the other while keeping it undetectable - invisible - all the way. Credit: Jordi Prat-Camps and Universitat Autònoma de Barcelona  These same researchers had already built a magnetic fibre in 2014: a device capable of transporting the magnetic field from one end to the other. 3D image of the wormhole, made up of concentric layers: from outside to inside, (b) an external metasurface made of ferromagnetic pieces, (c) an internal superconducting layer and, (d) a magnetic fibre made of folded ferromagnetic foil.

<hr>

[Visit Link](http://phys.org/news/2015-09-magnetic-wormhole-regions-space.html){:target="_blank" rel="noopener"}


