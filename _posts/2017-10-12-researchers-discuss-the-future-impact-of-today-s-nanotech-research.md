---
layout: post
title: "Researchers discuss the future impact of today's nanotech research"
date: 2017-10-12
categories:
author: "Northwestern University"
tags: [Nanotechnology,Chad Mirkin,Chemistry,Branches of science,Physical sciences,Science,Technology]
---


Credit: Northwestern University  World-renowned nanoscientists and chemists Chad Mirkin, the Director of the International Institute for Nanotechnology (IIN) at Northwestern University, and Teri Odom, the IIN's Associate Director, sit down to discuss the golden age of miniaturization and how the science of small things is fostering major advances. Q: Your team discovered spherical nucleic acid (SNA) technology, where tiny particles can be decorated with short snippets of DNA or RNA. One area is photonics, where advances at the nanoscale are changing how we communicate. That's the thrilling thing about science in general, but about nanotechnology in particular: we often have goals, which are driven by engineering needs, but along the way we discover fundamentally interesting principles that we didn't anticipate and that inform our view of the world around us. This is how we do science at Northwestern, and we really apply it to nanotechnology.

<hr>

[Visit Link](https://phys.org/news/2017-10-discuss-future-impact-today-nanotech.html){:target="_blank" rel="noopener"}


