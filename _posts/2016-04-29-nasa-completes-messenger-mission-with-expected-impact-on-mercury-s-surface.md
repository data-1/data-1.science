---
layout: post
title: "NASA completes MESSENGER mission with expected impact on Mercury's surface"
date: 2016-04-29
categories:
author: Johns Hopkins University
tags: [MESSENGER,Mercury (planet),NASA Deep Space Network,Physical sciences,Bodies of the Solar System,Planetary science,Solar System,Science,Spaceflight,Space science,Astronomy,Outer space]
---


On April 30, 2015, the MESSENGER spacecraft sent its final image. Mission controllers were able to confirm the end of operations just a few minutes later at 3:40 p.m., when no signal was detected by the Deep Space Network (DSN) station in Goldstone, California, at the time the spacecraft would have emerged from behind the planet had MESSENGER not impacted the surface. MESSENGER's final hours  MESSENGER's last orbit with real-time flight operations began at 11:15 a.m. EDT, with initiation of the final delivery of data and images from Mercury via the DSN 70-m antenna in Madrid, Spain. MESSENGER passed behind Mercury (as viewed from Earth) at 3:29 p.m., however the signal from our intrepid spacecraft started fading prior to that and dropped out for the last time at 3:25 p.m.  At 3:38 p.m. EDT, at the time the spacecraft would have emerged from behind the planet as viewed from the Goldstone station had MESSENGER not impacted, mission operators began monitoring for a signal, but as expected they were unable to establish communications between MESSENGER and the DSN. Explore further NASA spacecraft crashes on Mercury after 11-year mission

<hr>

[Visit Link](http://phys.org/news349679370.html){:target="_blank" rel="noopener"}


