---
layout: post
title: "UAB researchers develop a harmless artificial virus for gene therapy"
date: 2015-12-09
categories:
author: Universitat Autonoma de Barcelona
tags: [Virus,DNA,Gene,Protein,Cell (biology),Nanobiotechnology,Chemistry,Biology,Life sciences,Biotechnology,Molecular biology,Biochemistry,Genetics,Medical specialties]
---


Researchers of the Nanobiology Unit from the UAB Institute of Biotechnology and Biomedicine, led by Antonio Villaverde, managed to create artificial viruses, protein complexes with the ability of self-assembling and forming nanoparticles which are capable of surrounding DNA fragments, penetrating the cells and reaching the nucleus in a very efficient manner, where they then release the therapeutic DNA fragments. The achievement represents an alternative with no biological risk to the use of viruses in gene therapy. Nevertheless, it is very complicated to control the way in which protein blocks are organised, in order to form more complex structures which could be used to transport DNA in an efficient manner, as happens with viruses. Professor Antonio Villaverde's group has discovered the combination necessary to make these proteins act as an artificial virus and self-assemble themselves to form regular protein nanoparticles capable of penetrating target cells and reaching the nucleus in a very efficient manner. What occurs in chemotherapy as a cancer treatment can also be compared to the problems in gene therapy.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/uadb-urd040815.php){:target="_blank" rel="noopener"}


