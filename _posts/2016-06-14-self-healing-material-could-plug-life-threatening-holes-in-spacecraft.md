---
layout: post
title: "Self-healing material could plug life-threatening holes in spacecraft"
date: 2016-06-14
categories:
author: "American Chemical Society"
tags: [Atmosphere of Earth,Oxygen]
---


Although shields and fancy maneuvers could help protect space structures, scientists have to prepare for the possibility that debris could pierce a vessel. But should the bumpers fail, a wall breach would allow life-sustaining air to gush out of astronauts' living quarters. Explore further ISS astronauts dodge flying Russian space debris  More information: Rapid, Puncture-Initiated Healing via Oxygen-Mediated Polymerization, ACS Macro Lett., 2015, 4 (8), pp 819–824. DOI: 10.1021/acsmacrolett.5b00315  Abstract  Autonomously healing materials that utilize thiol–ene polymerization initiated by an environmentally borne reaction stimulus are demonstrated by puncturing trilayered panels, fabricated by sandwiching thiol–ene–trialkylborane resin formulations between solid polymer panels, with high velocity projectiles; as the reactive liquid layer flows into the entrance hole, contact with atmospheric oxygen initiates polymerization, converting the liquid into a solid plug. Using infrared spectroscopy, we find that formulated resins polymerize rapidly, forming a solid polymer within seconds of atmospheric contact.

<hr>

[Visit Link](http://phys.org/news/2015-08-self-healing-material-life-threatening-holes-spacecraft.html){:target="_blank" rel="noopener"}


