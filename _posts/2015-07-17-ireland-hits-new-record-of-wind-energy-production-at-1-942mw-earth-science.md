---
layout: post
title: "Ireland hits new record of wind energy production at 1,942MW - Earth Science"
date: 2015-07-17
categories:
author: Colm Gorey, Colm Gorey Was A Senior Journalist With Silicon Republic
tags: [Wind power,Renewable resources,Aerodynamics,Energy and the environment,Economies,Economy,Sustainable development,Wind,Sustainable technologies,Power (physics),Physical quantities,Renewable electricity,Nature,Sustainable energy,Electric power,Renewable energy,Energy]
---


The windy conditions that have battered Ireland have been be a major boon to the country’s wind-energy production, contributing to its largest ever wind-energy output. The figure recorded by EirGrid this morning (Wednesday) at 9.30am showed that 1,942MW of energy had been created through the country’s wind energy turbines which is enough to power over 1.26m homes. Despite us being only a matter of days within the new year, this marks the second time in 2015 that the record has been broken as on 1 January, wind energy output was as high as 1,872MW. According to the Irish Wind Energy Association (IWEA), when combined with the output from Northern Ireland, the island’s total output this morning was 2,490MW. Since the start of the new year, the equivalent of 33pc of Ireland’s energy has been produced through the high winds of recent days.

<hr>

[Visit Link](http://www.siliconrepublic.com/clean-tech/item/40044-ireland-hits-new-record-of){:target="_blank" rel="noopener"}


