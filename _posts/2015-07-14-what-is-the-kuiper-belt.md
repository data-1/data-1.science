---
layout: post
title: "What is the Kuiper Belt?"
date: 2015-07-14
categories:
author: Matt Williams
tags: [Kuiper belt,Planets beyond Neptune,Solar System,Pluto,Michael E Brown,Local Interstellar Cloud,Substellar objects,Science,Planets,Physical sciences,Planemos,Outer space,Astronomical objects,Bodies of the Solar System,Planetary science,Space science,Astronomy]
---


But instead, in the history of the solar system, when Neptune formed it led to these objects not being able to get together, so it's just this belt of material out beyond Neptune. Discovery:  Shortly after Tombaugh's discovery of Pluto, astronomers began to ponder the existence of a Trans-Neptunian population of objects in the outer Solar System. Even more exciting is the fact that surveys of other solar systems indicate that our Solar System isn't unique. Future of the Kuiper Belt:  When he initially speculated about the existence of a belt of objects beyond Neptune, Kuiper indicated that such a belt probably did not exist anymore. Credit: Lexicon/NASA Images  We call it a belt, but it's a very wide belt.

<hr>

[Visit Link](http://phys.org/news353749610.html){:target="_blank" rel="noopener"}


