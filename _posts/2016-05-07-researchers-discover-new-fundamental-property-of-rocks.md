---
layout: post
title: "Researchers discover new fundamental property of rocks"
date: 2016-05-07
categories:
author: National Oceanography Centre
tags: [Sedimentary rock,Rock (geology),Porosity,Research,Geophysics,Science,Technology,Earth sciences]
---


The pore network  The discovery of a new fundamental rock property will improve estimates of underground resources, such as hydrocarbons and drinking water, as well as CO₂ storage reservoir capacity. This finding will improve the interpretation of geological fluid flow from geophysical surveys. Fluids and electrical currents can flow through sedimentary rock via a network of gaps in between the sediment grains, called pores. However, because the unique electrical resistivity measurement system and processing software that I developed at the NOC is so accurate, I knew that the data must be the result of a 'real' rock property. bit.ly/1AZDw8b Anomalous electrical resistivity anisotropy in clean reservoir sandstones., 62: 1315–1326.

<hr>

[Visit Link](http://phys.org/news353058311.html){:target="_blank" rel="noopener"}


