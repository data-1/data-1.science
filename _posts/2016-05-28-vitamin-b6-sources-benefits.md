---
layout: post
title: "Vitamin B6: Sources & Benefits"
date: 2016-05-28
categories:
author: "Alina Bradford"
tags: [Vitamin B6,Vitamin,Health sciences,Nutrition,Clinical medicine,Health,Medical specialties,Diseases and disorders]
---


Since the body can so easily run out of B6, it is important to consume foods that contain B6. First breath  Vitamin B6 may have given rise to Earth's first oxygen-producing organisms, according to researchers at the University of Illinois. B6, specifically, is a vitamin that is used to make several neurotransmitters in the brain, according to the University of Maryland Medical Center. People who have trouble absorbing vitamin B6 from food or dietary supplements can develop a deficiency, as well. Men over 50 have an RDA of 1.7 mg, while women over 50 have an RDA of 1.5 mg, according to NLM.

<hr>

[Visit Link](http://www.livescience.com/51920-vitamin-b6.html){:target="_blank" rel="noopener"}


