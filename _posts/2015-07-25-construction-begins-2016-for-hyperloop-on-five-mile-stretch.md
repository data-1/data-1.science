---
layout: post
title: "Construction begins 2016 for Hyperloop on five-mile stretch"
date: 2015-07-25
categories:
author: Nancy Owano, Tech Xplore
tags: [Hyperloop,Hyperloop Transportation Technologies,Technology]
---


Hyperloop Transportation Technologies announced it plans to start building a five-mile track in central California in 2016. The company, Hyperloop Transportation Technologies, reached an agreement for the working Hyperloop passenger transportation system on a five-mile stretch in California's Central Valley as part of Quay Valley. Quay Valley will be a model town for the 21st century, a self-sustaining community, according to the About Quay Valley website. What is the Hyperloop? Instead the Hyperloop system would use tubes at very low pressure to reduce air resistance and the capsules would run on a 'ski' of air - rather than wheels - a bit like a puck on an air hockey table.

<hr>

[Visit Link](http://phys.org/news344410744.html){:target="_blank" rel="noopener"}


