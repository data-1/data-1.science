---
layout: post
title: "Swiftly moving gas streamer eclipses supermassive black hole"
date: 2014-06-23
categories:
author: University College London 
tags: [Active galactic nucleus,Galaxy,Black hole,NGC 5548,Supermassive black hole,Radiation,Astronomical objects,Astronomy,Space science,Physical sciences,Extragalactic astronomy,Outer space,Galaxies]
---


The researchers detected a clumpy gas stream flowing quickly outward and blocking 90 percent of the X-rays emitted by the supermassive black hole at the center of the galaxy. There are other galaxies that show gas streams near a black hole, but they haven't changed as dramatically. Right after the Hubble Space Telescope had observed NGC 5548 on June 22, 2013, the team discovered unexpected features in the data. Because of this shielding, the persistent wind far away from the nucleus receives less radiation and cools down. ###  Team:  The team consists of Jelle Kaastra (SRON Utrecht, The Netherlands), Jerry Kriss (Space Telescope Science Institute, Baltimore, USA), Massimo Cappi (INAF-IASF Bologna, Italy), Missagh Mehdipour (SRON Utrecht, The Netherlands), Pierre-Olivier Petrucci (Univ.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/ucl-smg061714.php){:target="_blank" rel="noopener"}


