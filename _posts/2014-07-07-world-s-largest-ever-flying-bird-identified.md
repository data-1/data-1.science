---
layout: post
title: "World's largest-ever flying bird identified"
date: 2014-07-07
categories:
author: National Evolutionary Synthesis Center, Nescent
tags: [Argentavis,Pelagornis sandersi]
---


A reconstruction of the world's largest-ever flying bird, Pelagornis sandersi, identified by Daniel Ksepka, Curator of Science at the Bruce Museum in Greenwich, Conn. Credit: Liz Bradford  Scientists have identified the fossilized remains of an extinct giant bird that could be the biggest flying bird ever found. With an estimated 20-24-foot wingspan, the creature surpassed size estimates based on wing bones from the previous record holder—a long-extinct bird named Argentavis magnificens—and was twice as big as the Royal Albatross, the largest flying bird today. Now in the collections at the Charleston Museum, the strikingly well-preserved specimen consisted of multiple wing and leg bones and a complete skull. To find out, Ksepka fed the fossil data into a computer program designed to predict flight performance given various estimates of mass, wingspan and wing shape. Once it was airborne, Ksepka's simulations suggest that the bird's long, slender wings made it an incredibly efficient glider.

<hr>

[Visit Link](http://phys.org/news323962726.html){:target="_blank" rel="noopener"}


