---
layout: post
title: "Engineers invent breakthrough millimeter-wave circulator IC: Researchers are first to demonstrate a circulator on a silicon chip at mm-wave frequencies that enables nonreciprocal transmission of waves"
date: 2017-10-08
categories:
author: "Columbia University School of Engineering and Applied Science"
tags: [Circulator,Duplex (telecommunications),Integrated circuit,Extremely high frequency,Semiconductor,Wireless,Computing,Communication,Electronic engineering,Computer science,Telecommunications engineering,Telecommunications,Electricity,Computer engineering,Electrical engineering,Information and communications technology,Electronics,Technology]
---


At the IEEE International Solid-State Circuits Conference in February, Krishnaswamy's group unveiled a new device: the first magnet-free non-reciprocal circulator on a silicon chip that operates at millimeter-wave frequencies (frequencies near and above 30GHz). Following up on this work, in a paper published today in Nature Communications, the team demonstrated the physical principles behind the new device. Most devices are reciprocal: signals travel in the same manner in forward and reverse directions. Full-duplex communications, in which a transmitter and a receiver of a transceiver operate simultaneously on the same frequency channel, enables doubling of data capacity within existing bandwidth. The Columbia Engineering circulator could also be used to build millimeter-wave full-duplex wireless links for VR headsets, which currently rely on a wired connection or tether to the computing device.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171006085919.htm){:target="_blank" rel="noopener"}


