---
layout: post
title: "Thirty years of AIDS data highlight survival gains, room for improvement"
date: 2016-05-07
categories:
author: Infectious Diseases Society of America
tags: [HIVAIDS,Infection,Preventive healthcare,Disease,Progressive multifocal leukoencephalopathy,Health,HIV,Public health,Infectious diseases,Diseases and disorders,Life sciences,Microbiology,Medical statistics,Immunology,Causes of death,Epidemiology,Health care,Medicine,Health sciences,Medical specialties,Clinical medicine]
---


For the study, researchers analyzed HIV surveillance data collected continuously over the past 30-plus years by the San Francisco Department of Public Health on initial and subsequent AIDS-defining opportunistic infections, citywide, beginning in 1981. These were common during the early years of the U.S. epidemic, when death often quickly followed an AIDS diagnosis: From 1981 to 1986, only 7 percent of patients in San Francisco diagnosed with their first opportunistic infection lived more than five years, according to the study. Since then, with advances in antiretroviral therapy (ART), wider availability of HIV testing, and improvements in the treatment of opportunistic illnesses, survival rates have markedly risen. The remaining 35 percent of AIDS patients--those who died within five years of being diagnosed with an opportunistic infection--underscore the work still to be done to address AIDS-related illnesses today, the authors noted. Indeed, some of these infections, such as progressive multifocal leukoencephalopathy (PML), and infection-related cancers, including brain lymphoma, remain associated with substantial mortality risk today, the authors found, despite an overall improvement in survival rates.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150604084715.htm){:target="_blank" rel="noopener"}


