---
layout: post
title: "MAGNDATA: Towards a database of magnetic structures"
date: 2017-10-07
categories:
author: "International Union Of Crystallography"
tags: [Magnetic structure,Crystallography,Technology,Computing,Information technology]
---


Magnetic structure of a multferroic material taken from the database in (a) and (b) with different magnetic symmetry and no multiferroic character. In 1976, an important effort was made to gather information available on all the magnetic structures known at that point, and a compilation of about 1000 magnetic structures was published. In a recently published paper [Gallego et al., (2016). 49, DOI: 10.1107/S1600576716012863] the scientists present and discuss its main features for the case of commensurate structures. The commensurate case, Journal of Applied Crystallography (2016).

<hr>

[Visit Link](http://phys.org/news/2016-11-magndata-database-magnetic.html){:target="_blank" rel="noopener"}


