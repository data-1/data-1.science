---
layout: post
title: "Microsoft Buys GitHub: The Linux Foundation’s Reaction"
date: 2018-06-09
categories:
author: "Jim Zemlin"
tags: [GitHub,Open source,Open-source software,Linux,Open-source-software movement,Microsoft,Linux Foundation,Technology,Software,Software engineering,Computing,Software development,Intellectual works]
---


How folks seem to conflate “buying GitHub” the company and development platform with somehow buying “open source”: Two of the fastest growing projects in The Linux Foundation family, Kubernetes and Node.js, are developed on GitHub. So what does this mean for open source? I can’t wait to help make the GitHub platform and community that’s special to all of us even greater.” I believe he means it. Buying GitHub does not mean Microsoft has engaged in some sinister plot to “own” the more than 70 million open source projects on GitHub. Why would Microsoft do this?

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/microsoft-buys-github-the-linux-foundations-reaction/){:target="_blank" rel="noopener"}


