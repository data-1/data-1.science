---
layout: post
title: "Image: Hubble sees a dying star's final moments"
date: 2016-05-16
categories:
author:  
tags: [Nebula,Star,Hubble Space Telescope,Planetary nebula,Space science,Astronomical objects,Physical sciences,Astronomy]
---


Credit: ESA/Hubble & NASA, Acknowledgement: Matej Novak  A dying star's final moments are captured in this image from the NASA/ESA Hubble Space Telescope. The death throes of this star may only last mere moments on a cosmological timescale, but this star's demise is still quite lengthy by our standards, lasting tens of thousands of years! The star's agony has culminated in a wonderful planetary nebula known as NGC 6565, a cloud of gas that was ejected from the star after strong stellar winds pushed the star's outer layers away into space. These same colors can be seen in the famous and impressive Ring Nebula (heic1310), a prominent example of a nebula like this one. When this happens, the star's light drastically diminishes and ceases to excite the surrounding gas, so the nebula fades from view.

<hr>

[Visit Link](http://phys.org/news/2015-07-image-hubble-dying-star-moments.html){:target="_blank" rel="noopener"}


