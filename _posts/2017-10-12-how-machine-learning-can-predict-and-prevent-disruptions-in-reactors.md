---
layout: post
title: "How machine learning can predict and prevent disruptions in reactors"
date: 2017-10-12
categories:
author: "Massachusetts Institute Of Technology"
tags: [Fusion power,Plasma (physics),Tokamak,Electron,Applied and interdisciplinary physics,Chemistry,Electromagnetism,Physics,Physical sciences,Nature]
---


He recently gave a talk hosted by the MIT Energy Initiative (MITEI) on using machine learning to develop a real-time warning system for impending disruptions in fusion reactors. However, at the high temperatures that we need for fusion, the thermal energy of each atom or molecule is much, much greater than the binding energy that holds the electrons and nuclei together, so the neutral particles break up into their constituents, i.e. electrons and nuclei, which we call the plasma state. Therefore, in a plasma, all the particles are charged, and there are long-range electric and magnetic forces acting between the particles. The whole goal of fusion energy is to develop large power plants to generate electrical power on the grid, and replace today's fossil-fueled utility power plants, and even replace fission nuclear power plants. When dealing with large, complicated datasets, machine learning may be a powerful way of finding subtle patterns in the data that elude human efforts.

<hr>

[Visit Link](https://phys.org/news/2017-10-machine-disruptions-reactors.html){:target="_blank" rel="noopener"}


