---
layout: post
title: "Engineers envision an electronic switch just three atoms thick"
date: 2014-07-01
categories:
author: Stanford University
tags: [Graphene,Materials science,Crystal structure,Crystal,Technology,Chemistry,Materials]
---


Computer simulations show that this crystalline lattice has the remarkable ability to behave like a switch: it can be mechanically pulled and pushed, back and forth, between two different atomic structures – one that conducts electricity well, the other that does not. Theoretically, such electronic materials have potential to reduce battery-draining power consumption in existing devices such as smart phones. Duerloo said this switchable material is formed when one atomic layer of molybdenum atoms gets sandwiched between two atomic layers of tellurium atoms. Credit: Karel-Alexander Duerloo  In his simulation, Duerloo relied on the fact that molybdenum and tellurium form a sheet-like crystal lattice that is just three-atoms thick. Duerloo's simulations show that it takes just a tiny effort to toggle the atomic structure of this three-layer amalgam from a non-conductive state into a conductive state.

<hr>

[Visit Link](http://phys.org/news323366944.html){:target="_blank" rel="noopener"}


