---
layout: post
title: "‘Gradient Descent’: when Artificial Intelligence meets art"
date: 2018-08-14
categories:
author: ""
tags: [Artificial intelligence,Art,Interdisciplinary subfields,Cognition,Cognitive psychology,Branches of science,Concepts in metaphysics,Cognitive science,Neuroscience]
---


On a mission to create a new genre of art, the ‘Gradient Descent’ show in Delhi observes how artists around the globe are using machines to make masterpieces  On a mission to create a new genre of art, the ‘Gradient Descent’ show in Delhi observes how artists around the globe are using machines to make masterpieces  GANs vs cGANs GANs or ‘Generative Adversarial Networks’ are a set of instructions that engage as networks do. By varying the amount of training the algorithm gets, Harshit generates vivid abstract painterly images. Using AI that has been trained on portraits of the Old Masters, Mario presents a bizarre take on how AI can build upon images. A pioneer of AI and art, Tom White from London will be presenting two provocative series. Then she can generate a whole film using those paintings.

<hr>

[Visit Link](https://www.thehindu.com/sci-tech/technology/gradient-descent-when-artificial-intelligence-meets-art-641/article24677618.ece){:target="_blank" rel="noopener"}


