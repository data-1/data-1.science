---
layout: post
title: "Modeling shockwaves through the brain"
date: 2015-07-24
categories:
author: Massachusetts Institute of Technology 
tags: [Traumatic brain injury,Blast wave,Brain,Shock wave]
---


Nevertheless, there remain many gaps in scientists' understanding of the effects of blasts on the human brain; most new knowledge has come from experiments with animals. Now MIT researchers have developed a scaling law that predicts a human's risk of brain injury, based on previous studies of blasts' effects on animal brains. A group of ISN researchers led by Aurélie Jean, a postdoc in Radovitzky's group, developed simulations of human, pig, and rat heads, and exposed each to blasts of different intensities. Their simulations predicted the effects of the blasts' shockwaves as they propagated through the skulls and brains of each species. In collaboration with Army colleagues, Radovitzky and his group are performing basic research to help the Army develop helmets that better protect soldiers.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/miot-mst092914.php){:target="_blank" rel="noopener"}


