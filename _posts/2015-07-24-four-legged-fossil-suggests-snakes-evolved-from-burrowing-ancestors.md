---
layout: post
title: "Four-legged fossil suggests snakes evolved from burrowing ancestors"
date: 2015-07-24
categories:
author: American Association for the Advancement of Science (AAAS) 
tags: [Snake,American Association for the Advancement of Science,Tetrapodophis]
---


This news release is available in Japanese. The discovery of a four-legged fossil of a snake hints that this suborder may have evolved from burrowing, rather than marine, ancestors. The unique four-legged specimen, found in Brazil's Crato Formation, provides us with more insight into how these creatures transitioned into the sleek, slithering reptiles that we are familiar with - and often fearful of - today. The newly discovered species Tetrapodophis amplectus, which lived during the Early Cretaceous 146 to 100 million years ago, maintains many classic snake features, such as a short snout, long braincase, elongated body, scales, fanged teeth and a flexible jaw to swallow large prey. The main, glaring difference is Tetrapodophis's four limbs, which do not appear to have been used for locomotion.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/aaft-ffs072015.php){:target="_blank" rel="noopener"}


