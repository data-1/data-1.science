---
layout: post
title: "Complex genetic ancestry of Americans uncovered"
date: 2016-04-19
categories:
author: Oxford University
tags: [Americans,Race and ethnicity in the United States,African Americans,United States,Africa,Genetics,Caribbean,Ethnicity,Ethnic groups,Human populations]
---


Credit: George Hodan/public domain  By comparing the genes of current-day North and South Americans with African and European populations, an Oxford University study has found the genetic fingerprints of the slave trade and colonisation that shaped migrations to the Americas hundreds of years ago. The study found that:  While Spaniards provide the majority of European ancestry in continental American Hispanic/Latino populations, the most common European genetic source in African-Americans and Barbadians comes from Great Britain. Compared to South Americans, people from Caribbean countries (such as the Barbados) had a larger genetic contribution from Africa. The research team analysed DNA samples collected from people in Barbados, Columbia, the Dominican Republic, Ecuador, Mexico, Puerto Rico and African-Americans in the USA. 'The differences in European ancestry between the Caribbean islands and mainland American population that we found were also previously unknown.

<hr>

[Visit Link](http://phys.org/news346406346.html){:target="_blank" rel="noopener"}


