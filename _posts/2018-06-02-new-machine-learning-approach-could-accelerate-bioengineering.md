---
layout: post
title: "New machine learning approach could accelerate bioengineering"
date: 2018-06-02
categories:
author: "Dan Krotz, Lawrence Berkeley National Laboratory"
tags: [Lawrence Berkeley National Laboratory,Systems biology,Technology,Branches of science,Science]
---


The scientists used the technique to automatically predict the amount of biofuel produced by pathways that have been added to E. coli bacterial cells. Trouble-shooting takes up 99% of our time. Then machine learning took over: The algorithm taught itself how the concentrations of metabolites in these pathways change over time, and how much biofuel the pathways produce. It learned these dynamics by analyzing data from the two experimentally known pathways that produce small and large amounts of biofuels. Explore further New pathways, better biofuels  More information: Zak Costello et al. A machine learning approach to predict metabolic pathway dynamics from time-series multiomics data, npj Systems Biology and Applications (2018).

<hr>

[Visit Link](https://phys.org/news/2018-05-machine-approach-bioengineering.html){:target="_blank" rel="noopener"}


