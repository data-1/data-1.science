---
layout: post
title: "How strong is the gravity on Mars?"
date: 2017-10-08
categories:
author: "Matt Williams"
tags: [Surface gravity,Mars,Earth radius,Earth,Space science,Astronomy,Outer space,Planetary science,Physical sciences,Planets of the Solar System,Science,Planets]
---


What is the gravity on Mars? When applied to a spherical body with a given average density, it will be approximately proportional to its radius. These proportionalities can be expressed by the formula g = m/r2, where g is the surface gravity of Mars (expressed as a multiple of the Earth's, which is 9.8 m/s²), m is its mass – expressed as a multiple of the Earth's mass (5.976·1024 kg) – and r its radius, expressed as a multiple of the Earth's (mean) radius (6,371 km). Credit: geodesy.curtin.edu.au  For instance, Mars has a mass of 6.4171 x 1023 kg, which is 0.107 times the mass of Earth. Learning more about Martian gravity and how terrestrial organisms fare under it could be a boon for space exploration and missions to other planets as well.

<hr>

[Visit Link](http://phys.org/news/2016-12-strong-gravity-mars.html){:target="_blank" rel="noopener"}


