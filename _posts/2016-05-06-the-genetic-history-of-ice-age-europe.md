---
layout: post
title: "The Genetic History of Ice Age Europe"
date: 2016-05-06
categories:
author:  
tags: [Neanderthal,DNA sequencing,Archaeology,Ancient DNA,Human,David Reich (geneticist),Science,DNA,Archaeogenetics,Europe,Branches of science,Genetics]
---


The genetic data show that, beginning 37,000 years ago, all Europeans come from a single founding population that persisted through the Ice Age, said Reich. This branch seems to have been displaced in most parts of Europe 33,000 years ago, but around 19,000 years ago, a population related to it re-expanded across Europe, Reich explained. The researchers also detected some mixture with Neanderthals, around 45,000 years ago, as modern humans spread across Europe. After they washed the ancient DNA over the 1.2 million probe sequences, the researchers sequenced the ancient DNA that was captured by the probes. The genetic history of Ice Age Europe.

<hr>

[Visit Link](http://www.ancient-origins.net/news-evolution-human-origins/genetic-history-ice-age-europe-005829){:target="_blank" rel="noopener"}


