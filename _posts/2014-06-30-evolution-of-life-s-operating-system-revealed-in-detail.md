---
layout: post
title: "Evolution of life's operating system revealed in detail"
date: 2014-06-30
categories:
author: Georgia Institute Of Technology
tags: [Ribosome,Life,Abiogenesis,Loren Williams,Bacteria,Messenger RNA,Biology,Cell (biology),Life sciences,Genetics,Biotechnology,Molecular biology,Biochemistry,Nature,Chemistry]
---


The researchers found distinct fingerprints in the ribosomes where new structures were added to the ribosomal surface without altering the pre-existing ribosomal core from the last universal common ancestor. Ribosomes, in all species use mRNA as a blueprint for building all the proteins and enzymes essential to life. The common core of the ribosome is essentially the same in humans, yeast, bacteria and archaea – in all living systems. The researchers found distinct fingerprints in the ribosomes where new structures were added to the ribosomal surface without altering the pre-existing core. We learned some of the rules of the ribosome, that evolution can change the ribosome as long as it does not mess with its core, Williams said.

<hr>

[Visit Link](http://phys.org/news323359278.html){:target="_blank" rel="noopener"}


