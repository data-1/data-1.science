---
layout: post
title: "Scientists map brain's 'thesaurus' to help decode inner thoughts"
date: 2016-04-30
categories:
author: National Science Foundation
tags: [Brain,Language,Braincomputer interface,Functional magnetic resonance imaging,Human brain,Information,Neuroimaging,Interdisciplinary subfields,Science,Cognitive psychology,Branches of science,Neuroscience,Cognition,Cognitive science]
---


What if a map of the brain could help us decode people's inner thoughts? The atlas identifies brain areas that respond to words that have similar meanings. The participants' brain imaging data were then matched against time-coded, phonemic transcriptions of the stories. Not surprisingly, the maps show that many areas of the human brain represent language that describes people and social relations, rather than abstract concepts. But we also get the fine-grained information that tells us what kind of information is represented in each brain area.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/nsf-smb042916.php){:target="_blank" rel="noopener"}


