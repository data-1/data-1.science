---
layout: post
title: "Oceania: How Humans Populated One of Earth's Most Remote Places"
date: 2017-10-26
categories:
author: Alvaro Montenegro
tags: [Pacific Ocean,Polynesia,Wind,Outrigger boat]
---


Just look at a map of Remote Oceania – the region of the Pacific that contains Hawaii, New Zealand, Samoa, French Polynesia and Micronesia – and it’s hard not to wonder how people originally settled on these islands. What did the environmental data suggest? (Moving eastward in the area east of Samoa does require sailing against the wind, though.) And no matter what time of year, El Niño or not, you need to move against the wind to travel eastward around Samoa. In addition to changes to wind patterns that facilitate movement to the east, the El Niño weather pattern also causes drier conditions over western portions of Micronesia and Polynesia every two to seven years.

<hr>

[Visit Link](http://www.livescience.com/56626-how-humans-first-populated-oceania.html){:target="_blank" rel="noopener"}


