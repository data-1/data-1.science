---
layout: post
title: "Giant solar flare"
date: 2015-09-03
categories:
author: "$author"   
tags: [Sun,Solar flare,Ancient astronomy,Space science,Astronomy,Bodies of the Solar System,Solar System,Outer space,Space plasmas,Astronomical objects,Astronomical objects known since antiquity,Stellar astronomy,Physical sciences,Astrophysics,Physical phenomena,Nature,Plasma physics,Electromagnetism]
---


Halloween 2003 will live forever in the annals of solar history. In the space of two weeks centred around the spooky celebration, solar physicists witnessed the most sustained bout of solar activity since satellites took to the skies. This image shows the second spot’s parting volley. M1 flares are ten times more powerful than C1, and X1 flares are ten times more powerful than M1 flares, or 100 times more powerful than C1. Analysis showed that it clocked in at X28, making it 28 times more powerful than an X1.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2014/11/Giant_solar_flare){:target="_blank" rel="noopener"}


