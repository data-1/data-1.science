---
layout: post
title: "Planktonic world: First scientific results from the Tara Oceans expedition"
date: 2015-07-12
categories:
author: European Molecular Biology Laboratory
tags: [Plankton,Ocean,Climate change,Organism,Ecosystem,Biodiversity,Marine food web,Virus,Climate,Microorganism,Water,Oceanography,Ecology,Nature,Earth sciences,Natural environment,Biology,Environmental science,Physical geography,Organisms,Systems ecology]
---


How do planktonic organisms interact? Thanks to novel computer models, the researchers were able to predict how these diverse planktonic organisms interact. This map is a first step towards a better understanding of the dynamics and structure of the global marine ecosystem. Different sets of organisms come together depending on the water temperature. Understanding the distribution and the interactions of the plankton across the oceans will be very useful for predictive models necessary to study climate change.

<hr>

[Visit Link](http://phys.org/news351435816.html){:target="_blank" rel="noopener"}


