---
layout: post
title: "Ferroelectric switching seen in biological tissues"
date: 2014-06-23
categories:
author: Michelle Ma, University Of Washington
tags: [Elastin,Biology,Technology]
---


This is an illustration of the molecular structure of tropoelastin, the smallest unit of the protein elastin. These findings are the first that clearly track this phenomenon, called ferroelectricity, occurring at the molecular level in biological tissues. We wanted to bring in different experimental techniques, evidence and theoretical understanding of ferroelectricity in biological functions, said Jiangyu Li, a UW professor of mechanical engineering and corresponding author of the paper. A research team led by Li first discovered ferroelectric properties in biological tissues in 2012, then in 2013 found that glucose can suppress this property in the body's connective tissues, wherever the protein elastin is present. They then measured the current with the poling field removed and found that the current switched direction when the poling electric field was switched, a sign of ferroelectricity.

<hr>

[Visit Link](http://phys.org/news322756517.html){:target="_blank" rel="noopener"}


