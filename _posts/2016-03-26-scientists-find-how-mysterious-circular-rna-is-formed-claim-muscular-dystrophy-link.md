---
layout: post
title: "Scientists find how mysterious 'circular RNA' is formed, claim muscular dystrophy link"
date: 2016-03-26
categories:
author: The Hebrew University of Jerusalem
tags: [Circular RNA,RNA,Gene,Muscular dystrophy,Genetics,DNA,Skeletal muscle,Myotonic dystrophy,Life sciences,Molecular biology,Biochemistry,Biotechnology,Biology,Nucleic acids,Cell biology]
---


Unlike all other known RNAs, this molecule is circular, and was labeled circular RNA. The researchers also demonstrated that circular RNA molecules are highly produced in the brain, and in many cases from genes with very important functions. This strongly suggests that circRNAs play an important role in brain function — and likely in brain disease. When considered together, the important role played by muscleblind in regulating circRNAs, combined with these molecules' abundance in the brain, suggests that circRNAs might be involved in development of myotonic dystrophy. The research is published in Molecular Cell as CircRNA biogenesis competes with pre-mRNA splicing.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/thuo-rdh091814.php){:target="_blank" rel="noopener"}


