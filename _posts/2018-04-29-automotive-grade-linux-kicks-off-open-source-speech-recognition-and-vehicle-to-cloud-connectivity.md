---
layout: post
title: "Automotive Grade Linux Kicks Off Open Source Speech Recognition and Vehicle-to-Cloud Connectivity"
date: 2018-04-29
categories:
author: "The Linux Foundation"
tags: [Linux Foundation,Connected car,Cloud computing,Technology,Computing,Computer science]
---


Led by Amazon Alexa, Nuance and Voicebox Technologies, the AGL Speech Expert Group is developing open APIs to voice-enable every application in the vehicle  SAN FRANCISCO, Feb. 28, 2018 – Automotive Grade Linux (AGL), a collaborative cross-industry effort developing an open platform for the connected car, today announced the launch of two new Expert Groups (EG) focused on Speech Recognition and Vehicle-to-Cloud (V2C) connectivity. “We plan to provide a standard set of open APIs that allows developers to write their application only once, and it will work on any system from any automaker using AGL, regardless of the underlying speech recognition technology. Making it simpler for automakers to implement voice services is a big step toward this vision – we’re excited to join this working group and innovate on new ways to improve the voice experience for customers.”  “We are proud to be a founding member of the Speech Expert Group and fully share in the mission of creating a standardized set of speech recognition APIs,” said Eric Montague, Sr. Director, Product Marketing and Strategy, Automotive Speech, Nuance Communications, Inc. “Our Dragon Drive automotive platform powers more than 200 million cars on the road today across more than 40 languages, creating conversational experiences for major automakers worldwide. About Automotive Grade Linux (AGL)  Automotive Grade Linux is a collaborative open source project that is bringing together automakers, suppliers and technology companies to accelerate the development and adoption of a fully open software stack for the connected car. Learn more: https://www.automotivelinux.org/  Automotive Grade Linux is hosted at The Linux Foundation.

<hr>

[Visit Link](https://www.linuxfoundation.org/press-release/automotive-grade-linux-kicks-off-open-source-speech-recognition-vehicle-cloud-connectivity/){:target="_blank" rel="noopener"}


