---
layout: post
title: "Cells sense and explore their environments"
date: 2018-01-20
categories:
author: "University Of Barcelona"
tags: [Cell (biology),Extracellular matrix,Cell biology]
---


The process through which cells are able to sense their environment is regulated by force detection. Since this force depends on the ligand spatial distribution, this enables cells to sense their surroundings. Depending on this cell force distribution, it can affect the activation of genetic transcription, a phenomenon that determines which genes are expressed, says Roger Oria, first author of the study and Ph.D. student at the UB in Dr Roca-Cusachs' laboratory. With this deeper knowledge of how cells detect their surroundings, researchers proved that by altering the conditions of the cell's environment (rigidity and distribution of those ligands that create the extracellular matrix), they can control the adherence response of the cell, and even define a range in which the cell adheres. The cell recognizes these nanospheres as a ligand, so researchers can measure how cells regulate force distribution and the number of ligands to which they adhere regarding their density.

<hr>

[Visit Link](https://phys.org/news/2017-12-cells-explore-environments.html){:target="_blank" rel="noopener"}


