---
layout: post
title: "'Blurred times' in a quantum world"
date: 2017-09-17
categories:
author: "University Of Vienna"
tags: [Time,Uncertainty principle,General relativity,Theory of relativity,Quantum mechanics,Energy,Space,Mechanics,Physics,Spacetime,Academic discipline interactions,Scientific method,Concepts in metaphysics,Science,Epistemology of science,Theoretical physics,Scientific theories,Physical sciences,Applied and interdisciplinary physics]
---


However, when quantum mechanical and gravitational effects are taken into account, this picture is no longer tenable, as the clocks mutually disturb each other and hands of the clocks become fuzzy. However, combining quantum mechanics and Einstein's theory of general relativity theoretical physicists from the University of Vienna and the Austrian Academy of Sciences have demonstrated a fundamental limitation for our ability to measure time. The more precise a given clock is, the more it blurs the flow of time measured by neighbouring clocks. However, in quantum mechanics, one of the major theories in modern physics, Heisenberg's uncertainty principle asserts a fundamental limit to the precision with which pairs of physical properties can be known, such as the energy and time of a clock. According to quantum mechanics, if we have a very precise clock its energy uncertainty is very large.

<hr>

[Visit Link](https://phys.org/news/2017-03-blurred-quantum-world.html){:target="_blank" rel="noopener"}


