---
layout: post
title: "'Nightmare' geometry of Uranus creates tumbling magnetosphere – Physics World"
date: 2017-10-08
categories:
author: "Sarah Tesh"
tags: [Magnetosphere,Uranus,Solar wind,Planet,Planetary science,Astronomy,Space science,Outer space,Physical sciences,Astronomical objects,Planets,Planets of the Solar System,Science,Nature,Physics,Bodies of the Solar System,Solar System]
---


Most planets are surrounded by a magnetosphere – a plasma structure created by the planet’s magnetic fields controlling the flow of cosmic charged particles. Realignment switch  Usually, this structure is in a “closed” state – charged particles in the solar wind are directed away from Earth. Magnetic reconnection can happen in Earth’s magnetosphere when the solar wind changes direction due to solar storms. “When the magnetized solar wind meets this tumbling field in the right way, it can reconnect, and Uranus’s magnetosphere goes from open to closed to open on a daily basis,” explains Paty. As well as this “switch-like” mechanism, Cao and Paty also suggest that solar-wind reconnection (as on Earth) occurs upstream of Uranus’s magnetosphere, in its twisted tail.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/jun/28/nightmare-geometry-of-uranus-creates-tumbling-magnetosphere){:target="_blank" rel="noopener"}


