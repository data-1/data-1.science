---
layout: post
title: "First detection of boron on the surface of Mars"
date: 2017-09-23
categories:
author: "DOE/Los Alamos National Laboratory"
tags: [Chemistry and Camera complex,Curiosity (rover),Mars,Gale (crater),Earth sciences,Nature]
---


No prior mission to Mars has found boron, said Patrick Gasda, a postdoctoral researcher at Los Alamos National Laboratory. If the boron that we found in calcium sulfate mineral veins on Mars is similar to what we see on Earth, it would indicate that the groundwater of ancient Mars that formed these veins would have been 0-60 degrees Celsius [32-140 degrees Fahrenheit] and neutral-to-alkaline pH. The discovery of boron is only one of several recent findings related to the composition of Martian rocks. Groundwater and chemicals dissolved in it that appeared later on Mars left its effects most clearly in mineral veins that filled cracks in older layered rock. The boron and clay underline the mobility of elements and electrons, and that is good for life.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-12/danl-fdo121316.php){:target="_blank" rel="noopener"}


