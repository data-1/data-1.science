---
layout: post
title: "Oxford, UK to create first zero-emissions zone in the world"
date: 2017-10-12
categories:
author: "Greg Beach"
tags: []
---


Oxford, England, with its history of learning dating back to the 11th century, is now shifting into the future with an electric-vehicle only zone in the city center. By 2035, the ban will have expanded to all fossil-fuel powered vehicles and will encompass the entire city center. Continue reading below Our Featured Videos  While such a dramatic change in the city center’s urban design may encourage less driving, thus less greenhouse gas emissions, the zone was inspired by a need to reduce levels of nitrogen dioxide, most of which comes from car exhaust, by three-fourths. Chronic exposure to nitrogen dioxide can cause respiratory problems and eye irritation. SIGN UP  Related: GM’s plans for “all-electric-future” spell doom for fossil fuel industry  The switch-over plan is expected to cost Oxford city government, bus companies, and local businesses approximately £7 million to replace the fossil-fuel consuming vehicles, including all municipal vehicles, with electric vehicles.

<hr>

[Visit Link](https://inhabitat.com/oxford-uk-to-create-first-zero-emissions-zone-in-the-world){:target="_blank" rel="noopener"}


