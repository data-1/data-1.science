---
layout: post
title: "Peering into complex, tiny structures with 3D analysis tool tomviz"
date: 2017-09-24
categories:
author: "Robert Hovden"
tags: [Nanotechnology,Red Hat,3D computer graphics,Technology,Science,Computing]
---


New open source software tomviz—short for tomographic visualization—enables researchers to interactively understand large 3D datasets. The algorithms we've developed in tomviz can handle big datasets and enable scientists to understand the nanoscale universe across dimensions, says Yi Jiang. (Robert Hovden, CC BY)  Now scientists can rapidly study the nanoscale in 3D at the highest resolution by utilizing data acquired from an electron microscope and loading that data into the tomviz software. 3D nanomaterials for clean-energy vehicles visualized in tomviz 1.0 by Elliot Padget (Cornell University). And because the tool is open source, a much greater community of talented developers has contributed.

<hr>

[Visit Link](https://opensource.com/article/17/3/tomviz-large-3D-datasets){:target="_blank" rel="noopener"}


