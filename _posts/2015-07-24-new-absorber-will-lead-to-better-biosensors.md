---
layout: post
title: "New absorber will lead to better biosensors"
date: 2015-07-24
categories:
author: Northwestern University 
tags: [Biosensor,Technology,Applied and interdisciplinary physics,Materials,Electromagnetic radiation,Electromagnetism,Optics,Physical sciences,Materials science,Physical chemistry,Electrical engineering,Chemistry]
---


Biosensors are more sensitive and able to detect smaller changes in the environment  Biological sensors, or biosensors, are like technological canaries in the coalmine. The narrower the band of absorbed light is, the more sensitive the biosensor. Aydin and his team have created a new nanostructure that absorbs a very narrow spectrum of light—having a bandwidth of just 12 nanometers. This ultranarrow band absorber can be used for a variety of applications, including better biosensors. The beauty of our design is that we found a way to engineer the material by using a different substrate, Aydin said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/nu-naw100114.php){:target="_blank" rel="noopener"}


