---
layout: post
title: "The role of 'extra' DNA in cancer evolution and therapy resistance"
date: 2018-08-03
categories:
author: "Henry Ford Health"
tags: [Extrachromosomal DNA,Cancer,Gene duplication,Glioblastoma,DNA,Evolution,Biochemistry,Medical specialties,Health,Molecular biology,Biotechnology,Genetics,Clinical medicine,Biology,Causes of death,Diseases and disorders,Life sciences,Neoplasms]
---


Research is beginning to provide a better understanding of the processes underlying cell-to-cell differences within GBM tumors - a crucial finding because these differences contribute to therapy resistance. The ultimate goal is to identify what pathways can be targeted to block glioma progression. More detailed investigation showed that many instances of oncogene amplification found in the glioma tumors involved ecDNA elements. Sometimes both daughter cells inherit ecDNA, but sometimes all or most of it will end up in one cell and not the other. The bigger goal is to learn how and why ecDNA elements form.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-04/hfhs-tro042318.php){:target="_blank" rel="noopener"}


