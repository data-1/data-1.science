---
layout: post
title: "Jena Experiment: Loss of species destroys ecosystems"
date: 2017-11-30
categories:
author: "Technical University of Munich (TUM)"
tags: [Biodiversity,Ecosystem,Soil,Meadow,Extinction,Earth sciences,Environmental technology,Organisms,Earth phenomena,Physical geography,Environmental conservation,Environmental social science,Biogeochemistry,Environmental science,Systems ecology,Ecology,Nature,Natural environment]
---


In order to find this out, the Jena Experiment was established in 2002, one of the largest biodiversity experiments worldwide. Professor Weisser from the Chair for Terrestrial Ecology at the TUM has summarized the findings of the long-term project Jena Experiment, which is managed by the Friedrich Schiller University Jena, since its inception in a 70-page article in the journal Basic and Applied Ecology. According to these findings, this extinction then has a delayed effect on the material cycles. In the Jena Experiment, it was demonstrated for the first time the extent to which e.g. the nitrogen cycle of a certain piece of land depended on a wide range of factors such as species diversity, microbiological organisms, the water cycle, and plant interaction. Due to its breadth, the Jena Experiment proves for the very first time that a loss of biodiversity results in negative consequences for many individual components and processes in ecosystems.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/tuom-jel112817.php){:target="_blank" rel="noopener"}


