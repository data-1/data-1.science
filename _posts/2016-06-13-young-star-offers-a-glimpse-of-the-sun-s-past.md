---
layout: post
title: "Young star offers a glimpse of the Sun’s past"
date: 2016-06-13
categories:
author: ""
tags: [Star,Sun,Outer space,Astronomy,Space science,Physical sciences,Astronomical objects,Stellar astronomy,Stars,Sky,Physics]
---


It may look like a star, it may be called a star, but it does not yet generate energy like a normal star. Located 1800 light-years away in the constellation Cygnus, V1331 Cyg was originally nothing but a diffuse cloud of gas in space. V1331 is not yet fully formed and so is still larger than it will eventually be once gravity has done its job. Hydrogen will then be transformed into helium and this will release the torrents of energy that will make V1331 Cyg shine for billions of years as a bona fide star. This circumstellar disc is the site where planets may be forming, and in the case of V1331 Cyg time is running out.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2016/06/Young_star_offers_a_glimpse_of_the_Sun_s_past){:target="_blank" rel="noopener"}


