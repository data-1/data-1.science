---
layout: post
title: "Repeated marine predator evolution tracks changes in ancient and Anthropocene oceans"
date: 2015-09-14
categories:
author: "$author"   
tags: [Marine vertebrate,Marine life,Convergent evolution,Evolution,Tetrapod,Animals,Nature,Biology]
---


The paper also highlights how evolutionary history informs an understanding of the impact of human activities on marine species today. For example, modern dolphins and extinct marine reptiles called ichthyosaurs descended from distinct terrestrial species, but independently converged on an extremely similar fish-like body plan although they were separated in time by more than 50 million years. The repeated transformation of legs adapted for walking on land into fins is another classic example of convergent evolution. In the case of deep divers such as beaked whales and seals, these species have independently evolved to have positively charged oxygen-binding proteins called myoglobin in their muscles, allowing them to survive underwater for long periods of time. They intend that this comprehensive review will encourage future collaboration between researchers across scientific fields and lead to new insights about evolutionary biology, paleontology and marine conservation.

<hr>

[Visit Link](http://phys.org/news348408423.html){:target="_blank" rel="noopener"}


