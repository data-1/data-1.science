---
layout: post
title: "A CRISPR antiviral tool"
date: 2015-12-09
categories:
author: Emory Health Sciences 
tags: [Virus,Cas9,Infection,RNA interference,Hepatitis C,Bacteria,RNA,Hepatitis,Microbiology,Biotechnology,Clinical medicine,Molecular biology,Biology,Medical specialties,Genetics,Biochemistry,Medicine,Health sciences,Life sciences]
---


Emory scientists have adapted an antiviral enzyme from bacteria called Cas9 into an instrument for inhibiting hepatitis C virus in human cells. Cas9 is part of the CRISPR genetic defense system in bacteria, which scientists have been harnessing to edit DNA in animals, plants and even human cells. In this case, Emory researchers are using Cas9 to put a clamp on RNA, which hepatitis C virus uses for its genetic material, rather than change cells' DNA. Here, we're targeting a viral RNA, for which there is no corresponding DNA in the cells. The RNA-targeting technique resembles RNA interference, which scientists use as a tool for shutting off selected genes in cells, animals and plants.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/ehs-aca042715.php){:target="_blank" rel="noopener"}


