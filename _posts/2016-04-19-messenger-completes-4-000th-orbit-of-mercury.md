---
layout: post
title: "MESSENGER completes 4,000th orbit of Mercury"
date: 2016-04-19
categories:
author: Johns Hopkins University
tags: [MESSENGER,Mercury (planet),Flight,Spacecraft,Science,Astronautics,Solar System,Astronomy,Spaceflight,Space science,Outer space]
---


The orbital phase of the MESSENGER mission, which was originally designed to collect data for one Earth year, just completed its fourth year of operation around Mercury. The mission has received a final extension to allow scientists to gather specific low-altitude data over an additional several weeks. Thanks to requisite optimization tools and techniques—such as the SciBox science planning tool, customized downlink rate stepping during Deep Space Network contacts, and automated prioritization of data playback with the use of the Consultative Committee for Space Data Systems (CCSDS) file delivery protocol, to name just a few—we have captured more than 275,000 images and downlinked more than four Earth years of data from our comprehensive suite of instruments, he continued. MESSENGER really is the little spacecraft that could. The MESSENGER milestone of 4,000 orbits around Mercury in just over four years is a testament to the talent and dedication of the teams that designed, built, and operated a spacecraft for which the original plan was to complete about 740 orbits during a single year after Mercury orbit insertion, said APL's James McAdams, MESSENGER's Mission Design Lead Engineer.

<hr>

[Visit Link](http://phys.org/news346918691.html){:target="_blank" rel="noopener"}


