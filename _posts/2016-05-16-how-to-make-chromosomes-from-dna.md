---
layout: post
title: "How to make chromosomes from DNA"
date: 2016-05-16
categories:
author: The University of Tokyo
tags: [DNA,Cell (biology),Genetics,Condensin,Chromosome,Gene,Transcription (biology),Macromolecules,Biological processes,Branches of genetics,Nucleic acids,Molecular genetics,Cell biology,Life sciences,Biotechnology,Biology,Molecular biology,Biochemistry,Chemistry,Cellular processes,Nucleotides,Biomolecules]
---


DNA molecules are long, string-like polymers storing the genetic information of life and, in a cell, are tightly packed into structures called chromosomes. Researchers at the University of Tokyo, including Assistant Professor Takashi Sutani, Professor Katsuhiko Shirahige (Institute of Molecular and Cellular Biosciences) and Ph.D student Toyonori Sakata (Graduate School of Agricultural and Life Sciences), isolated from cells and analyzed DNA segments to which condensin binds, and revealed that condensin is associated with single-stranded DNA (ssDNA) which is produced by unwinding of the DNA double-helix. They also discovered that chromosome segregation defects in mutant cells that showed lowered levels of condensin function were largely rescued by transcription inhibition. They therefore concluded that ssDNA is produced by unwinding of double-stranded DNA during transcription, that ssDNA is detrimental to assembling chromosomes, and that condensin restores unwound ssDNA segments to double-stranded DNA. This work was conducted in collaboration with the research group of Dr. Tatsuya Hirano (Chief Scientist at RIKEN Institute, Japan).

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150727095904.htm){:target="_blank" rel="noopener"}


