---
layout: post
title: "Breakthrough helps explain how DNA is organized in our cells"
date: 2016-05-03
categories:
author: Institut De Recherches Cliniques De Montreal
tags: [Chromatin,Histone,Histone H2A,Gene,DNA,Molecular genetics,Genetics,Molecular biology,Biochemistry,Biotechnology,Cell biology,Biology,Life sciences,Macromolecules,Branches of genetics]
---


In our laboratory, we study one such histone variant called H2A.Z, says Célia Jeronimo, PhD, research associate in Dr. Robert's laboratory and first author of the study. H2A.Z is located in specific regions of the gene called promoters, which are involved in controlling gene expression. In fact, we found that these proteins are essential to prevent an accumulation of H2A.Z in inappropriate regions of the gene, which leads to a disorganized chromatin structure. Although our study was performed in yeast cells, it suggests that mislocalization of H2A.Z may lead to cryptic transcription in some types of cancer such as lymphoma, and this may contribute to the disease. Our next step is therefore to investigate the possible role of H2A.Z and its associated gene expression defects in cancer cells.

<hr>

[Visit Link](http://phys.org/news351859510.html){:target="_blank" rel="noopener"}


