---
layout: post
title: "Evaporation dynamics at the nano- and micro-scale"
date: 2017-10-27
categories:
author: Institute Of Physical Chemistry Of The Polish Academy Of Sciences
tags: [Gas,Evaporation,Temperature,Liquid,Energy,Applied and interdisciplinary physics,Physical sciences,Chemistry,Physical chemistry,Physics,Nature,Continuum mechanics]
---


Small drops of micro- and nanometre dimensions have surprised researchers: they evaporate more slowly than expected from hitherto predictions, because of the ballistic energy transfer between gas molecules and the surface of liquid. Credit: IPC PAS, Grzegorz Krzyzewski  A new evaporation dynamics study finds that very small droplets evaporate more slowly than predicted by current models. The gas molecule simply takes its energy and hits the surface, sometimes several times, says Dr. Marek Litniewski (IPC PAS), co-author of the research. During evaporation, the ballistic transfer of energy begins to play a role for gas molecules micrometers away from the surface of the droplet, which in the scale of the phenomenon should be regarded as a relatively large value. Therefore, when such a molecule bounces off the surface, it can collide with another nearby gas molecule and hit the surface again, depositing another portion of energy into it.

<hr>

[Visit Link](https://phys.org/news/2017-10-evaporation-dynamics-nano-micro-scale.html){:target="_blank" rel="noopener"}


