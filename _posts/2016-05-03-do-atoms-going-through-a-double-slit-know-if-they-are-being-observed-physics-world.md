---
layout: post
title: "Do atoms going through a double slit ‘know’ if they are being observed? – Physics World"
date: 2016-05-03
categories:
author:  
tags: [Wave interference,Double-slit experiment,Photon,Physics,Science,Theoretical physics,Quantum mechanics,Applied and interdisciplinary physics,Physical sciences,Scientific theories,Scientific method]
---


Does a massive quantum particle – such as an atom – in a double-slit experiment behave differently depending on when it is observed? The techniques used could have practical applications for future physics research, and perhaps for information theory. In the famous double-slit experiment, single particles, such as photons, pass one at a time through a screen containing two slits. If an interference pattern is still seen when the second slit is opened, this would force us either to conclude that our decision to measure the particle’s path affects its past decision about which path to take, or to abandon the classical concept that a particle’s position is defined independent of our measurement. “I can’t prove that isn’t what occurs,” says Truscott, “But 99.999% of physicists would say that the measurement – i.e. whether the beamsplitter is in or out – brings the observable into reality, and at that point the particle decides whether to be a wave or a particle.”  Indeed, the results of both Truscott and Aspect’s experiments shows that a particle’s wave or particle nature is most likely undefined until a measurement is made.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/mPjsL1YzcxA/do-atoms-going-through-a-double-slit-know-if-they-are-being-observed){:target="_blank" rel="noopener"}


