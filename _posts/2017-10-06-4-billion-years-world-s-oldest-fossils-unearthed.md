---
layout: post
title: "4 billion years: World's oldest fossils unearthed"
date: 2017-10-06
categories:
author: "University College London"
tags: [Life,Earliest known life forms,Hydrothermal vent,Nuvvuagittuq Greenstone Belt,Earth,Earth sciences,Nature]
---


Haematite tubes from the NSB hydrothermal vent deposits that represent the oldest microfossils and evidence for life on Earth. Credit: Matthew Dodd  Remains of microorganisms at least 3,770 million years old have been discovered by an international team led by UCL scientists, providing direct evidence of one of the oldest life forms on Earth. Published today in Nature and funded by UCL, NASA, Carnegie of Canada and the UK Engineering and Physical Sciences Research Council, the study describes the discovery and the detailed analysis of the remains undertaken by the team from UCL, the Geological Survey of Norway, US Geological Survey, The University of Western Australia, the University of Ottawa and the University of Leeds. They also found that the mineralised fossils are associated with spheroidal structures that usually contain fossils in younger rocks, suggesting that the haematite most likely formed when bacteria that oxidised iron for energy were fossilised in the rock. The fact we unearthed them from one of the oldest known rock formations, suggests we've found direct evidence of one of Earth's oldest life forms.

<hr>

[Visit Link](https://phys.org/news/2017-03-world-oldest-fossils-unearthed.html){:target="_blank" rel="noopener"}


