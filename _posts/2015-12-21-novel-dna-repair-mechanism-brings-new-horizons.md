---
layout: post
title: "Novel DNA repair mechanism brings new horizons"
date: 2015-12-21
categories:
author: Lomonosov Moscow State University
tags: [DNA,DNA repair,RNA,Protein biosynthesis,Nucleosome,Transcription (biology),Cell biology,Structural biology,Molecular biophysics,Nucleic acids,Macromolecules,Branches of genetics,Chemistry,Biomolecules,Molecular genetics,Nucleotides,Life sciences,Cellular processes,Biology,Biotechnology,Natural products,Biological processes,Organic acids,Genetics,Biochemistry,Molecular biology]
---


A group of researchers, lead by Vasily M. Studitsky, professor at the Lomonosov Moscow State University, discovered a new mechanism of DNA repair, which opens up new perspectives for the treatment and prevention of neurodegenerative diseases. Therefore, when it is necessary to synthesize specific protein, small region of DNA is unwound, the two strands are disconnected, and the information on the protein structure with one of the DNA strands is written in form of RNA, single-stranded molecule. During the transcription of information (its rewriting into RNA) the RNA polymerase enzyme rides on the DNA chain, and stops when it finds the break. According to our hypothesis, it occurs due to the formation of special small DNA loops in the nucleosome, although normally DNA wounds around the histone spool very tightly, -- says Vasily M. Studitsky, -- The loops form when the DNA is coiled back on nucleosome together with polymerase. It turned out that only in nucleosomes, rather than in the histone-free DNA, the enzyme stopped, when the break was present in the other DNA strand.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/lmsu-ndr062615.php){:target="_blank" rel="noopener"}


