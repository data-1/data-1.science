---
layout: post
title: "Why we did not evolve to live forever: Unveiling the mystery of why we age"
date: 2017-09-16
categories:
author: "Johannes Gutenberg Universitaet Mainz"
tags: [Evolution,Ageing,Caenorhabditis elegans,Natural selection,Gene,Mutation,Autophagy,Longevity,Antagonistic pleiotropy hypothesis,Evolutionary biology,Life sciences,Biological evolution,Genetics,Nature,Biology]
---


They have identified that genes belonging to a process called autophagy -- one of the cells most critical survival processes -- promote health and fitness in young worms but drive the process of ageing later in life. This research published in the journal Genes & Development gives some of the first clear evidence for how the ageing process arises as a quirk of evolution. This evidence has now arrived according to the co-lead author of the paper Jonathan Byrne, The evolutionary theory of ageing just explains everything so nicely but it lacked real evidence that it was happening in nature. Jonathan continues These AP genes haven't been found before because it's incredibly difficult to work with already old animals, we were the first to figure out how to do this on a large scale. Previous studies had found genes that encourage ageing while still being essential for development, but these 30 genes represent some of the first found promoting ageing specifically only in old worms.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/09/170915144151.htm){:target="_blank" rel="noopener"}


