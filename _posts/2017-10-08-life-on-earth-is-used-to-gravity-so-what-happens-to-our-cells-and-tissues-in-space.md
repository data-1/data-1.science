---
layout: post
title: "Life on Earth is used to gravity – so what happens to our cells and tissues in space?"
date: 2017-10-08
categories:
author: "Andy Tay, The Conversation"
tags: [Gravity,Sense,Micro-g environment,Cell (biology),Force,Weightlessness,Immune system]
---


Now with the prospect of longer space missions, researchers are working to figure out what a lack of gravity means for our physiology – and how to make up for it. Freed from gravity's grip  It wasn't until explorers traveled to space that any earthly creature had spent time in a microgravity environment. These channels are pores on the cell membrane that let particular charged molecules pass in or out of the cell depending on the forces they detect. An example of this kind of mechano-receptor is the PIEZO ion channel, found in almost all cells. For instance, a pinch on the arm would activate a PIEZO ion channel in a sensory neuron, telling it to open the gates.

<hr>

[Visit Link](https://phys.org/news/2017-03-life-earth-gravity-cells-tissues.html){:target="_blank" rel="noopener"}


