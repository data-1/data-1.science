---
layout: post
title: "Icarus lifts off"
date: 2017-10-17
categories:
author: "Max Planck Society"
tags: [International Space Station,Technology,Computing]
---


In future, it will be responsible for decoding the signals received by the Icarus antenna and separating the different data streams from each other. Credit: dpa  After the launch of a Soyuz 2 rocket scheduled for October 12 was postponed by two days, the carrier rocket yesterday docked with the ISS. The central computer unit of Icarus will be installed in the pressurized cabin of the ISS international space station's Russian module. In future, it will be responsible for decoding the signals received by the Icarus antenna and separating the different data streams from each other. A new era in behaviour research is dawning with the Icarus global animal observation system of Martin Wikelski from the Max Planck Institute for Ornithology.

<hr>

[Visit Link](https://phys.org/news/2017-10-icarus.html){:target="_blank" rel="noopener"}


