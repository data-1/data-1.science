---
layout: post
title: "Geoengineering is an essential bridge to true sustainability"
date: 2017-08-31
categories:
author: "Bjørn Lomborg, Director, Copenhagen Consensus Centre, Monday, July"
tags: [Climate engineering,Climate change,Marine cloud brightening,Renewable energy,Change,Nature,Earth sciences,Natural environment,Global environmental issues,Environmental issues with fossil fuels,Societal collapse,Environmental impact,Climate variability and change]
---


Much more research funding is needed, but such innovation will take time. This year, for the first time, the US Government office that oversees federally funded climate studies is formally recommending geoengineering research. Geoengineering promises to be exceptionally cheap, making it much more likely than expensive carbon cuts to be implemented  Last year, 11 climate scientists declared that the Paris Agreement had actually set back the fight against climate change, saying: “Our backs are against the wall and we must now start the process of preparing for geoengineering.” The crucial benefit of investigating geoengineering is that it offers the only way to reduce the global temperature quickly. Any standard fossil fuel reduction policy will take decades to implement, and a half-century to have any noticeable climate impact. Marine cloud whitening, for example, amplifies a natural process and would not lead to permanent atmospheric changes – switching off the entire process would return the world to its previous state in a matter of days.

<hr>

[Visit Link](https://www.theneweconomy.com/energy/geoengineering-is-an-essential-bridge-to-true-sustainability){:target="_blank" rel="noopener"}


