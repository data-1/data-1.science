---
layout: post
title: "Quantum mechanics inside Earth's core"
date: 2017-09-20
categories:
author: "Julius-Maximilians-Universität Würzburg"
tags: [Atom,Metal,Iron,Planetary core,Crystal structure,Electronic band structure,Electron,Temperature,Nickel,Earths outer core,Earth,Magnetism,Quantum mechanics,Magnetic field,Solid,Physics,Dynamo theory,Geophysics,Earths magnetic field,Mechanics,Earths inner core,Chemistry,Physical sciences,Applied and interdisciplinary physics,Nature]
---


Flowing of liquid metal in the outer core can intensify electric currents and create Earth's magnetic field – at least according to the common geodynamo theory. Band-structure induced correlation effects  This is because at room temperature iron differs significantly from common metals such as copper or gold due to its strong effective electron-electron interaction. Nickel is also a strongly correlated metal. They keep this layout even when temperature and pressure become very large, Hausoel explains. And what's the Earth's core got to do with this?

<hr>

[Visit Link](https://phys.org/news/2017-07-quantum-mechanics-earth-core.html){:target="_blank" rel="noopener"}


