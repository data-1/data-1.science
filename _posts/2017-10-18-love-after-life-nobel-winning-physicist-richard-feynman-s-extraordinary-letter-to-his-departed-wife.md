---
layout: post
title: "Love After Life: Nobel-Winning Physicist Richard Feynman’s Extraordinary Letter to His Departed Wife"
date: 2017-10-18
categories:
author: Maria Popova
tags: [Richard Feynman,Tuberculosis,Science]
---


In early 1945, two and a half years into their marriage, Richard and Arline made love for the first time. I want to tell you I love you. I want to love you. But you can’t help it, darling, nor can I — I don’t understand it, for I have met many girls and very nice ones and I don’t want to remain alone — but in two or three meetings they all seem ashes. I love my wife.

<hr>

[Visit Link](https://www.brainpickings.org/2017/10/17/richard-feynman-arline-letter/){:target="_blank" rel="noopener"}


