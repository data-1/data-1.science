---
layout: post
title: "Chemists develop a promising drug synthesis method"
date: 2017-10-18
categories:
author: Rudn University
tags: [Chemical reaction,Sodium hydroxide,Chemistry,Physical sciences,Chemical substances]
---


The results of the work are published in Tetrahedron Letters. Our latest development simplifies and reduces the cost of one of the stages in active ingredients production, says Anton Shetnev, one of the authors of the study from the Research Institute of Chemistry of RUDN. The usual method of production of 1,2,4-oxadiazoles is carried out in two stages. The authors of the study offered a method of 1,2,4-oxadiazole production in a single step at room temperature. We have published a series of articles devoted to the application of this methodology, and we are planning to continue the research.

<hr>

[Visit Link](https://phys.org/news/2017-10-chemists-drug-synthesis-method.html){:target="_blank" rel="noopener"}


