---
layout: post
title: "Same forces as today caused climate changes 1.4 billion years ago"
date: 2015-07-05
categories:
author: University of Southern Denmark 
tags: [Earth,Ice age,Milankovitch cycles,Climate variability and change,Science,Applied and interdisciplinary physics,Planetary science,Physical sciences,Climate,Natural environment,Geology,Physical geography,Nature,Earth sciences]
---


Now researchers have found geological evidence that some of the same forces as today were at play 1.4 billion years ago. Now researchers from University of Southern Denmark, China National Petroleum Corporation and others have looked deep into Earth's history and can reveal that orbital forcing of climate change contributed to shaping the Earth's climate 1.4 billion years ago. Changes in wind patterns and ocean circulations  The sediments in the Xiamaling Formation have preserved evidence of repeated climate fluctuations, reflecting apparent changes in wind patterns and ocean circulation that indicates orbital forcing of climate change. Over the last one million years these cycles have caused ice ages every 100,000 years, and right now we are in the middle of a warming period that has so far lasted 11,000 years. With this research we can show that cycles like the Milankovich cycles were at play 1.4 billion years ago - a period, we know only very little about, says Donald Canfield, adding:  This research will also help us understand how Milankovitch cyclicity ultimately controls climate change on Earth.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/uosd-sfa031015.php){:target="_blank" rel="noopener"}


