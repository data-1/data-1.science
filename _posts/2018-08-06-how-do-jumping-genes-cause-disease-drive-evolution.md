---
layout: post
title: "How do jumping genes cause disease, drive evolution?"
date: 2018-08-06
categories:
author: "Carnegie Institution for Science"
tags: [Transposable element,Gene,Piwi-interacting RNA,Genome,Molecular genetics,Biological processes,Branches of genetics,Biochemistry,Molecular biology,Biotechnology,Life sciences,Biology,Genetics]
---


These jumping genes use nurse cells to produce invasive material (copies of themselves called virus-like particles) that move into a nearby egg and then mobilize into the egg's DNA. However, tracking the mobilization of jumping genes to understand their tactics has been a daunting task. Carnegie co-author Zhao Zhang explained: We were very surprised that the these jumping genes barely moved in stem cells that produce developing egg cells, possibly because the stem cells would only have two copies of the genome for these jumping genes to use. So the Zhang group's findings are likely to be important for understanding mammalian evolution and disease as well, commented Allan Spradling, who is a pioneer researcher on studying the egg development in both fruit fly and mammals and a longtime scientist at Carnegie's Department of Embryology. The Carnegie Institution for Science (carnegiescience.edu) is a private, nonprofit organization headquartered in Washington, D.C., with six research departments throughout the U.S.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/cifs-hdj072018.php){:target="_blank" rel="noopener"}


