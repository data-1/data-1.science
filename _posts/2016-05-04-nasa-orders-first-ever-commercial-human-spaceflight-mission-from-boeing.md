---
layout: post
title: "NASA orders first ever commercial human spaceflight mission from Boeing"
date: 2016-05-04
categories:
author: Ken Kremer
tags: [Commercial Crew Program,Boeing Starliner,NASA,SpaceX Dragon,SpaceX Dragon 2,Astronautics,American spacecraft,Transport,Human spaceflight programs,Space access,Space-based economy,Space organizations,Space exploration,Rocketry,Space science,Crewed spacecraft,Spaceflight technology,Spaceflight,Outer space,Flight,Human spaceflight,Space programs,Spacecraft,Space vehicles,Space program of the United States,Aerospace,Space industry,Life in space]
---


Boeing was awarded the first service flight of the CST-100 crew capsule to the International Space Station as part of the Commercial Crew Transportation Capability agreement with NASA in this artists concept. NASA's Commercial Crew Program (CCP) office gave the first commercial crew rotation mission award to the Boeing Company to launch its CST-100 astronaut crew capsule to the ISS by late 2017, so long as the company satisfactorily meets all of NASA's human spaceflight certification milestones. Boeing was awarded a $4.2 Billion contract in September 2014 by NASA Administrator Charles Bolden to complete development and manufacture of the CST-100 'space taxi' under the agency's Commercial Crew Transportation Capability (CCtCap) program and NASA's Launch America initiative. Credit: Ken Kremer – kenkremer.com  Orders under the CCtCap contracts are made two to three years prior to the missions to provide time for each company to manufacture and assemble the launch vehicle and spacecraft. Explore further Boeing completes first milestone for NASA's commercial crew transportation systems

<hr>

[Visit Link](http://phys.org/news352110672.html){:target="_blank" rel="noopener"}


