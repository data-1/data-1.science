---
layout: post
title: "Louis Taillefer wins Kamerlingh Onnes prize for superconductivity research"
date: 2018-07-28
categories:
author: "CIFAR"
tags: [Superconductivity,Heike Kamerlingh Onnes,Chemistry,Physics,Applied and interdisciplinary physics,Physical sciences,Science,Condensed matter physics,Quantum mechanics,Materials science,Condensed matter,Phases of matter,Materials,Theoretical physics,Electromagnetism,Physical chemistry]
---


Canadian physicist Louis Taillefer, Director of CIFAR's Quantum Materials program, will be awarded the 2018 Kamerlingh Onnes prize. Superconductivity is a phenomenon where materials have the ability to conduct electricity without any loss of energy under specific conditions. Taillefer has received numerous awards and honours for his contributions to science. ###  The Kammerlingh Onnes Prize was established in 2000 by the organizers of the International Conference on the Materials and Mechanisms of Superconductivity. Taillefer and Matsuda will be honoured on Aug. 21, 2018 during the 12th International Conference on Materials and Mechanisms of Superconductivity in Beijing, China.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/cifa-ltw072018.php){:target="_blank" rel="noopener"}


