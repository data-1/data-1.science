---
layout: post
title: "Faster genome evolution methods to transform yeast for industrial biotechnology"
date: 2018-05-26
categories:
author: "Chinese Academy of Sciences Headquarters"
tags: [Yeast,Biotechnology,Evolution,Genome,Saccharomyces cerevisiae,Molecular biology,Biochemistry,Genetics,Life sciences,Biology]
---


Scientists have created a new way of speeding up the genome evolution of baker's yeast Saccharomyces cerevisiae, the same yeast people use for bread and beer production, to develop a synthetic yeast strain that can be transformed on demand for use in various industrial applications. A research team led by Prof. DAI Junbiao at the Shenzhen Institutes of Advanced Technology (SIAT) of the Chinese Academy of Sciences, in collaboration with Prof. Patrick Cai from the Manchester Institute of Biotechnology, developed a rapid, efficient and universal way of transforming yeast at the molecular level using a method called SCRaMbLE (Synthetic Chromosome Rearrangement and Modification by LoxP-mediated Evolution). Yeast is a very well understood organism and in a biological sense humans and yeast share a number of similarities in their genetic makeup. Therefore, re-building the yeast genome from the ground up helps us better understand the basis of human life. This makes it particularly attractive for industrial biotechnology applications, such as the production of advanced medicines.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/caos-fge052518.php){:target="_blank" rel="noopener"}


