---
layout: post
title: "Frequency of tornadoes, hail linked to El Nino, La Nina"
date: 2015-07-09
categories:
author: Columbia University
tags: [El NioSouthern Oscillation,La Nia,Tornado,Climate,Physical geography,Meteorology,Earth sciences,Earth phenomena,Weather,Atmospheric sciences,Branches of meteorology,Oceanography,Natural hazards,Meteorological phenomena,Applied and interdisciplinary physics,Natural environment,Nature,Weather hazards,Atmosphere]
---


A new study links the frequency of tornadoes and hailstorms in parts of the southern United States to ENSO, a cyclic temperature pattern in the Pacific Ocean. Now, a new study shows that El Niño and La Niña conditions can also help predict the frequency of tornadoes and hail storms in some of the most susceptible regions of the United States. Credit: Allen et al., Nature Geoscience, 2015  Last year, 47 people died in tornadoes. The scientists then verified the indices using available observational records. Second, the current study shows robust correlation only in the southern states, where the ENSO signal is especially clear.

<hr>

[Visit Link](http://phys.org/news345718027.html){:target="_blank" rel="noopener"}


