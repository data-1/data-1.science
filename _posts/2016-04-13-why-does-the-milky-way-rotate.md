---
layout: post
title: "Why does the Milky Way rotate?"
date: 2016-04-13
categories:
author: Elizabeth Howell
tags: [Milky Way,Galaxy,Star,Star cluster,Astronomy,Spiral galaxy,Radio astronomy,European Southern Observatory,Sun,Astronomical objects,Stellar astronomy,Space science,Outer space,Sky,Physical sciences]
---


Credit: NASA  We live in a galaxy that is called the Milky Way. It's spinning at 270 kilometers per second (168 miles per second) and takes about 200 million years to complete one rotation, according to the National Radio Astronomy Observatory. In the denser areas, gas clumped together in protogalactic clouds; the thickest areas collapsed into stars. These stars burned out quickly and became globular clusters, but gravity continued to collapse the clouds, How Stuff Works wrote. The star cluster, known as NGC 3293, would have been just a cloud of gas and dust itself about ten million years ago, but as stars began to form it became the bright group we see here.

<hr>

[Visit Link](http://phys.org/news342866992.html){:target="_blank" rel="noopener"}


