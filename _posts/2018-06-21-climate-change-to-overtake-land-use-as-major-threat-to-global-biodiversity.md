---
layout: post
title: "Climate change to overtake land use as major threat to global biodiversity"
date: 2018-06-21
categories:
author: "University College London"
tags: [Biodiversity,Ecology,Ecosystem,Climate change,Human impact on the environment,Natural environment,Biogeochemistry,Physical geography,Global environmental issues,Environmental science,Earth phenomena,Earth sciences,Nature,Environmental social science,Systems ecology]
---


Previous studies have suggested that ecosystem function is substantially impaired where more than 20 per cent of species are lost; this is estimated to have occurred across over a quarter of the world's surface, rising to nearly two thirds when roads are taken into account. The findings suggest that efforts to minimise human impact on global biodiversity should now take both land use and climate change into account instead of just focusing on one over the other, as the combined effects are expected to have significant negative effects on the global ecosystem. Study author, Dr Tim Newbold (UCL Genetics, Evolution & Environment), said: This is the first piece of research looking at the combined effects of future climate and land use change on local vertebrate biodiversity across the whole of the land surface, which is essential when considering how to minimise human impact on the local environment at a global scale. Furthermore, when combined with land use, vertebrate community diversity is predicted to have decreased substantially by 2070, with species potentially declining by between 20 and nearly 40 per cent. Temperate regions, which have been the most affected by land use, stand to see relatively small biodiversity changes from future climate change, while tropical grasslands and savannahs are expected to see strong losses as a result of both climate change and land use.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180619230846.htm){:target="_blank" rel="noopener"}


