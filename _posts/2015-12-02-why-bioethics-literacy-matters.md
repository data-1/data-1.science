---
layout: post
title: "Why bioethics literacy matters"
date: 2015-12-02
categories:
author: The Hastings Center 
tags: [Bioethics,The Hastings Center,Ethics,Health,Nursing,Health care,Medical ethics,Physician,Medicine,Education,Health sciences]
---


The Hastings Center and the Presidential Commission for the Study of Bioethical Issues collaborate to publish a collection of papers on bioethics education, citing high stakes of success for health care and research  From accessible and affordable health care to reproductive technologies, the justice and well-being of our society depend on the ability of people to identify key issues, articulate their values and concerns, deliberate openly and respectfully, and find the most defensible ways forward. The Hastings Center and the Presidential Commission for the Study of Bioethical Issues have teamed up to publish a series of essays to highlight the best practices in teaching bioethics and to identify gaps in our knowledge of how best to inspire and increase moral understanding, analytical thinking in the moral domain, and professional integrity. The Presidential Commission for the Study of Bioethical Issues and The Hastings Center are committed to improving the ethical literacy of the American public in the domain of bioethics, states an introduction to the essays written by Lisa M. Lee, executive director of the Presidential commission; Mildred Z. Solomon, president of The Hastings Center; and Amy Gutmann, chair of the commission. The three published essays discuss bioethics education for nurses, doctors, and medical students. discusses an empirical study of educators involved in graduate-level training and physicians who deliver reproductive health care.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/thc-wbl091714.php){:target="_blank" rel="noopener"}


