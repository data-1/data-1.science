---
layout: post
title: "An avocado a day keeps the cardiologist away"
date: 2016-04-12
categories:
author: Penn State
tags: [Fat,Low-density lipoprotein,Cholesterol,Health promotion,Food and drink,Health sciences,Determinants of health,Self-care,Health,Nutrition]
---


Previous studies have suggested that avocados are a cholesterol-lowering food, but this is the first study -- to the researchers' knowledge -- to look at health implications of avocados beyond monounsaturated fatty acids. Kris-Etherton and colleagues tested three different diets, all designed to lower cholesterol: a lower-fat diet, consisting of 24 percent fat, and two moderate fat diets, with 34 percent fat. The moderate fat diets were nearly identical, however one diet incorporated one Hass avocado every day while the other used a comparable amount of high oleic acid oils -- such as olive oil -- to match the fatty acid content of one avocado. Compared to the participants' baseline measurements, all three diets significantly lowered LDL -- also known as bad cholesterol -- as well as total cholesterol. However, participants experienced an even greater reduction in LDL and total cholesterol while on the avocado diet, compared to the other two diets, the researchers report today (Jan. 7) in the Journal of the American Heart Association.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/ps-aaa010715.php){:target="_blank" rel="noopener"}


