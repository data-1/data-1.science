---
layout: post
title: "Improving stroke treatment through machine learning"
date: 2018-07-14
categories:
author: "University of Heidelberg"
tags: [Optogenetics,Brain,News aggregator,Visual perception,Neuroscience,Cognitive science,Branches of science,Cognition]
---


Methods from optogenetics and machine learning should help improve treatment options for stroke patients. Researchers from Heidelberg University have developed a computer vision technique to analyse the changes in motor skills that result from targeted stimulation of healthy areas of the brain. Movements recorded with a video camera are automatically analysed to monitor the rehabilitation process and evaluate and adjust the optogenetic stimulation. Researchers from the Interdisciplinary Center for Scientific Computing (IWR) in Heidelberg worked with neurobiologists from Switzerland to develop the method. According to lead author Dr Dr Anna-Sophia Wahl, a neuroscientist at the Swiss Federal Institute of Technology (ETH) in Zurich, neurorehabilitation is the only treatment option for the majority of stroke victims.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/01/180116123820.htm){:target="_blank" rel="noopener"}


