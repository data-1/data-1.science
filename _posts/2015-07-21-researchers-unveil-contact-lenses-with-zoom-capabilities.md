---
layout: post
title: "Researchers unveil contact lenses with zoom capabilities"
date: 2015-07-21
categories:
author: ""   
tags: [Lens,Zoom lens,Glasses,Technology,Electromagnetic radiation,Optics]
---


The contact lens is really two lenses in one -- one lens for seeing the world as it is, another to magnify one's surroundings by a factor of three. A single wink (the blink of one eye), will trigger the switch back and forth between the two lenses. A wink triggers the glasses to filter and concentrate light through the telescopic portion of the contact lenses, enabling instant magnification of what lies in the forefront of the user's field of vision. So what people usually use are head-mounted telescopes, which doesn't work for everything. The work of Tremblay and his colleagues was made possible by a grant from DARPA, the Defense Advanced Research Projects Agency.

<hr>

[Visit Link](http://www.spacedaily.com/reports/Researchers_unveil_contact_lenses_with_zoom_capabilities_999.html){:target="_blank" rel="noopener"}


