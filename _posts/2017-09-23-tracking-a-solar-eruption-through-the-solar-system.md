---
layout: post
title: "Tracking a solar eruption through the Solar System"
date: 2017-09-23
categories:
author: ""
tags: [Coronal mass ejection,Mars,Sun,Sky,Outer space,Solar System,Astronomy,Space science,Science,Bodies of the Solar System,Astronautics,Astronomical objects,Planetary science,Astronomical objects known since antiquity,Planets,Spaceflight,Planets of the Solar System,Local Interstellar Cloud,Physical sciences]
---


Science & Exploration Tracking a solar eruption through the Solar System 15/08/2017 15951 views 182 likes  Ten spacecraft, from ESA’s Venus Express to NASA’s Voyager-2, felt the effect of a solar eruption as it washed through the Solar System while three other satellites watched, providing a unique perspective on this space weather event. Although Earth itself was not in the firing line, a number of Sun-watching satellites near Earth – ESA’s Proba-2, the ESA/NASA SOHO and NASA’s Solar Dynamics Observatory – had witnessed a powerful solar eruption a few days earlier, on 14 October. In the firing line Thanks to the fortuitous locations of other satellites lying in the direction of the CME’s travel, unambiguous detections were made by three Mars orbiters – ESA’s Mars Express, NASA’s Maven and Mars Odyssey – and NASA’s Curiosity Rover operating on the Red Planet’s surface, ESA’s Rosetta at Comet 67P/Churyumov–Gerasimenko, and the international Cassini mission at Saturn. Cosmic ray drop A drop of about 20% in cosmic rays was observed at Mars – one of the deepest recorded at the Red Planet – and persisted for about 35 hours. “Finally, coming back to our original intended observation of the passage of Comet Siding Spring at Mars, the results show the importance of having a space weather context for understanding how these solar events might influence or even mask the comet’s signature in a planet’s atmosphere.”  Notes for Editors “Interplanetary coronal mass ejection observed at Stereo-A, Mars, comet 67P/Churyumov–Gerasimenko, Saturn and New Horizons en route to Pluto.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Tracking_a_solar_eruption_through_the_Solar_System){:target="_blank" rel="noopener"}


