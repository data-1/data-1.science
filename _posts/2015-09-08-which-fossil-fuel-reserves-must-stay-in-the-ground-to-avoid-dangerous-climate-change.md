---
layout: post
title: "Which fossil fuel reserves must stay in the ground to avoid dangerous climate change?"
date: 2015-09-08
categories:
author: University College London 
tags: [Petroleum,Climate change,Low-carbon economy,Energy development,Sustainable energy,Coal,Climate variability and change,Natural environment,Energy,Nature,Environmental issues with fossil fuels,Global environmental issues,Environmental impact,Societal collapse,Environment,Fossil fuels,Economy and the environment,Change]
---


A third of oil reserves, half of gas reserves and over 80% of current coal reserves globally should remain in the ground and not be used before 2050 if global warming is to stay below the 2°C target agreed by policy makers, according to new research by the UCL Institute for Sustainable Resources. For the study, the scientists first developed an innovative method for estimating the quantities, locations and nature of the world's oil, gas and coal reserves and resources. They then used an integrated assessment model to explore which of these, along with low-carbon energy sources, should be used up to 2050 to meet the world's energy needs. Lead author Dr Christophe McGlade, Research Associate at the UCL Institute for Sustainable Resources said: We've now got tangible figures of the quantities and locations of fossil fuels that should remain unused in trying to keep within the 2°C temperature limit. The greater global attention to climate policy also means that fossil fuel companies are becoming increasingly risky for investors in terms of the delivery of long-term returns.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/ucl-wff010615.php){:target="_blank" rel="noopener"}


