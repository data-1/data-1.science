---
layout: post
title: "ESO telescopes observe first light from gravitational wave source"
date: 2017-10-17
categories:
author: "ESO"
tags: [European Southern Observatory,Very Large Telescope,LIGO,GW170817,Kilonova,VISTA (telescope),Gravitational wave,Paranal Observatory,Telescope,Observatory,Fermi Gamma-ray Space Telescope,Optical spectrometer,Space science,Telescopes,Astronomical observatories,Physical sciences,Observational astronomy,Science,Scientific observation,Astronomy,Astronomical objects,Optics]
---


For the first time ever, astronomers have observed both gravitational waves and light (electromagnetic radiation) from the same event, thanks to a global collaborative effort and the quick reactions of both ESO's facilities and others around the world. About 70 observatories around the world also observed the event, including the NASA/ESA Hubble Space Telescope. [4] The comparatively small distance between Earth and the neutron star merger, 130 million light-years, made the observations possible, since merging neutron stars create weaker gravitational waves than merging black holes, which were the likely case of the first four gravitational wave detections. The extensive list of team members is available in this PDF file: https://www.eso.org/public/archives/releases/pdf/eso1733a.pdf  ESO is the foremost intergovernmental astronomy organisation in Europe and the world's most productive ground-based astronomical observatory by far. At Paranal, ESO operates the Very Large Telescope and its world-leading Very Large Telescope Interferometer as well as two survey telescopes, VISTA working in the infrared and the visible-light VLT Survey Telescope.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/e-eto101617.php){:target="_blank" rel="noopener"}


