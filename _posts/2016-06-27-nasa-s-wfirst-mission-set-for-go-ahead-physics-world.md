---
layout: post
title: "NASA's WFIRST mission set for go-ahead – Physics World"
date: 2016-06-27
categories:
author: ""
tags: [Nancy Grace Roman Space Telescope,Astronomy,Space science,Science,Outer space,Spaceflight,Physical sciences]
---


One for the future: NASA's Wide-Field Infrared Survey Telescope will aim to answer fundamental questions about the universe, dark energy and exoplanets. (Courtesy: NASA)  NASA’s Wide-Field Infrared Survey Telescope (WFIRST) is expected to become a formal project next month, about a year earlier than initially planned. With an estimated cost of $1.6bn, WFIRST is expected to launch in the next decade, and was listed as the top priority for NASA in its 2010 decadal survey carried out by the National Research Council. The probe is seen as a successor to the planned James Webb Space Telescope, which is set to launch in 2018. It will also aim to directly image exoplanets and take spectroscopic measurements of their atmospheres.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2016/jan/22/nasas-wfirst-mission-set-for-go-ahead){:target="_blank" rel="noopener"}


