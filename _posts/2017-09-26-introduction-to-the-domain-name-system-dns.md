---
layout: post
title: "Introduction to the Domain Name System (DNS)"
date: 2017-09-26
categories:
author: "David Both
(Correspondent)"
tags: [Name server,Domain Name System,Root name server,Hostname,Domain name,MX record,Fully qualified domain name,Zone file,Reverse DNS lookup,CNAME record,Localhost,World Wide Web,Directories,Information technology management,Information technology governance,Computer architecture,Network protocols,IT infrastructure,Internet architecture,Communications protocols,Internet Standards,Internet,Internet governance,Computer networking,Network addressing,Computing,Network architecture,Protocols,Internet protocols,Application layer protocols,Cyberspace,Technology,Networking standards,Network layer protocols,Wide area networks]
---


The local name server is configured to use the root name servers so the root name server for the .com top-level domain returns the IP Address of the authoritative name server for www.opensource.com. In other cases, the root servers provide the IP addresses of the authoritative server for the desired domain. My browser makes the request of the local name server, which does not contain that IP address. Of course, the local name server must know how to locate the root name servers so it uses the /var/named/named.ca file, which contains the names and IP addresses of the root name servers. A records are the primary name resolver records and there must be an A record, which contains the IP address for each host.

<hr>

[Visit Link](https://opensource.com/article/17/4/introduction-domain-name-system-dns){:target="_blank" rel="noopener"}


