---
layout: post
title: "How to build your own Git server"
date: 2017-09-24
categories:
author: "Seth Kenlon
(Team, Red Hat)"
tags: [Git,Scripting language,Bash (Unix shell),Public-key cryptography,Server (computing),Version control,Repository (version control),Superuser,Computer file,Information technology management,Computer programming,Computers,Computer engineering,Computer architecture,System software,Computer science,Software development,Software,Software engineering,Technology,Computing]
---


If you know how to use Git and SSH, then you already know how to create a Git server. The way Git is designed, the moment you create or clone a repository, you have already set up half the server. Assuming that you have a server available (if not, that's not exactly a problem Git can help with, but CentOS on a Raspberry Pi 3 is a good start), then the first step is to enable SSH logins using only SSH key authorization. However, you don't really want to give your developers access to your server, even if only as gituser. endif  Make the script executable:  $ chmod +x ~ / jupiter / .git / hooks / post-receive  Now when a user commits to the server's master branch, the code is copied to an in-production directory, a commit to the dev branch get copied someplace else, and any other branch triggers no action.

<hr>

[Visit Link](https://opensource.com/life/16/8/how-construct-your-own-git-server-part-6){:target="_blank" rel="noopener"}


