---
layout: post
title: "Deep learning: A superhuman way to look at cells"
date: 2018-07-18
categories:
author: "Gladstone Institutes"
tags: [Artificial intelligence,Deep learning,Machine learning,Artificial neural network,Gladstone Institutes,Science,Technology,Cognitive science,Branches of science,Neuroscience]
---


Steven Finkbeiner, MD, PhD, a director and senior investigator at the Gladstone Institutes, teamed up with computer scientists at Google. Using artificial intelligence approaches, they discovered that by training a computer, they could give scientists a way to surpass regular human performance. Over time, they developed methods that add fluorescent labels to cells in order to see features the human eye normally can't see. We trained the neural network by showing it two sets of matching images of the same cells; one unlabeled and one with fluorescent labels, explained Christiansen, software engineer at Google Accelerated Science. The deep network can identify whether a cell is alive or dead, and get the answer right 98 percent of the time.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-04/gi-dla041018.php){:target="_blank" rel="noopener"}


