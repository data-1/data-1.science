---
layout: post
title: "Mars navigation"
date: 2016-07-22
categories:
author: ""
tags: [Trace Gas Orbiter,Quasar,ExoMars,Delta-DOR,Astronautics,Astronomy,Space science,Outer space,Physical sciences,Spaceflight,Astronomical objects,Sky,Solar System,Flight,Planetary science,Science,Astronomical objects known since antiquity]
---


To achieve this amazing level of accuracy, ESA experts are making use of ‘quasars’ – the most luminous objects in the Universe – as ‘calibrators’ in a technique known as Delta-Differential One-Way Ranging, or delta-DOR. In the delta-DOR technique, radio signals from ExoMars/TGO are being received by two widely separated deep-space ground stations, one, say, at New Norcia, Western Australia, and one at Cebreros, Spain, and the difference in the times of signal arrival is precisely measured. On Wednesday this week, ESA ground stations began the first of many delta-DOR observations that will be used to precisely locate ExoMars/TGO, using quasar P1514-24, seen inset in an image of ESA's deep-space tracking station at Malargüe, Argentina, above. “In October, in the final critical week before Mars arrival, teams will be conducting two delta-DOR observations daily,” says Mattia Mercolino, responsible for delta-DOR activities at ESOC, ESA’s operations centre in Darmstadt, Germany. “The current set of delta-DOR observations will enable us to locate the spacecraft to less than 1000 m when it’s near Mars, a distance of slightly more than 150 million km from Earth,” says Mattia.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2016/07/Mars_navigation){:target="_blank" rel="noopener"}


