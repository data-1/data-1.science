---
layout: post
title: "New insights into underlying cellular mechanisms of information processing in the brain"
date: 2016-04-13
categories:
author: Max Planck Florida Institute for Neuroscience
tags: [Synaptic vesicle,Synapse,Neuron,Neurotransmitter,Chemical synapse,Neurotransmission,Neural circuit,Neuroscience,Physiology,Neurophysiology]
---


The continuous release of neurotransmitters is essential to maintain communication between neurons. Synaptic vesicles in cell-to-cell communication  While synapses contain hundreds to thousands of synaptic vesicles, when a specific signal is received by the neuron, only a fraction of synaptic vesicles that exist in what is called the readily releasable pool are discharged from one neuron to another. What was not known was the cellular mechanisms that regulate the release of synaptic vesicles from the readily releasable pool to support the early stages of auditory processing. This study identifies the critical mechanism is the regulation of Ca2+ influx through voltage gated calcium channels and release of neurotransmitters by synaptic vesicles. These findings are important for understanding the mechanisms of synaptic transmission, specifically for neuronal circuits that rely on fast, continuous synaptic transmission.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/mpfi-nii021815.php){:target="_blank" rel="noopener"}


