---
layout: post
title: "What Is Ultraviolet Light?"
date: 2017-10-08
categories:
author: "Jim Lucas"
tags: [Ultraviolet,Electromagnetic radiation,Electromagnetic spectrum,Fluorescence,Sunburn,Sun tanning,Blacklight,Light,Physical sciences,Chemistry,Electromagnetism,Radiation,Physical phenomena,Electrodynamics,Waves]
---


This can be useful for chemical processing, or it can be damaging to materials and living tissues. This ultraviolet radiation is invisible but contains more energy than the visible light emitted. The energy from the ultraviolet light is absorbed by the fluorescent coating inside the fluorescent lamp and re-emitted as visible light. Because Earth's atmosphere blocks much of this UV radiation, particularly at shorter wavelengths, observations are conducted using high-altitude balloons and orbiting telescopes equipped with specialized imaging sensors and filters for observing in the UV region of the EM spectrum. It may seem counterintuitive to treat skin cancer with the same thing that caused it, but PUVA can be useful due to UV light’s effect on the production of skin cells.

<hr>

[Visit Link](https://www.livescience.com/50326-what-is-ultraviolet-light.html){:target="_blank" rel="noopener"}


