---
layout: post
title: "NIST's super quantum simulator 'entangles' hundreds of ions"
date: 2016-06-12
categories:
author: "National Institute of Standards and Technology (NIST)"
tags: [Quantum simulator,Quantum mechanics,Quantum entanglement,Trapped ion quantum computer,Science,Theoretical physics,Applied and interdisciplinary physics,Physical sciences,Physics]
---


The new NIST system can generate quantum entanglement in about 10 times as many ions as any previous simulators based on ions, a scale-up that is crucial for practical applications. Improving the control also allows us to more perfectly mimic the system we want our simulator to tell us about. When measured, unentangled ions collapse from a superposition to a simple spin state, creating noise, or random fluctuations, in the measured results. The reduction in the quantum noise is what makes this form of entanglement useful for enhancing ion and atomic clocks, Bohnet said. Quantum spin dynamics and entanglement generation with hundreds of trapped ions.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/nios-nsq060116.php){:target="_blank" rel="noopener"}


