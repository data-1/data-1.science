---
layout: post
title: "DNA-guided 3-D printing of human tissue is unveiled"
date: 2015-12-21
categories:
author: University of California - San Francisco
tags: [University of California San Francisco,Organoid,Health sciences,Life sciences,Medicine,Biotechnology,Biology,Clinical medicine]
---


These mini-tissues in a dish can be used to study how particular structural features of tissue affect normal growth or go awry in cancer. In the context of human tissues, when organization fails, it sets the stage for cancer. But studying how the cells of complex tissues like the mammary gland self-organize, make decisions as groups, and break down in disease has been a challenge to researchers. Cells can be incubated with several sets of DNA bar codes to specify multiple allowable partners. To demonstrate the precision of the technique and its ability to generalize to many different human tissue types, the research team created several proof-of-principle organoid arrays mimicking human tissues such as branching vasculature and mammary glands.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/uoc--d3p082815.php){:target="_blank" rel="noopener"}


