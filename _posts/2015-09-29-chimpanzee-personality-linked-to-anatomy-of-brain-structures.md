---
layout: post
title: "Chimpanzee personality linked to anatomy of brain structures"
date: 2015-09-29
categories:
author: Georgia State University 
tags: [Cingulate cortex,Extraversion and introversion,News aggregator,Psychological concepts,Behavioural sciences,Neuroscience,Psychology,Brain,Cognitive science]
---


Chimpanzees' personality traits are linked to the anatomy of specific brain structures, according to researchers at Georgia State University, The University of Texas MD Anderson Cancer Center and University of Copenhagen. The findings, published online in the journal NeuroImage in August, reveal that both gray- matter volumes of various frontal cortex regions and gray-matter volume asymmetries (larger right versus left or vice versa) are associated with various personality traits. Our results confirm the importance of neuroscientific approaches to the study of basic personalities and suggest that when compared to humans many of these associations are comparable in chimpanzees, said Robert Latzman, assistant professor in the Department of Psychology at Georgia State. The researchers studied 107 chimpanzees' brains using magnetic resonance image (MRI) scans and also assessed each chimpanzee's personality by using a 41-item personality questionnaire. They found chimpanzees who were rated as higher for the personality traits of openness and extraversion had greater gray-matter volumes in the anterior cingulate cortex in both hemispheres of the brain.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150929092738.htm){:target="_blank" rel="noopener"}


