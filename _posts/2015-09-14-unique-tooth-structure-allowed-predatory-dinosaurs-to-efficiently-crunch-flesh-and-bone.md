---
layout: post
title: "Unique tooth structure allowed predatory dinosaurs to efficiently crunch flesh and bone"
date: 2015-09-14
categories:
author: University Of Toronto
tags: [Theropoda,Tyrannosaurus,Tooth,Dinosaur,Dinosaur tooth,Carnivore,Allosaurus,Gorgosaurus,Predation]
---


Gorgosaurus is shown using its specialized teeth for feeding on a young Corythosaurus in Alberta, 75 million years ago. Credit: Danielle Dufault  The Tyrannosaurus rex and its fellow theropod dinosaurs that rampage across the screen in movies like Jurassic World were successful predators partly due to a unique, deeply serrated tooth structure that allowed them to easily tear through the flesh and bone of other dinosaurs, says new research from the University of Toronto Mississauga (UTM). Brink and her colleagues determined that this deeply serrated—or sawlike—tooth structure is uniquely common to carnivorous theropods such as T. rex and Allosaurus, and even one of the first theropods, Coelophysis. It, too, preys on larger animals. What is startling and amazing about this work is that Kirstin was able to take teeth with these steak knife-like serrations and find a way to make cuts to obtain sections along the cutting edge of these teeth, said Reisz.

<hr>

[Visit Link](http://phys.org/news/2015-07-unique-tooth-predatory-dinosaurs-efficiently.html){:target="_blank" rel="noopener"}


