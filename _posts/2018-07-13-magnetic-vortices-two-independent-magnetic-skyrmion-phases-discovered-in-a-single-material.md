---
layout: post
title: "Magnetic vortices: Two independent magnetic skyrmion phases discovered in a single material"
date: 2018-07-13
categories:
author: "Technische Universität Dresden"
tags: [Magnetism,Skyrmion,News aggregator,Physical sciences,Applied and interdisciplinary physics,Physics,Science,Theoretical physics]
---


Magnetic whirls are formed when the magnetic moments are aligned in a circular fashion. Tiny magnetic structures for compact magnetic storage? Now, in a single material we have found two different skyrmion phases, with two different sets of parameters. Previously it was thought that the new mechanism is very weak. Second skyrmion phase at very low temperatures  Alfonso Chacon discovered the new phase, when he studied the metastable properties of an already known skyrmion phase at the research neutron source of TUM.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180712100427.htm){:target="_blank" rel="noopener"}


