---
layout: post
title: "How debuggers really work"
date: 2018-04-26
categories:
author: "Levente Kurusa"
tags: [Signal (IPC),Breakpoint,Ptrace,Debugger,Software development,Operating system technology,System software,Office equipment,Software,Computer science,Computer programming,Software engineering,Technology,Computer architecture,Computers,Computer engineering,Computing]
---


This means that, once we issue the PTRACE_TRACEME request and call the execve system call to actually start the program in the tracee, the tracee will immediately stop, since execve delivers a SIGTRAP , and that is caught by a wait-event in the tracer. It can be used in combination with other requests (which we will cover later in this article) to monitor and modify a system call's arguments or return value. It can be used in combination with other requests (which we will cover later in this article) to monitor and modify a system call's arguments or return value. Meet debug registers, a set of registers designed to fulfill this goal more efficiently:  DR0 to DR3 : Each of these registers contains an address (a memory location), where the debugger wants the tracee to stop for some reason. A bitmask controls the size of the watchpoint (whether 1, 2, 4, or 8 bytes are monitored) and whether to raise an exception on execution, reading, writing, or either of reading and writing.

<hr>

[Visit Link](https://opensource.com/article/18/1/how-debuggers-really-work){:target="_blank" rel="noopener"}


