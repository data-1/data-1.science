---
layout: post
title: "On the evolution of how we have defined time, time interval and frequency since antiquity"
date: 2016-03-23
categories:
author:  
tags: [Time,Coordinated Universal Time,Universal Time,Astronomy,Spacetime,Branches of science,Science,Concepts in metaphysics]
---


The solar year was often determined as the interval between consecutive spring equinoxes when the sun is directly over the equator. The earliest definitions of time and time-interval quantities were based on observed astronomical phenomena, such as apparent solar or lunar time, and as such, time as measured by clocks, and frequency, as measured by devices were derived quantities. Today's definition of time uses a combination of atomic and astronomical time. However, the rate of divergence between UTC and UT1 is estimated to be less than one minute per century. Explore further Why moving from astronomic to atomic time is a tricky business  More information: Judah Levine.

<hr>

[Visit Link](http://phys.org/news/2016-03-evolution-interval-frequency-antiquity.html){:target="_blank" rel="noopener"}


