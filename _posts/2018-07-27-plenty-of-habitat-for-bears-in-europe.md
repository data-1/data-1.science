---
layout: post
title: "Plenty of habitat for bears in Europe"
date: 2018-07-27
categories:
author: "German Centre For Integrative Biodiversity Research, Idiv"
tags: [Biodiversity,Bear,Europe,Habitat,Privacy,Brown bear,Conservation biology,Martin Luther University of Halle-Wittenberg]
---


Map of Europe shows areas currently inhabited by brown bears (blue), areas that are suitable habitat for bears according to the new study, but which are currently not populated (green) and areas unsuitable as bear habitat (grey). Effective management of the species, including a reduction of direct pressures by humans (like hunting) has the potential to help these animals come back in many of these areas, according to the head of the study. It is now important to plan the recovery of the species while taking measures to prevent conflicts. In Germany, there are 16,000 square kilometres of potential bear habitat. Credit: Adam Wajrak  The fact that there is still suitable habitat for brown bears is a great opportunity for species conservation, says the head of the study, Dr. Néstor Fernández from the iDiv research centre and the University of Halle.

<hr>

[Visit Link](https://phys.org/news/2018-07-plenty-habitat-europe.html){:target="_blank" rel="noopener"}


