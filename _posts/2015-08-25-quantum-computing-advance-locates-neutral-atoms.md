---
layout: post
title: "Quantum computing advance locates neutral atoms"
date: 2015-08-25
categories:
author: Penn State 
tags: [Quantum mechanics,Quantum computing,Computer,Computing,Quantum superposition,Atom,Information,Qubit,Physics,Theoretical physics,Science,Physical sciences,Branches of science,Applied and interdisciplinary physics]
---


The methods of encoding information onto neutral atoms, ions or Josephson junctions -- electronic devices used in precise measurement, to create quantum computers -- are currently the subject of much research. Quantum computers can solve some problems that classical computers can't, said Weiss. Weiss and his graduate students Yang Wang and Aishwarya Kumar, looked at using neutral atoms for quantum computing and investigated ways to individually locate and address an atom to store and retrieve information. Other researchers are investigating ions and superconducting Josephson junctions, but Weiss's team chose neutral atoms. Currently, the researchers can only fill about 50 percent of the laser atom traps with atoms, but they can perform quantum gates on those atoms with 93 percent fidelity and cross talk that is too small to measure.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150812151247.htm){:target="_blank" rel="noopener"}


