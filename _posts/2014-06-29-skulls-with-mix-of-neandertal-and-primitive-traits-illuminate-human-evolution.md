---
layout: post
title: "Skulls with mix of Neandertal and primitive traits illuminate human evolution"
date: 2014-06-29
categories:
author: American Association for the Advancement of Science (AAAS) 
tags: [Human evolution,Neanderthal,Pleistocene]
---


To answer these questions, scientists have needed an accurate picture of European populations around 400,000 years ago, the early stages of the Neandertal lineage. What makes the Sima de los Huesos site unique, Arsuaga said, is the extraordinary and unprecedented accumulation of hominin fossils there; nothing quite so big has ever been discovered for any extinct hominin species—including Neanderthals. The researchers' skull samples showed Neandertal features present in the face and teeth, but not elsewhere; the nearby braincase, for example, still showed features associated with more primitive hominins. The work of Arsuaga et al. suggests that facial modification was the first step in Neandertal evolution. AAAS was founded in 1848, and includes some 261 affiliated societies and academies of science, serving 10 million individuals.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/aaft-swm061314.php){:target="_blank" rel="noopener"}


