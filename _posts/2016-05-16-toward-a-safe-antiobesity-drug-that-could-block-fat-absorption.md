---
layout: post
title: "Toward a safe antiobesity drug that could block fat absorption"
date: 2016-05-16
categories:
author: American Chemical Society
tags: [Obesity,Health,Health care,Medicine,Medical treatments,Medical specialties,Pharmacology,Drugs,Diseases and disorders,Health sciences,Clinical medicine]
---


To help address the global obesity epidemic, scientists are developing a new class of compounds called micelle sequestrant polymers, or MSPs, that could prevent fat particles from getting absorbed in the body and thus potentially reduce weight gain. They report on their novel agents, which they tested on mice, in the ACS journal Biomacromolecules. Research has shown that worldwide, obesity rates have been climbing for years. Treatments for overweight and obesity include diet and exercise, surgery and prescription medications. But currently available drugs can have serious side effects, including increased risk of cardiovascular disease and depression.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/acs-tas072915.php){:target="_blank" rel="noopener"}


