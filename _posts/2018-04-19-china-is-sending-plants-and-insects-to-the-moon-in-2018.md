---
layout: post
title: "China Is Sending Plants and Insects to the Moon in 2018"
date: 2018-04-19
categories:
author: ""
tags: [Chinese Lunar Exploration Program,Moon,Lander (spacecraft),Astronomical objects known since antiquity,Physical sciences,Sky,Solar System,Flight,Planetary science,Astronautics,Spaceflight,Astronomy,Outer space,Space science]
---


This year, the Chinese Lunar Exploration Program (CLEP), otherwise known as the Chang'e Program, plans to launch the Chang'e 4 Mission. The program has previously sent two orbiters and one lander to the Moon, and now, they plan to study its geology while exploring the effects of lunar gravity on both insects and plants from Earth. As Zhang Yuanxun, the chief designer of the container, told the Chongqing Morning Post (according to China Daily), the Chang'e 4 Mission's container will house potatoes, arabidopsis seeds, and silkworm eggs. By improving our understanding of how seeds might sprout and grow or how insects might survive and potentially thrive under such difficult and abnormal conditions, we might better prepare for life on the Moon. Any human lunar explorers would be outfitted with protective gear to prevent the negative effects caused by the Moon's lower gravity, but still, those stationed at a lunar base for an extended period of time could face unforeseen problems.

<hr>

[Visit Link](https://futurism.com/china-sending-plants-insects-moon-2018/){:target="_blank" rel="noopener"}


