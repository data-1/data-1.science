---
layout: post
title: "Brain protein influences how the brain manages stress; suggests new model of depression"
date: 2016-04-11
categories:
author: The Mount Sinai Hospital / Mount Sinai School of Medicine
tags: [Catenin beta-1,Reward system,Major depressive disorder,Dopamine,Brain,Health,Neuroscience]
---


Using mouse models exposed to chronic social stress, Mount Sinai investigators discovered that it is the activity of the protein in the D2 neurons, a specific set of nerve cells (neurons) in the nucleus accumbens (NAc), the brain's reward and motivation center, which drives resiliency. It is the first report that B-catenin is deficient in nucleus accumbens in human depression and mouse depression models; it is the first study to show that higher activity of B-catenin drives resilience and the first report demonstrating a strong connection between B-catenin and control of microRNA synthesis. While most prior efforts in antidepressant drug discovery have focused on ways to undo the bad effects of stress, our findings provide a pathway to generate novel antidepressants that instead activate mechanisms of natural resilience, says Dr. Nestler. About the Mount Sinai Health System  The Mount Sinai Health System is an integrated health system committed to providing distinguished care, conducting transformative research, and advancing biomedical education. Physicians are affiliated with the Icahn School of Medicine at Mount Sinai, which is ranked among the top 20 medical schools both in National Institutes of Health funding and by U.S. News & World Report.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-11/tmsh-bpi111214.php){:target="_blank" rel="noopener"}


