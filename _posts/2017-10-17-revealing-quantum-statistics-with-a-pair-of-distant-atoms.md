---
layout: post
title: "Revealing quantum statistics with a pair of distant atoms"
date: 2017-10-17
categories:
author: University Of Bonn
tags: [Atom,Quantum mechanics,Spin (physics),Quantum entanglement,Matter,Privacy,Time,Physics,Science,Theoretical physics,Physical sciences,Scientific theories]
---


After all, to you, both cups look completely identical. Playing a game such as Find the Lady in the quantum world has now been proposed by physicists at the Institute of Applied Physics (IAP) of the University of Bonn together with their colleagues from Austria and the USA. In different places at the same time  In the quantum world, the cups are replaced by two atoms that are exactly in the same atomic state. In other words: at the end of the quantum manipulation, the observer has no way to say – as a matter of principle – whether atom 1 is actually still atom 1 or whether it has been swapped with atom 2. This is not the case for identically prepared atoms; they are exactly the same.

<hr>

[Visit Link](https://phys.org/news/2017-10-revealing-quantum-statistics-pair-distant.html){:target="_blank" rel="noopener"}


