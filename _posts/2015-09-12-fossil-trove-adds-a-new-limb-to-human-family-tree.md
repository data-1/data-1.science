---
layout: post
title: "Fossil trove adds a new limb to human family tree"
date: 2015-09-12
categories:
author: University of Wisconsin-Madison 
tags: [Rising Star Cave,Homo naledi,Paleoanthropology,Lee Rogers Berger]
---


So far, the remains of newborns to the aged have been retrieved from the cave and the researchers expect that many more bones remain in the chamber, which is nearly 100 feet underground and accessible only after squeezing, clambering and crawling 600 feet to a large chamber where the brittle fossils cover the floor. The cave, according to Hawks, was likely more accessible to Homo naledi than it is today for modern humans. The discovery of the fossil chamber was made after amateur cavers exploring the Rising Star cave complex spotted a skull and alerted paleoanthropologist Lee Berger of the University of Witwatersrand, who organized an expedition to retrieve the fossil and whose Evolutionary Studies Institute vault now houses what turned out to be an astonishing haul of hominin fossils, the single largest such collection ever found. Among the Homo naledi fossils, the pelvis is not the best represented, with only about 40 brittle bits and pieces of hip bone from multiple individuals of different ages collected so far, making VanSickle's job less than straightforward. The downside is that the Homo naledi fragments are far less complete than fossils from other sites, so figuring out how they fit together and what they mean for how Homo naledi walked or gave birth is much more difficult.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/uow-fta091015.php){:target="_blank" rel="noopener"}


