---
layout: post
title: "Earth's water is older than the sun"
date: 2015-07-04
categories:
author: Carnegie Institution for Science 
tags: [Deuterium,Solar System,Sun,Formation and evolution of the Solar System,Hydrogen,Isotope,Water,Abiogenesis,Planet,Space science,Nature,Planetary science,Physical sciences,Chemistry,Astronomy,Outer space,Astronomical objects]
---


New work from a team including Carnegie's Conel Alexander found that much of our Solar System's water likely originated as ices that formed in interstellar space. Water is found throughout our Solar System. But if the early Solar System's water was largely the result of local chemical processing during the Sun's birth, then it is possible that the abundance of water varies considerably in forming planetary systems, which would obviously have implications for the potential for the emergence of life elsewhere. They did this in order to see if the system can reach the ratios of deuterium to hydrogen that are found in meteorite samples, Earth's ocean water, and time capsule comets. They found that it could not do so, which told them that at least some of the water in our own Solar System has an origin in interstellar space and pre-dates the birth of the Sun.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/ci-ewi092214.php){:target="_blank" rel="noopener"}


