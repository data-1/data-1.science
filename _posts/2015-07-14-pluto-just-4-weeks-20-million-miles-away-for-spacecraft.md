---
layout: post
title: "Pluto just 4 weeks, 20 million miles away for spacecraft"
date: 2015-07-14
categories:
author: Bymarcia Dunn
tags: [New Horizons,Pluto,Long Range Reconnaissance Imager,Outer space,Bodies of the Solar System,Astronomy,Space science,Science,Astronomical objects,Planemos,Spacecraft,Flight,Discovery and exploration of the Solar System,Space exploration,Astronautics,Planetary science,Spaceflight,Solar System]
---


This image made available by NASA/Johns Hopkins University Applied Physics Laboratory/Southwest Research Institute on June 11, 2015 shows four computer-enhanced views of Pluto, taken by New Horizons' Long Range Reconnaissance Imager (LORRI). (NASA/Johns Hopkins University Applied Physics Laboratory/Southwest Research Institute via AP)  NASA's New Horizons spacecraft is at Pluto's doorstep, following an incredible journey of nine years and 3 billion miles. As of Tuesday, New Horizons was just over 20 million miles from Pluto. That's closer than Earth is to neighbor Venus, at their closest point. It's really a mission of raw exploration, flying into the unknown to see what's there.

<hr>

[Visit Link](http://phys.org/news353693380.html){:target="_blank" rel="noopener"}


