---
layout: post
title: "Epicenter of brain's predictive ability pinpointed by scientists"
date: 2016-05-07
categories:
author: Northeastern University
tags: [News aggregator,Cognitive science,Interdisciplinary subfields,Neuroscience,Cognition,Psychological concepts,Psychology,Concepts in metaphysics,Cognitive psychology]
---


Experts say humans' reac­tions are in fact the body adjusting to pre­dic­tions the brain is making based on the state of our body the last time it was in a sim­ilar situation. The unique con­tri­bu­tion of our paper is to show that limbic tissue, because of its struc­ture and the way the neu­rons are orga­nized, is pre­dicting, Bar­rett said. In the Nature paper, Bar­rett sum­ma­rized research on the cel­lular com­po­si­tion of limbic tissue, which shows that limbic regions of the brain send but do not receive pre­dic­tions. This means that limbic regions direct pro­cessing in the brain. In her paper, Bar­rett shows that your brain is not wired to be a reac­tive organ.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150602130553.htm){:target="_blank" rel="noopener"}


