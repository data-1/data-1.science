---
layout: post
title: "Webb telescope microshutters journey into NASA clean room"
date: 2014-06-24
categories:
author: ""        
tags: [NIRSpec,James Webb Space Telescope,Goddard Space Flight Center,Technology,Science,Astronomy,Observational astronomy]
---


This closeup of the four microshutter arrays shows some shutters open and some closed. The microshutters were moved into a NASA Goddard cleanroom for testing to verify they work correctly before being installed in the Webb's Near Infrared Spectrograph (NIRSpec) instrument. An open shutter lets light from a selected target in a particular part of the sky to pass through to NIRSpec's detectors while a closed shutter blocks unwanted light from any objects that scientists don't want to observe. A short video was created to show the transport of the flight microshutter device into a cleanroom at NASA Goddard on May 15, 2014, where it will undergo various tests for several months to verify its integrity and operation. Explore further NASA engineers rehearse placement of Webb Telescope's NIRSpec and microshutters

<hr>

[Visit Link](http://phys.org/news322813231.html){:target="_blank" rel="noopener"}


