---
layout: post
title: "NASA's Hubble sees martian moon orbiting the Red Planet"
date: 2017-09-24
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Phobos (moon),Mars,Planet,Deimos (moon),Moons of Mars,Natural satellite,Moon,Asteroid,Hubble Space Telescope,Local Interstellar Cloud,Moons,Planets,Physical sciences,Astronomical objects known since antiquity,Planets of the Solar System,Astronomical objects,Bodies of the Solar System,Outer space,Solar System,Planetary science,Space science,Astronomy]
---


The sharp eye of NASA's Hubble Space Telescope has captured the tiny moon Phobos during its orbital trek around Mars. Orbiting 3,700 miles above the Martian surface, Phobos is closer to its parent planet than any other moon in the solar system. The origin of Phobos and Deimos is still being debated. Hubble took the images of Phobos orbiting the Red Planet on May 12, 2016, when Mars was 50 million miles from Earth. This was just a few days before the planet passed closer to Earth in its orbit than it had in the past 11 years.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-07/nsfc-nhs072017.php){:target="_blank" rel="noopener"}


