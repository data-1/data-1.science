---
layout: post
title: "Europe secures new generation of weather satellites"
date: 2015-09-03
categories:
author: "$author"   
tags: [MetOp,European Organisation for the Exploitation of Meteorological Satellites,Satellite,Sentinel-5,Astronautics,Physical geography,Atmospheric sciences,Bodies of the Solar System,Applied and interdisciplinary physics,Outer space,Space science,Meteorology,Satellites,Spaceflight,Earth sciences,Sky,Spacecraft]
---


Applications Europe secures new generation of weather satellites 16/10/2014 11130 views 66 likes  Contracts were signed today to build three pairs of MetOp Second Generation satellites, ensuring the continuity of essential information for global weather forecasting and climate monitoring for decades to come. “The MetOp-SG satellites will continue and enhance essential observations from polar orbit that are needed for numerical weather prediction.” Alain Ratier remarked, “The signature of the MetOp-SG contracts is a new landmark in our highly successful cooperation with ESA. “The MetOp-SG satellites will improve all the observations of the first MetOp generation and, in addition, will observe precipitation and cirrus clouds. This will further improve weather forecasting and climate monitoring from space in Europe and worldwide.” MetOp-SG takes the highly successful cooperation with ESA into the next decades. The satellites constitute a true milestone for an innovative system that will yield benefits from 2022 onwards to further improve forecasting.” As a cooperative effort, ESA funds the development of the first satellites and procures the repeat satellites on behalf of Eumetsat.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/The_Living_Planet_Programme/Meteorological_missions/Europe_secures_new_generation_of_weather_satellites){:target="_blank" rel="noopener"}


