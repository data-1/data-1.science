---
layout: post
title: "Getting started with Standard Notes for encrypted note-taking"
date: 2018-08-01
categories:
author: "Mo Bitar"
tags: [Encryption,Server (computing),Web server,Nginx,IOS,Android (operating system),Ruby (programming language),Linux,Application software,Installation (computer programs),Computer science,Computer engineering,Computer architecture,Digital media,Information technology management,Information Age,Intellectual works,System software,Software engineering,Technology,Software development,Computing,Software,Computers]
---


If you don’t want to host your own server and are ready to start using Standard Notes right away, you can use our public syncing server. Hosting your own Standard Notes server  Get the Standard File Rails app running on your Linux box and expose it via NGINX or any other web server. rvm / scripts / rvm Install Ruby: rvm install ruby This should install the latest version of Ruby (2.3 at the time of this writing.) conf Add this to the bottom of the file, inside the last curly brace: server {  listen 443 ssl default_server;  ssl_certificate / etc / letsencrypt / live / domain. On the Standard Notes Android or iOS app:  Open the Settings window, click Advanced Options when signing in or registering, and enter your server URL in the Sync Server field.

<hr>

[Visit Link](https://opensource.com/article/18/8/getting-started-standard-notes){:target="_blank" rel="noopener"}


