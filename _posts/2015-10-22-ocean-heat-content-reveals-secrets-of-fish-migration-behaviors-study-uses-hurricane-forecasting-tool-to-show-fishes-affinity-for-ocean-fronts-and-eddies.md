---
layout: post
title: "Ocean heat content reveals secrets of fish migration behaviors: Study uses hurricane forecasting tool to show fishes affinity for ocean fronts and eddies"
date: 2015-10-22
categories:
author: University of Miami Rosenstiel School of Marine & Atmospheric Science
tags: [Loop Current,Ocean,Tropical cyclone,Ocean heat content,Earth sciences,Physical geography,Natural environment,Hydrology,Nature,Hydrography,Oceanography,Applied and interdisciplinary physics]
---


Researchers at the University of Miami (UM) Rosenstiel School of Marine and Atmospheric Science developed a new method to estimate fish movements using ocean heat content images, a dataset commonly used in hurricane intensity forecasting. In addition to providing the OHC for forecasting, these previous studies showed OHC images reveal dynamic ocean features, such as fronts and eddies, in the ocean better than just using standard techniques (e.g., sea surface temperature), especially during the summer months. The researchers compared data on fish movements obtained from pop-up satellite tags affixed to the highly migratory fish alongside maps of the heat stored in the upper ocean. In one 109-day analysis, the researchers documented a yellowfin tuna move along a weak front off the Mississippi River before reaching an eddy centered in the Gulf of Mexico. Eddies are swirling masses of water that have been shed from strong ocean current fronts, and pump nutrient-rich water to the surface.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151021170825.htm){:target="_blank" rel="noopener"}


