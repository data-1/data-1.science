---
layout: post
title: "Strong magnetic fields discovered in majority of stars"
date: 2016-01-29
categories:
author: University of Sydney
tags: [Star,Sun,Magnetism,Magnetic field,Stellar evolution,Science,Astronomy,Space science,Physical sciences,Physics,Stellar astronomy,Nature]
---


Because only 5-0 percent of stars were previously thought to host strong magnetic fields, current models of how stars evolve lack magnetic fields as a fundamental ingredient, Associate Professor Stello said. The research is based on previous work led by the Californian Institute of Technology (Caltech) and including Associate Professor Stello, which found that measurements of stellar oscillations, or sound waves, inside stars could be used to infer the presence of strong magnetic fields. Associate Professor Stello said. The team measured tiny brightness variations of stars caused by the ringing sound and found certain oscillation frequencies were missing in 60 percent of the stars because they were suppressed by strong magnetic fields in the stellar cores. This could potentially lead to a better general understanding of magnetic dynamos, including the dynamo controlling the Sun's 22-year magnetic cycle, which is known to affect communication systems and cloud cover on Earth.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/uos-smf010316.php){:target="_blank" rel="noopener"}


