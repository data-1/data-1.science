---
layout: post
title: "The Digest: These Mind-Blowing Images of The Human Body Were Made By A New Kind of Scanner"
date: 2018-07-13
categories:
author: ""
tags: [X-ray,Medical imaging,Image scanner,Bone,Technology,Clinical medicine,Imaging]
---


Phil and Anthony Butler aren't just father and son. When x-rays travel through your body, they're absorbed by denser materials (bones) and pass right through softer ones (muscles and other tissues). The places where the x-rays couldn't pass through appear solid white. “This technology sets the machine apart diagnostically because its small pixels and accurate energy resolution mean that this new imaging tool is able to get images that no other imaging tool can achieve,” Phil Butler said in a CERN news release. Next, the researchers plans to test out their scanner in a trial focused on orthopedic and rheumatology patients in New Zealand.

<hr>

[Visit Link](https://futurism.com/3d-x-rays-medical-scanner/){:target="_blank" rel="noopener"}


