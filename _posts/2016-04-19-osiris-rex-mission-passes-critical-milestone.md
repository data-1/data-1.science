---
layout: post
title: "OSIRIS-REx mission passes critical milestone"
date: 2016-04-19
categories:
author: Dwayne Brown
tags: [OSIRIS-REx,Near-Earth object,NASA,Asteroid,Asteroid Redirect Mission,Goddard Space Flight Center,Astronomy,Astronautics,Flight,Space program of the United States,Science,Spacecraft,Planetary science,Space exploration,Solar System,Space science,Spaceflight,Outer space]
---


OSIRIS-REx is the first U.S. mission to return samples from an asteroid to Earth. On March 27, assembly, launch and test operations officially began at Lockheed Martin in Denver. NASA's Marshall Space Flight Center in Huntsville, Alabama, manages New Frontiers for the agency's Science Mission Directorate. OSIRIS-REx complements NASA's Asteroid Initiative, which aligns portions of the agency's science, space technology and human exploration capabilities in a coordinated asteroid research effort. The president's NASA budget included, and Congress authorized, $20.4 million for an expanded NASA Near-Earth Object (NEO) Observations Program, increasing the resources for this critical program from the $4 million per year it had received since the 1990s.

<hr>

[Visit Link](http://phys.org/news347176640.html){:target="_blank" rel="noopener"}


