---
layout: post
title: "6 pivotal moments in open source history"
date: 2018-04-22
categories:
author: "Dave Neary
(Red Hat)"
tags: [Free and open-source software,GNU,Richard Stallman,Free software,Linux,Computing,Computers,Information technology management,Information technology,Computer engineering,Intellectual property activism,Open content,Computer science,Intellectual works,Free content,Technology,Open-source movement,Software engineering,Software development,Software]
---


Early in 1985, he published The GNU Manifesto, which was a call to arms for programmers to join the effort, and launched the Free Software Foundation in order to accept donations to support the work. This essay has been credited with bringing free software to a broader audience, and with convincing executives at software companies at the time that releasing their software under a free software license was the right thing to do. The term open source was coined by Christine Peterson to describe free software, and the Open Source Institute was later founded by Bruce Perens and Eric Raymond. Richard Stallman and the Free Software Foundation continued to champion the term free software, because to them, the fundamental difference with proprietary software was user freedom, and the availability of source code was just a means to that end. The combined effect of massive Silicon Valley funding of open source projects, the attention of Wall Street for young companies built around open source software, and the market credibility that tech giants like IBM and Sun Microsystems brought had combined to create the massive adoption of open source, and the embrace of the open development model that helped it thrive have led to the dominance of Linux and open source in the tech industry today.

<hr>

[Visit Link](https://opensource.com/article/18/2/pivotal-moments-history-open-source){:target="_blank" rel="noopener"}


