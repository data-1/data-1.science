---
layout: post
title: "'Memory region' of brain also involved in conflict resolution: Hippocampus is involved in quick, successful decisions"
date: 2016-05-28
categories:
author: Ruhr-University Bochum
tags: [Memory,Hippocampus,News aggregator,Psychology,Cognitive psychology,Mental processes,Cognition,Cognitive science,Neuroscience,Brain,Interdisciplinary subfields]
---


If, however, a car comes speeding along at the same time, the pedestrian should stay where he is. Results confirmed with two measurement methods  The team demonstrated with two different measurement methods that the hippocampus is active in such conflicting situations; this applies particularly when a person solves the conflicts quickly and successfully. Memory system could learn from resolved conflicts  Because the hippocampus is essential for memory, the researchers speculate about its role in conflict resolution: Our data show first of all a completely new function of the Hippocampus -- processing of activity conflicts, says Carina Oehrn from the Department of Epileptology at the University Hospital of Bonn. Perhaps the memory system becomes particularly active if a conflict has been successfully resolved, speculates Nikolai Axmacher. It responds strongly to resolved conflicts, but not to unsolved conflicts or standard situations.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150820134704.htm){:target="_blank" rel="noopener"}


