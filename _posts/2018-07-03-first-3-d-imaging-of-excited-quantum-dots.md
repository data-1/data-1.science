---
layout: post
title: "First 3-D imaging of excited quantum dots"
date: 2018-07-03
categories:
author: "American Institute of Physics"
tags: [Quantum dot,Nanoparticle,Microscope,Density functional theory,Scanning tunneling microscope,Chemistry,Optics,Chemical product engineering,Electromagnetism,Emerging technologies,Atomic molecular and optical physics,Technology,Physical chemistry,Nanotechnology,Applied and interdisciplinary physics,Materials science,Materials,Physical sciences]
---


Understanding how the presence of defects localizes excited electronic states of quantum dots will help to advance the engineering of these nanoparticles, said Martin Gruebele from the University of Illinois at Urbana-Champaign and a co-author of the paper. SMA-STM allows individual nanoparticles to be imaged in a laser beam, so their excited electronic structure can be visualized. The slices can be combined to reconstruct a 3-D image of an electronically excited quantum dot. Researchers are now working to advance SMA-STM into a single-particle tomography technique. But, before SMA-STM becomes a true single-particle tomography approach, they still have to ensure that the scanning and rolling does not damage the nanoparticle while it is being reoriented.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/aiop-f3i020618.php){:target="_blank" rel="noopener"}


