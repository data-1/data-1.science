---
layout: post
title: "ESA’s Jupiter mission moves off the drawing board"
date: 2017-09-23
categories:
author: ""
tags: [Jupiter Icy Moons Explorer,Jupiter,Flight,Space science,Outer space,Astronomy,Solar System,Planetary science,Spaceflight,Bodies of the Solar System,Astronautics,Astronomical objects,Planets of the Solar System,Planets,Astronomical objects known since antiquity,Physical sciences,Sky,Spacecraft]
---


Science & Exploration ESA’s Jupiter mission moves off the drawing board 16/03/2017 8975 views 139 likes  Demanding electric, magnetic and power requirements, harsh radiation, and strict planetary protection rules are some of the critical issues that had to be tackled in order to move ESA’s Jupiter Icy Moons Explorer – Juice – from the drawing board and into construction. An important milestone was reached earlier this month, when the preliminary design of Juice and its interfaces with the scientific instruments and the ground stations were fixed, which will now allow a prototype spacecraft to be built for rigorous testing. The spacecraft’s main engine will be used to enter orbit around the giant planet, and later around Jupiter’s largest moon, Ganymede. The review also ensured that Juice will meet strict planetary protection guidelines, because it is imperative to minimise the risk that the potentially habitable ocean moons, particularly Europa, might be contaminated by viruses, bacteria or spores carried by the spacecraft from Earth. “The spacecraft design has been extensively and positively reviewed, and confirmed to address the many critical mission requirements,” says Giuseppe Sarri, Juice project manager.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/ESA_s_Jupiter_mission_moves_off_the_drawing_board){:target="_blank" rel="noopener"}


