---
layout: post
title: "Researchers uncover evidence of people predating Amazonian rainforest"
date: 2014-08-18
categories:
author: Bob Yirka
tags: [Amazon rainforest,Deforestation,Rainforest,Earth sciences,Nature,Natural environment]
---


In their paper published in Proceedings of the National Academy of Sciences, the researchers describe how they took sediment core samples from lakes and noted large ditches dug by people that lived in the area during a time before the rainforests—their findings suggest such people adapted to the wetter climate over time, rather than moved into it and started cutting down trees, as has been suggested by other research. The discovery of the ditches and pollen in the core samples suggests that people were living in the area and farming, prior to the changeover to rain forest. The researchers also noted that the core samples showed that rain forest plants became dominant in one area as recently as 500 years ago, possibly due to the disappearance of the people that lived there dying from diseases brought by European settlers. Earthwork construction and agriculture on terra firme landscapes currently occupied by the seasonal rainforests of southern Amazonia may therefore not have necessitated large-scale deforestation using stone tools. Our findings demonstrate that current debates over the magnitude and nature of pre-Columbian Amazonian land use, and its impact on global biogeochemical cycling, are potentially flawed because they do not consider this land use in the context of climate-driven forest–savanna biome shifts through the mid-to-late Holocene.

<hr>

[Visit Link](http://phys.org/news324028796.html){:target="_blank" rel="noopener"}


