---
layout: post
title: "The Open Information Security Foundation Joins Open Source Initiative as Affiliate Member"
date: 2015-06-30
categories:
author: ""   
tags: [Open source,Open Source Initiative,Open-source software,Open-source-software movement,Intellectual works,Information Age,Business,Software development,Digital rights,Communication,Software,Open-source movement,Information technology,Technology,Computing,Cyberspace]
---


Their mission of building community and innovative open source security technologies can be seen through all their efforts. Kelley Misata, Executive Director of OISF  The OSI Affiliate Member Program, available at no-cost, allows non-profit and not-for-profit organizations—unequivocally independent groups with a clear commitment to open source—to join and support our mission to promote and protect open source software and to build bridges among different constituencies in the open source community. Affiliate Members participate directly in the direction and development of the OSI through Board of Director elections as well as incubator projects and working groups that support the open source movement. For more information about the OSI Affiliate Member Program and how your organization can join, see: http://opensource.org/affiliates. About The Open Information Security Foundation  The Open Information Security Foundation (OISF) is a non-profit foundation organized to build a next generation IDS/IPS engine.

<hr>

[Visit Link](http://opensource.org/node/754){:target="_blank" rel="noopener"}


