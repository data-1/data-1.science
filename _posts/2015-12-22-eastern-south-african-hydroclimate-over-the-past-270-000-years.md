---
layout: post
title: "Eastern South African hydroclimate over the past 270,000 years"
date: 2015-12-22
categories:
author: Simon, Margit H., School Of Earth, Ocean Sciences, Cardiff University, Cardiff, United Kingdom, Uni Research Climate, Allegaten, Bjerknes Centre For Climate Research
tags: [Climate,Climate variability and change,Precipitation,Solar irradiance,Intertropical Convergence Zone,Weathering,Indian Ocean,Subtropics,Atlantic Ocean,Anticyclone,Radiocarbon dating,Rain,Soil,Africa,River,Monsoon,Horse latitudes,Cold drop,Climate change,Atmospheric sciences,Natural environment,Earth phenomena,Applied and interdisciplinary physics,Earth sciences,Nature,Physical geography]
---


Our model results show that during Pmax, higher DJF insolation causes higher temperatures over the Southern Hemisphere, especially over land (Fig. Our record of long-term climate variability over eastern South Africa is consistent with other regional records that track Southern Hemisphere summer insolation changes14,16,33. The average absolute age difference between the initial and the resulting fine-tuned age model is only ~400 years (1σ = 1.47 kyr; SI). The tropical mean circulation responds to the Northern Hemisphere cooling (and Southern Hemisphere warming) by generating anomalous energy transport from the Southern Hemisphere to the Northern Hemisphere. However, it should be noted that while cooler western Indian Ocean SSTs both north and south of the equator might explain a reduction in precipitation in southeastern equatorial Africa during Northern Hemisphere cold anomalies, the same sensitivity experiment demonstrates that southwest Indian Ocean temperatures (along the Agulhas Current) are increased instead8.

<hr>

[Visit Link](http://www.nature.com/articles/srep18153){:target="_blank" rel="noopener"}


