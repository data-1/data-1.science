---
layout: post
title: "HINODE captures record breaking solar magnetic field"
date: 2018-06-21
categories:
author: "National Astronomical Observatory Of Japan"
tags: [Sunspot,Sun,Hinode (satellite),Outer space,Space plasmas,Stellar astronomy,Astronomical objects known since antiquity,Solar System,Bodies of the Solar System,Physical sciences,Astronomy,Space science]
---


Credit: NAOJ/JAXA  Astronomers at the National Astronomical Observatory of Japan (NAOJ) using the HINODE spacecraft observed the strongest magnetic field ever directly measured on the surface of the Sun. Surprisingly the data indicated a magnetic field strength of 6,250 gauss. HINODE continuously tracked the same sunspot with high spatial resolution for several days. These continuous data showed that the strong field was always located at the boundary between the bright region and the umbra, and that the horizontal gas flows along the direction of the magnetic fields over the bright region turned down into the Sun when they reached the strong-field area. The horizontal gas flows from the southern umbra compressed the fields near the other umbra (N-pole) and enhanced the field strength to more than 6,000 gauss.

<hr>

[Visit Link](https://phys.org/news/2018-02-hinode-captures-solar-magnetic-field.html){:target="_blank" rel="noopener"}


