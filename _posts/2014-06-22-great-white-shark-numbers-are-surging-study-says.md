---
layout: post
title: "Great white shark numbers are surging, study says"
date: 2014-06-22
categories:
author: Patrick Whittle
tags: [Great white shark,Shark,Privacy,Animals]
---


This undated photo provided by the National Oceanic and Atmospheric Administration shows a great white shark encountered off the coast of Massachusetts. The scientists report the shark's growing numbers are due to conservation efforts and greater availability of prey. But confrontations are rare, with only 106 unprovoked white shark attacks—13 of them fatal—in U.S. waters since 1916, according to data provided by the University of Florida. White shark abundance in the western North Atlantic declined by an estimated 73 percent from the early 1960s to the 1980s, the report says. The report does not provide a local estimate for the great white shark population, which some scientists say is between 3,000 and 5,000 animals.

<hr>

[Visit Link](http://phys.org/news322488229.html){:target="_blank" rel="noopener"}


