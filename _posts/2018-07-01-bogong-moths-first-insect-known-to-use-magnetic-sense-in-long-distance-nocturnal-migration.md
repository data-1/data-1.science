---
layout: post
title: "Bogong moths first insect known to use magnetic sense in long-distance nocturnal migration"
date: 2018-07-01
categories:
author: "Cell Press"
tags: [Bird migration,Bogong moth,Cognitive science]
---


After a few months of summer dormancy in those cool mountain caves, the moths fly right back to the breeding grounds where they were born. The discovery offers the first reliable evidence that nocturnal insects can use the Earth's magnetic field to steer flight during migration, the researchers say. They found that the moths' flight direction turned predictably when dominant visual landmarks and a natural Earth-strength magnetic field were turned together. The findings suggest that nocturnally migrating insects might use the Earth's magnetic field as a compass during migration just as nocturnally migrating birds do. The researchers suspect the moths use a magnetic compass to determine their migratory direction and then align this direction with a celestial or terrestrial landmark in the same or a similar direction, which they then use as a visual beacon.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180621141006.htm){:target="_blank" rel="noopener"}


