---
layout: post
title: "The future of search engines"
date: 2017-09-15
categories:
author: "University of Texas at Austin, Texas Advanced Computing Center"
tags: [Texas Advanced Computing Center,Artificial intelligence,WordNet,Machine learning,Deep learning,Natural language processing,Information,Research,Interdisciplinary subfields,Computer science,Information science,Cognition,Computing,Cognitive science,Technology,Branches of science]
---


Combining AI with the insights of annotators and the information encoded in domain-specific resources, he and his collaborators are developing new approaches to IR that will benefit general search engines, as well as niche ones like those for medical knowledge or non-English texts. We've been using crowdsourcing to annotate medical and news articles at scale so that our intelligent systems will be able to more accurately find the key information contained in each article. Lease, Wallace and their collaborators used the GPUs (graphics processing units) on the Maverick supercomputer at TACC to enable their analyses and train the machine learning system. By improving core natural language processing technologies for automatic information extraction and the classification of texts, web search engines built on these technologies can continue to improve. Resources like TACC are incredibly empowering for researchers in enabling us to pursue high-risk, potentially transformative research.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-08/uota-tfo080217.php){:target="_blank" rel="noopener"}


