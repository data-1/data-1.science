---
layout: post
title: "Can anonymity and accountability coexist?"
date: 2018-04-26
categories:
author: "Susie Choi"
tags: [Anonymity,Organization,Communication,Meritocracy,Organizational culture,Crowdsourcing,Free and open-source software,Whistleblower,Politics]
---


For instance, when establishing anonymous programs to drive greater diversity and more meritocratic evaluation of ideas, organizations may need to sacrifice the ability to hold speakers accountable for the opinions they express. Outlets for anonymous speech may be as similar to open as crowdsourcing is—or rather, is not. Organizations in which anonymous speech is not the main form of communication should acknowledge the strengths of anonymous speech, but carefully consider whether anonymity is the wisest means to the goal of sustainable openness. Availability of additional communication mechanisms: Rather than investing time and resources into establishing a new, anonymous channel for communication, can the culture or structure of existing avenues of communication be reconfigured to achieve the same goal? Designing the anonymous communication channel: How can accountability be promoted in anonymous communication without the ability to determine the identity of speakers?

<hr>

[Visit Link](https://opensource.com/open-organization/18/1/balancing-accountability-and-anonymity){:target="_blank" rel="noopener"}


