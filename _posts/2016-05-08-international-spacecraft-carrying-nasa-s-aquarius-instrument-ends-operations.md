---
layout: post
title: "International spacecraft carrying NASA's Aquarius instrument ends operations"
date: 2016-05-08
categories:
author: Nasa'S Goddard Space Flight Center
tags: [Aquarius (SAC-D instrument),SAC-D,Flight,Science,Astronomy,Spacecraft,Spaceflight,Earth sciences,Outer space,Space science,Astronautics]
---


The Aquarius mission studied the interactions between changes in the ocean circulation, global water cycle and climate by measuring ocean surface salinity. Credit: NASA  An international Earth-observing mission launched in 2011 to study the salinity of the ocean surface ended June 8 when an essential part of the power and attitude control system for the SAC-D spacecraft, which carries NASA's Aquarius instrument, stopped operating. The instrument's surface salinity measurements are contributing to a better understanding of ocean dynamics and advancing climate and ocean models, both from season to season and year to year. Salinity information is critical to improving our understanding of two major components of Earth's climate system: the water cycle and ocean circulation. Goddard managed the mission's operations phase and processes Aquarius science data.

<hr>

[Visit Link](http://phys.org/news353787307.html){:target="_blank" rel="noopener"}


