---
layout: post
title: "Designing gene therapy"
date: 2016-04-10
categories:
author: European Molecular Biology Laboratory
tags: [Genome editing,CRISPR gene editing,Cas9,Lymphoma,Genetics,Health sciences,Branches of genetics,Molecular biology,Biochemistry,Medical specialties,Medicine,Clinical medicine,Life sciences,Biotechnology,Biology]
---


The genetically modified T-cells are injected back into the patient, where their new gene enables them to seek out and destroy the cancer cells. The quicker the T-cells can be modified and transplanted back, the better the treatment prognosis for the patients, and the lower the costs. Until now, however, efforts to increase the treatment's efficiency were educated guesses, based on the structure of similar molecules. More efficient than CRISPR  Transposons like Sleeping Beauty have advantages for therapies that hinge on inserting a gene, compared to other genome engineering approaches such as CRISPR/Cas9. By contrast, Sleeping Beauty inserts the extra genetic material directly into the genome - and the EMBL scientists' enhancements now make it even more efficient.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/embl-dgt033116.php){:target="_blank" rel="noopener"}


