---
layout: post
title: "Sentinel-2A liftoff"
date: 2015-09-03
categories:
author: "$author"   
tags: [Outer space,Space agencies,Satellites orbiting Earth,Remote sensing,Space vehicles,European space programmes,Earth observation satellites,Spacecraft,Satellites,Spaceflight]
---


Replay of the Sentinel-2A liftoff on a Vega launcher from Europe’s Spaceport in French Guiana at 01:52 GMT (03:52 CEST) on 23 June 2015. Sentinel-2A is the second satellite to be launched for Europe’s Copernicus environment monitoring programme. Designed as a two-satellite constellation – Sentinel-2A and -2B – the Sentinel-2 mission carries an innovative wide swath high-resolution multispectral imager with 13 spectral bands for a new perspective of our land and vegetation. This information will be used for agricultural and forestry practices and for helping manage food security. It will also provide information on pollution in lakes and coastal waters.

<hr>

[Visit Link](http://www.esa.int/spaceinvideos/Videos/2015/06/Sentinel-2A_liftoff){:target="_blank" rel="noopener"}


