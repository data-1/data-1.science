---
layout: post
title: "US company in Iowa churns out 100 cloned cows a year"
date: 2014-06-29
categories:
author: Juliette Michel
tags: [Cloning,Cattle,Meat,Biology]
---


Only the numbers that follow are different: 2, 3, 4 and 6. The tag also bears the name of the company that bred them and is holding them temporarily in a field at its headquarters in Sioux Center, Iowa: Trans Ova Genetics, the only large US company selling cloned cows. Each year, the company gives birth, using the cloning technique, to about 100 calves. In 2008, the agency charged with US food safety, the Food and Drug Administration, approved consumption of meat and milk from cloned cows, pigs and goats. A scientist works at Trans Ova Genetics in Sioux Center, Iowa, June 16, 2014  But on the other side of the Atlantic, just last December, the European Commission, the European Union's executive, proposed a ban on the cloning of animals used for food and their import.

<hr>

[Visit Link](http://phys.org/news323231743.html){:target="_blank" rel="noopener"}


