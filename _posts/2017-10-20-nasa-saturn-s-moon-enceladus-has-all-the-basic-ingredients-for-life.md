---
layout: post
title: "NASA: Saturn's Moon Enceladus Has All the Basic Ingredients for Life"
date: 2017-10-20
categories:
author:  
tags: [General Electric,Technology,Innovation,Analytics,Sustainable energy,Computer network,Energy development,Electrical grid,Culture,3D printing,Augmented reality,Infrastructure,Economy]
---


But there’s more. His team is planning to feed the AR’s visual information into a database and analyze it for insights with apps running on Predix. “I think the more we leverage augmented reality, the more data we can harvest out of our processes,” Beacham says. “The way the AR system works, it takes pictures as it goes. Those pictures provide data you can analyze and discover ways to further optimize your processes and insights about production that are hard to get otherwise.”  tags

<hr>

[Visit Link](http://www.gereports.com/nasa-saturns-moon-enceladus-basic-ingredients-life/){:target="_blank" rel="noopener"}


