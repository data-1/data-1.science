---
layout: post
title: "Atomically thin magnetic device could lead to new memory technologies"
date: 2018-07-01
categories:
author: "University of Washington"
tags: [Computer data storage,Magnetism,Graphene,Electron,News aggregator,Tunnel magnetoresistance,Technology,Electrical engineering,Electromagnetism,Electricity]
---


A University of Washington-led team has now taken this one step further by encoding information using magnets that are just a few layers of atoms in thickness. With the explosive growth of information, the challenge is how to increase the density of data storage while reducing operation energy, said corresponding author Xiaodong Xu, a UW professor of physics and of materials science and engineering, and faculty researcher at the UW Clean Energy Institute. The functional units of this type of memory are magnetic tunnel junctions, or MTJ, which are magnetic 'gates' that can suppress or let through electrical current depending on how the spins align in the junction, said co-lead author Xinghan Cai, a UW postdoctoral researcher in physics. But with three and four layers, there are more combinations for spins between each layer, leading to multiple, distinct rates at which the electrons can flow through the magnetic material from one graphene sheet to the other. So not only would storage devices using CrI3 junctions be more efficient, but they would intrinsically store more data.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/05/180503142759.htm){:target="_blank" rel="noopener"}


