---
layout: post
title: "An evolutionary approach reveals new clues toward understanding the roots of schizophrenia"
date: 2015-12-07
categories:
author: SMBE Journals (Molecular Biology and Evolution and Genome Biology and Evolution) 
tags: [Evolution,Schizophrenia,Gene,Human,Biology,Brain,Genomics,Genome,Species,Mental disorder,Mutation,Genetics]
---


In a new study appearing in Molecular Biology and Evolution, Mount Sinai researcher Joel Dudley has led a new study that suggests that the very changes specific to human evolution may have come at a cost, contributing to the genetic architecture underlying schizophrenia traits in modern humans. The team examined a link between these regions, and human-specific evolution, in genomic segments called human accelerated regions, or HARs. It is the largest genome-wide association study ever performed on any psychiatric disease. They found that the schizophrenic loci were most strongly associated in genomic regions near the HARs that are conserved in non-human primates, and these HAR-associated schizophrenic loci are found to be under stronger evolutionary selective pressure when compared with other schizophrenic loci. Furthermore, these regions controlled genes that were expressed only in the prefrontal cortex of the brain, indicating that HARs may play an important role in regulating genes found to be linked to schizophrenia.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/mbae-aea022015.php){:target="_blank" rel="noopener"}


