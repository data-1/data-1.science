---
layout: post
title: "New Horizons reveals Pluto's striking surface variations and unique moon rotations"
date: 2015-10-20
categories:
author: University of Maryland 
tags: [Pluto,Nix (moon),Hydra (moon),Moons of Pluto,New Horizons,Natural satellite,Lunar water,Charon (moon),Tholin,Planetary science,Physical sciences,Dwarf planets,Plutinos,Astronomical objects,Solar System,Planemos,Astronomy,Outer space,Space science,Bodies of the Solar System]
---


Hamilton helped confirm the shapes, sizes and unique rotations of two of Pluto's moons and the finding that no other moons appear to orbit Pluto. LEISA data released by NASA but not included in the Science paper reveals numerous small exposed regions of water ice in the red areas of the color images taken by the MVIC. Why water ice appears where it does and not elsewhere, and which types of tholins are present on Pluto's surface are questions Protopapa and the New Horizons surface composition team hope to answer . Pluto isn't alone in having water ice on its surface--measurements indicate that Nix and Hydra, two of Pluto's five moons, are also covered with water ice. The rotational patterns of Pluto's moons also puzzle astronomers, as the two moons do not always have the same face locked toward Pluto.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/uom-nhr101415.php){:target="_blank" rel="noopener"}


