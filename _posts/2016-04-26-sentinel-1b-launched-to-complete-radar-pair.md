---
layout: post
title: "Sentinel-1B launched to complete radar pair"
date: 2016-04-26
categories:
author:  
tags: [Sentinel-1A,European Space Agency,Sentinel-1,Spacecraft,Outer space,Spaceflight,Astronautics,Space science,Flight,Space vehicles,Satellites,Spaceflight technology,Bodies of the Solar System,Space programs,Astronomy,Technology,Rocketry]
---


Applications Sentinel-1B launched to complete radar pair 25/04/2016 18096 views 103 likes  The second Sentinel-1 satellite – Sentinel-1B – was launched today to provide more ‘radar vision’ for Europe’s environmental Copernicus programme. Sentinel-1B joins its identical twin, Sentinel-1A, in orbit to deliver information for numerous services, from monitoring ice in polar seas to tracking land subsidence, and for responding to disasters such as floods. Birth of two icebergs During the launch, the satellite’s 12 m-long radar antenna and two 10 m-long solar wings were folded up to fit into the Soyuz rocket’s protective fairing. “This is the fourth satellite that we’ve launched for Copernicus in just two years and this launch is certainly a special moment because it completes the Sentinel-1 constellation.”  The launch of Sentinel-1B also provided an opportunity to give other smaller satellites a ride into space. “Importantly, the programme is helping to educate the next generation of scientists and engineers by transferring ESA knowhow in designing, building, testing, launching and operating satellites,” said Piero Galeone, ESA’s Head of the Tertiary Education Unit.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-1/Sentinel-1B_launched_to_complete_radar_pair){:target="_blank" rel="noopener"}


