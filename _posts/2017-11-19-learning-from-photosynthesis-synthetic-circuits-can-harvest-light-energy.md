---
layout: post
title: "Learning from photosynthesis: Synthetic circuits can harvest light energy"
date: 2017-11-19
categories:
author: "Arizona State University"
tags: [Photosynthesis,Light-harvesting complex,Energy,Chromophore,DNA,Technology,Nature,Applied and interdisciplinary physics,Physical sciences,Chemistry]
---


This multi-institute collaborative effort demonstrates a nice use of DNA nanotechnology to spatially control and organize chromophores for future excitonic networks, Yan said  Light Moves  In research appearing in the advanced online issue of the journal Nature Materials, a system for the programmed assembly of light-gathering elements or chromophores is described. In natural systems like plants and photosynthetic bacteria, the spatial organization of densely packed chromophores is vital for efficient, directed energy transfer. Plants carry out photosynthesis by converting photons of light striking their chromophores into another form of energy known as an exciton. Designing from Nature  In the current study, dye molecules responsive to particular ranges of light energy are used as synthetic chromophores. This feature—known as quantum coherence—can significantly enhance the efficiency of energy transfer.

<hr>

[Visit Link](https://phys.org/news/2017-11-photosynthesis.html){:target="_blank" rel="noopener"}


