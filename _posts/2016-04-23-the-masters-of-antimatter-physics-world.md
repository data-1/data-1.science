---
layout: post
title: "The masters of antimatter – Physics World"
date: 2016-04-23
categories:
author:  
tags: [Antimatter,Antiproton Decelerator,Particle physics,Physics,Science]
---


Fantastical and yet very real, the bizarre and exciting world of antimatter research is at a tipping point. Thanks to huge advances in the field over the past decade, scientists are now at the point where they can stably create and trap antimatter atoms for long periods of time. This will finally allow them to probe the cataclysmic stuff in the hope of deciphering exactly what makes antimatter tick just that little bit differently to regular matter  Physics World reporter Tushna Commissariat recently visited the ALPHA antimatter experiment at CERN and caught up with its spokesperson Jeffrey Hangst. In this podcast, they talk about the perfect recipe for making antihydrogen, they discuss dealing with the fact and fiction that surrounds the field, and reveal the everyday realties of being an antimatter architect. Housed within CERN’s Antimatter Factory, which includes the Antiproton Decelerator (AD) (the source that provides low-energy antiprotons), ALPHA and the other antimatter experiments – ACE, AEGIS, ATRAP and ASACUSA – all study the many puzzling facets of antimatter.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/dVgXjXO0ihY/the-masters-of-antimatter){:target="_blank" rel="noopener"}


