---
layout: post
title: "Rosetta’s ever-changing view of a comet"
date: 2017-10-12
categories:
author: ""
tags: [Rosetta (spacecraft),Philae (spacecraft),Spaceflight,Flight,Space missions,Outer space,Solar System,Space science,Bodies of the Solar System,Astronomy,Space vehicles,Planetary science,Spacecraft,Astronomical objects,Discovery and exploration of the Solar System,Space probes,Space exploration,Astronautics,Local Interstellar Cloud,Comets,Uncrewed spacecraft,Space research,Orbiters (space probe)]
---


These 210 images reflect Rosetta’s ever-changing view of Comet 67P/Churyumov–Gerasimenko between July 2014 and September 2016. The subsequent images, taken by Rosetta, reflect the varying distance from the comet as well as the comet’s rise and fall in activity as they orbited the Sun. Before the comet reached its most active phase in August 2015, Rosetta was able to make some close flybys, including one in which the lighting geometry from the Sun was such that the spacecraft’s shadow could be seen on the surface. Once the activity began to subside, Rosetta could come closer again and conduct science nearer to the nucleus, including capturing more high-resolution images of the surface, and looking out for changes after this active period. A selection of the final images taken are reflected in the last images shown in this montage.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2017/09/Rosetta_s_ever-changing_view_of_a_comet){:target="_blank" rel="noopener"}


