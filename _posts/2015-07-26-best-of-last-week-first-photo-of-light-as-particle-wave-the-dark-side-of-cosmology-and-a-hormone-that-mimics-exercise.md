---
layout: post
title: "Best of Last Week – First photo of light as particle/wave, the dark side of cosmology and a hormone that mimics exercise"
date: 2015-07-26
categories:
author: Bob Yirka
tags: [Light,Energy,Nature,Physical sciences]
---


(Phys.org)—It was another interesting week for physics as a team working at École Polytechnique Fédérale de Lausanne found a way to take the first ever photograph of light as both a particle and wave. In space news, David Spergel, an astrophysicist at Princeton, offered a compelling look at the dark side of cosmology—and explained why scientists are so sure that dark matter and dark energy exist. Also, a team analyzing data from the Keck telescope discovered, all jokes aside, a giant methane storm on Uranus, a surprise, as the planet's atmosphere was not known to be so energetic. And speaking of energy, researchers working at Northumbria University described a breakthrough in energy harvesting that could someday power life on Mars—it is a motor that runs on carbon dioxide and is based on the Leidenfrost effect. It was also a good week for technology development as a team of students launched a desktop recycler that turns pop bottles into 3D printer plastic—a development that could greatly reduce the cost of printing 3D objects.

<hr>

[Visit Link](http://phys.org/news345107719.html){:target="_blank" rel="noopener"}


