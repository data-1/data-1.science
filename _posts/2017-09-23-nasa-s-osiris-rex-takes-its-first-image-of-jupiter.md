---
layout: post
title: "NASA's OSIRIS-REx takes its first image of Jupiter"
date: 2017-09-23
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Jupiter,Astronomical objects known since antiquity,Spaceflight,Bodies of the Solar System,Planetary science,Solar System,Astronomy,Outer space,Space science,Astronomical objects,Space exploration,Astronautics,Planets of the Solar System,Spacecraft,Science,Planets,Flight,Discovery and exploration of the Solar System,Space program of the United States]
---


This magnified, cropped image showing Jupiter and three of its moons was taken by NASA's OSIRIS-REx spacecraft's MapCam instrument during optical navigation testing for the mission's Earth-Trojan Asteroid Search. The image shows Jupiter in the center, the moon Callisto to the left and the moons Io and Europa to the right. The image was taken at 3:38 a.m. EST on Feb. 9, 2017, when the spacecraft was 75 million miles (120 million kilometers) from Earth and 419 million miles (675 million kilometers) from Jupiter. NASA's Goddard Space Flight Center provides overall mission management, systems engineering and the safety and mission assurance for OSIRIS-REx. OSIRIS-REx is the third mission in NASA's New Frontiers Program.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/nsfc-not021417.php){:target="_blank" rel="noopener"}


