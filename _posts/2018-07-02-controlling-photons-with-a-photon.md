---
layout: post
title: "Controlling photons with a photon"
date: 2018-07-02
categories:
author: "University Of Electro Communications"
tags: [Photon,Quantum mechanics,Quantum information,Optics,Laser cooling,Science,Physics,Theoretical physics,Technology,Applied and interdisciplinary physics]
---


Vacuum chamber with high-finesse optical resonator and cold atoms. In order to take full advantage of quantum information carried by photons, it is important to make them directly interact with each other for information processing. So it is necessary to mediate such interactions with matter to realize effective photon-photon interaction, but light-matter interaction is usually extremely weak in normal media. In order to realize the strong light-matter interaction that is necessary for such devices, Tanji-Suzuki uses a laser-cooled ensemble of 87Rb atoms (~10 uK) trapped within a high-finesse optical resonator (finesse ~50000) in an ultrahigh-vacuum chamber. The realization of such all-optical single-photon devices will be a large step towards deterministic multi-mode entanglement generation as well as high-fidelity photonic quantum gates that are crucial for all-optical quantum information processing, says Tanji-Suzuki.

<hr>

[Visit Link](https://phys.org/news/2018-06-photons-photon.html){:target="_blank" rel="noopener"}


