---
layout: post
title: "The EPA Just Approved the Release of Mosquito-Killing Insects Grown in the Lab"
date: 2017-11-19
categories:
author: ""
tags: [Aedes aegypti,Wolbachia,Mosquito,Aedes albopictus,Causes of death,Organisms,Health,Animals and humans,Insect-borne diseases,Clinical medicine,Animal diseases,Medical specialties,Infectious diseases,Microbiology,Diseases and disorders,Insects and humans,Epidemiology,Immunology,Biology,Health sciences,Zoonoses]
---


On November 3, the EPA approved the release of male Asian tiger mosquitoes infected with a bacterium that prevents them from successfully reproducing. Kentucky-based biotech company MosquitoMate infected the lab-grown male mosquitoes with Wolbachia pipientis, a naturally occurring bacteria found in many insects. In the wild, Wolbachia protects individual mosquitoes from viruses, but male mosquitoes do not bite humans regardless. Nature reports that MosquitoMate has tested Wolbachia-infected A. aegypti mosquitoes in the Florida keys and in Fresno, Calfornia. An Aedes aegypti mosquito.

<hr>

[Visit Link](https://futurism.com/epa-approved-release-mosquito-killing-insects-grown-lab/){:target="_blank" rel="noopener"}


