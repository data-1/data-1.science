---
layout: post
title: "Human cell death captured for first time"
date: 2016-05-08
categories:
author: Tim Mitchell, La Trobe University
tags: [Apoptosis,Immune system,Cell (biology),Innate immune system,Medical specialties,Clinical medicine,Biology,Immunology,Cell biology]
---


An example of a white blood cell experiencing apoptotic long-beaded apoptopodia. The team's work, published in the journal Nature Communications, has determined it is in fact highly regulated. The role of white blood cells is central to our body's innate immune system and much like fighter jet pilots are ejected from their downed aeroplane, we have discovered certain molecules are pushed free from the dying cell, while others are left behind in the 'wreckage' of the cell fragments, Dr Poon said. Never-before-seen vision: The dying white blood cell ejects a string of molecules as it breaks down. Programmed cell death occurs throughout life in essentially all tissues in the human body as part of the normal process of development and death, and the human body has innate mechanisms to clean up these fragments of dead cells.

<hr>

[Visit Link](http://phys.org/news353656272.html){:target="_blank" rel="noopener"}


