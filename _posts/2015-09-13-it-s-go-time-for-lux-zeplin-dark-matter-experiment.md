---
layout: post
title: "It's go time for LUX-Zeplin dark matter experiment"
date: 2015-09-13
categories:
author: Yale University 
tags: [Large Underground Xenon experiment,XENON,Astronomy,Astroparticle physics,Physics,Particle physics,Physical sciences,Science,Astrophysics,Nature]
---


New Haven, Conn. -- From the physics labs at Yale University to the bottom of a played-out gold mine in South Dakota, a new generation of dark matter experiments is ready to commence. LZ's approach posits that dark matter may be composed of Weakly Interacting Massive Particles – known as WIMPs – which pass through ordinary matter virtually undetected. The experiment aims to spot these particles as they move through a container of dense, liquid xenon. The researchers hope that the remaining collisions, the ones involving nuclei, will identify the presence of dark matter. We want to get moving soon, McKinsey said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/yu-igt071814.php){:target="_blank" rel="noopener"}


