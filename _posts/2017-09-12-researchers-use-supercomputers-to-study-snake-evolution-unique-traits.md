---
layout: post
title: "Researchers use supercomputers to study snake evolution, unique traits"
date: 2017-09-12
categories:
author: "Aaron Dubrow, University Of Texas At Austin"
tags: [Evolution,Gene,Speciation,Genetics,Species,Natural selection,Regeneration (biology),Burmese python,Genome,Biology,Life sciences]
---


A Burmese python superimposed on an analysis of gene expression that uncovers how the species changes in its organs upon feeding. To uncover new insights that link variation in DNA with variation in vertebrate form and function, Castoe's group uses supercomputing and data analysis resources at the Texas Advanced Computing Center or TACC, one of the world's leading centers for computational discovery. Making Evolutionary Sense of Secondary Contact  Castoe and his team used a similar genomic approach to understand gene flow in two closely related species of western rattlesnakes with an intertwined genetic history. However, they also found regions of the rattlesnake genome that are important in only one of these two scenarios. The tool uses simulations to statistically determine which changes are meaningful and can help biologists better understand the processes that underlie genetic variation.

<hr>

[Visit Link](https://phys.org/news/2017-06-supercomputers-snake-evolution-unique-traits.html){:target="_blank" rel="noopener"}


