---
layout: post
title: "Comet close-ups"
date: 2015-09-03
categories:
author: "$author"   
tags: [Comets]
---


Science & Exploration Comet close-ups 22/01/2015 68778 views  High-resolution images from ESA’s Rosetta spacecraft reveal an incredible array of surface features on the comet

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Rosetta/Highlights/Comet_close-ups){:target="_blank" rel="noopener"}


