---
layout: post
title: "Toward roads that de-ice themselves"
date: 2015-12-18
categories:
author: American Chemical Society
tags: [American Chemical Society,American Association for the Advancement of Science]
---


As winter approaches, stores, cities and homeowners are stocking up on salt, gravel and sand in anticipation of slippery roads. But this annual ritual in colder climates could soon become unnecessary. Researchers report in ACS' journal Industrial & Engineering Chemistry Research a new road material that could de-ice itself. Every winter, when weather forecasters predict snow or icy conditions, local governments deploy trucks that dust roads with salt, sand or other chemical mixtures to help prevent ice build-up. In that instance, the salt-polymer composite would be evenly embedded throughout the asphalt.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/acs-trt121615.php){:target="_blank" rel="noopener"}


