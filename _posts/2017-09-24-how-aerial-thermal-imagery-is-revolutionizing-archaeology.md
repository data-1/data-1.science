---
layout: post
title: "How aerial thermal imagery is revolutionizing archaeology"
date: 2017-09-24
categories:
author: "Dartmouth College"
tags: [Thermography,Archaeology,Survey (archaeology),Branches of science,Science,Technology]
---


A Dartmouth-led study has demonstrated how the latest aerial thermal imagery is transforming archaeology due to advancements in technology. Today's thermal cameras, commercial drones and photogrammetric software has introduced a new realm of possibilities for collecting site data. But now, aerial thermography makes it possible to gather field survey data across a much larger area in much less time. They analyzed how weather, environment, time of day, ground cover, and archaeological features may affect the results, and compared their findings to earlier research and historical images. I think our results demonstrate aerial thermography's potential to transform how we explore archaeological landscapes in many parts of the world, says Jesse Casana, an associate professor of anthropology at Dartmouth, who has been using drones in aerial thermography for five years in his archaeological research.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-09/dc-hat092117.php){:target="_blank" rel="noopener"}


