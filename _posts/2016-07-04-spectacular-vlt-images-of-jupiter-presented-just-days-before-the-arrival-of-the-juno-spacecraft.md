---
layout: post
title: "Spectacular VLT images of Jupiter presented just days before the arrival of the Juno spacecraft"
date: 2016-07-04
categories:
author: ""
tags: [Juno (spacecraft),European Southern Observatory,Very Large Telescope,Jupiter,Astronomy,Space science,Science,Outer space,Physical sciences,Astronomical objects]
---


They are part of a campaign to create high-resolution maps of the giant planet to inform the work to be undertaken by Juno over the following months, helping astronomers to better understand the gas giant. This false-colour image was created by selecting and combining the best images obtained from many short VISIR exposures at a wavelength of 5 micrometres. The maps do not just give snapshots of the planet, they also reveal how Jupiter's atmosphere has been shifting and changing in the months prior to Juno's arrival. Fletcher  Leigh Fletcher describes the significance of this research in preparing for Juno's arrival: These maps will help set the scene for what Juno will witness in the coming months. Capturing sharp images through the Earth's constantly shifting atmosphere is one of the greatest challenges faced by ground-based telescopes.

<hr>

[Visit Link](http://phys.org/news/2016-06-spectacular-vlt-images-jupiter-days.html){:target="_blank" rel="noopener"}


