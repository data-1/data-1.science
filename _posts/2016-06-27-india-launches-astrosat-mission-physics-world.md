---
layout: post
title: "India launches ASTROSAT mission – Physics World"
date: 2016-06-27
categories:
author: "Michael Banks"
tags: [Astrosat,Indian Space Research Organisation,Space science,Outer space,Physical sciences,Science,Spaceflight,Astronomical objects,Astronautics,Spacecraft,Astronomy]
---


The mission took off yesterday on a Polar Satellite Launch Vehicle from ISRO’s Satish Dhawan Space Centre, located in Sriharikota, Andhra Pradesh. Dubbed ASTROSAT, the Rs3.7bn ($70m) mission will study black holes, neutron stars and active galactic nuclei over a wide wavelength range from visible to hard X-rays. Weighing around 1600 kg, ASTROSAT will operate for five years in a near-equatorial orbit 650 km above the Earth’s surface. Unmatched capability  Sandip Trivedi is director of the Tata Institute of Fundamental Research (TIFR), which led the construction of three of the probe’s instruments – LAXPC, SXT and CZT. “These will give us a capability in X-ray astronomy that is unmatched, in many ways, globally,” says Trivedi.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/sep/29/india-launches-astrosat-mission){:target="_blank" rel="noopener"}


