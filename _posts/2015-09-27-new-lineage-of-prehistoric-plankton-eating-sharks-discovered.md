---
layout: post
title: "New lineage of prehistoric, plankton-eating sharks discovered"
date: 2015-09-27
categories:
author: DePaul University 
tags: [Shark,Megamouth shark,Fossil,Batoidea,Dinosaur,News aggregator,Pseudomegachasma,Paleontology,Animals,Taxa]
---


An international team of scientists has discovered a new lineage of extinct plankton-feeding sharks, Pseudomegachasma, that lived in warm oceans during the age of the dinosaurs nearly 100 million years ago. The fossil sharks had tiny teeth very similar to a modern-day, plankton-eating megamouth shark. The study, A new clade of putative plankton-feeding sharks from the Upper Cretaceous of Russia and the United States, is published in the September issue of the Journal of Vertebrate Paleontology. The study is significant because Pseudomegachasma would represent the oldest known plankton-feeding shark in the fossil record, said Shimada. He added that these sharks would have evolved independent of the four known lineages of modern-day planktivorous cartilaginous fishes: the megamouth sharks, basking sharks, whale sharks, and manta rays.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150921134120.htm){:target="_blank" rel="noopener"}


