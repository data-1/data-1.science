---
layout: post
title: "CRISPR-carrying nanoparticles edit the genome"
date: 2017-11-19
categories:
author: "Anne Trafton"
tags: [Cas9,RNA,CRISPR gene editing,Guide RNA,Genome editing,Virus,Gene,PCSK9,Molecular biology,Biology,Biochemistry,Genetics,Chemistry,Life sciences,Biotechnology,Branches of genetics,Molecular genetics]
---


In a new study, MIT researchers have developed nanoparticles that can deliver the CRISPR genome-editing system and specifically modify genes in mice. “What’s really exciting here is that we’ve shown you can make a nanoparticle that can be used to permanently and specifically edit the DNA in the liver of an adult animal,” says Daniel Anderson, an associate professor in MIT’s Department of Chemical Engineering and a member of MIT’s Koch Institute for Integrative Cancer Research and Institute for Medical Engineering and Science (IMES). Using this approach, in which the guide RNA was still delivered by a virus, the researchers were able to edit the target gene in about 6 percent of hepatocytes, which is enough to treat tyrosinemia. Also, some patients have pre-existing antibodies to the viruses being tested as CRISPR delivery vehicles. In the new Nature Biotechnology paper, the researchers came up with a system that delivers both Cas9 and the RNA guide using nanoparticles, with no need for viruses.

<hr>

[Visit Link](http://news.mit.edu/2017/crispr-carrying-nanoparticles-edit-genome-1113){:target="_blank" rel="noopener"}


