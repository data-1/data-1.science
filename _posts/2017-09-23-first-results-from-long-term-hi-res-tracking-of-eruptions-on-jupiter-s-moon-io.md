---
layout: post
title: "First results from long-term, hi-res tracking of eruptions on Jupiter's moon Io"
date: 2017-09-23
categories:
author: "University of California - Berkeley"
tags: [Volcanism on Io,Io (moon),Volcano,Solar System,Space science,Physical sciences,Planets of the Solar System,Bodies of the Solar System,Planetary science,Astronomy,Astronomical objects,Outer space,Science]
---


Using near-infrared adaptive optics on two of the world's largest telescopes - the 10-meter Keck II and the 8-meter Gemini North, both located near the summit of the dormant volcano Maunakea in Hawaii - University of California, Berkeley astronomers tracked 48 volcanic hot spots on the surface over a period of 29 months from August 2013 through the end of 2015. She and Imke de Pater, a UC Berkeley professor of astronomy and of earth and planetary science, observed the heat coming off of active eruptions as well as cooling lava flows and were able to determine the temperature and total power output of individual volcanic eruptions.They tracked their evolution over days, weeks and sometimes even years. Models for how this heating occurs predict that most of Io's total volcanic power should be emitted either near the poles or near the equator, depending on the model, and that the pattern should be symmetric between the forward- and backward-facing hemispheres in Io's orbit (that is, at longitudes 0-180 degrees versus 180-360 degrees). One key target of interest was Io's most powerful persistent volcano, Loki Patera, which brightens by more than a factor of 10 every 1-2 years. With the renewed activity, the waves traveled clockwise around the lava lake, she noted.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/uoc--frf101716.php){:target="_blank" rel="noopener"}


