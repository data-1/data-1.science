---
layout: post
title: "11 Advanced Linux ‘Grep’ Commands on Character Classes and Bracket Expressions"
date: 2017-03-17
categories:
author: "Jalpan Trivedi"
tags: [Grep,Plain text,Linux,Regular expression,Technology,Software engineering,Software,Computing,Software development,Computers,Computer science,Computer programming,Computer engineering,Computer data]
---


If you will divide the word grep like g/re/p then the meaning of grep is (globally search a regular expression and print) which search pattern from the file and print the line on the screen i.e. standard output. Search Alpha Characters  Similar options like if you want to search line which will start from only [A-Z & a-z] i.e. Alpha Characters . Search Blank Characters  Another options like if you want to search line which will start from [Tab & Space] i.e. Blank Characters. Search Digit Characters  The digit option for grep is also very useful to search line which will start from digit [0-9] i.e. Digit Characters. $ grep ^[[:xdigit:]] tecmint.txt  I have explained the advanced functionality of grep which is very strong and powerful tool to search the pattern in a File.

<hr>

[Visit Link](http://www.tecmint.com/linux-grep-commands-character-classes-bracket-expressions/){:target="_blank" rel="noopener"}


