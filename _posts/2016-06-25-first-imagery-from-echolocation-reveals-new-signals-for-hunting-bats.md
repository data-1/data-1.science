---
layout: post
title: "First imagery from echolocation reveals new signals for hunting bats"
date: 2016-06-25
categories:
author: "eLife"
tags: [Animal echolocation,Bat,Predation,Insect,Cognitive science]
---


Only a third of all bat species are able to hunt for prey on surfaces. In the new study, researchers in Bristol developed a new imaging technique to allow them to enter the bat's world view. The research team found that the acoustic cues available to bats to detect motionless prey don't improve greatly if the profile of the insect seems to stand out more. But surprisingly, the team's data suggest that bat should not look for prey, but search surfaces with which they are familiar, particular smooth ones, and then home in on patches where there are 'echo gaps' in what they'd normally find. Our findings also suggest a new phenomenon of acoustic camouflage, where insects are harder to discern on rough surfaces such as bark, and bats compensate for this by focusing their attention on the simpler, mirror-like surfaces in their patch, says Dr Clare.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/e-fif082715.php){:target="_blank" rel="noopener"}


