---
layout: post
title: "Shallow soils promote savannas in South America"
date: 2017-10-20
categories:
author: Senckenberg Research Institute, Natural History Museum
tags: [Savanna,Rainforest,Tropics,Biodiversity,Soil,Tropical rainforest,Climate,Precipitation,Earth sciences,Natural environment,Ecology,Nature,Physical geography,Earth phenomena]
---


Previous research has shown that precipitation and fire mediate tropical forest and savanna distributions. This is what Liam Langan and his team at the Senckenberg Biodiversity and Climate Centre found when looking at the natural boundary between the lush Amazonian rainforest and the adjoining savanna with sparsely spaced trees, known as the Brazilian Cerrado. Simulation results from computer models and data gained from satellite MODIS on this area don't agree. That fire and precipitation mediate tropical rainforest and savanna biome boundaries has been established empirically and included in models, says Langan. The researchers found however that the probability of observing a rainforest or savanna at a given point in the Amazonas-Cerrado region hinges on a complex interplay of precipitation, fire and tree rooting depth.

<hr>

[Visit Link](https://phys.org/news/2017-10-shallow-soils-savannas-south-america.html){:target="_blank" rel="noopener"}


