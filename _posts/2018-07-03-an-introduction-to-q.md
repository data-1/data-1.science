---
layout: post
title: "An Introduction to Q#"
date: 2018-07-03
categories:
author: "Jan."
tags: []
---


Prerequisites  Please visit my earlier article An Introduction To Quantum Computing to get a basic understanding of Quantum Computing and to also know how to install Quantum Development Kit in Visual Studio 2017. Array Type  We can create an array type of any valid Q# primitive type. Tuple Type Tuple type represents a tuple of values of any given primitive type. It is represented as (T1, T2, T3,…) where T1, T2, T3 is a primitive type. A tuple expression can contain values of multiple primitive types, i.e. a tuple of type (Int, Double, Result) is a valid tuple.

<hr>

[Visit Link](https://dzone.com/articles/an-introduction-to-q?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


