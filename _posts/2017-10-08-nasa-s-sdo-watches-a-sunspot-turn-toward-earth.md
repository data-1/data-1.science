---
layout: post
title: "NASA's SDO watches a sunspot turn toward Earth"
date: 2017-10-08
categories:
author: "Nasa'S Goddard Space Flight Center"
tags: [Solar Dynamics Observatory,Sun,Sunspot,Solar cycle,Space science,Space plasmas,Astronomical objects known since antiquity,Bodies of the Solar System,Solar System,Stellar astronomy]
---


Such sunspots are a common occurrence on the sun, but are less frequent as we head toward solar minimum, which is the period of low solar activity during its regular approximately 11-year cycle. This sunspot is the first to appear after the sun was spotless for two days, and it is the only sunspot group at this moment. Like freckles on the face of the sun, they appear to be small features, but size is relative: The dark core of this sunspot is actually larger than Earth. Credit: NASA's Goddard Space Flight Center/SDO/Joy Ng, producer  An active region on the sun—an area of intense and complex magnetic fields—has rotated into view on the sun and seems to be growing rather quickly in this video captured by NASA's Solar Dynamics Observatory between July 5-11, 2017. Such sunspots are a common occurrence on the sun, but are less frequent as we head toward solar minimum, which is the period of low solar activity during its regular approximately 11-year cycle.

<hr>

[Visit Link](https://phys.org/news/2017-07-nasa-sdo-sunspot-earth.html){:target="_blank" rel="noopener"}


