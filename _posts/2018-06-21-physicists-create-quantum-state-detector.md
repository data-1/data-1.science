---
layout: post
title: "Physicists create quantum state detector"
date: 2018-06-21
categories:
author: "Moscow Institute Of Physics"
tags: [Superconductivity,Josephson effect,SQUID,Quantum mechanics,Magnetic flux quantum,Wave,Science,Physical quantities,Physical sciences,Metrology,Electrical engineering,Electromagnetism,Applied and interdisciplinary physics,Electricity,Theoretical physics,Physics]
---


The terms wave and amplitude imply that the objects described by wave functions behave much like waves. Vladimir Gurtovoi, a senior research scientist at MIPT's Laboratory of Artificial Quantum Systems and one of the authors of the paper, commented on the results: Our technology is remarkably simple: We use a material that is quite typical for superconductivity research and standard fabrication techniques such as electron-beam lithography and high vacuum deposition of aluminum. In a variable magnetic field, the physicists observed periodic voltage jumps corresponding to the changes in the quantum states of the superconducting loops of the detector. By conducting a theoretical analysis of the operation of the device, the researchers showed (see the appendix) that the superconducting current through the two Josephson junctions in the new interferometer is equal to the sum of the individual currents through each of the junctions with some phase corrections, which lead to voltage jumps occurring when the quantum numbers associated with the states of the two loops change. The double-contour interferometer with one of the loops replaced with a qubit may be used to direct the detection of qubit quantum states.

<hr>

[Visit Link](https://phys.org/news/2018-01-physicists-quantum-state-detector.html){:target="_blank" rel="noopener"}


