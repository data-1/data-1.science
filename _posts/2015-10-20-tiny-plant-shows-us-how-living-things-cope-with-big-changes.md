---
layout: post
title: "Tiny plant shows us how living things cope with big changes"
date: 2015-10-20
categories:
author: University of Edinburgh
tags: [Life,Evolution,News aggregator,Biology,Nature]
---


A small freshwater plant that has evolved to live in harsh seawater is giving scientists insight into how living things adapt to changes in their environment. In adapting to new surroundings, organisms must develop ways to perform everyday functions, such as securing food and oxygen, and reproducing. They found that freshwater algae adapted to seawater in two stages. As salt levels increased, changes enabled those genes to stay switched on -- indicative of a process known as epigenetics. Josianne Lachapelle, of the University of Edinburgh's School of Biological Sciences, who led the study, said: Our approach enables a new way to understand how living things evolve new ways of life during major transitions.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151015115746.htm){:target="_blank" rel="noopener"}


