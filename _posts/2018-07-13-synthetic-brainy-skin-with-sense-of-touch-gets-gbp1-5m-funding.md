---
layout: post
title: "Synthetic 'brainy skin' with sense of touch gets £1.5m funding"
date: 2018-07-13
categories:
author: "University Of Glasgow"
tags: [Neuromorphic engineering,Sense,Branches of science,Cognitive science,Computing,Neuroscience,Technological change,Emerging technologies,Technology]
---


Credit: University of Glasgow  A robotic hand covered in 'brainy skin' that mimics the human sense of touch is being developed by scientists. University of Glasgow's Professor Ravinder Dahiya has plans to develop ultra-flexible, synthetic Brainy Skin that 'thinks for itself'. His futuristic research, called neuPRINTSKIN (Neuromorphic Printed Tactile Skin), has just received another £1.5m funding from the Engineering and Physical Science Research Council (EPSRC). To achieve this, Professor Dahiya will add a new neural layer to the e-skin that he has already developed using printing silicon nanowires. Professor Dahiya added: By adding a neural layer underneath the current tactile skin, neuPRINTSKIN will add significant new perspective to the e-skin research, and trigger transformations in several areas such as robotics, prosthetics, artificial intelligence, wearable systems, next-generation computing, and flexible and printed electronics.

<hr>

[Visit Link](https://phys.org/news/2018-07-robotic-brainy-skin-mimics-human.html){:target="_blank" rel="noopener"}


