---
layout: post
title: "Breakthrough enables screening millions of human antibodies for new drug discovery"
date: 2018-04-20
categories:
author: "University Of Kansas"
tags: [Antibody,Vaccine,Virus,Drug discovery,Infection,National Institutes of Health,Biology,Medical specialties,Life sciences,Clinical medicine,Medicine,Biotechnology,Immunology,Health sciences]
---


However, single-cell cloning holds several major drawbacks because of its very high costs and is limited to sampling a tiny fraction of the human antibody inventory. Non-natural gene pairing has been used in the past because it's so much easier to do, but often the quality of antibodies discovered is not good enough for an effective drug, and the synthetic nature of those antibodies makes it difficult to understand the true human immune response, DeKosky said. Researchers next will apply this new platform technology to find additional promising antibodies that could serve as the basis for drug therapies. Promising sources to discover new antibodies include donated blood samples from HIV patients with powerful immune responses against the virus, and also individuals who have received vaccines so that we can understand how those vaccines are working. This project was several years in the making, and many undergraduates, grad students and postdocs involved have now transitioned to the next stage of their career.

<hr>

[Visit Link](https://phys.org/news/2018-01-breakthrough-enables-screening-millions-human.html){:target="_blank" rel="noopener"}


