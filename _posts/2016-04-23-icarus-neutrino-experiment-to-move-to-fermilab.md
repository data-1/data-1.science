---
layout: post
title: "ICARUS neutrino experiment to move to Fermilab"
date: 2016-04-23
categories:
author:  
tags: [ICARUS experiment,Fermilab,Neutrino,Particle physics,Science,Physics,Particle physics facilities]
---


The 760-ton, 20-metre-long detector took data for the ICARUS experiment at the Italian Institute for Nuclear Physics (link is external)' (INFN) Gran Sasso National Laboratory in Italy from 2010 to 2014, using a beam of neutrinos sent through the earth from CERN. The liquid-argon time projection chamber is a new and very promising technology that we originally developed in the ICARUS collaboration from an initial table-top experiment all the way to a large neutrino detector, Rubbia said. Scientists plan to transport the detector to the United States in 2017. Discovering this fourth type of neutrino would revolutionize physics, changing scientists' entire picture of the universe and how it works. The arrival of ICARUS and the construction of this on-site research programme is a lofty goal in itself, said Fermilab Director Nigel Lockyer.

<hr>

[Visit Link](http://phys.org/news348997906.html){:target="_blank" rel="noopener"}


