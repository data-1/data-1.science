---
layout: post
title: "How does my therapist rate?"
date: 2015-12-21
categories:
author: University of Southern California
tags: [Psychotherapy,Empathy,Computer science,Machine learning,Speech,Research,Medicine,Engineering,Hospital,Speech recognition,Cognition,Technology,Psychology,Branches of science,Cognitive science]
---


Leveraging developments in automatic speech recognition, natural language processing and machine learning, researchers Bo Xiao (Ming Hsieh Department of Electrical Engineering at the USC Viterbi School of Engineering), Zac E. Imel (Department of Educational Psychology at the University of Utah), Panayiotis G. Georgiou (Ming Hsieh Department of Electrical Engineering at the USC Viterbi School of Engineering), David C. Atkins (Department of Psychiatry and Behavioral Sciences at the University of Washington) and Shrikanth S. Narayanan (Ming Hsieh Department of Electrical Engineering at the USC Viterbi School of Engineering), developed software to detect high-empathy or low-empathy speech by analyzing more than 1,000 therapist-patient sessions. Their methodology is documented in a forthcoming article titled, 'Rate My Therapist': Automated Detection of Empathy in Drug and Alcohol Counseling via Speech and Language Processing, and according to the authors, is the first study of its kind to record therapy sessions and automatically determine the quality of a therapy session based on a single characteristic. The authors taught their algorithm to recognize empathy via data from training sessions for therapists, specifically looking at therapeutic interactions with individuals coping with addiction and alcoholism. ...The sort of technology our team of engineers and psychologists is developing may offer one way to help providers get immediate feedback on what they are doing - and ultimately improve the effectiveness of mental health care, said Zac Imel, a University of Utah professor of educational psychology and the paper's corresponding author. USC Viterbi is ranked among the top graduate programs in the world and enrolls more than 6,500 undergraduate and graduate students taught by 185 tenured and tenure-track faculty, with 73 endowed chairs and professorships.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/uosc-hdm120115.php){:target="_blank" rel="noopener"}


