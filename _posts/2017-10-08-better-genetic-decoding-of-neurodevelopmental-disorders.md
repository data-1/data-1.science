---
layout: post
title: "Better genetic decoding of neurodevelopmental disorders"
date: 2017-10-08
categories:
author: "Université libre de Bruxelles"
tags: [Mental disorder,Autism,Development of the nervous system,Intellectual disability,Signs and symptoms,Clinical medicine,Medicine,Health sciences,Behavioural sciences,Human diseases and disorders,Diseases and disorders,Neuroscience,Health,Causes of death,Mental disorders]
---


A key question in biology is understanding how the brain works. Its basic working units, the neurons, transmit information in the form of electrical impulses and chemical signals. Neurodevelopmental disorders (NDDs) are a group of frequent and often severe pediatric conditions that can manifest, for example, as intellectual disability, autism or early-onset psychiatric symptoms. The recent development of higher resolution genetic diagnostic tools has underlined the prevalence of genetic anomalies, such as copy number variations (for example, loss of a gene), in children with NDDs. The deletion of these new regions were found statistically associated with developmental delay and intellectual disability in two independent patient cohorts, supporting the pathogenic role of these new elements into the neurodevelopmental symptoms of both HUDERF patients.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171005141754.htm){:target="_blank" rel="noopener"}


