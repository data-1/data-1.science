---
layout: post
title: "New tool for efficiently validating the accuracy of CRISPR-Cas9 reactions"
date: 2016-02-05
categories:
author: Institute for Basic Science
tags: [Cas9,Genome editing,DNA,Biology,Life sciences,Branches of genetics,Molecular genetics,Nucleic acids,Genomics,Biological engineering,Biological techniques and tools,Omics,Modification of genetic information,Genetics techniques,Chemistry,Macromolecules,Bioinformatics,Nucleotides,Biochemistry,Molecular biology,Biotechnology,Genetics]
---


First author Daesik Kim explains that Digenome-seq differs from other cell-based methods in that it detects DNA cleavages in vitro, rather than in cells, using cell-free genomic DNA. In the study, researchers identified on- and off-target sites cleaved in vitro and measured indel frequencies at hundreds of off-target sites in cells which could potentially become unintended mutations. This multiplex process is faster, more efficient and more cost effective than previous methods. IBS Director Jin-Soo Kim said CRISPR-Cas9 is an incredible scientific breakthrough and will continue to be the research focus of biologists around the world. He added that This technique can be used to choose target sites with the least off-target effects, a method essential for therapeutic applications of CRISPR-Cas9.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/ifbs-ntf020416.php){:target="_blank" rel="noopener"}


