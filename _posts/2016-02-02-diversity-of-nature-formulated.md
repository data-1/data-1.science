---
layout: post
title: "Diversity of nature formulated"
date: 2016-02-02
categories:
author: University of Copenhagen - Niels Bohr Institute
tags: [Predation,Food web,Ecosystem,Environmental science,Ecology,Natural environment,Systems ecology,Biogeochemistry,Environmental social science,Nature,Earth sciences,Systems biology,Biogeography]
---


A group of biophysicists from the Niels Bohr Institute have therefore analysed data and calculated how the species in an area affect each other and how an ecosystem can be in balance or out of balance. We can see that it is extremely important that there is a balance between who eats what and how many are hunting the same prey, explains Namiko Mitarai, Associate Professor of biophysics at the Niels Bohr Institute at the University of Copenhagen. Namiko Mitarai explains that they first used the classical calculations from the theories about ecosystems that say that two predators cannot exist simultaneously if they both live exclusively on the same prey. Predators may also be prey  Whatever is eating grass is eaten by a carnivore, which is eaten by another carnivore and so on. What they can also see is the combination of species in the different links of the food chain.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/uoc--don020116.php){:target="_blank" rel="noopener"}


