---
layout: post
title: "ANU Team Achieves Record Solar Thermal Efficiency Of 97%"
date: 2017-09-25
categories:
author: "Guest Contributor, David Waterworth, Iqtidar Ali, Written By"
tags: [Solar energy,Solar thermal energy,Concentrated solar power,Renewable energy,Thermal power station,Energy,Energy and the environment,Climate change mitigation,Renewable resources,Environmental technology,Energy conversion,Sustainable development,Technology,Energy technology,Sustainable energy,Sustainable technologies,Physical quantities,Nature]
---


By Sophie Vorrath  A team of Australian National University scientists has brought economically competitive, grid-scale solar thermal energy generation closer to reality, after achieving a new record in efficiency for the technology that could compete with the cost of electricity from fossil fuels. The ANU team, whose CST technology harnesses the power of the sun using a 500 square meter solar concentrator dish, made the breakthrough by redesigning the system’s receiver in a way that halved its convection losses and boosted its conversion of sunlight into steam from 93 per cent to 97 per cent. According to the ANU’s Dr John Pye, the new design could result in a 10 per cent reduction in the cost of solar thermal electricity. Heat that does leak out of the cavity can be absorbed by the cooler water around the hat’s brim, the team said. Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2016/08/25/anu-team-achieves-record-solar-thermal-efficiency-97/){:target="_blank" rel="noopener"}


