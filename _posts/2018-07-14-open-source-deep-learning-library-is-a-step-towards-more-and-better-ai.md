---
layout: post
title: "Open-Source Deep Learning Library Is a Step Towards More and Better AI"
date: 2018-07-14
categories:
author: "Dec."
tags: []
---


If you need an expert on hand regardless, why do you need an AI capable of deep learning? In October, AWS and Microsoft announced that they were working together to create Gluon, an open-source deep learning library. The companies have stated that Gluon is currently intended to assist with creating machine learning models for cloud-based applications, mobile apps, and any dedicated devices that may need AI. If Gluon proves successful, it could represent a major step forward not merely for Microsoft and AWS, but the entire development community. This isn't the first effort from these two companies to improve AI.

<hr>

[Visit Link](https://dzone.com/articles/open-source-deep-learning-library-is-a-step-toward?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


