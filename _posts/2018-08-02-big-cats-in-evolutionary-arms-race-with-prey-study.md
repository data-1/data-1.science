---
layout: post
title: "Big cats in evolutionary arms race with prey: study"
date: 2018-08-02
categories:
author: "Marlowe Hood"
tags: [Predation,Cheetah,Lion,Animals]
---


When chased, zebras and impalas compensate for their slower speed by moving unpredictably to evade outstretched claws  Lions and cheetah are faster, stronger and no less agile than their prey, but zebras and impalas compensate with a surprising tactic, researchers said Wednesday: slow down, and keep the big cats guessing. The proof is in the kill rate: lions (which hunt zebra) and cheetah (which target impalas) fail two out of three times when they give chase. Their muscles were also 20 percent more powerful. The prey define the hunt and know not to just run away but to turn at the last moment, explained Wilson. For prey species, avoiding the claws and maw of big cats is not the only survival skill required.

<hr>

[Visit Link](https://phys.org/news/2018-01-big-cats-evolutionary-arms-prey.html){:target="_blank" rel="noopener"}


