---
layout: post
title: "New tool enables scientists to interpret 'dark matter' DNA: Breakthrough technology opens the door to identifying new drug targets that could treat many genetic diseases"
date: 2016-04-05
categories:
author: Gladstone Institutes
tags: [Gene,Enhancer (genetics),Genetics,DNA,Non-coding DNA,Human genome,Genome,Mutation,News aggregator,Biotechnology,Branches of genetics,Life sciences,Biochemistry,Biology,Molecular biology]
---


The computational method, called TargetFinder, can predict where non-coding DNA--the DNA that does not code for proteins--interacts with genes. Most genetic mutations that are associated with disease occur in enhancers, making them an incredibly important area of study, said senior author Katherine Pollard, PhD, a senior investigator at the Gladstone Institutes. When an enhancer is far away from the gene it affects, the two connect by forming a three-dimensional loop, like a bow on the genome. Using machine learning technology, the researchers analyzed hundreds of existing datasets from six different cell types to look for patterns in the genome that identify where a gene and enhancer interact. They discovered several patterns that exist on the loops that connect enhancers to genes.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/04/160404134029.htm){:target="_blank" rel="noopener"}


