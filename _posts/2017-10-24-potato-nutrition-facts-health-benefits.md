---
layout: post
title: "Potato nutrition facts & health benefits"
date: 2017-10-24
categories:
author: Jessie Szalay, Jonathan Gordon
tags: [Potato,Carbohydrate,Dietary fiber,Cooking,Glycemic index,Vegetable,Food,Vitamin,Diet (nutrition),Human nutrition,Solanine,French fries,Vitamin B6,Phytochemical,Nutrient,Nutrition facts label,Obesity,Food and drink,Health,Nutrition]
---


In fact, a study published in 2017 in The American Journal of Clinical Nutrition (opens in new tab) found that people who ate fried potatoes twice a week saw an increased risk of death. They may also help with digestion , heart health, blood pressure and even cancer prevention. Use it as a carbohydrate rather than as your only vegetable, she said. The Harvard School of Public Health tracked the diet and lifestyle of 120,000 men and women for about 20 years and found that people who increased their consumption of French fries and baked or mashed potatoes gained more weight over time — as much as 3.4 lbs. However you cook a potato, try to eat the skin.

<hr>

[Visit Link](https://www.livescience.com/45838-potato-nutrition.html){:target="_blank" rel="noopener"}


