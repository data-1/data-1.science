---
layout: post
title: "How brain architecture leads to abstract thought"
date: 2015-12-21
categories:
author: University of Massachusetts Amherst
tags: [Deep learning,Brain,Artificial intelligence,Abstraction,Consciousness,Thought,Cognition,Functional magnetic resonance imaging,Hava Siegelmann,Neuroscience,Cognitive psychology,Cognitive neuroscience,Science,Concepts in metaphysics,Cognitive science,Branches of science,Interdisciplinary subfields,Cybernetics,Neuropsychology,Technology,Academic discipline interactions,Psychology]
---


The authors say their work demonstrates not only the basic operational paradigm of cognition, but shows that all cognitive behaviors exist on a hierarchy, starting with the most tangible behaviors such as finger tapping or pain, then to consciousness and extending to the most abstract thoughts and activities such as naming. They then processed the massive repository of fMRI data. If the balance arm describes the total brain activity for a particular cognitive behavior, the right pan will be lower, creating a negative slope, when most activity is in shallow areas, and the left pan will go lower when most activity is deeper, creating a positive slope. The researchers summed all neural activity for a given behavior over all related fMRI experiments, then analyzed it using the slope algorithm. Siegelmann says this work will have great impact in computer science, especially in deep learning.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/uoma-hba121615.php){:target="_blank" rel="noopener"}


