---
layout: post
title: "X-ray pulses create 'molecular black hole': The strongest ionisation of a molecule yet is providing important insights for analyzing biomolecules with X-ray lasers"
date: 2017-10-08
categories:
author: "Deutsches Elektronen-Synchrotron DESY"
tags: [DESY,SLAC National Accelerator Laboratory,Electron,Free-electron laser,Particle accelerator,Physical sciences,Physics,Chemistry,Science,Nature]
---


Scientists have used an ultra-bright pulse of X-ray light to turn an atom in a molecule briefly into a sort of electromagnetic black hole. Unlike a black hole in space, the X-rayed atom does not draw in matter from its surroundings through the force of gravity, but electrons with its electrical charge -- causing the molecule to explode within the tiniest fraction of a second. The researchers used the free-electron laser LCLS at the SLAC National Accelerator Laboratory in the US to bath iodomethane (CH3I) molecules in intense X-ray light. As far as we are aware, this is the highest level of ionisation that has ever been achieved using light, explains the co-author Robin Santra from the research team, who is a leading DESY scientist at the Center for Free-Electron Laser Science (CFEL). The resulting strong positive charge means that the iodine atom then sucks electrons away from the methyl group, like a sort of atomic black hole.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/05/170531133323.htm){:target="_blank" rel="noopener"}


