---
layout: post
title: "First direct exploration of magnetic fields in the upper solar atmosphere"
date: 2017-10-08
categories:
author: "National Institutes Of Natural Sciences"
tags: [Sun,Polarization (waves),Ultraviolet,Light,Polarimetry,Optics,Astronomy,Electromagnetic radiation,Physical sciences,Electromagnetism,Space science,Physical phenomena,Radiation,Physics,Electrodynamics,Science]
---


Polarization spectra of the hydrogen Lyman-α line from the Sun taken by the CLASP sounding rocket experiment. CLASP is a project to investigate the magnetic fields in the upper chromosphere and the transition region, using the hydrogen Lyman-α line in UV. Credit: NAOJ, JAXA, NASA/MSFC  The researchers discovered that the hydrogen Lyman-α line from the Sun is actually polarized. To investigate if the measured polarization was affected by the magnetic field, the team observed 3 different wavelength ranges: the core of the hydrogen Lyman-α line (121.567 nm), whose polarization is affected by even a weak magnetic field; an ionized silicon emission line (120.65 nm) whose polarization is affected only by a relatively strong magnetic field; and the wing of the hydrogen Lyman-α spectral line, which is not sensitive to magnetically induced polarization changes. A, B, C, and D correspond to the areas labeled in Figure 1. Credit: NAOJ  Dr. Ryoko Ishikawa, project scientist for the Japanese CLASP team, describes the significance of the results, The successful observation of polarization indicative of magnetic fields in the upper chromosphere and the transition region means that ultraviolet spectropolarimetry has opened a new window to such solar magnetic fields, allowing us to see new aspects of the Sun.

<hr>

[Visit Link](https://phys.org/news/2017-05-exploration-magnetic-fields-upper-solar.html){:target="_blank" rel="noopener"}


