---
layout: post
title: "Best of Opensource.com: Government"
date: 2015-12-24
categories:
author: "Melanie Chernoff"
tags: [Open source,Digital Single Market,Red Hat,European Union,Business,Public sphere,Technology,Economy]
---


Each year, I reflect on Opensource.com's top government stories of the year. We saw that investment theme echoed in articles from Europe, including Croatia's proposal for an open source policy (by Josip Almasi) and the EU's Digital Single Market Strategy (by Paul Brownell). This shift from acquiring technology as a product to investing in innovation leads one to wonder: Should governments develop their own open source software? It's a great read, and a good question to ponder as we ring in the new year. Dr. Wheeler, widely recognized for his expertise on use of open source software in the US government, found that [open source software] is being used in [the US government], as well as being released by the government (as both minor improvements and whole new projects), and the government is receiving benefits from doing so.

<hr>

[Visit Link](https://opensource.com/government/15/12/best-of-government-2015){:target="_blank" rel="noopener"}


