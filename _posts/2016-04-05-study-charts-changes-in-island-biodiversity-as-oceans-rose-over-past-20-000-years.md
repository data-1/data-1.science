---
layout: post
title: "Study charts changes in island biodiversity as oceans rose over past 20,000 years"
date: 2016-04-05
categories:
author: Bob Yirka
tags: [Biodiversity,Sea level rise,Extinction,Ice age,Endemism,Climate,Sea level,Biogeography,Nature,Environmental science,Ecology,Natural environment,Physical geography,Earth sciences]
---


For many years, the main themes regarding island biodiversity fell into two categories, degree of isolation and island size—it was believed that both contributed significantly to the degree of diversity for any given island as well as to the numbers of endemic species. To learn more about how sea level rise related to diversity, the researchers looked at 184 oceanic islands across the globe over the course of the past 21,000 years—which covered the time from the last glacial maximum, till the present day—focusing most specifically on data regarding land area, degree of isolation, elevation, the number of islands in any given chain, and of course temperature and precipitation levels. In looking at patterns that emerged, the researchers found that the degree of endemic species is higher today for islands that had a lot more land mass back then. Credit: Manuel Steinbauer  More information: Patrick Weigelt et al. Late Quaternary climate change shapes island biodiversity, Nature (2016). Patrick Weigelt et al. Late Quaternary climate change shapes island biodiversity,(2016).

<hr>

[Visit Link](http://phys.org/news/2016-03-island-biodiversity-oceans-rose-years.html){:target="_blank" rel="noopener"}


