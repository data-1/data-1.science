---
layout: post
title: "Auditing, Hardening and Security"
date: 2018-06-28
categories:
author: "Michael Boelen"
tags: [Localhost,Linux,Port (computer networking),Patch (computing),Kernel (operating system),Package manager,Operating system,Computer networking,Information technology management,Software,Computers,System software,Computer science,Technology,Computing,Computer architecture,Software engineering,Software development,Information Age,Information technology,Computer engineering]
---


Linux system hardening takes a good amount of understanding about how the Linux kernel works. After completing this guide, you will know more about:  What system hardening means specifically for Linux  What steps can be taken to improve the overall security of your system  Why technical audits are a powerful way to keep you secure  How to do regular technical audits on Linux systems  Let’s start with Linux hardening! An audit is typically focused on business processes or on the implementation of technical security measures. For that reason, you firewall configuration typically will have to allow all traffic on the 127.0.0.0/8 network. There is a security tool available for most parts of the system and its software.

<hr>

[Visit Link](https://linux-audit.com/how-to-secure-linux-systems-auditing-hardening-and-security/){:target="_blank" rel="noopener"}


