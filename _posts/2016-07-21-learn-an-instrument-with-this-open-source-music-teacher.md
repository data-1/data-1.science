---
layout: post
title: "Learn an instrument with this open source music teacher"
date: 2016-07-21
categories:
author: "Tomasz Bojczuk"
tags: [Music,Mobile app,Test (assessment),Real-time computing,Communication,Computing,Technology]
---


But, the heart of Nootka is the exercises and exams. The app asks a question which can be: a single note or whole melody on the score, a position on the guitar, a played melody, or just a note name. You can play on a real instrument, write the score of a melody you're listening to, or simply enter a note name. When you are exercising with Nootka it can be like a friendly teacher. It will show clues or corrected answers when you make a mistake.

<hr>

[Visit Link](https://opensource.com/life/16/7/introducing-nootka){:target="_blank" rel="noopener"}


