---
layout: post
title: "Understanding how cells follow electric fields"
date: 2016-05-04
categories:
author: Uc Davis
tags: [Dictyostelid,Electrotaxis,Privacy]
---


Many living things can respond to electric fields, either moving or using them to detect prey or enemies. UC Davis dermatology professor Min Zhao, Peter Devroetes at Johns Hopkins University and colleagues hope to unravel how these responses work, studying both body cells and Dictyostelium discoideum, an amoeba that lives in soil. In a paper just published in the journal Science Signaling, Zhao and colleagues screened Dictyostelium for genes that affect electrotaxis. Other genes associated with electrotaxis in Dictyostelium were also linked to the same pathway. Right now, no one nows how cells detect these very weak electric fields, Zhao said.

<hr>

[Visit Link](http://phys.org/news352044807.html){:target="_blank" rel="noopener"}


