---
layout: post
title: "Expert system for early diagnosis of schizophrenia"
date: 2015-12-21
categories:
author: Inderscience Publishers
tags: [Schizophrenia,Artificial intelligence,Diagnosis of schizophrenia,Diseases and disorders,Mental disorders,Mental health,Health,Clinical medicine,Abnormal psychology,Human diseases and disorders,Cognitive science,Causes of death,Cognition,Medicine,Psychology,Behavioural sciences]
---


However, additional medical evidence based on such an algorithm might be useful in early diagnosis, according to work published in the International Journal of Intelligent Systems Technologies and Applications. Pawan Kumar Singh and Ram Sarkar of the Department of Computer Science and Engineering, at Jadavpur University, in West Bengal, India, explain how expert systems, usually reserved for problem solving, might also be useful in medical diagnostics, particularly in psychiatry. An ES comprises two main components: a knowledge base containing facts from a particular field and a reasoning, or inference, engine that uses logical relations to process inputs by working with the information in the knowledge base. No single approach is widely considered effective for all patients, they add. and Sarkar, R. (2015) 'A simple and effective expert system for schizophrenia detection', Int.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-11/ip-esf110515.php){:target="_blank" rel="noopener"}


