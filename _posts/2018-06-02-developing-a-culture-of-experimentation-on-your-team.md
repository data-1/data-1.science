---
layout: post
title: "Developing a culture of experimentation on your team"
date: 2018-06-02
categories:
author: "Catherine Louis"
tags: [Experiment,Innovation,Hypothesis,Culture,Strategic management,Advertising,Communication,Business]
---


So the question for your innovative organization becomes How do we create a culture that allows us to be comfortable with trying out new ideas, methods, and activities using a scientific procedure? You need to run everything by me before conducting a test with the customer. Create a simple test experiment that you can begin to work on today. Each card begins by stating the hypothesis, then the test, an accompanied metric, and criteria for success. Anyone can run a failed experiment because these failed experiments mean we learn.

<hr>

[Visit Link](https://opensource.com/article/18/5/cultivating-attitude-experimentation-team){:target="_blank" rel="noopener"}


