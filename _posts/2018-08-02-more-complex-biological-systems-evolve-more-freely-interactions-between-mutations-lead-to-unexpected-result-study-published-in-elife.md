---
layout: post
title: "More complex biological systems evolve more freely: Interactions between mutations lead to unexpected result | Study published in eLife"
date: 2018-08-02
categories:
author: "Institute of Science and Technology Austria"
tags: [Gene,Epistasis,Evolution,Transcription (biology),Phenotype,Mutation,Genetics,Transcription factor,Escherichia coli,Branches of genetics,Evolutionary biology,Biological evolution,Molecular genetics,Chemistry,Biochemistry,Biotechnology,Life sciences,Molecular biology,Biology]
---


Evolution acts on changes in the phenotype, which occur when mutations change the underlying genotype. Researchers at the Institute of Science and Technology Austria (IST Austria) found that in a gene regulatory system in the bacterium Escherichia coli, the more components that are mutated, the more freely the system can evolve. When 1+1 does not equal 2  They found that the system evolves more freely because mutations in the two components interact with each other, a phenomenon they call intermolecular epistasis. This means that mutations interact, giving the whole system more freedom to change and evolve. In this study, the researchers give a mechanistic understanding of how the mutations in two different molecules interact, explains Mato Lagator: Most excitingly, we show that -- in this gene regulatory system -- most of the epistasis arises from the genetic structure of the system.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/12/171219110038.htm){:target="_blank" rel="noopener"}


