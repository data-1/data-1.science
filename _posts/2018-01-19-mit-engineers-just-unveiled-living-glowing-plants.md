---
layout: post
title: "MIT engineers just unveiled living, glowing plants"
date: 2018-01-19
categories:
author: "Lacy Cooke"
tags: []
---


Postdoctoral researcher Seon-Yeong Kwak led a team of engineers and scientists to instill the plants with the same enzyme that makes fireflies sparkle. The light is ultimately powered by the energy metabolism of the plant itself.”  Continue reading below Our Featured Videos  Plant lamps or even tree street lights could brighten our world in the future thanks to recent research on glowing plants. The scientists showed they can also turn off the light by adding nanoparticles with a luciferase inhibitor, so they think they could eventually create plants that stop emitting light in response to conditions like sunlight. Join Our Newsletter Receive the latest in global news and designs building a better future. + Nano Letters  + MIT News  Images via Seon-Yeong Kwak

<hr>

[Visit Link](https://inhabitat.com/mit-engineers-just-unveiled-living-glowing-plants){:target="_blank" rel="noopener"}


