---
layout: post
title: "Pollinator biodiversity"
date: 2018-07-12
categories:
author: "National Science Foundation"
tags: [Pollinator,Bee,Pollination,Flower,Bumblebee,Bees,Organisms]
---


These generalist pollen foragers are widespread in the western U.S. and much of western/southern Mexico. The only job requirement is that they transfer pollen from stamen to pistil (a flowering plant's male and female organs). Interestingly, it also was observed to be the dominant bumblebee species in San Diego in 2016. Credit: K. James Hung, UC San Diego  NSF-funded researcher Rachael Winfree and her team at Rutgers University revealed just how important pollinator biodiversity is for crops in a recent study conducted across dozens of watermelon, cranberry and blueberry farms in the mid-Atlantic United States. Credit: K. James Hung, UC San Diego  Hung's research revealed that habitat fragmentation due to human activity reduces bee diversity and creates a shift in natural seasonal changes that influences the number and type of bees present, affecting pollination services. The loss of diversity and changes to seasonal turnover of bee species may threaten plant pollination in the community and potentially even crops that rely on wild bee species for pollination.

<hr>

[Visit Link](https://phys.org/news/2018-07-pollinator-biodiversity.html){:target="_blank" rel="noopener"}


