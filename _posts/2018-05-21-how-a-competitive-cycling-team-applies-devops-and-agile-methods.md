---
layout: post
title: "How a competitive cycling team applies DevOps and agile methods"
date: 2018-05-21
categories:
author: "Conor Delanbanque"
tags: [DevOps,Red Hat,Business,Technology,Computing]
---


In a cycling team, everyone works for the success of the entire organization, not just for themselves. The DevOps architect or engineer in your organization (Disclaimer: Yes, I believe that it’s everyone’s responsibility to practice DevOps methodologies, but often there is a person who has the strongest knowledge of DevOps and how to implement it across the organization) does not spend their day thinking about how to show off about some awesome code they wrote or the system they built. They won’t understand our failures and what we have learned to advance to where we are now. For the cycling team, these processes worked very well, and they continued to improve. It’s so much easier.” It wasn’t easier to use email than Slack; they just didn’t want to change and learn to use a new tool, even if it would take only a few hours.

<hr>

[Visit Link](https://opensource.com/article/18/5/agile-devops-cycling){:target="_blank" rel="noopener"}


