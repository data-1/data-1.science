---
layout: post
title: "Making cows more environmentally friendly"
date: 2017-09-10
categories:
author: "Royal Botanic Gardens"
tags: [Livestock,Environmental impact of meat production,Climate change,Cattle,Agriculture,Methane emissions,Global environmental issues,Climate variability and change,Environmental impact,Natural environment]
---


Dr Mark Lee, a research fellow in Natural Capital & Plant Health at the Royal Botanic Gardens, Kew who led the research says; The vicious cycle we are seeing now is that ruminant livestock such as cattle produce methane which warms our planet. There are several reasons why rising temperatures may make plants tougher for grazing livestock to digest. This is a pressing concern, because climate change is likely to make plants tougher for grazing cattle, increasing the amount of methane that the animals breathe out into the atmosphere. Methane production is generally expected to increase all around the world, with hotspots identified in North America, Central and Eastern Europe, and Asia, where the effects of climate change may be the most severe. Many of these regions are where livestock farming is growing most rapidly.

<hr>

[Visit Link](https://phys.org/news/2017-03-cows-environmentally-friendly.html){:target="_blank" rel="noopener"}


