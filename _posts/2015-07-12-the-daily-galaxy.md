---
layout: post
title: "The Daily Galaxy"
date: 2015-07-12
categories:
author: ""   
tags: []
---


The Galaxy Report newsletter brings you daily news of space and science that has the capacity to provide clues to the mystery of our existence and add a much needed cosmic perspective in our current Anthropocene Epoch. Subscribe free here…

<hr>

[Visit Link](http://www.dailygalaxy.com/my_weblog/2015/07/hubble-largest-moon-in-our-solar-system-harbors-a-buried-ocean-100-kilometers-deep-weekend-feature.html){:target="_blank" rel="noopener"}


