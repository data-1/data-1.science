---
layout: post
title: "3Q: Richard Milner on a new U.S. particle accelerator"
date: 2018-07-28
categories:
author: "Jennifer Chu"
tags: [Proton,Gluon,Electronion collider,Nucleon,Matter,Quark,Quantum chromodynamics,Electron,Nuclear physics,Atomic nucleus,Particle physics,Physics,Neutron,Particle accelerator,Collider,Applied and interdisciplinary physics,Quantum field theory,Nature,Science,Physical sciences,Theoretical physics,Quantum mechanics]
---


What has it taken to make the case for this new particle accelerator? Significant initial impetus for the EIC came from nuclear physicists at the university user-facilities at the University of Indiana and MIT as well as from physicists seeking to understand the origin of the proton’s spin, at laboratories and universities in the U.S. and Europe. After the 2007 exercise, the two U.S. flagship nuclear facilities, namely the Relativistic Heavy Ion Collider at Brookhaven National Laboratory and the Continuous Electron Beam Accelerator Facility at Jefferson Laboratory, took a leadership role in coordinating EIC activities across the broad U.S. QCD community. These issues are fundamental to our understanding of the matter in the universe. A: At present, more than a dozen MIT physics department faculty lead research groups in the Laboratory for Nuclear Science that work directly on understanding the fundamental structure of matter as described by QCD.

<hr>

[Visit Link](http://news.mit.edu/2018/3q-richard-milner-new-us-particle-accelerator-0724){:target="_blank" rel="noopener"}


