---
layout: post
title: "How to Run Commands from Standard Input Using Tee and Xargs in Linux"
date: 2017-10-25
categories:
author: Aaron Kili
tags: [Pipeline (Unix),Xargs,Standard streams,System software,Computer programming,Digital media,Operating system technology,Information technology,Information technology management,Information Age,Unix,Software development,Computer architecture,Computing,Technology,Software,Computer engineering,Computers,Software engineering,Computer science]
---


Read Also: Learn The Basics of How Linux I/O (Input/Output) Redirection Works  Two of the most important command line utilities that can be used with pipelines to build command lines are:  xargs – reads streams of data from standard input, then generates and executes command lines. In this simple article, we will describe how to build and execute multiple commands from standard input using pipes, tee and xargs commands in Linux. You can find other practical xargs command usage examples in these articles:  How to Use Tee with Commands in Linux  This example shows how to send command output to standard output and save to a file; the command below allows you to view top running processes by highest memory and CPU usage in Linux. $ ps -eo cmd,pid,ppid,%mem,%cpu --sort=-%mem | head | tee -a topprocs.txt  You can find more information in tee and xargs man pages. In this article, we described how to generate command lines using pipelines; xargs and tee commands.

<hr>

[Visit Link](https://www.tecmint.com/run-commands-from-standard-input-using-tee-and-xargs-in-linux/){:target="_blank" rel="noopener"}


