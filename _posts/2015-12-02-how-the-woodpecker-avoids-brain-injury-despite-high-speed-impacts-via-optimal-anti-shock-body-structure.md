---
layout: post
title: "How the woodpecker avoids brain injury despite high-speed impacts via optimal anti-shock body structure"
date: 2015-12-02
categories:
author: Science China Press 
tags: [Science,American Association for the Advancement of Science,Woodpecker,China,CT scan,Research,Technology]
---


Their findings, presented in a new study titled Energy conversion in the woodpecker on successive pecking and its role in anti-shock protection of the brain and published in the Beijing-based journal SCIENCE CHINA Technological Sciences, could provide guidance in the design of anti-shock devices and structures for humans. To build a sophisticated 3D model of the woodpecker, scientist Wu Chengwei and colleagues at the State Key Lab of Structural Analysis for Industrial Equipment, part of the Department of Engineering Mechanics at the Dalian University of Technology in northeastern China, scanned the structure of the woodpecker and replicated it in remarkable detail. High-speed impacts and collisions can destroy structures and materials, Wu states. The results showed that the body not only supports the woodpecker to peck on the tree, but also stores the majority of the impact energy in the form of strain energy, significantly reducing the quantity of impact energy that enters the brain. Energy conversion in the woodpecker on successive peckings and its role on anti-shock protection of the brain.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/scp-htw081114.php){:target="_blank" rel="noopener"}


