---
layout: post
title: "Biodiversity Areas under Threat: Overlap of Climate Change and Population Pressures on the World’s Biodiversity Priorities"
date: 2017-03-17
categories:
author: "Juliann E. Aukema, National Center For Ecological Analysis, Synthesis, University Of California, Santa Barbara, Ca, United States Of America, Narcisa G. Pricope, Department Of Earth, Ocean Sciences"
tags: []
---


Forested areas have experienced some drying but more wetting. In the Afrotropic realm only about 6% of the tropical moist forest has experienced wetting and another 6% drying, but this biome has seen more than 90% population increase (Fig 6). In the Mesoamerica and the Caribbean Islands Hotspots, we found small patches of wetting and drying in Southern Mexican Dry Forest G200 and wetting in the Cuban dry forest. Population has increased about 60% in this biome. Primarily rangelands and pastoral anthromes, deserts and xeric shrublands in the Afrotropic have experienced population increase of 127%.The vast majority of this biome is found in the Palearctic, but only 3% has experienced precipitation change; there has been 74% (6 people/km2) population increase.

<hr>

[Visit Link](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0170615){:target="_blank" rel="noopener"}


