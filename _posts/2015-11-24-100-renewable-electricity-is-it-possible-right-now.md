---
layout: post
title: "100% Renewable Electricity: Is It Possible Right Now?"
date: 2015-11-24
categories:
author: Guest Contributor, Raymond Tribdino, David Waterworth, Written By
tags: [Efficient energy use,Compact fluorescent lamp,LED lamp,Wind power,Renewable energy,Energy storage,Lighting,Watt,Net metering,Electric power,Renewable resources,Electricity,Technology,Climate change mitigation,Energy and the environment,Energy technology,Sustainable development,Power (physics),Sustainable energy,Sustainable technologies,Physical quantities,Nature,Energy,Environmental technology,Economy,Electrical engineering]
---


By Roberto Verzola  Most national 100%-renewable electricity scenarios target the 2030-2050 period, which is still a long way off. The same energy plan also refers to a National Renewable Energy Program (NREP), with planned RE capacity additions of 8,240 MW over the same planning period. If the Philippine authorities had taken their 2012 energy projections and plans seriously and resolved to implement them – specifically the energy efficiency project to cut down peak demand by 200 MW per year, and the 8,240-MW renewable energy program – then all new electricity demand could be covered with 100% renewables, starting 2012. Then, the Philippine electricity demand and supply scenario for the 2012-2030 period would have looked like Figure 1, with the peak demand rising more slowly than usual and the renewables-only additions sufficing until 2030 to cover the peak demand plus reserve requirements with 670 MW to spare, based purely on existing government plans in 2012. From the demand side, a more ambitious energy efficiency program can also reduce peak demand by more than 3,600 MW.

<hr>

[Visit Link](http://cleantechnica.com/2015/11/23/100-renewable-electricity-is-it-possible-right-now/){:target="_blank" rel="noopener"}


