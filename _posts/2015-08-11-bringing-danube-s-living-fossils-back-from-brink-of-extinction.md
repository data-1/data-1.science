---
layout: post
title: "Bringing Danube's 'living fossils' back from brink of extinction"
date: 2015-08-11
categories:
author: Diana Simeonova
tags: [Sturgeon,Beluga (sturgeon),Caviar]
---


Members of conservationist group WWF Bulgaria release baby sturgeons in the Danube river, near the village of Vetren, northeastern Bulgaria  Europe's last wild sturgeons got a rare boost this summer when the conservationist group WWF Bulgaria released more than 50,000 babies of these prehistoric fish into the lower Danube, marking the end of a three-year project co-funded by the European Union. Six varieties of sturgeons once lived in the Danube, the expert said. Now only four of these species are left. Six varieties of sturgeons once lived in the Danube river, according to experts, while now only four of these species remain  The Sterlets are the only sturgeon species that do not migrate to the Black sea. As a result, WWF fish experts and local fishermen will try to check on their flock by catching them in fishing nets with smaller openings.

<hr>

[Visit Link](http://phys.org/news/2015-08-danube-fossils-brink-extinction.html){:target="_blank" rel="noopener"}


