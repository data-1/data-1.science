---
layout: post
title: "Quantum measurement is for the birds, but is not essential for plants – Physics World"
date: 2015-07-26
categories:
author: Hamish Johnston
tags: [Flavin adenine dinucleotide,Magnetoreception,Photosynthetic reaction centre,Quantum mechanics,Photosynthesis,Physics,Science,Physical sciences,Chemistry,Applied and interdisciplinary physics]
---


However, some quantum systems can be extremely sensitive to external magnetic fields, and this is why scientists believe that some birds could navigate by making quantum measurements. This, physicists believe, should be long enough for a robin to make a quantum measurement. The direction in the protein along which this separation occurs provides a spatial reference for measuring the Earth’s magnetic field. In particular, the relative orientation of the separation direction and the Earth’s field affects the rate at which the radical pair will decay to a protonated state that provides a signal to the bird’s nervous system. In this case, the quantum meter is a collection of chromophore molecules, which transfer energy from absorbed sunlight to a “reaction centre” where the energy is extracted in the form of mobile electrons.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/mehhzswUxgE/quantum-measurement-is-for-the-birds-but-is-not-essential-for-plants){:target="_blank" rel="noopener"}


