---
layout: post
title: "Integrated simulations answer 20-year-old question in fusion research"
date: 2018-07-01
categories:
author: "Leda Zimmerman"
tags: [Nuclear fusion,Tokamak,Fusion power,Plasma (physics),Simulation,Heat transfer,Heat,Experiment,Research,Physics,Science,Turbulence,Physical sciences]
---


Replicated many times, these cold-pulse experiments with their unlikely results defy what is called the standard local model for the turbulent transport of heat and particles in fusion devices. With Rodriguez Fernandez as first author, White’s group reported this key result in the journal Nuclear Fusion in 2017. Using PRIMA, Rodriguez Fernandez discovered that a competition between types of turbulent modes in the plasma could explain the cold-pulse experiments. This means, says White, that “we are more confident that the local model can be used to predict plasma behavior in future high performance fusion plasma experiments — and eventually, in reactors.”  This work is of great significance for validating fundamental assumptions underpinning the standard model of core tokamak turbulence,” says Jonathan Citrin, Integrated Modelling and Transport Group leader at the Dutch Institute for Fundamental Energy Research (DIFFER), who was not involved in the research. Rodriguez Fernandez has used the integrated simulation tool PRIMA to predict the cold-pulse behavior at DIII-D, and he will perform an experimental test of the predictions later this year to complete his PhD research.

<hr>

[Visit Link](http://news.mit.edu/2018/integrated-simulations-answer-20-year-old-question-fusion-research-0216){:target="_blank" rel="noopener"}


