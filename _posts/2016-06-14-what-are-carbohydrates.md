---
layout: post
title: "What are carbohydrates?"
date: 2016-06-14
categories:
author: "Jessie Szalay, Scott Dutfield"
tags: [Carbohydrate,Dieting,Sugar,Dietary fiber,Low-carbohydrate diet,Whole grain,Glycemic index,Nutrient,Food,Cholesterol,Fat,Cardiovascular disease,Nutrition,Health promotion,Health,Self-care,Determinants of health,Food and drink]
---


There are three macronutrients: carbohydrates, protein and fats , Smathers said. Carbohydrates are found in foods you know are good for you (vegetables) and ones you know are not (doughnuts). Carbs usually considered good are complex carbs, such as whole grains, fruits, vegetables, beans and legumes. A 2014 study published in JAMA (opens in new tab) found that overweight adults eating a balanced diet did not see much additional improvement on a low-calorie, low-glycemic index diet. A study published in the Journal of Nutrition (opens in new tab) in 2009 followed middle-age women for 20 months and found that participants who ate more fiber lost weight, while those who decreased their fiber intake gained weight.

<hr>

[Visit Link](http://www.livescience.com/51976-carbohydrates.html){:target="_blank" rel="noopener"}


