---
layout: post
title: "NASA's Magnetospheric Multiscale Mission locates elusive electron act"
date: 2018-06-21
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Space science,Astronomy,Outer space,Spaceflight,Science,Physics,Nature,Physical sciences]
---


The space high above Earth may seem empty, but it's a carnival packed with magnetic field lines and high-energy particles. New research uses MMS data to improve understanding of how electrons move through this complex region -- information that will help untangle how such particle acrobatics affect Earth. Scientists with MMS have been watching the complex shows electrons put on around Earth and have noticed that electrons at the edge of the magnetosphere often move in rocking motions as they are accelerated. Finding these regions where electrons are accelerated is key to understanding one of the mysteries of the magnetosphere: How does the magnetic energy seething through the area get converted to kinetic energy -- that is, the energy of particle motion. New research, published in the Journal of Geophysical Research, found a novel way to help locate regions where electrons are accelerated.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/nsfc-nm010318.php){:target="_blank" rel="noopener"}


