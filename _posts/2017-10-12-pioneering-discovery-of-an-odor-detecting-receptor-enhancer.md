---
layout: post
title: "Pioneering discovery of an odor-detecting receptor enhancer"
date: 2017-10-12
categories:
author: "Tokyo Institute Of Technology"
tags: [Gene,Enhancer (genetics),Olfactory receptor,Gene expression,Sense of smell,Regulatory sequence,Evolution,Conserved sequence,Branches of genetics,Life sciences,Biotechnology,Biochemistry,Biology,Molecular genetics,Molecular biology,Cell biology,Genetics]
---


On the other hand, class II sensory neurons activate other class II enhancers (white circle, H element is yellow or blue). Credit: Junji Hirota  Each odor-detecting neuron (referred to as olfactory sensory neuron from here on), chooses a single odorant receptor gene from a fairly large number of options that are split into class I (fish-like) and class II (terrestrial-specific) odorant receptors. This strict selectiveness of sensory neurons is in part due to enhancers (DNA sequences that enhance transcription of a gene when bound by specific protein), which remain poorly understood. The findings are especially important as they highlight the discovery of a regulatory sequence, termed the J element, that controls class I gene expression of many more genes than the counterparts that regulate class II gene expression. Their findings point to a conserved sequence among mammalian genomes that was only present in the J element and not in any other class II elements.

<hr>

[Visit Link](https://phys.org/news/2017-10-discovery-odor-detecting-receptor.html){:target="_blank" rel="noopener"}


