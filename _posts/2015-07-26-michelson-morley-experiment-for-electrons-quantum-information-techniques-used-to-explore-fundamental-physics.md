---
layout: post
title: "Michelson-Morley experiment for electrons: Quantum-information techniques used to explore fundamental physics"
date: 2015-07-26
categories:
author: Joint Quantum Institute
tags: [Electron,Quantum mechanics,Luminiferous aether,MichelsonMorley experiment,Physics,Standard Model,Magnetic field,Particle physics,Quantum superposition,Energy level,Wave,Atom,Scientific theories,Physical sciences,Theoretical physics,Science,Applied and interdisciplinary physics]
---


The work uses quantum-correlated electrons within a pair of calcium ions to look for shifts in quantum energy levels with unprecedented sensitivity. The Berkeley experiment may be interpreted as test of Lorentz invariance for either electrons or light. Turning on a magnetic field splits what was a single quantum energy level into six sub-states identified by the strength of the electron's magnetic strength along the direction singled out by the external magnetic field as shown in Figure 1b. Since the energy spacing of the sub-levels is directly proportional to the magnetic field, any noise in the field translates directly into noise, or uncertainty, in the measured energy levels. During this time, because they have a different spatial orientation, the 1/2 part and the 5/2 part of the wave packet will evolve differently if a spatially-anisotropic, Lorentz-violating effect is at work.

<hr>

[Visit Link](http://phys.org/news342867067.html){:target="_blank" rel="noopener"}


