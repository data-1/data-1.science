---
layout: post
title: "Practical challenges in quantum key distribution"
date: 2017-03-23
categories:
author: "Diamanti, Laboratoire Traitement Et Communication De L Information, Cnrs, Télécom Paristech, Université Paris-Saclay, Paris, Lo, Department Of Physics, Department Of Electrical, Computer Engineering"
tags: []
---


104 (see also ref. The CV MDI-QKD system requires high efficiency (>85%) homodyne detectors for a positive key rate.112 This efficiency requirement has been met in recent proof-of-principle laboratory free-space experiments.122,125 However, achieving the required efficiencies in a fibre-based optical network setting is more challenging, owing to the detector coupling loss and losses by fibre network interconnects and components110 (see also ref. On one hand, the imperfections in quantum state preparation need to be carefully quantified and taken into account in the security proof; on the other hand, practical countermeasures are required to prevent Trojan horse attacks119 on the source. To address imperfections in quantum state preparation in QKD, a loss-tolerant protocol was proposed in ref. In ref.

<hr>

[Visit Link](http://www.nature.com/articles/npjqi201625?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


