---
layout: post
title: "How embedded Linux accelerates IoT development"
date: 2017-09-24
categories:
author: "Hunyue Yau"
tags: [Embedded system,Internet of things,Linux,Internet,World Wide Web,Gateway (telecommunications),System on a chip,Wi-Fi,Information Age,Mass media technology,Information and communications technology,Telecommunications,Computer architecture,Computer networking,Computers,Computer science,Computer engineering,Software,Technology,Computing]
---


You'll find that the quickest way to build components of an IoT ecosystem is to use embedded Linux, whether you're augmenting existing devices or designing a new device or system from the beginning. A gateway would speak the low-power protocol to the sensors and would translate them to IP. A main requirement for an IoT device is connectivity, usually in the form of IP. In contrast, an embedded Linux implementation leverages hardware separation and a widely utilized IP stack that probably has been exposed to corner cases. Let's look at an existing embedded Linux device.

<hr>

[Visit Link](https://opensource.com/article/17/3/embedded-linux-iot-ecosystem){:target="_blank" rel="noopener"}


