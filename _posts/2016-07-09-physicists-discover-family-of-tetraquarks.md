---
layout: post
title: "Physicists discover family of tetraquarks"
date: 2016-07-09
categories:
author: "Syracuse University"
tags: [Quark,Particle physics,LHCb experiment,Matter,Hadron,Proton,Pentaquark,Quantum field theory,Science,Standard Model,Theoretical physics,Quantum mechanics,Subatomic particles,Nuclear physics,Nature,Physical sciences,Physics]
---


Professor Tomasz Skwarnicki and Ph.D. student Thomas Britton G'16, both members of the Experimental High-Energy Physics Group at Syracuse and the Large Hadron Collider beauty (LHCb) collaboration at CERN, have confirmed the existence of a tetraquark candidate known as X(4140). A tetraquark is a particle made of four quarks: two quarks and two antiquarks. The particles that Skwarnicki and Britton study have two charm quarks and two strange quarks. This information, along with each particle's quantum numbers, enhances our understanding of the formation of particles and the fundamental structures of matter. It is the energy configuration of the quarks, he explains, that gives each particle its unique mass and identity.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/su-pdf070716.php){:target="_blank" rel="noopener"}


