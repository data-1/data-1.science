---
layout: post
title: "Fossilized dinosaur brain tissue identified for the first time"
date: 2017-09-25
categories:
author: "University Of Cambridge"
tags: [Dinosaur,Meninges,Fossil,Skull,Brain,Cranial cavity]
---


The fossilised brain, found by fossil hunter Jamie Hiscocks near Bexhill in Sussex in 2004, is most likely from a species similar to Iguanodon: a large herbivorous dinosaur that lived during the Early Cretaceous Period, about 133 million years ago. Since the water had little oxygen and was very acidic, the soft tissues of the brain were likely preserved and cast before the rest of its body was buried in the sediment. Environmental scanning electron microscopy images of tubular structures on the exterior of the Bexhill iguanodontian cranial endocast and within the outer laminar layer, interpreted here as meningeal blood vessels. Credit: David Norman  In contrast, the tissue in the fossilised brain appears to have been pressed directly against the skull, raising the possibility that some dinosaurs had large brains which filled much more of the cranial cavity. Of course, it's entirely possible that dinosaurs had bigger brains than we give them credit for, but we can't tell from this specimen alone.

<hr>

[Visit Link](http://phys.org/news/2016-10-fossilized-dinosaur-brain-tissue.html){:target="_blank" rel="noopener"}


