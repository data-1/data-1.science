---
layout: post
title: "Video: Sentinel-3 for oceans"
date: 2016-03-15
categories:
author: European Space Agency
tags: [Privacy,Communication,Technology,Cyberspace,Computing,Information technology]
---


Covering 70% of the planet, the oceans are directly linked to our weather and climate. They are also essential for global transport and provide a wealth of resources. What happens far out to sea has a direct effect on societies all over the world. Sentinel-3 provides crucial observations of our oceans, helping us to understand the overall health of our planet. Credit: ESA  Explore further Acidification affects the ability of bacteria to clean our oceans

<hr>

[Visit Link](http://phys.org/news/2016-02-video-sentinel-oceans.html){:target="_blank" rel="noopener"}


