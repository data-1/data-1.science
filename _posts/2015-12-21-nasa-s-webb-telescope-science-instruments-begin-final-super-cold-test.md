---
layout: post
title: "NASA's Webb Telescope science instruments begin final super cold test"
date: 2015-12-21
categories:
author: NASA/Goddard Space Flight Center
tags: [James Webb Space Telescope,Goddard Space Flight Center,Vacuum,NASA,Cryogenics,Spaceflight,Science,Physical sciences,Outer space,Astronomy,Space science]
---


This move marks the start of the third and final cryogenic test at Goddard to prepare the Integrated Science Instrument Module (ISIM), or the heart of the telescope, for space. To create temperatures that cold on Earth, the team uses the massive thermal vacuum chamber at Goddard called the Space Environment Simulator, or SES, that duplicates the vacuum and extreme temperatures of space. This 40-foot-tall, 27-foot-diameter cylindrical chamber eliminates almost all of the air with vacuum pumps and uses liquid nitrogen and even colder liquid helium to drop the temperature, thereby simulating the space environment. The several-month-long test is used to ensure all of the science instruments are ready for flight. We are at the beginning of the final cryogenic test, most of the mechanical work is done.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/nsfc-nwt102815.php){:target="_blank" rel="noopener"}


