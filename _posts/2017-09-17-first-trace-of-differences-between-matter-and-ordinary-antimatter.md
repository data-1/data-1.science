---
layout: post
title: "First trace of differences between matter and 'ordinary' antimatter"
date: 2017-09-17
categories:
author: "The Henryk Niewodniczanski Institute of Nuclear Physics Polish Academy of Sciences"
tags: [Matter,Baryon,LHCb experiment,Quark,Antimatter,Annihilation,Meson,Universe,Lambda baryon,Hadron,Antiparticle,Particle physics,CP violation,Subatomic particles,Standard Model,Quantum chromodynamics,Theoretical physics,Physical sciences,Nature,Nuclear physics,Physics,Quantum field theory]
---


The most recent analysis of data from the LHCb collaboration, published in the journal Nature Physics and concerning the decays of Lambda b particles composed of down, up and beauty quarks, is thus the first indication of the possible differences between baryonic matter and its antimatter reflection. Meanwhile, astronomers only observe annihilation radiation here and there and in residual amounts, well explained by physical phenomena which are also today responsible for the formation of small amounts of antimatter. 6,000 cases in which Lambda b particles decayed to a proton and three pi mesons (pions), and approx. +48 12 6628050  email: marcin.kucharczyk@ifj.edu.pl  SCIENTIFIC PAPERS:  Measurement of matter-antimatter differences in beauty baryon decays  The LHCb collaboration  Nature Physics (2017)  DOI: 10.1038/nphys4021  LINKS:  http://lhcb-public.web.cern.ch/lhcb-public/  The website of the LHCb Collaboration. http://www.ifj.edu.pl/  The website of the Institute of Nuclear Physics of the Polish Academy of Sciences.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/thni-fto022217.php){:target="_blank" rel="noopener"}


