---
layout: post
title: "The evolution of package managers"
date: 2018-07-27
categories:
author: "Steve Ovens
(Alumni, Red Hat)"
tags: [Arch Linux,APT (software),Yum (software),Package manager,RPM Package Manager,Free software,Free content,Open-source movement,Information technology management,Computer science,System software,Free system software,Software distribution,Linux,Unix software,Unix,Technology,Operating system technology,Software engineering,Software development,Free software projects,Utility software,Computing,Software]
---


How was software on Linux installed before package managers? What is a package? What is a software repository? Software repositories are a centralized listing of all of the available software for any repository the system has been configured to use. Arch also uses repositories similar to other package managers.

<hr>

[Visit Link](https://opensource.com/article/18/7/evolution-package-managers){:target="_blank" rel="noopener"}


