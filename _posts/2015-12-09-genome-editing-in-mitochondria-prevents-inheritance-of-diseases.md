---
layout: post
title: "Genome editing in mitochondria prevents inheritance of diseases"
date: 2015-12-09
categories:
author: Cell Press
tags: [Mitochondrial disease,Mitochondrion,Mitochondrial DNA,Mitochondrial replacement therapy,Mutation,Medical specialties,Diseases and disorders,Health sciences,Biochemistry,Clinical medicine,Biotechnology,Biology,Medicine,Genetics,Life sciences]
---


To test this approach, the researchers used a mouse model that carries two different types of mitochondrial DNA and designed TALENs and restriction endonucleases to target and destroy only one type of mitochondrial DNA in the eggs of these mice. The injected mouse embryos, which showed normal patterns of development, were then transferred to female mice, which gave birth to healthy pups that had low levels of the targeted mitochondrial DNA in various organs. Moreover, the offspring themselves gave birth to pups that showed barely detectable levels of the targeted mitochondrial DNA, demonstrating the potential of this approach for preventing the transgenerational transmission of mitochondrial diseases. But before any clinical trials begin, it will be necessary to evaluate the safety and efficacy of the method in eggs from patients with mitochondrial diseases. Toward this goal, Belmonte's team is collaborating with several IVF clinics to test this technology in surplus human eggs that are donated by patients with mitochondrial diseases for research purposes.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/cp-gei041615.php){:target="_blank" rel="noopener"}


