---
layout: post
title: "How the mind sharpens the senses"
date: 2016-06-14
categories:
author: "Ruhr-Universitaet-Bochum"
tags: [Somatosensory system,Meditation,Neuroplasticity,Sense,Perception,Two-point discrimination,Psychology,Psychological concepts,Mental processes,Cognitive psychology,Interdisciplinary subfields,Concepts in metaphysics,Nervous system,Cognitive science,Behavioural sciences,Brain,Cognition,Neuroscience]
---


A study conducted with experienced scholars of Zen-Meditation shows that mental focussing can induce learning mechanisms, similar to physical training. Additionally, some participants applied a special finger-meditation for two hours per day, during which they were asked to specifically focus on their right index finger and become aware of spontaneously arising sensory percepts in this finger. Subsequent assessment of the group that practiced finger-meditation showed a significant improvement in the tactile acuity of the right index and middle finger. Data show significant improvement of the sense of touch  In order to assess the sense of touch quantitatively, researchers measured the so-called “two-point discrimination threshold”. Meditation induces plasticity and learning processes as active training or physical stimulation  It is known for long that extensive training induces neuroplasticity, which denotes the ability of the brain to adapt and restructure itself, thereby improving perception and behavior.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150827083659.htm){:target="_blank" rel="noopener"}


