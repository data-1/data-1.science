---
layout: post
title: "AWS, Microsoft Offer New Open Source AI Framework"
date: 2017-10-13
categories:
author: ""
tags: []
---


Amazon Web Services and Microsoft on Thursday announced the availability of Gluon, an open source deep learning library for building artificial intelligence neural networks. The Gluon interface allows developers of all skill levels to prototype, build, train and deploy sophisticated machine learning models for the cloud, devices at the edge, and mobile apps, according to both companies. Gluon competes with Keras, an AI framework tool that integrates with Google’s TensorFlow. AWS and Microsoft are aware that today’s developer communities are forming around open source software. A D V E R T I S E M E N T  The open source donation proves “Microsoft has awakened to the reality that it is the open source developers that have the strongest communities, and those communities are also influencing business decisions,” he told LinuxInsider.

<hr>

[Visit Link](http://www.linuxinsider.com/story/84881.html?rss=1){:target="_blank" rel="noopener"}


