---
layout: post
title: "UCI physicists confirm possible discovery of fifth force of nature"
date: 2016-08-30
categories:
author: "University of California - Irvine"
tags: [Fifth force,Fundamental interaction,Subatomic particle,Matter,Physics,Weak interaction,Photon,Dark matter,Nuclear physics,Theoretical physics,Nature,Science,Particle physics,Physical sciences]
---


If confirmed by further experiments, this discovery of a possible fifth force would completely change our understanding of the universe, with consequences for the unification of forces and dark matter. The UCI group studied the Hungarian researchers' data as well as all other previous experiments in this area and showed that the evidence strongly disfavors both matter particles and dark photons. The UCI work demonstrates that instead of being a dark photon, the particle may be a protophobic X boson. That said, because the new particle is so light, there are many experimental groups working in small labs around the world that can follow up the initial claims, now that they know where to look. This dark sector force may manifest itself as this protophobic force we're seeing as a result of the Hungarian experiment.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/uoc--upc081516.php){:target="_blank" rel="noopener"}


