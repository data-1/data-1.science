---
layout: post
title: "Nanomotor guided inside a living cell using a magnetic field"
date: 2018-07-01
categories:
author: "Bob Yirka"
tags: [Nanomotor,Nanorobotics,Technology]
---


DOI: 10.1002/adma.201800429  A team of researchers at the Indian Institute of Science in Bangalore has developed a type of nanomotor that can be guided inside of a living cell using an external magnetic field. Such objects could conceivably carry drugs directly to individual parts of a cell. They report that they have now developed a nanomotor that can be guided to desired locations inside of a cell without causing disruptions. The researchers made several of the nanomotors in different sizes and tested them in different types of cells, some of which were from cancerous tumors. More information: Malay Pal et al. Maneuverability of Magnetic Nanomotors Inside Living Cells, Advanced Materials (2018).

<hr>

[Visit Link](https://phys.org/news/2018-04-nanomotor-cell-magnetic-field.html){:target="_blank" rel="noopener"}


