---
layout: post
title: "Herd mentality: Are we programmed to make bad decisions?"
date: 2016-04-12
categories:
author: University Of Exeter
tags: [Branches of science,Communication,Science,Technology,Cognitive science]
---


We showed that evolution will lead individuals to over use social information, and copy others too much than they should. The result is that groups evolve to be unresponsive to changes in their environment and spend too much time copying one another, and not making their own decisions. The team used mathematical models to look at how the use of social information has evolved within animal groups. The study, Social information use and the evolution of unresponsiveness in collective systems, is published in the journal Interface on December 17 2014. New study into animal social behavior

<hr>

[Visit Link](http://phys.org/news337975902.html){:target="_blank" rel="noopener"}


