---
layout: post
title: "Researchers discover that DNA naturally fluoresces"
date: 2016-08-22
categories:
author: "Northwestern University"
tags: [Fluorescence,Fluorophore,Staining,Macromolecule,Cell (biology),Fluorescent tag,Biology,Life sciences,Biotechnology,Chemistry,Biochemistry,Physical sciences,Scientific techniques,Technology,Optics,Molecular physics]
---


For decades, textbooks have stated that macromolecules within living cells, such as DNA, RNA, and proteins, do not fluoresce on their own. There are textbooks that say biological molecules don't absorb light and don't fluoresce, said Zhang, associate professor of biological engineering. Backman, Zhang, and Sun discovered that when illuminated with visible light, the molecules get excited and light up well enough to be imaged without fluorescent stains. This toxicity makes it tricky to get an accurate image of the active processes in living cells because they die immediately after the application of fluorescent stains. There are special dyes used to image living cells, but those just cause the cells to die slower.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/nu-rd081516.php){:target="_blank" rel="noopener"}


