---
layout: post
title: "Discovery of pure organic substances exhibiting the quantum spin liquid state"
date: 2014-06-24
categories:
author: Mikiko Tanifuji, National Institute For Materials Science
tags: [Superconductivity,Matter,Physical chemistry,Chemical product engineering,Nature,Condensed matter,Materials,Phases of matter,Applied and interdisciplinary physics,Physics,Physical sciences,Chemistry,Materials science,Condensed matter physics]
---


(a) An arrangement of dimerized molecules of κ-H3(Cat- EDT-TTF)2 on the 2-D plane, b-c. (b) An anisotropic 2-D triangular lattice made of spin-1/2 molecular dimers. On a triangular lattice, when two spins (red and blue arrows) align antiparallel, the third spin cannot decide a direction either up or down (and its energy became unstable). Researchers at the University of Tokyo and Japan's National Institute for Materials Science have discovered pure organic substances exhibiting the quantum spin liquid state. To clarify these matters, efforts have been made over many years in the quest for quantum spin liquid substances. In this process, in partnership with another research group led by Unit Director Shinya Uji at the Superconducting Properties Unit of the National Institute for Materials Science, they discovered that electron spins in a pure organic substance, κ-H3(Cat- EDT-TTF), were in the quantum spin liquid state.

<hr>

[Visit Link](http://phys.org/news322725747.html){:target="_blank" rel="noopener"}


