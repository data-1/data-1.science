---
layout: post
title: "How machine learning can help with voice disorders"
date: 2016-08-30
categories:
author: "Massachusetts Institute of Technology, CSAIL"
tags: [Human voice,Cognition,Cognitive science,Branches of science]
---


Using accelerometer data collected from a wearable device developed by researchers at the MGH Voice Center, researchers demonstrated that they can detect differences between subjects with MTD and matched controls. People with vocal disorders aren't always misusing their voices, and people without disorders also occasionally misuse their voices, says Ghassemi. The study was broken into two groups: patients that had been diagnosed with voice disorders, and a control group of individuals without disorders. The team also found that after voice therapy the distribution of patients' glottal pulses were more similar to those of the controls. That sort of technology can help with the most challenging aspect of voice therapy: getting patients to actually employ the healthier vocal behaviors that they learned in therapy in their everyday lives.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/miot-hml082916.php){:target="_blank" rel="noopener"}


