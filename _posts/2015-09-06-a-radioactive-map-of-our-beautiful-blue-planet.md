---
layout: post
title: "A radioactive map of our beautiful blue planet"
date: 2015-09-06
categories:
author: John Kennedy, John Kennedy Is A Journalist Who Served As Editor Of Silicon Republic For Years
tags: [Neutrino,Radioactive decay,Space science,Ionizing radiation,Forms of energy,Radiation,Chemistry,Physical sciences,Nuclear physics,Physics,Particle physics,Nuclear chemistry,Astronomy,Nature,Radioactivity]
---


Scientists have mapped the radiation that exists on planet Earth using antineutrino detectors in a project entitled AGM2015. The result? This is a pretty radioactive place. The neutrino was theorised by Wolfgang Pauli in 1930 to explain the continuous energy spectrum of nuclear beta rays. Further research by Enrico Fermi around the time of the Manhattan Project in the 1940s produced a self-sustaining nuclear chain reaction caused by antineutrinos, antimatter cousins to neutrinos and the smallest particles known to science.

<hr>

[Visit Link](https://www.siliconrepublic.com/earth-science/2015/09/05/a-radioactive-map-of-our-beautiful-blue-planet){:target="_blank" rel="noopener"}


