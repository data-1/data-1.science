---
layout: post
title: "Marine bacteria are natural source of chemical fire retardants"
date: 2014-06-30
categories:
author: University Of California - San Diego
tags: [Polybrominated diphenyl ethers,Flame retardant,Nature,Chemistry]
---


Some marine bacteria produce potent persistent organic compounds that are nearly identical to flame retardant chemicals. Credit: UC San Diego School of Medicine  Researchers at the University of California, San Diego School of Medicine have discovered a widely distributed group of marine bacteria that produce compounds nearly identical to toxic man-made fire retardants. The study is published in the June 29 online issue of Nature Chemical Biology. In the study, the researchers identified a group of ten genes involved in the synthesis of more than 15 bromine-containing polyaromatic compounds, including some PBDEs. The next step is to look more broadly in the marine environment for the distribution of this gene signature and to document how these compounds are entering the food chain, said Vinayak Agarwal, PhD, a postdoctoral researcher with the Scripps Center for Oceans and Human Health at UC San Diego.

<hr>

[Visit Link](http://phys.org/news323244573.html){:target="_blank" rel="noopener"}


