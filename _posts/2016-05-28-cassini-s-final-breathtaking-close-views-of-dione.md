---
layout: post
title: "Cassini's final breathtaking close views of Dione"
date: 2016-05-28
categories:
author: Preston Dyches, Jet Propulsion Laboratory
tags: [CassiniHuygens,Dione (moon),Saturn,Enceladus,Astronomical objects known since antiquity,Planets,Spaceflight,Gas giants,Outer planets,Moons,Astronomical objects,Bodies of the Solar System,Planets of the Solar System,Solar System,Space science,Planetary science,Astronomy,Outer space]
---


This view from NASA's Cassini spacecraft looks toward Saturn's icy moon Dione, with giant Saturn and its rings in the background, just prior to the mission's final close approach to the moon on August 17, 2015. Credit: NASA/JPL-Caltech/Space Science Institute  A pockmarked, icy landscape looms beneath NASA's Cassini spacecraft in new images of Saturn's moon Dione taken during the mission's last close approach to the small, icy world. Two of the new images show the surface of Dione at the best resolution ever. The December Enceladus encounter will be Cassini's final close pass by that moon, at an altitude of 3,106 miles (4,999 kilometers). NASA's Cassini spacecraft captured this parting view showing the rough and icy crescent of Saturn's moon Dione following the spacecraft's last close flyby of the moon on Aug. 17, 2015. Credit: NASA/JPL-Caltech/Space Science Institute  NASA's Cassini spacecraft gazes out upon a rolling, cratered landscape in this oblique view of Saturn's moon Dione. Credit: NASA/JPL-Caltech/Space Science Institute  This view of Dione from Cassini includes the mission's highest-resolution view of the icy moon's surface as an inset at center left.

<hr>

[Visit Link](http://phys.org/news/2015-08-cassini-breathtaking-views-dione.html){:target="_blank" rel="noopener"}


