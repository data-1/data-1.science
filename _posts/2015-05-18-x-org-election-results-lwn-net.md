---
layout: post
title: "X.org election results [LWN.net]"
date: 2015-05-18
categories:
author: Posted April, Corbet
tags: [Computing,Finnish computer programmers,Open content,Linus Torvalds,Computer science,System software,Operating system families,Software engineering,Technology,Unix variants,Unix,Free content,Software development,Linux,Free system software,Software,Open-source movement,Free software,Linux kernel programmers,Digital media,Finnish computer scientists,Torvalds family,Free software projects,Linux people,Intellectual works,Linux websites]
---


[Announcements] Posted Apr 10, 2015 11:38 UTC (Fri) by corbet  As was discussed in this LWN article, the X.Org Foundation recently held an election to choose four board members and decide whether to change the organization's by-laws to enable it to become a member of Software in the Public Interest (SPI). The results are now available. The board members elected are Peter Hutterer, Martin Peres, Rob Clark, and Daniel Vetter. The measure to change the by-laws did not pass, though, despite receiving only two no votes, because the required two-thirds majority was not reached. Comments (1 posted)

<hr>

[Visit Link](http://lwn.net/Articles/639917/rss){:target="_blank" rel="noopener"}


