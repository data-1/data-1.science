---
layout: post
title: "Researchers find patterns in evolving genomes of thousands of species"
date: 2016-04-19
categories:
author: Fred Love, Iowa State University
tags: [DNA,Genetics,Genome,Biochemistry,Life sciences,Molecular biology,Biology,Biotechnology]
---


A pair of genetics researchers at Iowa State University found striking patterns in the building blocks of DNA in a wide variety of species, according to their recently published paper. Jianming Yu, an associate professor of agronomy and the Pioneer Distinguished Chair in Maize Breeding, and Xianran Li, a research assistant professor of agronomy, waded deep into the reams of data compiled on the genomes of 2,210 various species, including vertebrates, plants, fungi and bacteria. Yu and Li ran similar analyses on multiple genomes within eight individual species for which such data exists, including humans. The researchers found what Yu called a shocking consistency regarding the approximate equality of the proportions of adenine-thymine and guanine-cytosine. The research team found that individuals in a species with bottlenecks – events during which a small part of a population becomes separated and evolves independently – tend to have a higher proportion of adenine-thymine content in their genomes than individuals that descended from the population that didn't go through the bottleneck.

<hr>

[Visit Link](http://phys.org/news346660130.html){:target="_blank" rel="noopener"}


