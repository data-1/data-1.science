---
layout: post
title: "Textricator: Data extraction made simple"
date: 2018-07-27
categories:
author: "Steve Spiker"
tags: [PDF,Software development,Information Age,Computer science,Technology,Intellectual works,Computing,Information technology,Digital media,Information technology management,Software engineering,Software]
---


Data, interrupted. We do this by producing a series of up to 32 performance measures covering the entire criminal justice system, county by county. Textricator can process just about any text-based PDF format—not just tables, but complex reports with wrapping text and detail sections generated from tools like Crystal Reports. We evaluated other great open source solutions like Tabula, but they just couldn’t handle the structure of some of the PDFs we needed to scrape. At MFJ, we’re committed to transparency and knowledge-sharing, which includes making our software available to anyone, especially those trying to free and share data publicly.

<hr>

[Visit Link](https://opensource.com/article/18/7/textricator){:target="_blank" rel="noopener"}


