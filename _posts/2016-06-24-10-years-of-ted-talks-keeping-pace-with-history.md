---
layout: post
title: "10 years of TED Talks keeping pace with history"
date: 2016-06-24
categories:
author: "Cynthia Betubiza"
tags: [Arab Spring,Syria,Edward Snowden,Physics,Gravitational wave,Islamic State,National Security Agency]
---


The following wave of demonstrations and protests resulted in President Mubarak’s resignation and much later, a draft for a new constitution. The whistleblower — and the whistleblowee: Edward Snowden was a government contractor working in Hawaii for the NSA. The refugee crisis begins: In the midst of the Arab Spring, a group of Syrian pro-democracy demonstrators protested in March 2011. This marked a monumental achievement in physics. In 2014, Ghani became president of Afghanistan.

<hr>

[Visit Link](http://blog.ted.com/10-years-of-ted-talks-keeping-pace-with-history/){:target="_blank" rel="noopener"}


