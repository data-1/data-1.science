---
layout: post
title: "Butterfly pattern emerges from quantum simulation"
date: 2018-04-26
categories:
author: "Centre for Quantum Technologies at the National University of Singapore"
tags: [Photon,Simulation,Electron,Hofstadters butterfly,Quantum computing,Quantum simulator,Science,Applied and interdisciplinary physics,Quantum mechanics,Theoretical physics,Physics,Physical sciences,Scientific theories]
---


The international team used photons in Google's quantum chip to simulate the surprising and beautiful pattern of the 'Hofstadter butterfly', a fractal structure characterizing the behaviour of electrons in strong magnetic fields. It shows how a quantum simulator can reproduce all kinds of exotic complex quantum behaviour. We do the same with the quantum chip, hitting it with photons and then following its evolution in time, explains Angelakis. The team saw the butterfly by hitting the qubits with one photon at a time. This is a quantum phase transition, akin to the phase change that happens when water freezes into ice, that determines whether materials are conductors or insulators The team found precursors of many-body localization by applying their 'hit and listen' technique to different regimes of disorder and interaction.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/11/171130141112.htm){:target="_blank" rel="noopener"}


