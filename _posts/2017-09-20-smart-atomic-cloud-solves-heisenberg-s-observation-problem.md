---
layout: post
title: "Smart atomic cloud solves Heisenberg's observation problem"
date: 2017-09-20
categories:
author: "University of Copenhagen - Faculty of Science"
tags: [Quantum mechanics,Copenhagen (play),Uncertainty principle,Niels Bohr,Niels Bohr Institute,Physics,Science,Applied and interdisciplinary physics,Theoretical physics,Physical sciences]
---


And this problem - which has to do with the fact that in-accuracies inevitably taint certain measurements conducted at quantum level - is described in Heisenberg's Uncertainty Principle. Professor Eugene Polzik, head of Quantum Optics (QUANTOP) at the Niels Bohr Institute, has been in charge of the research - which has included the construction of a vibrating membrane and an advanced atomic cloud locked up in a minute glass cage. To conduct the experiments at NBI professor Polzik and his team of young, enthusiastic and very skilled NBI-researchers used a 'tailor-made' membrane as the object observed at quantum level. the light reaches the membrane, explains Eugene Polzik: This results in the laser light-photons 'kicking' the object - i.e. the membrane - as well as the atomic cloud, and these 'kicks' so to speak cancel out. For instance when developing new and much more advanced types of sensors for various analyses of movements than the types we know today from cell phones, GPS and geological surveys, says professor Eugene Polzik: Generally speaking sensors operating at quantum level are receiving a lot of attention these days.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-07/fos--s071217.php){:target="_blank" rel="noopener"}


