---
layout: post
title: "DNA that only females have"
date: 2016-05-07
categories:
author: Uppsala Universitet
tags: [Sex,Chromosome,Male,Female,Y chromosome,ZW sex-determination system,Species,Gene,Genetics,News aggregator,Evolution,Biological classification,Biology,Biological processes,Life sciences,Branches of genetics]
---


In birds, the females have their own sex chromosome, the W chromosome. Two copies of it produces a male, one copy (plus a W chromosome) produces a female, says Hans Ellegren. In order for certain genes to work, it is critical that an individual has two copies of that gene. This is because it is only inherited on the maternal side and fewer mutations arise in females than in males, says Hans Ellegren. Males produce a vastly greater number of germ cells than females and so the probability that a sperm contains a new mutation is much greater than for an egg cell.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150604084511.htm){:target="_blank" rel="noopener"}


