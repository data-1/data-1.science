---
layout: post
title: "NASA pushes first flight of Orion spacecraft with crew to 2023"
date: 2016-07-02
categories:
author: "Bymarcia Dunn"
tags: [Orion (spacecraft),NASA,Human spaceflight,Space Launch System,Spacecraft,Aerospace,Space exploration,Space science,Spaceflight technology,Spaceflight,Outer space,Astronautics,Flight,Space programs,Space program of the United States,Space vehicles,Life in space]
---


Managers set 2023 as the new official launch date for the capsule, although they said they haven't entirely given up yet on 2021. The 11-foot capsule will blast off atop a megarocket still under development by NASA, called SLS for Space Launch System. That first flight with astronauts will be to check out Orion's crew systems close to Earth, especially the life-support equipment. On the second crewed mission, NASA will push deeper into space, perhaps the far side of the moon, said William Gerstenmaier, associate administrator for human exploration and operations. Orion is NASA's first new spacecraft for humans in more than a generation, succeeding the now-retired space shuttles.

<hr>

[Visit Link](http://phys.org/news/2015-09-nasa-flight-orion-spacecraft-crew.html){:target="_blank" rel="noopener"}


