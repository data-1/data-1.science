---
layout: post
title: "The million year old monkey: New evidence confirms the antiquity of fossil primate"
date: 2015-09-05
categories:
author: University of Melbourne 
tags: [Hispaniola monkey,News aggregator]
---


An international team of scientists have dated a species of fossil monkey found across the Caribbean to just over 1 million years old. The discovery was made after the researchers recovered a fossil tibia (shin bone) belonging to the species of extinct monkey Antillothrix bernensis from an underwater cave in Altagracia Province, Dominican Republic. In a paper published this week in the well international journal, the Journal of Human Evolution, the team use three-dimensional geometric morphometrics to confirm that the fossil tibia does indeed belong to Antillothrix bernensis, a primate that we now know existed on Hispaniola relatively unchanged for over a million years. This monkey, roughly the size of a small cat, was tree-dwelling and lived largely on a diet of fruit and leaves. Dr Helen Green of Melbourne University's School of Earth Sciences, a lead researcher involved in the dating of the limestone surrounding the fossils, said the question of the age of primate fossils from this region has puzzled scientists since the days of Darwin and Wallace.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150904121350.htm){:target="_blank" rel="noopener"}


