---
layout: post
title: "Events Archive – Physics World"
date: 2017-09-19
categories:
author: ""
tags: [Computing,Science,Branches of science,Technology,Communication]
---


Explore the ways in which today’s world relies on AI, and ponder how this technology might shape the world of tomorrow

<hr>

[Visit Link](http://physicsworld.com/cws/event/2017/may/29/quantum-science-approaches-to-strongly-correlated-systems-from-ultracold-atoms-to-high-energy-and-condensed-matter-physics){:target="_blank" rel="noopener"}


