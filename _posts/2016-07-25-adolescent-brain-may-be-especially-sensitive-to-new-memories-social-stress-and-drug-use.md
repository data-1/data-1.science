---
layout: post
title: "Adolescent brain may be especially sensitive to new memories, social stress, and drug use"
date: 2016-07-25
categories:
author: "Cell Press"
tags: [Adolescence,Memory,Critical period,Working memory,Mental disorder,News aggregator,Neuroplasticity,Psychology,Behavioural sciences,Cognitive science,Branches of science,Neuroscience,Cognition,Mental processes,Psychological concepts,Cognitive psychology,Interdisciplinary subfields]
---


Adolescence, like infancy, has been said to include distinct sensitive periods during which brain plasticity is heightened; but in a review of the neuroscience literature published on September 23 in Trends in Cognitive Sciences, University College London (UCL) researchers saw little evidence for this claim. However, a small number of studies do support that memory formation, social stress, and drug use are processed differently in the adolescent brain compared to other periods of life. Adolescents are much more likely than children to choose their own environments and choose what they want to experience. The ability to form memories seems to be augmented during adolescence, one example for how it may be a sensitive period. Working memory can be trained in adolescents, but we don't know how these training effects differ from other age groups, Fuhrmann says.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150923133517.htm){:target="_blank" rel="noopener"}


