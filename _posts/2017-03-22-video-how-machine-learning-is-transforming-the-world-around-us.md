---
layout: post
title: "Video: How machine learning is transforming the world around us"
date: 2017-03-22
categories:
author: "Lanisha Butterfield, Oxford Science Blog, University Of Oxford"
tags: [Machine learning,Privacy,Computer,Computing,Branches of science,Cognitive science,Cyberspace,Communication,Information technology,Technology,Information Age]
---


Siri, will it rain today?, Facebook, tag my friend in this photo. These are just two examples of the incredible things that we ask computers to do for us. But, have you ever asked yourself how computers know how to do these things? Machine learning is not a new concept but it is constantly evolving and the potential benefits of its capability are increasing by the second. A form of artificial intelligence, it provides computers with the ability to learn through experience, without being explicitly programmed to perform a task.

<hr>

[Visit Link](https://phys.org/news/2017-01-video-machine-world.html){:target="_blank" rel="noopener"}


