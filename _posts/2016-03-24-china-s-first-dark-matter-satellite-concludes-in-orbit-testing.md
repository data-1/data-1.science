---
layout: post
title: "China’s first dark matter satellite concludes in-orbit testing"
date: 2016-03-24
categories:
author:  
tags: []
---


China’s first dark matter detection satellite has completed three months of in-orbit testing and found 460 million high energy particles in a 92-day flight, officials said on Friday. The satellite’s initial findings are expected to appear before the end of the year as it has completed three months of in-orbit testing, the Chinese Academy of Sciences (CAS) said. Dark matter is an invisible material that makes up most of the universe’s mass. Launched on December 17 last year on a Long March 2D rocket, “Wukong” was handed over to the CAS Purple Mountain Observatory on Thursday. The four major parts of the payload, a plastic scintillator array detector, a silicon array detector, a BGO calorimeter and a neutron detector, functioned satisfactorily.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/chinas-dampe-satellite-wukong-concludes-satellite-inorbit-testing/article8369876.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


