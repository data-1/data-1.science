---
layout: post
title: "Study: 93 percent of advanced leukemia patients in remission after immunotherapy"
date: 2016-04-29
categories:
author: Fred Hutchinson Cancer Research Center
tags: [Chimeric antigen receptor T cell,Cancer,Leukemia,Immune system,Acute lymphoblastic leukemia,T cell,Cancer immunotherapy,Health sciences,Causes of death,Medicine,Neoplasms,Immunology,Epidemiology,Health,Diseases and disorders,Clinical medicine,Medical specialties]
---


However, the immune system is not always able to eliminate cancer cells when they form. This study is the first CAR T-cell trial to infuse patients with an even mixture of two types of T cells (helper and killer cells, which work together to kill cancer). Not all patients stayed in complete remission: some relapsed and were treated again with CAR T cells, and two relapsed with leukemias that were immune to the CAR T cells. Study limitations: As an early-phase trial, this study was not designed to provide definitive evidence of the therapy's effectiveness against cancer. Fred Hutch's pioneering work in bone marrow transplantation led to the development of immunotherapy, which harnesses the power of the immune system to treat cancer with minimal side effects.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/fhcr-s9p042716.php){:target="_blank" rel="noopener"}


