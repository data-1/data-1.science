---
layout: post
title: "Fruit and vegetables aren't only good for a healthy body; they protect your mind too"
date: 2016-07-04
categories:
author: "BioMed Central"
tags: [Healthy diet,Diet (nutrition),Mediterranean diet,Health,Fat,Nut (fruit),Health sciences,Nutrition,Self-care,Determinants of health,Food and drink,Health promotion,Public health,Health care]
---


Participants used a scoring system to measure their adherence to the selected diet, i.e. the higher the dietary score indicated that the participant was eating a healthier diet. Lead researcher, Almudena Sanchez-Villegas, University of Las Palmas de Gran Canaria, says We wanted to understand what role nutrition plays in mental health, as we believe certain dietary patterns could protect our minds. The protective role is ascribed to their nutritional properties, where nuts, legumes, fruits and vegetables (sources of omega-3 fatty acids, vitamins and minerals) could reduce the risk of depression. The study included 15,093 participants free of depression at the beginning of the study. Thus, common nutrients and food items such as omega-3 fatty acids, vegetables, fruits, legumes, nuts and moderate alcohol intake present in both patterns (Alternative Healthy Eating Index-2010 and Mediterranean diet) could be responsible for the observed reduced risk in depression associated with a good adherence to the Alternative Healthy Eating Index-2010.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150916215535.htm){:target="_blank" rel="noopener"}


