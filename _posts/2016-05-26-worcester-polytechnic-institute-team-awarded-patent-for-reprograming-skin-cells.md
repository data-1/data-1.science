---
layout: post
title: "Worcester Polytechnic Institute team awarded patent for reprograming skin cells"
date: 2016-05-26
categories:
author: Worcester Polytechnic Institute
tags: [Stem cell,Embryonic stem cell,Cell potency,Regeneration (biology),Induced pluripotent stem cell,Worcester Polytechnic Institute,Cellular differentiation,Cell (biology),Life sciences,Cell biology,Biology,Biotechnology]
---


Based on research conducted at WPI, the method of converting human skin cells into engines of wound healing and tissue regeneration may accelerate cell therapies for a range of serious conditions, including heart attacks, diabetes, and traumatic injuries  Worcester, Mass. - Cell therapies for a range of serious conditions, including heart attacks, diabetes, and traumatic injuries, will be accelerated by research at Worcester Polytechnic Institute (WPI) that yielded a newly patented method of converting human skin cells into engines of wound healing and tissue regeneration. It further causes those cells to express genes and proteins typically associated with stem cells, thereby demonstrating that the cells are in a less differentiated state. Called induced pluripotent stem cells (iPS), they were first created in 2007 by Shinya Yamanaka's team at Kyoto University in Japan, which inserted extra copies of four known stem cell genes into human skin cells. With the method developed by Page and Dominko, however, cells from similar tissue can maintain regeneration competence for 70 generations or more.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/wpi-wpi052416.php){:target="_blank" rel="noopener"}


