---
layout: post
title: "Computing with time travel"
date: 2015-12-22
categories:
author: National University Of Singapore
tags: [Time travel,Quantum mechanics,Closed timelike curve,Quantum entanglement,Academic discipline interactions,Applied and interdisciplinary physics,Scientific theories,Branches of science,Physics,Science,Theoretical physics,Concepts in metaphysics]
---


If the universe allows 'open timelike curves', particles travelling back in time along them could help to perform currently intractable computations. Even though such curves don't allow for interaction with anything in the past, researchers writing in npj Quantum Information show there is a gain computational power as long as the time-travelling particle is entangled with one kept in the present. Entanglement, a strange effect only possible in the realm of quantum physics, creates correlations between the time-travelling message and the laboratory system. The problem was, Bacon's quantum computer was travelling around 'closed timelike curves'. However, the new work shows that a quantum computer can solve insoluble problems even if it is travelling along open timelike curves, which don't create causality problems.

<hr>

[Visit Link](http://phys.org/news/2015-12-computing-with-time-travel.html){:target="_blank" rel="noopener"}


