---
layout: post
title: "Monitoring Linux File access, Changes and Data Modifications"
date: 2015-09-30
categories:
author: Michael Boelen
tags: [Computer file,System call,Linux,File system,Kernel (operating system),Open (system call),Extended file attributes,Image scanner,Lynis,Metadata,Information,Computer science,Software,Information Age,Computers,Technology,Computer engineering,Information technology,Operating system technology,Computer architecture,Computer data,Data management,Computer programming,System software,Software development,Computing,Software engineering,Information technology management]
---


Monitoring File access, Changes and Data Modifications  Linux has several solutions to monitor what happens with your data. From changing contents to who accessed particular information, and at what time. For more tips regarding the Linux audit framework, have a look at our other article Configuring & Auditing Linux Systems with the Audit Daemon  File Integrity Monitoring  Another interesting level to monitor file changes, is by implementing file integrity tooling. Monitoring File Attributes  To monitor file permissions, we can also use the audit framework. The file attributes can be monitored with “-p a“.

<hr>

[Visit Link](http://linux-audit.com/monitoring-linux-file-access-changes-and-modifications/){:target="_blank" rel="noopener"}


