---
layout: post
title: "Biology in a twist -- deciphering the origins of cell behavior"
date: 2016-04-19
categories:
author: National University of Singapore
tags: [Cytoskeleton,Cell (biology),Actin,Cell biology]
---


Researchers at the Mechanobiology Institute (MBI) at the National University of Singapore have discovered that the inherent 'handedness' of molecular structures directs the behaviour of individual cells and confers them the ability to sense the difference between left and right. Cellular senses have been attributed to various force-sensing cellular structures such as the cytoskeleton. The question that has long intrigued scientists is how cytoskeleton dynamics can direct the behaviour of different cell types. It is clear from this study that the asymmetry inherent in molecular structures can define the behaviour of whole cells, and this provides new insight into the ability of cells to 'make decisions' based on the mechanical properties of its environment. Indeed relatively simple biological systems, such as cells grown on defined patterns, display a pronounced asymmetry in their movement.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/nuos-bia033015.php){:target="_blank" rel="noopener"}


