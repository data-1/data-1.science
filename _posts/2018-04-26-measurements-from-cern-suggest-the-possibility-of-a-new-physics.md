---
layout: post
title: "Measurements From CERN Suggest the Possibility of a New Physics"
date: 2018-04-26
categories:
author: ""
tags: [Particle physics,Large Hadron Collider,Standard Model,Muon,Quark,Physics,Hadron,Nuclear physics,Physical sciences,Theoretical physics,Quantum mechanics,Science,Nature,Quantum field theory]
---


A New Quantum Physics? Image credit: starsandspirals  Quantum physicists found in a recent study that mesons don't decay into kaon and muon particles often enough, according to the Standard Model predictions of frequency. The Standard Model  The Standard Model is a well-established fundamental theory of quantum physics that describes three of the four fundamental forces believed to govern our physical reality. We need to be able to see a leptoquark or Zprime pop out of LHC collisions, Tevong said. That is what we estimated in our paper: the feasibility of directly discovering leptoquarks or Zprime particles at future colliders with higher energy.

<hr>

[Visit Link](https://futurism.com/measurements-cern-new-physics/){:target="_blank" rel="noopener"}


