---
layout: post
title: "Researchers study radiation risks for astronauts journeying to Mars"
date: 2016-04-30
categories:
author: Jeff Dodge, Colorado State University
tags: [Health threat from cosmic rays,Radiation,Nature]
---


A new research facility at Colorado State University – the only one of its kind in the world – will be established with a $9 million grant from NASA to help reveal the effects of long-term exposure to space radiation as the nation prepares for a manned mission to Mars. The five-year project will renovate an existing gamma ray facility at CSU, allowing researchers to assess the impact of low doses of neutron radiation over long periods. In fact, Weil's colleague Susan Bailey is one of 10 researchers chosen by NASA to study the effects that a full year on the space station has on the body of astronaut Scott Kelly, whose mission began March 27. And astronauts encounter radiation composed of energetic cosmic rays that no current spacecraft or spacesuit can shield, Weil said. Scientists know that liver tumors resulting from space radiation are more likely to metastasize than those that develop without such exposure, Thamm said.

<hr>

[Visit Link](http://phys.org/news350200804.html){:target="_blank" rel="noopener"}


