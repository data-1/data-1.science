---
layout: post
title: "New research shows Earth's core contains 90 percent of Earth's sulfur"
date: 2015-12-11
categories:
author: European Association of Geochemistry
tags: [Sulfur,Chemical element,Geochemistry,Earth,Planetary core,Impact event,Giant-impact hypothesis,Isotope,Crust (geology),Copper,Earths mantle,Atoms,Earth sciences,Chemical elements,Geology,Planetary science,Chemistry,Nature,Physical sciences]
---


New research confirms that the Earth's core does in fact contain vast amounts of sulphur, estimated to be up to 8.5 x 1018 tonnes. This is the first time that scientists have conclusive geochemical evidence for sulphur in the Earth's core, lending weight to the theory that the Moon was formed by a planet-sized body colliding with the Earth. Because of variability in mantle composition, it is difficult to draw firm conclusions from measuring sulphur directly, so the researchers chose to analyse copper from the Earth's mantle and crust - copper is often bound to sulphur. The work comprised 3 distinct stages:  Firstly, the researchers had to estimate the isotopic composition of copper in the Earth's mantle and crust. Because the isotopes of copper divide unevenly between a sulphur-rich liquid and the rest of Earth's mantle, this shows that a large amount of sulphur must have been removed from the mantle.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/eaog-nrs061515.php){:target="_blank" rel="noopener"}


