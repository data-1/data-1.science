---
layout: post
title: "Leaving Flatland: Quantum Hall effect physics in 4-D"
date: 2018-07-03
categories:
author: "Max Planck Institute of Quantum Optics"
tags: [Dimension,Quantum mechanics,Quantum Hall effect,Optical lattice,Hall effect,Ultracold atom,Atom,Superlattice,Physics,Scientific theories,Theoretical physics,Applied and interdisciplinary physics,Physical sciences,Science]
---


In a completely different context, an international team of researchers led by Professor Immanuel Bloch (LMU/MPQ) and Professor Oded Zilberberg (ETH Zürich) has now demonstrated a way to observe physical phenomena proposed to exist in higher-dimensional systems in analogous real-world experiments. Using ultracold atoms trapped in a periodically modulated two-dimensional superlattice potential, the scientists could observe a dynamical version of a novel type of quantum Hall effect that is predicted to occur in four-dimensional systems. The magnetic field generates a Lorentz force, which deflects the particles in the direction orthogonal to their motion. Such an optical lattice is created by interference of retro-reflected laser beams of a certain wavelength along two orthogonal directions. When modulating the potential in time, the atoms predominantly move in the direction of the modulation and do so in a quantized way -- the linear (i.e. 1D) response corresponding to the 2D quantum Hall effect as predicted by Thouless.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/01/180104124113.htm){:target="_blank" rel="noopener"}


