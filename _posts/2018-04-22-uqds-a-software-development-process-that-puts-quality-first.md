---
layout: post
title: "UQDS: A software-development process that puts quality first"
date: 2018-04-22
categories:
author: "Moshe Zadka
(Correspondent)"
tags: [Reason,Software release life cycle,Continuous integration,Twisted (software),Software development,Software engineering,Computing,Technology]
---


The Ultimate Quality Development System (UQDS) is a software development process that provides clear guidelines for how to use branches, tickets, and code reviews. Tickets  In a project using the UQDS methodology, no change is allowed to happen if it's not accompanied by a ticket. Tests are also coupled with tickets. The goal is quality, and quality does not come from compromise. The answer to this wasn't to compromise on quality by getting rid of UQDS; it was to refocus the community priorities such that reviewing commits became one of the most important ways to contribute to the project.

<hr>

[Visit Link](https://opensource.com/article/18/2/uqds){:target="_blank" rel="noopener"}


