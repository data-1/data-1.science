---
layout: post
title: "Team pieces together the mystery of how single cell life forms evolved into multicellular organisms"
date: 2018-08-02
categories:
author: "Wits University"
tags: [Evolution,Cell (biology),Multicellular organism,Gene,Genetics,Organism,Life,Biology,Nature,Life sciences,Biotechnology,Biological evolution,Biochemistry,Molecular biology,Evolutionary biology]
---


By studying the genome of this simple alga, a number of genetic mechanisms that control how cells divide were associated with the origin of multicellularity. UPP is a complicated pathway that controls the cellular concentration of key proteins that drive cell division and it plays a role in many cellular functions. Featherston's study suggests that UPP may play a regulating how many divisions each species of volvocine undergoes through degradation of key molecules that control cell division. Featherston compared the genome sequence of multicellular algae to their nearest single celled relative, in order to establish the genetic differences associated with the evolution of multicellularity. Explore further How and why single cell organisms evolved into multicellular life

<hr>

[Visit Link](https://phys.org/news/2018-02-team-pieces-mystery-cell-life.html){:target="_blank" rel="noopener"}


