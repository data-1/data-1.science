---
layout: post
title: "How do bone cells grow in space?"
date: 2016-04-23
categories:
author: Novel Cell Culture Experiments On Way To International Space Station Following Spacex Launch, Biotechnology, Biological Sciences Research Council
tags: [Tissue engineering,3D cell culture,Research,Bone,Cell culture,Privacy,Technology,Science]
---


A Falcon9 launch: Credit: SpaceX  Alvetex Scaffold technology, produced by Durham University spin-out company Reinnervate, allows cells to be grown in three dimensions (3D), overcoming problems with two-dimensional (2D) culture methods and offering a more life-like model of how cells grow in tissues. A team from Massachusetts General Hospital investigating bone loss during bed rest, in microgravity or through diseases such as osteoporosis, will use the Alvetex® Scaffold in experiments 150 miles above the surface of the Earth after the equipment is delivered by the SpaceX Dragon capsule. BBSRC-funded research by Professor Stefan Przyborski and his team at Durham University was crucial to the development of the technology. Credit: Reinnervate  Being able to grow cells in a 3D culture prevents the cells from flattening and altering their structure and function, which is common when they are grown in traditional 2D Petri dishes. Light micrograph of bone cells (osteocytes) grown using Alvetex 3D cell culture system.

<hr>

[Visit Link](http://phys.org/news348308945.html){:target="_blank" rel="noopener"}


