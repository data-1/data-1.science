---
layout: post
title: "Cells in space"
date: 2018-08-16
categories:
author: ""
tags: [Micro-g environment,Experiment,Health]
---


On a biological mission Four biological experiments conducted in Europe’s Columbus module on the Station are looking into how microgravity wears down muscle, retinal and stem cells to develop medicine and other countermeasures. A closer look at Kubik Across the four experiments, conducted on behalf of Italy’s ASI space agency, the overarching goal was to understand how to prevent cell death. Researchers are studying muscle cells in two ways. While muscle atrophy, osteoporosis and vision problems may not be on the minds of the vast majority of Earth dwellers, it’s this type of research that makes it possible to develop better drugs and other measures for these problems. Setting up Kubik

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Research/Cells_in_space){:target="_blank" rel="noopener"}


