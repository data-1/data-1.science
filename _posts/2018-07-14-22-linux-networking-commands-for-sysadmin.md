---
layout: post
title: "22 Linux Networking Commands for Sysadmin"
date: 2018-07-14
categories:
author: "Aaron Kili"
tags: [Ping (networking utility),Nmap,Traceroute,Name server,Ifconfig,Iptables,System software,Network architecture,Protocols,Internet Standards,Computer architecture,Internet Protocol,Technology,Networking standards,Cyberspace,Internet,Telecommunications,Information Age,Software,Network layer protocols,Computer engineering,Internet architecture,Communications protocols,Computer science,Computing,Network protocols,Data transmission,Internet protocols,Computer networking]
---


Network Configuration, Troubleshooting, and Debugging Tools  1. ifconfig Command  ifconfig is a command-line interface tool for network interface configuration and is also used to initialize interfaces at system boot time. IP Command  ip command is another useful command-line utility for displaying and manipulating routing, network devices, interfaces. The following command will show the IP address and other information about a network interface. Netstat Command  netstat is a command-line tool that displays useful information such as network connections, routing tables, interface statistics, and much more, concerning the Linux networking subsystem. In this comprehensive guide, we reviewed some of the most used command-line tools and utilities for network management in Linux, under different categories, for system administrators, and equally useful to full-time network administrators/engineers.

<hr>

[Visit Link](https://www.tecmint.com/linux-networking-commands/){:target="_blank" rel="noopener"}


