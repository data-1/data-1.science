---
layout: post
title: "Mysterious 'Magic Island' appears on Saturn's moon Titan"
date: 2014-06-22
categories:
author: Cornell University
tags: [Titan (moon),Ligeia Mare,CassiniHuygens,Space science,Planetary science,Astronomy,Physical sciences,Outer space,Bodies of the Solar System,Solar System,Science,Planets of the Solar System,Astronomical objects]
---


Titan's methane cycle is strikingly similar to Earth’s hydrologic cycle and the only other one known to include stable bodies of surface liquids, such as this north polar sea Ligeia Mare. Astronomers have discovered a bright, mysterious geologic object – where one never existed – on Cassini mission radar images of Ligeia Mare, the second-largest sea on Saturn's moon Titan. To discover this geologic feature, the astronomers relied on an old technique – flipping. Region of the transient features before they were discovered. The moon's northern hemisphere is transitioning from spring to summer.

<hr>

[Visit Link](http://phys.org/news322590086.html){:target="_blank" rel="noopener"}


