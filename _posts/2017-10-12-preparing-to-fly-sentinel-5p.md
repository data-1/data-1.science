---
layout: post
title: "Preparing to fly Sentinel-5P"
date: 2017-10-12
categories:
author: ""
tags: [Sentinel-5,Outer space,Flight,Spacecraft,Space science,Space vehicles,Spaceflight technology,Space programs,Spaceflight,Astronautics]
---


Enabling & Support Preparing to fly Sentinel-5P 26/09/2017 3932 views 63 likes  The teams that will fly Sentinel-5P are training intensively for launch, ensuring that everyone knows their job and can react to any emergency. A ‘team of teams’ at ESA’s mission control centre has spent months preparing to assume control of Europe’s next Earth observation mission, and the final weeks before launch have been the most intense. “It’s called ‘acquisition of signal’, and it’s the moment when the years of careful development and preparation for our mission control systems, and the months of training for our mission control teams, will prove their worth,” says flight operations director Pier Paolo Emanuelli. Getting ready to go to space Between now and launch day, the final round of ‘sims’ will take place twice per week, culminating with a final dress rehearsal on 11 October, which, by tradition, simulates a completely normal launch sequence. During the rehearsal, the mission control systems will be connected to Sentinel-5P sitting on top of the rocket via a ground link, which will be removed only a few minutes before liftoff.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Operations/Preparing_to_fly_Sentinel-5P){:target="_blank" rel="noopener"}


