---
layout: post
title: "DNA of early Neanderthal gives timeline for new modern human-related dispersal from Africa"
date: 2017-08-31
categories:
author: "Max Planck Institute for the Science of Human History"
tags: [Recent African origin of modern humans,Denisovan,Neanderthal,Interbreeding between archaic and modern humans,Human,Mitochondrial DNA,Genetics,Human populations,Biological anthropology,Human evolution,Biology]
---


Complicated relationship between Neanderthals and modern humans  Prior research analyzing nuclear DNA from Neanderthals and modern humans estimated the split of the two groups at approximately 765,000 to 550,000 years ago. However, studies looking at mitochondrial DNA showed a much more recent split of around 400,000 years ago. This human group, more closely related to modern humans than to Neanderthals, could have introduced their mitochondrial DNA to the Neanderthal population in Europe through genetic admixture, as well as contributing a small amount of nuclear DNA to Neanderthals but not to Denisovans as recently detected. Timeline for additional migration of hominins out of Africa  The proposed scenario is that after the divergence of Neanderthals and modern human mitochondrial DNA (dated to a maximum of 470,000 years ago), but before HST and the other Neanderthals diverged (dated to a minimum of 220,000 years ago), a group of hominins moved from Africa to Europe, introducing their mitochondrial DNA to the Neanderthal population. This scenario reconciles the discrepancy in the nuclear DNA and mitochondrial DNA phylogenies of archaic hominins and the inconsistency of the modern human-Neanderthal population split time estimated from nuclear DNA and mitochondrial DNA, explains Johannes Krause, also of the Max Planck Institute for the Science of Human History, senior author of the study.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-07/mpif-doe062917.php){:target="_blank" rel="noopener"}


