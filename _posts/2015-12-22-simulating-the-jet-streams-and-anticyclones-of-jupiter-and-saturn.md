---
layout: post
title: "Simulating the jet streams and anticyclones of Jupiter and Saturn"
date: 2015-12-22
categories:
author: University of Alberta
tags: [Jupiter,Saturn,Jet stream,Anticyclone,Weather,Storm,CassiniHuygens,Planet,Bodies of the Solar System,Astronomical objects,Physical sciences,Nature,Planets of the Solar System,Solar System,Sky,Planets,Outer space,Planetary science,Astronomy,Space science]
---


Heimpel notes that despite 350 years of observation, the origin and dynamics of planetary jet streams and vortices or planetary storms remain debated. One of the big questions we have is how deep do these structures go? says Heimpel. Our simulations imply that the jet streams plunge deep into the interior, while the storms are rather shallow. At its core, our research is curiosity-based, and our ideas are driven by observations.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-11/uoa-stj112715.php){:target="_blank" rel="noopener"}


