---
layout: post
title: "Duplications of noncoding DNA may have affected evolution of human-specific traits"
date: 2017-10-18
categories:
author: American Society of Human Genetics
tags: [Gene duplication,Gene,Genome,Genetics,Evolution,Human genome,Human,Non-coding DNA,DNA,Brain,Enhancer (genetics),Gene expression,Regulatory sequence,Branches of genetics,Biochemistry,Biotechnology,Life sciences,Molecular biology,Biology]
---


Duplications of large segments of noncoding DNA in the human genome may have contributed to the emergence of differences between humans and nonhuman primates, according to results presented at the American Society of Human Genetics (ASHG) 2017 Annual Meeting in Orlando, Fla. Identifying these duplications, which include regulatory sequences, and their effect on traits and behavior may help scientists explain genetic contributions to human disease. Paulina Carmona-Mora, PhD, who presented the work; Megan Dennis, PhD; and their colleagues at the University of California, Davis, study the history of human-specific duplications (HSDs), segments of DNA longer than 1,000 base pairs that are repeated in humans but not in primates or other animals. What's special about these regulatory elements is that they have the propensity to impact the expression of genes nearby on the same chromosome, as well as elsewhere in the genome, said Dr. Dennis. This allowed them to identify areas likely to contain enhancers, which are regulatory elements that increase expression of other genes, and assess their effect on gene expression across organs and tissue types. They are also assessing the effects of a duplication's introduction on the structure and function of nearby segments.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171018133230.htm){:target="_blank" rel="noopener"}


