---
layout: post
title: "Research enhances performance of Germany's new fusion device"
date: 2018-07-01
categories:
author: "John Greenwald, Princeton Plasma Physics Laboratory"
tags: [Stellarator,Wendelstein 7-X,Princeton Plasma Physics Laboratory,Fusion power,Technology,Plasma physics,Science]
---


Credit: Max Planck Institute of Plasma Physics  A team of U.S. and German scientists has used a system of large magnetic trim coils designed and delivered by the U.S. Department of Energy's (DOE) Princeton Plasma Physics Laboratory (PPPL) to achieve high performance in the latest round of experiments on the Wendelstein 7-X (W7-X) stellarator. The German machine, the world's largest and most advanced stellarator, is being used to explore the scientific basis for fusion energy and test the suitability of the stellarator design for future fusion power plants. The recent experiments demonstrated the ability of the trim coils to measure and correct such deviations, which are known as error fields. The trim coils have proven extremely useful, not only by ensuring a balanced plasma exhaust onto the divertor plates, but also as a tool for the physicists to perform magnetic field measurements of unprecedented accuracy, said Thomas Sunn Pederson, Max Planck director of stellarator edge and divertor physics. The fact that we only required 10 percent of the rated capacity of the trim coils is a testament to the precision with which W7-X was constructed, Lazerson said.

<hr>

[Visit Link](https://phys.org/news/2018-03-germany-fusion-device.html){:target="_blank" rel="noopener"}


