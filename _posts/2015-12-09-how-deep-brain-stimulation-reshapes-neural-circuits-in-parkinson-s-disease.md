---
layout: post
title: "How deep-brain stimulation reshapes neural circuits in Parkinson's disease"
date: 2015-12-09
categories:
author: University of California - San Francisco
tags: [University of California San Francisco,Brain,Neuroscience,Medicine,Clinical medicine,Health,Health care,Health sciences,Medical specialties,Nervous system]
---


When the DBS device was activated and began stimulating the STN, the effect of the stimulation reached the motor cortex, where over-synchronization rapidly diminished. Importantly, said Starr, recordings revealed that DBS eliminated excessive synchrony of motor cortex activity and facilitated movement without altering normal changes in brain activity that accompany movements. To broaden opportunities for research, Starr and his team have collaborated with medical device company Medtronic on a new generation of permanently implantable DBS devices that can record activity in the motor cortex while delivering stimulation to the STN. Five UCSF patients have received with these new devices, and all data they collect can be uploaded for research during follow-up visits, de Hemptinne said, which will bring an even deeper understanding of how DBS reshapes brain activity. Now celebrating the 150th anniversary of its founding as a medical college, UCSF is dedicated to transforming health worldwide through advanced biomedical research, graduate-level education in the life sciences and health professions, and excellence in patient care.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/uoc--hds041015.php){:target="_blank" rel="noopener"}


