---
layout: post
title: "Sentinel-2B prepared for liftoff"
date: 2017-08-31
categories:
author: ""
tags: [Sentinel-2,Outer space,Spaceflight,European space programmes,Satellites,Spacecraft,Earth observation satellites,Pan-European scientific organizations,European Space Agency,Space policy of the European Union,Space agencies]
---


This timelapse video shows Sentinel-2B satellite, from final preparations to liftoff on a Vega launcher, flight VV09, from Europe’s Spaceport in French Guiana, on 7 March 2017. Sentinel-2B is the second satellite in the Sentinel-2 mission for Europe’s Copernicus environment monitoring programme. Designed as a two-satellite constellation – Sentinel-2A and -2B – the Sentinel-2 mission carries an innovative wide swath high-resolution multispectral imager with 13 spectral bands for a new perspective of our land and vegetation. This information is used for agricultural and forestry practices and for helping manage food security. It also provides information on pollution in lakes and coastal waters.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2017/03/Sentinel-2B_prepared_for_liftoff){:target="_blank" rel="noopener"}


