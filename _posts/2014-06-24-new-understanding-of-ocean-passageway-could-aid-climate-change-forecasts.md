---
layout: post
title: "New understanding of ocean passageway could aid climate change forecasts"
date: 2014-06-24
categories:
author: Talia Ogliore, University Of Hawaii At Manoa
tags: [Oceanography,La Nia,El Nio,El NioSouthern Oscillation,Physical oceanography,Ocean,Climate change,Climate,Atmospheric sciences,Meteorology,Environmental science,Hydrology,Nature,Earth phenomena,Earth sciences,Natural environment,Physical geography,Applied and interdisciplinary physics,Hydrography]
---


The scientists have found that the flow of water in the Indonesian Throughflow – the network of straits that pass Indonesia's islands – has changed since the late 2000s under the influence of dominant La Niña conditions. This is a seminal paper on a key oceanographic feature that may have great utility in climate research in this century, said Eric Lindstrom, a physical oceanography program scientist who co-chairs the Global Ocean Observing System Steering Committee at NASA, which funded Sprintall's portion of the study. Model simulations have suggested that without this flow, the Indian Ocean would be generally colder at the surface as the Pacific would not be able to route warm water to it as efficiently. La Niña and El Niño are characterized in part by the location of a warm pool of surface water in the Pacific Ocean. Warm water in the western Pacific near Indonesia is usually associated with La Niña and warm water in the eastern equatorial Pacific with El Niño.

<hr>

[Visit Link](http://phys.org/news322813622.html){:target="_blank" rel="noopener"}


