---
layout: post
title: "CRISPR-Cas9 tool expedites production of biofuel precursors and specialty polymers in living systems"
date: 2016-01-29
categories:
author: Sarah Nightingale, University Of California - Riverside
tags: [CRISPR gene editing,Genome editing,Yarrowia,Yeast,Biotechnology,Life sciences,Technology,Biology,Chemistry]
---


Gene editing in this yeast strain will lead to new precursors for biofuels and specialty polymers. Published recently in the journal ACS Synthetic Biology, the research involves the oleaginous (oil-producing) yeast Yarrowia lipolytica, which is known for converting sugars to lipids and hydrocarbons that are difficult to make synthetically. Described in 2012, CRISPR-Cas9 is a groundbreaking technique that enables scientists to make precise targeted changes in living cells. Traditionally, researchers have focused on model organisms that are relatively easy to manipulate at the genetic level, and those working on less tractable species have had to go through long and tedious processes to create new strains. Wheeldon said the current work was the first step in a National Science Foundation-funded project to create long chain hydrocarbons—used to make specialty polymers, adhesives, coatings and fragrances—from yeast rather than synthetically.

<hr>

[Visit Link](http://phys.org/news/2016-01-crispr-cas9-tool-production-biofuel-precursors.html){:target="_blank" rel="noopener"}


