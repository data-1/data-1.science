---
layout: post
title: "Linguists explore the evolution of color in new study"
date: 2017-03-19
categories:
author: "Yale University"
tags: [Color term,News aggregator,Psychology,Linguistics,Interdisciplinary subfields,Science,Cognition,Branches of science,Cognitive science,Behavioural sciences]
---


A major question in the study of both anthropology and cognitive science is why the world's languages show recurrent similarities in color naming. Linguists at Yale tracked the evolution of color terms across a large language tree in Australia in order to trace the history of these systems. We need large numbers of languages to get a good sample size and a good variety of colors, says Bowern. The Pama-Nyungan societies are also relatively similar in terms of material culture and the sorts of ways they make use of resources for color. We were able to reconstruct a number of different systems by testing two or three explicit hypotheses.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/12/161209133255.htm){:target="_blank" rel="noopener"}


