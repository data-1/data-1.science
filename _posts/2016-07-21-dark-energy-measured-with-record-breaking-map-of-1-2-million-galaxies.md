---
layout: post
title: "Dark energy measured with record-breaking map of 1.2 million galaxies"
date: 2016-07-21
categories:
author: "DOE/Lawrence Berkeley National Laboratory"
tags: [Universe,Dark energy,Physical cosmology,Dark matter,Sloan Digital Sky Survey,Expansion of the universe,Galaxy,Physics,Astronomy,Science,Nature,Space science,Cosmology,Astrophysics,Physical sciences]
---


The team constructed this map to make one of the most precise measurements yet of the dark energy currently driving the accelerated expansion of the Universe. This map has allowed us to make the best measurements yet of the effects of dark energy in the expansion of the Universe. Shaped by a continuous tug-of-war between dark matter and dark energy, the map revealed by BOSS allows scientists to measure the expansion rate of the Universe and thus determine the amount of matter and dark energy that make up the present-day Universe. BOSS measures the expansion rate of the Universe by determining the size of the baryonic acoustic oscillations (BAO) in the three-dimensional distribution of galaxies. SCIENTIFIC CONTACTS:  David Schlegel, Lawrence Berkeley National Laboratory  djschlegel@lbl.gov  Shirley Ho, Lawrence Berkeley National Laboratory and Carnegie Mellon University  shirleyho@lbl.gov  SDSS-III is managed by the Astrophysical Research Consortium for the Participating Institutions of the SDSS?III Collaboration including the University of Arizona, the Brazilian Participation Group, Brookhaven National Laboratory, Carnegie Mellon University, University of Florida, the French Participation Group, the German Participation Group, Harvard University, the Instituto de Astrofisica de Canarias, the Michigan State/Notre Dame/JINA Participation Group, Johns Hopkins University, Lawrence Berkeley National Laboratory, Max Planck Institute for Astrophysics, Max Planck Institute for Extraterrestrial Physics, New Mexico State University, New York University, Ohio State University, Pennsylvania State University, University of Portsmouth, Princeton University, the Spanish Participation Group, University of Tokyo, University of Utah, Vanderbilt University, University of Virginia, University of Washington, and Yale University.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/dbnl-dem071416.php){:target="_blank" rel="noopener"}


