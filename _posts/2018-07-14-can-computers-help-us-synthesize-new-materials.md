---
layout: post
title: "Can computers help us synthesize new materials?"
date: 2018-07-14
categories:
author: "Larry Hardesty"
tags: [Autoencoder,Machine learning,Technology,Science,Cognitive science,Branches of science]
---


Now, in a paper in the journal npj Computational Materials, the same three materials scientists, with a colleague in MIT’s Department of Electrical Engineering and Computer Science (EECS), take a further step in that direction, with a new artificial-intelligence system that can recognize higher-level patterns that are consistent across recipes. For instance, the new system was able to identify correlations between “precursor” chemicals used in materials recipes and the crystal structures of the resulting products. The problem with sparse, high-dimensional data is that for any given training example, most nodes in the bottom layer receive no data. Such systems, in which the output attempts to match the input, are called “autoencoders.”  Autoencoding compensates for sparsity, but to handle scarcity, the researchers trained their network on not only recipes for producing particular materials, but also on recipes for producing very similar materials. After training a variational autoencoder with a two-node middle layer on recipes for manganese dioxide and related compounds, the researchers constructed a two-dimensional map depicting the values that the two middle nodes took on for each example in the training set.

<hr>

[Visit Link](http://news.mit.edu/2017/machine-learning-system-synthesizes-new-materials-recipes-1221){:target="_blank" rel="noopener"}


