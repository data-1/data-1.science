---
layout: post
title: "Extra DNA creates cucumber with all female flowers"
date: 2016-05-07
categories:
author: Cornell University
tags: [Flower,Genetics,Genome,Agriculture,Plant,Gene duplication,Sex,Structural variation,Gene,Plant reproductive morphology,Cucumber,DNA sequencing,Biotechnology,Life sciences,Biology]
---


Some high-yield cucumber varieties produce only female flowers, and a new study identifies the gene duplication that causes this unusual trait. When grown in nutrient-rich, soil-free greenhouses they produce a cucumber from each flower, greatly increasing the yield over monoecious varieties that produce both types of flowers. If you compare the greenhouse production in the Netherlands, which uses plants with female flowers, to China, where they use monoecious plants and normal agricultural practices, the production in the Netherlands is about 15 times greater than in China, said Fei. The researchers discovered the extra DNA by screening genome sequences from a core collection of 115 different cucumber lines. The analyses show that collectively, these structural variations affect more than 1,600 genes in the cucumber genome.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/cu-edc060415.php){:target="_blank" rel="noopener"}


