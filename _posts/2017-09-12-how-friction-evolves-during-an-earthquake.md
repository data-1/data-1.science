---
layout: post
title: "How friction evolves during an earthquake"
date: 2017-09-12
categories:
author: "California Institute Of Technology"
tags: [Earthquake,Friction,Earthquake rupture,Applied and interdisciplinary physics,Science]
---


Dynamic friction evolves throughout an earthquake, affecting how much and how fast the ground will shake and thus, most importantly, the destructiveness of the earthquake. Yet the precise nature of dynamic friction remains one of the biggest unknowns in earthquake science. Previously, it commonly had been believed that the evolution of dynamic friction was mainly governed by how far the fault slipped at each point as a rupture went by—that is, by the relative distance one side of a fault slides past the other during dynamic sliding. At the facility, researchers use advanced high-speed optical diagnostics and other techniques to study how earthquake ruptures occur. Our unique facility allows us to study dynamic friction laws by following individual, fast-moving shear ruptures and recording friction along their sliding faces in real time, Rosakis says.

<hr>

[Visit Link](https://phys.org/news/2017-08-friction-evolves-earthquake.html){:target="_blank" rel="noopener"}


