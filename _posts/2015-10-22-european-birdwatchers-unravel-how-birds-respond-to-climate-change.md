---
layout: post
title: "European birdwatchers unravel how birds respond to climate change"
date: 2015-10-22
categories:
author: University of Copenhagen - Faculty of Science 
tags: [Bird migration,American Association for the Advancement of Science,Citizen science,Climate change,Natural environment]
---


The information they've gathered shows birds respond to changing conditions in different seasons of the year. For example, the study found warmer winters benefit resident birds, such as the Short-toed treecreeper and the Collared Dove, with more productive spring times benefiting short-distance migrants such as the Goldfinch and the Woodlark. We found benefits from conditions observed under climate change for both resident birds, short-distance migrants and long distance-migrants, but at very different times of the year that complement their breeding season. The results were generated with yearly data on 51 different bird species gathered by around 50,000 volunteers in 18 different European countries between 1990 to 2008. It found long-distance migrants may be particularly vulnerable to the combination of agricultural intensification and climate change.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/fos--ebu101915.php){:target="_blank" rel="noopener"}


