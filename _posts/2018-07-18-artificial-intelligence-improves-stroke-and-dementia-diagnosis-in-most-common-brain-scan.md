---
layout: post
title: "Artificial Intelligence improves stroke and dementia diagnosis in most common brain scan"
date: 2018-07-18
categories:
author: "Imperial College London"
tags: [Stroke,Dementia,Neuroimaging,Magnetic resonance imaging,CT scan,Disease,Medicine,Clinical medicine,Medical specialties,Health,Health sciences,Diseases and disorders,Causes of death,Health care,Neuroscience]
---


Machine learning has detected one of the commonest causes of dementia and stroke, in the most widely used form of brain scan (CT), more accurately than current methods. Researchers say that this technology can help clinicians to administer the best treatment to patients more quickly in emergency settings -- and predict a person's likelihood of developing dementia. In CT scans it is often difficult to decide where the edges of the SVD are, making it difficult to estimate the severity of the disease, explains Dr Bentley. Dr Bentley added: Current methods to diagnose the disease through CT or MRI scans can be effective, but it can be difficult for doctors to diagnose the severity of the disease by the human eye. advertisement  The study used historical data of 1082 CT scans of stroke patients across 70 hospitals in the UK between 2000-2014, including cases from the Third International Stroke Trial.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/05/180516101440.htm){:target="_blank" rel="noopener"}


