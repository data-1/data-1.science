---
layout: post
title: "Supercomputer simulations show new target in HIV-1 replication"
date: 2018-08-12
categories:
author: "University of Texas at Austin, Texas Advanced Computing Center"
tags: [Texas Advanced Computing Center,HIV,Xeon Phi,Simulation,Science,Technology]
---


Scientists don't understand many of the details of how HIV-1 can fool our immune system cells so effectively. Supercomputers helped model a key building block in the HIV-1 protective capsid, which could lead to strategies for potential therapeutic intervention in HIV-1 replication. Perilla ran simulations of inositol phosphate interactions with HIV structural proteins CA-CTD-SP1 using NAMD through allocations on XSEDE, the Extreme Science and Engineering Environment, funded by the National Science Foundation. These machines really enable new science. We really need to have the scale of these machines available to the scientific community to enable the kind of science that we do, Perilla said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/uota-sss080918.php){:target="_blank" rel="noopener"}


