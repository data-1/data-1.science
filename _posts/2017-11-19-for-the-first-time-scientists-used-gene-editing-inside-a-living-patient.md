---
layout: post
title: "For the First Time, Scientists Used Gene Editing Inside a Living Patient"
date: 2017-11-19
categories:
author: ""
tags: [Zinc finger nuclease,Hunter syndrome,Biology,Genetics,Biotechnology,Life sciences,Clinical medicine,Health sciences,Molecular biology,Branches of genetics,Medicine,Medical treatments,Biochemistry,Health]
---


These enzymes cut a gene that's present in liver cells, inserting a functional copy of the gene that produces the desired enzyme. The treatment was developed by Sangamo Therapeutics, a biotechnology firm based in California. ZFNs are a more unwieldy method of performing gene editing than CRISPR Cas-9, but they do offer more precision. Gene Genie  A successful trial like this demonstrates how far we've come in terms of being able to make edits to the human genome. The use of ZFNs to treat Hunter syndrome offers up a huge improvement over the way the disease is addressed at present.

<hr>

[Visit Link](https://futurism.com/first-time-scientists-used-gene-editing-inside-living-patient/){:target="_blank" rel="noopener"}


