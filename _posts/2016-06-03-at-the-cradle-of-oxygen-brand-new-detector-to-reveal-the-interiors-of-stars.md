---
layout: post
title: "At the cradle of oxygen: Brand-new detector to reveal the interiors of stars"
date: 2016-06-03
categories:
author: "University of Warsaw, Faculty of Physics"
tags: [Extreme Light Infrastructure,Gamma ray,Star,Helium,Alpha particle,Atomic nucleus,Nuclear fusion,Photon,Electron,Carbon,Nuclear physics,Oxygen,Gas,Radiation,Physical sciences,Nature,Chemistry,Physics,Particle physics]
---


A demonstrator version of the detector, constructed at the Faculty of Physics, University of Warsaw (FUW), has recently completed the first round of tests in Romania. During the first stage, they convert hydrogen into helium, then helium into carbon, oxygen and nitrogen, with heavier elements formed in subsequent stages. The gamma radiation beam passes through the gas, with some of the photons colliding with oxygen nuclei to produce carbon and helium nuclei. The device was tested with a beam of alpha particles (helium nuclei). (Source: Faculty of Physics, University of Warsaw)  FUW160602_fot02s.jpg  HR: http://www.fuw.edu.pl/press/images/2016/FUW160602b_fot02.jpg  Example track of an alpha particle (energy 15 MeV) from the test beam registered with the demonstrator detector mini-eTPC.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/fopu-atc060216.php){:target="_blank" rel="noopener"}


