---
layout: post
title: "Qbits for Cubists: Robots Made These Incredible Works of Fine Art"
date: 2018-07-14
categories:
author: ""
tags: [Robot,Artificial intelligence,Painting,Art,Robotics,Technology,Cognitive science,Branches of science,Cybernetics,Cognition,Computing,Emerging technologies]
---


That's right, sophisticated robots can now create works of art comparable to the old masters. Image Credit: CloudPainter/RoboArt  The teams used a number of different approaches, showing that there are a hell of a lot of ways to interpret “artwork created by robot.” The first-place team, CloudPainter, used a machine learning system to generate vivid, sometimes outlandish portraits and landscapes. Image Credit: Creative Machines Labs/Columbia Univeristy  “I think in the short term, robots will be used more in the final production of digital art — especially those that have a lot of detail or repetitive elements,” Andrew Conru, the artist and engineer who founded RobotArt, told Futurism. He doesn’t think that robotic or algorithmic artists will replace creative humans. These algorithms and robotic arms create images but cannot step back and appreciate them.

<hr>

[Visit Link](https://futurism.com/contest-creative-robots-fine-art/){:target="_blank" rel="noopener"}


