---
layout: post
title: "Asteroseismologists listen to the relics of the Milky Way"
date: 2016-06-13
categories:
author: "University of Birmingham"
tags: [Astronomy,Star,Galaxy,Milky Way,Kepler space telescope,Asteroseismology,Stellar astronomy,Astrophysics,Physics,Natural sciences,Science,Astronomical objects,Physical sciences,Space science]
---


Astrophysicists from the University of Birmingham have captured the sounds of some of the oldest stars in our galaxy, the Milky Way, according to research published today in the Royal Astronomical Society journal Monthly Notices. The research team, from the University of Birmingham's School of Physics and Astronomy, has reported the detection of resonant acoustic oscillations of stars in 'M4', one of the oldest known clusters of stars in the Galaxy, some 13 billion years old. These oscillations lead to miniscule changes or pulses in brightness, and are caused by sound trapped inside the stars. Dr Andrea Miglio, from the University of Birmingham's School of Physics and Astronomy, who led the study, said: 'We were thrilled to be able to listen to some of the stellar relics of the early universe. Dr Guy Davies, from the University of Birmingham's School of Physics and Astronomy, and co-author on the study, said: 'The age scale of stars has so far been restricted to relatively young stars, limiting our ability to probe the early history of our Galaxy.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/uob-alt060216.php){:target="_blank" rel="noopener"}


