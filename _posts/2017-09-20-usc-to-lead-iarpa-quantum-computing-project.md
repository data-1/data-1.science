---
layout: post
title: "USC to lead IARPA quantum computing project"
date: 2017-09-20
categories:
author: "University of Southern California"
tags: [Quantum computing,Quantum annealing,Computing,Information Sciences Institute,Intelligence Advanced Research Projects Activity,Theoretical computer science,Applied mathematics,Science,Computer science,Branches of science,Technology]
---


USC will lead the effort among various universities and private contractors to design, build and test 100 qubit quantum machines. At USC, the effort includes the USC Center for Quantum Information Science and Technology in the Viterbi School of Engineering, and the Center for Quantum Computing at the Information Sciences Institute, a unit of the Viterbi School. Government partner MIT Lincoln Labs will fabricate the hardware designed by the USC-led consortium. The team's goal is to build quantum annealers that allow for what quantum computing researchers call high coherence or long coherence time so that the qubits behave in a quantum fashion for long periods of time. USC Viterbi School of Engineering  Engineering Studies began at the University of Southern California in 1905.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-06/uosc-utl062617.php){:target="_blank" rel="noopener"}


