---
layout: post
title: "International Space Station panoramic tour"
date: 2015-09-03
categories:
author: "$author"   
tags: [VR photography,Panorama,Technology,Computing,Digital media,Software]
---


Node-3 Tranquillity provides life-support for the International Space Station. Previous releases:  Explore Zvezda module via Flickr, Facebook or Youtube. Explore Unity module via Flickr, Facebook or Youtube. Click here to explore in full screen  Just before ESA astronaut Samantha Cristoforetti left the International Space Station after 199 days, she took up to 15 pictures inside each module. Now, the images have been stitched together to create this interactive panorama.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/International_Space_Station/Highlights/International_Space_Station_panoramic_tour){:target="_blank" rel="noopener"}


