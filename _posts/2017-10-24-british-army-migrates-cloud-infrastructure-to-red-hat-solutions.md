---
layout: post
title: "British Army Migrates Cloud Infrastructure to Red Hat Solutions"
date: 2017-10-24
categories:
author:  
tags: [Red Hat,Ansible (software),Cloud computing,Patch (computing),DevOps,File system,Forward-looking statement,Computing,Technology,Information technology,Systems engineering,Software engineering,Computer engineering,Software,Information Age,Computer science,Information technology management]
---


With Red Hat Enterprise Linux and Ansible Tower by Red Hat, the British Army is able to deliver applications more quickly and efficiently, with less downtime, to better meet the needs of end users. To help with management, the Army deployed Ansible Tower by Red Hat, initially using the enterprise-grade agentless automation platform to automate the installation of routine patches and configuration updates to Red Hat Enterprise Linux. The British Army has expanded its Ansible Tower implementation, now using it to provide rapid patching and system reconfiguration for urgent requirements, and to provide other DevOps functionality for routine system administration. Supporting Quotes  Lt Col. Dorian Seabrook, Head of Operations, IAS Branch, British Army  “We want to deliver software quicker and more efficiently to meet the end users’ requirements and we absolutely made the right choice with Ansible. Our users are just staggered by the agility and the turnaround time and what we can now offer them.”  Joe Fitzgerald, vice president, Management, Red Hat  “The British Army, like many organizations, is under constant pressure to deliver software and services quickly with no downtime.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/british-army-migrates-cloud-infrastructure-red-hat-solutions){:target="_blank" rel="noopener"}


