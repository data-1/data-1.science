---
layout: post
title: "New quantum tunneling application captures electricity from Earth’s heat"
date: 2018-07-03
categories:
author: "Greg Beach"
tags: []
---


Researchers at King Abdullah University of Science and Technology (KAUST) in Saudi Arabia have learned how to produce electricity from Earth’s excess heat through quantum tunneling. Quantum tunneling is a phenomenon in which particles are able to tunnel through a barrier that, under the rules of classical mechanics, they are usually unable to pass through. It is estimated that the global output of infrared radiation may be as much as millions of gigawatts per second. SIGN UP  Related: New double-pane quantum dot solar windows generate power with better efficiency  Via a tunneling device known as a metal-insulator-metal (MIM) diode, electrons are able to pass through a small barrier, despite lacking the energy classically required to do so. The technology could be applied to solar panels, which currently only harvest a small percentage of the potential heat and light energy available for electrical power.

<hr>

[Visit Link](https://inhabitat.com/new-quantum-tunneling-application-captures-electricity-from-earths-heat){:target="_blank" rel="noopener"}


