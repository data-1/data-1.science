---
layout: post
title: "Researchers developed single cell level sorting technology using sound waves"
date: 2017-10-17
categories:
author: Singapore University Of Technology
tags: [Sound,Cell sorting,Microfluidics,Flow cytometry,Technology]
---


Dr Ai's research team recently developed a highly accurate single cell level sorting technology using a highly focused sound wave beam (50 μm wide roughly ¼ of a single human hair's diameter). This new cell manipulation technology enables highly accurate isolation of rare cell populations in complex biological samples. Microfluidics technology capable of precise cell manipulation has great potential to reinvent the next-generation cell sorting technology. In this research, Dr Ai's team designed and built an acoustic sorting system that included a disposable microfluidic channel, a reusable sound wave generator and a fluorescence detection module. The sound wave beam with a width of 50 μm is highly localized, enabling accurate sorting at the single cell level.

<hr>

[Visit Link](https://phys.org/news/2017-10-cell-technology.html){:target="_blank" rel="noopener"}


