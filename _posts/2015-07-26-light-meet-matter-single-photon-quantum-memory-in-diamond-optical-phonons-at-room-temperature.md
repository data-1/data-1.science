---
layout: post
title: "Light, meet matter: Single-photon quantum memory in diamond optical phonons at room temperature"
date: 2015-07-26
categories:
author: Stuart Mason Dambrot
tags: [Quantum memory,Photon,Quantum mechanics,Science,Applied and interdisciplinary physics,Physics,Theoretical physics,Optics,Atomic molecular and optical physics,Physical chemistry,Electromagnetic radiation]
---


The laser output is split to pump the photon source and to produce the orthogonally polarized read and write beams. To date, however, development of a practical single-photon quantum memory has been stymied by (1) the need for high efficiency, (2) the read/write lasers used introducing noise that contaminates the quantum state, and (3) decoherence of the information stored in the memory. The bandwidth of a stored pulse here is ultimately only limited by the large 40 THz energy of the phonon, although he acknowledges that in this investigation that it was experimentally limited by their longer duration 2 THz write pulse. Sussman adds that, broadly speaking, the researchers work in three areas: developing optical methods of controlling quantum systems; developing novel quantum systems; and combining these to then develop quantum technology applications. Explore further Improved interface for a quantum internet  More information: Storage and Retrieval of THz-Bandwidth Single Photons Using a Room-Temperature Diamond Quantum Memory, Physical Review Letters (2015) 114: 053602, Storage and Retrieval of THz-Bandwidth Single Photons Using a Room-Temperature Diamond Quantum Memory,(2015)053602, doi:10.1103/PhysRevLett.114.053602  Related:  1Entangling Macroscopic Diamonds at Room Temperature, Science (2011) 334(6060): 1253-1256, doi:10.1126/science.1211914

<hr>

[Visit Link](http://phys.org/news344501981.html){:target="_blank" rel="noopener"}


