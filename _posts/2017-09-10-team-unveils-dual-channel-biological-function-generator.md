---
layout: post
title: "Team unveils dual-channel biological function generator"
date: 2017-09-10
categories:
author: "Rice University"
tags: [Electronic circuit,Optogenetics,Electronics,Signal,Gene,Gene expression,Oscilloscope,Voltage,Protein,Genetics,Sensor,Electrical engineering,Technology]
---


Credit: Karl Gerhardt/Rice University  Rice University bioengineers who specialize in creating tools for synthetic biology have unveiled the latest version of their biofunction generator and bioscilloscope, an optogenetic platform that uses light to activate and study two biological circuits at a time. Genetic circuits can be switched on or off—produce protein or not—and they can be tuned to produce more or less protein, much like voltage from an electronic circuit can be raised or lowered. In the new paper, recent Ph.D. graduate and lead author Evan Olson and colleagues tested new dual-function tools using the latest optogenetic hardware and software tools developed by Tabor's lab in conjunction with a new mathematical model for the biofunction generator output. The model allows us to predict the output gene-expression response to any light input signal, regardless of how the intensity or spectral composition of the light signal changes over time, Olson said. In the second, they demonstrated multiplexed control by simultaneously driving two independent gene expression signals in two optogenetic circuits in the same bacteria.

<hr>

[Visit Link](https://phys.org/news/2017-05-team-unveils-dual-channel-biological-function.html){:target="_blank" rel="noopener"}


