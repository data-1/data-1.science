---
layout: post
title: "Water scarcity and climate change through 2095"
date: 2015-10-29
categories:
author: Pacific Northwest National Laboratory
tags: [Climate change mitigation,Climate change,Politics of climate change,Water scarcity,Greenhouse gas emissions,Water,Biomass,Climate variability and change,Global environmental issues,Environmental issues with fossil fuels,Nature,Environmental impact,Societal collapse,Environment,Human impact on the environment,Environmental issues,Earth sciences,Natural environment]
---


When they incorporated water use and availability in this powerful engine and ran scenarios of possible climate mitigation policy targets, they found that without any climate policy to curb carbon emissions, half the world will be living under extreme water scarcity. Schematic of the Global Change Assessment Model and systems, with water systems added. When compared to a baseline scenario where no climate policy is implemented, water scarcity declines under a universal carbon tax mitigation policy, but it increases with a fossil fuel and industrial emissions carbon tax mitigation scenario by the year 2095, mainly due to variations in prevailing bioenergy productions. Integrated Assessment of Global Water Scarcity over the 21st Century: Global Water Supply and Demand under Extreme Radiative Forcing. Integrated Assessment of Global Water Scarcity over the 21st Century: Global Water Supply and Demand under Extreme Radiative Forcing.18:2859-2883.

<hr>

[Visit Link](http://phys.org/news327560165.html){:target="_blank" rel="noopener"}


