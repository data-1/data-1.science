---
layout: post
title: "Why the Sky Enchants Us: Our Longing for Transcendence and How Myths Elevate Human Life"
date: 2015-07-10
categories:
author: Maria Popova
tags: [Heaven,Transcendence (religion),Myth,God,Religion]
---


One of the most fascinating expressions of mythmaking in our search for meaning has to do with the sky, the sweeping mystery of which has enchanted human beings since the dawn of our species. Ancient astronomers tried to interpret it, Goethe wrote breathtaking poems for it, and Georgia O’Keeffe captured its mesmerism perfectly in a letter to her best friend: “There is something wonderful about the bigness and the lonelyness and the windyness of it all.”  Armstrong traces the origin of the sky’s ancient allure:  Some of the very earliest myths, probably dating back to the Paleolithic period, were associated with the sky, which seems to have given people their first notion of the divine. Human beings could do nothing to affect it. It was inconceivable that the sky could be “persuaded” to do the will of poor, weak human beings. At some point in human history, we did to the sky what we did to animals — we personified it in order to better understand its mystery and relate it to human life.

<hr>

[Visit Link](http://www.brainpickings.org/2015/07/08/karen-armstrong-short-history-of-myth/){:target="_blank" rel="noopener"}


