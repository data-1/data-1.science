---
layout: post
title: "Design and build of synthetic DNA goes back to 'BASIC'"
date: 2016-04-17
categories:
author: Imperial College London
tags: [Synthetic biology,Biology,Automation,Standardization,Research,Gene,Organism,Branches of science,Technology]
---


The new technique should enable greater advances in research and could offer industry a way to automate the design and manufacture of synthetic DNA. BASIC is fast to use because it can draw on a large database of standardised parts, which can be produced in bulk and stored for use as required, rather than creating new parts each time. The standardisation and accuracy of the process means that it could be used on an industrial scale. BASIC is already set to be used in a high throughput automated process in SynbiCITE, the innovation and knowledge centre (IKC) based at Imperial which is promoting the adoption of synthetic biology by industry. Professor Paul Freemont, co-Director of the Centre for Synthetic Biology & Innovation, says: This system is an exciting development for the field of synthetic biology.

<hr>

[Visit Link](http://phys.org/news345216675.html){:target="_blank" rel="noopener"}


