---
layout: post
title: "Tiny super magnets could be the future of drug delivery"
date: 2017-10-07
categories:
author: "Elsevier"
tags: [Physics,Crystal,Magnetism,Superparamagnetism,Magnet,American Association for the Advancement of Science,Magnetite,RELX,Materials,Physical sciences,Condensed matter physics,Electromagnetism,Chemistry,Materials science,Applied and interdisciplinary physics]
---


If some magnetic materials, such as iron oxides, are small enough - perhaps a few millionths of a millimeter across, smaller than most viruses - they have an unusual property: their magnetization randomly flips as the temperature changes. By applying a magnetic field to these crystals, scientists can make them almost as strongly magnetic as ordinary fridge magnets. Chen and Ma explained that the high temperature and pressure under which the crystals form made tiny meteorite-like 'micro-particles' of magnetite escape from their surface. This method of making larger superparamagnetic crystals paves the way for the development of superparamagnetic bulk materials that can be reliably controlled by moderate external magnetic forces, revolutionizing drug delivery to tumors and other sites in the body that need to be targeted precisely. Now that superparamagnetism is no longer restricted to minute particles that are difficult to handle, researchers can start exploring in which ways this can contribute to improving our lives.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/e-tsm111416.php){:target="_blank" rel="noopener"}


