---
layout: post
title: "DARPA Wants to Use Genetic Modification to Turn Plants into Spy Tech"
date: 2018-06-10
categories:
author: ""
tags: [Technology,Surveillance,Sensor,Plant,Information]
---


Mother Nature Knows Best  The Defense Advanced Research Projects Agency (DARPA), the think-tank that's under the U.S. Department of Defense, recently announced that it's working on a new project that could change how pertinent information is gathered on the battlefield. The APT's goal is to boost the natural stimulus-response mechanisms in plants to detect the presence of certain chemicals, pathogens, radiation, and even electromagnetic signals. “Plants are highly attuned to their environments and naturally manifest physiological responses to basic stimuli such as light and temperature, but also in some cases to touch, chemicals, pests, and pathogens,” Blake Bextine, APT program manager, said in the press release. For one, it's quite easy to imagine some Black Mirror-esque scenario where the plant-based sensors could be modified to collect more than just the kind of information that DARPA described. Exactly what kind of experiments APT will perform on exactly which kind of plants will be determined soon.

<hr>

[Visit Link](https://futurism.com/darpa-plants-spy-tech/){:target="_blank" rel="noopener"}


