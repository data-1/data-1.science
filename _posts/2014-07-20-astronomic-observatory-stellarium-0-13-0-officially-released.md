---
layout: post
title: "Astronomic Observatory Stellarium 0.13.0 Officially Released"
date: 2014-07-20
categories:
author: Silviu Stahie, Jul
tags: [Amateur astronomy,Stellarium (software),Software release life cycle,Computing,Software,Software development,Software engineering,Technology,Astronomy,Computer science]
---


Stellarium 0.13.0, a free, open source planetarium software that displays a realistic and accurate sky in 3D, just like the one that can be seen with the naked eye, binoculars, or a telescope, has been released and is now available for download. Despite the version number, this latest Stellarium release is actually stable. The development has been progressing very slowly, but the devs have made some very big changes over the years. In fact, this is one of the most advanced applications of its kind and, to top it all off, it’s completely free. Unfortunately, Linux users have to wait for stable releases to test the new versions.

<hr>

[Visit Link](http://news.softpedia.com/news/Astronomic-Observatory-Stellarium-0-13-0-Officially-Released-451389.shtml){:target="_blank" rel="noopener"}


