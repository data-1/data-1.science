---
layout: post
title: "Intrusion detection: Linux rootkits"
date: 2015-09-30
categories:
author: Michael Boelen
tags: [Rootkit,Malware,Backdoor (computing),Information Age,Digital media,Secure communication,Crime prevention,System software,Cyberspace,Security technology,Computer engineering,Information technology management,Systems engineering,Security engineering,Cybercrime,Software development,Cyberwarfare,Information technology,Software engineering,Computer security,Computing,Software,Technology,Computer science]
---


What is a rootkit? Rootkit detection  Methods to detect rootkit presence  Since rootkits are malicious, they should be detected as soon as possible. Rootkit detection tools  File integrity tools  One method to detect alterations to a system is with the help of file integrity tools. What is the best way to detect a rootkit? Which tools can I use to detect a rootkit?

<hr>

[Visit Link](http://linux-audit.com/intrusion-detection-linux-rootkits/){:target="_blank" rel="noopener"}


