---
layout: post
title: "NASA's MMS delivers promising initial results"
date: 2015-12-22
categories:
author: NASA/Goddard Space Flight Center
tags: [Magnetic reconnection,Magnetosphere,Solar wind,Sun,Coronal mass ejection,Plasma (physics),Goddard Space Flight Center,Interplanetary medium,Magnetopause,Stellar corona,Physics,Astrophysics,Electrical engineering,Science,Space plasmas,Plasma physics,Phases of matter,Space science,Applied and interdisciplinary physics,Outer space,Physical sciences,Nature,Electromagnetism,Astronomy]
---


All of this magnetic and electric energy means that magnetic reconnection plays a huge role in shaping the environment wherever plasma exists -- whether that's on the sun, in interplanetary space, or at the boundaries of Earth's magnetic system. This suite of instruments, the FIELDS suite -- duplicated on each of the four MMS spacecraft -- contains six sensors that work together to form a three-dimensional picture of the electric and magnetic fields near the spacecraft. Goodrich presented observations from MMS that showed how the FIELDS suite can spot examples of parallel electric fields at time scales down to half a second. Cohen presented MMS observations that are clearly able to distinguish between the directions the particles are moving, which will help scientists better understand what mechanisms drive magnetic reconnection. Goddard built, integrated and tested the four MMS spacecraft and is responsible for overall mission management and mission operations.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/nsfc-nmd121815.php){:target="_blank" rel="noopener"}


