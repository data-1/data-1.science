---
layout: post
title: "Shrinking dinosaurs evolved into flying birds"
date: 2015-07-16
categories:
author: University of Adelaide 
tags: [Dinosaur,Bird,Animals,Dinosaurs,Taxa]
---


A new study led by an Adelaide scientist has revealed how massive, meat-eating, ground-dwelling dinosaurs − the theropods − evolved into agile flyers: they just kept shrinking and shrinking, for over 50 million years. These bird ancestors also evolved new adaptations (such as feathers, wishbones and wings) four times faster than other dinosaurs. Birds evolved through a unique phase of sustained miniaturisation in dinosaurs, says lead author Associate Professor Michael Lee, from the University of Adelaide's School of Earth and Environmental Sciences and the South Australian Museum. The study examined over 1500 anatomical traits of dinosaurs to reconstruct their family tree. Birds out-shrank and out-evolved their dinosaurian ancestors, surviving where their larger, less evolvable relatives could not, says Associate Professor Lee.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/uoa-sde072814.php){:target="_blank" rel="noopener"}


