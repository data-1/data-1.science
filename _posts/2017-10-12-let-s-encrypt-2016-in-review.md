---
layout: post
title: "Let’s Encrypt 2016 In Review"
date: 2017-10-12
categories:
author: ""
tags: [Lets Encrypt,HTTPS,World Wide Web,Internet protocols,Information technology management,Computer security,Internet architecture,Telecommunications,Internet Standards,Computer science,Computing,Information technology,Information Age,Software,Application layer protocols,Computer networking,Cyberspace,Technology,Internet,Software development,Software engineering]
---


I’m incredibly proud of what our team and community accomplished during 2016. The ACME protocol for issuing and managing certificates is at the heart of how Let’s Encrypt works. We also learned a lot about our client ecosystem. As exciting as 2016 was for Let’s Encrypt and encryption on the Web, 2017 seems set to be an even more incredible year. We’d like to thank our community, including our sponsors, for making everything we did this past year possible.

<hr>

[Visit Link](https://letsencrypt.org//2017/01/06/le-2016-in-review.html){:target="_blank" rel="noopener"}


