---
layout: post
title: "Target-Driven Evolution of Scorpion Toxins"
date: 2015-10-08
categories:
author: Zhang, Group Of Peptide Biology, Evolution, State Key Laboratory Of Integrated Management Of Pest Insects, Rodents, Institute Of Zoology, Chinese Academy Of Sciences, Beijing, Gao, Zhu
tags: [Evolution,Ion channel,Conserved sequence,Toxin,Natural selection,Predation,Sodium channel,Scorpion,Protein,Computational phylogenetics,Venom,Insect,Species,Mammal,Gene,Bird,Lizard,Docking (molecular),Protein domain,Adaptation,Nervous system,Mutation,Genetic code,Proteinprotein interaction,Biochemistry,Biology,Molecular biology,Biotechnology]
---


(A) Ribbon models showing interactions between PSSs of toxins and residues derived from the VSD of rNa v 1.2, previously identified as a primary component of the receptor site for α-toxins (spheres in blue, PSSs and red, channel residues). Full size table  Figure 4 Divergence rates (ω) for each site of VSD from both predators and prey of scorpions. (A) Na v channel VSDs from birds, lizards, mammals and insects; (B) Scorpion α-toxins. The complex model is the same with that of Fig. Full size image  Channel’s Variability Driving Adaptive Evolution of Scorpion Toxins  In the toxin-channel complex, we have established several pairs of interactions, in which three channel sites located in the two loops are involved in interaction with the PSSs of toxins, including 1560, 1611 and 1613 (Fig.

<hr>

[Visit Link](http://www.nature.com/articles/srep14973){:target="_blank" rel="noopener"}


