---
layout: post
title: "CRISPR-edited mushroom cleared"
date: 2016-04-22
categories:
author:  
tags: []
---


A species of genetically engineered mushroom has been freed of regulation by the U.S. Department of Agriculture (USDA). The browning of the mushroom is caused by the enzyme polyphenol oxidase (PPO) and the above effect may be got by removing the genes that code for this enzyme. The report will also comment on regulatory processes. With regard to genetically modified crops, the U.S. has been quite liberal and it is likely to become more so. As the technology advances, more complex procedures will be initiated and so the whole process of regulating these products will need to be reviewed as and when that happens.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/crispredited-mushroom-cleared/article8483468.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


