---
layout: post
title: "Targeting DNA"
date: 2015-12-21
categories:
author: Massachusetts Institute of Technology
tags: [Protein splicing,Zinc finger,Virus latency,Protein,DNA-binding protein,Virus,Medical specialties,Cell biology,Genetics,Molecular biology,Life sciences,Biotechnology,Biochemistry,Biology]
---


CAMBRIDGE, MA -- MIT biological engineers have developed a modular system of proteins that can detect a particular DNA sequence in a cell and then trigger a specific response, such as cell death. This system can be customized to detect any DNA sequence in a mammalian cell and then trigger a desired response, including killing cancer cells or cells infected with a virus, the researchers say. This allows you to readily design constructs that enable a programmed cell to both detect DNA and act on that detection, with a report system and/or a respond system. The MIT researchers also deployed this system to kill cells by linking detection of the DNA target to production of an enzyme called NTR. Future versions of the system could be designed to bind to DNA sequences found in cancerous genes and then produce transcription factors that would activate the cells' own programmed cell death pathways.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/miot-td092115.php){:target="_blank" rel="noopener"}


