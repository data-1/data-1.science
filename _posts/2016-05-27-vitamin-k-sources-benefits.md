---
layout: post
title: "Vitamin K: Sources & Benefits"
date: 2016-05-27
categories:
author: Alina Bradford
tags: [Vitamin K,Vitamin,Dietary supplement,Dose (biochemistry),Disease,Bleeding,Leaf vegetable,Gastrointestinal disease,Clinical medicine,Health care,Nutrition,Health,Medical specialties,Health sciences]
---


Kale contains more than 500 percent the daily recommended allowance for vitamin K, which is important for healthy bones and for healing wounds. Benefits  Vitamin K is an important factor in bone health and wound healing. However, the suggested RDA is typically 70 to 90 micrograms (mcg) for adults, according to the NLM. The National Health Service of the UK (NHS) states that, every day, adults need approximately 0.001 milligrams (mg) — or 1 mcg — of vitamin K for every kilogram (2.20 lbs.) Both Ross and Arthur said that there is no clear toxic dose for vitamin K, and there are no harmful effects in taking excessive vitamin K through your diet.

<hr>

[Visit Link](http://www.livescience.com/51908-vitamin-k.html){:target="_blank" rel="noopener"}


