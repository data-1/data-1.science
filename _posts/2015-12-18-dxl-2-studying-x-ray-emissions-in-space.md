---
layout: post
title: "DXL-2: Studying X-ray emissions in space"
date: 2015-12-18
categories:
author: NASA/Goddard Space Flight Center
tags: [Emission spectrum,X-ray,NASA,Light,Aerobee,Flight,Physics,Physical phenomena,Physical sciences,Science,Spaceflight,Outer space,Astronomy,Space science]
---


There isn't much visible light in space - but there are numerous other wavelengths of light and scientists want to know what's out there and where it comes from. On Dec. 4, 2015, NASA will launch the DXL-2 payload at 11:45 p.m. EST, from the White Sands Missile Range in New Mexico to continue the study of these x-rays. The purpose of the flight is to better understand the nature and characteristics of the local hot bubble and solar wind charge exchange, with the double goal of understanding their fundamental physics and improving our modeling capability to use in the interpretation of past, present and future X-ray missions, said Massimiliano Galeazzi, the DXL-2 principal investigator from the University of Miami. The 1,497 pound DXL-2 payload will fly on a NASA Black Brant IX suborbital sounding rocket to an altitude 139 miles. DXL-2 is supported through NASA's Sounding Rocket Program at the Goddard Space Flight Center's Wallops Flight Facility in Virginia.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/nsfc-dsx120315.php){:target="_blank" rel="noopener"}


