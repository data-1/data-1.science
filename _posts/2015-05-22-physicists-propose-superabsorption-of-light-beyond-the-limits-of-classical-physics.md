---
layout: post
title: "Physicists propose superabsorption of light beyond the limits of classical physics"
date: 2015-05-22
categories:
author: Lisa Zyga
tags: [Light,Physics,Atom,Quantum mechanics,Emission spectrum,Superradiance,Photon,Exciton,Optics,Technology,Applied and interdisciplinary physics,Atomic molecular and optical physics,Electromagnetism,Electromagnetic radiation,Physical sciences,Science,Electrodynamics,Chemistry,Physical chemistry]
---


In one potential method to realize superabsorption, a superabsorbing ring absorbs incident photons, giving rise to excitons. Credit: Higgins, et al.  (Phys.org) —In a well-known quantum effect called superradiance, atoms can emit light at an enhanced rate compared to what is possible in classical situations. In natural systems, light emission dominates over light absorption, which is why superabsorption has not yet been observed. Eventually, harvesting sunlight in a highly efficient way might one day be possible using superabsorbing systems based on our design, but a more immediate application would be building an extremely sensitive light sensor that could form the basis of new camera technology, said coauthor Simon Benjamin, Professor at Oxford University. We are working on an alternative scheme for quantum enhanced light absorption, which uses what's called a 'dark state' as an efficient means of extracting energy from light, and is similar to what happens in photosynthesis, which is in contrast to superabsorption, which is very different to how natural light harvesters work, Higgins said.

<hr>

[Visit Link](http://phys.org/news328428104.html){:target="_blank" rel="noopener"}


