---
layout: post
title: "ATV-5 liftoff: full replay"
date: 2015-09-03
categories:
author: "$author"   
tags: [Automated Transfer Vehicle,Guiana Space Centre,European Space Agency,Space launch vehicles,Astronautics,Rocketry,Spacecraft,European space programmes,Spaceflight technology,Space agencies,Pan-European scientific organizations,Space policy of the European Union,Outer space,Space traffic management,Space access,Space industry,Vehicles,Space policy,Rockets and missiles,Space organizations,Flight,Space programs,Space vehicles,Spaceflight,Human spaceflight,Transport,Aerospace,Space-based economy,Transport authorities,Space exploration]
---


Liftoff of an Ariane 5 launcher from Europe’s spaceport in French Guiana with ESA’s last Automated Transfer vehicle to the Space Station. The fifth and final mission of ESA’s Automated Transfer Vehicle got off to a flying start with its launch from Europe’s Spaceport in Kourou, French Guiana, heading for the International Space Station. Georges Lemaître is the fifth ATV built and launched by ESA as part of Europe’s contribution to cover the operational costs for using the Space Station. Named after the Belgian scientist who formulated the Big Bang Theory, ATV Georges Lemaître lifted off at 23:47 GMT on 29 July (01:47 CEST 30 July, 20:47 local time 29 July) on an Ariane 5 ES rocket. The vehicle will deliver 6561 kg of freight, including 2628 kg of dry cargo and 3933 kg of water, propellants and gases.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2014/07/ATV-5_liftoff_full_replay){:target="_blank" rel="noopener"}


