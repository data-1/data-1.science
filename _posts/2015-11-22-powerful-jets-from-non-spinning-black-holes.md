---
layout: post
title: "Powerful jets from non-spinning black holes"
date: 2015-11-22
categories:
author: Harvard-Smithsonian Center For Astrophysics
tags: [Astrophysical jet,Black hole,Accretion disk,Astrophysical X-ray source,Physics,Physical quantities,Science,Astronomical objects,Physical phenomena,Space science,Astrophysics,Astronomy,Physical sciences]
---


A multi-wavelength image of the ultra-luminous X-ray source X2 in the galaxy M82. A new paper successfully proposes an alternate explanation to magnetically dominated black hole rotation, namely, that the X-ray jets are radiatively driven. of Toulouse/M.Bachetti et al, Optical: NOAO/AURA/NSF  A black hole is so simple (at least in traditional theories) that it can be completely described by just three parameters: its mass, its spin, and its electric charge. Instead of rotation-powered, magnetically dominated jets, they find that non-spinning black holes can also drive powerful jets by means of the intense radiation emitted by hot gas. In the new alternative scenario, in which radiation drives the jets, the source naturally emits particles in narrow beams.

<hr>

[Visit Link](http://phys.org/news/2015-11-powerful-jets-non-spinning-black-holes.html){:target="_blank" rel="noopener"}


