---
layout: post
title: "Researchers develop silicon chip-based quantum photonic devices"
date: 2018-07-02
categories:
author: "Ulsan National Institute Of Science"
tags: [Photonics,Quantum dot,Quantum mechanics,Silicon photonics,Single-photon source,Quantum information,Photonic integrated circuit,Light,Electromagnetic radiation,Theoretical physics,Electrical engineering,Science,Materials science,Physical chemistry,Applied and interdisciplinary physics,Physics,Technology,Chemistry,Electromagnetism,Optics,Atomic molecular and optical physics,Physical sciences]
---


They used a hybrid approach combining silicon photonic waveguides with InAs/InP quantum dots that act as efficient sources of single photons at telecom wavelengths spanning the O-band and C-band. There are several potentially fruitful approaches to quantum information processing, including atom, light, and superconducting devices. Professor Kim focuses on the quantum information processing using light. Here, they used a hybrid approach that combines silicon photonic waveguides with InAs/InP quantum dots that act as efficient sources of single photons at telecom wavelengths spanning the O-band and C-band. The quantum optical device, developed by the research team has successfully transferred the emission from the quantum dots along the silicon photonic circuits with high efficiency.

<hr>

[Visit Link](https://phys.org/news/2017-12-silicon-chip-based-quantum-photonic-devices.html){:target="_blank" rel="noopener"}


