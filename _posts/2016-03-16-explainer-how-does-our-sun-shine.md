---
layout: post
title: "Explainer: How does our sun shine?"
date: 2016-03-16
categories:
author: Brad Carter, The Conversation
tags: [Sun,Neutrino,Borexino,Solar neutrino,Star,Protonproton chain,Atom,Nuclear fusion,Neutrino detector,Hydrogen,Nuclear physics,Nature,Astronomy,Chemistry,Physics,Physical sciences]
---


The resulting conversion of matter to energy could keeping the sun shining for many billions of years. Following Eddington's insight it took years for a theory to be developed as to how the collision of hydrogen atoms inside the sun and other stars makes hydrogen atoms and release energy. In addition, stars generate energy by the fusion of hydrogen atoms into helium into two ways. A new discovery  A team from the Borexino neutrino observatory in Italy have announced, in a research paper published in Nature today, the detection of low-energy neutrinos produced in the nuclear reaction that initiates solar energy generation. Borexino Collaboration  The physics of what makes our sun and stars shine informs our understanding of the origins of our solar system, our planet and ourselves.

<hr>

[Visit Link](http://phys.org/news328435136.html){:target="_blank" rel="noopener"}


