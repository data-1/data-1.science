---
layout: post
title: "Global change: Trees continue to grow at a faster rate"
date: 2015-07-20
categories:
author: Technical University of Munich (TUM) 
tags: [Forest,Carbon dioxide,Tree,Forestry,Natural environment,Environmental science,Earth phenomena,Systems ecology,Ecology,Earth sciences,Physical geography,Nature]
---


Trees have been growing significantly faster since the 1960s. The typical development phases of trees and stands have barely changed, but they have accelerated -- by as much as 70 percent. It was based on data from experimental forest plots that have been observed systematically since 1870. Accelerated Growth  In the cases of spruce and beech, respectively the dominant species of coniferous and deciduous trees in Central Europe, the TUM scientists noted significantly accelerated tree growth. This could benefit the forestry industry in that target diameters and the optimal harvest rotation age will be reached sooner.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/tum-gct091714.php){:target="_blank" rel="noopener"}


