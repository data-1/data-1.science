---
layout: post
title: "Image: Magnetospheric Multiscale Observatories processed for launch"
date: 2016-04-13
categories:
author:  
tags: [Magnetosphere,NASA,Goddard Space Flight Center,Magnetic reconnection,Outer space,Space science,Spaceflight,Astronomy,Astronautics,Space program of the United States,Flight,Spacecraft,Science]
---


Credit: NASA/Ben Smegelsky  NASA's Magnetospheric Multiscale (MMS) observatories are processed for launch in a clean room at the Astrotech Space Operations facility in Titusville, Florida. MMS is an unprecedented NASA mission to study the mystery of how magnetic fields around Earth connect and disconnect, explosively releasing energy via a process known as magnetic reconnection. MMS consists of four identical spacecraft that work together to provide the first three-dimensional view of this fundamental process, which occurs throughout the universe. The mission observes reconnection directly in Earth's protective magnetic space environment, the magnetosphere. By studying reconnection in this local, natural laboratory, MMS helps us understand reconnection elsewhere as well, such as in the atmosphere of the sun and other stars, in the vicinity of black holes and neutron stars, and at the boundary between our solar system's heliosphere and interstellar space.

<hr>

[Visit Link](http://phys.org/news343635029.html){:target="_blank" rel="noopener"}


