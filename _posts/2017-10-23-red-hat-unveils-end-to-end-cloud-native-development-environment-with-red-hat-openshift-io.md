---
layout: post
title: "Red Hat Unveils End-to-End Cloud-Native Development Environment with Red Hat OpenShift.io"
date: 2017-10-23
categories:
author:  
tags: [OpenShift,Red Hat,Cloud computing,Linux,Application software,Information technology management,Computing,Technology,Software engineering,Information technology,Software,Computer science,Software development,Computer engineering,Information Age,Systems engineering,Digital media]
---


BOSTON – RED HAT SUMMIT 2017 – MAY 2, 2017 - May 2, 2017 —  Red Hat, Inc. (NYSE: RHT), the world's leading provider of open source solutions, today announced Red Hat OpenShift.io, a free, online development environment optimized for creating cloud-native, container-based applications. OpenShift.io also includes a free subscription to the Red Hat Developer Program, which offers a variety of Red Hat’s products for development use, including the no-cost Red Hat Enterprise Linux developer subscription, Red Hat JBoss Enterprise Middleware and other Red Hat technologies. Red Hat OpenShift: The industry's most comprehensive enterprise Kubernetes platform for cloud-native application development  With the addition of Red Hat OpenShift.io as well as the newly-announced Red Hat OpenShift Application Runtimes, Red Hat delivers the most robust, open, integrated and supported cloud-native application development environment for production workloads. Demonstrating the reliability and scalability of Red Hat’s container application platform, applications built with Red Hat OpenShift.io are deployed using OpenShift Online, a managed, multi-tenant offering of Red Hat OpenShift. Launching at Red Hat Summit 2017, the next-generation of OpenShift Online helps developers focus on building applications instead of assembling and managing container files.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/red-hat-unveils-end-end-cloud-native-development-environment-red-hat-openshiftio){:target="_blank" rel="noopener"}


