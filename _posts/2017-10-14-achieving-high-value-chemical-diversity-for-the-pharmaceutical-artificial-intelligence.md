---
layout: post
title: "Achieving high-value chemical diversity for the pharmaceutical artificial intelligence"
date: 2017-10-14
categories:
author: "InSilico Medicine"
tags: [Insilico Medicine,Artificial intelligence,Technology,Branches of science,Life sciences,Cognitive science]
---


One of the most significant tasks at Insilico Medicine is adapting best neural network architectures for drug discovery process and it is committed to publishing the proof of concept advances that are at least one year old. These advances are usually integrated into a comprehensive drug discovery pipeline with the goal to enable the deep neural networks to produce perfect molecules for the specific set of diseases. Insilico Medicine has a policy of publishing the proof of concept research, which is one year or older to attract more data scientists to work on the healthcare problems. The use of GANs with RL is likely to transform the pharmaceutical industry, said Alex Zhavoronkov, Ph.D., founder and CEO of Insilico Medicine, Inc.  ###  Source:  druGAN: An Advanced Generative Adversarial Autoencoder Model for de Novo Generation of New Molecules with Desired Molecular Properties in Silico http://pubs.acs.org/doi/abs/10.1021/acs.molpharmaceut.7b00346  About Insilico Medicine, Inc  Insilico Medicine, Inc. is an artificial intelligence company located at the Emerging Technology Centers at the Johns Hopkins University Eastern campus in Baltimore, with R&D resources in Belgium, Russia, and the UK sourced through hackathons and competitions. The company is pursuing internal drug discovery programs in cancer, Parkinson's Disease, Alzheimer's Disease, ALS, diabetes, sarcopenia, and aging.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/imi-ahc101217.php){:target="_blank" rel="noopener"}


