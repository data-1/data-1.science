---
layout: post
title: "8 excellent open source data visualization tools"
date: 2015-06-25
categories:
author: "Nitish Tiwari"
tags: [Leaflet (software),Graphical user interface,Website,Data and information visualization,Information technology,Computing,Software,Technology,Software development,Software engineering,Digital media,Information technology management,Computer science,World Wide Web,Information Age]
---


There are several open source tools that can help you create useful, informative graphs. Datawrapper is fully open source, and you can download it from their GitHub page and host it yourself. Before you can create a chart, you'll need to include the library in your frontend code. You can just paste a link of a Google spreadsheet or a .csv file (input data) and tool create a chart with the data. If you are interested in more data visualization tools, take a look at this list of more than 50 tools.

<hr>

[Visit Link](http://opensource.com/life/15/6/eight-open-source-data-visualization-tools){:target="_blank" rel="noopener"}


