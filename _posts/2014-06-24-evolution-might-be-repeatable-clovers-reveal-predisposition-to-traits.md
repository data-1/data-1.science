---
layout: post
title: "Evolution Might be Repeatable: Clovers Reveal Predisposition to Traits"
date: 2014-06-24
categories:
author: Science World Report
tags: [Evolution,Species,Convergent evolution,Biological evolution,Evolutionary biology,Biology,Genetics,Nature]
---


Can evolution be repeated? That's a good question, and it's one that scientists wanted to find out. Although this particular scenario was impossible to repeat, the researchers decided to look at related species that have independently evolved the same traits and ask if the same genes are responsible and, if so, whether the same mutations led to the trait. So what did they find? Instead, the clover species seemed to be predisposed to develop the trait.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15629/20140624/evolution-repeatable-clovers-reveal-predisposition-traits.htm){:target="_blank" rel="noopener"}


