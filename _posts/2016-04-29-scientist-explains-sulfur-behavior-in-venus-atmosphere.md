---
layout: post
title: "Scientist explains sulfur behavior in Venus atmosphere"
date: 2016-04-29
categories:
author: Moscow Institute of Physics and Technology
tags: [Venus,Ultraviolet,Atmosphere of Earth,Cloud,Atmosphere,Aerosol,Astronomy,Space science,Science,Nature,Chemistry,Physical sciences]
---


These areas mean that somewhere in the upper cloud layer there is a substance that is absorbing UV radiation. In the new paper, Krasnopolsky presents the first photochemical model of the formation of sulfur particles in Venus's clouds. However, observations in the near UV radiation range obtained from the Soviet interplanetary station Venera 14 indicate that absorption in this range occurs in the upper cloud layer at the altitude of approximately 60 km. This means that sulfur aerosol cannot be the cause of absorption of Venus's atmosphere in the near UV range, concludes Krasnopolsky. We can therefore consider this mixture of sulfuric acid and ferric chloride to be the most likely substance causing this very mysterious UV absorption, says Krasnopolsky.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/miop-dso042616.php){:target="_blank" rel="noopener"}


