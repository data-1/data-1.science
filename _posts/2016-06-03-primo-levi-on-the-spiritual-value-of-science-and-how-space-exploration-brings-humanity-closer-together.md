---
layout: post
title: "Primo Levi on the Spiritual Value of Science and How Space Exploration Brings Humanity Closer Together"
date: 2016-06-03
categories:
author: "Maria Popova"
tags: [Carl Sagan,Apollo 8,Space exploration,LIGO,Science]
---


“It’s part of the nature of man to start with romance and build to a reality,” Ray Bradbury observed in his forgotten conversation with Carl Sagan and Arthur C. Clarke about the future of space exploration. Three years earlier, on December 21, 1968, a romance of the most imaginative caliber became reality when three astronauts launched into the cosmos aboard the Apollo 8 spacecraft and returned six days later with the iconic Earthrise photograph — Earth’s first look at itself, taken on Christmas Eve from aboard the spacecraft. The Apollo 8 mission was the costliest investment in space exploration thus far, but who could put a price on its largely unanticipated cascading consequences for science, society, and the human spirit? On the eve of this momentous occasion for humanity, the great Italian Jewish scientist, writer, and Holocaust survivor Primo Levi (July 31, 1919–April 11, 1987) considered the spiritual value of such scientific ambitions in a beautiful essay titled “The Moon and Man,” included in his altogether indispensable The Mirror Maker: Stories and Essays (public library). As if to remind us that at any moment when the improbable becomes possible, human nature is such that we immediately take for granted what we had only just moments ago taken for impossible, Levi considers the sheer miraculousness — a miracle enkindled by our scientific doggedness and ingenuity — of a human being leaving the planet and voyaging into the cosmic unknown:  Man, the naked ape, the terrestrial animal who is the son of a very long dynasty of terrestrial or marine beings, molded in all of his organs by a restricted environment which is the lower atmosphere, can detach himself from it without dying.

<hr>

[Visit Link](https://www.brainpickings.org/2016/06/02/primo-levi-mirror-maker-science-space/){:target="_blank" rel="noopener"}


