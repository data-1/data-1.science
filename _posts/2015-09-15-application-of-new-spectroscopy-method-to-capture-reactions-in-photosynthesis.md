---
layout: post
title: "Application of new spectroscopy method to capture reactions in photosynthesis"
date: 2015-09-15
categories:
author: Rensselaer Polytechnic Institute 
tags: [Photosystem II,Photosynthesis,Water splitting,Photosystem,Oxygen-evolving complex,Chemical reaction,Nature,Physical sciences,Physical chemistry,Applied and interdisciplinary physics,Biochemistry,Chemical substances,Chemistry]
---


A new spectroscopy method is bringing researchers at Rensselaer Polytechnic Institute (RPI) closer to understanding -- and artificially replicating -- the solar water-splitting reaction at the heart of photosynthetic energy production. Lakshmi, developed the method as part of an ongoing investigation into the photosynthetic protein, Photosystem II. The solar-powered water-splitting photosynthetic protein complex, Photosystem II, catalyzes one of the most energetically demanding reactions in nature by using light energy to split water to dioxygen, said Lakshmi, associate professor of chemistry and chemical biology, and scientific director at the Baruch '60 Center for Biochemical Solar Energy Research at Rensselaer. The oxygen-evolving complex uses four photons of light to split two molecules of water in five distinct steps known as S-states. And the insight that we have gained on the mechanism of manganese catalase and the high-resolution spectroscopic methods that we have developed in this study greatly enhance the ultimate goal and bring us closer to determining the mechanism of the solar-powered water-splitting reaction of Photosystem II.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150914152720.htm){:target="_blank" rel="noopener"}


