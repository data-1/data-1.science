---
layout: post
title: "What Is the Greenhouse Effect?"
date: 2016-04-13
categories:
author: Marc Lallanila
tags: [Greenhouse effect,Greenhouse gas,Climate change,Atmosphere of Earth,Atmosphere,Radiation,Earth,Infrared,Thermal radiation,Nature,Earth sciences,Applied and interdisciplinary physics,Physical sciences,Physical geography,Climate variability and change,Chemistry,Natural environment,Climate]
---


However, 97 percent of climate scientists agree that humans have changed Earth's atmosphere in dramatic ways over the past two centuries, resulting in global warming. The greenhouse effect  The exchange of incoming and outgoing radiation that warms the Earth is often referred to as the greenhouse effect because a greenhouse works in much the same way. Gases in the atmosphere can reflect or trap heat energy, much like what happens in a greenhouse for plants. There are those that say that gases are not the cause of global warming, though that goes against the opinion of the global scientific community. [Carbon Dioxide Is Warming the Planet (Here's How)]  Can the greenhouse effect be reversed?

<hr>

[Visit Link](http://www.livescience.com/37743-greenhouse-effect.html){:target="_blank" rel="noopener"}


