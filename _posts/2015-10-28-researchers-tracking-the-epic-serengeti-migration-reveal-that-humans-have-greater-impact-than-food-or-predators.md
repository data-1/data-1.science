---
layout: post
title: "Researchers tracking the epic Serengeti migration reveal that humans have greater impact than food or predators"
date: 2015-10-28
categories:
author: University Of Glasgow
tags: [Wildebeest,Serengeti,Animals]
---


The group's findings suggest that although wildebeest and zebra migrate together, they move for very different reasons: wildebeest are constantly looking for fresh grazing, whereas zebra balance their need to get access to good food against the relative risk of being killed by a predator. The impact of humans trumps everything else, said Dr Hopcraft. All 1.3 million wildebeest and 250,000 zebra would have to cross that road in order to access the Mara River which is the only source of water during the dry season, he said. Another threat to wildebeest and zebra is poaching. When the grazing is at its peak, the entire herd of 1.3 million wildebeest, plus 250,000 zebra, and about 300,000 gazelle all converge on the same area of the Serengeti.

<hr>

[Visit Link](http://phys.org/news326620529.html){:target="_blank" rel="noopener"}


