---
layout: post
title: "Geoengineering polar glaciers to slow sea-level rise: A researcher suggests a radical solution to prevent catastrophic glacial melting."
date: 2018-04-19
categories:
author: "Princeton  University"
tags: [Sea level rise,Glacier,Climate change,Ice sheet,Climate engineering,Ice stream,Climate change in the Arctic,Ice,Climate variability and change,Natural environment,Nature,Environmental impact,Environmental issues with fossil fuels,Earth phenomena,Global environmental issues,Physical geography,Earth sciences,Societal collapse,Environmental science,Human impact on the environment,Climate,Applied and interdisciplinary physics,Hydrology,Environmental issues]
---


Targeted geoengineering to preserve continental ice sheets deserves serious research and investment, argues an international team of researchers in a Comment published March 14 in the journal Nature. The ice sheets of Greenland and Antarctica will contribute more to sea-level rise this century than any other source, so stalling the fastest flows of ice into the oceans would buy us a few centuries to deal with climate change and protect coasts, say the authors. There is going to be some sea-level rise in the 21st century, but most models say that the ice sheets won't begin collapsing in earnest until the 22nd or 23rd centuries, said Wolovick. Wolovick started investigating geoengineering approaches when he realized how disproportionate the scale was between the origin of the problem at the poles and its global impact: For example, many of the most important outlet glaciers in Greenland are about 5 kilometers (3 miles) wide, and there are bridges that are longer than [that]. Glacial geoengineering will not be able to save the ice sheets in the long run if the climate continues to warm, Wolovick said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/03/180319144550.htm){:target="_blank" rel="noopener"}


