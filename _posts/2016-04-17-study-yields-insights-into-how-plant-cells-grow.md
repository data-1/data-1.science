---
layout: post
title: "Study yields insights into how plant cells grow"
date: 2016-04-17
categories:
author: Natalie Van Hoose, Purdue University
tags: [Cell (biology),Actin,Cytoskeleton,Protein,Trichome,Microtubule,Plant,Cell biology]
---


How this protein complex influences the actin network of the cell was a major discovery, Szymanski said. If we can learn more about the interactions between the actin and microtubule systems, we could translate this knowledge into improved cotton fibers and trichome plant defense systems in crop species. The model produced a number of predictions about cell wall properties that Szymanski then verified in live trichomes. Actin allows the cell wall to maintain a thickness gradient - thicker at the base and thinner at the tip - as it grows. Understanding cell shape could also be used to model organ growth and development, he said.

<hr>

[Visit Link](http://phys.org/news345224103.html){:target="_blank" rel="noopener"}


