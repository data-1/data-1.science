---
layout: post
title: "PHP 5.5.14 Officially Released"
date: 2014-06-28
categories:
author: Silviu Stahie, Jun
tags: [PHP,Systems engineering,Software engineering,Computing,Computer programming,Software development,Software,Computers,Technology,Programming interfaces,Computer science,Information technology management,Computer engineering,Programming paradigms,Object-oriented programming,Computing platforms,Computer architecture,Technology development]
---


PHP, an HTML-embedded scripting language with syntax borrowed from C, Java, and Perl, with a couple of unique PHP-specific features thrown in, has been updated to version 5.5.14. The PHP 5.x branch includes a new OOP model based on the Zend Engine, a new extension for improved MySQL support, built-in native support for SQLite, and much more. “Please, note that this release also fixes a backward compatibility issue that has been detected in the PHP 5.5.13 release. Still, the fix in PHP 5.5.14 may break some very rare situations. As this tiny compatibility break involves security, and as security is our primary concern, we had to fix it.

<hr>

[Visit Link](http://news.softpedia.com/news/PHP-5-5-14-Officially-Released-448701.shtml){:target="_blank" rel="noopener"}


