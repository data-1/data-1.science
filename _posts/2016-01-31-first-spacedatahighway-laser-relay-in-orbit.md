---
layout: post
title: "First SpaceDataHighway laser relay in orbit"
date: 2016-01-31
categories:
author: "$author" 
tags: [European Data Relay System,Technology,Space vehicles,Astronautics,Bodies of the Solar System,Outer space,Spaceflight,Satellites,Spacecraft,Flight,Telecommunications,Communications satellites,Space science,Spaceflight technology]
---


Applications First SpaceDataHighway laser relay in orbit 30/01/2016 10957 views 107 likes  The European Data Relay System’s first laser terminal has reached space aboard its host satellite and is now under way to its final operating position. EDRS-A was launched on 29 January as part of the Eutelsat-9B telecom satellite at 22:20 GMT (23:20 CET, 04:20 30 January local time) atop a Proton rocket from Baikonur, Kazakhstan. ESA, Airbus and DLR will in a few days begin testing EDRS-A’s general health and performance, working with the EDRS ground stations in Germany, Belgium and the UK. Completing the system The second EDRS node, the dedicated EDRS-C satellite, will be launched next year to join EDRS-A over Europe. A third is planned in 2020 over the Asia-Pacific region, doubling the system’s coverage.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Telecommunications_Integrated_Applications/EDRS/First_SpaceDataHighway_laser_relay_in_orbit){:target="_blank" rel="noopener"}


