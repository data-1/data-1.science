---
layout: post
title: "The story of neutrinos – Physics World"
date: 2014-06-24
categories:
author: ""        
tags: [Neutrino,Physics,Wolfgang Pauli,Universe,Nature,Theoretical physics,Particle physics,Physical sciences,Science]
---


Shy but significant, neutrinos may hold the key to some of the biggest questions in modern physics. In this podcast, astronomer Ray Jayawardhana tells the story of how these tiny, nearly massless particles were discovered and explains what their still-mysterious properties could tell us about the cosmos  The story of neutrinos began in 1930, when Wolfgang Pauli suggested that an unknown neutral particle could account for some puzzling behaviour in radioactive decay. At the time, the idea was speculative at best, and Pauli knew it, joking to a friend he said “I have done a terrible thing. I have postulated a particle that cannot be detected.” A pair of experimentalists, Clyde Cowan and Frederick Reines, would eventually prove Pauli wrong, but subsequent efforts to study this new particle – which Enrico Fermi dubbed the neutrino, or “little neutral one” – seemed to raise more questions than they answered. These stories – and others from the neutrino’s rich history – are the subject of Ray Jayawardhana’s book The Neutrino Hunters.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/_OE4_FS0JYg/the-story-of-neutrinos){:target="_blank" rel="noopener"}


