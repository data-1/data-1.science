---
layout: post
title: "The iconic text editor Vim celebrates 25 years"
date: 2017-09-24
categories:
author: "Ruth Holloway
(Alumni)"
tags: [Vim (text editor),Software engineering,Computing platforms,Free software,Computer architecture,Intellectual works,Computer science,Computers,Technology,Operating system families,System software,Software,Computing,Software development]
---


Turn back the dial of time a bit. No, keep turning... a little more... there! Over 25 years ago, when some of your professional colleagues were still toddlers, Bram Moolenaar started working on a text editor for his Amiga. On November 2, 1991, after three years in development, he released the first version of the Vi IMitation editor, or Vim. For many years now, sales of T-shirts and other Vim logo gear on the website has supported ICCF, a Dutch charity that supports children in Uganda.

<hr>

[Visit Link](https://opensource.com/life/16/11/happy-birthday-vim-25){:target="_blank" rel="noopener"}


