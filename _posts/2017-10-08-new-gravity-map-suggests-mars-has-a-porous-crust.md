---
layout: post
title: "New gravity map suggests Mars has a porous crust"
date: 2017-10-08
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Mars,Crust (geology),InSight,Geophysics,Moon,GRAIL,Goddard Space Flight Center,Space science,Astronomy,Outer space,Science,Planets of the Solar System,Astronomical objects known since antiquity,Bodies of the Solar System,Physical sciences,Solar System,Planetary science,Earth sciences,Spaceflight,Terrestrial planets]
---


The crust is the end-result of everything that happened during a planet's history, so a lower density could have important implications about Mars' formation and evolution, said Sander Goossens of NASA's Goddard Space Flight Center in Greenbelt, Maryland. The researchers mapped the density of the Martian crust, estimating the average density is 2,582 kilograms per meter cubed (about 161 pounds per cubic foot). The gravity field for Earth is extremely detailed, because the data sets have very high resolution. Recent studies of the Moon by NASA's Gravity Recovery and Interior Laboratory, or GRAIL, mission also yielded a precise gravity map. With this approach, we were able to squeeze out more information about the gravity field from the existing data sets, said Goddard geophysicist Terence Sabaka, the second author on the paper.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/09/170913193005.htm){:target="_blank" rel="noopener"}


