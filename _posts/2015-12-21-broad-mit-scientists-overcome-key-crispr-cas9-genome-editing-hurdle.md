---
layout: post
title: "Broad, MIT scientists overcome key CRISPR-Cas9 genome editing hurdle"
date: 2015-12-21
categories:
author: Broad Institute of MIT and Harvard
tags: [Cas9,Genome editing,Broad Institute,CRISPR gene editing,Biology,Biotechnology,Genetics,Life sciences,Molecular biology,Biochemistry,Branches of genetics,Molecular genetics,Biological engineering]
---


Researchers at the Broad Institute of MIT and Harvard and the McGovern Institute for Brain Research at MIT have engineered changes to the revolutionary CRISPR-Cas9 genome editing system that significantly cut down on off-target editing errors. In a paper published today in Science, Feng Zhang and his colleagues report that changing three of the approximately 1,400 amino acids that make up the Cas9 enzyme from S. pyogenes dramatically reduced off-target editing to undetectable levels in the specific cases examined. Zhang and his colleagues used knowledge about the structure of the Cas9 protein to decrease off-target cutting. We hope the development of eSpCas9 will help address some of those concerns, but we certainly don't see this as a magic bullet. Courtesy: Ian Slaymaker, Broad Institute of MIT and Harvard  Paper cited: Slaymaker et al., Rationally Engineered Cas9 Nucleases with Improved Specificity, Science (2015)  About the Broad Institute of MIT and Harvard  The Eli and Edythe L. Broad Institute of MIT and Harvard was launched in 2004 to empower this generation of creative scientists to transform medicine.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/biom-bms113015.php){:target="_blank" rel="noopener"}


