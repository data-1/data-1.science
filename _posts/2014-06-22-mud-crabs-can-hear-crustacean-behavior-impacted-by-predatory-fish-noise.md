---
layout: post
title: "Mud Crabs Can Hear: Crustacean Behavior Impacted by Predatory Fish Noise"
date: 2014-06-22
categories:
author: Science World Report
tags: [Predation,Sound,Fish,Crab,Animals,Zoology]
---


Now, scientists have discovered that crustaceans, such as crabs, also hear and rely on sound to tell them about a predatory fish's approach. This is possibly because they move on and off the reef during feeding times, while the toadfish stick around all the time. Prey usually respond differently if the cue is constant versus variable, said Randalla Hughes, one of the researchers, in a news release. That said, if the ocean is too noisy for them to hear, then it's possible that their ability to survive may decline. Currently, the researchers plan to examine whether mud crabs on all reefs show the same behaviors, or if they're only sensitive to locally dominant predator species.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15509/20140618/mud-crabs-hear-crustacean-behavior-impacted-predatory-fish-noise.htm){:target="_blank" rel="noopener"}


