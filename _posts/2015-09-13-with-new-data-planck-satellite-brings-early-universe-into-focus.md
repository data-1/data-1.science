---
layout: post
title: "With new data, Planck satellite brings early universe into focus"
date: 2015-09-13
categories:
author: The Kavli Foundation 
tags: [Universe,Chronology of the universe,Inflation (cosmology),Cosmic microwave background,Big Bang,Dark matter,Planck (spacecraft),Astrophysics,Physical cosmology,Cosmology,Nature,Space science,Science,Theoretical physics,Physical sciences,Astronomy,Physics]
---


Just this month, Planck released new maps of the cosmic microwave background supporting the theory of cosmic inflation, which posits that the universe underwent a monumental expansion in the moments following the Big Bang. Yet the theory of inflation is not yet a full and detailed theory that can completely explain the universe's earliest moments. Neutrinos are some of the most mysterious particles in the universe, so this would be an important step toward understanding them. From the Planck data, Efstathiou said, it looks like dark energy is completely constant and does not interact with dark matter. ###  Learn more about the hunt for gravitational waves - and the Planck results as a whole - in The Kavli Foundation Q&A with George Efstathiou: http://www.kavlifoundation.org/science-spotlights/planck-satellite-brings-early-universe-focus  The Kavli Foundation will also be hosting a live conversation with George Efstathiou, Clement Pryke and Paul Steinhardt when they will discuss the latest results, what they mean for the theory of inflation, and what we can expect to learn about the very early universe in the coming decade:  http://www.kavlifoundation.org/science-spotlights/looking-back-time-oldest-light-existence-offers-insight-universe

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/tkf-wnd021615.php){:target="_blank" rel="noopener"}


