---
layout: post
title: "Strong teeth: Nanostructures under stress make teeth crack resistant"
date: 2015-12-11
categories:
author: Helmholtz-Zentrum Berlin für Materialien und Energie
tags: [Tooth,Dentin,Human tooth,Nanoparticle,Chemistry,Materials]
---


The internal stress works against crack propagation and increases resistance of the biostructure. These mineral nanoparticles are embedded in collagen protein fibres, with which they are tightly connected. The compressed state helps to prevents cracks from developing and we found that compression takes place in such a way that cracks cannot easily reach the tooth inner parts, which could damage the sensitive pulp. Their results may explain why artificial tooth replacements usually do not work as well as healthy teeth do: they are simply too passive, lacking the mechanisms found in the natural tooth structures, and consequently fillings cannot sustain the stresses in the mouth as well as teeth do. Compressive Residual Strains in Mineral Nanoparticles as a Possible Origin of Enhanced Crack Resistance in Human Tooth Dentin.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/hbfm-stn061015.php){:target="_blank" rel="noopener"}


