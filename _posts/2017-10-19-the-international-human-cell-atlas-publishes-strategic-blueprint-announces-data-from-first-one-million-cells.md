---
layout: post
title: "The international Human Cell Atlas publishes strategic blueprint, announces data from first one million cells"
date: 2017-10-19
categories:
author: Broad Institute Of Mit
tags: [Wellcome Sanger Institute,Genome,Technology,Biology,Life sciences,Science]
---


The blueprint's release—posted as a white paper to the HCA website—coincides with the publication of a Nature commentary by the HCA organizing committee summarizing the consortium's vision and mission. Such knowledge will, over time, have a transformative effect on the scientific understanding of human biology and on human health. The white paper states that for the first draft of the atlas, the consortium will study and map between 30 and 100 million cells from select organs and tissues, using massively-parallel single-cell RNA sequencing (a suite of genomic techniques capable of identifying gene expression profiles in thousands of individual cells at a time), related technologies to characterize other molecules, and spatial methods to map cells' locations and interactions. The OC comprises 27 scientists from 10 countries and is currently co-chaired by Aviv Regev (Broad Institute) and Sarah Teichmann (Wellcome Trust Sanger Institute). Data from the first one million cells, and the path to ten billion  The immune cell expression data announced today will be available in a public online resource by early November, and include single-cell RNA expression profiles of approximately one million cells collected from bone marrow and cord blood from healthy human donors.

<hr>

[Visit Link](https://phys.org/news/2017-10-international-human-cell-atlas-publishes.html){:target="_blank" rel="noopener"}


