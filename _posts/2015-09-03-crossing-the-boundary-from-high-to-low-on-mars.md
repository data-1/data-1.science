---
layout: post
title: "Crossing the boundary from high to low on Mars"
date: 2015-09-03
categories:
author: "$author"   
tags: [Mesa,Impact crater,Martian dichotomy,Cydonia (Mars),Mars,Planetary geology,Physical geography,Space science,Surface features of planets,Surface features of bodies of the Solar System,Planets,Planetary science,Earth sciences,Planets of the Solar System,Astronomical objects known since antiquity,Terrestrial planets,Bodies of the Solar System,Geology]
---


On the boundary between the heavily cratered southern highlands and the smooth northern lowlands of Mars is an area rich in features sculpted by water and ice. Cydonia Mensae is a region of mesa-like structures, craters and otherwise smooth terrain. It is home to the so-called ‘ Face on Mars ’ seen in NASA’s Viking 1 images, but long since known from subsequent higher-resolution imaging to be just an eroded mesa. The portion of the Cydonia Mensae region shown here lies to the southeast of the Face, and was imaged by ESA’s Mars Express on 19 November 2014. The region is thought to have hosted ancient seas or lakes that were later covered by hundreds of metres of thick lava and sediment deposits.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Mars_Express/Crossing_the_boundary_from_high_to_low_on_Mars){:target="_blank" rel="noopener"}


