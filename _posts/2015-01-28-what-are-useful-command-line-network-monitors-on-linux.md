---
layout: post
title: "What are useful command-line network monitors on Linux"
date: 2015-01-28
categories:
author: ""      
tags: [Transmission Control Protocol,Computer network,Web server,Ping (networking utility),Network monitoring,Port (computer networking),Traceroute,Packet analyzer,User Datagram Protocol,Routing,Command-line interface,Internet architecture,Cyberspace,Internet protocols,Software,Information technology,Communications protocols,Information technology management,Information Age,Computer architecture,Information and communications technology,Network architecture,Network protocols,Technology,Computer data,Telecommunications,Computer engineering,Data transmission,Computer science,Computer networking,Computing,Protocols,Mass media technology,Internet,System software,Software engineering]
---


While there are many dedicated network monitoring systems capable of 24/7/365 monitoring, you can also leverage command-line network monitors in certain situations, where a dedicated monitor is an overkill. Packet-Level Sniffing  In this category, monitoring tools capture individual packets on the wire, dissect their content, and display decoded packet content or packet-level statistics. 10. tcpdump  An command-line packet sniffer which is capable of capturing nework packets on the wire based on filter expressions, dissect a wide range of protocols associated with the packets, and dump the packet content for packet-level analysis. 15. netstat  A command-line tool that shows various statistics and properties of the networking stack, such as open TCP/UDP connections, network interface RX/TX statistics, routing tables, protocol/socket statistics. It relies on a number of TCP/UDP based scanning techniques to detect open ports, live hosts, or existing operating systems on the local network.

<hr>

[Visit Link](http://xmodulo.com/useful-command-line-network-monitors-linux.html){:target="_blank" rel="noopener"}


