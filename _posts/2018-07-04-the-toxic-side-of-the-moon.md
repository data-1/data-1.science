---
layout: post
title: "The toxic side of the Moon"
date: 2018-07-04
categories:
author: ""
tags: [Lunar soil,Moon,Dust,Apollo program,Space science,Outer space,Astronomy,Spaceflight,Planetary science,Astronautics,Flight,Nature,Physical sciences]
---


Science & Exploration The toxic side of the Moon 04/07/2018 28766 views 541 likes  When the Apollo astronauts returned from the Moon, the dust that clung to their spacesuits made their throats sore and their eyes water. Lunar dust is made of sharp, abrasive and nasty particles, but how toxic is it for humans? The Moon missions left an unanswered question of lunar exploration – one that could affect humanity’s next steps in the Solar System: can lunar dust jeopardise human health? Nasty dust  Lunar dust has silicate in it, a material commonly found on planetary bodies with volcanic activity. Miners on Earth suffer from inflamed and scarred lungs from inhaling silicate.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/The_toxic_side_of_the_Moon){:target="_blank" rel="noopener"}


