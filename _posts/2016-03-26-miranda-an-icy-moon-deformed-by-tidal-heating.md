---
layout: post
title: "Miranda: An icy moon deformed by tidal heating"
date: 2016-03-26
categories:
author: Geological Society Of America
tags: [Miranda (moon),Planetary science,Astronomical objects,Outer space,Planets of the Solar System,Bodies of the Solar System,Solar System,Astronomy,Space science]
---


Visible from left to right are Elsinore, Inverness, and Arden coronae. These coronae are visible in Miranda's southern hemisphere, and each one is at least 200 km across. During convection, warm buoyant ice rose toward the surface, driving concentric surface extension beneath the locations of the coronae, causing the formation of extensional tectonic faults. Hammond and Barr write that the internal energy that powered convection probably came from tidal heating. Hammond and Barr find that convection powered by tidal heating explains the locations of the coronae, the deformation patterns within the coronae, and the estimated heat flow during corona formation.

<hr>

[Visit Link](http://phys.org/news330274655.html){:target="_blank" rel="noopener"}


