---
layout: post
title: "Open source software: 20 years and counting"
date: 2018-04-22
categories:
author: "Simon Phipps"
tags: [Open source,Red Hat,Free software movement,Open-source software,Creative Commons license,Open-source movement,Technology,Computing,Software,Information technology,Intellectual works,Information Age]
---


Soon afterwards, the Open Source Definition was created and the seeds that became the Open Source Initiative (OSI) were sown. Like any human creation, it has a dark side as well. In the first decade, the key question concerned business models—“how can I contribute freely yet still be paid?”—while during the second, more people asked about governance—“how can I participate yet keep control/not be controlled?” Open source projects of the first decade were predominantly replacements for off-the-shelf products; in the second decade, they were increasingly components of larger solutions. While open source was always intended as a way to promote software freedom, during the first decade, conflict arose with those preferring the term “free software.” In the second decade, this conflict was largely ignored as open source adoption accelerated. This article, as well as my work at OSI, is supported by Patreon patrons.

<hr>

[Visit Link](https://opensource.com/article/18/2/open-source-20-years-and-counting){:target="_blank" rel="noopener"}


