---
layout: post
title: "Icelandic volcano's toxic gas is triple that of Europe's industry"
date: 2016-08-01
categories:
author: "University of Edinburgh"
tags: [Types of volcanic eruptions,Volcano,2010 eruptions of Eyjafjallajkull,Brarbunga,Nature,Earth sciences,Natural environment]
---


Discharge of lava from the eruption at Bárðarbunga volcano released a huge mass -- up to 120,000 tonnes per day -- of sulphur dioxide gas, which can cause acid rain and respiratory problems. Researchers hope that their study will aid understanding of how such eruptions can affect air quality in the UK. Dr John Stevenson, of the University of Edinburgh's School of GeoSciences, who took part in the study, said: This eruption produced lava instead of ash, and so it didn't impact on flights -- but it did affect air quality. These results help scientists predict where pollution from future eruptions will spread. In the study, we were concerned with the quantity of sulphur dioxide emissions, with numbers that are equally astonishing: In the beginning, the eruption emitted about eight times more sulphur dioxide per day than is emitted from all human-made sources in Europe per day.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150924104112.htm){:target="_blank" rel="noopener"}


