---
layout: post
title: "Chinese team performs gene editing on human embryo"
date: 2016-04-23
categories:
author: Bob Yirka
tags: [CRISPR,Genome editing,Cas9,CRISPR gene editing,DNA repair,Gene,Life sciences,Biology,Genetics,Biological engineering,Modification of genetic information,Molecular genetics,Branches of genetics,Biochemistry,Molecular biology,Biotechnology]
---


(Phys.org)—A team of researchers in China has announced that they have performed gene editing on human embryos. The researchers report that their desire was to see how well CRISPR would work on human embryos. In this report, we used tripronuclear (3PN) zygotes to further investigate CRISPR/Cas9-mediated gene editing in human cells. However, the efficiency of homologous recombination directed repair (HDR) of HBB was low and the edited embryos were mosaic. See also: The ISSCR has responded to the publication of gene editing research in human embryos  via Nature

<hr>

[Visit Link](http://phys.org/news348999623.html){:target="_blank" rel="noopener"}


