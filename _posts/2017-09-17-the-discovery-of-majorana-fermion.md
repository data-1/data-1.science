---
layout: post
title: "The discovery of Majorana fermion"
date: 2017-09-17
categories:
author: "Science China Press"
tags: [Majorana fermion,Superconductivity,Physics,Applied and interdisciplinary physics,Phases of matter,Physical sciences,Materials science,Quantum field theory,Science,Electromagnetism,Materials,Particle physics,Condensed matter,Condensed matter physics,Theoretical physics,Quantum mechanics]
---


The research group in Shanghai Jiao Tong University (SJTU) achieved great breakthrough in the detection of Majorana fermion in the artificial topological superconductor, which is the heterostructure of a normal superconductor and a topological insulator. There exist two major difficulties in the research of Majorana fermion in artificial topological superconductor. In the following years, they built the heterostructure Bi2Te3/NbSe2 and detected Majorana fermions in such system via STM. In the vortex core, there exist other low energy states that are unrepeatable under current STM energy resolution. Via a spin-polarized STM, this process was also detected in the heterostructure, which became not only another evidence of Majorana fermion's existence but also a potential method to control Majorana fermion in this system.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/scp-tdo031717.php){:target="_blank" rel="noopener"}


