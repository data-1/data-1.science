---
layout: post
title: "Neutrons measured with unprecedented precision using a 'magneto-gravitational trap'"
date: 2018-05-21
categories:
author: "Indiana University"
tags: [Neutron,Subatomic particle,Quark,Science,Atoms,Physics,Particle physics,Physical sciences,Nuclear physics,Subatomic particles,Nature]
---


-- A study led in part by physicists at the Indiana University Center for the Exploration of Energy and Matter could provide new insight into the composition of the universe immediately after the Big Bang -- as well as improve calculations used to predict the life span of stars and describe the rules that govern the subatomic world. The study, published May 11 in the journal Science, reports a highly accurate way to measure the decay rate of neutrons. This is a significant improvement compared to previous experiments, said Liu, who is a leader on the UNCtau experiment, which uses neutrons from the Los Alamos Neutron Science Center Ultracold Neutron source at Los Alamos National Laboratory in New Mexico. Some physicists regard the beam method as more accurate because the bottle method risks miscounting neutrons absorbed into the container as disappearing from decay. Five years to get an experiment running and producing data is very fast in our field, Liu said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/iu-nmw051518.php){:target="_blank" rel="noopener"}


