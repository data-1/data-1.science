---
layout: post
title: "Unprecedentedly wide and sharp dark matter map"
date: 2018-05-07
categories:
author: "Subaru Telescope"
tags: [Lambda-CDM model,Physical cosmology,Universe,Dark matter,Weak gravitational lensing,Redshift,Big Bang,Expansion of the universe,Cosmological constant,Accelerating expansion of the universe,Galaxy,Celestial mechanics,Astronomy,Concepts in astronomy,Nature,Cosmology,Astrophysics,Physics,Physical sciences,Science,Scientific method,Space science]
---


The standard cosmological model (called LCDM) incorporates the cosmological constant. Wide and deep imaging survey using Hyper Suprime-Cam  Figure 2: Expansion history of the Universe. Figure 3: Hyper Suprime-Cam image of a location with a highly significant dark matter halo detected through the weak gravitational lensing technique. This 3-D dark matter mass map is also quite new. What the dark matter halo count suggests and future prospects  Figure 4: An example of 3D distribution of dark matter reconstructed via tomographic methods using the weak lensing technique combined with the redshift estimates of the background galaxies.

<hr>

[Visit Link](https://phys.org/news/2018-03-unprecedentedly-wide-sharp-dark.html){:target="_blank" rel="noopener"}


