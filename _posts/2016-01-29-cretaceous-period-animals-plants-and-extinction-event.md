---
layout: post
title: "Cretaceous period: Animals, plants and extinction event"
date: 2016-01-29
categories:
author: Michael Dhar
tags: [Cretaceous,CretaceousPaleogene boundary,Dinosaur,Flowering plant,CretaceousPaleogene extinction event,Flower,Pangaea,Mesozoic,Gondwana,Bird,Archaeopteryx,Impact event,Entomophily,Chicxulub crater,Earth sciences,Nature]
---


(Image credit: USGS)  Cretaceous period plants  One hallmark of the Cretaceous period was the development and radiation of flowering plants, or angiosperms, which rapidly diversified, according to the National Park Service. Duck-billed dinosaurs were the most common type of ornithischians, a group of mostly herbivorous dinosaurs with bird-like hips, according to the Cal Poly Humboldt Natural History Museum (opens in new tab). How did the Cretaceous period end? This crater dates to within 33,000 years of the K-Pg event, Live Science previously reported. Illustration of the K-Pg extinction event at the end of the Cretaceous Period.

<hr>

[Visit Link](http://www.livescience.com/29231-cretaceous-period.html){:target="_blank" rel="noopener"}


