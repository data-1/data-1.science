---
layout: post
title: "400-million-year-old fish fossil reveals jaw structure linked to humans"
date: 2017-10-06
categories:
author: "Australian National University"
tags: [Placodermi,Fossil,Jaw,Evolution of fish,Maxilla,Blood]
---


Credit: Australian National University  A new study from ANU on a 400 million year old fish fossil has found a jaw structure that is part of the evolutionary lineage linked to humans. Co-researcher, ANU PhD scholar Yuzhi Hu, said this example was the best preserved skull and braincase of a placoderm ever found. Ms Hu analysed the CT scanning data to reveal a complete set of internal jaw cartilages for the first time in any placoderm, with structures surrounding the jaw joint different to all previous interpretations. The extinct placoderms have been traditionally regarded as an evolutionary side branch, until the recent discovery of Chinese maxillate placoderms, a fossil group researched in Beijing by co-author Dr Jing Lu before she came to ANU. The maxilla is the bone forming the upper jaw in humans, said Dr Lu from the Department of Applied Mathematics within the ANU Research School of Physics and Engineering.

<hr>

[Visit Link](https://phys.org/news/2017-08-million-year-old-fish-fossil-reveals-jaw.html){:target="_blank" rel="noopener"}


