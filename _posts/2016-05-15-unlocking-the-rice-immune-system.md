---
layout: post
title: "Unlocking the rice immune system"
date: 2016-05-15
categories:
author: Lawrence Berkeley National Laboratory
tags: [Pamela Ronald,Activator (genetics),Biology,Life sciences,Biotechnology]
---


Rice is a staple for half the world's population and the model plant for grass-type biofuel feedstocks. The research team discovered that a tyrosine-sulfated bacterial protein called RaxX, activates the rice immune receptor protein called XA21. This activation triggers an immune response against Xanthomonas oryzaepv.oryzae (Xoo), a pathogen that causes bacterial blight, a serious disease of rice crops. Our results show that RaxX, a small, previously undescribed bacterial protein, is required for activation of XA21-mediated immunity to Xoo, says Pamela Ronald, a plant geneticist for both JBEI and UC Davis who led this study. In 2009, Ronald and her group identified a small bacterial protein they named Ax21 as the molecular key that binds to the XA21 receptor to activate a rice plant's immune response.

<hr>

[Visit Link](http://phys.org/news/2015-07-rice-immune.html){:target="_blank" rel="noopener"}


