---
layout: post
title: "When helium behaves like a black hole"
date: 2017-09-17
categories:
author: "University of Vermont"
tags: [Quantum mechanics,Quantum entanglement,Physics,Superfluidity,Helium,Atom,Scientific method,Scientific theories,Applied and interdisciplinary physics,Physical sciences,Theoretical physics,Science]
---


A team of scientists has discovered that a law controlling the bizarre behavior of black holes out in space--is also true for cold helium atoms that can be studied in laboratories. They calculated that when matter falls into one of these bottomless holes in space, the amount of information it gobbles up--what scientists call its entropy--increases only as fast as its surface area increases, not its volume. We have found the same type of law is obeyed for quantum information in superfluid helium, says Del Maestro. The observation of an entanglement area law in this new experiment points toward quantum liquids, like superfluid helium, as a possible medium for starting to master entanglement. That suggests that laboratory experiments and, eventually, quantum computers could manipulate the density of a quantum liquid as a possible knob, Del Maestro says, for regulating entanglement.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/uov-whb032117.php){:target="_blank" rel="noopener"}


