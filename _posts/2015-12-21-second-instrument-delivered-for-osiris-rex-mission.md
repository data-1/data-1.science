---
layout: post
title: "Second instrument delivered for OSIRIS-REx mission"
date: 2015-12-21
categories:
author: University of Arizona
tags: [OSIRIS-REx,Goddard Space Flight Center,Solar System,Flight,Space program of the United States,Space probes,NASA,Discovery and exploration of the Solar System,Bodies of the Solar System,Planetary science,Spacecraft,Astronautics,Space exploration,Science,Astronomy,Spaceflight,Outer space,Space science]
---


The OSIRIS-REx Visible and Infrared Spectrometer (OVIRS) instrument measures visible and near infrared light from the asteroid Bennu that can be used to identify water and organic materials. The delivery of OVIRS to the spacecraft means the mission now has the capability to measure the minerals and chemicals at the sample site on Bennu, said Dante Lauretta, principal investigator for OSIRIS-REx at the University of Arizona. Through the team's efforts, OVIRS has become a remarkably capable instrument, which we expect to return exciting science from the asteroid Bennu, said Dennis Reuter, OVIRS instrument lead from Goddard. OSIRIS-REx is the first U.S. mission to return samples from an asteroid to Earth for study. ###  NASA's Goddard Space Flight Center in Greenbelt, Maryland provides overall mission management, systems engineering and safety and mission assurance for OSIRIS-REx.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/uoa-sid070815.php){:target="_blank" rel="noopener"}


