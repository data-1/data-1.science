---
layout: post
title: "TESS readies for takeoff"
date: 2018-07-01
categories:
author: "Jennifer Chu"
tags: [Transiting Exoplanet Survey Satellite,Exoplanet,Methods of detecting exoplanets,Kepler space telescope,Astronomy,Space science,Outer space,Physical sciences,Science,Planetary science,Spaceflight]
---


Scientists expect that thousands of these stars will host transiting planets, which they hope to detect through images taken with TESS’s cameras. The Transiting Exoplanet Survey Satellite (TESS) will discover thousands of exoplanets in orbit around the brightest stars in the sky. The Transiting Exoplanet Survey Satellite (TESS) will be the first to seek out these nearby worlds. At this time, the satellite design included six CCD cameras, and the team proposed that the spacecraft fly in a low-Earth orbit, similar to that of HETE-2. From there, the TESS Science Team, including Sara Seager, the Class of 1941 Professor of Earth, Atmospheric and Planetary Sciences, and deputy director of science for TESS, will look through thousands of light curves, for at least two similar dips in starlight, indicating that a planet may have passed twice in front of its star.

<hr>

[Visit Link](http://news.mit.edu/2018/tess-readies-takeoff-discover-exoplanets-0412){:target="_blank" rel="noopener"}


