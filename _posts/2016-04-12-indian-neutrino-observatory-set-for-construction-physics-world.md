---
layout: post
title: "Indian Neutrino Observatory set for construction – Physics World"
date: 2016-04-12
categories:
author:  
tags: [India-based Neutrino Observatory,Particle physics,Physics,Nature,Science,Physical sciences]
---


Getting ready for construction The Indian government has approved the $236m Indian Neutrino Observatory to be built at Pottipuram, while development of the detector is taking place at the TIFR. (Courtesy: INO)  The Indian government has given the go-ahead for a huge underground observatory that researchers hope will provide crucial insights into neutrino physics. Construction will now begin on the Rs15bn ($236m) Indian Neutrino Observatory (INO) at Pottipuram, which lies 110 km from the temple city of Madurai in the southern Indian state of Tamil Nadu. The outcome of this investment will be extraordinary and long term  Krishnaswamy Vijayraghavan, secretary of the Department of Science and Technology  The INO team hopes to use the detector to address the “neutrino-mass hierarchy”. The country led the way in the 1960s when physicists used a gold mine at Kolar in the southern state of Karnataka to create what was then the world’s deepest underground lab.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/oCwt25F-bDo/indian-neutrino-observatory-set-for-construction){:target="_blank" rel="noopener"}


