---
layout: post
title: "NMR spectroscopy platform to target the development of new therapeutic agents"
date: 2017-10-08
categories:
author: "Institut national de la recherche scientifique - INRS"
tags: [Spectroscopy,Nuclear magnetic resonance,Nuclear magnetic resonance spectroscopy,Drug discovery,Research,Life sciences,Applied and interdisciplinary physics,Biotechnology,Biology,Scientific method,Biochemistry,Molecular physics,Science,Scientific techniques,Instrumental analysis,Physical chemistry,Physical sciences,Laboratory techniques,Chemistry]
---


Implementation of a new nuclear magnetic resonance (NMR) spectroscopy platform will provide professors Nicolas Doucet and Steven LaPlante of Centre INRS-Institut Armand-Frappier with a powerful new tool for conducting an ambitious research program aimed at identifying new therapeutic molecules. This new platform, funded in part by the Canadian Foundation for Innovation's John R. Evans Leaders Fund and the Government of Quebec, will enable the two researchers to explore the role of atomic-scale molecular motions in protein and enzyme families involved in amyotrophic lateral sclerosis (ALS), diabetes, asthma, HIV, auto-immune disease, and cancer. Pooling their parallel expertise in protein NMR and drug discovery, professors Doucet and LaPlante will have access to a 600 megahertz NMR spectrometer with high throughput screening capabilities to characterize biomolecular interactions between proteins and their ligands. The knowledge gained will be crucial to fragment-based drug discovery (FBDD), a new pharmaceutical approach aimed at developing targeted and more effective drugs that cause fewer side effects. This new research capability has the potential to revolutionize drug development practices and to provide new impetus to the pharmaceutical industry in Quebec and across Canada.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/indl-nsp022817.php){:target="_blank" rel="noopener"}


