---
layout: post
title: "How bees find their way home"
date: 2017-10-17
categories:
author: "Lund University"
tags: [Honey bee,Brain,Bee,Pollination,Privacy,Cognitive science,Neuroscience]
---


The study, involving nocturnal rain forest bees, identifies which neurons in the brain allow the bee to measure speed and distance covered. In a laboratory environment, the researchers placed electrodes into individual nerve cells in the bees' brains as they undertook virtual flights, simulating their experience of searching for food in the rain forest. We sent it out on a random route and the model of the bee's navigation system that we implemented in the robot allowed it to find the direct path back to its starting point, says Stanley Heinze. He is fascinated by the fact that these insects, whose brains are about the size of a grain of rice and have 100 000 times fewer neurons than human brains, register their convoluted routes, often several kilometres long, and then have no trouble flying the most direct way home again, a task that we humans can only master with the help of GPS devices, despite our huge brains. After all, we know that pesticides are detrimental to the bees' sense of direction, which means that fewer of them will be able to return to their hive after pollinating plants in our modern agricultural landscapes.

<hr>

[Visit Link](https://phys.org/news/2017-10-bees-home.html){:target="_blank" rel="noopener"}


