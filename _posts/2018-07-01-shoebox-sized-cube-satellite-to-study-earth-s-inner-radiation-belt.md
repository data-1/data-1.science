---
layout: post
title: "Shoebox-sized cube satellite to study Earth's inner radiation belt"
date: 2018-07-01
categories:
author: "Trent Knoss, University Of Colorado At Boulder"
tags: [Van Allen radiation belt,CubeSat,Colorado Student Space Weather Experiment,Space science,Outer space,Spaceflight,Astronautics,Astronomy,Flight,Science,Spacecraft,Sky,Technology,Physical sciences]
---


CU Boulder Professor Xinlin Li holds up a model of the CSSWE cube satellite that studied energetic particles in Earth's magnetosphere. Credit: LASP  A NASA-funded cube satellite built and operated by CU Boulder researchers will study the inner radiation belt of Earth's magnetosphere, providing new insight into the energetic particles that can disrupt satellites and threaten spacewalking astronauts. CU Boulder students (including undergraduates) have worked on several successful cube satellite missions in recent years, and the campus currently has more than eight cube satellite projects in operation or in development across its various departments and research institutes. CIRBE is poised to build on the success of the Colorado Student Space Weather Experiment (CSSWE), a cube satellite that launched in 2012 to study the Van Allen belt and operated for over two years. In the years since CSSWE's launch, the researchers have further improved their ground station, which is located on the roof of the LASP building on CU Boulder's campus.

<hr>

[Visit Link](https://phys.org/news/2018-03-shoebox-sized-cube-satellite-earth-belt.html){:target="_blank" rel="noopener"}


