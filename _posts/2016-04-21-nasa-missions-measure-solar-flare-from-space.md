---
layout: post
title: "NASA missions measure solar flare from space"
date: 2016-04-21
categories:
author:  
tags: []
---


Solar flares are intense bursts of light from the Sun, created when complicated magnetic fields suddenly rearrange themselve  Solar flares are intense bursts of light from the Sun, created when complicated magnetic fields suddenly rearrange themselve  Scientists, using NASA’s solar-watching missions, have captured the most comprehensive observations of an electromagnetic phenomenon called ‘current sheet’, providing evidence that our understanding of solar flares is correct. Solar flares are intense bursts of light from the Sun, created when complicated magnetic fields suddenly rearrange themselves, converting magnetic energy into light through a process called magnetic reconnection. This configuration of magnetic fields is unstable, meaning that the same conditions that create current sheets are also ripe for magnetic reconnection. “Magnetic reconnection happens at the interface of oppositely-aligned magnetic fields,” said lead author Chunming Zhu, from the New Mexico State University. Because current sheets are so closely associated with magnetic reconnection, observing a current sheet in such detail backs up the idea that magnetic reconnection is the force behind solar flares.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/nasa-missions-measure-solar-flare-from-space/article8499824.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


