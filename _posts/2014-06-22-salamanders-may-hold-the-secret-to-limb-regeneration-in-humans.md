---
layout: post
title: "Salamanders May Hold the Secret to Limb Regeneration in Humans"
date: 2014-06-22
categories:
author: Science World Report
tags: [Regeneration (biology),Biochemistry,Life sciences,Biotechnology,Cell biology,Biology]
---


This remarkable feat of regeneration allows these amphibians to better survive and escape predators. Now, scientists have taken a closer look at the secrets of regrowing body parts in order to apply it to humans. While humans have limited regenerative abilities, other organisms, such as the salamander, are able to regenerate an impressive repertoire of complex structures including parts of their hearts, eyes, spinal cord, tails, and they are the only adult vertebrates able to regenerate full limbs, said Max Yun, one of the researchers, in a news release. More specifically, the ERK pathway must be constantly active for salamander cells to be reprogrammed and contribute to the regeneration of different body parts. We're thrilled to have found a critical molecular pathway, the ERK pathway, that determines whether an adult cell is able to be reprogrammed and help the regeneration processes, said Yun.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15581/20140621/salamanders-hold-secret-limb-regeneration-humans.htm){:target="_blank" rel="noopener"}


