---
layout: post
title: "Southern Tibetan Plateau"
date: 2016-06-21
categories:
author: ""
tags: [Alluvial fan,Earth sciences,Physical geography,Geology,Geography,Geomorphology,Natural environment,Hydrology,Nature]
---


The Tibetan Plateau was created by continental collision some 55 million years ago when the north-moving Indian Plate collided with the Eurasian Plate, causing the land to crumple and rise. With an average elevation exceeding 4500 m and an area of 2.5 million sq km, it is the highest and largest plateau in the world today. The plateau is also the world’s third largest store of ice, after the Arctic and Antarctic. Part of the Himalayas is visible along the bottom of the false-colour image, with the distinct pattern of water runoff from the mountains. At the end of these rivers and streams we can see the triangle-shapes of sediment deposits – alluvial fans – formed when the streams hit the plain and spread out.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2016/06/Southern_Tibetan_Plateau){:target="_blank" rel="noopener"}


