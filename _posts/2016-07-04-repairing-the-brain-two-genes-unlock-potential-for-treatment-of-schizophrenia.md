---
layout: post
title: "Repairing the brain: Two genes unlock potential for treatment of schizophrenia"
date: 2016-07-04
categories:
author: "Duke-NUS Graduate Medical School Singapore"
tags: [Schizophrenia,Mental disorder,Mental disorders,Neuroscience,Health,Mental health,Human diseases and disorders,Diseases and disorders,Clinical medicine,Abnormal psychology]
---


In particular, we wanted to understand the ability of a specific type of cell in the brain, termed interneurons, to modulate brain network activity to maintain a balance in brain signalling. Dr. Je and his team analysed signalling activity in neuronal cultures that either did not have the DTNBP1 gene or had lowered levels of the gene, because reduced DTNBP1 levels and genetic disruptions of DTNBP1 in mice resulted in schizophrenia-like behaviours. Using multiple model systems, they found that the low levels of DTNBP1 resulted in dysfunctional interneurons and over-activated neuronal network activity. Without BDNF, the abnormal circuit development and brain network activity observed in schizophrenia patients results. Pinpointing the importance of the abnormal delivery of BDNF has shed considerable insight into how the brain network develops.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150918105431.htm){:target="_blank" rel="noopener"}


