---
layout: post
title: "Artificial intelligence model “learns” from patient data to make cancer treatment less toxic"
date: 2018-08-12
categories:
author: "Rob Matheson"
tags: [Glioblastoma,Radiation therapy,Dose (biochemistry),Machine learning,Chemotherapy,Clinical medicine,Health sciences,Medicine,Health care,Medical specialties,Medical treatments,Health]
---


In simulated trials of 50 patients, the machine-learning model designed treatment cycles that reduced the potency to a quarter or half of nearly all the doses while maintaining the same tumor-shrinking potential. “We kept the goal, where we have to help patients by reducing tumor sizes but, at the same time, we want to make sure the quality of life — the dosing toxicity — doesn’t lead to overwhelming sickness and harmful side effects,” says Pratik Shah, a principal investigator at the Media Lab who supervised this research. At each action, it pings another clinical model — often used to predict a tumor’s change in size in response to treatments — to see if the action shrinks the mean tumor diameter. This technique, he adds, has various medical and clinical trial applications, where actions for treating patients must be regulated to prevent harmful side effects. “We said [to the model], ‘Do you have to administer the same dose for all the patients?

<hr>

[Visit Link](http://news.mit.edu/2018/artificial-intelligence-model-learns-patient-data-cancer-treatment-less-toxic-0810){:target="_blank" rel="noopener"}


