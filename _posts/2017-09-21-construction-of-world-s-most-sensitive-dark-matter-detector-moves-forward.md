---
layout: post
title: "Construction of world's most sensitive dark matter detector moves forward"
date: 2017-09-21
categories:
author: "Lawrence Berkeley National Laboratory"
tags: [Large Underground Xenon experiment,XENON,Weakly interacting massive particles,ZEPLIN-III,Dark matter,LZ experiment,Physical sciences,Science,Nature,Physics,Particle physics]
---


Credit: Lawrence Berkeley National Laboratory  LUX-ZEPLIN (LZ), a next-generation dark matter detector that will be at least 100 times more sensitive than its predecessor, has cleared another approval milestone and is on schedule to begin its deep-underground hunt for theoretical particles known as WIMPs, or weakly interacting massive particles, in 2020. LZ is named for the merger of two dark matter detection experiments: the Large Underground Xenon experiment (LUX) and the U.K.-based ZonEd Proportional scintillation in Liquid Nobel gases experiment (ZEPLIN). LUX, a smaller liquid xenon-based underground experiment at SURF will be dismantled to make way for the new project. A high-voltage system is being tested at Berkeley Lab that will generate an electric field within the detector to guide the flow of electrons produced in particle interactions to the top of the liquid xenon chamber. In the next year there will be lot of work at SURF to disassemble LUX and prepare the underground site for LZ assembly and installation.

<hr>

[Visit Link](http://phys.org/news/2016-09-world-sensitive-dark-detector.html){:target="_blank" rel="noopener"}


