---
layout: post
title: "NASA MESSENGER and STEREO measurements open new window into high-energy processes on the sun"
date: 2015-10-03
categories:
author: "$author" 
tags: [Sun,Neutron,Solar flare,STEREO,MESSENGER,Mercury (planet),Nature,Outer space,Physical phenomena,Science,Solar System,Physics,Bodies of the Solar System,Physical sciences,Astronomy,Space science]
---


Understanding the sun from afar isn't easy. Closer to Earth we can observe charged particles from the sun, but analyzing them can be a challenge as their journey is affected by magnetic fields. Neutrons, however, as they are not electrically charged, travel in straight lines from the flaring region. STEREO provided useful observations of the flare. So, together, the MESSENGER and STEREO data can provide new information about how particles are accelerated in solar flares.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/nsfc-nma070914.php){:target="_blank" rel="noopener"}


