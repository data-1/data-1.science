---
layout: post
title: "Researchers find speedometer in the brain"
date: 2016-05-07
categories:
author: DZNE - German Center for Neurodegenerative Diseases
tags: [Memory,Brain,Hippocampus,Perception,Spatial memory,Neuron,Neural circuit,Parkinsons disease,Neuroscience,Mental processes,Cognitive science,Cognition,Cognitive psychology,Interdisciplinary subfields]
---


Not just on our way to the coffee machine, but any time we move in space. Rhythmic fluctuations  It has been known for some time that the hippocampus -- the part of the brain that controls memory, particularly spatial memory -- adjusts to the speed of locomotion. We have identified the neural circuits in mice that link their spatial memory to the speed of their movement. Small cell group  The cells in question are located in the medial septum, a part of the brain directly connected to the hippocampus. In this way, they tune the spatial memory systems for optimized processing of sensory stimuli during locomotion, explains Remy.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150603132252.htm){:target="_blank" rel="noopener"}


