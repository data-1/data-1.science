---
layout: post
title: "Space radiation on Earth"
date: 2018-08-16
categories:
author: ""
tags: [Radiation,Cosmic ray,Particle accelerator,GSI Helmholtz Centre for Heavy Ion Research,Spaceflight,Space science,Outer space,Astronautics,Astronomy,Science,Flight]
---


The constant ‘rain’ of radiation in space includes cosmic rays, which, despite the name ‘ray’, comprises highly energetic particles arriving from beyond the Solar System. A new international accelerator, the Facility for Antiproton and Ion Research (FAIR), now under construction near Darmstadt, Germany, at the existing GSI Helmholtz Centre for Heavy Ion Research (GSI), will provide particle beams like the ones that exist in space and make them available to scientists for studies that will be used to make spacecraft more robust and help humans survive the rigours of spaceflight. For example, researchers will be able to investigate how cells and human DNA are altered or damaged by exposure to cosmic radiation and how well microchips stand up to the extreme conditions in space. On 14 February 2018, ESA and FAIR inked a cooperation agreement that will build on an existing framework of cooperation between the Agency and GSI, and see the two cooperate in the fields of radiation biology, electronic components, materials research, shielding materials and instrument calibration. More information  The Universe in the Laboratory: ESA and FAIR form partnership for researching cosmic radiation  Heavy but fast  New radiation research programme for human spaceflight  Cosmic opportunity for radiation research at ESA  Radiation: satellites’ unseen enemy  Follow GSI/FAIR  Instagram @universeinthelab  Twitter @GSI_en  Facebook GSIHelmholtzzentrum & FAIRAccelerator

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2018/02/Space_radiation_on_Earth){:target="_blank" rel="noopener"}


