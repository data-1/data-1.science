---
layout: post
title: "Big dinosaur discoveries in tiny toothy packages"
date: 2015-08-11
categories:
author: University Of Alberta
tags: [Dinosaur,Theropoda,CretaceousPaleogene extinction event,Biodiversity]
---


The study of 142 isolated teeth from the Campanian-Maastrichtian of the South Pyrenean Basin suggests six additional species of toothed theropods (five small, one large) were present in the region. Studying these small parts helps us reconstruct the ancient world where dinosaurs lived and to understand how their extinction happened, says lead author Angelica Torices, post-doctoral fellow in biological sciences at the University of Alberta. Artist's rendering of small theropod from the South Pyrenees. Credit: Sydney Mohr (artist), University of Alberta. The findings provide huge strides in understanding not only the diversity of carnivorous dinosaurs at the end of the Cretaceous in Europe, but also how the diversity of large animals responds to climatic changes.

<hr>

[Visit Link](http://phys.org/news/2015-08-big-dinosaur-discoveries-tiny-toothy.html){:target="_blank" rel="noopener"}


