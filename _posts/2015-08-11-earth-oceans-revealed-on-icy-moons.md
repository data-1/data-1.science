---
layout: post
title: "EARTH: Oceans revealed on icy moons"
date: 2015-08-11
categories:
author: American Geosciences Institute 
tags: [American Association for the Advancement of Science,Earth science,Natural environment,Science,Nature]
---


Alexandria, Va. - It now appears that, of the many moons of Jupiter and Saturn, two of them may have oceans beneath their icy exteriors. ###  For more on how scientists identified lunar bodies with potential oceans, read the free article in the August issue of EARTH Magazine at: http://bit.ly/1hfAwM7. EARTH Magazine brings reads exclusive and groundbreaking stories from the geoscience community in its August 2015 digital issue, and the July/August 2015 print issue, both available from http://www.earthmagazine.com. Stories include new research that widens the dispersal of humans into Arabia, a comment from scientist Rodney Viereck on space weather forecasting, and feature stories on the future of the U.S. icebreaker fleet and the role the World Endurance Championship is playing in innovating energy technologies for the auto industry. Keep up to date with the latest happenings in Earth, energy and environment news with EARTH magazine online at: http://www.earthmagazine.org.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/agi-eor080615.php){:target="_blank" rel="noopener"}


