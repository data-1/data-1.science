---
layout: post
title: "Environmentally friendly farming practices used by nearly one third of world's farms: Less developed countries tend to see the largest improvements in productivity"
date: 2018-08-28
categories:
author: "Washington State University"
tags: [Agriculture,Intensive farming,Sustainability,Organic farming,Biodiversity,Natural resources,Economy and the environment,Environment,Economy,Earth sciences,Agricultural science,Environmental science,Industries (economics),Food industry,Primary sector of the economy,Natural environment]
---


They have seen that the new practices can improve productivity, biodiversity and ecosystem services while lowering farmer costs. While the word intensification typically applies to environmentally harmful agriculture, Pretty used the term to indicate that desirable outcomes, such as more food and better ecosystem services, need not be mutually exclusive. The changes include an advanced form of Integrated Pest Management that involves Farmer Field Schools teaching farmers agroecological practices, such as building the soil, in more than 90 countries. Sustainable intensification has been shown to increase productivity, raise system diversity, reduce farmer costs, reduce negative externalities and improve ecosystem services, the researchers write. Stronger government policies across the globe are now needed to support the greater adoption of sustainable intensification farming systems so that the United Nations Sustainable Development Goals endorsed by all members of the UN are met by 2030, said Reganold.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180827180741.htm){:target="_blank" rel="noopener"}


