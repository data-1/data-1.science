---
layout: post
title: "Photoelectric Effect: Explanation & Applications"
date: 2017-10-08
categories:
author: "Elizabeth Howell"
tags: [Photoelectric effect,Photon,Light,Electron,Physics,Physical phenomena,Electrical engineering,Optics,Physical chemistry,Chemistry,Electromagnetic radiation,Atomic molecular and optical physics,Applied and interdisciplinary physics,Physical sciences,Electromagnetism,Science]
---


Physicist Albert Einstein was the first to describe the effect fully, and received a Nobel Prize for his work. What is the photoelectric effect? Each particle of light, called a photon, collides with an electron and uses some of its energy to dislodge the electron. Discovery  Before Einstein, the effect had been observed by scientists, but they were confused by the behavior because they didn't fully understand the nature of light. The latter constant describes how particles and waves behave in the atomic world.

<hr>

[Visit Link](http://www.livescience.com/58816-photoelectric-effect.html){:target="_blank" rel="noopener"}


