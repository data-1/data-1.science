---
layout: post
title: "3 Questions: Pinning down a neutrino’s mass"
date: 2018-07-01
categories:
author: "Jennifer Chu"
tags: [Neutrino,Electron,KATRIN,Electronvolt,Universe,Metrology,Nature,Physical quantities,Physical sciences,Theoretical physics,Science,Particle physics,Physics]
---


At KATRIN’s heart is a 200-ton, zeppelin-like spectrometer, and scientists hope that with the experiment launching today they can start to collect data that in the next few years will give them a better idea of just how massive neutrinos can be. Q: How will KATRIN measure the mass of a neutrino? And now we know that different neutrinos have different masses from each other. Some of that energy goes to an electron, and if you measure the electron’s energy very precisely, it turns out that tells you how much the neutrino took away. If the neutrino does have a mass, what does it look like in our experiment?

<hr>

[Visit Link](http://news.mit.edu/2018/3-questions-pinning-down-neutrino-mass-0611){:target="_blank" rel="noopener"}


