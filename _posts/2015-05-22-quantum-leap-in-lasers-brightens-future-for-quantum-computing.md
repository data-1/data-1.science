---
layout: post
title: "Quantum leap in lasers brightens future for quantum computing"
date: 2015-05-22
categories:
author: Dartmouth College
tags: [Superconductivity,Electron,Laser,Atom,Quantum dot,Light,Science,Theoretical physics,Technology,Quantum mechanics,Physical sciences,Electromagnetism,Physics]
---


The laser may play a crucial role in the development of quantum computers, which are predicted to eventually outperform today's most powerful supercomputers. Light from the laser is produced by applying electricity to the artificial atom. This causes electrons to hop across the atom and, in the process, produce photons that are trapped between two superconducting mirrors. With the new laser, electrical energy is converted to light that has the ability to transmit information to and from a quantum computer. Much the laser development came out of the thesis work of one of Rimberg's former graduate students, Fei Chen, first author on the Physical Review B paper, with help from another graduate student Juliang Li, and postdoctoral researcher Joel Stettenheim.

<hr>

[Visit Link](http://phys.org/news325255211.html){:target="_blank" rel="noopener"}


