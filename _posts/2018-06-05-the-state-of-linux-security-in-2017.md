---
layout: post
title: "The state of Linux security in 2017"
date: 2018-06-05
categories:
author: "Michael Boelen"
tags: [Linux kernel,Rootkit,Lynis,Malware,Ransomware,Vulnerability (computing),Heartbleed,Software engineering,Information technology,Computer security,Information Age,Cyberwarfare,Cybercrime,System software,Information technology management,Computer engineering,Digital media,Software development,Computer science,Technology,Computing,Software]
---


Linux security (2017 edition)  The year is closing, so it is time to review Linux security. With CVE number CVE-2017-1000367, an issue was discovered in the function get_process_ttyname(), resulting in revealing sensitive information. Linus showing some love for security  A sudden surprise: Linus wants offensive security specialists to join in the development of the Linux kernel. The Linux Security Expert project has a new database with security tools. Linux security experts  Want to learn who is active in the field of Linux security?

<hr>

[Visit Link](https://linux-audit.com/the-state-of-linux-security-2017/){:target="_blank" rel="noopener"}


