---
layout: post
title: "University scientists unraveling nature of Higgs boson"
date: 2014-06-23
categories:
author: Kansas State University 
tags: [Elementary particle,Higgs boson,Boson,Particle physics,Matter,Lepton,Fermion,Standard Model,Theoretical physics,Physical sciences,Physics,Quantum field theory,Nuclear physics,Science,Nature,Subatomic particles,Quantum mechanics]
---


MANHATTAN, Kansas — New physics research involving Kansas State University faculty members has helped shed light on how our universe works. A recently published study in the journal Nature Physics reports scientists have found evidence that the Higgs boson — a fundamental particle proposed in 1964 and discovered in 2012 — is the long sought-after particle responsible for giving mass to elementary particles. Building on the full data collected in 2011 and 2012, part of which was used to identify the Higgs boson's existence, researchers see evidence that the Higgs boson decays into fermions. We think that the Higgs boson is responsible for the generation of mass of fundamental particles, Kaadze said. The findings appear in the journal article, Evidence for the direct decay of the 125 GeV Higgs boson to fermions.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/ksu-usu062214.php){:target="_blank" rel="noopener"}


