---
layout: post
title: "The brightest, furthest pulsar in the Universe"
date: 2017-09-21
categories:
author: ""
tags: [Neutron star,Star,Pulsar,Stars,Nature,Astrophysics,Outer space,Physical phenomena,Physics,Stellar astronomy,Astronomical objects,Physical sciences,Space science,Astronomy]
---


This X-ray source is the most luminous of its type detected to date: it is 10 times brighter than the previous record holder. XMM-Newton observed the object several times in the last 13 years, with the discovery a result of a systematic search for pulsars in the data archive – its 1.13 s periodic pulses giving it away. The archival data also revealed that the pulsar’s spin rate has changed over time, from 1.43 s per rotation in 2003 to 1.13 s in 2014. “The discovery of this very unusual object, by far the most extreme ever discovered in terms of distance, luminosity and rate of increase of its rotation frequency, sets a new record for XMM-Newton, and is changing our ideas of how such objects really ‘work’,” says Norbert Schartel, ESA’s XMM-Newton project scientist. For further information, please contact: Markus Bauer  ESA Science and Robotic Exploration Communication Officer  Tel: +31 71 565 6799  Mob: +31 61 594 3 954  Email: markus.bauer@esa.int Gian Luca Israel  INAF, Osservatorio Astronomico di Roma, Italy  Email: gianluca@oa-roma.inaf.it Norbert Schartel  XMM-Newton project scientist  Email: Norbert.Schartel@esa.int

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/The_brightest_furthest_pulsar_in_the_Universe){:target="_blank" rel="noopener"}


