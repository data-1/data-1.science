---
layout: post
title: "Quantum-mechanical monopoles discovered"
date: 2015-05-20
categories:
author: ""    
tags: [Magnetism,Quantum mechanics,Magnetic monopole,Particle physics,Gas,Quantum field theory,Ultracold atom,Physics,Science,Scientific theories,Applied and interdisciplinary physics,Theoretical physics,Physical sciences]
---


Under these extreme conditions they were able to create a monopole in the quantum-mechanical field that describes the gas. 'In this nonmagnetic state, a structure was created in the field describing the gas, resembling the magnetic monopole particle as described in grand unified theories of particle physics. ', enthuses Dr. Mikko Mottonen, Aalto University. 'In the nonmagnetic state of the gas, no quantum whirlpools or monopoles are created in the synthetic magnetic field. The result is a remarkable step forward in quantum research.

<hr>

[Visit Link](http://www.spacedaily.com/reports/Quantum_mechanical_monopoles_discovered_999.html){:target="_blank" rel="noopener"}


