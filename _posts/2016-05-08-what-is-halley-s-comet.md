---
layout: post
title: "What is Halley's Comet?"
date: 2016-05-08
categories:
author: Matt Williams
tags: [Comet,Halleys Comet,Solar System,Orbit,Coma (cometary),Comet nucleus,Oort cloud,Apsis,Sun,Kuiper belt,Comet tail,Planetary science,Outer space,Nature,Local Interstellar Cloud,Bodies of the Solar System,Physical sciences,Astronomy,Space science,Astronomical objects]
---


Credit: NASA/JPL  Halley's Comet, also known as 1P/Halley, is the most well known comet in the Solar System. Using these calculations and recorded observations made of comets, he was able to determine that a comet observed in 1682 followed the same path as a comet observed in 1607. Origin and orbit:  Like all comets that take less than about 200 years to orbit the Sun, Halley's Comet is believed to have originated from the Kuiper Belt. Halley's orbital period over the last 3 centuries has been between 75–76 years, although it has varied between 74–79 years since 240 BC. The orbits of the Halley-type comets suggest that they were originally long-period comets whose orbits were perturbed by the gravity of the gas giants and directed into the inner Solar System.

<hr>

[Visit Link](http://phys.org/news353579451.html){:target="_blank" rel="noopener"}


