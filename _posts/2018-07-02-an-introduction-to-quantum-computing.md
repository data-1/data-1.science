---
layout: post
title: "An Introduction to Quantum Computing"
date: 2018-07-02
categories:
author: "Dec."
tags: []
---


The laws of physics are different at the atomic level and the classic laws of physics that we can observe in our daily life won’t apply here. What Is a Quantum Computer? What Is a Qubit? Superposition and entanglement are two fundamental principles of quantum computing. Another challenge is to make the system stable as the number of Qubits increases.

<hr>

[Visit Link](https://dzone.com/articles/an-introduction-to-quantum-computing-ankit-sharmas?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


