---
layout: post
title: "How many trees are in the world? 3 trillion, apparently"
date: 2015-09-04
categories:
author: Colm Gorey, Colm Gorey Was A Senior Journalist With Silicon Republic
tags: [Forest,Deforestation,Environmental science,Systems ecology,Climate change,Climate variability and change,Ecology,Environmental conservation,Environmental social science,Environment,Environmental issues,Natural environment,Earth sciences,Nature,Physical geography,Global environmental issues,Earth phenomena]
---


In one of the most detailed surveys ever undertaken of the world’s forests, it appears that across the globe there are approximately 3 trillion trees, but a significant number of these are under threat. The question of how many trees are in the world is not just an opportunity for a brain-teaser, but vital in terms of understanding the rate of deforestation globally, particularly when millions of trees are cut down daily. But now, a team from Yale University has gone to great lengths to combine data from satellite imagery, forest inventories and supercomputer-powered calculations to estimate the amount of trees at a square-kilometre level. Publishing its results in Nature, the three trillion trees figure that has been generated has shown that since the start of major human civilisation the planet’s tree population has plummeted by almost half (46pc). Using the current number, this amount would equate to 422 trees to every person on Earth.

<hr>

[Visit Link](https://www.siliconrepublic.com/earth-science/2015/09/03/how-many-trees-are-in-the-world){:target="_blank" rel="noopener"}


