---
layout: post
title: "Brain region that motivates behavior change discovered"
date: 2017-10-25
categories:
author: University of Pennsylvania
tags: [Brain,Reward system,Cingulate cortex,Posterior cingulate cortex,Creativity,Behavioural sciences,Interdisciplinary subfields,Cognition,Psychology,Psychological concepts,Cognitive science,Neuroscience]
---


Once the salesman understands this, he follows that pattern until it stops working and a behavior change is necessary for continued prosperity. Monkeys in the traveling-salesman experiment had the option to visit six different locations, two of which contained rewards, one large and one small. Simultaneous to watching the macaques' behavior in both experiments, Platt and his colleagues recorded neuron behavior in the posterior cingulate cortex. Techniques that directly activate the posterior cingulate cortex like brain stimulation or game play that promotes distraction, particularly within situations that don't allow a routine to form, can lead to more creativity. People who have more activity there have more mind-wandering, and they tend to be more creative, according to Platt.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171024105656.htm){:target="_blank" rel="noopener"}


