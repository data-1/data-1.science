---
layout: post
title: "Researchers glimpse distortions in atomic structure of materials"
date: 2015-07-21
categories:
author: ""   
tags: [Catalysis,Chemical bond,Atom,Materials science,News,Structure,X-ray crystallography,Materials,Physical sciences,Chemistry]
---


Image courtesy James LeBeau. Researchers have known for years that the properties of complex materials, such as alloys, are influenced by how the material's component atoms are organized - i.e., where the atoms fit into the material's crystal structure. However, detecting these distortions required indirect methods that could be difficult to interpret, so we couldn't fully explore how a material's atomic structure affects its properties, says Dr. James LeBeau, an assistant professor of materials science and engineering at NC State and corresponding author of a paper describing the new work. Now we've come up with a way to see the distortions directly, at the atomic scale, LeBeau says. To test the technique and learn more about the links between structural distortions and chemical bonds, the researchers looked at a complex material called lanthanum strontium aluminum tantalum oxide (LSAT).

<hr>

[Visit Link](http://www.spacedaily.com/reports/Researchers_glimpse_distortions_in_atomic_structure_of_materials_999.html){:target="_blank" rel="noopener"}


