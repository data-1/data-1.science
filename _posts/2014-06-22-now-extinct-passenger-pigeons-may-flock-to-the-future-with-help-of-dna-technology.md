---
layout: post
title: "Now Extinct Passenger Pigeons may Flock to the Future with Help of DNA Technology"
date: 2014-06-22
categories:
author: Science World Report
tags: [Passenger pigeon,Columbidae,De-extinction,Genetics,Biology]
---


According to researchers from the non-profit organization, Revive and Restore, with the help of advanced DNA technology, we could be seeing a passenger pigeon in the not so distant future. The goal... is to bring the passenger pigeon all the way back using the genome of the band-tailed pigeon and state-of-the-art genomic technology, wrote the organization, who created the Great Passenger Pigeon Comeback project. This was a real wake-up call for the public and frankly for scientists too, said Helen James, curator of birds at the Smithsonian Natural History Museum, via The Los Angeles Times. Ornithologists studied birds and they didn't really think of species becoming extinct. Though no breakthrough reports have yet been noted, Martha may be up for de-extinction with the help of these scientists in time.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15570/20140620/now-extinct-passenger-pigeons-may-flock-to-the-future-with-help-of-dna-technology.htm){:target="_blank" rel="noopener"}


