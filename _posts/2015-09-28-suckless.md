---
layout: post
title: "Suckless"
date: 2015-09-28
categories:
author: "$author"   
tags: []
---


This project focuses on advanced and experienced computer users, in contrast with the usual proprietary software world or many mainstream open source projects that focus more on average and non-technical end users. Download or subscribe to this show at twit.tv/floss  Here's what's coming up for FLOSS in the future. Think your open source project should be on FLOSS Weekly? Email Randal at merlyn@stonehenge.com  Thanks to Cachefly for providing the bandwidth for this podcast and Lullabot's Jeff Robbins, web designer and musician, for our theme music. Links

<hr>

[Visit Link](http://lxer.com/module/newswire/ext_link.php?rid=219724){:target="_blank" rel="noopener"}


