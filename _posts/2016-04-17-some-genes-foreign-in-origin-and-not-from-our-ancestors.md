---
layout: post
title: "Some genes 'foreign' in origin and not from our ancestors"
date: 2016-04-17
categories:
author: Biomed Central
tags: [Horizontal gene transfer,Gene,Organism,Bacteria,Evolution,Metabolism,Animal,Biology,Genetics,Biotechnology,Nature,Life sciences]
---


However, the idea that HGT occurs in more complex animals, such as humans, rather than them solely gaining genes directly from ancestors, has been widely debated and contested. The researchers studied the genomes of 12 species of Drosophila or fruit fly, four species of nematode worm, and 10 species of primate, including humans. In humans, they confirmed 17 previously-reported genes acquired from HGT, and identified 128 additional foreign genes in the human genome that have not previously been reported. This explains why some previous studies, which only focused on bacteria as the source of HGT, originally rejected the idea that these genes were 'foreign' in origin. While screening for contamination is necessary, the potential for bacterial sequences being a genuine part of an animal's genome originating from HGT should not be ignored, say the authors.

<hr>

[Visit Link](http://phys.org/news345367056.html){:target="_blank" rel="noopener"}


