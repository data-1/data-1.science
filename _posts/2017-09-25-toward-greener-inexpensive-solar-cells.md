---
layout: post
title: "Toward 'greener,' inexpensive solar cells"
date: 2017-09-25
categories:
author: "American Chemical Society"
tags: [Perovskite solar cell,American Chemical Society,Solar cell,Solar power,Renewable energy,Climate change mitigation,Electricity,Chemistry,Energy,Sustainable energy,Energy conversion,Nature,Technology,Sustainable technologies,Energy technology,Solar energy,Photovoltaics,Energy harvesting,Materials,Physical quantities,Glass applications,Optoelectronics,Energy and the environment,Electrical engineering,Semiconductors]
---


Now scientists are reporting in the Journal of the American Chemical Society a new advance toward more practical, greener solar cells made with inexpensive halide perovskite materials. They have developed low-bandgap perovskite solar cells with a reduced lead content and a power conversion efficiency of 15 percent. But further improving the efficiency requires stacking two sub-cells with the top one exhibiting a wide bandgap and the bottom a low bandgap to form a tandem cell. Other teams recently reported low-bandgap cells with about 13.6 percent efficiency. The reduction in lead content and improved efficiency for a low-bandgap cell represent a significant step toward practical, more environmentally friendly perovskite tandem solar cells, the researchers say.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/acs-ti100716.php){:target="_blank" rel="noopener"}


