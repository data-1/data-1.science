---
layout: post
title: "Octopus genome sequenced"
date: 2015-09-07
categories:
author: University of Chicago Medical Center 
tags: [Gene,Genome,Cephalopod,Octopus,Evolution,Transposable element,Whole genome sequencing,Genetics,Nervous system,Brain,Zinc finger,Gene family,DNA sequencing,Transcription factor,Human genome,Biology,Transcription (biology),Gene expression,Hox gene,Biotechnology,Life sciences,Molecular biology,Biochemistry]
---


The first whole genome analysis of an octopus reveals unique genomic features that likely played a role in the evolution of traits such as large complex nervous systems and adaptive camouflage. The researchers discovered striking differences from other invertebrates, including widespread genomic rearrangements and a dramatic expansion of a family of genes involved in neuronal development that was once thought to be unique to vertebrates. To study the genetics of these specialized traits, Ragsdale and his colleagues sequenced the genome of the California two-spot octopus (Octopus bimaculoides) to a high level of coverage (on average, each base pair was sequenced 60 times). Instead, the evolution of the octopus genome was likely driven by the expansion of a few specific gene families, widespread genome shuffling and the appearance of novel genes. Overall, however, gene family sizes in octopuses are largely similar to those found in other invertebrates.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/uocm-ogs081015.php){:target="_blank" rel="noopener"}


