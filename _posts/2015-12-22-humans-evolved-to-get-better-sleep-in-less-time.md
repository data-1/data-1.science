---
layout: post
title: "Humans evolved to get better sleep in less time"
date: 2015-12-22
categories:
author: Duke University
tags: [Sleep,Primate,Rapid eye movement sleep,Human,Lemur,Animals,Zoology]
---


What's more, our sleep tends to be more efficient, meaning we spend a smaller proportion of time in light stages of sleep, and more of our sleep time in deeper stages of sleep. But in primates such as mouse lemurs, mongoose lemurs and African green monkeys, REM sleep barely climbs above five percent. Humans are unique in having shorter, higher quality sleep, said anthropologist and study co-author David Samson of Duke, who logged nearly 2,000 hours watching orangutans in REM and non-REM sleep as part of his dissertation research prior to coming to Duke. If artificial light and other aspects of modern life were solely responsible for shortening our sleep, we'd expect hunter-gatherer societies without access to electricity to sleep more, Samson said. The researchers attribute the shift towards shorter, more efficient sleep in part to the transition from sleeping in beds in the trees, as our early human ancestors probably did, to sleeping on the ground as we do today.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/du-het121415.php){:target="_blank" rel="noopener"}


