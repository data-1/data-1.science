---
layout: post
title: "To infinity and beyond: Light goes infinitely fast with new on-chip material"
date: 2015-12-02
categories:
author: Harvard John A. Paulson School of Engineering and Applied Sciences 
tags: [Metamaterial,Light,Waveguide,Wavelength,Refraction,Refractive index,Optics,Wave,Photonics,Waves,Oscillation,Electrodynamics,Physics,Electromagnetic radiation,Scientific phenomena,Electrical engineering,Electromagnetism,Natural philosophy,Physical sciences,Physical phenomena,Applied and interdisciplinary physics,Atomic molecular and optical physics,Radiation,Motion (physics),Science]
---


When light passes through water, for example, its phase velocity is reduced as its wavelengths get squished together. In a zero-index material, there is no phase advance, meaning light no longer behaves as a moving wave, traveling through space in a series of crests and troughs. Instead, the zero-index material creates a constant phase -- all crests or all troughs -- stretching out in infinitely long wavelengths. Integrated photonic circuits are hampered by weak and inefficient optical energy confinement in standard silicon waveguides, said Yang Li, a postdoctoral fellow in the Mazur Group and first author on the paper. This on-chip metamaterial opens the door to exploring the physics of zero index and its applications in integrated optics, said Mazur.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/hu-tia101415.php){:target="_blank" rel="noopener"}


