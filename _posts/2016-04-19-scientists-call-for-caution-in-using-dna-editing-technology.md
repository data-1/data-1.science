---
layout: post
title: "Scientists call for caution in using DNA-editing technology"
date: 2016-04-19
categories:
author: Robert Sanders, University Of California - Berkeley
tags: [CRISPR gene editing,Genome editing,Genetics,Jennifer Doudna,Biology,Biotechnology,Life sciences,Technology,Molecular biology,Branches of genetics,Biological engineering]
---


The bacterial enzyme Cas9 is the engine of RNA-programmed genome engineering in human cells. Credit: Jennifer Doudna/UC Berkeley  A group of 18 scientists and ethicists today warned that a revolutionary new tool to cut and splice DNA should be used cautiously when attempting to fix human genetic disease, and strongly discouraged any attempts at making changes to the human genome that could be passed on to offspring. Among the authors of this warning is Jennifer Doudna, the co-inventor of the technology, called CRISPR-Cas9, which is driving a new interest in gene therapy, or genome engineering. Correcting genetic defects  Scientists today are changing DNA sequences to correct genetic defects in animals as well as cultured tissues generated from stem cells, strategies that could eventually be used to treat human disease. Even a germline modification that eliminated a genetic disease that has plagued a family for generations could have unintended consequences, given current limitations on our knowledge of human genetics.

<hr>

[Visit Link](http://phys.org/news346320388.html){:target="_blank" rel="noopener"}


