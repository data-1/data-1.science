---
layout: post
title: "Physicists propose test of quantum gravity using current technology"
date: 2017-11-01
categories:
author: "Lisa Zyga"
tags: [Quantum gravity,Quantum mechanics,Planck units,Physics,Science,Theoretical physics,Scientific theories,Physical sciences,Applied and interdisciplinary physics,Scientific method,Epistemology of science,Physical cosmology]
---


One reason why testing quantum gravity is so challenging is that its effects appear only at very high-energy scales and their corresponding tiny length scales. Many proposed theories of quantum gravity, including loop quantum gravity and string theory, are noncommutative theories, in which spacetime geometry is noncommutative. Using current optical setups, this phase shift can be measured with sufficiently high levels of accuracy that, according to the physicists' calculations, would make it possible to access the energy scale near the Planck length. By accessing this scale, the experiment could potentially probe the effects of noncommutative theories at the energy regime relevant to quantum gravity. So, we have proposed a way to test this idea using an opto-mechaical experiment.

<hr>

[Visit Link](https://phys.org/news/2017-10-physicists-quantum-gravity-current-technology.html){:target="_blank" rel="noopener"}


