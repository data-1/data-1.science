---
layout: post
title: "Galileo begins serving the globe"
date: 2017-09-22
categories:
author: ""
tags: [Galileo (satellite navigation),International Cospas-Sarsat Programme,Satellite navigation,Technology,Telecommunications,Spaceflight,Geopositioning,Geographic position,Satellites,Outer space,Computing,Telecommunications engineering]
---


Applications Galileo begins serving the globe 15/12/2016 23587 views 213 likes  Europe’s own Galileo satellite navigation system has begun operating, with the satellites in space delivering positioning, navigation and timing information to users around the globe. Further launches will continue to build the satellite constellation, which will gradually improve the system performance and availability worldwide. ESA has overseen the design and deployment of Galileo on behalf of the Commission, with system operations and service provision due to be entrusted to the European Global Navigation Satellite System Agency next year. Galileo satellite “The announcement of Initial Services is the recognition that the effort, time and money invested by ESA and the Commission has succeeded, that the work of our engineers and other staff has paid off, that European industry can be proud of having delivered this fantastic system.” Paul Verhoef, ESA’s Director of the Galileo Programme and Navigation-related Activities, added, “Today’s announcement marks the transition from a test system to one that is operational. “In addition, together with the Commission we have started work on the second generation, and this is likely to be a long but rewarding adventure.”  Cospas–Sarsat Initial Services Galileo is now providing three service types, the availability of which will continue to be improved.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Navigation/Galileo_begins_serving_the_globe){:target="_blank" rel="noopener"}


