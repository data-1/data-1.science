---
layout: post
title: "Why does Venus' atmosphere rotate sixty times faster than its surface?"
date: 2015-10-03
categories:
author: Instituto De Astrofísica De Andalucía
tags: [Venus,Atmosphere,Planet,Atmosphere of Venus,Venus Express,Wind,Nature,Wave,Earth,Space science,Outer space,Physical sciences,Planets,Planetary science,Astronomy]
---


A study has just identified the nature of these waves for the first time. This is mostly due to the fact that the dynamics of Venus' atmosphere is radically different from planets with higher rotation velocities such as Mars or the Earth: while wind plays a predominant role in the equilibrium of atmospheric pressure of the former, in the latter two the dominant factor is rotation. We have for the first time solved the equations describing the movement of the atmospheric waves particular to Venus, which in turn has allowed us to identify the nature of the waves observed by the Venus Express mission, says Javier Peralta (IAA-CSIC). Some of the waves we have found, such as the acoustic waves or the gravity waves, have properties almost identical to those on Earth, says Javier Peralta (IAA-CSIC)-. iopscience.iop.org/0067-0049/213/1/18/article  J. Peralta et al. Analytical Solution for Waves in Planets with Atmospheric Superrotation.

<hr>

[Visit Link](http://phys.org/news324203307.html){:target="_blank" rel="noopener"}


