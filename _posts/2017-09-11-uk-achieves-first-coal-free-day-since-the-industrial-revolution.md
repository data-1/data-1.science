---
layout: post
title: "UK achieves first coal-free day since the Industrial Revolution"
date: 2017-09-11
categories:
author: "Emily Cashen, Monday, April"
tags: [Coal,Wind power,Fossil fuel power station,Power (physics),Sustainable development,Sustainable technologies,Physical quantities,Nature,Energy,Sustainable energy,Energy and the environment,Climate change mitigation,Natural resources,Natural environment,Renewable energy,Renewable resources,Climate change,Electric power]
---


In a landmark moment for energy usage, the UK has generated a day’s worth of electricity without using coal  For the first time in over 130 years, the UK has gone a full day without using coal power. In 2015, the polluting fuel accounted for 23 percent of the nation’s energy creation, but this figure had fallen to just nine percent by 2016. A combination of a low demand for electricity and a large amount of wind helped to create the nation’s landmark coal-free day  In an effort to cut carbon emissions in line with the Paris Agreement on climate change, the UK Government has pledged to phase out coal from its energy system over the next decade. Many of the nation’s coal plants have already been closed or have been switched to biomass burning plants in a bid to reduce carbon emissions. According to National Grid officials, a combination of a low demand for electricity and a large amount of wind helped to create the nation’s landmark coal-free day.

<hr>

[Visit Link](http://www.theneweconomy.com/home/featured/uk-achieves-first-coal-free-day-since-the-industrial-revolution){:target="_blank" rel="noopener"}


