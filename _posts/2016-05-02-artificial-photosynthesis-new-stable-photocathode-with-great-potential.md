---
layout: post
title: "Artificial photosynthesis: New, stable photocathode with great potential"
date: 2016-05-02
categories:
author: Helmholtz-Zentrum Berlin für Materialien und Energie
tags: [Titanium dioxide,Solar cell,Hydrogen,Solar energy,Thin-film solar cell,Corrosion,Thin film,Nanoparticle,Chemistry,Materials,Physical sciences,Physical chemistry,Technology,Nature,Applied and interdisciplinary physics,Artificial materials,Manufacturing,Chemical substances]
---


New photocathode with several advantages  Under the Light2Hydrogen BMBF Cluster project and an on-going Solar H2 DFG Priority programme, a team from the HZB Institute for Solar Fuels has now developed a novel photoelectrode that solves these problems: it consists of chalcopyrite (a material used in device grade thin film solar cells) that has been coated with a thin, transparent, conductive oxide film of titanium dioxide (TiO2). The special characteristics are: the TiO2 film is polycrystalline and contains a small amount of platinum in the form of nanoparticles. Firstly, it produces under sun light illumination a photovoltage of almost 0.5 volts and very high photocurrent densities of up to 38 mA/cm2; secondly, it acts as a catalyst to accelerate the formation of hydrogen, and finally, it is chemically protected against corrosion as well. More than 80 % of the incident visible sunlight was photoelectrically converted by this composite system into electric current available for the hydrogen generation, says Schedel-Niedrig. But anyway, we demonstrate the feasibility of such future-oriented chemical robust photoelectrocatalytic systems that have the potential to convert solar energy to hydrogen, i.e to chemical energy for storage.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/hbfm-apn051215.php){:target="_blank" rel="noopener"}


