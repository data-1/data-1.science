---
layout: post
title: "Fermilab’s NOvA neutrino experiment kicks off – Physics World"
date: 2016-03-28
categories:
author: Tushna Commissariat
tags: [NOvA,Fermilab,Neutrino,Particle physics,Physics,Science,Leptons]
---


Up and away: NOvA’s massive far detector being constructed  Construction of a giant neutrino experiment operated by the Fermi National Accelerator Laboratory (Fermilab) in the US was completed on schedule last month and under budget. The NOvA experiment is made up of two colossal detectors – one at Fermilab near Chicago and the other 800 km away deep in the North Woods, Minnesota. NOvA is the most powerful accelerator-based neutrino experiment to be built in the US and the one with the furthest diatance between detectors in the world. Ghostly particles  Interacting with matter only via the weak force and being so fiendishly difficult to detect, neutrinos come in three different types or “flavours” – electron, muon and tau – and change or “oscillate” from one type to another as they travel over long distances. NOvAs two detectors sit in the path of an extremely powerful beam of neutrinos that will be sent from Fermilab.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/nHANUH7NJWs/fermilabs-nova-neutrino-experiment-kicks-off){:target="_blank" rel="noopener"}


