---
layout: post
title: "CryoSat unveils secrets of the deep"
date: 2015-09-03
categories:
author: "$author"   
tags: [Seabed,Seamount,Nature,Earth sciences,Physical geography,Oceanography,Applied and interdisciplinary physics,Hydrography,Geology,Hydrology,Geography]
---


Applications CryoSat unveils secrets of the deep 03/10/2014 12721 views 111 likes  ESA’s ice mission has been used to create a new gravity map, exposing thousands of previously unchartered ‘seamounts’, ridges and deep ocean structures. Carrying a radar altimeter, CryoSat’s main role is to provide detailed measurements of the height of the world’s ice. However, CryoSat works continuously, whether there is ice below or not. This means that the satellite can also measure the height of the surface of the sea. Atlantic bed imprinted in gravity They used measurements that CryoSat has captured over the oceans during the last four years as well as measurements from the French–US Jason-1 satellite, which was retasked to map the gravity field during the last year of its 12-year mission.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/CryoSat/CryoSat_unveils_secrets_of_the_deep){:target="_blank" rel="noopener"}


