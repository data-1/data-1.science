---
layout: post
title: "Experiments of the Russian scientists in space lead to a new way of 3D-bioprinting"
date: 2018-07-01
categories:
author: "AKSON Russian Science Communication Association"
tags: [3D bioprinting,Magnetism,Magnetic field,Materials,Materials science,Science,Electromagnetism,Physics,Applied and interdisciplinary physics,Chemistry,Physical sciences]
---


Thanks to the research of magnetic levitation in the conditions of microgravity, a new technology for 3D printing of biological tissues was developed. There are ways in which biological objects are developed without the use of multi-layer approach, for example, magnetic bioprinting, when the cell material is directed to the desired location by means of the magnetic fields. The researchers from the 3D BioprintingSolutions company in collaboration with the other Russian and foreign scientists developed the new method of bioprinting that allows to create 3D- biological objects without the use of layer-by-layer approach and magnetic labels. In their experimental study, the JIHT researchers described how small charged particles behave in the magnetic field of a special shape under the microgravity conditions, including zero gravity. The results of the Coulomb crystal experiment on the study of the formation of the spatially ordered structures led to the development of a new method for the formative 3D-biofactory of the tissue-like structures based on the programmable self-assembly of the living tissues and organs under the conditions of gravity and microgravity by means of an inhomogeneous magnetic field, summarized the author.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/arsc-eot062218.php){:target="_blank" rel="noopener"}


