---
layout: post
title: "Researchers discover gene that controls melting point of cocoa butter"
date: 2016-04-23
categories:
author: Jeff Mulhollem, Pennsylvania State University
tags: [Theobroma cacao,Chocolate,Cocoa butter,Cocoa bean,Biology]
---


Blossoms on a chocolate (or cacao) tree. Research by Penn State scientists has identified a gene that determines the melting point of cocoa butter, which could lead to new varieties of chocolate and to improved pharmaceuticals and cosmetics. Cocoa butter with altered melting points may find new uses in specialty chocolates, cosmetics and pharmaceuticals, said lead researcher Mark Guiltinan, professor of plant molecular biology, who has been conducting research on the cacao tree for three decades. Penn State scientist Mark Guiltinan leads a research team studying the genetics behind key traits of the plant. Characterization of a stearoyl-acyl carrier protein desaturase gene family from chocolate tree, Theobroma cacao L, 14 April 2015.

<hr>

[Visit Link](http://phys.org/news348475849.html){:target="_blank" rel="noopener"}


