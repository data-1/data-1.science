---
layout: post
title: "Hubble confirms new dark spot on Neptune"
date: 2016-06-24
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Neptune,Hubble Space Telescope,Jupiter,Vortex,Cloud,Planets of the Solar System,Sky,Planemos,Astronomy,Space science,Physical sciences,Bodies of the Solar System,Science,Outer space,Solar System,Planets,Astronomical objects,Planetary science]
---


New images obtained on May 16, 2016, by NASA's Hubble Space Telescope confirm the presence of a dark vortex in the atmosphere of Neptune. Though similar features were seen during the Voyager 2 flyby of Neptune in 1989 and by the Hubble Space Telescope in 1994, this vortex is the first one observed on Neptune in the 21st century. Neptune's dark vortices are high-pressure systems and are usually accompanied by bright companion clouds, which are also now visible on the distant planet. And the companion clouds are similar to so-called orographic clouds that appear as pancake-shaped features lingering over mountains on Earth. Astronomers suspected that these clouds might be bright companion clouds following an unseen dark vortex.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/nsfc-hcn062316.php){:target="_blank" rel="noopener"}


