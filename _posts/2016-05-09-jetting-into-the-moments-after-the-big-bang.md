---
layout: post
title: "Jetting into the moments after the Big Bang"
date: 2016-05-09
categories:
author: Department of Energy, Office of Science
tags: [ALICE experiment,Matter,Gluon,Quark,Large Hadron Collider,Subatomic particle,Subatomic particles,Quantum mechanics,Theoretical physics,Nature,Physical sciences,Nuclear physics,Particle physics,Physics]
---


To explore the properties of this plasma of quarks and gluons as it expands and cools, a new Di-Jet Calorimeter (DCal) was installed in the ALICE detector to provide unique insights into the nature of the universe. The structure of the neutrons and protons (the particle constituents of an atom's nuclei) that make up all matter, including coffee cups and constellations, are explained in terms of the underlying quarks and gluons along with a mechanism that keeps quarks tightly confined. The relationship between the energy and angles of the two jets is modified by the plasma of quarks and gluons. With the LHC, scientists can study strongly interacting matter at extreme energy densities using, among other instruments, a heavy ion detector codenamed ALICE (A Large Ion Collider Experiment). The installation of the calorimeter was recently completed.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150619141732.htm){:target="_blank" rel="noopener"}


