---
layout: post
title: "Accelerating materials discovery"
date: 2016-08-22
categories:
author: "Harvard University"
tags: [OLED,Thermally activated delayed fluorescence,Liquid-crystal display,Technology,Chemistry,Science]
---


- August 15, 2016 - A powerful materials discovery platform created at Harvard University to dramatically accelerate the process of screening millions of molecules for use in future technologies will now speed the commercial development of next-generation electronic displays. Harvard has licensed the deep-learning software platform, dubbed the Molecular Space Shuttle, to Kyulux, Inc., a Japan- and Boston-based developer of OLED display and lighting products. Except for display and lighting applications, which Kyulux is pursuing, the Molecular Space Shuttle will be available for additional licensing to the broad range of industries that seek to identify candidate molecules for new technologies. Adding the incoming team and the Molecular Space Shuttle will allow us to rapidly accelerate our discovery and commercialization of the next generation of OLED materials. Development of the screening software was supported in part by OTD's Physical Sciences & Engineering Accelerator.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/hu-amd081416.php){:target="_blank" rel="noopener"}


