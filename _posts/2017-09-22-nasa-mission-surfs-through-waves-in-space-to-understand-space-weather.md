---
layout: post
title: "NASA mission surfs through waves in space to understand space weather"
date: 2017-09-22
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Van Allen radiation belt,Goddard Space Flight Center,Wave,Particle physics,Radiation,Physical sciences,Electromagnetism,Science,Physics,Space science,Outer space,Nature,Astronomy]
---


One type of wave, plasmaspheric hiss, is particularly important for removing charged particles from the Van Allen radiation belts, a seething coil of particles encircling Earth, which can interfere with satellites and telecommunications. The new study looked at a newly identified population of hiss waves at a lower frequency than usually studied. Constantly changing electric and magnetic fields rolling through space interact with the particles, creating waves in the plasma (like hiss), which are integral to sculpting the near-Earth space environment. To understand the ever-changing near-Earth particle ecosystem and make better space weather predictions, scientists create models of the plasma waves. NASA's Van Allen Probes spacecraft study hiss and other plasma waves as part of their work to understand the complex interactions of particles and electromagnetic fields in near-Earth space.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-07/nsfc-nms072417.php){:target="_blank" rel="noopener"}


