---
layout: post
title: "Free HTTPS certs for all – Let's Encrypt opens doors to world+dog"
date: 2015-12-04
categories:
author: Chris Williams, Editor In Chief
tags: []
---


How-to The Let's Encrypt project has opened to the public, allowing anyone to obtain free TLS certificates and set up HTTPS websites in a few simple steps. The public beta went live at 1800 GMT (1000 PT) today. Next, the client installs its dependencies, and then asks for your email address so you can be contacted if there are any problems. Next, it asks if you want to force all traffic to go through HTTPS – yes, obviously. Let’s Encrypt was built to enable that by making it as easy as possible to get and manage certificates, the team wrote in a blog post today.

<hr>

[Visit Link](http://lxer.com/module/newswire/ext_link.php?rid=222865){:target="_blank" rel="noopener"}


