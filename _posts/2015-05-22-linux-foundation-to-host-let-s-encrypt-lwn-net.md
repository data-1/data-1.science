---
layout: post
title: "Linux Foundation to host Let's Encrypt [LWN.net]"
date: 2015-05-22
categories:
author: Posted April
tags: [Lets Encrypt,Cybercrime,Cryptography,Cyberspace,Security engineering,Application layer protocols,Digital rights,Computing,Transport Layer Security,Internet protocols,Espionage techniques,Internet,Cryptographic protocols,Computer security standards,Internet security,Internet Standards,Computer networking,Information Age,Internet architecture,Cyberwarfare,Technology,Communications protocols,Protocols,Secrecy,Military communications,Security technology,Crime prevention,Telecommunications,Computer network security,Secure communication,Computer security]
---


[Announcements] Posted Apr 9, 2015 23:44 UTC (Thu) by n8willis  The Linux Foundation (LF) has announced that it will serve as host of the Let's Encrypt project, as well as the Internet Security Research Group (ISRG). Let's Encrypt is the free, automated SSL/TLS certificate authority that was announced in November 2014 by the Electronic Frontier Foundation (EFF) to provide TLS certificates for every domain on the web. ISRG is the non-profit organization created to spearhead efforts like Let's Encrypt (which, as of now, is ISRG's only public project). In the LF announcement, executive director Jim Zemlin notes that  by hosting this important encryption project in a neutral forum we can accelerate the work towards a free, automated and easy security certification process that benefits millions of people around the world. Comments (18 posted)

<hr>

[Visit Link](http://lwn.net/Articles/639857/rss){:target="_blank" rel="noopener"}


