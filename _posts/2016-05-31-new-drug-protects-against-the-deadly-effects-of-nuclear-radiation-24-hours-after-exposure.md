---
layout: post
title: "New drug protects against the deadly effects of nuclear radiation 24 hours after exposure"
date: 2016-05-31
categories:
author: "University of Texas Medical Branch at Galveston"
tags: [Acute radiation syndrome,News aggregator,Gastrointestinal tract,Causes of death,Diseases and disorders,Health sciences,Medicine,Medical specialties,Clinical medicine,Health]
---


An interdisciplinary research team led by The University of Texas Medical Branch at Galveston reports a new breakthrough in countering the deadly effects of radiation exposure. UTMB lead author Carla Kantara, postdoctoral fellow in biochemistry and molecular biology, said that a single injection of the investigative peptide drug TP508 given 24 hours after a potentially-lethal exposure to radiation appears to significantly increase survival and delay mortality in mice by counteracting damage to the gastrointestinal system. Exposure to high doses of radiation triggers a number of potentially lethal effects. The GI toxicity syndrome is triggered by radiation-induced damage to crypt cells in the small intestines and colon that must continuously replenish in order for the GI tract to work properly. The lack of available treatments that can effectively protect against radiation-induced damage has prompted a search for countermeasures that can minimize the effects of radiation after exposure, accelerate tissue repair in radiation-exposed individuals and increase the chances for survival following a nuclear event, said Darrell Carney, UTMB adjunct professor in biochemistry and molecular biology and CEO of Chrysalis BioTherapeutics, Inc. Because radiation-induced damage to the intestines plays such a key role in how well a person recovers from radiation exposure, it's crucial to develop novel medications capable of preventing GI damage.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150821164157.htm){:target="_blank" rel="noopener"}


