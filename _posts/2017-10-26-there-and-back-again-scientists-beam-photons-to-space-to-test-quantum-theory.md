---
layout: post
title: "There and Back Again: Scientists Beam Photons to Space to Test Quantum Theory"
date: 2017-10-26
categories:
author: Harrison Tasoff
tags: [Light,Quantum mechanics,Photon,Physics,Science,Theoretical physics,Physical sciences,Applied and interdisciplinary physics,Scientific theories]
---


In the quantum theory of reality, particles like electrons and photons behave like waves as well, depending on how scientists measure them. In 1803, long before the conception of quantum theory, physicist Thomas Young conducted a famous experiment to demonstrate that light behaves like a wave. A beam of light (top left) is split in two and heads down separate paths. The experiment Vallone's team conducted is closer to Wheeler's original proposal, Jennewein told Space.com, which relied on the distance the light traveled to keep it separated for a long time. (Image credit: QuantumFuture Research Group/University of Padova - DEI)  More quantum physics in space  The experiment conducted by Vallone's team joins a new trend of space-based quantum research.

<hr>

[Visit Link](https://www.livescience.com/60777-testing-quantum-mechanics-in-space.html){:target="_blank" rel="noopener"}


