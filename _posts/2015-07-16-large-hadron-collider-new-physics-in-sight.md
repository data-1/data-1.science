---
layout: post
title: "Large Hadron Collider: New physics in sight?"
date: 2015-07-16
categories:
author: ""   
tags: []
---


The LHC is looking for signs of physics beyond the standard model, such as supersymmetry, dark matter and new particles. In its second run, the Large Hadron Collider, among other things, will be looking for signs of physics beyond the standard model. In this search, of the experiments running in the LHC, the LHCb experiment could well be the first to detect such physics. B mesons are a group of particles all of which contain the b quark, and, hence the name LHCb. However, physicists believe that this model is incomplete, and there exists physics beyond what is described by the standard model.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/lhc-new-physics-in-sight/article7411360.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


