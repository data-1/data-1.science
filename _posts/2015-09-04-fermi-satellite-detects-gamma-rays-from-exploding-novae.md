---
layout: post
title: "Fermi satellite detects gamma-rays from exploding novae"
date: 2015-09-04
categories:
author: Arizona State University 
tags: [Nova,Star,Fermi Gamma-ray Space Telescope,Gamma ray,White dwarf,Nature,Astrophysics,Science,Stars,Outer space,Stellar astronomy,Astronomical objects,Physical sciences,Physics,Physical phenomena,Space science,Astronomy]
---


ASU Regents' Professor Sumner Starrfield is part of a team that used the Large Area Telescope (LAT) onboard NASA's Fermi Gamma-ray Space Telescope satellite to discover very high energy gamma rays (the most energetic form of light) being emitted by an exploding star. It erupted in a rare type of nova called a likely recurrent symbiotic nova, in which the companion star is a pulsating red giant star (about 500 times the size of the sun) blowing out a strong stellar wind. Researchers suggested that the gamma rays in the nova were generated when the blast waves from the nova collided with the very dense winds from the red giant, something that doesn't appear in classical novae. Then, in June 2012, two separate novae were seen in gamma rays: Nova Sco 2012 and Nova Mon 2012 both emitted detectable levels of gamma rays. This exciting discovery is telling us something important about the explosions of classical novae but we don't, as yet, know what it means.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/asu-fsd073114.php){:target="_blank" rel="noopener"}


