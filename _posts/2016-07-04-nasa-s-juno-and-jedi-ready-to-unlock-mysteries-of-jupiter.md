---
layout: post
title: "NASA’s Juno and JEDI: Ready to unlock mysteries of Jupiter"
date: 2016-07-04
categories:
author: "Johns Hopkins University Applied Physics Laboratory"
tags: [JEDI,Juno (spacecraft),Jupiter,Aurora,Magnetosphere of Jupiter,Solar wind,Space science,Astronomy,Outer space,Physical sciences,Solar System,Nature,Physics,Science]
---


On board NASA's Juno spacecraft -- set to enter Jupiter orbit on July 4 -- are instruments that will help scientists answer fundamental questions not just about the solar system's largest planet but also about Earth and the universe. Mauk leads the investigation team for the Jupiter Energetic Particle Detector Instrument (JEDI), which was built by APL. A unique aspect of JEDI is its ability to simultaneously measure incoming particles from many directions, allowing JEDI to obtain nearly instantaneous snapshots of the complete particle distributions, said Dennis Haggerty, APL's instrument scientist for the JEDI investigation. The JEDI team will look at these upstream ions on the approach to Jupiter and answer a question about their origin. From what JEDI and JADE and the rest of the instruments on Juno will tell us, we should be able to understand more about the particles and radiation belts that we can observe around distant, more energetic objects like the Crab Nebula, said Mauk.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/07/160701100109.htm){:target="_blank" rel="noopener"}


