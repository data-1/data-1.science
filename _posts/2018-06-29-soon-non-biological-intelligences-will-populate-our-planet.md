---
layout: post
title: "Soon, Non-Biological Intelligences Will Populate Our Planet"
date: 2018-06-29
categories:
author: ""
tags: [Artificial intelligence,Intelligence,Artificial general intelligence,Technology,Cognitive science,Science,Branches of science]
---


One way or another, then, we are going to be sharing the planet with a lot of non-biological intelligence. Imagine that we were taking humanity into space in a fleet of giant ships. In the case of the long-term future of AI, there are reasons to be optimistic. One of the far-sighted writers who saw this coming was the great Alan Turing. We need the best of human intelligence to make the best of artificial intelligence.

<hr>

[Visit Link](https://futurism.com/soon-non-biological-intelligences-will-populate-our-planet/){:target="_blank" rel="noopener"}


