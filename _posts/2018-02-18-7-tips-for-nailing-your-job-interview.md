---
layout: post
title: "7 tips for nailing your job interview"
date: 2018-02-18
categories:
author: "VM (Vicky) Brasseur
(Alumni)"
tags: [Research,Job interview]
---


Prepare questions  When an interviewer asks, Do you have any questions for me? any answer that isn't Yes! Before you start an interview, make sure to have several questions prepared. Ask questions  Speaking of being engaged, don't feel you have to save all your questions for when your interviewer asks for them. You say, I don't know.

<hr>

[Visit Link](https://opensource.com/article/17/11/7-job-interview-tips){:target="_blank" rel="noopener"}


