---
layout: post
title: "A guide to Git branching"
date: 2018-05-21
categories:
author: "Kedar Vijay Kulkarni
(Red Hat)"
tags: [Git,GitHub,Computer file,Computing,Software engineering,Software development,Software,Technology]
---


Next, run the following commands:  git status  git checkout -b myBranch  git status  The first command, git status reports you are currently on branch master , and (as you can see in the terminal screenshot below) it is up to date with origin/master , which means all the files you have on your local copy of the branch master are also present on GitHub. Since you just created myBranch , it is on the same commit as master and the remote counterpart of master , namely remotes/origin/master . git add newFile  git commit -m Adding newFile to myBranch  git push origin myBranch  In these commands, the branch in the push command is myBranch instead of master . Git is taking newFile , pushing it to your Demo repository in GitHub, and telling you it's created a new branch on GitHub that is identical to your local copy of myBranch . Delete the local copy of your branch: Since you can't delete a branch you're on, switch to the master branch (or another one you plan to keep) by running the commands shown in the terminal image below:  git branch lists the available branches; checkout changes to the master branch and git branch -D myBranch removes that branch.

<hr>

[Visit Link](https://opensource.com/article/18/5/git-branching){:target="_blank" rel="noopener"}


