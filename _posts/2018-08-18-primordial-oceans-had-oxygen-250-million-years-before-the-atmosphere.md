---
layout: post
title: "Primordial oceans had oxygen 250 million years before the atmosphere"
date: 2018-08-18
categories:
author: "University of Minnesota"
tags: [Earth,Life,American Association for the Advancement of Science,Ocean,Oxygen,University of Minnesota Duluth,University of Maryland College Park,Science,Research,Earth sciences,Physical sciences,Nature]
---


Research by a University of Minnesota Duluth (UMD) graduate student Mojtaba Fakhraee and Associate Professor Sergei Katsev has pushed a major milestone in the evolution of the Earth's environment back by about 250 million years. Our work pinpoints the time when the ocean began accumulating oxygen at levels that would substantially change the ocean's chemistry and it's about 250 million years earlier than what we knew for the atmosphere. This helps us theorize not only about early life on Earth but also about the signatures of life that we might find on other planets, said Fakhraee. The study conclusions are the result of creating a detailed computer model of chemical reactions that took place in the ocean's sediments. Researchers focused on the cycle of sulfur and simulated the patterns in which three different isotopes of sulfur could combine in ancient sedimentary rocks.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/uom-poh012418.php){:target="_blank" rel="noopener"}


