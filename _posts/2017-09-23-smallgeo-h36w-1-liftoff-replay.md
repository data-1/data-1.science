---
layout: post
title: "SmallGEO/H36W-1 liftoff replay"
date: 2017-09-23
categories:
author: ""
tags: [Guiana Space Centre,Rocketry,Flight,Spacecraft,Astronautics,Outer space,Spaceflight,Space vehicles,Space programs,European space programmes,Technology,Space industry,Spaceflight technology,European Space Agency,Space agencies,Space traffic management,Telecommunications,Aerospace,Space access,Space science]
---


The first mission to use ESA's SmallGEO platform, Hispasat 36W-1, is launched from Europe's Spaceport in in French Guiana atop a Soyuz launcher. SmallGEO is a multipurpose satellite platform capable of accommodating a wide range of commercial telecommunications payloads and missions, from TV broadcasting to multimedia applications, Internet access and mobile or fixed services in a wide range of frequency bands. Its new, modular and flexible design boosts European industry’s ability to play a significant role in commercial satcoms by easing entry into the lower-mass telecom satellite market.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2017/01/SmallGEO_H36W-1_liftoff_replay){:target="_blank" rel="noopener"}


