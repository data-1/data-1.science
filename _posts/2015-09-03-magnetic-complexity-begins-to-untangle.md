---
layout: post
title: "Magnetic complexity begins to untangle"
date: 2015-09-03
categories:
author: "$author"   
tags: [Swarm (spacecraft),Satellite,Earths magnetic field,Space weather,Earth,Magnetosphere,Planets of the Solar System,Bodies of the Solar System,Physical sciences,Astronautics,Nature,Applied and interdisciplinary physics,Solar System,Spaceflight,Science,Planetary science,Space science,Astronomy,Outer space]
---


Applications Magnetic complexity begins to untangle 22/06/2015 15407 views 120 likes  After a year in orbit, the three Swarm satellites have provided a first glimpse inside Earth and started to shed new light on the dynamics of the upper atmosphere – all the way from the ionosphere about 100 km above, through to the outer reaches of our protective magnetic shield. Swarm is the first mission to take advantage of ‘magnetic gradiometry’, which is achieved by two of the satellites orbiting side-by-side at a distance of about 100 km. The constellation provides detail on the way the field is changing and thereby weakening our protective shield. Roger Haagmans, ESA’s Swarm Mission Scientist, said, “The Swarm satellites will be in orbit for another three years at least. “New science has already emerged, for example, a joint analysis of data from Swarm and ESA’s Space Science Cluster mission has been published.”  “For the time being, the priority is to make sure that the science community can take advantage of the first results of the mission,” remarks Nils Olsen, who leads the Swarm Satellite Constellation Application and Research Facility, a consortium of European, US and Canadian institutes in charge of producing advanced models of the various sources of the geomagnetic field from Swarm data.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Swarm/Magnetic_complexity_begins_to_untangle){:target="_blank" rel="noopener"}


