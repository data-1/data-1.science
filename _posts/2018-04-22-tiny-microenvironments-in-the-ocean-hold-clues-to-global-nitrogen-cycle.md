---
layout: post
title: "Tiny microenvironments in the ocean hold clues to global nitrogen cycle"
date: 2018-04-22
categories:
author: "University Of Rochester"
tags: [Ocean,Hypoxia (environmental),Phytoplankton,Marine snow,Plankton,Nitrogen cycle,Oxygen,Microorganism,Climate change,Environmental engineering,Physical sciences,Biogeochemistry,Aquatic ecology,Nature,Earth sciences,Ecology,Applied and interdisciplinary physics,Chemistry,Hydrography,Systems ecology,Natural environment,Environmental science,Oceanography,Hydrology,Physical geography]
---


In the nitrogen cycle, phytoplankton and other marine plants turn nitrate (NO 3 ) into organic nitrogen during photosynthesis. The organic nitrogen sinks into the deep ocean, where microbes eat the organic nitrogen and use oxygen to respire and turn the nitrogen into nitrate. Using this data, they developed a computer model that changes the way we think about the marine nitrogen cycle. Credit: Thomas Weber / University of Rochester  Researchers previously believed anaerobic microbes—small microorganisms and bacteria that do not need oxygen to respire—were only found in pockets of the ocean with exceptionally low oxygen levels; particularly, three regions known as dead zones. Whenever oxygen is available, there should not be organisms that respire anaerobically, Weber says.

<hr>

[Visit Link](https://phys.org/news/2018-04-tiny-microenvironments-ocean-clues-global.html){:target="_blank" rel="noopener"}


