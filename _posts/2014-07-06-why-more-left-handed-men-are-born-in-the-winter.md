---
layout: post
title: "Why More Left-Handed Men are Born in the Winter"
date: 2014-07-06
categories:
author: Science World Report
tags: [Handedness,Behavioural sciences,Cognitive science,Neuroscience,Psychology,Cognition,Interdisciplinary subfields,Psychological concepts,Brain]
---


Are you left-handed or right-handed? Around 90 percent of the general population is right-handed, and most tools that are sold are optimized for people who work primarily with their right hands. Yet what particularly surprised the researchers wasn't the left-handedness, but rather when the people were born. We were surprised to see this imbalance was caused by more left-handed men being born specifically during November, December and January, said Ulrich Tran, the lead author of the new study, in a news release. Presumably, the relative darkness during the period November to January is not directly connected to this birth seasonality of handedness, said Tran.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15836/20140704/why-more-left-handed-men-born-winter.htm){:target="_blank" rel="noopener"}


