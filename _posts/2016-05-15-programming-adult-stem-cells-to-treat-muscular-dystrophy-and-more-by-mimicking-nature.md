---
layout: post
title: "Programming adult stem cells to treat muscular dystrophy and more by mimicking nature"
date: 2016-05-15
categories:
author: American Chemical Society
tags: [Stem cell,Skeletal muscle,Adult stem cell,Human body,Muscular dystrophy,Clinical medicine,Diseases and disorders,Medical specialties,Causes of death,Anatomy,Biology,Human diseases and disorders,Life sciences,Cell biology]
---


Stem cells hold great potential for addressing a variety of conditions from spinal cord injuries to cancer, but they can be difficult to control. Scientists are now reporting in the journal ACS Nano a new way to mimic the body's natural approach to programming these cells. In the 1990s, scientists first isolated human embryonic stem cells, which can turn into any kind of cell in the body, and the promise of a new way to treat diseases emerged. Since then, scientists have also discovered adult stem cells in a number of organs in the body, including the brain, lungs and skin, that can turn into a limited number of cell types. One of the main obstacles has been figuring out how to control them.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/acs-pas072215.php){:target="_blank" rel="noopener"}


