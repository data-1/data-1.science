---
layout: post
title: "Finger and toe fossils belonged to tiny primates 45 million years ago"
date: 2018-07-19
categories:
author: "Northern Illinois University"
tags: [Primate,Fossil,Paleontology,Eosimias,Finger,Taxa]
---


The nearly 500 finger and toe bones belonged to tiny early primates--some half the size of a mouse. The new study provides further evidence that early anthropoids were minuscule creatures, the size of a mouse or smaller, Gebo said. In more recent years, Gebo found additional specimens, sifting through miscellaneous elements from Shanghuang both at the Carnegie Museum and the University of Kansas. We can actually identify different types of primates from the shapes of their fingers and toes, Gebo said. We can see evolution occurring at this site, from the broader finger or toe tips to more narrow.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/niu-fat110917.php){:target="_blank" rel="noopener"}


