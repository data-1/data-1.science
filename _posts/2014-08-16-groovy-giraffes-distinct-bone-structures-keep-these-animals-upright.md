---
layout: post
title: "Groovy giraffes…distinct bone structures keep these animals upright"
date: 2014-08-16
categories:
author: Society for Experimental Biology 
tags: [American Association for the Advancement of Science,Branches of science,Learned societies of the United States,Scientific societies,Science,Science and technology,Scientific organizations,Scientific supraorganizations]
---


Researchers at the Royal Veterinary College have identified a highly specialised ligament structure that is thought to prevent giraffes' legs from collapsing under the immense weight of these animals. This means their leg bones are under high levels of mechanical stress. In giraffes, the equivalents to our metatarsal bone (in the foot) and metacarpal bone (in the hand) are extremely elongated, accounting for roughly half the leg length. The researchers hypothesised that this arrangement may help solve the mystery of how the giraffes' spindly legs can support its weight. As the suspensory ligament is elastic tissue, and not muscle, it cannot generate force itself so can only offer passive support.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/sfeb-ggd070314.php){:target="_blank" rel="noopener"}


