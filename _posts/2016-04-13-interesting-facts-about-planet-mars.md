---
layout: post
title: "Interesting facts about planet Mars"
date: 2016-04-13
categories:
author: Elizabeth Howell
tags: [Mars,Mariner 9,Phobos (moon),NASA,Solar System,Martian meteorite,Mariner program,Atmosphere,Astronomy,Bodies of the Solar System,Terrestrial planets,Planets,Astronomical objects,Astronomical objects known since antiquity,Planetary science,Outer space,Planets of the Solar System,Space science]
---


The Planet Mars. For water to flow in the past, the Red Planet needs more atmosphere. But in the life of the Solar System, Phobos has a pretty short lifetime. Mars has methane in its atmosphere, but we don't know how much. Explore further NASA spacecraft completes 40,000 Mars orbits

<hr>

[Visit Link](http://phys.org/news343298489.html){:target="_blank" rel="noopener"}


