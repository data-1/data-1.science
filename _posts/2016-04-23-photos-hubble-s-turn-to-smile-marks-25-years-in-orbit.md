---
layout: post
title: "PHOTOS: Hubble's turn to smile, marks 25 years in orbit"
date: 2016-04-23
categories:
author: Bymarcia Dunn
tags: [Hubble Space Telescope,Milky Way,Star,Nebula,NASA,Spiral galaxy,Space telescope,Space Telescope Science Institute,European Space Agency,Star cluster,Space Shuttle,Outer space,Physical sciences,Sky,Astronomical objects,Space science,Astronomy]
---


(NASA, ESA, Allison Loll/Jeff Hester - Arizona State University via AP)  This image made by the NASA/ESA Hubble Space Telescope shows the Orion Nebula and the proceess of star formation, from the massive, young stars that are shaping the nebula to the pillars of dense gas that may be the homes of budding stars. (NASA, ESA, Hubble Heritage STScI/AURA-ESA/Hubble Collaboration via AP)  This image made by the NASA/ESA Hubble Space Telescope shows the star cluster Pismis 24 in the core of the large emission nebula NGC 6357. (NASA, ESA, Hubble Heritage (STScI/AURA)-ESA/Hubble Collaboration via AP)  This February 2010 image made by the NASA/ESA Hubble Space Telescope shows a three-light-year-tall pillar of gas and dust in the Carina Nebula which is being eaten away by the light from nearby bright stars. It is a relatively nearby spiral galaxy, a little over 20 million light-years away. (NASA/ESA/Hubble Heritage Team (STScI/AURA) via AP)  This image made by the NASA/ESA Hubble Space Telescope shows the barred spiral galaxy NGC 1300.

<hr>

[Visit Link](http://phys.org/news348921689.html){:target="_blank" rel="noopener"}


