---
layout: post
title: "LightSail's solar sails look good in latest deployment"
date: 2015-06-28
categories:
author: Nancy Owano
tags: [Solar sail,Spaceflight,Outer space,Space science,Astronautics,Flight,Spacecraft,Astronomy,Space vehicles,Spaceflight technology,Aerospace,Sky,Science,Solar System]
---


LightSail captured this image of its deployed solar sails in Earth orbit on June 8, 2015. Credit: The Planetary Society  The Planetary Society solar sail exploration called LightSail is looking good. In measuring mission success, observers were looking to the deployment of LightSail's Mylar solar sails. Davis said that NASA and The Planetary Society are sharing data on the LightSail mission through a Space Act Agreement. They work by reflecting photons from the sun, providing a small thrust in the opposite direction. Explore further LightSail team prepares for tests of mylar space wonder  © 2015 Phys.org

<hr>

[Visit Link](http://phys.org/news353095529.html){:target="_blank" rel="noopener"}


