---
layout: post
title: "Geckos use toe hairs to turn stickiness on/off"
date: 2015-10-29
categories:
author: Oregon State University
tags: [Gecko,Adhesion,Technology]
---


Geckos' feet are nonsticky by default, but they can activate 'stickiness' through application of a small shear force, according to research from Oregon State University. These extraordinary hairs contribute to the ability of geckos to run, evade predators, and protect its very life and survival. In essence, a gecko never has a bad hair day. These are really fascinating nanoscale systems and forces at work, said Alex Greaney, an assistant professor in the OSU College of Engineering. The adhesion of geckos' feet is made possible by millions of tiny 'seta' that function at the nanoscale to turn stickiness on and off.

<hr>

[Visit Link](http://phys.org/news327058258.html){:target="_blank" rel="noopener"}


