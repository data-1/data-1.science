---
layout: post
title: "WSU Spokane researchers isolate smallest unit of sleep to date"
date: 2015-12-11
categories:
author: Washington State University
tags: [Sleep,Electroencephalography,Nervous system,Brain,Neuron,Tumor necrosis factor,Glia,Physiology,Neuroscience]
---


The study - the first to document that sleep originates in small neural networks - opens the door to deeper understanding of the genetic, molecular and electrical aspects underlying sleep disorders. WSU Regents professor James Krueger and doctoral student Kathryn Jewett cultured neurons and glial cells that matured over two weeks into active neural networks exhibiting some of the same EEG sleep patterns seen in the brains of animals. Krueger said the normal state of cultured neurons is sleep-like. When the researchers prolonged electrical stimulation, the cells responded with a miniature version of sleep homeostasis seen in animals; that is, after their extra activity, the nerve cells slept in the next day. Before, people viewed sleep as a whole-brain phenomenon - using theories that often invoke, a miracle occurs and then you go to sleep.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/wsu-wsr060815.php){:target="_blank" rel="noopener"}


