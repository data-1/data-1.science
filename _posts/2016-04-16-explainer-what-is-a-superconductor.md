---
layout: post
title: "Explainer: What is a superconductor?"
date: 2016-04-16
categories:
author: Michael Sutherland, The Conversation
tags: [Superconductivity,Electron,Electric current,Yttrium barium copper oxide,Physical sciences,Materials,Physics,Electricity,Applied and interdisciplinary physics,Electromagnetism,Chemistry,Electrical engineering,Technology]
---


Credit: Shutterstock  Materials can be divided into two categories based on their ability to conduct electricity. Paired electrons on the other hand are given a priority pass to travel in the fast lane through a material, able to avoid congestion. Credit: Shutterstock  One interesting and potentially useful property of superconductors arises when they are placed near a strong magnet. The magnetic field causes electrical currents to spontaneously flow on the surface of a superconductor, which then give rise to their own, counteracting, magnetic field. The fact that superconductors will levitate above a strong magnet also creates possibilities for efficient, ultra-high speed trains that float above a magnetic track, much like Marty McFly's hoverboard in Back to the Future.

<hr>

[Visit Link](http://phys.org/news344762855.html){:target="_blank" rel="noopener"}


