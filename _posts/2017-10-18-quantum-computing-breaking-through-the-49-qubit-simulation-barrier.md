---
layout: post
title: "Quantum computing—breaking through the 49 qubit simulation barrier"
date: 2017-10-18
categories:
author: Edwin Pednault
tags: [Quantum computing,Quantum supremacy,Computing,Simulation,Computer,Applied mathematics,Information Age,Science,Theoretical computer science,Branches of science,Computers,Computer science,Technology,Computer engineering]
---


That insight immediately led to the idea of pulling a grid circuit apart into individual bristle brushes, one for each qubit, then computing the corresponding tensors, and finally combining the tensors for each qubit to calculate the quantum amplitudes for the overall circuit. This possibility completely changes the economics of full simulations, enabling quantum circuits to be simulated that were previously thought to be impossible to simulate. For this simulation, the calculations were divided into 2^11 slices with 2^38 amplitudes calculated per slice; 4.5 Terabytes were required to hold the tensor values. They will also facilitate the development and debugging of short–depth algorithms for problems where quantum computing has the potential to provide real advantage over conventional approaches. Depending on the particular kind of application, we will need physical quantum computers to perform computations that will either require too much memory, or too much processing power to be economically performed on classical computers.

<hr>

[Visit Link](https://phys.org/news/2017-10-quantum-computingbreaking-qubit-simulation-barrier.html){:target="_blank" rel="noopener"}


