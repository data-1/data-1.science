---
layout: post
title: "Star with different internal driving force than the sun"
date: 2016-05-06
categories:
author: University of Copenhagen - Niels Bohr Institute
tags: [Sunspot,Star,Sun,Telescope,Magnetic field,Outer space,Stellar astronomy,Physics,Astronomical objects,Physical sciences,Space science,Astronomy,Physical phenomena,Bodies of the Solar System]
---


Sunspots are cool areas caused by the strong magnetic fields where the flow of heat is slowed. On our star, the Sun, the sunspots are seen in a belt around the equator, but now scientists have observed a large, distant star where sunspots are located near the poles. Astronomers have previously seen sunspots on Zeta Andromeda using the Doppler method, which means that you observe that light wavelengths of the rotating star. It is the rapid rotation that creates a different and very strong magnetic field. These storms result in very strong northern lights and can also cause problems for orbiting satellites and the power grid on Earth.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/uoc--swd050316.php){:target="_blank" rel="noopener"}


