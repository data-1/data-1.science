---
layout: post
title: "Simple detection of magnetic skyrmions"
date: 2015-10-08
categories:
author: Christian-Albrechts-Universitaet zu Kiel
tags: [News aggregator,Electromagnetism,Electrical engineering,Technology,Applied and interdisciplinary physics,Physics,Chemistry,Physical sciences,Electricity,Materials science,Science]
---


At present, tiny magnetic whirls -- so called skyrmions -- are discussed as promising candidates for bits in future robust and compact data storage devices. Now researchers from the University of Hamburg and the Christian-Albrechts-Universität in Kiel have demonstrated that skyrmions can be detected much more easily because of a drastic change of the electrical resistance in these magnetic whirls. The discovery of such skyrmions in thin magnetic films and multilayers, already used in today's technology, and the possibility to move these skyrmions at very low electrical current densities, has opened the perspective to use them as bits in novel data storage devices. Employing a scanning tunneling microscope researchers of the University of Hamburg were now able to demonstrate that the resistance changes also when a non-magnetic metal is used in such a measurement. When the electrons are travelling through a magnetic whirl, they feel the canting between the atomic magnets, leading to a local resistance change of the material.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151006085356.htm){:target="_blank" rel="noopener"}


