---
layout: post
title: "The thermodynamic cost of driving quantum systems by their boundaries"
date: 2015-10-08
categories:
author: Barra, Departamento De Física, Facultad De Ciencias Físicas Y Matemáticas, Universidad De Chile, Santiago, Felipe Barra, You Can Also Search For This Author In, Author Information
tags: [Lindbladian,Entropy,Heat,Open quantum system,Thermodynamic system,Thermodynamics,Quantum thermodynamics,Entropy production,Thermodynamic equilibrium,Non-equilibrium thermodynamics,Applied mathematics,Mathematical physics,Science,Mathematics,Systems theory,Mechanics,Physical sciences,Applied and interdisciplinary physics,Scientific method,Continuum mechanics,Theoretical physics,Scientific theories,Physical chemistry,Quantum mechanics,Physics]
---


When this equation is obtained from the weak coupling limit for a time independent system, one finds global Lindblad operators that are eigen-operator of the Hamiltonian H S 30. For system passively and weakly coupled to the heat-baths, these quantities are defined as  In section Methods: Heat from a given reservoir in the weak-coupling limit we justify these definitions. For arbitrary strength coupling between the system and environment31, the internal energy is defined by and the first law relates its changes to work and heat with the work performed on the system in the time interval [0, t], which is also given by  and the total heat flow split in reservoir contributions  given by minus the change in energy of the r-reservoir. In the weak-coupling limit and assuming that the open system satisfies a Lindblad equation obtained from the Born-Markov-secular approximation30, the rate of entropy production and the above expressions for work and heat take the standard form given in Eq. Thus to obtain the appropriate expressions for the thermodynamical quantities in boundary driven systems we apply in the next section the previous formulation, in particular Eqs (4),(5),(6), to a system plus environment evolving unitarily in which the reduced density matrix for the system satisfy a boundary driven Lindblad equation in an exact limit.

<hr>

[Visit Link](http://www.nature.com/articles/srep14873){:target="_blank" rel="noopener"}


