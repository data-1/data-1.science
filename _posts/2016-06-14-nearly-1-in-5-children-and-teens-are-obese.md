---
layout: post
title: "Nearly 1 in 5 Children and Teens Are Obese"
date: 2016-06-14
categories:
author: "Sara G. Miller"
tags: [Obesity,Child,Childhood obesity,Adolescence,Preventive healthcare,Adult,Health,Human development,Health sciences,Human life stages,Nutrition,Determinants of health,Childhood]
---


The report, released today (Aug. 25) by researchers at the Centers for Disease Control and Prevention, shows that 17.5 percent of children and adolescents ages 3 to 19 are now obese. The overall obesity rate may not have changed, but that misses what's happening in certain populations — the rate is actually dropping in some groups, but going up in others, Goran told Live Science. A major factor contributing to the high prevalence of childhood obesity is sugar in people's diets. In low-income populations especially, sugary foods tend to be introduced into children's diets very early in their lives, Goran said. Studies have shown that children who are obese at a young age are significantly more likely to become obese adults according to the report.

<hr>

[Visit Link](http://www.livescience.com/51973-childhood-obesity-united-states.html){:target="_blank" rel="noopener"}


