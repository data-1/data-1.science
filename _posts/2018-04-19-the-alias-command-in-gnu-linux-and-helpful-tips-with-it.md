---
layout: post
title: "The Alias command in GNU/Linux and helpful tips with it"
date: 2018-04-19
categories:
author: ""
tags: [Aliasing (computing),Computer file,Shortcut (computing),Sudo,Software,Technology,Information Age,Digital media,Computers,Computer engineering,System software,Software engineering,Software development,Information technology management,Computing,Computer science,Computer architecture]
---


I did not add sudo to this, purposely, so at least system files canâ€™t be deleted through this method, but caution must still be used. Tip: Type alias to display the list of aliases set on the Linux machine. Here are some other aliases that you might find useful! alias home='cd /home/martin/' -- Switch to the /home/martin/ directory when you type the alias. Last thoughts  ADVERTISEMENT  Aliases can make things so much faster when working with the command line, but always be careful not to set aliases up that could destroy your system with a simple keystroke, like a=sudo rm -rf *as this runs far too many risks.

<hr>

[Visit Link](https://www.ghacks.net/2018/03/06/the-alias-command-in-gnu-linux-and-helpful-tips-with-it/){:target="_blank" rel="noopener"}


