---
layout: post
title: "Georgia Tech researchers demonstrate how the brain can handle so much data"
date: 2015-12-21
categories:
author: Georgia Institute of Technology
tags: [Machine learning,Neural network,Research,Computer vision,Theory,American Association for the Advancement of Science,Data,Cognitive psychology,Learning,Science,Academic discipline interactions,Technology,Neuroscience,Interdisciplinary subfields,Cognitive science,Branches of science,Cognition]
---


And can such techniques be computationally replicated to improve computer vision, machine learning or robotic performance? Researchers at Georgia Tech discovered that humans can categorize data using less than 1 percent of the original information, and validated an algorithm to explain human learning -- a method that also can be used for machine learning, data analysis and computer vision. Next, researchers tested a computational algorithm to allow machines (very simple neural networks) to complete the same tests. Humans and machines performed equally, demonstrating that indeed one can predict which data will be hardest to learn over time. We were surprised by how close the performance was between extremely simple neural networks and humans, Vempala said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/giot-gtr121515.php){:target="_blank" rel="noopener"}


