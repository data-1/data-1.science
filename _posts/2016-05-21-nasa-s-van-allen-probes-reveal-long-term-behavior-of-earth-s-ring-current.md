---
layout: post
title: "NASA's Van Allen Probes reveal long-term behavior of Earth's ring current"
date: 2016-05-21
categories:
author: NASA/Goddard Space Flight Center
tags: [Van Allen radiation belt,Van Allen Probes,Physical sciences,Physics,Science,Nature,Spaceflight,Electromagnetism,Outer space,Astronomy,Space science]
---


Using data gathered by the Radiation Belt Storm Probes Ion Composition Experiment, or RBSPICE, on one of the Van Allen Probes, researchers have determined that the high-energy protons in the ring current change in a completely different way from the current's low-energy protons. It also modifies the magnetic field in near-Earth space, which in turn controls the motion of the radiation belt particles that surround our planet. After looking at one year of continuous ion data it became clear to us that there is a substantial, persistent ring current around the Earth even during non-storm times, which is carried by high-energy protons. During geomagnetic storms, the enhancement of the ring current is due to new, low-energy protons entering the near-Earth region. ###  The Johns Hopkins Applied Physics Laboratory in Laurel, Maryland, built and operates the Van Allen Probes for NASA's Science Mission Directorate.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/nsfc-nva051916.php){:target="_blank" rel="noopener"}


