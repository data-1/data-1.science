---
layout: post
title: "Real-world intercontinental quantum communications enabled by the Micius satellite"
date: 2018-07-03
categories:
author: "University of Science and Technology of China"
tags: [Quantum Experiments at Space Scale,Quantum network,Quantum key distribution,Computer science,Quantum information science,Science,Applied mathematics,Telecommunications,Theoretical physics,Quantum mechanics,Technology,Physics]
---


Five ground stations are built in China to cooperate with the Micius satellite, located in Xinglong (near Beijing), Nanshan (near Urumqi), Delingha (37°22'44.43''N, 97°43'37.01E), Lijiang (26°41'38.15''N, 100°1'45.55''E), and Ngari in Tibet (32°19'30.07''N, 80°1'34.18''E). For example, the Xinglong station has now been connected to the metropolitan multi-node quantum network in Beijing via optical fibers. The Micius satellite can be further exploited as a trustful relay to conveniently connect any two points on Earth for high-security key exchange. To further demonstrate the Micius satellite as a robust platform for quantum key distribution with different ground stations on Earth, QKD from the Micius satellite to Graz ground station near Vienna has also been performed successfully this June in collaboration with Professor Anton Zeilinger of Austrian Academy of Sciences. This work points towards an efficient solution for an ultra-long-distance global quantum network.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/uosa-riq011718.php){:target="_blank" rel="noopener"}


