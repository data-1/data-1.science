---
layout: post
title: "Quantum twisted Loong confirms the physical reality of wavefunctions"
date: 2017-09-21
categories:
author: "Science China Press"
tags: [Double-slit experiment,Quantum mechanics,Photon,Wave function,Physics,Theoretical physics,Science,Scientific theories,Physical sciences,Scientific method,Applied and interdisciplinary physics]
---


Bao-Sen Shi's research group from the University of Science and Technology of China collaborated with Prof. Zhi-Han Zhu from Harbin University of Science and Technology and other collaborators to propose and demonstrate a quantum twisted double-slit experiment, in which photonics orbital angular momentum (OAM) and its group velocity slowing-down feature are employed to extract photons' propagation history after detections. This finding clarifies the long-held misunderstanding of the role of wavefunctions and their collapses in the evolution of quantum entities. In addition, they present a cartoon titled Quantum Twisted Loong, which is shown in the Cover and Fig. ###  This work is supported by the National Natural Science Funds for Distinguished Young Scholar of China (Grant No. Quantum twisted double-slits experiments: confirming wavefunctions' physical reality, Science Bulletin 62 (2017) 1185-1192 http://www.sciencedirect.com/science/article/pii/S2095927317304358

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-09/scp-qtl091917.php){:target="_blank" rel="noopener"}


