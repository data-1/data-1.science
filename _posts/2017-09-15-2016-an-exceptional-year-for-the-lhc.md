---
layout: post
title: "2016—an exceptional year for the LHC"
date: 2017-09-15
categories:
author: ""
tags: [Large Hadron Collider,ATLAS experiment,Particle accelerator,Compact Muon Solenoid,CERN,ALICE experiment,Collider,LHCb experiment,Physics,Particle physics,Science,Applied and interdisciplinary physics,Experimental physics,Particle physics facilities,Accelerator physics]
---


The number of collisions recorded by ATLAS and CMS during the proton run from April to the end of October was 60% higher than anticipated. The performance is even more remarkable considering that colliding protons with lead ions, which have a mass 206 times greater and a charge 82 times higher, requires numerous painstaking adjustments to the machine. An event recorded by the CMS experiment during the LHC’s proton-lead ion run for which no fewer than 449 particles tracks were reconstructed. A proton-lead ion collision recorded by the LHCb experiment in the last few days of the LHC’s 2016 run. Credit: LHCb  The integrated luminosity of the LHC with proton-proton collisions in 2016 compared to previous years.

<hr>

[Visit Link](http://phys.org/news/2016-12-2016an-exceptional-year-lhc.html){:target="_blank" rel="noopener"}


