---
layout: post
title: "Landing site recommended for ExoMars 2018"
date: 2015-10-22
categories:
author: "$author"  
tags: [ExoMars,Mars,Schiaparelli EDM,Discovery and exploration of the Solar System,Flight,Bodies of the Solar System,Planets,Astronautics,Solar System,Terrestrial planets,Spaceflight,Astronomical objects known since antiquity,Astronomy,Outer space,Planets of the Solar System,Planetary science,Space science]
---


Oxia Planum has been recommended as the primary candidate for the landing site of the ExoMars 2018 mission. The main goal for the rover is to search for evidence of martian life, past or present, in an area with ancient rocks where liquid water was once abundant. Taking into account these requirements and the individual science cases put forward for each site, the Landing Site Selection Working Group today recommended that Oxia Planum be the primary focus for further detailed evaluation for the 2018 mission. A period of volcanic activity may have covered early clays and other aqueous deposits, offering preservation for biosignatures against the planet's harsh radiation and oxidation environment, and have only been exposed by erosion within the last few hundred million years. “It made for a challenging decision today, given the quality of the cases for all sites, but we are looking forward to the next stage of analysis as we move closer to the launch of our exciting mission: our rover will search for molecular biosignatures in the subsurface for the very first time.”  Selection of the final landing site by ESA and Roscosmos is planned to occur six months before launch.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Landing_site_recommended_for_ExoMars_2018){:target="_blank" rel="noopener"}


