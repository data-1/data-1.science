---
layout: post
title: "Three leaders in longevity biotechnology form a research collaboration"
date: 2018-08-15
categories:
author: "InSilico Medicine"
tags: [Insilico Medicine,Ageing,Artificial intelligence,Drug discovery,Health,Drug development,Technology,Health sciences,Life sciences,Health care,Medicine]
---


The collaboration will utilize an end-to-end machine learning system to identify the molecular targets and generate the novel compounds. The Verdin lab will collaborate with Napa, using Insilico's drug development engine to speed the discovery of new compounds. Napa Therapeutics lets Juvenescence deepen our collaboration with the Buck Institute and with Insilico Medicine. Learn more at: https://buckinstitute.org  About Juvenescence, Ltd.  Juvenescence Limited is a company focused on developing therapies and drugs to increase human longevity and complementary investments in related sectors. The company and its scientists are dedicated to extending human productive longevity and transforming every step of the drug discovery and drug development process through excellence in biomarker discovery, drug development, digital medicine and aging research.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/imi-tli081318.php){:target="_blank" rel="noopener"}


