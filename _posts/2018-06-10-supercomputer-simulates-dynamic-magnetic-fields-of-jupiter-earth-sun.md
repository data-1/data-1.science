---
layout: post
title: "Supercomputer simulates dynamic magnetic fields of Jupiter, Earth, Sun"
date: 2018-06-10
categories:
author: "Becky Oskin, Uc Davis"
tags: [Dynamo theory,Jupiter,Juno (spacecraft),Planet,Geophysics,Earths magnetic field,Supercomputer,Space science,Astronomy,Science,Planetary science,Physical sciences]
---


Only a supercomputer can help get us under that lid. Computational Infrastructure for Geodynamics is headquartered at UC Davis. The CIG describes itself as a community organization of scientists that disseminates software for geophysics and related fields. The working group was awarded 260 million core hours on the Mira supercomputer at the U.S. Department of Energy's Argonne National Laboratory – rated the sixth-fastest in the world—to model magnetic fields inside the Earth, Sun and Jupiter. Credit: UC Davis  Explore further The inner secrets of planets and stars

<hr>

[Visit Link](https://phys.org/news/2017-11-supercomputer-simulates-dynamic-magnetic-fields.html){:target="_blank" rel="noopener"}


