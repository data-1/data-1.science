---
layout: post
title: "3Q: A bold mission to touch the sun"
date: 2018-08-14
categories:
author: "Jennifer Chu"
tags: [Sun,Solar wind,Parker Solar Probe,Voyager program,Nature,Astronomical objects known since antiquity,Astronomical objects,Physics,Astronautics,Sky,Physical sciences,Bodies of the Solar System,Spaceflight,Solar System,Outer space,Space science,Astronomy]
---


The probe will orbit the blistering corona, withstanding unprecedented levels of radiation and heat, in order to beam back to Earth data on the sun’s activity. A: The spacecraft will come as close as 3.9 million miles to the sun, well within the orbit of Mercury and more than seven times closer than any spacecraft has come before. The acceleration of the solar wind is still an outstanding question, mostly because all of the acceleration is over by [the time the wind has traveled] 25 solar radii. The SWEAP Investigation is the set of instruments on the spacecraft that will directly measure the properties of the plasma in the solar atmosphere during these encounters. At the same time the solar probe Faraday cup is measuring the properties of the solar wind close to the sun at 8 solar radii, a sister Faraday cup on Voyager (launched in 1977) will probably be measuring plasma in the local interstellar space, totally outside the solar atmosphere, beyond 100 astronomical units, or 20,000 solar radii.

<hr>

[Visit Link](http://news.mit.edu/2018/3q-john-belcher-parker-solar-probe-0813){:target="_blank" rel="noopener"}


