---
layout: post
title: "Using mathematical modeling and evolutionary principles important in treatment decisions: Moffitt researchers show continuous maximum dose and adaptive treatment approaches are effective strategies fo"
date: 2018-08-03
categories:
author: "H. Lee Moffitt Cancer Center & Research Institute"
tags: [Cancer,Treatment of cancer,Therapy,Therapeutic index,Dose (biochemistry),Neoplasm,Evolution,Clinical medicine,Medicine,Health,Neoplasms,Diseases and disorders,Medical specialties,Health sciences,Causes of death,Health care,Epidemiology]
---


Researchers at Moffitt Cancer Center are using mathematical modeling based on evolutionary principles to show that adaptive drug treatments based on tumor responses to prior treatment are more effective than maximum-tolerated dose approaches for certain tumor situations. It is now clear that cancer cells can be insensitive even to treatment that they have never seen before, explained study author Jill Gallaher, Ph.D., an applied research scientist in the Department of Integrated Mathematical Oncology at Moffitt. The researchers discovered that in general, there is no single treatment approach that works best for all tumors, and that the ability of a tumor to respond to a particular treatment depends on the composition of the tumor. However, tumors that are made up of a mixture of sensitive and resistant cells tend to respond better to an adaptive treatment approach. They compared a typical adaptive strategy with a treatment vacation strategy during which no treatment is given in between high drug doses.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180430131809.htm){:target="_blank" rel="noopener"}


