---
layout: post
title: "Engineers develop a new biosensor chip for detecting DNA mutations"
date: 2016-06-14
categories:
author: "University Of California - San Diego"
tags: [Single-nucleotide polymorphism,DNA,Complementarity (molecular biology),DNA sequencing,Biosensor,Hybridization probe,Technology,Molecular biology,Chemistry,Nucleic acids,Biochemistry,Genetics,Biology,Biotechnology,Life sciences]
---


The biosensor chip—consisting of a double stranded DNA probe embedded onto a graphene transistor—electronically detects DNA SNPs. In this study, the DNA probe is a double helix containing two complementary DNA strands that are engineered to bind weakly to each other: a normal strand, which is attached to the graphene transistor, and a weak strand, in which four the G's in the sequence were replaced with inosines to weaken its bond to the normal strand. Credit: Lal Research Group at UC San Diego  The use of a double stranded DNA probe in the technology developed by Lal's team is another improvement over other SNP detection methods, which typically use single stranded DNA probes. With a double stranded DNA probe, only a DNA strand that's a perfect match to the normal strand is capable of displacing the weak strand. Another advantage of a double stranded DNA probe is that the probe can be longer, enabling the chip to detect an SNP within longer stretches of DNA.

<hr>

[Visit Link](http://phys.org/news/2016-06-biosensor-chip-dna-mutations.html){:target="_blank" rel="noopener"}


