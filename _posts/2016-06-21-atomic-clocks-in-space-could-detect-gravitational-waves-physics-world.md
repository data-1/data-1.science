---
layout: post
title: "Atomic clocks in space could detect gravitational waves – Physics World"
date: 2016-06-21
categories:
author: "Hamish Johnston"
tags: [Laser Interferometer Space Antenna,Gravitational wave,LIGO,Gravitational-wave observatory,Atomic clock,Electromagnetic radiation,Astronomy,Physics,Science]
---


(Courtesy: iStockphoto/hh5800)  A proposal for a gravitational-wave detector made of two space-based atomic clocks has been unveiled by physicists in the US. Just last week, a second detection was announced by LIGO from a different black-hole merger. The atoms are trapped within a 1D optical lattice that is a standing wave created by reflecting laser light from a mirror. In the proposal, this motion will be detected by using the atomic clock in one satellite – called “A” – to measure the frequency of its outgoing laser light. The atomic clock at satellite B will then measure the frequency of the incoming laser light from A.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2016/jun/20/atomic-clocks-in-space-could-detect-gravitational-waves){:target="_blank" rel="noopener"}


