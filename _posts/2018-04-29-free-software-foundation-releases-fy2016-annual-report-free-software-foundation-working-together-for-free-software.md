---
layout: post
title: "Free Software Foundation releases FY2016 Annual Report — Free Software Foundation — Working together for free software"
date: 2018-04-29
categories:
author: ""
tags: [Free Software Foundation,Free software,Open content,Computer science,Applied ethics,Free goods and services,Digital media,Criticism of intellectual property,Information technology,Intellectual property activism,Technology,Computing,Software,Software development,Open-source movement,Software engineering,Free content,Intellectual works]
---


BOSTON, Massachusetts, USA -- Wednesday, February 28, 2018 -- The Free Software Foundation (FSF) today published its Fiscal Year (FY) 2016 Annual Report. The report is available in low-resolution (11.5 MB PDF) and high-resolution (207.2 MB PDF). It is the result of a full external financial audit, along with a focused study of program results. About the Free Software Foundation  The Free Software Foundation, founded in 1985, is dedicated to promoting computer users' right to use, study, copy, modify, and redistribute computer programs. Donations to support the FSF's work can be made at https://my.fsf.org/donate.

<hr>

[Visit Link](http://www.fsf.org/news/free-software-foundation-releases-fy2016-annual-report){:target="_blank" rel="noopener"}


