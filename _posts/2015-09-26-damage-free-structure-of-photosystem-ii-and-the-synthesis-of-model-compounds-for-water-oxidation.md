---
layout: post
title: "Damage-free structure of photosystem II and the synthesis of model compounds for water-oxidation"
date: 2015-09-26
categories:
author: Okayama University
tags: [Photosystem II,Photosystem,Photosynthesis,Water splitting,Physical sciences,Chemistry]
---


PSII is an extremely large membrane protein complex consisting of 20 subunits with a total molecular mass of 700 kDa for a dimer. However, due to the intense and continuous SR X-rays, the Mn 4 CaO 5 -cluster has been suggested to suffer from radiation damage, leading to slight changes in the inter-atomic distances within the cluster. Now, a joint team led by Jian-Ren Shen at Okayama University, and Masaki Yamamoto and Hedeo Ago at the RIKEN SPring-8 Center, has solved the damage-free crystal structure of PSII at 1.95 Å resolution using the femtosecond X-ray free electron laser (XFEL) provided by SACLA, an XFEL facility located within the same campus of SPring-8. The research group prepared a large number of highly isomorphous, high quality, large sized PSII crystals, and used the femtosecond XFEL pulses to collect diffraction data from a fresh volume of the crystals after illumination by each pulse. Explore further Exploring the structural basis for high-efficiency energy transfer in photosynthetic organisms  More information: Native structure of photosystem II at 1.95Å resolution viewed by femtosecond X-ray pulses.

<hr>

[Visit Link](http://phys.org/news/2015-09-damage-free-photosystem-ii-synthesis-compounds.html){:target="_blank" rel="noopener"}


