---
layout: post
title: "What is CI/CD?"
date: 2018-08-06
categories:
author: "Brent Laster"
tags: [Unit testing,Continuous integration,Continuous delivery,Test-driven development,Continuous testing,DevOps,Software repository,Software testing,Virtual machine,Software engineering,Software,Information technology,Computers,Product development,Computing,Software development,Technology development,Computer science,Information Age,Systems engineering,Technology,Computer engineering,Information technology management]
---


What is continuous testing? CD incorporates CI (automatically detecting source code changes, executing build processes for the changes, and running unit tests to validate), continuous testing (running various kinds of tests on the code to gain successive levels of confidence in the quality of the code), and (optionally) continuous deployment (making releases from the pipeline automatically available to users). Pipeline users can specify the versions they want to use and have the pipeline pull in those versions. DevOps is a set of ideas and recommended practices around how to make it easier for development and operational teams to work together on developing and releasing software. Pipeline-as-code is a general term for creating pipeline jobs/tasks via programming code, just as developers work with source code for products.

<hr>

[Visit Link](https://opensource.com/article/18/8/what-cicd){:target="_blank" rel="noopener"}


