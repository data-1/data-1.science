---
layout: post
title: "'Compressive sensing' provides new approach to measuring a quantum system"
date: 2014-06-30
categories:
author: University Of Rochester
tags: [Uncertainty principle,Quantum mechanics,Information,Compressed sensing,Scientific method,Scientific theories,Theoretical physics,Applied and interdisciplinary physics,Physics,Science]
---


Recovered position and momentum images showing how compressive sensing can be used to measure two conjugate variables, in this case using the university logo as an object. Compressive sensing uses the possibility of compressing the signal to be able to recover more information from relatively few measurements, and therefore obtain an understanding of the system. We use random on-off patterns to gain a small amount of position information while only minimally affecting the momentum of the photons, explains Howell, professor of physics at the University of Rochester. Although the team applied compressive sensing in this case to gain information about momentum and position, they could also have applied it other conjugate variables like time and energy for example. This requires a series of random filters (random on-off patterns) to be applied to the system, which block some of the signal but allow enough of it to pass to be able to image the Fourier transform of the object, which is effectively a strong momentum measurement.

<hr>

[Visit Link](http://phys.org/news323089137.html){:target="_blank" rel="noopener"}


