---
layout: post
title: "Scientists report first semiaquatic dinosaur, Spinosaurus"
date: 2015-07-16
categories:
author: National Geographic Society 
tags: [Spinosaurus,Dinosaur,Animals]
---


In addition, Spinosaurus will be the subject of a new exhibition at the National Geographic Museum, opening Sept. 12, as well as a National Geographic/NOVA special airing on PBS Nov. 5 at 9 p.m.  An international research team — including paleontologists Nizar Ibrahim and Paul Sereno from the University of Chicago; Cristiano Dal Sasso and Simone Maganuco from the Natural History Museum in Milan, Italy; and Samir Zouhri from the Université Hassan II Casablanca in Morocco — found that Spinosaurus developed a variety of previously unknown aquatic adaptations. This made walking on two legs on land nearly impossible, but facilitated movement in water. The sail would have been visible even when the animal entered the water. About the National Geographic Society  With a mission to inspire people to care about the planet, the 126-year-old National Geographic Society is one of the world's largest nonprofit scientific and educational organizations. In the last two decades, the discovery and study of the first dinosaurs from Italy gave the museum unprecedented international prestige.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/ngs-srf090514.php){:target="_blank" rel="noopener"}


