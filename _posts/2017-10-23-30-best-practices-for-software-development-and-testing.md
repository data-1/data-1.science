---
layout: post
title: "30 best practices for software development and testing"
date: 2017-10-23
categories:
author: "Michael Foord"
tags: [Unit testing,Software bug,Source code,API,Dependency injection,Red Hat,Code refactoring,Computer programming,Technology development,Computer science,Computers,Systems engineering,Technology,Software development,Product development,Computing,Software engineering,Information technology management,Computer engineering]
---


Development and testing best practices  1. Tests don't need testing. Test the code you write, not other people’s code. For unit tests (including test infrastructure tests) all code paths should be tested. This follows the YAGNI principle: We have specific code for the use cases we need rather than general purpose code that has complexity for things we don’t need.

<hr>

[Visit Link](https://opensource.com/article/17/5/30-best-practices-software-development-and-testing){:target="_blank" rel="noopener"}


