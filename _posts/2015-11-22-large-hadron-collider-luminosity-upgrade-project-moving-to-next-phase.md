---
layout: post
title: "Large Hadron Collider luminosity upgrade project moving to next phase"
date: 2015-11-22
categories:
author: "$author"  
tags: [Large Hadron Collider,High Luminosity Large Hadron Collider,Particle physics,CERN,Particle accelerator,Collider,Physics,Science,Applied and interdisciplinary physics]
---


This week more than 230 scientists and engineers from around the world met at CERN to discuss the High-Luminosity LHC – a major upgrade to the Large Hadron Collider (LHC) that will increase the accelerator's discovery potential from 2025. Since discoveries in particle physics rely on statistics, the greater the number of collisions, the more chances physicists have to see a particle or process that they have not seen before. The High-Luminosity LHC will increase the luminosity by a factor of 10, delivering 10 times more collisions than the LHC would do over the same period of time. The High-Luminosity LHC will use pioneering technologies - such as high field niobium-tin magnets - for the first time, said Frédérick Bordry, CERN Director for Accelerators and Technology. Explore further CERN has 2020 vision for LHC upgrade

<hr>

[Visit Link](http://phys.org/news/2015-10-large-hadron-collider-luminosity-phase.html){:target="_blank" rel="noopener"}


