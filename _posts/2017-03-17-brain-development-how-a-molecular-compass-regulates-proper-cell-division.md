---
layout: post
title: "Brain development: How a 'molecular compass' regulates proper cell division"
date: 2017-03-17
categories:
author: "Institute Of Molecular Biotechnology"
tags: [MicroRNA,Development of the nervous system,Brain,Cell (biology),Institute of Molecular Biotechnology,Cellular differentiation,Mitosis,Stem cell,Spindle apparatus,Gene expression,Gene,Biotechnology,Biology,Life sciences,Cell biology,Biological processes,Neuroscience]
---


Researchers at the Institute of Molecular Biotechnology in Vienna have unravelled how a tiny microRNA molecule controls growth and differentiation of brain cells. The newly formed cells may then go on to take a specialised function – in the brain, for example, they can become various types of neurons to generate and transmit electrical impulses – or stay stem cells that will keep dividing to generate more cells. This showed us that radial glial cells can grow relatively normally, but suggested that they are unable to further differentiate into more complex cells. miR-34/449 regulates the orientation of the mitotic spindle  The scientists at IMBA compared the gene expression patterns in cells with or without miR-34/449 and discovered a difference in the expression of a protein called JAM-A. Our findings show that in developing mouse brains, miR-34/449 regulates JAM-A to ensure the correct orientation of dividing cells and accurate formation of brain layers concludes Daniel Gerlich.

<hr>

[Visit Link](http://phys.org/news/2016-11-brain-molecular-compass-proper-cell.html){:target="_blank" rel="noopener"}


