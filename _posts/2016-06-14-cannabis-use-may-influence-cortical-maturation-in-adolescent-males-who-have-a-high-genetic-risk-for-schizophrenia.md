---
layout: post
title: "Cannabis use may influence cortical maturation in adolescent males: ...who have a high genetic risk for schizophrenia"
date: 2016-06-14
categories:
author: "Baycrest Centre for Geriatric Care"
tags: [Schizophrenia,Adolescence,Mental disorder,Risk,Cannabis (drug),News aggregator,Brain,Neuroscience,Health,Development of the nervous system,Research,Behavioural sciences]
---


Male teens who experiment with cannabis before age 16, and have a high genetic risk for schizophrenia, show a different brain development trajectory than low risk peers who use cannabis. Given the solid epidemiologic evidence supporting a link between cannabis exposure during adolescence and schizophrenia, we investigated whether the use of cannabis during early adolescence (by 16 years of age) is associated with variations in brain maturation as a function of genetic risk for schizophrenia, said senior author Tomas Paus, MD, PhD, the Anne and Max Tanenbaum Professor and Chair in Population Neuroscience at Baycrest, University of Toronto, and the Dr. John and Consuela Phelan Scholar at Child Mind Institute, New York. Researchers examined data from a total of 1,577 participants (aged 12 -- 21 years, 57% male / 43% female), that included information on cannabis use, brain imaging results, and polygenic risk score for schizophrenia. Research strongly suggests the emergence of schizophrenia is a result of both genetic and environmental factors. Our study shows the importance of understanding environmental influences on the developing brain in early life as this can have important implications for brain health through the lifespan.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150826113612.htm){:target="_blank" rel="noopener"}


