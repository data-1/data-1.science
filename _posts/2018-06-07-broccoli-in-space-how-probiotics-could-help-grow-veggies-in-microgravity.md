---
layout: post
title: "Broccoli in space—how probiotics could help grow veggies in microgravity"
date: 2018-06-07
categories:
author: "University Of Washington"
tags: [Plant,International Space Station,Microorganism,Micro-g environment,Moon,Nature]
---


A new experiment will test whether microbes can help broccoli grow better in challenging conditions in space. While a number of different vegetable growing experiments have been conducted aboard the International Space Station, this is the first that studies natural microbes to possibly help plants grow under nutrient limitations and in microgravity, he said. We want plants to grow better. Credit: Sharon Doty/University of Washington  The microbes are first encapsulated inside a coating that covers the broccoli seeds, which protects the seeds from dehydration and allows for safe dry storage before the seeds are hydrated and grown in orbit. In separate projects, Doty and her lab, along with Bubenheim and Freeman, are starting to test whether plants given natural willow and poplar microbes can grow in conditions that exist on the moon and on Mars.

<hr>

[Visit Link](https://phys.org/news/2018-05-broccoli-spacehow-probiotics-veggies-microgravity.html){:target="_blank" rel="noopener"}


