---
layout: post
title: "NJIT's new solar telescope unveils the complex dynamics of sunspots' dark cores"
date: 2016-04-26
categories:
author: New Jersey Institute of Technology
tags: [Sun,Sunspot,Astrophysics,Nature,Space science,Astronomy,Space plasmas,Physical sciences,Electromagnetism,Physical phenomena]
---


Groundbreaking images of the Sun captured by scientists at NJIT's Big Bear Solar Observatory (BBSO) give a first-ever detailed view of the interior structure of umbrae - the dark patches in the center of sunspots - revealing dynamic magnetic fields responsible for the plumes of plasma that emerge as bright dots interrupting their darkness. We would describe these plasma flows as oscillating cool jets piercing the hot atmosphere. Sunspots are formed when strong magnetic fields rise up from the convection zone, a region beneath the photosphere that transfers energy from the interior of the Sun to its surface. At the surface, the magnetic fields concentrate into bundles, which prevent the hot rising plasma from reaching the surface. About NJIT  One of the nation's leading public technological universities, New Jersey Institute of Technology (NJIT) is a top-tier research university that prepares students to become leaders in the technology-dependent economy of the 21st century.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/njio-nns042915.php){:target="_blank" rel="noopener"}


