---
layout: post
title: "How physics explains the evolution of social organization"
date: 2018-08-06
categories:
author: "Duke University"
tags: [Evolution,Adrian Bejan,Water,Resource,System,Heat,Hierarchy,Water heating,Society,Nature,Branches of science,Science]
---


DURHAM, N.C. -- A scientist at Duke University says the natural evolution of social organizations into larger and more complex communities that exhibit distinct hierarchies can be predicted from the same law of physics that gives rise to tree branches and river deltas. With his newest paper, Bejan adds to the list the tendency for living organisms -- most notably humans -- to organize themselves into increasingly large and complex societies to have access to and make better use of finite resources. As communities grew, those living on the fringes would either have to walk further to get hot water or build longer pipes, leading to wasted energy and heat loss. Water systems typically have one large treatment plant with several pumping stations moving from larger to smaller pipes before finally reaching individual homes, explained Bejan. While the individual innovator may benefit greatly from the idea, the entire community also gains better access to that resource, which serves to reduce overall inequality.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/du-hpe062018.php){:target="_blank" rel="noopener"}


