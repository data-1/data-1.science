---
layout: post
title: "Using magnets to control chemical reactions that target release of medicines inside the body"
date: 2018-06-10
categories:
author: "Bob Yirka"
tags: [Enzyme,Nanoparticle,Biocatalysis,Substrate (chemistry),Chemical reaction,Biosensor,Physical sciences,Materials,Physical chemistry,Applied and interdisciplinary physics,Chemistry]
---


Each of the packets was then loaded with either an enzyme or a substrate meant to react with the enzyme, and, of course, the drug to be released. When the packets made their way to a site where a reaction was wanted, the researcher applied a magnet that forced them close together—close enough that they could react, releasing the drug. Magnetic field remotely controlled selective biocatalysis,(2017). Both cargos are shielded with a polymer brush structure of the nanoparticle shell, which prevents any enzyme–substrate interactions. The shield's barrier is overcome when a relatively weak (a fraction of 1 T) external magnetic field is applied and the enzyme and the substrate are merged and forced to interact in the generated nanocompartment.

<hr>

[Visit Link](https://phys.org/news/2017-11-magnets-chemical-reactions-medicines-body.html){:target="_blank" rel="noopener"}


