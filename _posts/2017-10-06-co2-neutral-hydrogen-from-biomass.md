---
layout: post
title: "CO2-neutral hydrogen from biomass"
date: 2017-10-06
categories:
author: "Vienna University Of Technology"
tags: [Iron,Biomass,Energy development,Carbon dioxide,Redox,Renewable energy,Carbon,Blast furnace,Steelmaking,Technology,Chemistry,Nature,Energy,Materials,Chemical substances]
---


From a chemical point of view, the important reactive step in the blast furnace process is the reduction of the iron ore, explains Johannes Schmid, Project Manager at TU Wien's Institute of Chemical Engineering. The whole process chain, from the production and integration of hydrogen gas from renewable sources to its use in blast furnaces, will require further research in the future. According to our measurements, in principle our production gas is a suitable additional biogenous energy source. Credit: Vienna University of Technology  Carbon dioxide exploitation: even better than CO2-neutral  The second gas flow produced in TU Wien's biomass reforming process contains CO2. The research project with voestalpine on this subject is therefore an important lighthouse project for our research group.

<hr>

[Visit Link](https://phys.org/news/2017-07-co2-neutral-hydrogen-biomass.html){:target="_blank" rel="noopener"}


