---
layout: post
title: "NASA's MESSENGER spacecraft's 10 years in space"
date: 2015-10-28
categories:
author: "$author"  
tags: [MESSENGER,Mercury (planet),Outer space,Science,Planets of the Solar System,Space science,Astronomical objects,Astronomical objects known since antiquity,Planetary science,Physical sciences,Bodies of the Solar System,Spaceflight,Solar System,Astronomy]
---


We have operated successfully in orbit for more than three Earth years and more than 14 Mercury years as we celebrate this amazing 10th anniversary milestone, said MESSENGER Mission Operations Manager Andy Calloway, of the Johns Hopkins University Applied Physics Laboratory (APL). MESSENGER is only the second spacecraft sent to Mercury. In celebration of the 10th anniversary of its launch, the MESSENGER team released this movie showing a flyover of Mercury. Over the next few months, MESSENGER will observe Mercury at lower altitudes and thus smaller spatial scales than ever before, and this is sure to result both in exciting scientific discoveries and new puzzles about our solar system's enigmatic innermost planet. The movie provides a bird's-eye view of what the spacecraft sees as it flies over the planet at close range and was assembled from 214 images taken by the narrow-angle camera (NAC) on June 8, 2014.

<hr>

[Visit Link](http://phys.org/news326436653.html){:target="_blank" rel="noopener"}


