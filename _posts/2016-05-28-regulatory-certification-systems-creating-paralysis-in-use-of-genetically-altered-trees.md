---
layout: post
title: "Regulatory, certification systems creating paralysis in use of genetically altered trees"
date: 2016-05-28
categories:
author: "Oregon State University"
tags: [Biotechnology,Plant breeding,Genetic engineering,American Association for the Advancement of Science,Forest,Natural environment,Branches of science,Nature]
---


These threats pose a real and present danger to the future of many of our forest trees, notes Steven Strauss, a distinguished professor of forest biotechnology at Oregon State University and lead author on the analysis. The forest health crisis we're facing makes it clear that regulations must change to consider catastrophic losses that could be mitigated by using advanced forest biotechnologies, including genetic engineering, Strauss said. With the precision enabled by new advances in genetic engineering - and their ability to make changes more rapidly and with less disruption to natural tree genetics than hybrid breeding methods - they can provide an important new tool. The authors point out that sustainable forest certification systems also are in need of a policy update. All major systems ban genetically engineered trees and will not certify any land as sustainable if genetically engineered trees are grown at all - even if the trees are being used solely for research or are designed to help stop a forest threat.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/osu-rcs081715.php){:target="_blank" rel="noopener"}


