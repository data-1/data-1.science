---
layout: post
title: "5 Useful Tools to Remember Linux Commands Forever"
date: 2018-06-07
categories:
author: "Aaron Kili"
tags: [Bash (Unix shell),Shell (computing),Command-line interface,Computer file,Shell script,Linux,Computer engineering,Information technology management,Computer science,Operating system technology,Digital media,Computing,Software,Technology,System software,Software engineering,Software development,Computer architecture,Computers]
---


Read Also: A – Z Linux Commands – Overview with Examples  Remembering Linux commands and their usage is not easy, especially for new Linux users. In this article, we will share 5 command-line tools for remembering Linux commands. A user can only view his/her own history file content and root can view the bash history file for all users on a Linux system. $ history  To fetch a command from bash history, press the Up arrow key continuously to search through a list of all unique commands that you run previously. $ source .bashrc  Assuming you have forgotten what the command “apropos -a” does, you can use explain command to help you remember it, as shown.

<hr>

[Visit Link](https://www.tecmint.com/remember-linux-commands/){:target="_blank" rel="noopener"}


