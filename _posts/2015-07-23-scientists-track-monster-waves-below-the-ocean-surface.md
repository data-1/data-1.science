---
layout: post
title: "Scientists track monster waves below the ocean surface"
date: 2015-07-23
categories:
author: University Of Miami
tags: [Wind wave,Ocean,Sea,Internal wave,University of Miami,Internal tide,Hydrology,Applied and interdisciplinary physics,Hydrography,Oceanography,Physical geography,Earth sciences]
---


The photo taken by Hans Graber from the R/V Knorr on 31 July 2006 at 13:46 EST during experiment in the Mid-Atlantic Bight off the New Jersey coast shows one of these internal wave packets several 10's of meters below the surface approaching the ship. University of Miami (UM) Rosenstiel School of Marine and Atmospheric Science scientists were part of the collaborative international field study trying to understand how these waves, which rarely break the ocean surface, develop, move and dissipate underwater. The team discovered that internal waves are generated daily from internal tides, which also occur below the ocean surface, and grow larger as the water is pushed westward through the Luzon Strait into the South China Sea. They move huge volumes of heat, salt, and nutrient rich-water, which are important to fish, industrial fishing operations and the global climate. The density map covering the South China Sea from Luzon Strait (right side) the 'Generation Area' to Dongsha Island (left side) shows how prolific these internal waves occur in this region and where they are most visible.

<hr>

[Visit Link](http://phys.org/news/2015-07-scientists-track-monster-ocean-surface.html){:target="_blank" rel="noopener"}


