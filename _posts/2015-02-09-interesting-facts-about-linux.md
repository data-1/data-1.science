---
layout: post
title: "Interesting facts about Linux"
date: 2015-02-09
categories:
author: ""      
tags: [Linux kernel,Operating system,Linus Torvalds,Computer architecture,Linux,Intellectual works,Free content,Operating system families,Open-source movement,Free software,Computer engineering,System software,Computers,Computer science,Software engineering,Software development,Technology,Software,Computing]
---


In this article, I will use the terms Linux, kernel or Linux kernel interchangeably to mean the same thing. Folks who argue that Linux is not an operating system are operating system purists who think that the kernel alone does not make the whole operating system, or free software ideologists who believe that the largest free operating system should be named GNU/Linux to give credit where credit is due (i.e., GNU project). The most prolific contributor is, of course, Linus Torvalds himself, who has committed code more than 20,000 times over the course of the lifetime of Linux. The figure below shows the top-10 corporate sponsors of Linux kernel development, in terms of total commit counts from their employees, as of year 2013. I don't ask for access to the hardware you design and sell.

<hr>

[Visit Link](http://xmodulo.com/2014/08/interesting-facts-linux.html){:target="_blank" rel="noopener"}


