---
layout: post
title: "Animal evolution: Revolution averted"
date: 2015-12-22
categories:
author: Ludwig-Maximilians-Universität München
tags: [Ctenophora,Animal,Placozoa,Sponge,Evolution,Biology,Biological evolution,Taxa,Evolutionary biology,Animals]
---


A new study by an team of Ludwig-Maximilians-Universitaet (LMU) in Munich reaffirms that sponges are the oldest animal phylum - and restores the classical view of early animal evolution, which recent molecular analyses had challenged. If we are to understand the evolution of certain key features of animals, such as their nervous systems, tissue organization and organs, we first have to clarify the early phylogenetic relationships Wörheide explains. The evolution of complexity  The traditional view of the issue postulates that the sponges were the first group to diverge from the lineage that gave rise to all other animals - in other words, Porifera are the sister group of all other animal species. Confirmation of this latter hypothesis would have far-reaching implications for our understanding of evolutionary history because comb jellies and their relatives are relatively complex animals - unlike sponges and placozoans, ctenophores possess muscles and a nervous system. Wörheide and colleagues used the new Compute Cloud at the Leibniz Supercomputing Centre (LRZ) of the Bavarian Academy of Sciences and Humanities for their complex calculations.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/lm-aer120115.php){:target="_blank" rel="noopener"}


