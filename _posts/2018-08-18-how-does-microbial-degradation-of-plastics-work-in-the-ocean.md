---
layout: post
title: "How does microbial degradation of plastics work in the ocean?"
date: 2018-08-18
categories:
author: "NIOZ Royal Netherlands Institute for Sea Research"
tags: [Biodegradation,Plastic,Microorganism,Carbon,Ocean,Environmental science,Physical sciences,Nature,Earth sciences,Chemistry]
---


How does microbial degradation of plastics work in the ocean? NIOZ marine scientist Helge Niemann was awarded a grant of 2 million euro by the European Research Council (ERC) to find out how the process of microbial degradation works in the ocean. Which microbes can feed on plastic and which environmental conditions control this enigmatic process? To study this, Niemann will gather a team of young scientists at the NIOZ Netherlands Institute for Sea Research. Microbes that are feeding on this plastic will take up and digest the heavy carbon.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/nrni-hdm112917.php){:target="_blank" rel="noopener"}


