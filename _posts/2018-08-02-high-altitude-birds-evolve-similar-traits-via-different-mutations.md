---
layout: post
title: "High-altitude birds evolve similar traits via different mutations"
date: 2018-08-02
categories:
author: "University Of Nebraska-Lincoln"
tags: [Hemoglobin,Evolution,Protein,Plateau,Species,Biology]
---


Published in the Proceedings of the National Academy of Sciences, the study found that many species from the two plateaus underwent different mutations to produce the same result: hemoglobins more adept at snaring oxygen from the lungs before sharing it with the other organs that depend on it. Jay Storz stands on the Tibetan Plateau, more than 15,000 feet above sea level. Storz and his colleagues have shown that many high-altitude bird species underwent different mutations to develop the same adaptation: hemoglobin better at capturing and distributing scarce oxygen. The interactions among those amino acids dictate the structure of a protein, which in turn determines its properties – how readily it binds with and releases oxygen, for instance. After comparing the ancestral vs. modern hemoglobin proteins of nine species that inhabit the Tibetan Plateau, the team did identify two cases in which distantly related species underwent identical, functionally important mutations.

<hr>

[Visit Link](https://phys.org/news/2018-02-high-altitude-birds-evolve-similar-traits.html){:target="_blank" rel="noopener"}


