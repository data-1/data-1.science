---
layout: post
title: "Diet drives primate brain size"
date: 2017-09-11
categories:
author: ""
tags: []
---


Alex DeCasien and her colleagues at New York University compiled existing data on primate brain and body size, and sorted the species into four dietary categories: omnivores, leaf-eaters, fruit-eaters and those that eat both leaves and fruit. The primates that ate fruit had significantly larger brains than those that ate just leaves; and the more fruit they ate, the larger was the ratio of their brain size to their body size. The authors think this could be due to a combination of factors — one being the high nutritional content of fruit, another being cognitive adaptations that help the primates to forage for fruit. Such adaptations could allow them, for example, to extract fruit from protective skins.

<hr>

[Visit Link](http://www.nature.com/nature/journal/v543/n7647/full/543592d.html?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


