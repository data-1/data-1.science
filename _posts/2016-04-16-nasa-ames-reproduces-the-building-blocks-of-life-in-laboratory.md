---
layout: post
title: "NASA Ames reproduces the building blocks of life in laboratory"
date: 2016-04-16
categories:
author: Ruth Marlaire
tags: [Pyrimidine,Uracil,Thymine,Cytosine,DNA,Cosmic dust,Scott Sandford,Physical sciences,Chemistry,Nature]
---


We have demonstrated for the first time that we can make uracil, cytosine, and thymine, all three components of RNA and DNA, non-biologically in a laboratory under conditions found in space, said Michel Nuevo, research scientist at NASA's Ames Research Center, Moffett Field, California. We are showing that these laboratory processes, which simulate conditions in outer space, can make several fundamental building blocks used by living organisms on Earth. Credit: NASA  Scientists tested their hypotheses in the Ames Astrochemistry Laboratory. Instead of being destroyed, many of the molecules took on new forms, such as the RNA/DNA components uracil, cytosine, and thymine, which are found in the genetic make-up of all living organisms on Earth. The ring-shaped molecule pyrimidine is found in cytosine and thymine.

<hr>

[Visit Link](http://phys.org/news344760745.html){:target="_blank" rel="noopener"}


