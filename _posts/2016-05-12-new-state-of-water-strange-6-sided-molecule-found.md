---
layout: post
title: "New State of Water: Strange 6-Sided Molecule Found"
date: 2016-05-12
categories:
author: Tom Metcalfe
tags: [Beryl,Proton,Atom,Molecule,Energy,Water,Applied and interdisciplinary physics,Physics,Physical sciences,Physical chemistry,Nature,Atomic molecular and optical physics,Quantum mechanics,Atomic physics,Atoms,Materials,Chemistry]
---


[The Surprisingly Strange Physics of Water]  The ring shape is caused by the quantum tunneling of the molecules, a phenomenon that lets subatomic particles pass or “tunnel” through seemingly-impossible physical barriers. In this scenario, the atoms of the water molecule are delocalized among six possible directions inside natural hexagonal pores or channels that run though the crystal structure of the beryl, so it partially exist in all six positions at the same time, the researchers said. We knew that natural beryl would have water in these channels in the structure, so we could go and look at that and see what the properties were, he said. Measuring the molecules  Alexander Kolesnikov, a physicist at ORNL and the lead author of the new paper, said additional studies at the Rutherford Appleton Laboratory had determined that the kinetic energy of the hydrogen protons in the six-sided water molecules was about 30 percent lower than in molecules of water in its normal state, or bulk water. That is a direct indication that this is a quantum property due to the tunneling of water in this beryl channel, Kolesnikov told Live Science.

<hr>

[Visit Link](http://www.livescience.com/54710-strange-six-sided-water-molecule-found.html){:target="_blank" rel="noopener"}


