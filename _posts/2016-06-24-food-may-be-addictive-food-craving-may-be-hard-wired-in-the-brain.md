---
layout: post
title: "Food may be addictive: Food craving may be 'hard-wired' in the brain"
date: 2016-06-24
categories:
author: "European College of Neuropsychopharmacology (ECNP)"
tags: [Obesity,Addiction,Brain,Functional magnetic resonance imaging,Body mass index,Reward system,Resting state fMRI,Neuroscience,Clinical medicine,Cognitive science]
---


This indicates that the tendency to want food may be 'hard-wired' into the brain of overweight patients, becoming a functional brain biomarker. Recently, studies are beginning to suggest that the brain mechanisms underlying obesity may be similar to those in substance addiction, and that treatment methodologies may be approached in the same way as other substance addictions, such as alcohol or drug addiction. The functional MRI scans showed that food craving was associated with different brain connectivity, depending on whether the subject was normal-weight or overweight. However, with normal weight individuals, food craving was associated with a greater connectivity between different parts of the brain -- e.g. between the ventral putamen and the orbitofrontal cortex. The researchers then measured Body Mass Index (BMI) three months afterwards and found that 11% of the weight gain in the obese individuals could be predicted by the presence of the increased connectivity between the dorsal caudate and the somatosensory cortex areas of the brain.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150831001121.htm){:target="_blank" rel="noopener"}


