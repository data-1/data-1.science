---
layout: post
title: "Drought-resistant plant genes could accelerate evolution of water-use efficient crops"
date: 2018-01-20
categories:
author: "DOE/Oak Ridge National Laboratory"
tags: [Office of Science,Crassulacean acid metabolism,Oak Ridge National Laboratory,Science,Biology]
---


OAK RIDGE, Tenn., Dec.1, 2017 - Scientists at the Department of Energy's Oak Ridge National Laboratory have identified a common set of genes that enable different drought-resistant plants to survive in semi-arid conditions, which could play a significant role in bioengineering and creating energy crops that are tolerant to water deficits. This form of photosynthesis, known as crassulacean acid metabolism or CAM, has evolved over millions of years, building water-saving characteristics in plants such as Kalanchoë, orchid and pineapple. PEPC is an important worker enzyme responsible for the nighttime fixation of carbon dioxide into malic acid. These convergent changes in gene expression and protein sequences could be introduced into plants that rely on traditional photosynthesis, accelerating their evolution to become more water-use efficient, said Yang. The research was funded by DOE's Office of Science (Biological and Environmental Research, Genomic Science Program) and ORNL's Laboratory Directed Research and Development program.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/drnl-dpg120117.php){:target="_blank" rel="noopener"}


