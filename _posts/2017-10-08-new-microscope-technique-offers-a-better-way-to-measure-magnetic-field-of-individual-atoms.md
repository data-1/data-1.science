---
layout: post
title: "New microscope technique offers a better way to measure magnetic field of individual atoms"
date: 2017-10-08
categories:
author: "Bob Yirka"
tags: [Atom,Magnetism,Microscope,Magnetic field,Nanotechnology,Sensor,Technology]
---


Scientists are eager to better measure the magnetic fields of individual atoms because they believe it will lead to a better understanding of material and biological interactions—most particularly those involving weak magnetic interactions. In this new effort, the team has come up with a way to get the job done that is relatively simple, though, they note, it requires special hardware. In the new approach, an atom called a sensor is placed near a target atom inside of a scanning tunneling microscope—a magnetic field is then applied to the microscope followed by a jolt of electricity to the tunnel junction. From there on, the frequency of the atom is monitored—when it matches the spin of the precess (the axis of rotation around a magnetic field that reflects its degree of magnetism), it reveals the measure of the magnetic field. The researchers found their approach to be far more accurate and easier to read than other methods, pointing out that the signal they got from the technique was both stronger and more robust.

<hr>

[Visit Link](https://phys.org/news/2017-03-microscope-technique-magnetic-field-individual.html){:target="_blank" rel="noopener"}


