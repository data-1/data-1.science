---
layout: post
title: "A molecular compass for bird navigation"
date: 2016-04-14
categories:
author: American Physical Society
tags: [Magnetism,Bird migration,Magnetic field,Physics]
---


Researchers have established that birds can sense the earth's magnetic field and use it to orient themselves. Behavioral experiments have shown that even subtle disruptions to the magnetic field can impact birds' ability to navigate. The researchers concluded that even low-level electromagnetic noise in the frequency range blocked by the aluminum screens—probably coming from AM radio signals and electronic equipment running in buildings —somehow interfered with the urban robins' magnetic orientation ability. This would suggest that the radical pairs in cryptochrome preserve their quantum coherence for much longer than previously believed possible. Explore further Quantum robins lead the way

<hr>

[Visit Link](http://phys.org/news344257108.html){:target="_blank" rel="noopener"}


