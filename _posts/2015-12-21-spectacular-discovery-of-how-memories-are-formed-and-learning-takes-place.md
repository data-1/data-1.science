---
layout: post
title: "'Spectacular discovery' of how memories are formed and learning takes place"
date: 2015-12-21
categories:
author: University of Leicester
tags: [Memory,Episodic memory,Brain,Learning,Research,Neuroscience,Concept,Concepts in metaphysics,Branches of science,Interdisciplinary subfields,Mental processes,Cognitive psychology,Cognition,Cognitive science,Psychology]
---


Jennifer Aniston, Clint Eastwood and Halle Berry images used in 'mind game' to establish for the first time how new memories are formed  Observations based on human brain studies  Study reveals that the same brain neuron that fires for one image (Jennifer Aniston) would also fire instantly for another image (Eiffel Tower) if the volunteer had been shown an image of Jennifer Aniston at the Eiffel Tower  This remarkable result shows that the neurons changed their firing properties at the exact moment the subjects formed the new memories  It demonstrates that the neuron encodes the memory of the person as well as the place if they are both shown together. Therefore a memory has been formed of that person at that place 'The discovery that individual neurons in the Medial Temporal Lobe, the brain's main engine for memory formation, changed their firing to encode new associations even after one single presentation provides a plausible mechanism underlying the creation of new memories. A collaboration between Dr Matias Ison and Professor Rodrigo Quian Quiroga at the University of Leicester and Dr Itzhak Fried at Ronald Reagan UCLA Medical Center revealed how a neuron in the brain instantly fired differently when a new memory was formed. The research group at Leicester and UCLA had previously announced the 'Jennifer Aniston neuron' -the firing of a single neuron for a single image to form a concept. The remarkable result was that the neurons changed their firing properties at the exact moment the subjects formed the new memories - the neuron initially firing to Jennifer Aniston started firing to the Eiffel Tower at the time the subject started remembering this association, said Rodrigo Quian Quiroga, head of the Centre for Systems Neuroscience at the University of Leicester.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/uol-do062615.php){:target="_blank" rel="noopener"}


