---
layout: post
title: "Quantum theory and Einstein's special relativity applied to plasma physics issues"
date: 2016-08-01
categories:
author: "DOE/Princeton Plasma Physics Laboratory"
tags: [Physics,Quantum mechanics,Quantum field theory,Plasma (physics),General relativity,Special relativity,Light,Wave,Theory of relativity,Physical sciences,Applied and interdisciplinary physics,Nature,Theoretical physics,Science]
---


Physicists want to know the strength of the magnetic field and density of the plasma that surrounds these pulsars, which can be millions of times greater than the density of plasma in stars like the sun. In pulsars, relativistic particles in the magnetosphere, the magnetized atmosphere that surrounds the body, absorb light waves, and this absorption displays peaks against a blackbody background. Analysis of the peaks with equations from special relativity and quantum field theory, he found, can determine the density and field strength of the magnetosphere. In high-energy physics, researchers use quantum field theory to describe the interaction of a handful of particles. The same technique can be used to infer the density of the plasma and strength of the magnetic field created by inertial confinement fusion experiments.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/07/160729190149.htm){:target="_blank" rel="noopener"}


