---
layout: post
title: "ALMA confirms comets forge organic molecules in their dusty atmospheres"
date: 2015-10-29
categories:
author: National Radio Astronomy Observatory
tags: [Atacama Large Millimeter Array,Coma (cometary),Comet,Hydrogen isocyanide,Associated Universities Inc,Comet ISON,Astronomical objects,Science,Bodies of the Solar System,Planetary science,Outer space,Physical sciences,Space science,Astronomy,Solar System]
---


ALMA's high-resolution observations provided a tantalizing 3D perspective of the distribution of the molecules within these two cometary atmospheres, or comas. Similar maps revealed that HNC and formaldehyde are produced in the coma, rather than originating from the comet's nucleus. Credit: B. Saxton (NRAO/AUI/NSF); Gerald Rhemann; M. Cordiner, NASA, et al.  Understanding organic dust is important, because such materials are more resistant to destruction during atmospheric entry, and some could have been delivered intact to the early Earth, thereby fueling the emergence of life, said Michael Mumma, director of the Goddard Center for Astrobiology and a co-author on the study. The emission from organic molecules in the atmosphere of comet ISON as observed with ALMA. The high sensitivity achieved in these studies paves the way for observations of perhaps hundreds of the dimmer or more distant comets, said Goddard's Stefanie Milam, a study co-author.

<hr>

[Visit Link](http://phys.org/news327038157.html){:target="_blank" rel="noopener"}


