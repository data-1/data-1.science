---
layout: post
title: "The roots of human altruism"
date: 2016-03-16
categories:
author: University Of Zurich
tags: [Altruism,Primate,Behavioural sciences,Cognitive science,Animals,Psychology]
---


For their study, Burkart and her colleagues developed the new paradigm of group service, which examines spontaneous helping behavior in a standardized way. The scientists applied this standardized test to 24 social groups of 15 different primate species. Test set-up for the altruism study  A treat is placed on a moving board outside the cage and out of the animal's reach. With the aid of a handle, an animal can pull the board closer and bring the food within reach. However, the handle attached to the board is so far from the food that the individual operating it cannot grab the food itself.

<hr>

[Visit Link](http://phys.org/news328355576.html){:target="_blank" rel="noopener"}


