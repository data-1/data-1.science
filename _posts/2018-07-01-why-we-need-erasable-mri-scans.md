---
layout: post
title: "Why we need erasable MRI scans"
date: 2018-07-01
categories:
author: "Whitney Clavin, California Institute Of Technology"
tags: [Magnetic resonance imaging,Medical imaging,Gas vesicle,MRI contrast agent,Medical specialties,Clinical medicine,Chemistry]
---


Caltech's Mikhail Shapiro and his colleagues have developed erasable MRI contrast agents that can blink off on command with ultrasound technology. As illustrated here, the contrast agents—which consist of air-filled protein structures called gas vesicles—give off magnetic signals. What's more, the gas vesicles could be turned off. Previous studies of gas vesicles have shown that these protein structures can be genetically modified to target receptors on specific cells, such as tumor cells. We have previously shown that we can genetically engineer the gas vesicles in a variety of ways for use in ultrasound imaging, says Shapiro.

<hr>

[Visit Link](https://phys.org/news/2018-04-erasable-mri-scans.html){:target="_blank" rel="noopener"}


