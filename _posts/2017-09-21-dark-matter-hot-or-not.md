---
layout: post
title: "Dark matter—hot or not?"
date: 2017-09-21
categories:
author: "Matt Williams"
tags: [Dark matter,Galaxy,Universe,Physical cosmology,Lambda-CDM model,Matter,Chronology of the universe,Milky Way,Cold dark matter,Redshift,Massive compact halo object,Hubble Space Telescope,Nature,Extragalactic astronomy,Astronomical objects,Celestial mechanics,Cosmology,Concepts in astronomy,Science,Astrophysics,Astronomy,Physical sciences,Physics,Space science]
---


Accounting for 27% of the mass and energy in the observable universe, the existence of this matter was intended to explain all the missing baryonic matter in cosmological models. So far, theories have ranged from saying that it is made up of cold, warm or hot matter, with the most widely-accepted theory being the Lambda Cold Dark Matter (Lambda-CDM) model. The different theories on dark matter (cold, warm, hot) refer not to the temperatures of the matter itself, but the size of the particles themselves with respect to the size of a protogalaxy – an early universe formation, from which dwarf galaxies would later form. As cosmological explanations go, it is the most simple and can account for the formation of galaxies or galaxy cluster formations. In short, the existence of dark matter as massive particles that have low FSL would result in small fluctuations in the density of matter in the early universe – which would lead to large amounts of low-mass galaxies to be found as satellites of galactic halos, and with large concentrations of dark matter in their centers.

<hr>

[Visit Link](http://phys.org/news/2016-08-dark-matterhot.html){:target="_blank" rel="noopener"}


