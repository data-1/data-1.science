---
layout: post
title: "How do you turn a mosquito's genes on and off?"
date: 2016-07-21
categories:
author: "University at Buffalo"
tags: [Genome,Genetics,DNA sequencing,Gene,Drosophila,Mosquito,Drosophila melanogaster,Anopheles,DNA,Health,Genetic engineering,Malaria,Biology,Biotechnology,Life sciences]
---


The research project, funded by the National Institutes of Health (NIH), could have implications for disease control, potentially facilitating efforts to use genetic engineering to control mosquito populations, or to create mosquitoes that have reduced ability to transmit maladies, such as malaria, to humans. Although we know the sequence of the mosquito genome, we have little functional information about what much of that genome sequence does. With Saurabh Sinha, a computer scientist at the University of Illinois at Urbana-Champaign, Halfon developed a software called SCRMshaw that learns the regulatory sequences within REDfly, then searches the genomes of other insects for strings of DNA with similarities. ###  Founded in 1846, the Jacobs School of Medicine and Biomedical Sciences at the University at Buffalo is beginning a new chapter in its history with the largest medical education building under construction in the nation. These new facilities will better enable the school to advance health and wellness across the life span for the people of New York and the world through research, clinical care and the education of tomorrow's leaders in health care and biomedical sciences.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/uab-hdy071116.php){:target="_blank" rel="noopener"}


