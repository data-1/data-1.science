---
layout: post
title: "Lichens can survive space conditions for extended periods"
date: 2016-03-28
categories:
author: Aaron L. Gronstal, Astrobio.Net
tags: [Astrobiology,Outer space,Spaceflight,Astronomy,Space science,Science,Nature]
---


Expose facility. Credit: ESA  A new study shows that a large percentage of hardy lichens exposed to space conditions for one and a half years remain viable after returning to Earth. The lichen Xanthoria elegans was part of the lichen and fungi experiment (LIFE) on the International Space Station (ISS). The lichen had been exposed before on previous experiments such as BIOPAN, but never for such a long period of time. A subset of the lichen samples were also exposed to simulated Mars conditions by adding an analog Mars atmosphere and solar radiation filters to the experimental chambers.

<hr>

[Visit Link](http://phys.org/news333186526.html){:target="_blank" rel="noopener"}


