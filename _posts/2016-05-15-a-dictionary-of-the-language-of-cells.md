---
layout: post
title: "A dictionary of the language of cells"
date: 2016-05-15
categories:
author:  
tags: [Cell signaling,Cell (biology),Organism,Cell biology,Biology,Life sciences]
---


In their struggle to survive and prosper, multicellular organisms rely on a complex network of communication between cells, which in humans are believed to number about 40 trillion. The group looked at the expression of 1,894 ligand-receptor pairs—based on 642 ligands and 589 receptors—that have been reported in the literature so far, and drew up a large-scale map of cell-to-cell communication between 144 primary human cell types. The development of multicellular organisms from unicellular ancestors is one of the most profound evolutionary events in the history of life on Earth. Currently there are no reports based on systematic studies attempting to elucidate and quantify the repertoire of signaling routes between different cell types. We also discovered that receptors in general seem to have evolved earlier than ligands.

<hr>

[Visit Link](http://phys.org/news/2015-07-dictionary-language-cells.html){:target="_blank" rel="noopener"}


