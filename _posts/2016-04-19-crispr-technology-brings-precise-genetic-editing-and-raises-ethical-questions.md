---
layout: post
title: "CRISPR technology brings precise genetic editing – and raises ethical questions"
date: 2016-04-19
categories:
author: Shouguang Jin, The Conversation
tags: [Genome editing,CRISPR gene editing,CRISPR,Transcription activator-like effector nuclease,Molecular biology,Life sciences,Biology,Genetics,Biotechnology]
---


A powerful new genetic engineering technique allows scientists to precisely cut out and replace DNA in genes. Changes can be made in the DNA around the cleavage site which alter the biological features of the resulting cells or organisms. Now, the CRISPR technology is enabling scientists to study those gene functions. By eliminating or replacing specific DNA fragments and observing the consequences in the resulting cells, we can now link particular DNA fragments to their biological functions. Credit: US Department of Energy  Call for ethical guidelines  With the CRISPR technology, scientists can now alter the genome composition of whole organisms, including humans, through manipulating reproductive cells and fertilized eggs or embryos.

<hr>

[Visit Link](http://phys.org/news346578382.html){:target="_blank" rel="noopener"}


