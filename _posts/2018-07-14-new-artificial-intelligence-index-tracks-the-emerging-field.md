---
layout: post
title: "New artificial intelligence index tracks the emerging field"
date: 2018-07-14
categories:
author: "Andrew Myers, Stanford University"
tags: [Artificial intelligence,Internet privacy,Speech recognition,Technology,Branches of science,Computing,Cognitive science]
---


In an effort to track the progress of this emerging field, a Stanford-led group of leading AI thinkers called the AI100 has launched an index that will provide a comprehensive baseline on the state of artificial intelligence and measure technological progress in the same way the gross domestic product and the S&P 500 index track the U.S. economy and the broader stock market. Baseline metrics  The AI Index tracks and measures at least 18 independent vectors in academia, industry, open-source software and public interest, plus technical assessments of progress toward what the authors call human-level performance in areas such as speech recognition, question-answering and computer vision – algorithms that can identify objects and activities in 2-D images. Venture capital investment has increased six times in the same period. In academia, publishing in AI has increased a similarly impressive nine times in the last 20 years while course enrollment has soared. Nonetheless, the authors note that computers continue to lag considerably in the ability to generalize specific information into deeper meaning.

<hr>

[Visit Link](https://phys.org/news/2018-01-artificial-intelligence-index-tracks-emerging.html){:target="_blank" rel="noopener"}


