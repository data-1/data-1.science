---
layout: post
title: "Japan scientists grow drugs in chicken eggs"
date: 2017-10-09
categories:
author: ""
tags: [Chicken,Interferon,Privacy]
---


The scientists now have three hens whose eggs containing the drug interferon beta, with the birds laying eggs almost daily, the report said  Japanese researchers have genetically engineered hens whose eggs contain drugs that can fight serious diseases including cancer, in a bid to dramatically reduce the cost of treatment, a report said Monday. Researchers at the National Institute of Advanced Industrial Science and Technology (AIST) in the Kansai region kicked off the process by introducing genes that produce interferon beta into cells which are precursors of chicken sperm, the newspaper reported. The researchers plan to sell the drug to pharmaceutical companies, halving its price, so the firms can use it first as a research material, the newspaper said. Consumers may have to wait a while, as Japan has strict regulations concerning the introduction of new or foreign pharmaceutical products, with screening processes that routinely take years to complete. But the team hopes that the technological breakthrough will eventually help drive down the cost of the drug to 10 percent of its current price, the newspaper reported.

<hr>

[Visit Link](https://phys.org/news/2017-10-japan-scientists-drugs-chicken-eggs.html){:target="_blank" rel="noopener"}


