---
layout: post
title: "Researchers succeed in imaging quantum events"
date: 2018-08-22
categories:
author: "Bar-Ilan University"
tags: [Quantum mechanics,Phase (matter),Phase transition,Temperature,SQUID,Physics,Water,Internet privacy,Microscope,Nanotechnology,Applied and interdisciplinary physics,Science,Physical sciences,Technology]
---


Scanning a SQUID sensor detects fluctuations near a quantum phase transition. Similar to classical phase transitions, quantum phase transitions are also accompanied by the presence of bubbles of one phase in the other. These changes lead to quantum bubbles of one phase into a second phase even at the absolute zero temperature. Until now it has been impossible to take pictures of these quantum fluctuations. We are used to this behavior of air bubbles in boiling water, but now similar bubbles can also be seen in quantum matter.

<hr>

[Visit Link](https://phys.org/news/2018-08-imaging-quantum-events.html){:target="_blank" rel="noopener"}


