---
layout: post
title: "3-D quantum gas atomic clock offers new dimensions in measurement"
date: 2017-10-06
categories:
author: "National Institute Of Standards"
tags: [Atomic clock,Atom,Quantum mechanics,Gas,Optical lattice,Physics,Applied and interdisciplinary physics,Science,Physical sciences,Theoretical physics,Physical chemistry]
---


With so many atoms completely immobilized in place, JILA's cubic quantum gas clock sets a record for a value called quality factor and the resulting measurement precision. A large quality factor translates into a high level of synchronization between the atoms and the lasers used to probe them, and makes the clock's ticks pure and stable for an unusually long time, thus achieving higher precision. In contrast, the new cubic quantum gas clock uses a globally interacting collection of atoms to constrain collisions and improve measurements. Compared with Ye's previous 1-D clocks, the new 3-D quantum gas clock can reach the same level of precision more than 20 times faster due to the large number of atoms and longer coherence times. This approach holds enormous promise for NIST and JILA to harness quantum correlations for a broad range of measurements and new technologies, far beyond timing.

<hr>

[Visit Link](https://phys.org/news/2017-10-d-quantum-gas-atomic-clock.html){:target="_blank" rel="noopener"}


