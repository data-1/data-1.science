---
layout: post
title: "What is the Raspberry Pi Foundation? 10 million computers sold"
date: 2017-09-24
categories:
author: "Opensource.com
(Red Hat)"
tags: [Red Hat,Communication,Technology,Computing]
---


With more than 10 million units sold, the Raspberry Pi is a massive success. At this year's All Things Open, community manager Ben Nuttall gave a five-minute lightning talk introducing the educational charity behind the popular mini computer. The Raspberry Pi Foundation was created in 2006 with the goal of getting more students to study computer science at Cambridge University. The foundation's new goal, Putting the power of digital making into the hands of people all over the world, is fulfilled by providing low-cost, high-performance computers; outreach and education programs; and free resources and teacher training. Anyone can start a Code Club, and the Raspberry Pi Foundation provides training and support for organizers and volunteers.

<hr>

[Visit Link](https://opensource.com/article/16/11/what-raspberry-pi-foundation-10-million-computers-sold){:target="_blank" rel="noopener"}


