---
layout: post
title: "Symbiotic plants are more diverse, finds new study"
date: 2016-04-11
categories:
author: Cornell University
tags: [Evolution,Plant,Species,Biology,Mutualism (biology),Nature,Biological evolution,Science]
---


The study finds that when plants develop mutually beneficial relationships with animals, mainly insects, those plant families become more diverse by evolving into more species over time. The researchers conducted a global analysis of all vascular plant families, more than 100 of which have evolved sugary nectar-secreting glands that attract and feed protective animals, such as ants. The study reports that plant groups with nectar glands contain greater numbers of species over time than groups without the glands. By attracting bodyguards to plants, these glands can increase plant success in a variety of habitats by protecting them from local pests, Weber said. ###  The study was funded by the National Science Foundation, the John Templeton Foundation and the Society for the Study of Evolution.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-11/cu-spa111314.php){:target="_blank" rel="noopener"}


