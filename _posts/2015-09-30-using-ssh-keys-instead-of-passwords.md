---
layout: post
title: "Using SSH keys instead of passwords"
date: 2015-09-30
categories:
author: Michael Boelen
tags: [Password,Linux,Operating system,Public-key cryptography,User (computing),Lynis,Computer science,Software development,Software engineering,Information Age,Secure communication,Cyberwarfare,Information technology management,Computer architecture,Computer security,System software,Technology,Software,Computing,Computer engineering,Security engineering,Digital media,Cybercrime,Utility software,Computers,Security technology]
---


Using SSH keys instead of passwords  Linux systems are usually managed remotely with SSH (secure shell). [root@arch ~]# ssh-keygen -l -f .ssh/id_rsa 4096 98:eb:9a:f7:94:bf:a0:a1:4b:55:ca:82:c3:24:46:b8 .ssh/id_rsa.pub (RSA)  As you can see in this example, the tool will select the public key, even if you don’t provide they private key. If you used PuTTYgen to create the key, it will give you the string to add to the authorized_keys file. When logging in with the agent, we see something like “Authenticating with public key “rsa-key-20150316″ from agent”. Now you can use ssh and connect to your configured system(s) without a password.

<hr>

[Visit Link](http://linux-audit.com/using-ssh-keys-instead-of-passwords/){:target="_blank" rel="noopener"}


