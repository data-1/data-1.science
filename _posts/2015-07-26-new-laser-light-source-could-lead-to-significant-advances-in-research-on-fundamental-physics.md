---
layout: post
title: "New laser-light source could lead to significant advances in research on fundamental physics"
date: 2015-07-26
categories:
author: Max Planck Institute Of Quantum Optics
tags: [Laser,Physics,Max Planck Institute of Quantum Optics,Spectroscopy,Electron,Attosecond physics,Electromagnetic radiation,Light,Atomic molecular and optical physics,Optics,Science,Electromagnetism,Physical chemistry,Physical sciences,Chemistry,Applied and interdisciplinary physics]
---


The LAP team has developed a novel ytterbium:yttrium-aluminium-garnet thin-disk laser that emits light pulses lasting 7.7 femtoseconds and consisting of 2.2 optical oscillations. That would enable data processing operations to be performed at frequencies equivalent to the rate of oscillation of visible light – some 100,000 times faster than is feasible with current techniques. Most of the lasers utilized in research laboratories are based on titanium:sapphire (Ti:Sa) crystals, and this type of instrument has been the dominant tool in the production of ultrashort light pulses for over 20 years. Previous experiments carried out by the team at the LAP had shown that it is indeed possible to switch electric currents on and off using specially shaped electromagnetic wave packets, i.e. phase-controlled laser pulses (Schiffrin, Nature 2012; Paasch-Colberg, Nature Photonics 2014, Krausz & Stockman, Nature Photonics 2014). This type of laser spectroscopy provides a means of determining the values of constants of nature with extremely high precision.

<hr>

[Visit Link](http://phys.org/news350033946.html){:target="_blank" rel="noopener"}


