---
layout: post
title: "Three-year ocean voyage discovered thousands of new, unfamiliar viruses"
date: 2015-07-12
categories:
author: Arielle Duhaime-Ross, May
tags: [Marine microorganisms,Microorganism,Plankton,Virus,Biodiversity,Nature,Earth sciences,Natural environment,Biology,Organisms,Oceanography]
---


Over the course of three and a half years, researchers traveled the world in a boat named Tara, gathering genetic information belonging to more than 35,000 plankton species that were largely unknown to humanity. 11.5 terabytes of molecular data  Single-cell eukaryotes — tiny organisms with a membrane-bound nucleus — are a lot more diverse than previously thought, according to one of the Tara Oceans studies. Credit: Christian Sardet/CNRS/Tara Expéditions  The expedition also revealed that the important factor in determining the composition of microbial communities at depth still reached by sunlight is temperature. The microbial diversity found in the ocean is about four times higher than in the human gut  Few large microbial environments have been studied this extensively. Credit: M.Ormestad/Kahikai/Tara Oceans  You don't have to compare the ocean's microbial communities to the human gut to appreciate the work produced by the Tara Oceans Expeditions, however.

<hr>

[Visit Link](http://www.theverge.com/2015/5/21/8634331/plankton-ocean-virus-discovered-Tara){:target="_blank" rel="noopener"}


