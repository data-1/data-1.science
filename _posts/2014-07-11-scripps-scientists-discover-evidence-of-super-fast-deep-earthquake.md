---
layout: post
title: "Scripps scientists discover evidence of super-fast deep earthquake"
date: 2014-07-11
categories:
author: University of California - San Diego 
tags: [Earthquake,Seismology,Earthquake rupture,Moment magnitude scale,Supershear earthquake,Solid mechanics,Lithosphere,Applied and interdisciplinary physics,Geological hazards,Structure of the Earth,Earths crust,Natural hazards,Earthquakes,Natural disasters,Geophysics,Geology]
---


Scientists at Scripps Institution of Oceanography at UC San Diego have discovered the first evidence that deep earthquakes, those breaking at more than 400 kilometers (250 miles) below Earth's surface, can rupture much faster than ordinary earthquakes. These supershear earthquakes have rupture speeds of four kilometers per second (an astonishing 9,000 miles per hour) or more. After closely analyzing the data, Zhan not only found that the aftershock ruptured extremely deeply at 640 kilometers (400 miles) below the earth's surface, but its rupture velocity was extraordinary—about eight kilometers per second (five miles per second), nearly 50 percent faster than the shear wave velocity at that depth. This is the first definitive example of supershear rupture for a deep earthquake since previously supershear ruptures have been documented only for shallow earthquakes. This finding will help us understand why deep earthquakes happen, said Zhan.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/uoc--ssd070914.php){:target="_blank" rel="noopener"}


