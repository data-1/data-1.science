---
layout: post
title: "Researchers discover new mechanism controlling cell response to DNA damage"
date: 2016-04-29
categories:
author: H. Lee Moffitt Cancer Center, Research Institute
tags: [DNA repair,Ubiquitin,Cancer,DNA,Apoptosis,Sirtuin 1,Oxidative stress,Cell (biology),Biology,Biotechnology,Cell biology,Life sciences,Chemistry,Molecular biology,Cellular processes,Biological processes,Biochemistry]
---


Therefore, cells must maintain an intricate regulatory network to ensure that their DNA remains intact. It can sense the presence of DNA damage, signal to other proteins that damage exists, aid in the repair of damage and stimulate cell death if the damage cannot be repaired. This modification allows SIRT1 to relay information about DNA damage to other proteins, leading to either DNA repair or cell death. They discovered that cells respond to ubiquitin modification of SIRT1 differently according to the type of environmental insult that occurs. These results are important because they increase scientists' understanding of how proteins and cells function, potentially leading to more effective therapeutic drugs in the future.

<hr>

[Visit Link](http://phys.org/news349608899.html){:target="_blank" rel="noopener"}


