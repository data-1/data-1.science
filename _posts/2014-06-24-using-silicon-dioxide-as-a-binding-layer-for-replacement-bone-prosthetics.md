---
layout: post
title: "Using silicon dioxide as a binding layer for replacement bone prosthetics"
date: 2014-06-24
categories:
author: Inderscience Publishers
tags: [Implant (medicine),Joint replacement,Metal,Bone,Prosthesis,Bacteria,Pathogenic bacteria,Medical specialties,Materials]
---


Using the stuff of sand, silicon dioxide, as a binding layer for replacement bone prosthetics could allow more biocompatible artificial joints to be manufactured as well as reducing the risk of post-operative infection, according to research published in the International Journal of Surface Science and Engineering. The metals titanium and tantalum are widely used to make replacement implants for diseased or damaged bone, in the classic hip replacement, for instance. Unfortunately, a smooth metal surface, while long-lasting and wear resistant is not entirely biocompatible so manufacturers are developing materials – such as the bony mineral hydroxyapatite – that can be used to coat such implants to allow the body to accept the prosthetic and for cells and blood vessels to accommodate it more effectively. They can then successfully spray this surface with hydroxyapatite using magnetron sputtering to create a composite coating on the implant metal just 200 nanometres thick. The smoothness of the metal surface is a hindrance when it comes to the body incorporating the prosthetic, but at the same time this prevent pathogenic bacteria from adhering to the joint and causing serious infection around the replacement bone.

<hr>

[Visit Link](http://phys.org/news322816733.html){:target="_blank" rel="noopener"}


