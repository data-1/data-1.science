---
layout: post
title: "Newly-hatched salmon use geomagnetic field to learn which way is up"
date: 2018-07-01
categories:
author: "Oregon State University"
tags: [Salmon,Magnetic field,Spawn (biology),Fish,Earths magnetic field,Hatchery,Oregon State University,Water,American Association for the Advancement of Science,Earth sciences]
---


The Oregon Hatchery Research Center is a collaborative research project of Oregon State University, where Noakes is a professor and senior scientist in the College of Agricultural Sciences, and the Oregon Department of Fisheries and Wildlife. Upon hatching, the young salmon remain in the gravel until they deplete residual yolk stores, after which they emerge from the gravel and live above the substrate in the open water of the stream or river. This finding indicates that magnetic cues are used for three-dimensional orientation across a wide range of spatial scales and habitats. One group of salmon were exposed to the normal magnetic field in Oregon and another group of salmon to an inverted magnetic field. Fish in the normal magnetic field moved significantly further up the tubes than did those that experienced the inverted magnetic field.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/osu-nsu021518.php){:target="_blank" rel="noopener"}


