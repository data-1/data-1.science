---
layout: post
title: "Pacific Ocean's effect on Arctic warming"
date: 2018-08-14
categories:
author: "Carnegie Institution for Science"
tags: [Climate change,Climate change feedback,Climate,Climate change in the Arctic,Climate of the Arctic,Arctic,Climate variability and change,Geophysics,Global natural environment,Oceanography,Earth sciences,Global environmental issues,Physical geography,Atmospheric sciences,Environmental issues with fossil fuels,Environmental impact,Earth phenomena,Atmosphere,Nature,Applied and interdisciplinary physics,Natural environment]
---


New research, led by former Carnegie postdoctoral fellow Summer Praetorius, shows that changes in the heat flow of the northern Pacific Ocean may have a larger effect on the Arctic climate than previously thought. The Arctic is experiencing larger and more rapid increases in temperature from global warming more than any other region, with sea-ice declining faster than predicted. Q7 What has not been well understood is how sea-surface temperature patterns and oceanic heat flow from Earth's different regions, including the temperate latitudes, affect these polar feedbacks. This new research suggests that the importance of changes occurring in the Pacific may have a stronger impact on Arctic climate than previously recognized. The researchers found that both cooling and warming anomalies in the North Pacific resulted in greater global and Arctic surface air temperature anomalies than the same perturbations modeled for the North Atlantic.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180807095149.htm){:target="_blank" rel="noopener"}


