---
layout: post
title: "Study finds increased ocean acidification due to human activities"
date: 2017-10-26
categories:
author: Massachusetts Institute of Technology
tags: [Climate change,Ocean,Ocean acidification,Water,Carbon dioxide,Carbon,Oceanography,Hydrography,Applied and interdisciplinary physics,Hydrology,Natural environment,Environmental science,Physical sciences,Earth sciences,Chemistry,Physical geography,Nature]
---


Protecting shells  Chu and her colleagues originally set out to study the effects of ocean acidification on pteropods, rather than the ocean's capacity to store carbon. The models then estimated the natural variability in dissolved inorganic carbon for each year. Sinking carbon  The researchers found that since 2001, the northeast Pacific has stored 11 micromoles per kilogram of anthropogenic carbon, which is comparable to the rate at which carbon dioxide has been emitted into the atmosphere. The team calculated that the increase in anthropogenic carbon in the upper ocean caused a decrease in the region's average pH, making the ocean more acidic as a result. While the total amount of anthropogenic carbon appears to be increasing with each year, Chu says the rate at which the northeast Pacific has been storing carbon has remained relatively the same since 2001.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-09/miot-sfi090716.php){:target="_blank" rel="noopener"}


