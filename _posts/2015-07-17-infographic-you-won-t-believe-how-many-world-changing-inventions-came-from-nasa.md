---
layout: post
title: "INFOGRAPHIC: You Won’t Believe How Many World-Changing Inventions Came From NASA"
date: 2015-07-17
categories:
author: Mike Chino
tags: []
---


Everyone knows NASA put a man on the moon – but did you know that the agency’s R&D has also led to the invention of water filters, memory foam, and the computer mouse? This great new infographic from Great Business Schools highlights just a few of the 1,800 world-changing developments that have been spun out of NASA technology – including cellphone cameras, smoke detectors, life rafts, and firefighter gear. Check out the full infographic after the break! Continue reading below Our Featured Videos  + Great Business Schools

<hr>

[Visit Link](http://inhabitat.com/infographic-you-wont-believe-how-many-world-changing-inventions-came-from-nasa/){:target="_blank" rel="noopener"}


