---
layout: post
title: "Sugarcane Bioethanol Project in Sierra Leone Underscores the Challenge of Producing Bioenergy in the Developing World"
date: 2015-09-12
categories:
author: Andrew Burger, Andrew Reports On Renewable Energy, Clean Technology, Other Issues, Topics Posts Abroad, Here In The Us., Root, --Ppa-Color-Scheme, --Ppa-Color-Scheme-Active
tags: [Clean Development Mechanism,Energy,Nature,Sustainable technologies,Economy,Natural resources,Sustainable development,Renewable energy,Climate change mitigation,Sustainable energy]
---


As the SEI research team recounts in “Agricultural investment and rural transformation: a case study of the Makeni bioenergy project in Sierra Leone,” the Ebola outbreak in May 2014 took a terrible toll on the nation and the Makeni project, increasing costs, delaying development and driving it to the edge of viability. Touting its intention that Makeni serve “as a benchmark for sustainable investment in Africa, ABSL was able to obtain funding from six regional development banks, according to the report. An Uncertain Future  The outbreak of Ebola this past May imposed deep and lasting costs in Sierra Leone. Furthermore, ABSL and AOG are thoroughly reviewing all projected options for the future. All Images Credit: Addax Bioenergy Sierra Leone (ABSL)

<hr>

[Visit Link](http://www.renewableenergyworld.com/articles/2015/08/sugarcane-bioethanol-project-in-sierra-leone-underscores-the-challenge-of-bringing-energy-to-the-developing-world.html){:target="_blank" rel="noopener"}


