---
layout: post
title: "Researchers observe individual cellular receptors at work"
date: 2017-10-19
categories:
author: University Of Würzburg
tags: [G protein,Receptor (biochemistry),Cell signaling,Cell biology,Biochemistry,Biotechnology,Neuroscience,Biology,Molecular biology]
---


The receptors and G proteins undergo transient interactions, which occur preferentially at “hot spots” on the cell membrane. Credit: Team Calebiro  Using a revolutionary live-cell microscopy technique, an international team of scientist has for the first time observed individual receptors for hormones and drugs working in intact cells. The receptors and G proteins undergo transient interactions, which occur preferentially at hot spots on the cell membrane. The authors also observed that receptors and G proteins usually remain associated for only a short time. In the future, it might be possible to influence these processes much more precisely—for example, by manipulating the mobility of receptors and G proteins on the cell membrane or their interactions at the hot spots.

<hr>

[Visit Link](https://phys.org/news/2017-10-individual-cellular-receptors.html){:target="_blank" rel="noopener"}


