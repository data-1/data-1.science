---
layout: post
title: "Biology study suggests father's nutrition before sex could contribute to health of baby: Biologists find the diet of male fruit flies influences the survivorship of their embryos."
date: 2017-10-13
categories:
author: "University of Cincinnati"
tags: [Genetics,Drosophila melanogaster,Gene,Mutation,Circadian rhythm,Epigenetics,News aggregator,Michael Rosbash,Biology]
---


UC researchers fed females the same diet. And by mating the males consecutively, researchers wanted to learn about the effect of male mating order and what role diet played in changing the male's ejaculate. Polak and Benoit found that embryos from the second mating were more likely to survive as their fathers' diets improved in nutrition. Females laid roughly the same number of eggs regardless of the male's diet or mating frequency. Polak's study also found a slightly higher incidence of embryo mortality associated with male flies in the first mating that were fed the highest-calorie diet.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171012125241.htm){:target="_blank" rel="noopener"}


