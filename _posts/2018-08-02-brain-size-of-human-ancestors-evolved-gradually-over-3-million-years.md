---
layout: post
title: "Brain size of human ancestors evolved gradually over 3 million years"
date: 2018-08-02
categories:
author: "University of Chicago Medical Center"
tags: [Human evolution]
---


Study of hominin fossils shows that brain size increased gradually and consistently, driven by evolution within populations, introduction of larger-brained species and extinction of smaller-brained ones  Modern humans have brains that are more than three times larger than our closest living relatives, chimpanzees and bonobos. His advisor, Bernard Wood, GW's University Professor of Human Origins and senior author of the study, gave his students an open-ended assignment to understand how brain size evolved through time. That's exactly what we see going on in brain size, he said. But we also see speciation events adding larger-brained daughter species, or recruiting bigger players, and we see extinction, or cutting the smallest players too. ###  The study, Pattern and process in hominin brain size evolution are scale-dependent, was supported by the National Science Foundation.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/uocm-bso022018.php){:target="_blank" rel="noopener"}


