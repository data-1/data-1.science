---
layout: post
title: "EARTH: Humans, megafauna coexisted in Patagonia before extinction"
date: 2017-09-25
categories:
author: "American Geosciences Institute"
tags: [Pleistocene,Nature]
---


Alexandria, VA - As we celebrate National Fossil Day, EARTH Magazine brings you a story set in Pleistocene South America, where the climate was warming following an ice age. At this time, Patagonia was home to large megafauna species like giant sloths and saber-toothed cats. By luck, numerous remains were preserved in cool volcanic and lakeshore caves. The chilled environment spared the DNA from destruction, providing scientists a unique opportunity for study. To find out how long humans and megafauna coexisted go to: http://bit.ly/2dcGwCS.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/agi-ehm101216.php){:target="_blank" rel="noopener"}


