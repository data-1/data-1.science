---
layout: post
title: "UH research finds evidence of 2 billion years of volcanic activity on Mars"
date: 2017-09-23
categories:
author: "University of Houston"
tags: [Volcanism on Mars,Mars,Volcano,Martian meteorite,Bodies of the Solar System,Physical sciences,Astronomical objects,Volcanism,Volcanology,Geology,Planets,Solar System,Outer space,Astronomy,Planetary science,Planets of the Solar System,Terrestrial planets,Space science,Astronomical objects known since antiquity]
---


Analysis of a Martian meteorite found in Africa in 2012 has uncovered evidence of at least 2 billion years of volcanic activity on Mars. Much of what we know about the composition of rocks from volcanoes on Mars comes from meteorites found on Earth. Analysis of different substances provides information about the age of the meteorite, its magma source, length of time in space and how long the meteorite was on Earth's surface. We see that they came from a similar volcanic source, Lapen said. In contrast, the meteorite analyzed by Lapen's research team was formed 2.4 billion years ago and suggests that it was ejected from one of the longest-lived volcanic centers in the solar system.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/uoh-urf012617.php){:target="_blank" rel="noopener"}


