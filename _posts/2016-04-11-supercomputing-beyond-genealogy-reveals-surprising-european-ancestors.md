---
layout: post
title: "Supercomputing beyond genealogy reveals surprising European ancestors"
date: 2016-04-11
categories:
author: Jorge Salazar, University Of Texas At Austin
tags: []
---


Right: Blue eyes and dark skin, that's how the European hunter-gatherer appeared 7,000 years ago. The main finding was that modern Europeans seem to be a mixture of three different ancestral populations, said study co-author Joshua Schraiber, a National Science Foundation Postdoctoral fellow at the University of Washington. But the genetic evidence is relatively strong, because we do have ancient DNA from an individual that's very closely related to that population, too, Schraiber said. What scientists did was take the genomes from these ancient humans and compare them to those from 2,345 modern-day Europeans. It made us happy in a lot of ways to find that these are people who share ancestors with modern Europeans.

<hr>

[Visit Link](http://phys.org/news334931668.html){:target="_blank" rel="noopener"}


