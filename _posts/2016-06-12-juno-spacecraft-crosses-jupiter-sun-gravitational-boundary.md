---
layout: post
title: "Juno spacecraft crosses Jupiter / Sun gravitational boundary"
date: 2016-06-12
categories:
author: "Jet Propulsion Laboratory"
tags: [Juno (spacecraft),Jupiter,Astronomical objects,Planets,Science,Physical sciences,Bodies of the Solar System,Astronautics,Flight,Planetary science,Sky,Spaceflight,Solar System,Outer space,Astronomy,Space science]
---


Launched in 2011, the Juno spacecraft will arrive at Jupiter in 2016 to study the giant planet from an elliptical, polar orbit. Credit: NASA  Since its launch five years ago, there have been three forces tugging at NASA's Juno spacecraft as it speeds through the solar system. The sun, Earth and Jupiter have all been influential—a gravitational trifecta of sorts. Once in orbit, the spacecraft will circle the Jovian world 37 times, skimming to within 3,100 miles (5,000 kilometers) above the planet's cloud tops. Explore further NASA's Juno spacecraft burns for Jupiter

<hr>

[Visit Link](http://phys.org/news/2016-05-juno-spacecraft-jupiter-sun-gravitational.html){:target="_blank" rel="noopener"}


