---
layout: post
title: "Bioreactors on a chip renew promises for algal biofuels"
date: 2017-10-06
categories:
author: "Boyce Thompson Institute"
tags: [Algae fuel,Biofuel,Mutation,Life sciences,Technology,Nature,Chemistry,Biotechnology,Biology]
---


One of many improvements necessary for sustainable production of algal biofuel is the development of better algae. A single algal cell is captured in a tiny droplet of water encapsulated by oil - imagine the tiny droplets that form when you mix vegetable oil with water - then millions of algal droplets squeeze onto a chip about the size of a quarter. The researchers first validated the chip system with algae known to grow faster or slower, or produce more or less lipid. Excitingly, the tools for improving throughput are already in development, including larger chips that can screen millions of droplets in one experiment. With the discovery and development of much more efficient algal strains, commercial-scale production of biofuel from algae may finally be a realistic promise.

<hr>

[Visit Link](https://phys.org/news/2017-09-bioreactors-chip-renew-algal-biofuels.html){:target="_blank" rel="noopener"}


