---
layout: post
title: "Light-activated genes might be precisely controlled and targeted"
date: 2016-04-13
categories:
author: Duke University
tags: [Genetics,Genetic engineering,Gene,Biology,CRISPR,Cell (biology),Cas9,Protein,Tissue engineering,Life sciences,Biotechnology,Biochemistry,Technology,Molecular biology]
---


With the ability to use light to activate genes in specific locations, researchers can better study genes' functions, create complex systems for growing tissue, and perhaps eventually realize science-fiction-like healing technologies. We can also target where the gene gets turned on by shining the light in specific patterns, for example by passing the light through a stencil. The Duke scientists then turned to another branch of the evolutionary tree to make the system light-activated. By attaching the CRISPR/Cas9 system to one of these proteins and gene-activating proteins to the other, the team was able to turn several different genes on or off just by shining blue light on the cells. And by creating different patterns of gene expression, Gersbach hopes the system can be used in tissue engineering.

<hr>

[Visit Link](http://phys.org/news342704543.html){:target="_blank" rel="noopener"}


