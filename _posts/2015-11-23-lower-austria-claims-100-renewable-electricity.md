---
layout: post
title: "Lower Austria Claims 100% Renewable Electricity"
date: 2015-11-23
categories:
author: Glenn Meyers, Jennifer Sensiba, U.S. Energy Information Administration, George Harvey, Written By
tags: [Renewable energy,Energy,Electric power,Technology,Environmental technology,Natural environment,Economy and the environment,Power (physics),Economy,Climate change mitigation,Energy and the environment,Renewable resources,Sustainable development,Physical quantities,Sustainable technologies,Sustainable energy,Nature]
---


Lower Austria, the largest of Austria’s nine states, has announced it now generates 100% of its electricity from renewable sources. Lower Austria’s electricity production is broken down into 63% hydroelectricity, 26% from wind energy, 9% from biomass, and 2% from solar. The nation voted against nuclear power in a 1978 referendum,  On the employment side of the energy scale, Lower Austria is laying claim to the creation of 38,000 “green jobs.” Proell has said, the state aims to increase to this total to 50,000 by 2030. It added these other considerations:  “Sweden recently announced it was aiming to become the world’s first fossil-fuel-free nation, and Denmark has enjoyed success in generating and sharing surplus energy through its wind power network. Image via ScienceAlert  Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2015/11/11/lower-austria-claims-100-renewable-electricity/){:target="_blank" rel="noopener"}


