---
layout: post
title: "Scientists find way to maintain quantum entanglement in amplified signals"
date: 2015-05-22
categories:
author: Moscow Institute of Physics and Technology 
tags: [Quantum mechanics,Quantum teleportation,Quantum entanglement,Photon,Wave function,Optical fiber,Wave,Quantum computing,Data communication,Complex number,Atom,Scientific theories,Physical sciences,Theoretical physics,Physics,Science,Applied and interdisciplinary physics]
---


In addition to this, quantum entanglement allows for carrying out quantum teleportation, wherein a quantum object, for example, an atom, in a certain state in one laboratory transmits its quantum state to another object in another laboratory. It is quantum entangled particles that play the key role in this process, and it is not necessarily about the quantum entanglement of the atoms between which the transmission of the state takes place. To achieve this effect, it is necessary to have the particles in a special, non-Gaussian state, or, as physicists put it, the wave function of the particles in the coordinate representation should not be in the form of a Gaussian wave packet. This detail was omitted due to reader unfamiliarity with complex numbers. Ordinary photons, which are used in most quantum entanglement experiments, are also described by a Gaussian function.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/miop-sfw072314.php){:target="_blank" rel="noopener"}


