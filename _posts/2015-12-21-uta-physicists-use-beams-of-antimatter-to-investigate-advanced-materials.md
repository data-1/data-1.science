---
layout: post
title: "UTA physicists use beams of antimatter to investigate advanced materials"
date: 2015-12-21
categories:
author: University of Texas at Arlington
tags: [Positron,Electron,Graphene,Physics,Spin (physics),University of Texas at Arlington,Antimatter,Annihilation,Materials science,Materials,Electromagnetism,Nature,Science,Chemistry,Physical sciences,Applied and interdisciplinary physics]
---


UTA Physics professor Ali Koymen is co-principal investigator on the grant, which continues through 2016. In their preliminary experiments, the UTA scientists used the new positron beam to explore the buried interface of eight layers of graphene on a copper substrate. These matter-antimatter annihilations produce electrons and gamma waves that carry information concerning the chemical nature, electronic structure and defects of the surfaces and interfaces at those sites. The UTA group plans to further develop the facility in the next few years by adding the capability to make measurements of the magnetic properties of nanomaterials through the addition of spin polarization - a uniform alignment of the positrons' spin or magnetic positioning - as another unique feature to the UTA beam. That feature will allow researchers to probe magnetic structures by determining their surface electrons' spin state, Dr. Koymen said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-11/uota-upu110915.php){:target="_blank" rel="noopener"}


