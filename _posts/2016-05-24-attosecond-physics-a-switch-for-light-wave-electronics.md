---
layout: post
title: "Attosecond physics: A switch for light-wave electronics"
date: 2016-05-24
categories:
author: Ludwig-Maximilians-Universität München
tags: [Electron,Ultrashort pulse,Electromagnetism,Physics,Physical sciences,Science,Electrical engineering,Applied and interdisciplinary physics,Optics,Electromagnetic radiation,Technology,Atomic molecular and optical physics]
---


Now a team led by Ferenc Krausz, who holds a Chair in Experimental Physics at LMU and is a Director of the Max Planck Institute for Quantum Optics in Garching, in collaboration with theorists from Tsukuba University in Japan, has used a novel combination of experimental and theoretical techniques, which for the first time provides direct access to the dynamics of this process. The electric field of light changes its direction a trillion times per second and is able to mobilize electrons in solids at this rate. This means that light waves can form the basis for future electronic switching, provided the induced electron motion and its influence on heat accumulation is precisely understood. Since it is possible to measure the energy exchanged within one light cycle for the first time, the parameters of the light-matter interaction can be precisely determined and optimized for ultrafast signal processing. The greater the degree of reversibility in the exchange and the smaller the amount of energy left behind in the medium after passage of the light pulse, the more suitable the interaction becomes for future light field-driven electronics.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/lm-apa052316.php){:target="_blank" rel="noopener"}


