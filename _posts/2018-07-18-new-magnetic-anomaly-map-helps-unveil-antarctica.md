---
layout: post
title: "New magnetic anomaly map helps unveil Antarctica"
date: 2018-07-18
categories:
author: "British Antarctic Survey"
tags: [Antarctica,Earth science,Geology,Continent,Plate tectonics,Transantarctic Mountains,Science,Earth sciences]
---


ADMAP-2.0 is a new magnetic anomaly map of Antarctica. Aircraft, helicopters and ships are used to collect aeromagnetic anomaly data over the Antarctic ice sheets and surrounding oceans. By including large new datasets and using advanced re-processing and data merging techniques we have vastly improved the Antarctic magnetic anomaly map. It allows us to image many of the key geological features hidden under the thick ice sheets that blanket the continent. ADMAP co-chair, Dr. Graeme Eagles from the Alfred Wegner Institute says:  Our next challenge is to fill in the remaining magnetic data gaps by exploring the most remote frontier areas of Antarctica building on the success of our international collaboration.

<hr>

[Visit Link](https://phys.org/news/2018-07-magnetic-anomaly-unveil-antarctica.html){:target="_blank" rel="noopener"}


