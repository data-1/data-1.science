---
layout: post
title: "10 Commands to Collect System and Hardware Info in Linux"
date: 2015-09-18
categories:
author: Aaron Kili
tags: [BIOS,Device file,Booting,GUID Partition Table,Ansible (software),SCSI,INT 13H,Linux,CPU cache,Floppy disk,PCI Express,Central processing unit,Unified Extensible Firmware Interface,Computing,Information Age,Manufactured goods,Operating system technology,Computer data,System software,Technology,Computer engineering,Computer hardware,Computers,Computer architecture,Computer science,Office equipment]
---


How to View Linux System Information  To know only the system name, you can use the uname command without any switch that will print system information or the uname -s command will print the kernel name of your system. How to View Linux System Hardware Information  Here you can use the lshw tool to gather vast information about your hardware components such as cpu, disks, memory, usb controllers, etc. How to Print USB Controllers Information  The lsusb command is used to report information about USB controllers and all the devices that are connected to them. How to Print SCSI Devices Information  To view all your scsi/sata devices, use the lsscsi command as follows. How to Check Linux Hardware Components Info  You can also use the dmidecode utility to extract hardware information by reading data from the DMI tables.

<hr>

[Visit Link](http://www.tecmint.com/commands-to-collect-system-and-hardware-information-in-linux/){:target="_blank" rel="noopener"}


