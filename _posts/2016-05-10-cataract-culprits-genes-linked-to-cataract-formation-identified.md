---
layout: post
title: "Cataract culprits: Genes linked to cataract formation identified"
date: 2016-05-10
categories:
author: University of Delaware
tags: [Gene,Bioinformatics,Genetics,Lens (anatomy),Cataract,Protein,Gene expression,MAFK,Regulation of gene expression,Eye,Biotechnology,Molecular biology,Biochemistry,Biology,Life sciences]
---


advertisement  Lachke and graduate students Smriti Agrawal, Archana Siddam and post-doctoral fellow Deepti Anand study lens development in mice to better understand the genetic mechanisms that lead to cataracts in humans. advertisement  They found answers in the proteins that regulate expression of genes necessary for transparency, demonstrating that deficiency of two regulatory proteins -- called Mafg and Mafk -- led to formation of cataracts. Extending the iSyTE approach to other components of the eye and other tissues or organs will greatly expedite disease gene discovery and advance our understanding of the human genome. Agrawal said working with Lachke on cataract research at UD inspired her to pursue her doctorate research on understanding the genetic basis of other eye diseases. Siddam said top researchers from around the world approached them at a recent conference to discuss the work of the Lachke Lab.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150618122111.htm){:target="_blank" rel="noopener"}


