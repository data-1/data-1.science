---
layout: post
title: "Quantum simulators wield control over more than 50 qubits, setting new record"
date: 2018-04-26
categories:
author: "University Of Maryland"
tags: [Quantum simulator,Quantum computing,Quantum mechanics,Science,Theoretical physics,Applied and interdisciplinary physics,Physical sciences,Electromagnetism,Technology,Physics]
---


What makes this problem hard is that each magnet interacts with all the other magnets, says UMD research scientist Zhexuan Gong, lead theorist and co-author on the study. Lasers manipulate an array of more than 50 atomic qubits to study the dynamics of quantum magnetism. The physicists can change the relative strengths of the laser beams and observe which phase wins out under different laser conditions. The researchers observe how the qubit magnets organize as different phases form, dynamics that the authors say are nearly impossible to calculate using conventional means when there are so many interactions. This quantum simulator is suitable for probing magnetic matter and related problems.

<hr>

[Visit Link](https://phys.org/news/2017-11-quantum-simulators-wield-qubits.html){:target="_blank" rel="noopener"}


