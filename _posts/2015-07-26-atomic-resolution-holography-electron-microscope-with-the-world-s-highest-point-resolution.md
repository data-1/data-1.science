---
layout: post
title: "Atomic-resolution holography electron microscope with the world's highest point resolution"
date: 2015-07-26
categories:
author: ""   
tags: [Transmission electron microscopy,Electron,Lens,Optical microscope,Electricity,Science,Electromagnetic radiation,Physical sciences,Applied and interdisciplinary physics,Chemistry,Physics,Optics,Electrical engineering,Electromagnetism,Atomic molecular and optical physics]
---


For example, materials for magnets must have not only high magnetic performance at room temperatures, but they are also required to be usable under high temperatures and high magnetic fields. In 2000, Hitachi in collaboration with the University of Tokyo developed an 1-MV holography electron microscope financially supported by CREST program of the Japan Science and Technology Agency (JST) and achieved point resolution of 120 pm making it possible to measure electromagnetic fields in the microscopic region. To improve the resolution of the microscope to the utmost limit, the following technical developments were made including an increase in the acceleration voltage to 1.2 MV resulting in decrease in electron beam wavelength and installation of a spherical-aberration corrector. Fig.2 Observation case of GaN crystal  In the current project, a 1.2-MV electron beam with suppressed energy dispersions and a high-stability field-emission electron gun have been achieved successfully, and they made it possible to improve the stability of the electron microscope system. And in doing so, Hitachi will contribute to the advancement of quantum mechanics, condensed-matter physics, materials science and technology, and other fields while developing new materials that will support a sustainable society.

<hr>

[Visit Link](http://phys.org/news343642383.html){:target="_blank" rel="noopener"}


