---
layout: post
title: "How Green is Your State?"
date: 2015-12-24
categories:
author: Pennenergy Editors, Root, --Ppa-Color-Scheme, --Ppa-Color-Scheme-Active
tags: [Renewable energy,Energy development,Hydroelectricity,Solar power,Electricity generation,Environmental technology,Economic development,Natural environment,Sustainability,Electricity,Economy,Economy and the environment,Natural resources,Energy technology,Sustainable technologies,Climate change mitigation,Electric power,Energy and the environment,Sustainable development,Renewable resources,Physical quantities,Sustainable energy,Nature,Energy,Power (physics),Technology]
---


Sustainable business practices and environmental impact via engineering renewable energy are keys to naming 10 of the greenest states in America, according to Olivet Nazarene University’s engineering department. In fact, in 2013, 78 percent of Idaho’s net generated electricity came from renewable sources of energy, primarily via geothermal energy sources. Delaware’s renewable portfolio standard requires retail electricity suppliers to generate 25 percent of the electricity sold in the state from renewable energy resources, with at least 3.5 percent from photovoltaics, by the compliance year June 2025 to May 2026. The Aloha State also generated electricity from renewable sources, about 18 percent, ahead of its goal of 40 percent by 2030. 10 Iowa’s energy was produced by wind power, and the state was ranked third in the production of non-hydroelectric renewable energy.

<hr>

[Visit Link](http://www.renewableenergyworld.com/articles/2015/12/energy-news-how-green-is-your-state.html){:target="_blank" rel="noopener"}


