---
layout: post
title: "Where bread began: Ancient tools used to reconstruct -- and taste -- prehistoric cuisine"
date: 2016-06-14
categories:
author: "Bar-Ilan University"
tags: [Mortar and pestle,Cereal,Barley,Natufian culture,Neolithic,Flour,Staple foods,Agriculture,Food and drink]
---


Using 12,500-year-old conical mortars carved into bedrock, they reconstructed how their ancient ancestors processed wild barley to produce groat meals, as well as a delicacy that might be termed proto-pita - small loaves of coal-baked, unleavened bread. The research team, consisting of independent researchers as well as faculty members from Bar-Ilan and Harvard Universities, conducted their study in the Late Natufian site of Huzuq Musa, located in Israel's Jordan Valley. Most investigators agree that cereal domestication was achieved about 10,500 years ago. At this point, the conical mortars were used to complete the transformation of wild grain into groats and flour that could be used for food, says team member Adiel Karty, explaining that the different-sized mortars served specific agricultural purposes. Evolution and Contribution  Prof. Ofer Bar-Yosef, an emeritus faculty member at Harvard who is a world-renowned expert on the origin of modern humans and early farming societies in the ancient Near East, says that the current study complements nearly 80 years of investigations suggesting that the Natufians - although subsisting as a hunter-gatherer society - used sickles to harvest wild, almost-ripe cereals, and were capable of producing large quantities of groat meals from roasted, half green barley grain.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/bu-wbb082515.php){:target="_blank" rel="noopener"}


