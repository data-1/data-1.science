---
layout: post
title: "'Weyl-Kondo semimetal': Physicists discover new type of quantum material"
date: 2018-07-02
categories:
author: "Rice University"
tags: [Superconductivity,Kondo effect,Weyl semimetal,Electron,High-temperature superconductivity,Fermion,Physics,Theoretical physics,Condensed matter physics,Condensed matter,Applied and interdisciplinary physics,Quantum mechanics,Phases of matter,Science,Materials science,Materials,Physical sciences,Electromagnetism,Chemistry]
---


In a new study due this week in the Early Edition of the Proceedings of the National Academy of Sciences (PNAS), Rice University theoretical physicist Qimiao Si and colleagues at the Rice Center for Quantum Materials in Houston and the Vienna University of Technology in Austria make predictions that could help experimental physicists create what the authors have coined a Weyl-Kondo semimetal, a quantum material with an assorted collection of properties seen in disparate materials like topological insulators, heavy fermion metals and high-temperature superconductors. The theory has allowed Si and colleagues to make a host of predictions about the quantum behavior that will arise in particular types of material as the materials are cooled to the quantum critical point. Collective behavior such as quantum criticality and high-temperature superconductivity have always been the center of our attention. The topological conductors, however, carry electricity in the bulk, thanks to the Weyl fermions. Si said this effect is huge, even by the standard of strongly correlated electron systems, and the work points to a larger principle.

<hr>

[Visit Link](https://phys.org/news/2017-12-weyl-kondo-semimetal-physicists-quantum-material.html){:target="_blank" rel="noopener"}


