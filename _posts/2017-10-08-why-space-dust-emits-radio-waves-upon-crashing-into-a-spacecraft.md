---
layout: post
title: "Why space dust emits radio waves upon crashing into a spacecraft"
date: 2017-10-08
categories:
author: "American Institute of Physics"
tags: [Electromagnetic radiation,Plasma (physics),Electron,Emission spectrum,Radio,Physics,Satellite,Particle-in-cell,Particle,Space debris,Radio wave,Dust,Simulation,Nature,Electromagnetism,Physical phenomena,Physical sciences,Space science,Applied and interdisciplinary physics,Electrical engineering,Science]
---


A new simulation provides the first mechanism to explain why plasma from hypervelocity impacts generates electromagnetic radiation  WASHINGTON, D.C., May 2, 2017 -- When spacecraft and satellites travel through space they encounter tiny, fast moving particles of space dust and debris. They show that as the plasma expands into the surrounding vacuum, the ions and electrons travel at different speeds and separate in a way that creates radio frequency emissions. For the last few decades researchers have studied these hypervelocity impacts and we've noticed that there's radiation from the impacts when the particles are going sufficiently fast, said lead author Alex Fletcher, now a postdoctoral researcher at the Boston University Center for Space Physics. The impact creates dust particles that interact with the plasma, Fletcher said. ###  The article, Particle-in-cell simulations of an RF emission mechanism associated with hypervelocity impact plasmas, is authored by Alex Fletcher and Sigrid Close.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-05/aiop-wsd042717.php){:target="_blank" rel="noopener"}


