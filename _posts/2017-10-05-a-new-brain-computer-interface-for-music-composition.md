---
layout: post
title: "A new brain-computer interface for music composition"
date: 2017-10-05
categories:
author: ""
tags: [Braincomputer interface,Technology,Computing]
---


Credit: Shutterstock  If that melody has just come to you, and if you know your way around a score, you might be able to think it into being now a group of researchers have developed a new brain-computer interface (BCI) application. The team has just published a paper showing that thought can power the transference of music from the composer's brain to a score. The Brain Composing system they designed consists of three parts: the EEG acquisition system, the P300 control software, and the music composing software. They then performed a copy-spelling, a copy-composing, and a free-composing task with the system, 'thinking' melodies onto a musical score. Explore further Brain Composer—'thinking' melodies onto a musical score  More information: Project website: Project website: www.moregrasp.eu/

<hr>

[Visit Link](https://phys.org/news/2017-10-brain-computer-interface-music-composition.html){:target="_blank" rel="noopener"}


