---
layout: post
title: "Bioengineers create pathway to personalized medicine"
date: 2018-07-13
categories:
author: "Cornell University"
tags: [Escherichia coli,Cell-free system,Glycosylation,Cell (biology),Synthetic biology,Glycoprotein,Cell biology,Physical sciences,Molecular biology,Technology,Chemistry,Biotechnology,Life sciences,Biochemistry,Biology]
---


Engineering cellular biology, minus the actual cell, is a growing area of interest in biotechnology and synthetic biology. Unfortunately, a long-standing gap in cell-free systems is the ability to manufacture glycosylated proteins -- proteins with a carbohydrate attachment. DeLisa and Jewett are co-senior authors of Single-pot Glycoprotein Biosynthesis Using a Cell-Free Transcription-Translation System Enriched with Glycosylation Machinery, published July 12 in Nature Communications. The resulting extracts enabled a simplified reaction scheme, which the team has dubbed cell-free glycoprotein synthesis (CFGpS). A major advance of this work is that our cell-free extracts contain all of the molecular machinery for protein synthesis and protein glycosylation, Stark said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180712143443.htm){:target="_blank" rel="noopener"}


