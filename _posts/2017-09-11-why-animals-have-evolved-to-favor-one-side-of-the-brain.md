---
layout: post
title: "Why animals have evolved to favor one side of the brain"
date: 2017-09-11
categories:
author: "Cell Press"
tags: [Brain,Handedness,News aggregator,Eye,Evolution,Neuroscience,Cognitive science]
---


But why do people and animals naturally favor one side over the other, and what does it teach us about the brain's inner workings? The first of those is perceptual specialization: the more complex a task, the more it helps to have a specialized area for performing that task. When a pigeon chick develops in the shell, its right eye turns toward the outside, leaving its left eye to face its body. By manipulating the genes in Nodal and other pathways, researchers can study the effects of these developmental changes on zebrafish behaviors. Güntürkün says that this research can provide insight into the effects of asymmetry on brain conditions in humans.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/04/170419131801.htm){:target="_blank" rel="noopener"}


