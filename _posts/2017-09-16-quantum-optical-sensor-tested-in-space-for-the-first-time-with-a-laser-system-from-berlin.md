---
layout: post
title: "Quantum optical sensor tested in space for the first time, with a laser system from Berlin"
date: 2017-09-16
categories:
author: "Forschungsverbund Berlin e.V. (FVB)"
tags: [Laser,Ultracold atom,BoseEinstein condensate,Laser diode,Interferometry,Distributed feedback laser,Equivalence principle,Electromagnetic radiation,Electrical engineering,Electromagnetism,Physical sciences,Atomic molecular and optical physics,Optics,Applied and interdisciplinary physics,Science,Physics]
---


The MAIUS mission demonstrates that quantum optical sensors can be operated even in harsh environments like space -- a prerequisite for finding answers to the most challenging questions of fundamental physics and an important innovation driver for everyday applications. More than 20 years after the groundbreaking results of the Nobel laureates Cornell, Ketterle, and Wieman on ultra-cold atoms, preliminary evaluation of the sounding rocket mission data indicates that such experiments can also be carried out under the harsh conditions of space operation -- back in 1995, living room-sized setups in a special laboratory environment were required. MAIUS, however, is not the first sounding rocket test for both institutions' laser technology in space; the technology has already been successfully tested in April 2015 and January 2016 on board of two sounding rockets within the FOKUS and KALEXUS experiments. Quantum optical sensors based on BECs enable high-precision measurements of accelerations and rotations using laser pulses which provide a reference for precise determination of the positions of the atomic cloud. These tapered amplifier chips boost the optical output power of a DFB laser to beyond 1 W without any loss of spectral stability.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/01/170123090842.htm){:target="_blank" rel="noopener"}


