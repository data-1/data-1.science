---
layout: post
title: "Revolutions in understanding the ionosphere, Earth's interface to space"
date: 2017-03-19
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Ionosphere,Atmosphere of Earth,Atmosphere,Sun,Earth,Lightning,Coronal mass ejection,Space weather,Earths magnetic field,Magnetosphere,Solar wind,Aurora,Energy,Goddard Space Flight Center,Thermosphere,Mesosphere,Weather,Physical phenomena,Physics,Outer space,Electromagnetism,Physical sciences,Applied and interdisciplinary physics,Sky,Astronomy,Nature,Space science]
---


New research shows that this understanding of the upper atmosphere's response to solar storms - and the resulting satellite drag - may not always hold true. Competing with this cooling process is the heating that caused by solar storm energy making its way into Earth's atmosphere. Because solar storms enhance the electric currents that let this magnetosphere-ionosphere lightning take place, this type of energy transfer is much more likely when Earth's magnetic field is jostled by a solar event. The huge energy transfer from this magnetosphere-ionosphere lightning is associated with heating of the ionosphere and upper atmosphere, as well as increased aurora. The ionosphere doesn't only react to energy input by solar storms, said Scott England, a space scientist at the University of California, Berkeley, who works on both the ICON and GOLD missions.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-12/nsfc-riu121416.php){:target="_blank" rel="noopener"}


