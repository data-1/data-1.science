---
layout: post
title: "Mapping coal's decline and the renewables' rise"
date: 2016-06-24
categories:
author: "Francesca Mccaffrey, Massachusetts Institute Of Technology"
tags: [Sustainable energy,Coal,Renewable energy,Wind power,Levelized cost of energy,Fossil fuel power station,Low-carbon economy,Coal-fired power station,Cost of electricity by source,Carbon pricing in Australia,Energy development,Solar energy,Power station,Photovoltaics,Low-carbon power,Energy,Nature,Technology]
---


Coal, after all, is cheap—or so it seems. The online tool they've created to help illustrate this argument is CoalMap, a web application that compares the levelized cost of electricity (LCOE)—that is, the minimum electricity price a power plant must receive to break even on investment costs over its lifecycle—of existing U.S. coal-fired plants with the expected LCOE of potential new utility-scale solar and wind generation in the same locations. The team that conceptualized CoalMap at the 2015 MIT Clean Earth Hackathon: (left to right) Joel Jean, Fanni Fan, Tony Wu, and David Borrelli. In the event of both a carbon price and improvements in clean energy technology, the researchers say, nearly all aging coal-fired plants in the U.S. could be headed for retirement within the next two decades, displaced by cheap low-carbon energy generation, even without subsidies and in areas with poor solar and wind resource. There, the team—made up of Wu and Jean, both of the Department of Electrical Engineering and Computer Science; Borrelli, an alumnus of the Department of Chemical Engineering; and Fanni Fan, an MIT Sloan School of Management MFin student—won in the energy category, rising to the Rocky Mountain Institute's challenge for hackathon participants to build a compelling map that could help regulators and policymakers take action against heavily polluting or uneconomic coal plants.

<hr>

[Visit Link](http://phys.org/news/2016-06-coal-decline-renewables.html){:target="_blank" rel="noopener"}


