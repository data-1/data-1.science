---
layout: post
title: "Solar energy prices see double-digit declines in 2013; Trend expected to continue"
date: 2015-12-04
categories:
author: "$author" 
tags: [National Renewable Energy Laboratory,Photovoltaics,Renewable energy,Photovoltaic system,SunShot Initiative,Electric power,Nature,Sustainable energy,Energy,Physical quantities,Renewable resources,Sustainable technologies,Energy and the environment,Technology,Climate change mitigation,Sustainable development,Power (physics),Solar energy,Electricity]
---


Distributed solar photovoltaic (PV) system prices dropped by 12 - 19 percent nationwide in 2013, according to the third edition of a jointly written report on PV pricing trends from the Energy Department's (DOE) National Renewable Energy Laboratory (NREL) and Lawrence Berkeley National Laboratory (LBNL). In addition, 2014 prices are expected to drop another 3 - 12 percent, depending on system location and market segment. Industry analysts expect this trend to continue over the next couple of years, keeping the nation on track to meet the DOE SunShot Initiative's 2020 targets. Other factors include differences in specific system configurations such as panel efficiency, mounting structure, and geographic location; and the time lags between commitments and commercial operation for utility-scale systems. Learn more at http://energy.gov/sunshot  NREL is the U.S. Department of Energy's primary national laboratory for renewable energy and energy efficiency research and development.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/drel-sep102114.php){:target="_blank" rel="noopener"}


