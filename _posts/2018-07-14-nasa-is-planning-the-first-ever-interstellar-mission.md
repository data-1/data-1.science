---
layout: post
title: "NASA Is Planning the First-Ever Interstellar Mission"
date: 2018-07-14
categories:
author: ""
tags: [NASA,Solar sail,Alpha Centauri,Exoplanet,Interstellar travel,Science,Spacecraft,Flight,Outer space,Spaceflight,Astronomy,Space science,Astronautics]
---


A probe is set to be sent to the chosen exoplanet to determine whether or not life is present. He later described its current status as nebulous in an interview with New Scientist. Antimatter-matter collisions, nuclear propulsion, and laser-powered light sails are all being considered as technologies that could potentially help a craft reach the speeds that are required. Even if the craft can travel fast enough to make the journey to another system, the trip would likely take centuries. It will be no small feat to develop the propulsion technology necessary to get this project off the ground, and the practicalities of carrying out a mission that will last decades upon decades offer up their own challenges.

<hr>

[Visit Link](https://futurism.com/nasa-planning-first-ever-interstellar-mission/){:target="_blank" rel="noopener"}


