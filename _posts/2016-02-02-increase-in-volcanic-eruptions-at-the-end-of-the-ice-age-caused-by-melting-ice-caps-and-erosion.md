---
layout: post
title: "Increase in volcanic eruptions at the end of the ice age caused by melting ice caps and erosion"
date: 2016-02-02
categories:
author: University Of Cambridge
tags: [Ice age,Magma,Volcano,Earth,Climate change,Ice,Types of volcanic eruptions,Martian polar ice caps,Geophysics,Earth sciences,Physical geography,Geology,Nature,Climate variability and change,Physical sciences,Planetary science,Structure of the Earth,Natural environment,Applied and interdisciplinary physics,Lithosphere,Climate]
---


Credit: Pietro Sternai  The combination of erosion and melting ice caps led to a massive increase in volcanic activity at the end of the last ice age, according to new research. As the climate warmed, the ice caps melted, decreasing the pressure on the Earth's mantle, leading to an increase in both magma production and volcanic eruptions. The researchers, led by the University of Cambridge, have found that erosion also played a major role in the process, and may have contributed to an increase in atmospheric carbon dioxide levels. Using numerical simulations, which modelled various different features such as ice caps and glacial erosion rates, Sternai and his colleagues from the University of Geneva and ETH Zurich found that erosion is just as important as melting ice in driving the increase in magma production and subsequent volcanic activity. There are several factors that contribute to climate warming and cooling trends, and many of them are related to the Earth's orbital parameters, said Sternai.

<hr>

[Visit Link](http://phys.org/news/2016-02-volcanic-eruptions-ice-age-caps.html){:target="_blank" rel="noopener"}


