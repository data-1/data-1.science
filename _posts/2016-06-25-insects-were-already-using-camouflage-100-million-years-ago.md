---
layout: post
title: "Insects were already using camouflage 100 million years ago"
date: 2016-06-25
categories:
author: "University of Bonn"
tags: [Camouflage,Predation,Neuroptera,Insect,Animals]
---


Insects were already doing something very similar in the Cretaceous: They cloaked themselves in pieces of plants, grains of sand, or the remains of their prey, in order, for example, to be invisible to predators. This camouflage protects the lacewing against being recognized by predators and at the same makes it easier to hunt its own prey. Today there are still numerous types of insects that make themselves invisible, for instance using grains of sand, such as caddis-fly larvae in rivers and streams. Based on the camouflage of the various amber insects, the research team reached conclusions about their habitat at the time. In this way, the costumed larvae were trapped in the tree resin and the scene preserved to this today.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/uob-iwa062416.php){:target="_blank" rel="noopener"}


