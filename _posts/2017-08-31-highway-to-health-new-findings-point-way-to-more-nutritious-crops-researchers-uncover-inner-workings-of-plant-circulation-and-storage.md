---
layout: post
title: "Highway to health: New findings point way to more nutritious crops: Researchers uncover inner workings of plant circulation and storage"
date: 2017-08-31
categories:
author: "Washington State University"
tags: [Plasmodesma,News aggregator,Plants,Chemistry,Botany,Kingdoms (biology),Organisms,Biology,Archaeplastida]
---


What we eat is mostly fruits, roots and seeds, cereals and so on, said Knoblauch. Facilitating the process are plasmodesmata, pores connecting neighboring cells. They also saw for the first time unique structures called funnel plasmodesmata. So if we are able to increase the sink strength of a specific sink of interest, then we can draw to this sink more nutrients of interest. If we want to make a tomato fruit a stronger sink by modifying phloem unloading, we draw more of the nutrients to the fruit and make more fruit product.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/03/170328135554.htm){:target="_blank" rel="noopener"}


