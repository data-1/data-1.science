---
layout: post
title: "Gene editing in the brain gets a major upgrade"
date: 2017-10-20
categories:
author: Max Planck Florida Institute For Neuroscience
tags: [CRISPR gene editing,Genome editing,Brain,Adeno-associated virus,Virus,Cas9,Molecular genetics,Branches of genetics,Biochemistry,Molecular biology,Life sciences,Genetics,Biotechnology,Biology]
---


When precursor brain cells mature into neurons, they are referred to as post-mitotic or nondividing cells, making the mature brain largely inaccessible to HDR - or so researchers previously thought. Credit: Max Planck Florida Institute for Neuroscience  Adeno-associated virus (AAV) is a low immunogenic, nontoxic virus utilized by scientists as an efficient delivery mechanism for all kinds of genes. They tested this dual-viral system in an aged Alzheimer's disease mouse model showing that the vSLENDR technique can be applicable in pathological models even at advanced ages. Credit: Max Planck Florida Institute for Neuroscience  vSLENDR is a powerful new tool for both basic and translational sciences alike, capable of the precise editing of genetic information regardless of cell type, cell maturity, brain region, or age. The new vSLENDR is more efficient, flexible, and concise, allowing researchers the potential to study a myriad of brain processes and functionalities with unprecedented ease.

<hr>

[Visit Link](https://phys.org/news/2017-10-gene-brain-major.html){:target="_blank" rel="noopener"}


