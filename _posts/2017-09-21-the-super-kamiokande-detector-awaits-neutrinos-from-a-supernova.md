---
layout: post
title: "The Super-Kamiokande detector awaits neutrinos from a supernova"
date: 2017-09-21
categories:
author: "Spanish Foundation for Science and Technology"
tags: [Neutrino,Super-Kamiokande,Kamioka Observatory,Supernova,Cosmic ray,Star,Science,Nature,Astrophysics,Space science,Astronomy,Physical sciences,Physics]
---


Only three or four supernovas happen in our galaxy every century. At the Super-Kamiokande detector in Japan, a new computer system has been installed in order to monitor in real time and inform the scientific community of the arrival of these mysterious particles, which can offer crucial information on the collapse of stars and the formation of black holes. This is the Super-Kamiokande experiment, one of the major objectives of which is the detection of neutrinos -particles with near-zero mass- that come from nearby supernovas. Supernova explosions are one of the most energetic phenomena in the universe and most of this energy is released in the form of neutrinos, says Labarga. The Super-Kamiokande Collaboration.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/f-sf-tsd111016.php){:target="_blank" rel="noopener"}


