---
layout: post
title: "Nuclear physicists leap into quantum computing with first simulations of atomic nucleus"
date: 2018-07-03
categories:
author: "DOE/Oak Ridge National Laboratory"
tags: [Quantum mechanics,Quantum computing,Oak Ridge National Laboratory,Nuclear physics,Computing,Atomic nucleus,Neutron,Proton,Deuterium,Simulation,System,Quantum information,Science,Computer,Physics,Subatomic particle,Energy,Measurement,Theoretical physics,Physical sciences,Branches of science,Technology]
---


Unlike normal computer bits, the qubit units used by quantum computers store information in two-state systems, such as electrons or photons, that are considered to be in all possible quantum states at once (a phenomenon known as superposition). In classical computing, you write in bits of zero and one, said Thomas Papenbrock, a theoretical nuclear physicist at the University of Tennessee and ORNL who co-led the project with ORNL quantum information specialist Pavel Lougovski. In October 2017 the multidivisional ORNL team started developing codes to perform simulations on the IBM QX5 and the Rigetti 19Q quantum computers through DOE's Quantum Testbed Pathfinder project, an effort to verify and validate scientific applications on different quantum hardware types. The team performed more than 700,000 quantum computing measurements of the energy of a deuteron, the nuclear bound state of a proton and a neutron. These systems aren't perfect, but in working with them, we can gain a better understanding of the intrinsic errors.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/drnl-npl052318.php){:target="_blank" rel="noopener"}


