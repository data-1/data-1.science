---
layout: post
title: "Mars-bound astronauts face chronic dementia risk from galactic cosmic ray exposure"
date: 2017-09-23
categories:
author: "University of California - Irvine"
tags: [Health threat from cosmic rays,Neuroscience,Health]
---


Irvine, Calif. -- Will astronauts traveling to Mars remember much of it? UCI's Charles Limoli and colleagues found that exposure to highly energetic charged particles - much like those found in the galactic cosmic rays that will bombard astronauts during extended spaceflights - causes significant long-term brain damage in test rodents, resulting in cognitive impairments and dementia. Deficits in fear extinction could make you prone to anxiety, Limoli said, which could become problematic over the course of a three-year trip to and from Mars. Limoli's work is part of NASA's Human Research Program. Investigating how space radiation affects astronauts and learning ways to mitigate those effects are critical to further human exploration of space, and NASA needs to consider these risks as it plans for missions to Mars and beyond.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/uoc--maf100716.php){:target="_blank" rel="noopener"}


