---
layout: post
title: "Whale behaviour linked to brain size"
date: 2017-10-18
categories:
author:  
tags: []
---


In terms of sheer brain size, the sperm whale is top on Earth, with a brain six times larger than that of a human being. Therefore, we have only a glimpse of what they are capable of.”  The researchers created a comprehensive database of brain size, social structures and cultural behaviours across cetacean species. The group of species with the largest brain size relative to body size was the large whale-like dolphins such as the killer whale, the similar-looking false killer whale and the pilot whale, Ms. Shultz said. Food preferences  “Killer whales have cultural food preferences, have matriarchs that lead and teach other group members, and cooperatively hunt,” Ms. Shultz said. The distinctive vocalisations sperm whales use to communicate sometimes differ depending upon where they live, much like regional dialects in human language.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/whale-behaviour-linked-to-brain-size/article19877612.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


