---
layout: post
title: "Bridging the advances in AI and quantum computing for drug discovery and longevity research"
date: 2017-03-23
categories:
author: "InSilico Medicine"
tags: [Photonics,Deep learning,Insilico Medicine,Computing,Artificial intelligence,Biomarker,Technology,Drug discovery,Ageing,Science,Branches of science,Cognitive science]
---


Tuesday, 22nd of November, 2016, Baltimore, MD - Insilico Medicine, Inc and YMK Photonics, Inc announced today a research collaboration and business cooperation to develop photonics quantum computing and accelerated deep learning techniques for drug discovery, biomarker development and aging research. Insilico Medicine made substantial advances in applying deep learning techniques to drug development and aging research, but to accelerate the process and simulate entire human bodies and populations or generate optimal molecular structures, they could really benefit from our expertise in quantum computing. One of such niches is quantum computing for deep learning. http://www.ymkholdings.com  About YMK Photonics  YMK Photonics Co., Ltd., funded by YMK Holdings, is a developing photonics businesses with leading Korean conglomerates and medium-sized competitive enterprises. Through its Pharma.AI division the company provides advanced machine learning services to biotechnology, pharmaceutical and skin care companies.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/imi-bta111716.php){:target="_blank" rel="noopener"}


