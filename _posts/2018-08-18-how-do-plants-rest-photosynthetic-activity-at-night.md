---
layout: post
title: "How do plants rest photosynthetic activity at night?"
date: 2018-08-18
categories:
author: "Tokyo Institute of Technology"
tags: [Photosynthesis,Redox,Chloroplast,Metabolism,Biology,Physical sciences,Biochemistry,Chemistry]
---


The proteins involved in photosynthesis need to be 'on' when they have the sunlight they need to function, but need to idle, like the engine of a car at a traffic light, in the dark, when photosynthesis is not possible. They do this by a process called 'redox regulation'--the activation and deactivation of proteins via changes in their redox (reduction/oxidation) states. These two proteins appear to function as part of a cascade that siphons energy from the photosynthetic proteins to the always energy-hungry hydrogen peroxide. TrxL2, unlike similar, better-known proteins, seems to be specialized for the 'switching off' process; it's an efficient oxidizer of many proteins, but only reduces 2CP, allowing the energy drained by TrxL2 from several upstream reactions to pass to 2CP and thence hydrogen peroxide. This cascade thus keeps photosynthesis on standby until light is available again.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/tiot-hdp081618.php){:target="_blank" rel="noopener"}


