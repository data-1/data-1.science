---
layout: post
title: "Scientists unlock tangled mysteries of DNA"
date: 2016-04-17
categories:
author: Joann C. Adkins
tags: [Evolution,DNA,Protein,Cell (biology),Mutation,Chromosome,Life,Biology,Genetics,Life sciences,Biotechnology,Molecular biology,Biochemistry]
---


It is up to a group of proteins known as chromosomal proteins to unlock the information required to trigger a function in a given cell—to form a bone, determine eye color, metabolize food, fight infections or any other function. The team of researchers is the first to explain the mechanisms responsible for the evolutionary diversification of a specific group of chromosomal proteins known as High Mobility Group Nucleosome-binding (HMG-N) proteins. The research unveils the mechanisms responsible for the functional specialization of this group of proteins, from a common ancestor directing a variety of activities to the actual HMG-N lineages working in concert in vertebrate organisms including humans. However, along with a better cell performance, a higher number of chromosomal proteins also provide more potential targets for harmful mutations. If one or more of these proteins are altered or mutated they will target wrong genes in the DNA, giving erroneous instructions to cells.

<hr>

[Visit Link](http://phys.org/news344862849.html){:target="_blank" rel="noopener"}


