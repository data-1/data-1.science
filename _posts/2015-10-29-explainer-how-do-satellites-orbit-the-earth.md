---
layout: post
title: "Explainer: How do satellites orbit the Earth?"
date: 2015-10-29
categories:
author: Michael J. I. Brown, The Conversation
tags: [Orbit,Earth,Weightlessness,Satellite,Moon,Rocket,Gravity,Mechanics,Physical sciences,Classical mechanics,Spaceflight,Science,Flight,Astronomy,Space science,Outer space,Solar System,Celestial mechanics,Motion (physics)]
---


Our moon orbits the Earth in the same way satellites do. Because the moon is in orbit. The cannon ball would follow the curvature of the Earth, being pulled towards the Earth by gravity but never reaching the ground. Going up  Newton's Cannon remains a thought experiment but in the 20th century it finally became possible to travel at speeds of 8 kilometres per second. Your spacecraft will travel away from the Earth and be slowed by gravity, but the gravitational pull of the Earth drops so rapidly that it will never stop you entirely.

<hr>

[Visit Link](http://phys.org/news326963710.html){:target="_blank" rel="noopener"}


