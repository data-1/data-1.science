---
layout: post
title: "NASA's GRACE satellites evaluate drought in southeast Brazil"
date: 2016-06-27
categories:
author: "NASA/Goddard Space Flight Center"
tags: [GRACE and GRACE-FO,Water,Goddard Space Flight Center,Reservoir,Earth sciences,Spaceflight,Physical geography,Nature,Hydrology,Outer space]
---


said Getirana. To answer them, he used data from NASA's Gravity Recovery and Climate Experiment (GRACE) satellites. The pair of satellites orbit Earth in precise formation and detect changes in Earth's gravity field. A new data visualization of 13 years of GRACE data shows the distribution of water across Brazil. Southeastern Brazil was hardest hit by drought conditions, said Getirana.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/nsfc-ngs102815.php){:target="_blank" rel="noopener"}


