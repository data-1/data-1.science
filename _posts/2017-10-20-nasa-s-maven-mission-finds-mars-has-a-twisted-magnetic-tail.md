---
layout: post
title: "NASA's MAVEN mission finds Mars has a twisted magnetic tail"
date: 2017-10-20
categories:
author: NASA/Goddard Space Flight Center
tags: [MAVEN,Mars,Magnetosphere,Goddard Space Flight Center,Solar wind,Sun,Planets,Science,Terrestrial planets,Bodies of the Solar System,Physical sciences,Planets of the Solar System,Space science,Astronomy,Solar System,Astronomical objects known since antiquity,Spaceflight,Planetary science,Outer space]
---


Mars has an invisible magnetic tail that is twisted by interaction with the solar wind, according to new research using data from NASA's MAVEN spacecraft. According to the new work, Mars' magnetotail is formed when magnetic fields carried by the solar wind join with the magnetic fields embedded in the Martian surface in a process called magnetic reconnection. If the solar wind field happens to be oriented in the opposite direction to a field in the Martian surface, the two fields join together in magnetic reconnection. Since the Martian magnetotail is formed by linking surface magnetic fields to solar wind fields, ions in the Martian upper atmosphere have a pathway to space if they flow down the magnetotail. MAVEN began its primary science mission on November 2014, and is the first spacecraft dedicated to understanding Mars' upper atmosphere.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171019181855.htm){:target="_blank" rel="noopener"}


