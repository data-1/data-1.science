---
layout: post
title: "LISA Pathfinder exceeds expectations"
date: 2016-06-12
categories:
author: ""
tags: [Laser Interferometer Space Antenna,LISA Pathfinder,Gravitational wave,Gravitational-wave observatory,Science,Space science,Astronomy,Physics,Astrophysics,Physical sciences,Outer space]
---


Science & Exploration LISA Pathfinder exceeds expectations 07/06/2016 20880 views 188 likes  ESA’s LISA Pathfinder mission has demonstrated the technology needed to build a space-based gravitational wave observatory. Results from only two months of science operations show that the two cubes at the heart of the spacecraft are falling freely through space under the influence of gravity alone, unperturbed by other external forces, to a precision more than five times better than originally required. LISA Technology Package The first two months of data show that, in the frequency range between 60 mHz and 1 Hz, LISA Pathfinder's precision is only limited by the sensing noise of the laser measurement system used to monitor the position and orientation of the cubes. “At the precision reached by LISA Pathfinder, a full-scale gravitational wave observatory in space would be able to detect fluctuations caused by the mergers of supermassive black holes in galaxies anywhere in the Universe,” says Karsten Danzmann, director at the Max Planck Institute for Gravitational Physics, director of the Institute for Gravitational Physics of Leibniz Universität Hannover, Germany, and Co-Principal Investigator of the LISA Technology Package. Science operations involving the full LISA Technology Package will last until late June, followed by three months of operations with the Disturbance Reduction System.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/LISA_Pathfinder_exceeds_expectations){:target="_blank" rel="noopener"}


