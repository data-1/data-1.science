---
layout: post
title: "Tissue-engineered human pancreatic cells successfully treat diabetic mice: Self-condensation process for cells generates vascularized organ tissues for transplant"
date: 2018-05-21
categories:
author: "Cincinnati Children's Hospital Medical Center"
tags: [Pancreatic islets,Induced pluripotent stem cell,Tissue engineering,Type 1 diabetes,Pancreas,Organoid,Cell biology,Biotechnology,Anatomy,Medicine,Biology,Life sciences,Medical specialties,Clinical medicine]
---


Researchers tissue-engineered human pancreatic islets in a laboratory that develop a circulatory system, secrete hormones like insulin and successfully treat sudden-onset type 1 diabetes in transplanted mice. Using either donated organ cells, mouse cells or iPS cells, the researchers combined these with MSNs, HUVECs along with other genetic and biochemical material that cue the formation of pancreatic islets. Blood Source Challenge  Human pancreatic islets already can be transplanted into diabetic patients for treatment. Takebe's and Taniguchi's research team already demonstrated the ability to use a self-condensation cell culture process using iPS cells to tissue engineer three-dimensional human liver organoids that can vascularize after transplant into laboratory mice. But the ability to generate organ tissue fragments that vascularize in the body -- like pancreatic islets -- had been an elusive goal until the current study, investigators said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/05/180508111745.htm){:target="_blank" rel="noopener"}


