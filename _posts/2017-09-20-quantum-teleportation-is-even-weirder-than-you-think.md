---
layout: post
title: "Quantum teleportation is even weirder than you think"
date: 2017-09-20
categories:
author: "Ball, Philip Ball, You Can Also Search For This Author In"
tags: []
---


Whatever it’s called, the process transfers the quantum state of one particle onto another, identical particle, and at the same time erases the state in the original. Crucially, however, this works even if you do not know what ‘information’ you are sending — that is, what the quantum state of the original particle actually is. Alice also has another particle C: one whose quantum state she does not know, but wants to teleport onto B. What is information? So what exactly is being transmitted through entanglement alone?

<hr>

[Visit Link](http://www.nature.com/news/quantum-teleportation-is-even-weirder-than-you-think-1.22321?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


