---
layout: post
title: "Quantum teleportation on a chip"
date: 2015-07-26
categories:
author: University of Bristol 
tags: [Quantum teleportation,Quantum mechanics,Photonics,Electrical engineering,Technology,Applied and interdisciplinary physics,Electromagnetic radiation,Electromagnetism,Atomic molecular and optical physics,Branches of science,Theoretical physics,Optics,Science,Physics]
---


The core circuits of quantum teleportation, which generate and detect quantum entanglement, have been successfully integrated into a photonic chip by an international team of scientists from the universities of Bristol, Tokyo, Southampton and NTT Device Technology Laboratories. One of the most important tasks is to successfully enable quantum teleportation, which transfers qubits from one photon to another. The team of researchers have taken a significant step closer towards their ultimate goal of integrating a quantum computer into a photonic chip. One of the most important steps in achieving this is to establish technologies for quantum teleportation (transferring signals of quantum bits in photons from a sender to a receiver at a distance). Professor Akira Furusawa from the University of Tokyo said: This latest achievement enables us to perform the perfect quantum teleportation with a photonic chip.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/uob-qto040115.php){:target="_blank" rel="noopener"}


