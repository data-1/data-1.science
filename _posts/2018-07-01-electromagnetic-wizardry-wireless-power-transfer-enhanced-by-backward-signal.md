---
layout: post
title: "Electromagnetic wizardry: Wireless power transfer enhanced by backward signal"
date: 2018-07-01
categories:
author: "Moscow Institute Of Physics"
tags: [Antenna (radio),Electromagnetic radiation,Wireless power transfer,Inductor,Wave,Transformer,Inductive charging,Transmitter,Electricity,Telecommunications engineering,Information and communications technology,Physics,Physical phenomena,Electromagnetism,Technology,Waves,Physical quantities,Electrodynamics,Electronics,Electrical engineering]
---


Credit: Alex Krasnok et al./Physical Review Letters  An international research team including scientists from the Moscow Institute of Physics and Technology and ITMO University has proposed a way to increase the efficiency of wireless power transfer over long distances and tested it with numerical simulations and experiments. And that is also why wireless chargers operate over distances less than 3-5 centimeters. Importantly, the receiving antenna does not absorb all of the incident radiation but reradiates some of it back. If, however, the receiver transmits an auxiliary signal back to the antenna and the signal's amplitude and phase match those of the incident wave, the two will interfere, potentially altering the proportion of extracted energy. However, for a detuned antenna whose decay times differ significantly—that is, when τF is several times larger than τw, or the other way round—the auxiliary signal has a noticeable effect.

<hr>

[Visit Link](https://phys.org/news/2018-04-electromagnetic-wizardry-wireless-power.html){:target="_blank" rel="noopener"}


