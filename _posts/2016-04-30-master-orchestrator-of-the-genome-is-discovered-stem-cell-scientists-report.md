---
layout: post
title: "Master orchestrator of the genome is discovered, stem cell scientists report"
date: 2016-04-30
categories:
author: Ellen Goldbaum, University At Buffalo
tags: [Developmental biology,Gene,Stem cell,Protein,Fibroblast growth factor receptor 1,Cell (biology),Embryonic stem cell,Cellular differentiation,Genome,Transcription factor,Nervous system,Cell biology,Biological processes,Molecular biology,Genetics,Biochemistry,Life sciences,Biotechnology,Biology]
---


Organizing 'this cacophony of genes'  We've known that the human body has almost 30,000 genes that must be controlled by thousands of transcription factors that bind to those genes, Stachowiak said, yet we didn't understand how the activities of genes were coordinated so that they properly develop into an organism. To study how nuclear FGFR1 worked, the UB team used genome-wide sequencing of mouse embryonic stem cells programmed to develop cells of the nervous system, with additional experiments in which nuclear FGFR1 was either introduced or blocked. The research shows that nuclear FGFR1 binds to promoters of genes that encode transcription factors, the proteins that control which genes are turned on or off in the genome. In the UB research, the DNA sequencing data were processed by the supercomputer at the university's Center for Computational Research (CCR). They found that the protein binds to genes that make neurons and muscles as well as to an important oncogene, TP53, which is involved in a number of common cancers.

<hr>

[Visit Link](http://phys.org/news350291092.html){:target="_blank" rel="noopener"}


