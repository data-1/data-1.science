---
layout: post
title: "Using encrypted documents with vim"
date: 2017-10-09
categories:
author: "Michael Boelen"
tags: [Encryption,Cryptography,Blowfish (cipher),Image scanner,Espionage techniques,Military communications,Cyberspace,Software engineering,Crime prevention,Cybercrime,Computer data,Digital media,Systems engineering,Software,Security technology,Computing,Technology,Information Age,Computer science,Computer security,Secure communication,Information technology management,Cyberwarfare,Information technology,Security engineering]
---


Encrypting files with vim  Everyone has secrets. Disable backups  During editing your files you may not want to leak any sensitive data. As this may contain sensitive data, disable the file if you don’t want to take any risk of leaking data. For example:  set cryptmethod=blowfish2  set nobackup  set nowritebackup  set viminfo=  With these settings in place, we can start using the encryption options vim has to offer. This will be used to mangle all data and ensure others (without the key) can’t see the data.

<hr>

[Visit Link](https://linux-audit.com/using-encrypted-documents-with-vim/){:target="_blank" rel="noopener"}


