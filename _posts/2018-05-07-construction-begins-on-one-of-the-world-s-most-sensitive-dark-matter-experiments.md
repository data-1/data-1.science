---
layout: post
title: "Construction begins on one of the world's most sensitive dark matter experiments"
date: 2018-05-07
categories:
author: "DOE/SLAC National Accelerator Laboratory"
tags: [Cryogenic Dark Matter Search,Weakly interacting massive particles,Dark matter,Astrophysics,Physics,Physical sciences,Particle physics,Nature,Science]
---


The SuperCDMS SNOLAB project, a multi-institutional effort led by SLAC, is expanding the hunt for dark matter to particles with properties not accessible to any other experiment  Menlo Park, Calif. -- The U.S. Department of Energy has approved funding and start of construction for the SuperCDMS SNOLAB experiment, which will begin operations in the early 2020s to hunt for hypothetical dark matter particles called weakly interacting massive particles, or WIMPs. The DOE's SLAC National Accelerator Laboratory is managing the construction project for the international SuperCDMS collaboration of 111 members from 26 institutions, which is preparing to do research with the experiment. SNOLAB is excited to welcome the SuperCDMS SNOLAB collaboration to the underground lab, said Kerry Loken, SNOLAB project manager. Fermilab's Dan Bauer, spokesperson of the SuperCDMS collaboration, said, Together we're now ready to build an experiment that will search for dark matter particles that interact with normal matter in an entirely new region. DOE's Office of Science is the single largest supporter of basic research in the physical sciences in the United States, and is working to address some of the most pressing challenges of our time.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/dnal-cbo050718.php){:target="_blank" rel="noopener"}


