---
layout: post
title: "Image: Unprecedented detail in enhanced high-resolution color image of Pluto"
date: 2016-08-01
categories:
author: ""
tags: [New Horizons,Technology]
---


Credit: NASA/JHUAPL/SwRI  NASA's New Horizons spacecraft captured this high-resolution enhanced color view of Pluto on July 14, 2015. Pluto's surface sports a remarkable range of subtle colors, enhanced in this view to a rainbow of pale blues, yellows, oranges, and deep reds. Many landforms have their own distinct colors, telling a complex geological and climatological story that scientists have only just begun to decode. The image resolves details and colors on scales as small as 0.8 miles (1.3 kilometers). The viewer is encouraged to zoom in on the image on a larger screen to fully appreciate the complexity of Pluto's surface features.

<hr>

[Visit Link](http://phys.org/news/2015-09-image-unprecedented-high-resolution-pluto.html){:target="_blank" rel="noopener"}


