---
layout: post
title: "Image: The effect of the winds of Mars"
date: 2016-05-07
categories:
author: European Space Agency
tags: [Mars,Impact crater,Arabia Terra,Planetary science,Solar System,Space science,Terrestrial planets,Bodies of the Solar System,Astronomy,Planets,Outer space,Astronomical objects known since antiquity,Planets of the Solar System]
---


Credit: ESA/DLR/FU Berlin  Here on Earth, we are used to the wind shaping our environment over time, forming smooth, sculpted rocks and rippling dunes. On the Red Planet, strong winds whip dust and sand from the surface into a frenzy, moving it across the planet at high speeds. The craters in this image, caused by impacts in Mars' past, all show different degrees of erosion. Some still have defined outer rims and clear features within them, while others are much smoother and featureless, almost seeming to run into one another or merge with their surroundings. This colour image was taken by Mars Express's High Resolution Stereo Camera on 19 November 2014, during orbit 13728.

<hr>

[Visit Link](http://phys.org/news352443370.html){:target="_blank" rel="noopener"}


