---
layout: post
title: "Schizophrenia emerged after humans diverged from Neanderthals"
date: 2017-03-17
categories:
author: "Elsevier"
tags: [Human evolution,Human,Neanderthal,Homo,Evolution,Genetics]
---


A new study in Biological Psychiatry may help explain why-comparing genetic information of Neanderthals to modern humans, the researchers found evidence for an association between genetic risk for schizophrenia and markers of human evolution. Along with Andreassen, first authors Saurabh Srinivasan and Francesco Bettella, both from the University of Oslo, and colleagues looked to the genome of Neanderthals, the closest relative of early humans, to pinpoint specific regions of the genome that could provide insight on the origin of schizophrenia in evolutionary history. Regions of the human genome associated with schizophrenia, known as risk loci, were more likely to be found in regions that diverge from the Neanderthal genome. An additional analysis to pinpoint loci associated with evolutionary markers suggests that several gene variants that have undergone positive selection are related to cognitive processes. Other such gene loci are known to be associated with schizophrenia and have previously been considered for a causal role in the disorder.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/08/160815064944.htm){:target="_blank" rel="noopener"}


