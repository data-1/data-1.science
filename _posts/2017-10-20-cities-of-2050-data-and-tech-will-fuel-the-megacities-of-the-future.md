---
layout: post
title: "Cities Of 2050: Data And Tech Will Fuel The Megacities Of The Future"
date: 2017-10-20
categories:
author:  
tags: [City,General Electric,Infrastructure,Building information modeling,Technology,Internet of things,Big data,Smart city,Innovation,Information,Simulation,Energy development,3D computer graphics,Computer network,Electrical grid,Sustainable energy,Economy,Branches of science]
---


The rise of Big Data and advanced modeling technology make it possible to plan and prioritize infrastructure investment with greater foresight, better communicate potential outcomes, and yield measurably better results. Creating smart cities means more than using the IoT to optimize services or communicate information to residents: It should be a construct used to frame local government decision making around city transformation. The good news is that data and technology will make work and life better by creating a well-connected community. Building Information Modeling (BIM) gives meaning to the vast information available to architects and engineers, urban citizens, and decision makers. That information helps architects and engineers enhance designs so individuals, firms, and cities can meet their “smart” connected goals, bringing neighboring cities together.

<hr>

[Visit Link](http://www.gereports.com/cities-of-2050-data-and-tech-will-fuel-the-megacities-of-the-future/){:target="_blank" rel="noopener"}


