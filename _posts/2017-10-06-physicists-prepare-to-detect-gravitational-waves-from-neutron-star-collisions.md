---
layout: post
title: "Physicists prepare to detect gravitational waves from neutron star collisions"
date: 2017-10-06
categories:
author: "Lisa Zyga"
tags: [QCD matter,Quark,Matter,Quarkgluon plasma,Strong interaction,Particle physics,Neutron star,Physical sciences,Theoretical physics,Nature,Nuclear physics,Quantum field theory,Quantum mechanics,Standard Model,Quantum chromodynamics,Applied and interdisciplinary physics,Physics]
---


An improved equation of state describing quark matter makes theoretical predictions regarding the properties of neutron star matter that researchers will hopefully be able to test in the future. Credit: NASA/CXC/SAO (X-Ray); NASA/JPL-Caltech (Infrared)  (Phys.org)—Last February, scientists made the groundbreaking discovery of gravitational waves produced by two colliding black holes. Their method makes theoretical predictions regarding the properties of neutron star matter that researchers working with the future data will hopefully be able to test. The hope is that the gravitational wave signal from a merger of two neutron stars or a neutron star and a black hole would provide detailed information about the structure of neutron stars, Kurkela told Phys.org. The results here also apply to the quark-gluon plasma produced in particle accelerators, which the scientists explain is somewhat different than the quark matter predicted to exist in neutron stars.

<hr>

[Visit Link](http://phys.org/news/2016-08-physicists-gravitational-neutron-star-collisions.html){:target="_blank" rel="noopener"}


