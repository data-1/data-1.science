---
layout: post
title: "Cosmic mystery deepens with discovery of new ultra-high-energy neutrino"
date: 2016-05-26
categories:
author: Kate Greene, Lawrence Berkeley National Laboratory
tags: [IceCube Neutrino Observatory,Neutrino,Cosmic ray,Muon,Particle physics,Astronomy,Science,Physical sciences,Astrophysics,Physics,Space science,Nature]
---


The researchers' main analysis objective was to confirm previous IceCube measurements of other astrophysical neutrinos. Scientists have hoped that ultra-high-energy neutrinos could point to sources of ultra-high-energy cosmic rays—supermassive black holes at the centers of galaxies or hypernova star explosions, for instance. The new neutrino was found thanks to a muon trail observed by an array of 5,160 optical detectors, using electronics designed and built by Berkeley Lab scientists and engineers. This means the actual neutrino energy was likely several times higher than was seen in the detector. Cosmic rays are charged particles that are suspected to come from ultra-high-energy sources outside the galaxy.

<hr>

[Visit Link](http://phys.org/news/2015-08-cosmic-mystery-deepens-discovery-ultra-high-energy.html){:target="_blank" rel="noopener"}


