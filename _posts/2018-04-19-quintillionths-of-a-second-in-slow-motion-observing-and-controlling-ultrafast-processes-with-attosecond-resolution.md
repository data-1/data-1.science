---
layout: post
title: "Quintillionths of a second in slow motion: Observing and controlling ultrafast processes with attosecond resolution"
date: 2018-04-19
categories:
author: "Technical University of Munich (TUM)"
tags: [Ultrafast laser spectroscopy,Physical sciences,Applied and interdisciplinary physics,Science,Chemistry,Physical chemistry,Optics,Atomic molecular and optical physics,Physics,Electromagnetism,Electromagnetic radiation,Scientific techniques]
---


Here, the sample is excited using an initial laser pulse, which sets the reaction into motion. Two eyes see more than one  Now, a team of scientists headed by Birgitta Bernhardt, a former staff member at the Chair of Laser and X-ray Physics at TU Munich and meanwhile junior professor at the Institute of Applied Physics at the University of Jena, have for the first time succeeded in combining two pump-probe spectroscopy techniques using the inert gas krypton. We have now combined the two techniques, which allows us to observe the precise steps by which the ionization takes place, how long these intermediate products exist and what precisely the exciting laser pulse causes in the sample. Ultrafast processes under control  The combination of the two measuring techniques allows the scientists not only to record the ultrafast ionization processes. If the ionization states of silicon can not only be sampled on such a short time scale, but can also be set -- as the first experiments with krypton suggest -- scientists might one day be able to use this to develop novel and even faster computer technologies.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/02/180220104109.htm){:target="_blank" rel="noopener"}


