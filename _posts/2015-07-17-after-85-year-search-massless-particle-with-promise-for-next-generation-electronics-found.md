---
layout: post
title: "After 85-year search, massless particle with promise for next-generation electronics found"
date: 2015-07-17
categories:
author: Princeton  University 
tags: [Electron,Subatomic particle,Fermion,Weyl equation,Matter,Physics,Particle physics,Theoretical physics,Nature,Applied and interdisciplinary physics,Science,Physical sciences,Quantum mechanics]
---


The researchers report in the journal Science July 16 the first observation of Weyl fermions, which, if applied to next-generation electronics, could allow for a nearly free and efficient flow of electricity in electronics, and thus greater power, especially for computers, the researchers suggest. Unlike electrons, Weyl fermions are massless and possess a high degree of mobility; the particle's spin is both in the same direction as its motion -- which is known as being right-handed -- and in the opposite direction in which it moves, or left-handed. The Weyl fermion, however, was discovered inside a synthetic metallic crystal called tantalum arsenide that the Princeton researchers designed in collaboration with researchers at the Collaborative Innovation Center of Quantum Matter in Beijing and at National Taiwan University. The researchers also found that Weyl fermions can be used to create massless electrons that move very quickly with no backscattering, wherein electrons are lost when they collide with an obstruction. The nature of this research and how it emerged is really different and more exciting than most of other work we have done before, Xu said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/pu-a8s071015.php){:target="_blank" rel="noopener"}


