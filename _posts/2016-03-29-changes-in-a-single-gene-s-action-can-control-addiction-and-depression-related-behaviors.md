---
layout: post
title: "Changes in a single gene's action can control addiction and depression-related behaviors"
date: 2016-03-29
categories:
author: The Mount Sinai Hospital / Mount Sinai School of Medicine
tags: [Gene,Addiction,FOSB,Epigenetics,Transcription factor,Gene expression,Regulation of gene expression,DNA,Transcription (biology),Health sciences,Health,Biotechnology,Molecular biology,Biochemistry,Biology,Life sciences,Genetics]
---


Regulation of a single, specific gene in a brain region related to drug addiction and depression is sufficient to reduce drug and stress responses, according to a study conducted at the Icahn School of Medicine at Mount Sinai and published October 27 online in the journal Nature Neuroscience. Using mouse models of human depression, stress and addiction, the current research team introduced synthetic- transcription factors into a brain region called the nucleus accumbens at a single gene called FosB, which has been linked by past studies to both addiction and depression. While all cells contain the DNA that codes for every gene, most genes are not activated at all times. The expression of a given gene depends on the action of transcription factors, proteins that regulate the structure of DNA within the cell, allowing some genes to be active and others to be repressed. In particular, activation of FosB expression is linked to increased sensitivity to drugs and to resilience to stress and is altered by exposure to such stimuli in the brains of mouse models and in drug-addicted and depressed human patients.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-11/tmsh-cia111014.php){:target="_blank" rel="noopener"}


