---
layout: post
title: "Genetic evidence that magnetic navigation guides loggerhead sea turtles: Loggerhead sea turtles that nest on beaches with similar magnetic fields are genetically similar"
date: 2018-07-01
categories:
author: "University of North Carolina at Chapel Hill"
tags: [Sea turtle,Loggerhead sea turtle,Turtle,News aggregator,Animals]
---


Genetic evidence that magnetic navigation guides loggerhead sea turtles: Loggerhead sea turtles that nest on beaches with similar magnetic fields are genetically similar. ScienceDaily, 12 April 2018. Genetic evidence that magnetic navigation guides loggerhead sea turtles: Loggerhead sea turtles that nest on beaches with similar magnetic fields are genetically similar. Retrieved August 21, 2022 from www.sciencedaily.com/releases/2018/04/180412141103.htm  University of North Carolina at Chapel Hill. Genetic evidence that magnetic navigation guides loggerhead sea turtles: Loggerhead sea turtles that nest on beaches with similar magnetic fields are genetically similar.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180412141103.htm){:target="_blank" rel="noopener"}


