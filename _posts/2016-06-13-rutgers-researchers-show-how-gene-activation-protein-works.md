---
layout: post
title: "Rutgers researchers show how gene activation protein works"
date: 2016-06-13
categories:
author: "Rutgers University"
tags: [Transcription (biology),Activator (genetics),RNA polymerase,DNA,RNA,Protein,CAMP receptor protein,Gene,Cell biology,Molecular genetics,Macromolecules,Genetics,Life sciences,Biology,Biotechnology,Biochemistry,Molecular biology,Biomolecules,Nucleotides,Natural products,Nutrients,Gene expression,Proteins,Molecular biophysics,Cellular processes,Nucleic acids,Branches of genetics,Chemistry,Structural biology]
---


Rutgers University scientists have discovered the three-dimensional structure of a gene-specific transcription activation complex, providing the first structural and mechanistic description of the process cells use to turn on, or activate, specific genes in response to changes in cell type, developmental state and environment. They also show how the transcription activator protein helps RNA polymerase bind to the DNA helix at a specific site preceding a gene, and how the transcription activator protein helps RNA polymerase unwind the DNA helix to initiate transcription of the gene. The Rutgers scientists show that the transcription activator protein functions by binding to a specific DNA sequence preceding the target gene and making adhesive, Velcro-like interactions with RNA polymerase that stabilize contacts by RNA polymerase with adjacent DNA sequences. The Rutgers scientists further show that the transcription activator protein makes two separate, successive sets of adhesive, Velcro-like interactions with RNA polymerase: one that helps RNA polymerase bind to the DNA helix, and a second set that helps RNA polymerase unwind the DNA helix. The structure determined by the Rutgers researchers is the structure of a transcription activation complex containing the TTHB099 activator protein (TAP) from the bacterium Thermus thermophilus.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/ru-rrs060316.php){:target="_blank" rel="noopener"}


