---
layout: post
title: "Oldest fossil sea turtle discovered: Huge fossilized turtle is at least 120 million years old"
date: 2015-09-08
categories:
author: Senckenberg Research Institute and Natural History Museum 
tags: [Fossil,Turtle,Desmatochelys,Taxa,Paleontology]
---


The fossilized reptile is at least 120 million years old -- which makes it about 25 million years older than the previously known oldest specimen. We described a fossil sea turtle from Colombia that is about 25 million years older, said Dr. Edwin Cadena, a scholar of the Alexander von Humboldt foundation at the Senckenberg Research Institute. originates from Cretaceous sediments and is at least 120 million years old, says Cadena. Since then, they have been stored in the collections of the Centro de Investigaciones Paleontológicas in Villa Leyva and the University of California Museum of Paleontology. Cadena and his colleague examined the almost complete skeleton, four additional skulls and two partially preserved shells, and they placed the fossils in the turtle group Chelonioidea, based on various morphological characteristics.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150907113919.htm){:target="_blank" rel="noopener"}


