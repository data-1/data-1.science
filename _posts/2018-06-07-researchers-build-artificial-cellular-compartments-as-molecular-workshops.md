---
layout: post
title: "Researchers build artificial cellular compartments as molecular workshops"
date: 2018-06-07
categories:
author: "Technical University Munich"
tags: [Cell (biology),Medical imaging,Protein,Microscopy,Genetics,Bacterial microcompartment,Enzyme,Privacy,Life sciences,Biotechnology,Biology,Chemistry,Biochemistry,Physical sciences,Technology]
---


A team from the Technical University of Munich (TUM) and the Helmholtz Zentrum München have altered mammalian cells in such a way that they formed artificial compartments in which sequestered reactions could take place, allowing the detection of cells deep in the tissue and also their manipulation with magnetic fields. We can thus spatially separate processes and give the cells new properties. But the nanospheres also have a natural property that is especially important to Westmeyer's team: They can take in iron atoms and process them in such a way that they remain inside the nanospheres without disrupting the cell's processes. This sequestered iron biomineralization makes the particles and also the cells magnetic. Magnetic and practical  In particular, this will make it easier to observe cells using different imaging methods: Magnetic cells can also be observed in deep layers with methods that do not damage the tissue, such as Magnetic Resonance Imaging (MRI).

<hr>

[Visit Link](https://phys.org/news/2018-05-artificial-cellular-compartments-molecular-workshops.html){:target="_blank" rel="noopener"}


