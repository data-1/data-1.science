---
layout: post
title: "Satellites, supercomputers, and machine learning provide real-time crop type data"
date: 2018-07-18
categories:
author: "University of Illinois College of Agricultural, Consumer and Environmental Sciences"
tags: [Infrared,National Center for Supercomputing Applications,Landsat program,Machine learning,Deep learning,Supercomputer,Maize,Data,Science,Technology,Branches of science,Computing]
---


If we want to predict corn or soybean production for Illinois or the entire United States, we have to know where they are being grown, says Kaiyu Guan, assistant professor in the Department of Natural Resources and Environmental Sciences at the University of Illinois, Blue Waters professor at the National Center for Supercomputing Applications (NCSA), and the principal investigator of the new study. The advancement, published in Remote Sensing of Environment, is a breakthrough because, previously, national corn and soybean acreages were only made available to the public four to six months after harvest by the USDA. But the new technique can distinguish the two major crops with 95 percent accuracy by the end of July for each field - just two or three months after planting and well before harvest. Guan says most previous attempts to differentiate corn and soybean from these images were based on the visible and near-infrared part of the spectrum, but he and his team decided to try something different. Even though it was a relatively small area, analyzing 15 years of satellite data at a 30-meter resolution still required a supercomputer to process tens of terabytes of data.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-04/uoic-ssa040418.php){:target="_blank" rel="noopener"}


