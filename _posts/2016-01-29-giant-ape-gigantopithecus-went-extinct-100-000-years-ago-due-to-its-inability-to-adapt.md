---
layout: post
title: "Giant ape Gigantopithecus went extinct 100,000 years ago due to its inability to adapt"
date: 2016-01-29
categories:
author: Senckenberg Research Institute, Natural History Museum
tags: [Gigantopithecus]
---


Estimated size of Giganthopithecus in comparison with a human. Credit: H. Bocherens  Scientists from the Senckenberg Center for Human Evolution and Palaeoenvironment in Tübingen and from the Senckenberg Research Institute in Frankfurt examined the demise of the giant ape Gigantopithecus. Together with his colleagues from the Senckenberg Research Institute, Prof. Dr. Friedmann Schrenk and PD Dr. Ottmar Kullmer, as well as other international scientists, the biogeologist from Tübingen examined the fossil giant ape's tooth enamel in order to make inferences on its diet and to define potential factors for its extinction. Our results indicate that the large primates only lived in the forest and obtained their food from this habitat, explains Bocherens, and he adds, Gigantopithecus was an exclusive vegetarian, but it did not specialize on bamboo. Relatives of the giant ape, such as the recent orangutan, have been able to survive despite their specialization on a certain habitat.

<hr>

[Visit Link](http://phys.org/news/2016-01-giant-ape-gigantopithecus-extinct-years.html){:target="_blank" rel="noopener"}


