---
layout: post
title: "How the brain encodes time and place: Neuroscientists identify a brain circuit that is critical for forming episodic memories"
date: 2016-07-25
categories:
author: "Massachusetts Institute of Technology"
tags: [Memory,Hippocampus,Entorhinal cortex,Brain,Neural circuit,Neuroscience,Cognitive science,Mental processes,Interdisciplinary subfields,Cognition,Cognitive psychology,Psychology]
---


MIT neuroscientists have now identified a brain circuit that processes the when and where components of memory. The researchers also identified two populations of neurons in the entorhinal cortex that convey this information, dubbed ocean cells and island cells. Previous models of memory had suggested that the hippocampus, a brain structure critical for memory formation, separates timing and context information. When and where  Located just outside the hippocampus, the entorhinal cortex relays sensory information from other cortical areas to the hippocampus, where memories are formed. The researchers also found that these two streams of information flow from the entorhinal cortex to different parts of the hippocampus: Ocean cells send their contextual information to the CA3 and dentate gyrus regions, while island cells project to CA1 cells.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150923134112.htm){:target="_blank" rel="noopener"}


