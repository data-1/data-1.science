---
layout: post
title: "EU decision process hinders use of genetically modified trees"
date: 2016-03-01
categories:
author: VIB (the Flanders Institute for Biotechnology)
tags: [Genetic engineering,Risk management,Genetically modified tree,Risk,Biotechnology,Branches of science,Technology,Natural environment]
---


The researchers state that Europe is lagging behind in worldwide GM developments and call for a more scientifically substantiated decision process. Politicized decision process  The European decision process is not only complex, but also unpredictable. After the risk analysis and a scientific conclusion from EFSA, it is still by no means certain that a European approval will follow. The fact that individual EU Member States can restrict or prohibit the cultivation of GMOs on their territory for reasons having nothing to do with substantiated risks further increases this uncertainty. René Custers: Just like with other GM crops, the commercial developments in the field of GM trees are taking place outside Europe.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/vfi-edp022416.php){:target="_blank" rel="noopener"}


