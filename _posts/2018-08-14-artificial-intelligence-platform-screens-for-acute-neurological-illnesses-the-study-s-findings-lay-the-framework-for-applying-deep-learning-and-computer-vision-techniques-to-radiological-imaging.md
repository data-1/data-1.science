---
layout: post
title: "Artificial intelligence platform screens for acute neurological illnesses: The study's findings lay the framework for applying deep learning and computer vision techniques to radiological imaging"
date: 2018-08-14
categories:
author: "The Mount Sinai Hospital / Mount Sinai School of Medicine"
tags: [Artificial intelligence,Burton Drayer,Radiology,Neurology,Deep learning,Branches of science,Health sciences,Health care,Medicine,Technology,Cognitive science,Health]
---


An artificial intelligence platform designed to identify a broad range of acute neurological illnesses, such as stroke, hemorrhage, and hydrocephalus, was shown to identify disease in CT scans in 1.2 seconds, faster than human diagnosis, according to a study conducted at the Icahn School of Medicine at Mount Sinai and published today in the journal Nature Medicine. We're executing on the vision to develop artificial intelligence in medicine that will solve clinical problems and improve patient care. Researchers used 37,236 head CT scans to train a deep neural network to identify whether an image contained critical or non-critical findings. This study used weakly supervised learning approaches, which built on the research team's expertise in natural language processing and the Mount Sinai Health System's large clinical datasets. The application of deep learning and computer vision techniques to radiological imaging is a clear imperative for 21st century medical care, says study author Burton Drayer, MD, the Charles M. and Marilyn Newman Professor and System Chair of the Department of Radiology for the Mount Sinai Health System, CEO of the Mount Sinai Doctors Faculty Practice, and Dean for Clinical Affairs of the Icahn School of Medicine.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180813113315.htm){:target="_blank" rel="noopener"}


