---
layout: post
title: "Artificial intelligence aids materials fabrication"
date: 2018-06-29
categories:
author: "Larry Hardesty"
tags: [Machine learning,Word2vec,Massachusetts Institute of Technology,Research,Cognition,Computing,Applied mathematics,Artificial intelligence,Systems science,Cybernetics,Science,Computer science,Cognitive science,Branches of science,Technology]
---


A team of researchers at MIT, the University of Massachusetts at Amherst, and the University of California at Berkeley hope to close that materials-science automation gap, with a new artificial-intelligence system that would pore through research papers to deduce “recipes” for producing particular materials. Filling in the gaps  The researchers trained their system using a combination of supervised and unsupervised machine-learning techniques. “Unsupervised” means that the training data is unannotated, and the system instead learns to cluster data together according to structural similarities. With Word2vec, the researchers were able to greatly expand their training set, since the machine-learning system could infer that a label attached to any given word was likely to apply to other words clustered with it. Instead of 100 papers, the researchers could thus train their system on around 640,000 papers.

<hr>

[Visit Link](http://news.mit.edu/2017/artificial-intelligence-aids-materials-fabrication-1106){:target="_blank" rel="noopener"}


