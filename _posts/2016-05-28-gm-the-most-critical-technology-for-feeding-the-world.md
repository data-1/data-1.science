---
layout: post
title: "GM -- 'the most critical technology' for feeding the world"
date: 2016-05-28
categories:
author: BMC (BioMed Central)
tags: [Agriculture,Genetically modified crops,Food security,Genetic engineering,Rice,Food,Sustainable agriculture,Plant breeding,BioMed Central,Golden rice,Wheat,Food industry,Food and drink]
---


A former adviser to the US Secretary of State says that genetic modification (GM) is the most critical technology in agriculture for meeting the challenges of feeding a growing global population, writing in the journal Agriculture & Food Security  A former adviser to the US Secretary of State says that genetic modification (GM) is the most critical technology in agriculture for meeting the challenges of feeding a growing global population, writing in the open access journal Agriculture & Food Security. The overwhelming evidence is that the GM foods now on the market are as safe, or safer, than non-GM foods, argues Fedoroff. Article  Nina V Fedoroff  Food in a future of 10 billion  Agriculture & Food Security 2015  doi 10.1186/s40066-015-0031-7  For an embargoed copy of the article, please contact Joel.Winston@biomedcentral.com  After embargo, article available at journal website here: http://agricultureandfoodsecurity.biomedcentral.com/articles/10.1186/s40066-015-0031-7  Please name the journal in any story you write. All articles are available free of charge, according to BioMed Central's open access policy. Agriculture & Food Security is a peer-reviewed open access journal that addresses the challenge of global food security.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/bc-g-081915.php){:target="_blank" rel="noopener"}


