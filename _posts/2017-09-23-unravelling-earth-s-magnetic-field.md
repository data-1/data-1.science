---
layout: post
title: "Unravelling Earth’s magnetic field"
date: 2017-09-23
categories:
author: ""
tags: [Plate tectonics,Crust (geology),Earth,Earths magnetic field,North magnetic pole,Planets of the Solar System,Astronomy,Terrestrial planets,Applied and interdisciplinary physics,Space science,Physical sciences,Nature,Earth sciences,Geology,Geophysics,Planetary science,Structure of the Earth]
---


Applications Unravelling Earth’s magnetic field 21/03/2017 38278 views 267 likes  ESA’s Swarm satellites are seeing fine details in one of the most difficult layers of Earth’s magnetic field to unpick – as well as our planet’s magnetic history imprinted on Earth’s crust. Although this ‘lithospheric magnetic field’ is very weak and therefore difficult to detect from space, the Swarm trio is able to map its magnetic signals. “Measurements from space have great value as they offer a sharp global view on the magnetic structure of our planet’s rigid outer shell.” Presented at this week’s Swarm Science Meeting in Canada, the new map shows detailed variations in this field more precisely than previous satellite-based reconstructions, caused by geological structures in Earth’s crust. When new crust is generated through volcanic activity, mainly along the ocean floor, iron-rich minerals in the solidifying magma are oriented towards magnetic north, thus capturing a ‘snapshot’ of the magnetic field in the state it was in when the rocks cooled. Swarm The latest map from Swarm gives us an unprecedented global view of the magnetic stripes associated with plate tectonics reflected in the mid-oceanic ridges in the oceans.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Swarm/Unravelling_Earth_s_magnetic_field){:target="_blank" rel="noopener"}


