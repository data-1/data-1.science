---
layout: post
title: "How to Save Command Output to a File in Linux"
date: 2017-08-31
categories:
author: "Aaron Kili"
tags: [Computer file,File system,Online and offline,Computer data storage,Computer science,Operating system technology,Utility software,Data management,Computing,Technology,Software,Computer architecture,System software,Computer engineering,Computers,Information technology management,Information Age,Computer data,Digital media,Information technology,Software engineering]
---


Viewing Output On Screen and also Writing to a File  Assuming you want to get a full summary of available and used disk space of a file system on a Linux system, you can employ the df command; it also helps you determine the file system type on a partition. $ df -h  Now to display the above information on the screen and also write it to a file, say for later analysis and/or send to a system administrator via email, run the command below. If a file(s) already exists, you can append it using the -a or --append option like this. $ df -h | tee -a df.log  Note: You can also use pydf an alternative “df” command to check disk usage in different colors. In this short article, I showed you how to view output of a command on the screen and also write to a file in Linux.

<hr>

[Visit Link](https://www.tecmint.com/save-command-output-to-a-file-in-linux/){:target="_blank" rel="noopener"}


