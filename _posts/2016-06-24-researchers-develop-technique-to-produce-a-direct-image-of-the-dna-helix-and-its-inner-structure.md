---
layout: post
title: "Researchers develop technique to produce a direct image of the DNA helix and its inner structure"
date: 2016-06-24
categories:
author: "King Abdullah University Of Science"
tags: [DNA,Epigenetics,Molecular biology,Biotechnology,Life sciences,Macromolecules,Biochemistry,Biology,Chemistry,Genetics]
---


Credit: KAUST  Enzo di Fabrizio and his team have developed a new technique to produce a direct image of the DNA helix and its inner structure. Then in 2012, Enzo di Fabrizio and his team at Italy's University of Catanzaro achieved an experimental breakthrough using a transmission electron microscope to produce the image of the DNA treads in a plane projection, allowing, for the first time, the visual identification of the DNA helix periodic swirl. Today, Enzo di Fabrizio and his colleagues Dr. Monica Marini, Prof. Andrea Falqui and Dr. Sergei Lopatin, together with their international team of researchers at the King Abdullah University of Science and Technology (KAUST), Saudi Arabia have gone one step further, producing the very first direct image of a DNA strand with resolution 20 times better than that achieved by Di Fabrizio in 2012. Di Fabrizio says, Two identical genes can express different proteins with very different characteristics due to a simple methyl group placed between the bases. Di Fabrizio says.

<hr>

[Visit Link](http://phys.org/news/2015-08-technique-image-dna-helix.html){:target="_blank" rel="noopener"}


