---
layout: post
title: "Scientists date the origin of the cacao tree to 10 million years ago"
date: 2015-11-23
categories:
author: Frontiers 
tags: [Theobroma cacao,Biodiversity,Agriculture,Evolution,Chocolate,Nature,Natural environment]
---


Chocolate, produced from seeds of the cacao tree Theobroma cacao, is one of the most popular flavors in the world, with sales around 100$ billion dollars per year. Studies of the evolutionary history of economically important groups are vital to develop agricultural industries, and demonstrate the importance of conserving biodiversity to contribute towards sustainable development. Wild populations of cacao across the Americas may therefore be treasure troves of genetic variation, which could be bred into cultivated strains to make the latter more resistant to disease and climate change, and perhaps even create new flavors of chocolate. These varieties may contribute towards improving a developing chocolate industry, says James Richardson. By understanding the diversification processes of chocolate and its relatives we can contribute to the development of the industry and demonstrate that this truly is the Age of Chocolate, says coauthor Dr Santiago Madriñán of the University of the Andes in Bogotá, Colombia.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-11/f-sdt110515.php){:target="_blank" rel="noopener"}


