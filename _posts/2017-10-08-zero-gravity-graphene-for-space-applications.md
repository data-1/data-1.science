---
layout: post
title: "Zero gravity: Graphene for space applications"
date: 2017-10-08
categories:
author: "Graphene Flagship"
tags: [Weightlessness,Gravity,Space,News aggregator,Graphene,Solar sail]
---


Researchers and students in the Graphene Flagship are preparing for two exciting experiments in collaboration with the European Space Agency (ESA) to test the viability of graphene for space applications. These ambitious space-related experiments are an excellent opportunity for Flagship students and researchers to gain new experiences in cutting-edge research. programme offers students the opportunity to design an experiment for the ZARM Drop Tower in Bremen, Germany, which simulates the low gravity and vacuum conditions of space. In this experiment, the wicks will be coated with different types of graphene-related materials to improve the efficiency of the heat pipe. Jari Kinaret (Chalmers University of Technology, Sweden), Director of the Graphene Flagship, said These two projects exemplify the two-fold character of the Graphene Flagship: the loop heat pipe project is targeting a specific application, while the light sail project is firmly linked to basic research and builds upon the unique combination of properties that only graphene can offer.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/07/170707211134.htm){:target="_blank" rel="noopener"}


