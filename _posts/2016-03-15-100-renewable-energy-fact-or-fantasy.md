---
layout: post
title: "100% Renewable Energy: Fact Or Fantasy?"
date: 2016-03-15
categories:
author: John Farrell, George Harvey, U.S. Department Of Energy, Tina Casey, Written By
tags: [Renewable energy,Renewable energy commercialization,Energy,Electrification,Distributed generation,Physical quantities,Climate change mitigation,Economy,Nature,Energy and the environment,Sustainable energy,Power (physics),Sustainable technologies,Renewable resources,Sustainable development,Electric power,Technology,Natural resources,Economy and the environment,Electricity,Environmental technology]
---


Three big things:  Only build wind, solar, or hydro power plants after 2020 Reduce energy use compared to business as usual by 40% Electrify everything  It’s the last that may be the most complicated, since it means a complete overhaul of the way we do everything from heating homes to moving people. Mark Jacobson, author of a seminal study on the transformation, doesn’t mince words about its complexity:  The recommendations — indeed, all 28 — would require coordinated action from Congress, federal agencies, state legislatures, and local officials. Together, they represent an unprecedented level of government activism, a skein of incentives, mandates, standards, and laws unmatched in U.S. history. Related:  70%, 80%, 99.9%, 100% Renewables — Study Central  Renewable Energy Is Possible, Practical, & Cheaper (Than Nuclear Or Fossil Fuels)  The Solutions Project: How 139 Countries Can Hit 100% Renewable Energy  Getting To 100% Renewable Energy In the US  State-By-State Plan To Bring US To 100% Renewables By 2050 Unveiled By Researchers  100% Renewable Energy — How To Get There (Mark Z Jacobson Video)  Powering The World With Wind, Water, & Sunshine (Mark Z Jacobson Video)  Photo Credit: author unknown  For timely updates, follow John Farrell on Twitter or get the Democratic Energy weekly update. Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2016/03/01/100-renewable-energy-fact-fantasy/){:target="_blank" rel="noopener"}


