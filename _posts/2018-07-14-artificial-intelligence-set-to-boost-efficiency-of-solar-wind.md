---
layout: post
title: "Artificial Intelligence Set To Boost Efficiency Of Solar & Wind"
date: 2018-07-14
categories:
author: "Joshua S Hill, Carolyn Fortuna, Jennifer Sensiba, Guest Contributor, Iqtidar Ali, Written By"
tags: [Artificial intelligence,Automation,Robotics,Robot,Technology,Self-driving car,Condition monitoring,Renewable energy,Machine learning,Branches of science]
---


New research has posited that artificial intelligence will increasingly automate operations for the wind and solar industries, boosting their efficiencies in areas such as decision making and planning, condition monitoring, robotics, and inspections. The new position paper published this week by DNV GL — international accredited registrar and classification society headquartered near Oslo — entitled Making Renewables Smarter: The benefits, risks, and future of artificial intelligence in solar and wind, outlines the advances being made in robotics, inspections, supply chain, and the way we work and showcases a variety of opportunities for the solar and wind industries to embrace artificial intelligence (AI) applications to improve their efficiency. “Artificial intelligence’s ability to use machine learning to analyse historical and new data, make predictions, control physical operations, and make decisions at increasingly higher levels is having an immense impact.”  Specifically, the authors of the report explain that the wind and solar industries will benefit from specific AI applications, including:  Robots — lying, crawling, swimming, and sailing for remote inspection, with new benefits in maintenance and troubleshooting  Accelerated due diligence, so that planning and analysis that today might require many human hours and thousands of documents can be reduced by an enormous factor in the future, and even enhanced  New efficiencies in supply chain optimization, such as the delivery of solar and wind components by self-driving trucks and even the automation of renewables construction  The report explores ways in which AI applications like machine learning can impact the efficiency levels of areas involved in the wind and solar industries such as decision making and planning, condition monitoring, robotics, inspections, certifications and supply chain optimization, as well as the way technical work is carried out. “As a result,” the authors of the report explain, “most of the advances supported by artificial intelligence have been in meteorology, control, and predictive maintenance (and arguably those have been the most useful).”  It is important, however, for the wind and solar industries to be careful as they move forward in exploring the benefits and risks of AI. Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2017/11/23/artificial-intelligence-set-boost-efficiency-solar-wind/){:target="_blank" rel="noopener"}


