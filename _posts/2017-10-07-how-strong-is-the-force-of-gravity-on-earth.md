---
layout: post
title: "How strong is the force of gravity on Earth?"
date: 2017-10-07
categories:
author: "Matt Williams"
tags: [General relativity,Gravity,Gravity of Earth,Star,Black hole,Newtons law of universal gravitation,Force,Spacetime,Mass,Astronomy,Physical sciences,Theory of relativity,Space science,Celestial mechanics,Physical cosmology,Physics,Classical mechanics,Science,Physical quantities,Astrophysics,Theories of gravitation,Mechanics,Statics,Nature]
---


The gravitational force of an object is also dependent on distance – i.e. the amount it exerts on an object decreases with increased distance. It also governs the orbits of the planets around stars, of moons around planets, the rotation of stars around their galaxy's center, and the merging of galaxies. This results in Earth having a gravitational strength of 9.8 m/s² close to the surface (also known as 1 g), which naturally decreases the farther away one is from the surface. Earth's gravity is also responsible for our planet having an escape velocity of 11.186 km/s (or 6.951 mi/s). Because of the difference between Earth's gravity and the gravitational force on other bodies – like the moon (1.62 m/s²; 0.1654 g) and Mars (3.711 m/s²; 0.376 g) – scientists are uncertain what the effects would be to astronauts who went on long-term missions to these bodies.

<hr>

[Visit Link](http://phys.org/news/2016-12-strong-gravity-earth.html){:target="_blank" rel="noopener"}


