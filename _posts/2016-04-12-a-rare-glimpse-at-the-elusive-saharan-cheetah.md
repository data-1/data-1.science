---
layout: post
title: "A rare glimpse at the elusive saharan cheetah"
date: 2016-04-12
categories:
author: Wildlife Conservation Society
tags: [Cheetah,Northwest African cheetah,Conservation biology,Sahara]
---


Research by scientists and conservationists from the Wildlife Conservation Society, the Zoological Society of London, and other groups published today in PLOS ONE shows that critically endangered Saharan cheetahs exist at incredibly low densities and require vast areas for their conservation. The research also offers some of the world's only photographs of this elusive big cat. The research also offers some of the world's only photographs of this elusive big cat. The findings by scientists and conservationists at WCS, ZSL, University College London, UK, and Université de Béjaïa, Algeria, in collaboration with the Office National du Parc Culturel de l'Ahaggar, show that the Saharan cheetah adapts its behavior to cope with the harsh desert environment in which it lives. The survival of large carnivores within the Sahara desert indicates that at present the Ahaggar Cultural Park is still a relatively healthy habitat; however there are threats to cheetah and their prey.

<hr>

[Visit Link](http://phys.org/news341769943.html){:target="_blank" rel="noopener"}


