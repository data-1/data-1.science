---
layout: post
title: "Human and artificial intelligence join forces to study complexity of the brain"
date: 2018-06-17
categories:
author: "VIB (the Flanders Institute for Biotechnology)"
tags: [Brain,Stein Aerts,Life sciences,Biotechnology,Branches of science,Technology,Biology]
---


A team of scientists lead by prof. Stein Aerts (VIB-KU Leuven) is the first to map the gene expression of each individual brain cell during aging, though they started small: with the brain of a fruit fly. Working with fruit flies as model organisms, the scientists took the challenge head on, immediately starting with the most complex organ of all--the brain. We have made all of our fly brain data freely available on a unique online analysis platform, where other scientists can deposit their data as well, says Aerts. Together with international colleagues who use single-cell technology to study different organs of the fruit fly, he founded the Fly Cell Atlas consortium. Everyone can submit questions concerning this and other medically-oriented research directly to VIB via this address.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/vfi-haa061318.php){:target="_blank" rel="noopener"}


