---
layout: post
title: "What is photosynthesis?"
date: 2016-05-16
categories:
author: Daisy Dobrijevic
tags: [Calvin cycle,Photosynthesis,Light-dependent reactions,Photorespiration,Chloroplast,Thylakoid,Plastid,Photosynthetic efficiency,Crassulacean acid metabolism,Plants,Plant metabolism,Nature,Cellular processes,Biotechnology,Physiology,Branches of botany,Plant physiology,Underwater diving physiology,Biology,Metabolism,Photochemistry,Physical sciences,Chemistry,Biochemistry]
---


The oxygenic photosynthesis equation is:  6CO2 + 12H2O + Light Energy → C6H12O6 + 6O2 + 6H2O  Here, six molecules of carbon dioxide (CO2) combine with 12 molecules of water (H2O) using light energy. Chlorophyll is a large molecule and takes a lot of resources to make; as such, it breaks down towards the end of the leaf's life, and most of the pigment's nitrogen (one of the building blocks of chlorophyll) is resorbed back into the plant, according to Harvard University's The Harvard Forest (opens in new tab). The pigments and proteins that convert light energy to chemical energy and begin the process of electron transfer are known as reaction centers. Both types of reactions take place in chloroplasts: light-dependent reactions in the thylakoid and light-independent reactions in the stroma. When a photon of light hits the reaction center, a pigment molecule such as chlorophyll releases an electron.

<hr>

[Visit Link](http://www.livescience.com/51720-photosynthesis.html){:target="_blank" rel="noopener"}


