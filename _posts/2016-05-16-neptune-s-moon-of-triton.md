---
layout: post
title: "Neptune's moon of Triton"
date: 2016-05-16
categories:
author: Matt Williams
tags: [Triton (moon),Neptune,Atmosphere,Natural satellite,Pluto,Solar System,Planet,Moons of Neptune,Planemos,Bodies of the Solar System,Planets,Outer space,Local Interstellar Cloud,Space science,Planetary science,Astronomy,Gas giants,Outer planets,Astronomical objects,Physical sciences,Planets of the Solar System]
---


Global Color Mosaic of Triton, taken by the Voyager 2 spacecraft in 1989. Credit: NASA/JPL/USGS  The planets of the outer solar system are known for being strange, as are their many moons. At this distance, Triton is the farthest satellite of Neptune, and orbits the planet every 5.87685 Earth days. Unlike other moons of its size, Triton has a retrograde orbit around its host planet. It is also one of the few moons in the solar system that is geologically active, which means that its surface is relatively young due to resurfacing. Like Pluto, Triton has an atmosphere that is thought to have resulted from the evaporation of ices from its surface.

<hr>

[Visit Link](http://phys.org/news/2015-07-neptune-moon-triton.html){:target="_blank" rel="noopener"}


