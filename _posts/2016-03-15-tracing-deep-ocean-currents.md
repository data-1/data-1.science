---
layout: post
title: "Tracing deep ocean currents"
date: 2016-03-15
categories:
author: Uni Research
tags: [Thermohaline circulation,North Atlantic Deep Water,Ocean current,Ocean,Nature,Earth sciences,Physical geography,Applied and interdisciplinary physics,Oceanography,Hydrography,Hydrology,Environmental science]
---


Researchers like Yongqi Gao follow the radioactive waste to understand how ocean currents are formed and to see where they flow. Radioactive material has also been released from the Sellafield nuclear power station in England. He is working on including biogeochemical tracers in an ocean circulation model to understand the formation and movement of deep ocean currents. Animation of simulated spreading of radioactive leakage from the Fukushima 1 plant in March 2011. Credit: Yanchun He, NERSC  Gao has previously simulated CFC's (chlorofluorocarbons) used in commercial products such as Freon, the greenhouse gas SF6 used in the electrical industry, and the radioactive isotopes Cs137 and Sr90 as tracers in an Ocean General Circulation Model (OGCM). In the northern Atlantic, cold, dense water sinks from the surface to the deeper ocean and eventually forms the North Atlantic Deep Water (NADW).

<hr>

[Visit Link](http://phys.org/news/2016-02-deep-ocean-currents.html){:target="_blank" rel="noopener"}


