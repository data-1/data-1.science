---
layout: post
title: "The chemical evolution of DNA and RNA on early Earth"
date: 2018-08-02
categories:
author: "Ludwig Maximilian University Of Munich"
tags: [Abiogenesis,Nucleotide,RNA,Life,Physical sciences,Chemistry,Biology,Nature,Biochemistry,Life sciences]
---


Now, chemists from Ludwig-Maximilians-Universitaet (LMU) in Munich have demonstrated that alternation of wet and dry conditions could have sufficed to drive the prebiotic synthesis of the RNA nucleosides found in all domains of life. How might the chemical structures that provide the basic subunits of RNA and DNA have formed from simpler starting materials some 4 billion years ago? In a paper published last year, Carell and his team first described this FaPy pathway as a possible chemical scenario for the prebiotic synthesis of nucleosides. This suggests that these compounds must have been available on early Earth when biological evolution began. Seen in this light, the RNA modifications found in today's organisms represent molecular fossils that have continued to participate in vital biological functions for billions of years.

<hr>

[Visit Link](https://phys.org/news/2018-01-chemical-evolution-dna-rna-early.html){:target="_blank" rel="noopener"}


