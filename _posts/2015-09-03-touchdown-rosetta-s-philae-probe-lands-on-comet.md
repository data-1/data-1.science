---
layout: post
title: "Touchdown! Rosetta’s Philae probe lands on comet"
date: 2015-09-03
categories:
author: "$author"   
tags: [Rosetta (spacecraft),Philae (spacecraft),Lander (spacecraft),European Space Agency,Comet,Scientific exploration,Aerospace,Planetary science,Sky,Space probes,Space vehicles,Bodies of the Solar System,Space research,Discovery and exploration of the Solar System,Spacecraft,Space exploration,Solar System,Outer space,Spaceflight,Space science,Astronautics,Astronomy,Flight]
---


Rosetta’s Philae probe lands on comet 12/11/2014 871709 views 2343 likes  ESA’s Rosetta mission has soft-landed its Philae probe on a comet, the first time in history that such an extraordinary feat has been achieved. “Today’s successful landing is undoubtedly the cherry on the icing of a 4 km-wide cake, but we’re also looking further ahead and onto the next stage of this ground-breaking mission, as we continue to follow the comet around the Sun for 13 months, watching as its activity changes and its surface evolves.” While Philae begins its close-up study of the comet, Rosetta must manoeuvre from its post-separation path back into an orbit around the comet, eventually returning to a 20 km orbit on 6 December. More about Rosetta Rosetta is an ESA mission with contributions from its Member States and NASA. By studying the gas, dust and structure of the nucleus and organic materials associated with the comet, via both remote and in situ observations, the Rosetta mission should become the key to unlocking the history and evolution of our Solar System. Today, it develops and launches satellites for Earth observation, naviga-tion, telecommunications and astronomy, sends probes to the far reaches of the Solar System and cooperates in the human exploration of space.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Rosetta/Touchdown!_Rosetta_s_Philae_probe_lands_on_comet){:target="_blank" rel="noopener"}


