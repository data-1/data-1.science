---
layout: post
title: "Geoengineering the Great Barrier Reef needs strong rules"
date: 2018-08-02
categories:
author: "Kerryn Brent, Brendan Gogarty, Jan Mcdonald And Jeff Mcgee, The Conversation"
tags: [Coral reef,Climate engineering,Great Barrier Reef,Climate change,Risk,Environmental law,Natural environment,Earth sciences]
---


These geoengineering approaches carry their own risks, and require careful management, even at the research and field testing stages. Stabilising environmental conditions to protect current reef biodiversity requires that global temperatures stay below 1.2℃. It is not surprising, then, that scientists are looking to buy the reef some time, while the international community works to stabilise and then reduce global greenhouse gas emissions. Governance necessary for public confidence  Building public confidence that potential risks have been identified and addressed is essential to the long-term success of reef geoengineering proposals. Any geoengineering trial that might have a significant impact on those areas is illegal without a permit from the Commonwealth Environment Minister.

<hr>

[Visit Link](https://phys.org/news/2018-08-geoengineering-great-barrier-reef-strong.html){:target="_blank" rel="noopener"}


