---
layout: post
title: "Complete design of a silicon quantum computer chip unveiled"
date: 2018-07-02
categories:
author: "University Of New South Wales"
tags: [Computer,Computing,Microprocessor,Quantum computing,Integrated circuit,Qubit,Quantum mechanics,CMOS,Electronics,Electronics industry,Branches of science,Computers,Information Age,Computer engineering,Computer science,Technology]
---


Credit: Tony Melov/UNSW  Research teams all over the world are exploring different ways to design a working computing chip that can integrate quantum interactions. This would allow a universal quantum computer to be millions of times faster than any conventional computer when solving a range of important problems. We expect that there will still be modifications required to this design as we move towards manufacture, but all of the key components that are needed for quantum computing are here in one chip. That's why UNSW's new design is so exciting: relying on its silicon spin qubit approach - which already mimics much of the solid-state devices in silicon that are the heart of the US$380 billion global semiconductor industry - it shows how to dovetail spin qubit error correcting code into existing chip designs, enabling true universal quantum computation. The UNSW team has struck a A$83 million deal between UNSW, Telstra, Commonwealth Bank and the Australian and New South Wales governments to develop, by 2022, a 10-qubit prototype silicon quantum integrated circuit - the first step in building the world's first quantum computer in silicon.

<hr>

[Visit Link](https://phys.org/news/2017-12-silicon-quantum-chip-unveiled.html){:target="_blank" rel="noopener"}


