---
layout: post
title: "Breakthrough in ultra-fast data processing at nanoscale"
date: 2017-10-20
categories:
author: National University Of Singapore
tags: [Nanoelectronics,Plasmonics,Plasmon,Nanotechnology,Electromagnetism,Electricity,Electronics,Electrical engineering,Applied and interdisciplinary physics,Chemistry,Technology,Materials science]
---


A research team led by Associate Professor Christian Nijhuis from the Department of Chemistry at the NUS Faculty of Science (second from right) has recently invented a novel “converter” that can harness the speed and small size of plasmons for high frequency data processing and transmission in nanoelectronics. While most advanced electronic devices are powered by photonics – which involves the use of photons to transmit information – photonic elements are usually large in size and this greatly limits their use in many advanced nanoelectronics systems. Addressing this technological gap, a research team from the National University of Singapore (NUS) has recently invented a novel converter that can harness the speed and small size of plasmons for high frequency data processing and transmission in nanoelectronics. Our plasmonic-electronic transducer is about 10,000 times smaller than optical elements. To convert electrical signals into plasmonic signals, and vice versa, in one single step, the NUS team employed a process called tunnelling, in which electrons travel from one electrode to another electrode, and by doing so, excite plasmons.

<hr>

[Visit Link](https://phys.org/news/2017-10-breakthrough-ultra-fast-nanoscale.html){:target="_blank" rel="noopener"}


