---
layout: post
title: "Breakthrough in human cell transformation could revolutionize regenerative medicine"
date: 2016-01-29
categories:
author: University of Bristol
tags: [Stem cell,Cell potency,Life sciences,Medical specialties,Health sciences,Clinical medicine,Medicine,Biotechnology,Biology]
---


Their paper, published today in Nature Genetics, demonstrates the creation of a system that predicts how to create any human cell type from another cell type directly, without the need for experimental trial and error. Pluripotent stem cells - or cells that have not yet 'decided' what to become - can be used to treat many different medical conditions and diseases. In the nine years since, scientists have only been able to discover further conversions for human cells a handful of times. Professor Gough said: 'Mogrify predicts how to create any human cell type from any other cell type directly. With Professor Jose Polo at Monash University in Australia, we tested it on two new human cell conversions, and succeeded first time for both.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/uob-bih011916.php){:target="_blank" rel="noopener"}


