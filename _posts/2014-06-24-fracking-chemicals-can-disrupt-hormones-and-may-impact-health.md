---
layout: post
title: "Fracking Chemicals Can Disrupt Hormones and May Impact Health"
date: 2014-06-24
categories:
author: Science World Report
tags: [Endocrine disruptor,Hydraulic fracturing,Estrogen,Medical specialties,Endocrine system]
---


Now, scientists have found that these chemicals can disrupt not only the human body's reproductive hormones, but also the glucocorticoid and thyroid hormone receptors, which are necessary to maintain good health. In hydraulic fracturing, chemicals and water is injected deep underground under high pressure in order to facture hard rock. However, there has been some concern about the chemicals seeping into groundwater and impacting the natural environment. That's why it's important to see what effect these chemicals could have on humans. In addition 17 inhibited the androgen receptor, 10 hindered the progesterone receptor, 10 blocked the glucocorticoid receptor and 7 inhibited the thyroid hormone receptor.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15612/20140623/fracking-chemicals-disrupt-hormones-impact-health.htm){:target="_blank" rel="noopener"}


