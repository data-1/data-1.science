---
layout: post
title: "Scientists reveal 'superbug's' artillery"
date: 2017-10-19
categories:
author: Monash University
tags: [Exotoxin,Secretion,Bacteria,Pseudomonas aeruginosa,Chemistry,Microscopy,Microbiology,Medical specialties]
---


Credit: Monash University  Monash University's Biomedicine Discovery Institute (BDI) researchers have created the first high-resolution structure depicting a crucial part of the 'superbug' Pseudomonas aeruginosa, classified by the WHO as having the highest level threat to human health. The image identifies the 'nanomachine' used by the highly virulent bacteria to secrete toxins, pointing the way for drug design targeting this. In a paper published this week in the online journal mBio, BDI researchers investigated a protein nanomachine on the surface of the bacterial cells responsible for the secretion of these toxins. Such a drug could potentially reduce virulence by stopping the secretion of toxins while other drugs worked at clearing the infection itself, Dr Hay said. The methodology developed by the researchers would be applicable to other related bacteria surface nanomachines, he said.

<hr>

[Visit Link](https://phys.org/news/2017-10-scientists-reveal-superbug-artillery.html){:target="_blank" rel="noopener"}


