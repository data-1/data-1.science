---
layout: post
title: "Hot lava flows discovered on Venus"
date: 2015-09-03
categories:
author: "$author"   
tags: [Volcano,Venus,Venus Express,Earth sciences,Geology,Nature,Bodies of the Solar System,Astronomical objects known since antiquity,Astronomical objects,Planets,Astronomy,Space science,Physical sciences,Planets of the Solar System,Planetary science,Terrestrial planets]
---


Science & Exploration Hot lava flows discovered on Venus 18/06/2015 56741 views 136 likes  ESA’s Venus Express has found the best evidence yet for active volcanism on Earth’s neighbour planet. Volcanic activity on Venus? This process can bring hot material to the surface, where it may be released through fractures as a lava flow. “But the VMC was designed to make these systematic observations of the surface and luckily we clearly see these regions that change in temperature over time, and that are notably higher than the average surface temperature.”  Because VMC’s view is blurred by the clouds, the areas of increased emission appear spread out over large areas more than 100 km across, but the hot regions on the surface below are probably much smaller. “Our study shows that Venus, our nearest neighbour, is still active and changing in the present day – it is an important step in our quest to understand the different evolutionary histories of Earth and Venus.”  Notes for editors “Active volcanism on Venus in the Ganiki Chasma rift zone,” by E.V.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Venus_Express/Hot_lava_flows_discovered_on_Venus){:target="_blank" rel="noopener"}


