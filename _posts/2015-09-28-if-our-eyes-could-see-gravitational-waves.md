---
layout: post
title: "If our eyes could see gravitational waves"
date: 2015-09-28
categories:
author: "$author"   
tags: [Gravitational wave,Laser Interferometer Space Antenna,Black hole,Astronomy,Outer space,Nature,Physical cosmology,Astronomical objects,Physical phenomena,Science,Astrophysics,Physics,Physical sciences,Space science]
---


This is what the merger of two black holes would look like. It is a computer simulation of the gravitational waves that would ripple away from the titanic collision, a bit like the ripples on a pond when a pebble drops into the water. Gravitational radiation is incredibly difficult to measure. But these detectors can see only half of the picture. The mass of the colliding black holes determines the frequency of the gravitational radiation.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2015/09/If_our_eyes_could_see_gravitational_waves){:target="_blank" rel="noopener"}


