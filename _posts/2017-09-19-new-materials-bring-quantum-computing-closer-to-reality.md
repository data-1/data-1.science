---
layout: post
title: "New materials bring quantum computing closer to reality"
date: 2017-09-19
categories:
author: "Tom Abate, Stanford University"
tags: [Electron,Quantum dot,Silicon carbide,Quantum mechanics,Computing,Integrated circuit,Silicon,Computer,Semiconductor,Quantum computing,Spin (physics),Materials science,Applied and interdisciplinary physics,Materials,Chemistry,Technology]
---


Quantum computers work by isolating spinning electrons inside a new type of semiconductor material. Quantum dots  One way to create this laser-electron interaction chamber is through a structure known as a quantum dot. But like the quantum dot, most diamond color center experiments require cryogenic cooling. So she worked with another global team to experiment with a third material, silicon carbide. Vuckovic's team knocked certain silicon atoms out of the silicon carbide lattice in a way that created highly efficient color centers.

<hr>

[Visit Link](https://phys.org/news/2017-05-materials-quantum-closer-reality.html){:target="_blank" rel="noopener"}


