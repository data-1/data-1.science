---
layout: post
title: "Biochemists discover mechanism that helps flu viruses evolve: Influenza viruses can hijack host cellular machinery to help mutated viral proteins fold and function"
date: 2017-10-04
categories:
author: "Massachusetts Institute of Technology"
tags: [Virus,Antiviral drug,Influenza,Severe acute respiratory syndrome coronavirus 2,Orthomyxoviridae,Biology,Life sciences,Medical specialties,Biotechnology,Biochemistry,Virology,Microbiology,Molecular biology,Clinical medicine]
---


The MIT team found that flu viruses' rapid evolution relies in part on their ability to hijack some of the cellular machinery of the infected host cell -- specifically, a group of proteins called chaperones, which help other proteins fold into the correct shape. The findings suggest that interfering with host cell chaperones could help prevent flu viruses from becoming resistant to existing drugs and vaccines, says Matthew Shoulders, the Whitehead Career Development Associate Professor of Chemistry at MIT. Our data suggest that, at some point in the future, targeting host chaperones might restrict the ability of a virus to evolve and allow us to kill viruses before they become drug resistant. Of particular interest to flu researchers is the gene for the hemagglutinin protein, which is displayed on the surface of the viral envelope and interacts with cells of the infected host. Targeting this phenomenon could offer a way to delay viral evolution and decelerate escape from existing drugs and vaccines, the researchers say.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/09/170926112012.htm){:target="_blank" rel="noopener"}


