---
layout: post
title: "Germany, Israel, and the UK turn to open source, new driverless car tech, and more news"
date: 2018-04-28
categories:
author: "Scott Nesbitt
(Alumni)"
tags: [Open source,Nextcloud,Curriculum,Amazon Web Services,Self-driving car,Education,Cloud computing,Red Hat,Computing,Information technology,Technology]
---


In this edition of our open source news roundup, we take a look three governments turning to open source, open sourcing driverless car safety practices, a school district developing an open source curriculum, and more. The code for the government's web portal gov.il will be the first to be released to the public, with other services being encouraged to follow suit. This is the second open source curriculum the district has developed. FOSSology celebrates 10 years  An open source project you've probably never heard of has hit a significant milestone. FOSSology, an open source license compliance toolkit, has turned 10.

<hr>

[Visit Link](https://opensource.com/article/18/4/news-april-28){:target="_blank" rel="noopener"}


