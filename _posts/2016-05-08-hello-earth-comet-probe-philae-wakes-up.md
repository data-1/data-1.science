---
layout: post
title: "'Hello Earth!': Comet probe Philae wakes up"
date: 2016-05-08
categories:
author: Mariette Le Roux
tags: [Philae (spacecraft),Rosetta (spacecraft),67PChuryumovGerasimenko,Lander (spacecraft),Astronomy,Planetary science,Space exploration,Spacecraft,Bodies of the Solar System,Flight,Astronautics,Outer space,Solar System,Spaceflight,Space science]
---


The lander is ready for operations. Stephan Ulamec, Philae project manager of German Aerospace Centre, says the lander is doing very well  A tweet in the name of Rosetta announced: Incredible news! - November 12, 2014: Rosetta sends down a 100-kilogramme (220-pound) lander, Philae, equipped with 10 instruments. After bouncing several times, Philae ends in a dark but unknown location and does not have enough sunlight to recharge its batteries. Escorted by Rosetta and with Philae on its back, the comet heads out of the inner Solar System.

<hr>

[Visit Link](http://phys.org/news353503692.html){:target="_blank" rel="noopener"}


