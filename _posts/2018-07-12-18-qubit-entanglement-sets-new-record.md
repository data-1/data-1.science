---
layout: post
title: "18-qubit entanglement sets new record"
date: 2018-07-12
categories:
author: "Lisa Zyga"
tags: [Quantum entanglement,Quantum information,Quantum teleportation,Photon,Qubit,Theoretical physics,Quantum mechanics,Physics,Applied mathematics,Theoretical computer science,Quantum information science,Computer science,Science,Branches of science,Scientific theories,Technology]
---


Credit: Wang et al. ©2018 American Physical Society  Physicists have experimentally demonstrated 18-qubit entanglement, which is the largest entangled state achieved so far with individual control of each qubit. Generally, there are two ways to increase the number of effective qubits in an entangled state: use more particles, or exploit the particles' additional degrees of freedom (DoFs). When exploiting multiple DoFs, the entanglement is called hyper-entanglement. In addition, a hyper-entangled 18-qubit state that exploits three DoFs is approximately 13 orders of magnitude more efficient than an 18-qubit state composed of 18 photons with a single DoF. With these advantages, the physicists expect that the ability to achieve 18-qubit hyper-entanglement will lead to previously unprecedented areas of research, such as experimentally realizing certain codes for quantum computing, implementing quantum teleportation of high-dimensional quantum states, and enabling more extreme violations of local realism.

<hr>

[Visit Link](https://phys.org/news/2018-07-qubit-entanglement.html){:target="_blank" rel="noopener"}


