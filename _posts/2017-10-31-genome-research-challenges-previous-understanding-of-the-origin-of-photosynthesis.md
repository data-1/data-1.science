---
layout: post
title: "Genome research challenges previous understanding of the origin of photosynthesis"
date: 2017-10-31
categories:
author: Leah Sloan, Us Department Of Energy
tags: [Evolution,Photosynthesis,Science,Nature,Biology]
---


However, further studies of anoxygenic photosynthesis, which does not produce oxygen, are crucial to understanding how early microbial metabolisms may have influenced the geochemical cycles of the planet. Photosynthesis supports the majority of life on our planet; however, we know very little about when this important metabolism evolved, explained Patrick Shih, a postdoctoral researcher in Berkeley Lab's Environmental Genomics and Systems Biology Division and co-first author of a recent study in the Proceedings of the National Academy of Sciences titled Evolution of the 3-hydroxypropionate bicycle and recent transfer of anoxygenic photosynthesis into the Chloroflexi. We looked through the genomes of different Chloroflexi and used phylogenetics to show that this group could not have been one of the original inventors of photosynthesis. Using a combination of comparative genomics and molecular clock analyses, researchers determined that phototrophic members of the Chloroflexi phylum evolved far too recently to have been the progenitors of oxygenic photosynthesis, a process that evolved about 2.3 billion years ago. We used phylogenetic methods to estimate when evolutionary events occurred, specifically molecular clock analyses.

<hr>

[Visit Link](https://phys.org/news/2017-10-genome-previous-photosynthesis.html){:target="_blank" rel="noopener"}


