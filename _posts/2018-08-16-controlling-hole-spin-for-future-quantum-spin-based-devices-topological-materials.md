---
layout: post
title: "Controlling hole spin for future quantum spin-based devices, topological materials"
date: 2018-08-16
categories:
author: ""
tags: [Electron hole,Spinorbit interaction,Electron,Spin (physics),Spintronics,Zeeman effect,Majorana fermion,Phases of matter,Electrical engineering,Materials,Chemistry,Electricity,Materials science,Physical sciences,Condensed matter,Condensed matter physics,Applied and interdisciplinary physics,Theoretical physics,Electromagnetism,Quantum mechanics,Physics]
---


Credit: FLEET  The 'spins' of electrons (and holes) in semiconductors have potential applications in spintronics, spin-based quantum computing, and topological systems. Because of this coupling, electrons (or holes) in motion 'sense' the electric field of the nucleus as an effective magnetic field, which then causes the electrons (or holes) to have two opposite spin orientations with an energy difference – an analogy of Zeeman splitting. In the study, the researchers demonstrated an entirely new mechanism for electrically controlling holes' spin in a quantum well, exploiting the unusual spin 3/2 nature of holes. Thanks to the strong spin-orbit interaction, the researchers showed that by solely using electric fields to increase the holes' momentum, the Zeeman splitting could be enhanced by as much as 300%. The extreme tunability of the Zeeman splitting via electric fields opens new possibilities for future quantum spin-based devices, such as spin transistors, spin-orbit qubits, and quantum logic gates.

<hr>

[Visit Link](https://phys.org/news/2018-08-hole-forfuture-quantum-spin-based-devices.html){:target="_blank" rel="noopener"}


