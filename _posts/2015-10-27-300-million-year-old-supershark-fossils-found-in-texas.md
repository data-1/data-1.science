---
layout: post
title: "300 million-year-old 'supershark' fossils found in Texas"
date: 2015-10-27
categories:
author: Society Of Vertebrate Paleontology
tags: [Shark,Fossil,Megalodon,Paleontology]
---


Previously, giant sharks had only been recovered from rock dating back 130 million years, during the age of the dinosaurs. The largest shark that ever lived, commonly called Megalodon, is much younger, with an oldest occurrence at about 15 million years ago. Although not nearly as large as Megalodon, which might have reached up to 67 feet in length (about 20 meters), the fossil sharks from Texas would have been by far the biggest sharks in the sea. Maisey, McKinzie, and Williams timed their research results very well, being able to present their Texas 'supershark' at the annual meeting for the Society of Vertebrate Paleontology in Dallas, Texas on October 16. According to Maisey, even 300 million years ago, everything is bigger in Texas!

<hr>

[Visit Link](http://phys.org/news/2015-10-million-year-old-supershark-fossils-texas.html){:target="_blank" rel="noopener"}


