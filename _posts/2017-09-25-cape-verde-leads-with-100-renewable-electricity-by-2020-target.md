---
layout: post
title: "Cape Verde Leads With 100% Renewable Electricity By 2020 Target"
date: 2017-09-25
categories:
author: "Steve Hanley, Remeredzai Joseph Kuhudzai, Written By"
tags: [Renewable energy,Clean technology,Cape Verde,Energy,Nature,Sustainable energy,Economy,Energy and the environment,Physical quantities,Sustainable development,Sustainable technologies,Climate change mitigation,Renewable resources,Natural environment,Economy and the environment,Natural resources]
---


To accomplish that goal, it established Project Cabeólica, the first public–private partnership to deliver commercial-scale wind power in sub-Saharan Africa. Combined with its solar installations, it gets more than a third of its energy from renewables. It not only wants to meet all its electrical needs from renewables; it wants to be a resource for all of Africa so it can help neighboring countries accomplish the same thing. “Cape Verde wants to serve as a laboratory,” he says. “We see our investment in renewable energy as something larger,” Mr Costa said.

<hr>

[Visit Link](http://cleantechnica.com/2016/10/03/cape-verde-leads-100-renewable-electricity-2020-target/){:target="_blank" rel="noopener"}


