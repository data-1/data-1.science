---
layout: post
title: "Some plants regenerate by duplicating their DNA"
date: 2016-04-11
categories:
author: University Of Illinois At Urbana-Champaign
tags: [Genome,Gene duplication,Arabidopsis thaliana,Polyploidy,Plant,Cell (biology),Biology,Genetics,Life sciences,Biotechnology]
---


Credit: L. Brian Stauffer  When munched by grazing animals (or mauled by scientists in the lab), some herbaceous plants overcompensate - producing more plant matter and becoming more fertile than they otherwise would. Their study is the first to show that a plant's ability to dramatically rebound after being cut down relies on a process called genome duplication, in which individual cells make multiple copies of all of their genetic content. In a 2011 study, Paige and Scholes demonstrated that plants that engage in rampant genome duplication also rebound more vigorously after being damaged. In the new study, Scholes crossed Arabidopsis plants that had the ability to duplicate their genomes with those that lacked this ability. Animal biology professor Ken Paige (left) and postdoctoral fellow Daniel Scholes found that a plant's ability to duplicate its genome within individual cells influences its ability to regenerate.

<hr>

[Visit Link](http://phys.org/news334932790.html){:target="_blank" rel="noopener"}


