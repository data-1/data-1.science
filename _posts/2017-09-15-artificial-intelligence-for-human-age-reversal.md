---
layout: post
title: "Artificial intelligence for human age-reversal"
date: 2017-09-15
categories:
author: "InSilico Medicine"
tags: [Ageing,Insilico Medicine,Artificial intelligence,Deep learning,Aging-associated diseases,Health,Life sciences,Biology,Biochemistry,Biotechnology,Medicine,Branches of science,Technology]
---


Center for Healthy Aging at the University of Copenhagen today announced a research collaboration with a company specializing in artificial intelligence (AI) to develop solutions for preventing early aging. Experts in the genetics of aging at the Department of Cellular and Molecular Medicine partnered with the Baltimore-based company, Insilico Medicine, specializing in AI to find molecules that can be developed into drugs to cure and prevent these diseases. The collaboration with Insilico Medicine will allow us to find the molecules that repair DNA and prevent accelerated aging, said the head of the biology of aging lab and assistant professor Morten Scheibye-Knudsen, Center for Healthy Aging. We hope that cooperation can lead to the development of some new drugs that can prevent early aging, thus ensuring increased health spans for everyone. It is therefore increasingly pertinent to find interventions for age-associated diseases such as Alzheimer's, Parkinson's and cardiovascular diseases.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-08/imi-aif080117.php){:target="_blank" rel="noopener"}


