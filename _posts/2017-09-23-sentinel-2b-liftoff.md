---
layout: post
title: "Sentinel-2B liftoff"
date: 2017-09-23
categories:
author: ""
tags: [Sentinel-2,Remote sensing,Spacecraft,Earth observation satellites,Satellites,Spaceflight,Outer space,Satellites orbiting Earth,European space programmes,Space agencies,Space policy of the European Union,Copernicus Programme]
---


Replay of the Sentinel-2B liftoff on a Vega launcher from Europe’s Spaceport in French Guiana at 01:49 GMT (02:49 CET) on 7 March 2017. Sentinel-2B is the second satellite in the Sentinel-2 mission for Europe’s Copernicus environment monitoring programme. Designed as a two-satellite constellation – Sentinel-2A and -2B – the Sentinel-2 mission carries an innovative wide swath high-resolution multispectral imager with 13 spectral bands for a new perspective of our land and vegetation. This information is used for agricultural and forestry practices and for helping manage food security. It also provides information on pollution in lakes and coastal waters.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2017/03/Sentinel-2B_liftoff){:target="_blank" rel="noopener"}


