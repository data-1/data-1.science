---
layout: post
title: "Solar Impulse takes off for six-day, six-night Pacific flight"
date: 2016-05-04
categories:
author: Bill Savadove
tags: [Solar Impulse,Aviation,Aircraft,Transport,Aeronautics,Vehicles]
---


Pilot Andre Borschberg, 62, left the ground in Nanjing, in eastern China, heading for the US island of Hawaii, at about 2:40 am (1840 GMT), after extended delays awaiting a suitable weather window over safety concerns. People take pictures as the Swiss-made solar-powered plane Solar Impluse 2 takes off from Nanjing's Lukou International Airport in Nanjing, in China's eastern Jiangsu province, early on May 31, 2015  'We have a parachute'  After more than eight hours in the air, the plan was over the East China sea and Borschberg could be heard discussing light turbulence along the upcoming journey with the Solar Impulse team. Planners had identified airports in Japan should the plane need to make a stop because of technical problems, but the open ocean offers no such possibility, he said. In advance of the Pacific flight, the crew stripped off two side wheels and internal brakes from the propellers to make the fragile-looking craft—already just 2.3 tonnes, the weight of a large SUV—as light as possible. Now is the moment just to demonstrate what this airplane can do—fly day and night with no fuel, Bertrand Piccard, who has flown the Solar Impulse on other stages of the voyage, told reporters by video link.

<hr>

[Visit Link](http://phys.org/news352274688.html){:target="_blank" rel="noopener"}


