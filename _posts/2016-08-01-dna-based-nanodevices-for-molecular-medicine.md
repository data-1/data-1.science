---
layout: post
title: "DNA-based nanodevices for molecular medicine"
date: 2016-08-01
categories:
author: "Aalto University"
tags: [DNA nanotechnology,DNA origami,DNA,Targeted drug delivery,Materials science,Biochemistry,Applied and interdisciplinary physics,Materials,Biotechnology,Emerging technologies,Physical sciences,Nanotechnology,Biology,Life sciences,Technology,Chemistry]
---


Furthermore, the researchers from Aalto University and University of Jyväskylä have recently shown how DNA origamis can be used in efficient fabrication of custom-shaped metal nanoparticles that could be used in various fields of material sciences. Groundbreaking approach to create nanomaterials  The research group lead by Professor Mauri Kostiainen works extensively with DNA nanostructures, and the group has just recently published two research articles regarding DNA-based applications in biotechnology and molecular medicine. ###  Article in Trends in Biotechnology: V. Linko, A. Ora, M. A. Kostiainen, DNA Nanostructures as Smart Drug-Delivery Vehicles and Molecular Devices. Virus-protein -coated DNA origami nanostructures. Additional information:  Veikko Linko, Academy of Finland Postdoctoral Researcher  Aalto University School of Chemical Technology  Tel: +358 50 408 1810  veikko.linko@aalto.fi  Mauri A. Kostiainen, Professor, Leader of the Biohybrid Materials Group  Aalto University School of Chemical Technology  Tel: + 358 50 362 7070  mauri.kostiainen@aalto.fi  Web page of the research group: http://chemtech.aalto.fi/bihy  Aalto University.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/au-dnf092415.php){:target="_blank" rel="noopener"}


