---
layout: post
title: "Liquid DNA behind virus attacks"
date: 2016-03-26
categories:
author: Lund University
tags: [Virus,Herpes simplex virus,Infection,Bacteria,DNA,Severe acute respiratory syndrome coronavirus 2,Bacteriophage,Phase (matter),Biotechnology,Clinical medicine,Biology,Life sciences,Microbiology,Medical specialties]
---


Viruses can convert their DNA from solid to fluid form, which explains how viruses manage to eject DNA into the cells of their victims. Both research studies are about the same discovery made for two different viruses, namely that viruses can convert their DNA to liquid form at the moment of infection. One of the studies investigated the herpes virus, which infects humans. The results show that this virus also has the ability to convert its DNA from solid to fluid form. In previous studies, Alex Evilevitch and his colleagues have succeeded in measuring the DNA pressure inside the virus that provides the driving force for infection.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/lu-ldb100614.php){:target="_blank" rel="noopener"}


