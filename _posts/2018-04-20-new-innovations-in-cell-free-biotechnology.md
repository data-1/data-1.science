---
layout: post
title: "New innovations in cell-free biotechnology"
date: 2018-04-20
categories:
author: "Stephan Benzkofer, Northwestern University"
tags: [Cell-free protein synthesis,Protein,Cell-free system,Biology,Life sciences,Biotechnology,Biochemistry,Molecular biology,Chemistry,Technology,Cell biology]
---


We developed a bacterial cell-free protein synthesis system that is capable of high level expression of pure proteins containing multiple non-canonical amino acids, said Michael Jewett, associate professor of chemical and biological engineering at Northwestern's McCormick School of Engineering. Without the worry of trying to keep a cell alive, this process opens up many possibilities, including the synthesis of new classes of enzymes, therapeutics, materials, and chemicals with diverse chemistry. A living cell may balk when asked to do something it hasn't seen in its evolutionary biology, not so for a cell-free protein synthesis (CFPS) platform. But Jewett and the team produced the highest yields of proteins with non-canonical amino acids ever reported for in vitro systems, suggesting that long-term commercial applications for CFPS might be realistic. The second element came courtesy of Jewett's student, Rey Martin, the lead author of the paper.

<hr>

[Visit Link](https://phys.org/news/2018-03-cell-free-biotechnology.html){:target="_blank" rel="noopener"}


