---
layout: post
title: "Patterns are math we love to look at"
date: 2016-07-11
categories:
author: "Frank A Farris, The Conversation"
tags: [Pattern,Symmetry,Cartesian coordinate system,Wallpaper group,Frieze group,Science,Geometry]
---


Credit: Frank A Farris, CC BY-ND  Mathematicians call this a frieze pattern because it repeats over and over again left and right. This row of letters definitely has what we call translational symmetry, since we can slide along the row, one A at a time, and wind up with the same pattern. What other symmetries does it have? First flip your hand over (the mirror symmetry), then slide it one unit to the right (the translation). The name for the pattern of A's is p1m1: no rotation, vertical mirror, no horizontal feature beyond translation.

<hr>

[Visit Link](http://phys.org/news/2015-09-patterns-math.html){:target="_blank" rel="noopener"}


