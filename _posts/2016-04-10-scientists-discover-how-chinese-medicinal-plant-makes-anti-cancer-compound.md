---
layout: post
title: "Scientists discover how Chinese medicinal plant makes anti-cancer compound"
date: 2016-04-10
categories:
author: John Innes Centre
tags: [Biotechnology,Science,Research,American Association for the Advancement of Science,Biotechnology and Biological Sciences Research Council]
---


New research led by Professor Cathie Martin of the John Innes Centre has revealed how a plant used in traditional Chinese medicine produces compounds which may help to treat cancer and liver diseases. It is wonderful to have collaborated with Chinese scientists on these traditional medicinal plants. It's exciting to consider that the plants which have been used as traditional Chinese remedies for thousands of years may lead to effective modern medicines. The knowledge, resources and trained researchers we generate help global societies address important challenges including providing sufficient and affordable food, making new products for human health and industrial applications, and developing sustainable bio-based manufacturing. The John Innes Centre is strategically funded by the Biotechnology and Biological Sciences Research Council (BBSRC).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/jic-sdh040616.php){:target="_blank" rel="noopener"}


