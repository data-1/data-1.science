---
layout: post
title: "Earliest 'modern' hand nearly two million years old"
date: 2016-05-27
categories:
author: Marlowe Hood
tags: [Hand,Phalanx bone,Homo,Human evolution,Olduvai Gorge,Homo habilis]
---


A picture released on August 18, 2015 by Nature Communications shows a OH 86 hominin manual proximal phalanx in (from L-R) dorsal, lateral, palmar (distal is top for each) and proximal views  A tiny, 1.85 million-year-old bone from the little finger of a human ancestor unearthed in East Africa has revealed the oldest modern hand ever found, scientists reported Tuesday. One is a longer thumb, allowing us to grip more precisely and to open our hands more fully. Our discovery not only shows that a creature—dubbed OH 86—with a modern-looking hand existed 1.85 million years ago, it also shows that OH 86 was bigger sized than any other prior and contemporary hominin, said Dominguez-Rodrigo. It brings support to those who challenge the view that Homo habilis was the maker of the stone artifacts becoming abundant in layers of this time period, commented Jean-Jacques Hublin, director of the department of human evolution at the Max Planck Institute for Evolutionary Anthropology. One single bone from a pinkie finger does not imply a whole modern human-like skeleton, Hublin said by email.

<hr>

[Visit Link](http://phys.org/news/2015-08-earliest-modern-million-years.html){:target="_blank" rel="noopener"}


