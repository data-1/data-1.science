---
layout: post
title: "A hellacious two weeks on Jupiter's moon Io"
date: 2015-07-17
categories:
author: University of California - Berkeley 
tags: [Io (moon),Volcanism on Io,Volcano,Outer space,Planets,Solar System,Bodies of the Solar System,Planets of the Solar System,Physical sciences,Astronomical objects,Space science,Planetary science,Astronomy]
---


Aside from Earth, is the only known place in the solar system with volcanoes erupting extremely hot lava like that seen on Earth. De Pater's long-time colleague and coauthor Ashley Davies, a volcanologist with NASA's Jet Propulsion Laboratory at the California Institute of Technology in Pasadena, Calif., said that the recent eruptions match past events that spewed tens of cubic miles of lava over hundreds of square miles in a short period of time. De Kleer timed her Gemini and IRTF observations to coincide with observations of the plasma torus by the Japanese HISAKI (SPRINT-A) spacecraft, which is in orbit around Earth, so she can correlate the different data sets. In a third paper accepted by Icarus, de Pater, Davies and their colleagues summarize a decade of Io observations with the Keck II and Gemini telescopes. In 2010 the hot spots were dominated by two volcanic centers: Loki Patera, an extremely large active lava lake on Io, and Kanehekili Fluctus, an area of continuing pahoehoe lava flows.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/uoc--aht080114.php){:target="_blank" rel="noopener"}


