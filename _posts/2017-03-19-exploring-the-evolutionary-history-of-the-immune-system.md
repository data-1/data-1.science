---
layout: post
title: "Exploring the evolutionary history of the immune system"
date: 2017-03-19
categories:
author: "Charité - Universitätsmedizin Berlin"
tags: [ALOX15,Evolution,Enzyme,Primate,Cell biology,Biotechnology,Molecular biology,Biology,Biochemistry]
---


As mammals have evolved, this enzyme has undergone changes to both its structure and function. Researchers from Charité - Universitätsmedizin Berlin have found that human ALOX15 appears to have developed a much higher capacity to stimulate the production of these lipid mediators than the enzyme variant found in lower primates. Results from this study have been published in the current issue of the journal Proceedings of the National Academy of Sciences*. This allowed us to conclude the functional alterations ALOX15 has experienced during late primate evolution. Prof. Kühn adds: Our results show that the functional characteristics of the ALOX15 enzyme have evolved to improve the body's control mechanisms of the inflammatory response, and thus to optimize the human immune systems.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-12/c-ub-ete120916.php){:target="_blank" rel="noopener"}


