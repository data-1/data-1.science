---
layout: post
title: "Photons simulate time travel in the lab – Physics World"
date: 2015-07-26
categories:
author: ""   
tags: [Time travel,Time,Spacetime,Quantum mechanics,Wormhole,Closed timelike curve,Physics,Photon,Theoretical physics,Science,Applied and interdisciplinary physics,Scientific theories,Physical sciences,Scientific method,Theory of relativity]
---


Caught in a loop: has time travel been simulated? They say they have prepared photons that behave as if they are travelling along short cuts in space–time known as “closed time-like curves”, and add that their work might help in the long-sought-after unification of quantum mechanics and gravity. In quantum mechanics, however, such paradoxes can be avoided. The paradox emerges if the particle started out in state one, travelled backwards in time, met a younger version of itself and then flipped the value of its earlier self to zero. They also found that the presence of a closed time-like curve allows an observer to perfectly distinguish non-orthogonal states of the time-travelling photon, such as horizontal and diagonal polarizations.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/dyA4O-TM2ns/photons-simulate-time-travel-in-the-lab){:target="_blank" rel="noopener"}


