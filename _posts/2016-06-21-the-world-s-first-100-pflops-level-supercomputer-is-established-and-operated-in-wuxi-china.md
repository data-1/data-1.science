---
layout: post
title: "The world's first 100-PFlops-level Supercomputer is established and operated in Wuxi, China"
date: 2016-06-21
categories:
author: "Science China Press"
tags: [Supercomputer,FLOPS,Sunway TaihuLight,System,Science,Earth system science,Computers,Computing,Technology,Computer science,Computer hardware,Computer engineering]
---


The Sunway TaihuLight supercomputer is the first system in the world that has a peak performance of over 100 PFlops. An overall introduction to the TaihuLight system, The Sunway TaihuLight Supercomputer: System and Applications, written by Dr. Haohuan Fu, the deputy director of NSCC-Wuxi, and the other authors, is published on SCIENCE CHINA Information Sciences. It provides a detailed illustration of the Sunway TaihuLight supercomputer, and its subsystems, including: the software system, the hardware system, the power system, the architecture of the new SW260101 many-core processor, and it also provides introductions to how research fields, such as earth system modeling, ocean surface wave modeling, atomistic simulation, and phase-field simulation, benefit from the supercomputer. The computational paradigm has been applied to various scientific domains, such as climate modeling, earth subsurface modeling and inversion, sky simulation, and phase-field simulation, with significant contributions to the advancement of those fields. The Sunway TaihuLight supercomputer: system and applications.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/scp-twf061816.php){:target="_blank" rel="noopener"}


