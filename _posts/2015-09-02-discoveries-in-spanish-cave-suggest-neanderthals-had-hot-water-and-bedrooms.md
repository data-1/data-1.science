---
layout: post
title: "Discoveries in Spanish cave suggest Neanderthals had hot water and bedrooms"
date: 2015-09-02
categories:
author: "$author"   
tags: [Neanderthal,Kraken,Archaeology,Human]
---


The finding adds to the mounting evidence that Neanderthals were a lot more sophisticated than previously thought and were at least as advanced, if not more so, than early Homo sapiens. The area identified as a bedroom in the Catalonia cave. Credit: Palmira Saladié/IPHES  While the archaeologists at the IPHES claimed that the 2015 finding presents the first evidence of a Neanderthal ‘bedroom’, findings in a collapsed rock shelter in Italy in 2013 already confirmed that Neanderthals had an organized use of space, something that had previously only been attributed to humans. The rock shelter in Italy contained separate levels. The middle level contained the most traces of human occupation and seems to have been a long-term sleeping area.

<hr>

[Visit Link](http://www.ancient-origins.net/news-evolution-human-origins/discoveries-spanish-cave-suggest-neanderthals-had-hot-water-020505){:target="_blank" rel="noopener"}


