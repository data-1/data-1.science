---
layout: post
title: "World’s first biologically powered chip created"
date: 2016-03-16
categories:
author:  
tags: []
---


In a major breakthrough, researchers at Columbia Engineering have harnessed the molecular machinery of living systems to power an integrated circuit. They achieved this by integrating a conventional solid-state complementary metal-oxide-semiconductor (CMOS) integrated circuit with an artificial lipid bilayer membrane containing adenosine triphosphate (ATP)-powered ion pumps. In living systems, ATP is used to transport energy from where it is generated to where it is consumed in the cell. While other groups have harvested energy from living systems, Ken Shepard and his team are exploring how to do this at the molecular level, isolating just the desired function and interfacing this with electronics. We just grab the component of the cell that’s doing what we want.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/technology/worlds-first-biologically-powered-chip-created/article7958437.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


