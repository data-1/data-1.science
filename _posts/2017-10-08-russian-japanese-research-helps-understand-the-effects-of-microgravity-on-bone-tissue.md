---
layout: post
title: "Russian-Japanese research helps understand the effects of microgravity on bone tissue"
date: 2017-10-08
categories:
author: "Kazan Federal University"
tags: [Bone,Osteoblast,Sp7 transcription factor,Osteoclast,Japanese rice fish,Micro-g environment,Glucocorticoid,Biology,Biochemistry]
---


The paper appeared in Scientific Reports. Oleg Gusev explains, «Fishes are one of the test organisms that give an opportunity to outline space flight effects on one's health. These different proteins are formed in osteoclasts (bone cells that eliminate the old bone tissue) and osteoblasts (the ones that create new bone tissue). Specific genes osterix and osteocalcin can react to gravity shifts because their activity rose simultaneously during the experiment. Earlier other researchers have shown that glucocorticoid hormones increase the activity of trans­cription factor AP-1 that regulates gene expression in response to a variety of stimuli, including cytokines, growth factors, stress, and bacterial and viral infections.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/kfu-rrh012617.php){:target="_blank" rel="noopener"}


