---
layout: post
title: "NASA-funded mission to study the sun's energy"
date: 2014-07-13
categories:
author: "$author" 
tags: [Solar irradiance,Sun,Extreme ultraviolet,Radiation,Electromagnetic radiation,Space science,Science,Physical phenomena,Physical sciences,Optics,Chemistry,Electromagnetism,Applied and interdisciplinary physics,Physical chemistry,Atomic molecular and optical physics]
---


On July 14, 2014, a sounding rocket will be ready to launch from White Sands Missile Range, New Mexico a little before noon local time. Soaring up to 180 miles into Earth's atmosphere, past the layers that can block much of the sun's high energy light, the Degradation Free Spectrometers experiment will have six minutes to observe the extreme ultraviolet and soft x-rays streaming from the sun, in order to measure the sun's total energy output, known as irradiance, in these short wavelengths. Data observations from recent missions have provided significantly improved measurements of irradiance, said Leonid Didkovsky, the principal investigator for the mission at the University of Southern California in Los Angeles. But the optical components of many of these missions can degrade during the time of the mission. One is called the Rare Gas Ionization Cell absolute irradiance detector and the second is the Solar EUV Monitor—a clone of an instrument on board the European Space Agency's and NASA's Solar and Heliospheric Observatory.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/nsfc-nmt071014.php){:target="_blank" rel="noopener"}


