---
layout: post
title: "Scientists create electric circuits inside plants"
date: 2017-10-06
categories:
author: "Stuart Thompson, The Conversation"
tags: [Plant,Electricity,Electronic circuit,Water,Xylem,Energy,Fuel,Nature,Technology]
---


To do this, we'd need a way of getting the energy out in the form of electricity. The Finnish researchers, whose work is published in PNAS, developed a chemical that was fed into a rose cutting to form a solid material that could carry and store electricity. For the new research, they designed a molecule called ETE-S that forms similar electrical conductors but can also be carried wherever the stream of water travelling though the xylem goes. Credit: Pixabay  E-plants  How well these electrical networks formed surprised even their developers. But the technology could also help us better understand regular plants.

<hr>

[Visit Link](https://phys.org/news/2017-02-scientists-electric-circuits.html){:target="_blank" rel="noopener"}


