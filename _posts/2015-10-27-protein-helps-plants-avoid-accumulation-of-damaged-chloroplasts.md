---
layout: post
title: "Protein helps plants avoid accumulation of damaged chloroplasts"
date: 2015-10-27
categories:
author: American Association for the Advancement of Science (AAAS) 
tags: [Chloroplast,Reactive oxygen species,Joanne Chory,Cell biology,Biology,Biochemistry,Biotechnology,Chemistry,Life sciences,Cellular processes]
---


The identification of a protein that selectively clears damaged chloroplasts from plant cells reveals how plants maintain a clean workshop during the process of photosynthesis. Jesse Woodson and colleagues therefore created two strains of mutant plants each lacking one of these enzymes and subjected the plants to varying doses of light to observe changes in their chloroplasts. Tests of FC2 mutants that also had the PUB4 mutation revealed accumulated Proto and oxygen, but no degradation of the plants' chloroplasts, indicating that PUB4 plays a direct role in signaling chloroplast degradation. Looking closer at healthy plants, the researchers found that PUB4 plays a selective role in chloroplast quality control, further highlighting the function of this protein in reducing oxidative stress. Sinson at University of California, San Diego in La Jolla, CA; J. Gilkerson at Howard Hughes Medical Institute in La Jolla, CA; P.A.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/aaft-php101915.php){:target="_blank" rel="noopener"}


