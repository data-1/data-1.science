---
layout: post
title: "Achievement unlocked: TEDx celebrates 100,000 talks!"
date: 2017-10-20
categories:
author: Ted Staff
tags: [TED (conference),American websites,International organizations,YouTube channels,Internet television channels,Crowdsourcing,Educational websites,Trade fairs,World Wide Web,Websites,American educational websites,International conferences,Conferences,Education events,Peabody Award-winning websites,Technology conferences,Business conferences,Technology events,Education-related YouTube channels,Academic conferences,Online edutainment,Communication]
---


Ideas never sleep — and neither does the TEDx archive. Since going online in 2009, the archive has amassed thousands of perspective-shifting talks from across the world — and we’ve just reached 100,000! So, to celebrate TEDx and its rich library of incredible ideas (honestly, where else but on the TEDx archive will you hear from your city’s leading fly expert, a neuroscientist and a farm activist all in one place? ), we’re highlighting this moment with some of our favorite milestones to date.

<hr>

[Visit Link](https://blog.ted.com/achievement-unlocked-tedx-celebrates-100000-talks/){:target="_blank" rel="noopener"}


