---
layout: post
title: "How does a machine smell? Better than it did"
date: 2015-12-06
categories:
author: University of Manchester 
tags: [Odor,Biosensor,Olfaction,Neuroscience]
---


The human nose can distinguish between some of these molecules and the different forms of the same molecule of carvone, for example, can smell either like spearmint or caraway. A collaboration of academics from The University of Manchester and the University of Bari in Italy, have created a biosensor that utilises an odorant binding protein. The team have found a method of manufacturing these proteins in quantities that would allow them to be used in biosensors. They have developed methods to change the way the proteins react so that they can recognise different types of chemicals. Now we have done this it will allow much better sensors to be developed and these could have many uses in industry.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/uom-hda011515.php){:target="_blank" rel="noopener"}


