---
layout: post
title: "Types of athletic training affect how brain communicates with muscles"
date: 2016-07-04
categories:
author: "University of Kansas"
tags: [Strength training,Skeletal muscle,Exercise,Determinants of health,Clinical medicine,Hobbies,Management of obesity,Physical exercise,Health,Physical fitness,Self-care,Health and sports,Recreation]
---


A University of Kansas study shows that the communication between the brain and quadriceps muscles of people who take part in endurance training, such as running long distances, is different than those who regularly took part in resistance training and those who were sedentary. The communication between the brains and their muscles was slightly different than the resistance trainers and sedentary individuals, Herda said of endurance trainers. The endurance trainers had consistently taken part in a structured running program for at least three years prior to the study and ran an average of 61 miles a week and did not take part in resistance training. The sedentary participants did not take part in any structured physical exercise for three years prior to the study. While not claiming that one type of exercise or sport is superior to another, Herda said the findings suggest that the human body's neuromuscular system may be more naturally inclined to adapt to aerobic exercise than resistance training for strength as the communication between the brain and muscles was similar between resistance training and sedentary individuals.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/uok-toa091815.php){:target="_blank" rel="noopener"}


