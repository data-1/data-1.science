---
layout: post
title: "JILA strontium atomic clock sets new records"
date: 2015-05-20
categories:
author: ""    
tags: [Atomic clock,Physics,Science,Physical sciences,Applied and interdisciplinary physics,Metrology]
---


JILA strontium atomic clock sets new records    by Staff Writers    Boulder CO (SPX) Apr 27, 2015    JILA's strontium lattice atomic clock now performs better than ever because scientists literally take the temperature of the atoms' environment. As described in Nature Communications, the experimental strontium lattice clock at JILA, a joint institute of NIST and the University of Colorado Boulder, is now more than three times as precise as it was last year, when it set the previous world record. The clock's stability-- how closely each tick matches every other tick--also has been improved by almost 50 percent, another world record. Relativistic geodesy is the idea of using a network of clocks as gravity sensors to make 3D precision measurements of the shape of the Earth. JILA and NIST scientists detect strontium's ticks (430 trillion per second) by bathing the atoms in very stable red laser light at the exact frequency that prompts the switch between energy levels.

<hr>

[Visit Link](http://www.spacedaily.com/reports/JILA_strontium_atomic_clock_sets_new_records_999.html){:target="_blank" rel="noopener"}


