---
layout: post
title: "Pluto's moon Nix"
date: 2016-06-29
categories:
author: "Matt Williams"
tags: [Nix (moon),Hydra (moon),Pluto,New Horizons,Moons of Pluto,Plutinos,Astronomy,Bodies of the Solar System,Planetary science,Space science,Outer space,Solar System,Planemos,Dwarf planets,Astronomical objects,Moons,Resonant trans-Neptunian objects,Kuiper belt objects]
---


For example, in 2005, two additional satellites were discovered in orbit of Pluto – Hydra and Nix. Discovery and Naming:  Nix was discovered in June of 2005 by the Hubble Space Telescope Pluto Companion Search Team, using discovery images that were taken on May 15th and 18th, 2005. The initials N and H (for Nix and Hydra) were also a deliberate reference to the New Horizons mission, which would be conducting a flyby of the Pluto system in less than ten years time after the announcement was made. This would be the New Horizons mission, which flew through the Pluto-Charon system on July 14th, 2015 and photographed Hydra and Nix from an approximate distance of 640,000 km (400,000 mi). Explore further Pluto's moon Hydra

<hr>

[Visit Link](http://phys.org/news/2015-09-pluto-moon-nix.html){:target="_blank" rel="noopener"}


