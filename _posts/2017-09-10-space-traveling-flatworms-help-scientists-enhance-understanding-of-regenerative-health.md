---
layout: post
title: "Space-traveling flatworms help scientists enhance understanding of regenerative health"
date: 2017-09-10
categories:
author: "Tufts University"
tags: [Planarian,Science]
---


Researchers led by the Allen Discovery Center at Tufts University sought to determine how microgravity and micro-geomagnetic fields would affect the growth and regeneration of planarian flatworms (D. japonica) and whether any changes would persist after the worms returned to earth. We want to learn more about how these forces affect anatomy, behavior and microbiology, said the paper's corresponding author, Michael Levin, Ph.D., Vannevar Bush professor of biology and director of the Allen Discovery Center at Tufts and the Tufts Center for Regenerative and Developmental Biology. These worms were used as controls for all of the experiments except mass spectroscopic analysis. However, researchers noted that because of the variation in temperatures experienced by the space and terrestrial worms, the observed difference could be due to temperature differences. Space-traveling and control group worms also differed in their reaction to light.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-06/tu-sfh060917.php){:target="_blank" rel="noopener"}


