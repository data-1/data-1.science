---
layout: post
title: "Mutations linked to genetic disorders shed light on a crucial DNA repair pathway"
date: 2016-05-17
categories:
author: Rockefeller University
tags: [DNA repair,RAD51,Gene,Fanconi anemia,Homologous recombination,Genetic recombination,Genetics,Agata Smogorzewska,DNA,Mutation,Cell (biology),Biological processes,Molecular genetics,Branches of genetics,Life sciences,Molecular biology,Biochemistry,Biotechnology,Biology,Macromolecules,Cell biology]
---


In two separate studies, the most recent described in Molecular Cell on August 6, they have identified two new genes in which mutations can produce one such rare genetic disorder, Fanconi anemia, and so revealed new insights on this critical repair pathway. The genes that code for RAD51 and UBE2T -- along with many other genes linked to Fanconi anemia in previous studies -- contribute to a repair process known as interstrand crosslink repair, which fixes a misplaced attachment between two strands of DNA. But because only one copy of the RAD51 gene was partially defective, her cells could still perform homologous recombination, but not ICL repair. In the UBE2T study, published July 7 in in Cell Reports, the team, including first author Kimberly Rickman, a biomedical fellow in Smogorzewska's lab, found that mutations in a gene for a protein named UBE2T explained the Fanconi anemia symptoms seen in another registry patient. Although we have discovered new causes for this devastating but very rare genetic disease, the implications of this work go much further.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150807220752.htm){:target="_blank" rel="noopener"}


