---
layout: post
title: "Study reveals how rivers regulate global carbon cycle"
date: 2015-05-14
categories:
author: Woods Hole Oceanographic Institution
tags: [Carbon cycle,Carbon sequestration,Atmosphere of Earth,River,Physical sciences,Applied and interdisciplinary physics,Chemistry,Environmental science,Earth sciences,Natural environment,Physical geography,Nature]
---


But Nature has its own methods for the removal and long-term storage of carbon, including the world's river systems, which transport decaying organic material and eroded rock from land to the ocean. The estimate will help modelers predict how the carbon export from global rivers may shift as Earth's climate changes. Some of that carbon—'new' carbon—is from decomposed plant and soil material that is washed into the river and then out to sea. From these river sediment flow measurements, the research team calculated amounts of particles of carbon-containing plant and rock debris that each river exported. The atmosphere is a small reservoir of carbon compared to rocks, soils, the biosphere, and the ocean, the scientists wrote in Nature.

<hr>

[Visit Link](http://phys.org/news350739650.html){:target="_blank" rel="noopener"}


