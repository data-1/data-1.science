---
layout: post
title: "Mystery Deepens: Matter and Antimatter Are Mirror Images"
date: 2016-05-24
categories:
author: Charles Q. Choi
tags: [Antimatter,Matter,Antiproton,Baryon,Proton,Baryon asymmetry,Nature,Science,Quantum field theory,Physical sciences,Theoretical physics,Particle physics,Physics]
---


Matter and antimatter appear to be perfect mirror images of each other as far as anyone can see, scientists have discovered with unprecedented precision, foiling hope of solving the mystery as to why there is far more matter than antimatter in the universe. [The 9 Biggest Unsolved Mysteries in Physics]  Checking charge parity  Theoretical physicists suspect that the extraordinary contrast between the amounts of matter and antimatter in the universe, technically known as baryon asymmetry, may be due to some difference between the properties of matter and antimatter, formally known as a charge-parity, or CP symmetry violation. To keep antimatter and matter from coming into contact, the researchers trapped protons and antiprotons in magnetic fields. Then they measured how these particles moved in a cyclical manner in those fields, a characteristic known as their cyclotron frequency, which is proportional to both the charge-to-mass ratio of those particles and the strength of the magnetic field. No known violations of the weak equivalence principle exist, and any detected violations of it could lead to a revolution in science's understanding of gravity and space-time, and how both relate to matter and energy.

<hr>

[Visit Link](http://www.livescience.com/51833-matter-and-antimatter-are-mirror-images.html){:target="_blank" rel="noopener"}


