---
layout: post
title: "Are humans the new supercomputer?"
date: 2016-04-14
categories:
author: Aarhus University
tags: [Computer,Intuition,Research,Quantum computing,Science,Physics,Algorithm,Quantum mechanics,Computing,Creativity,Branches of science,Cognitive science,Cognition,Interdisciplinary subfields,Technology,Academic discipline interactions,Concepts in metaphysics]
---


Our results are here to demonstrate that there is still a difference between the abilities of a man and a machine, explains Jacob Sherson. At the interface between quantum physics and computer games, Sherson and his research group at Aarhus University have identified one of the abilities that still makes us unique compared to a computer's enormous processing power: our skill in approaching problems heuristically and solving them intuitively. It is these intuitive insights that we discovered by analysing the Quantum Moves player solutions, explains Jacob Sherson. ? By turning science into games, anyone can do research in quantum physics. For Quantum Moves, intuitive human actions have been shown to be compatible with the best computer solutions.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/au-aht041316.php){:target="_blank" rel="noopener"}


