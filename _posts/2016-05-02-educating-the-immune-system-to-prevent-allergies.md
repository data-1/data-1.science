---
layout: post
title: "Educating the immune system to prevent allergies"
date: 2016-05-02
categories:
author: McGill University Health Centre
tags: [Allergy,Allergen,Public health,Immunology,Allergology,Health care,Epidemiology,Causes of death,Health sciences,Diseases and disorders,Health,Medicine,Clinical medicine,Medical specialties]
---


A research team at the Montreal Children's Hospital from the Research Institute of the McGill University Health Centre (RI-MUHC) is bringing them hope with a potential vaccine that nudges the immune response away from developing allergies. Our study, for the first time, offers a potential way of preventing allergies by using a molecule that redirects the immune response away from the allergic response, says lead author Dr. Christine McCusker, allergist at the Montreal Children's Hospital and researcher at the RI-MUHC. Dr. McCusker and her team from the Meakins-Christie Laboratories started to work on a specific molecule - called STAT6 - which is important in the development of allergic response. It just redirects the immune system away from the allergic response and then it will not matter if the child is exposed to pollen, cats or dogs, because the immune system will not form an aggressive allergic reaction anymore, adds Dr. McCusker. Researchers are now studying the effect of this peptide to see in what other areas this type of immune education will prevent disease, such as with food allergies.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/muhc-eti051315.php){:target="_blank" rel="noopener"}


