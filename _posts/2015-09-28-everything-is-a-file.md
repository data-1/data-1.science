---
layout: post
title: "Everything is a file"
date: 2015-09-28
categories:
author: "David Both"
tags: [Device file,Master boot record,Computer file,Disk partitioning,Computer terminal,File system,Booting,Utility software,Data,Computer data storage,Software engineering,Data management,Software development,Storage software,Computing,System software,Computer architecture,Technology,Computers,Software,Computer engineering,Operating system technology,Computer science,Computer data,Computer hardware,Information technology management]
---


It must be run as root because non-root users do not have access to the hard drive device files in the /dev directory. The dd command can be used to copy an entire partition of hard drive to a file or another hard drive as shown below. On some distributions, the login information includes the tty (Teletype) device associated with this console, but many do not. Before we actually perform this experiment, look at a listing of the tty2 and tty3 devices in /dev. Another interesting experiment is to print a file directly to the printer using the cat command.

<hr>

[Visit Link](http://opensource.com/life/15/9/everything-is-a-file){:target="_blank" rel="noopener"}


