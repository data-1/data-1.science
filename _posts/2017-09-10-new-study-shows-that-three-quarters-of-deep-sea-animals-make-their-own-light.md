---
layout: post
title: "New study shows that three quarters of deep-sea animals make their own light"
date: 2017-09-10
categories:
author: "Monterey Bay Aquarium Research Institute"
tags: [Monterey Bay Aquarium Research Institute,Bioluminescence,Oceanography]
---


In a new study in Scientific Reports, MBARI researchers Séverine Martini and Steve Haddock show that three quarters of the animals in Monterey Bay waters between the surface and 4,000 meters deep can produce their own light. You would think it would be easy to count the number of glowing (bioluminescent) animals in the ocean, just by looking at videos or photographs taken at different depths. The VARS database contains over five million observations of deep-sea animals, and has been used as a source of data for more than 360 research papers. Martini divided the observed animals into five categories:  - Definitely bioluminescent - Highly likely to be bioluminescent - Very unlikely to be bioluminescent - Definitely not bioluminescent, and - Undefined (not enough information was available to determine if an animal is bioluminescent or not). Looking through the data, Martini and Haddock were surprised to find that the proportion of glowing to non-glowing animals was pretty similar from the surface all the way down to 4,000 meters.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/mbar-nss041017.php){:target="_blank" rel="noopener"}


