---
layout: post
title: "'Cataclysmic' collision shaped Uranus' evolution"
date: 2018-07-03
categories:
author: "Durham University"
tags: [Uranus,Planet,Giant-impact hypothesis,Solar System,Formation and evolution of the Solar System,Earth,Atmosphere,Planetary science,Space science,Astronomy,Local Interstellar Cloud,Planets of the Solar System,Planemos,Science,Bodies of the Solar System,Astronomical objects,Planets,Outer space,Physical sciences]
---


Uranus was hit by a massive object roughly twice the size of Earth that caused the planet to tilt and could explain its freezing temperatures, according to new research. Astronomers at Durham University, UK, led an international team of experts to investigate how Uranus came to be tilted on its side and what consequences a giant impact would have had on the planet's evolution. The team ran the first high-resolution computer simulations of different massive collisions with the ice giant to try to work out how the planet evolved. The research confirms a previous study which said that Uranus' tilted position was caused by a collision with a massive object - most likely a young proto-planet made of rock and ice - during the formation of the solar system about 4 billion years ago. The research could also help explain the formation of Uranus' rings and moons, with the simulations suggesting the impact could jettison rock and ice into orbit around the planet.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/du-cs070218.php){:target="_blank" rel="noopener"}


