---
layout: post
title: "Nordic Seas cooled 500,000 years before global oceans"
date: 2015-10-28
categories:
author: Uni Research
tags: [Nordic Seas,Greenland ice sheet,Scandinavia,News aggregator,Sea,Water,Climate,Earth phenomena,Environmental science,Natural environment,Nature,Hydrography,Applied and interdisciplinary physics,Earth sciences,Oceanography,Physical geography,Hydrology]
---


The cooling of the Nordic Seas towards modern temperatures started in the early Pliocene, half a million years before the global oceans cooled. The cooling of the oceans toward the modern situation started from 4 million years ago, but a new study now shows that the Nordic Seas cooled 500,000 year earlier. Along east Greenland a cold water current known as the East Greenland Current flows southward and transports the major part of all exported Arctic sea ice. Thermal isolation of Greenland  Our study shows that a surface water temperature gradient was only established since 4.5 million years ago, when warm waters continued to flow along the Scandinavian coast and cool water entered the Nordic Seas along Greenland's east coast, De Schepper says. The cool surface water that arrives from 4.5 million years ago in the western Nordic Seas isolates Greenland from the warmer water in the eastern Nordic Seas.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151028084915.htm){:target="_blank" rel="noopener"}


