---
layout: post
title: "Understanding Different Classifications of Shell Commands and Their Usage in Linux"
date: 2017-03-17
categories:
author: "Aaron Kili"
tags: [Superuser,Unix filesystem,Linux,Shell (computing),Unix shell,Command-line interface,Reserved word,Bash (Unix shell),APT (software),Directory (computing),PATH (variable),Software engineering,System software,Technology,Software,Operating system technology,Software development,Computing,Information technology management,Computer architecture,Computer science,Utility software,Computer programming,Computing commands,Computers,Computer engineering,Unix]
---


Suggested Read: 5 Interesting Command Line Tips and Tricks in Linux – Part 1  One important thing to note is that the command line interface is different from the shell, it only provides a means for you to access the shell. Program Executables (File System Commands)  When you run a command, Linux searches through the directories stored in the $PATH environmental variable from left to right for the executable of that specific command. Linux Aliases  These are user defined commands, they are created using the alias shell built-in command, and contain other shell commands with some options and arguments. Suggested Read: 5 Shell Scripts for Linux Newbies to Learn Shell Programming  Save the file and thereafter, make the script executable. You can list or check Linux built-in commands using type command as shown:  $ type pwd pwd is a shell builtin $ type cd cd is a shell builtin $ type bg bg is a shell builtin $ type alias alias is a shell builtin $ type history history is a shell builtin  Learn about some Linux built-in Commands usage:  Conclusion  As a Linux user, it is always important to know the type of command you are running.

<hr>

[Visit Link](http://www.tecmint.com/understanding-different-linux-shell-commands-usage/){:target="_blank" rel="noopener"}


