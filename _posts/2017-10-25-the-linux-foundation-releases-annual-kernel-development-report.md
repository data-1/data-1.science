---
layout: post
title: "The Linux Foundation Releases Annual Kernel Development Report"
date: 2017-10-25
categories:
author: The Linux Foundation
tags: [Linux kernel,Linux,Linux Foundation,Open source,Open-source movement,System software,Linus Torvalds,Computing,Technology,Software,Computers,Computer science,Computer architecture,Operating system families,Computer engineering,Software engineering,Free software,Unix variants,Software development]
---


This year’s paper covers work completed through Linux kernel 4.13, with an emphasis on releases 4.8 to 4.13. The complete top 30 contributing organizations can be seen in the full report. The average days of development per release increased slightly to 67.66 days from 66 last year, with every release spaced either 63 or 70 days apart, providing significant predictability. Open Source Summit is a technical conference where 2,000+ developers, operators, and community leadership professionals convene to collaborate, share information and learn about the latest in open technologies, including Linux, containers, cloud computing and more. Additional Resources  Video: Greg Kroah-Hartman: Linux Kernel Development – https://www.youtube.com/watch?v=mmu0pkSI5sw  About The Linux Foundation  The Linux Foundation is the organization of choice for the world’s top developers and companies to build ecosystems that accelerate open technology development and commercial adoption.

<hr>

[Visit Link](https://www.linuxfoundation.org/press-release/linux-foundation-releases-annual-kernel-development-report/){:target="_blank" rel="noopener"}


