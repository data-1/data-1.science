---
layout: post
title: "Scientists create world’s tiniest thermometer using DNA"
date: 2016-06-27
categories:
author: ""
tags: []
---


Scientists have created the world’s tiniest thermometer that is 20,000 times smaller than a human hair, using DNA structures that can fold and unfold at specifically defined temperatures. “In recent years, biochemists also discovered that biomolecules such as proteins or RNA (a molecule similar to DNA) are employed as nanothermometers in living organisms and report temperature variation by folding or unfolding,” said Alexis Vallee-Belisle, professor at University of Montreal in Canada. “DNA is made from four different monomer molecules called nucleotides - nucleotide A binds weakly to nucleotide T, whereas nucleotide C binds strongly to nucleotide G,” said David Gareau, from the University of Montreal. “Using these simple design rules we are able to create DNA structures that fold and unfold at a specifically desired temperature,” Gareau said. These nanoscale thermometers open many exciting avenues in the emerging field of nanotechnology, and may even help us to better understand molecular biology.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/health/scientists-create-worlds-tiniest-thermometer-using-dna/article8528134.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


