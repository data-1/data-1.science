---
layout: post
title: "Birds vs. bees: Study helps explain how flowers evolved to get pollinators to specialize"
date: 2017-09-11
categories:
author: "Worcester Polytechnic Institute"
tags: [Flower,Pollinator,Bee,Nectar,Hummingbird,Bumblebee,Foraging,Pollination,Evolution]
---


- Ecologists who study flowering plants have long believed that flowers evolved with particular sets of characteristics--unique combinations of colors, shapes, and orientations, for example--as a means of attracting specific pollinators. They tested three characteristics--color, orientation, and nectar reward--in various combinations. Similar effects were observed when red coloration and dilute nectar were combined, showing that floral display and reward traits also interact to discourage bee visitation. Bumblebees, like most pollinators, are not genetically programmed to visit only particular flowers, Gegear says. During these runs each bee learned to associate every color and orientation combination with a reward.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/wpi-bvb041917.php){:target="_blank" rel="noopener"}


