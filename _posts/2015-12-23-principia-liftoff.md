---
layout: post
title: "Principia liftoff"
date: 2015-12-23
categories:
author: "$author" 
tags: [Tim Peake,Human spaceflight,Aeronauts,NASA,Aerospace,Space research,Astronauts,Space program of the United States,Space agencies,Space industry,Space-based economy,Spacecraft,Space exploration,Flight,Life in space,Outer space,Space programs,Astronautics,Spaceflight,European Space Agency,Human spaceflight programs,Space traffic management,Scientific exploration,European space programmes,Space science,Spaceflight technology,Crewed spacecraft]
---


ESA astronaut Tim Peake, NASA astronaut Tim Kopra and commander Yuri Malenchenko were launched into space 15 Decemeber 11:03 GMT from Baikonur cosmodrome, Kazakhstan. The launch marks the start of Tim Peake’s six-month Principia mission on the International Space Station running over 30 scientific experiments for ESA. Follow Tim Peake via timpeake.esa.int and follow the whole mission on ESA’s Principia blog.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2015/12/Principia_liftoff){:target="_blank" rel="noopener"}


