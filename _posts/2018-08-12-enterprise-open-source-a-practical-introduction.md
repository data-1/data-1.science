---
layout: post
title: "Enterprise Open Source: A Practical Introduction"
date: 2018-08-12
categories:
author: ""
tags: [Open source,Linux,Open-source software,Linux Foundation,Information technology management,Open content,Digital media,Computing,Software,Software development,Technology,Software engineering,Intellectual works,Open-source movement,Computer science,Free software]
---


If your company is involved in software engineering, it is very likely you already use open source software in your products or services; if so, you must have an open source strategy to ensure you are making the best use of open source software while protecting yourself from potential risks and liabilities. Learn how to accelerate your company’s open source efforts, based on the experience of hundreds of companies spanning more than two decades of professional, enterprise open source. In this 45-page ebook, we will cover:

<hr>

[Visit Link](https://www.linuxfoundation.org/open-source-management/2018/08/enterprise-open-source-practical-introduction/){:target="_blank" rel="noopener"}


