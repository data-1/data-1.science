---
layout: post
title: "For the First Time, Scientists Have Measured the Earth's Ability to Absorb Neutrinos"
date: 2018-04-26
categories:
author: ""
tags: [IceCube Neutrino Observatory,Neutrino,Science,Physics,Physical sciences,Astronomy,Nature,Space science,Particle physics,Astrophysics]
---


With this, they were able to compare their data with a simulation of neutrinos passing through the Earth. Neutrino Astronomy  While this study was enough to confirm Standard Model simulations of the Earth interacting with high-energy neutrinos, the team is anxious to take their research further. That array would have a superior energy range that researchers could use to gather even more data on how the Earth can absorb neutrinos. Additionally, an entirely new form of detection based on radio waves could prove even more effective than IceCube's optical system. Then we could triangulate the direction and speed of neutrino interaction.

<hr>

[Visit Link](https://futurism.com/first-time-scientists-measured-earths-ability-absorb-neutrinos/){:target="_blank" rel="noopener"}


