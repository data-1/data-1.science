---
layout: post
title: "ORNL neutron 'splashes' reveal signature of exotic particles"
date: 2016-04-10
categories:
author: DOE/Oak Ridge National Laboratory
tags: [Quantum spin liquid,Spin (physics),Neutron,Atom,Majorana fermion,Oak Ridge National Laboratory,Fermion,Magnetism,Quantum mechanics,Physical sciences,Science,Theoretical physics,Physical chemistry,Atomic physics,Chemistry,Condensed matter,Condensed matter physics,Nature,Particle physics,Applied and interdisciplinary physics,Physics]
---


This Kitaev quantum spin liquid supports magnetic excitations equivalent to Majorana fermions--particles that are unusual in that they are their own antiparticles. Arnab Banerjee, the study's lead author and a post-doctoral researcher at ORNL, explained that one way to observe spin liquid physics in such a material is to splash or excite the liquid using neutron scattering. The paper is published as Proximate Kitaev quantum spin liquid behaviour in a honeycomb magnet. The Spallation Neutron Source and the High Flux Isotope Reactor are DOE Office of Science User Facilities. Image credit: Genevieve Martin/ORNL  NOTE TO EDITORS: You may read other press releases from Oak Ridge National Laboratory or learn more about the lab at http://www.ornl.gov/news.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/drnl-on040716.php){:target="_blank" rel="noopener"}


