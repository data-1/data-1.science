---
layout: post
title: "Building blocks to create metamaterials"
date: 2018-07-03
categories:
author: "California Institute of Technology"
tags: [Metamaterial,Quantum mechanics,Wave,Subatomic particle,Mechanics,Applied and interdisciplinary physics,Physics,Physical sciences,Materials science,Materials,Chemistry,Electromagnetism,Technology,Science]
---


Engineers at Caltech and ETH Zürich in Switzerland have created a method to systematically design metamaterials using principles of quantum mechanics. Before our work, there was no single, systematic way to design metamaterials that control mechanical waves for different applications, she says. Each particle reacts to incoming waves in a unique way that is determined, in part, by the reaction of its neighbors. That deformation is governed not only by the geometry of that structure, but also by how the structures are connected and how the other structures around them are reacting. By tuning the design of the plates and how well-connected the plates were, the team created a perfect acoustic lens that focuses sound without loss of signal.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/ciot-bbt011718.php){:target="_blank" rel="noopener"}


