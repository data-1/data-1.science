---
layout: post
title: "HIV can develop resistance to CRISPR/Cas9"
date: 2016-04-10
categories:
author: Cell Press
tags: [Cas9,Virus,HIV,Antiviral drug,Mutation,DNA,CRISPR,Biotechnology,Molecular genetics,Medical specialties,Microbiology,Molecular biology,Nucleic acids,Life sciences,Branches of genetics,Genetics,Virology,Biology,Biochemistry]
---


Researchers who used CRISPR/Cas9 to mutate HIV-1 within cellular DNA found that while single mutations can inhibit viral replication, some also led to unexpected resistance. From here, CRISPR/Cas9 can be programmed to target a DNA sequence and cleave viral DNA. Such mutations do no harm to the virus, so these resistant viruses can still replicate, he says  The study, a collaborative effort between researchers at McGill University and the University of Montreal in Canada and the Chinese Academy of Medical Sciences and Peking Union Medical College in China serves as a cautionary tale for those who hope to apply CRISPR/Cas9 as an antiviral. CRISPR/Cas9 gives a new hope toward finding a cure, not just for HIV-1, but for many other viruses, Liang says. Cell Reports, Wang et al.: CRISPR/Cas9-derived mutations both inhibit HIV-1 replication and accelerate viral escape http://dx.doi.org/10.1016/j.celrep.2016.03.042  Cell Reports (@CellReports), published by Cell Press, is a weekly open-access journal that publishes high-quality papers across the entire life sciences spectrum.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/cp-hcd033116.php){:target="_blank" rel="noopener"}


