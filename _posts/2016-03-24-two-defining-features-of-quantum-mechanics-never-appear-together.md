---
layout: post
title: "Two defining features of quantum mechanics never appear together"
date: 2016-03-24
categories:
author: Lisa Zyga
tags: [Quantum entanglement,Quantum mechanics,Quantum nonlocality,Science,Quantum information science,Branches of science,Scientific method,Scientific theories,Theoretical physics,Physics]
---


Representation of measurements that demonstrate the contextuality-nonlocality tradeoff. Previously, physicists have theoretically shown that both of these phenomena cannot simultaneously exist in a quantum system, as they are both just different manifestations of a more fundamental concept, the assumption of realism. To show that a quantum system is nonlocal or contextual, physicists have defined inequalities that assume a system is the opposite (local or noncontextual). By performing various measurements on these photons, the researchers could violate the inequalities separately, but not at the same time. That is, to violate the locality inequality costs entanglement as a resource, while to violate the noncontextuality inequality costs contextuality as a resource.

<hr>

[Visit Link](http://phys.org/news/2016-03-features-quantum-mechanics.html){:target="_blank" rel="noopener"}


