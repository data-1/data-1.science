---
layout: post
title: "A new telescope expands Big Bear Solar Observatory's view of the Sun"
date: 2018-07-01
categories:
author: "New Jersey Institute Of Technology"
tags: [Sun,Stellar corona,Solar flare,Big Bear Solar Observatory,Solar phenomena,Solar cycle,Astrophysics,Stellar astronomy,Bodies of the Solar System,Physical phenomena,Electromagnetism,Nature,Astronomical objects known since antiquity,Solar System,Plasma physics,Physical sciences,Space plasmas,Space science,Astronomy]
---


Credit: NJIT  A solar telescope that captures images of the entire disk of the Sun, monitoring eruptions taking place simultaneously in different magnetic fields in both the photosphere and chromosphere, is now being installed beside the Goode Solar Telescope (GST) at NJIT's California-based Big Bear Solar Observatory (BBSO). Earlier this month, BBSO received a $2.3 million grant from the National Science Foundation (NSF) that will fund continuing scientific study of the Sun using the 1.6-meter GST at Big Bear, which is currently the highest resolution solar telescope in the world. It will measure the magnetic field strength and direction over the full solar disk within 15 minutes. Credit: New Jersey Institute of Technology  He added, This data improves our models of the behavior of the solar corona, particularly when flares occur. Last year, Haimin Wang, distinguished professor of physics at NJIT, and his colleagues released some of the first detailed views from the GST of the mechanisms that may trigger solar flares, colossal releases of magnetic energy in the Sun's corona that dispatch energized particles capable of penetrating Earth's atmosphere within an hour and disrupting orbiting satellites and electronic communications on the ground.

<hr>

[Visit Link](https://phys.org/news/2018-06-telescope-big-solar-observatory-view.html){:target="_blank" rel="noopener"}


