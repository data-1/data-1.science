---
layout: post
title: "The 6 most pressing environmental issues—and what you can do to help solve them"
date: 2018-06-17
categories:
author: "Joshua Marks"
tags: []
---


Continue reading below Our Featured Videos  Image via Shutterstock  CLIMATE CHANGE  While 97 percent of climate scientists agree that climate change is occurring and greenhouse gas emissions are the main cause, political will has not been strong enough so far to initiate a massive policy shift away from fossil fuels and toward sustainable forms of energy. SIGN UP  What You Can Do: Your home and transportation could be major sources of greenhouse gas emissions. Image via Shutterstock  POLLUTION  Air pollution and climate change are closely linked, as the same greenhouse gas emissions that are warming the planet are also creating smoggy conditions in major cities that endanger public health. Soil pollution threatens food security and poses health risks to the local population. The use of pesticides and fertilizers are also major factors in soil pollution  Related: Nine Chinese Cities More Polluted Than Beijing  What You Can Do: Many of the solutions to air pollution are similar to those for climate change, though it’s important to either make a concerted effort to drive less, or switch to a lower-emissions vehicle.

<hr>

[Visit Link](https://inhabitat.com/top-6-environmental-issues-for-earth-day-and-what-you-can-do-to-solve-them){:target="_blank" rel="noopener"}


