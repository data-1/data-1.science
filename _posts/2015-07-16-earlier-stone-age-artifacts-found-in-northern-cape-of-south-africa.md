---
layout: post
title: "Earlier Stone Age artifacts found in Northern Cape of South Africa"
date: 2015-07-16
categories:
author: University of Toronto 
tags: [Kathu,Archaeology,Branches of science,Periods and stages in archaeology]
---


These discoveries were made by archaeologists from the University of Cape Town (UCT), South Africa and the University of Toronto (U of T), in collaboration with the McGregor Museum in Kimberley, South Africa. The archaeologists' research on the Kathu Townlands site, one of the richest early prehistoric archaeological sites in South Africa, was published in the journal, PLOS ONE, on 24 July 2014. Steven James Walker from the Department of Archaeology at UCT, lead author of the journal paper, says: The site is amazing and it is threatened. Walker adds that the fact that such an extensive prehistoric site is located in the middle of a zone of intensive development poses a unique challenge for archaeologists and developers to find strategies to work cooperatively. Other sites in the complex include Kathu Pan 1 which has produced fossils of animals such as elephants and hippos, as well as the earliest known evidence of tools used as spears from a level dated to half a million years ago.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/uot-esa072414.php){:target="_blank" rel="noopener"}


