---
layout: post
title: "The size of a cereal box: ESA’s first satellite of 2018"
date: 2018-08-16
categories:
author: ""
tags: [CubeSat,European Space Agency,Small satellite,Rocketry,Spaceflight,Outer space,Spacecraft,Astronautics,Flight,Space vehicles,Space science,Spaceflight technology,Astronomy,Technology,Satellites,Space exploration,Space programs,Solar System,Bodies of the Solar System,Aerospace]
---


Enabling & Support The size of a cereal box: ESA’s first satellite of 2018 02/02/2018 7634 views 87 likes  ESA’s first mission of the year was launched today: GomX-4B is the Agency’s most advanced technology-tester yet, featuring a hyperspectral camera and tiny thrusters to manoeuvre thousands of kilometres from its near-twin to try out their radio link. These CubeSats are built around standard 10x10 cm units by GomSpace in Denmark. “ESA is harnessing CubeSats as a fast, cheap method of testing promising European technologies in orbit,” comments Roger Walker, heading ESA’s technology CubeSat efforts. The focus of Denmark’s GomX-4A on imaging includes monitoring Arctic territory. It carries no thrusters but the agile GomX-4B will fly behind it, allowing the pair to test their radio link across various distances up to 4500 km.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/The_size_of_a_cereal_box_ESA_s_first_satellite_of_2018){:target="_blank" rel="noopener"}


