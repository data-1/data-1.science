---
layout: post
title: "Study finds major ocean current is widening as climate warms"
date: 2018-08-14
categories:
author: "University of Miami Rosenstiel School of Marine & Atmospheric Science"
tags: [Ocean,Agulhas Current,Ocean current,Climate change,Rosenstiel School of Marine and Atmospheric Science,Boundary current,University of Miami,Climate,American Association for the Advancement of Science,Applied and interdisciplinary physics,Natural environment,Physical geography,Earth sciences,Nature,Oceanography,Hydrography,Earth phenomena]
---


MIAMI -- A new study by University of Miami (UM) Rosenstiel School of Marine and Atmospheric Science researchers found that the Indian Ocean's Agulhas Current is getting wider rather than strengthening. The findings, which have important implications for global climate change, suggest that intensifying winds in the region may be increasing the turbulence of the current, rather than increasing its flow rate. They found the Agulhas Current has broadened, not strengthened, since the early 1990s, due to more turbulence from increased eddying and meandering. ###  This paper analyzed data collected during the Agulhas Current Times-Series experiment, led by Beal and funded by the National Science Foundation. Visit the University of Miami's report on climate change http://www.climate.miami.edu.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/uomr-sfm110916.php){:target="_blank" rel="noopener"}


