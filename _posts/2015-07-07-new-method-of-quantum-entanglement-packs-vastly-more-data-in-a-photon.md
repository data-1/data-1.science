---
layout: post
title: "New method of quantum entanglement packs vastly more data in a photon"
date: 2015-07-07
categories:
author: ""   
tags: [Photon,Quantum entanglement,Physics,Science,Quantum mechanics,Applied and interdisciplinary physics,Physical sciences,Theoretical physics,Electromagnetic radiation,Optics,Atomic molecular and optical physics,Electromagnetism]
---


New method of quantum entanglement packs vastly more data in a photon    by Staff Writers    Los Angeles CA (SPX) Jul 10, 2015    A team of researchers led by UCLA electrical engineers has demonstrated a new way to harness light particles, or photons, that are connected to each other and act in unison no matter how far apart they are - a phenomenon known as quantum entanglement. In secure data transfer, photons sent over fiber optic networks can be encrypted through entanglement. With each dimension of entanglement, the amount of information carried on a photon pair is doubled, so a photon pair entangled by five dimensions can carry 32 times as much data as a pair entangled by only one. We show that an optical frequency comb can be generated at single photon level, Xie said. Co-authors on the paper are Sajan Shrestha, XinAn Xu and Junlin Liang, prior students and postdoctoral scientists at Columbia with Wong; Tian Zhong, professors Jeffrey Shapiro and Franco N.C. Wong of MIT; Yan-Xiao Gong of Southeast University in Nanjing, China; and Joshua Bienfang and Alessandro Restelli, affiliated with both the University of Maryland and the NIST.

<hr>

[Visit Link](http://www.spacedaily.com/reports/New_method_of_quantum_entanglement_packs_vastly_more_data_in_a_photon_999.html){:target="_blank" rel="noopener"}


