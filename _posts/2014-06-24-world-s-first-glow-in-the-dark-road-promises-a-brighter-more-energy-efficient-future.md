---
layout: post
title: "World’s First Glow-in-the-Dark Road Promises a Brighter, More Energy-Efficient Future"
date: 2014-06-24
categories:
author: Lucy Wang
tags: []
---


While the “Smart Highway” team irons out the kinks for the photo-luminescent roadways, Roosegaarde is pushing the boundaries of glow-in-the-dark technology in yet another invention that could replace street lights: auto luminescent trees. SIGN UP I agree to receive emails from the site. + Studio Roosegaarde  + Bioglow  Doing things for the first time keeps the world interesting. All around us people are doing things for the first time and Vodafone wants to help them get there. Share your firsts with Vodafone here!

<hr>

[Visit Link](http://inhabitat.com/worlds-first-glow-in-the-dark-road-promises-a-brighter-more-energy-efficient-future/){:target="_blank" rel="noopener"}


