---
layout: post
title: "Three-tailed Comet Q1 PanSTARRS lights up Southern skies"
date: 2016-05-14
categories:
author:  
tags: [Sky,Naked eye,Comet,Dawn,Twilight,Sunset,Privacy,Astronomy,Space science,Bodies of the Solar System,Solar System,Astronomical objects,Planetary science,Outer space,Physical sciences]
---


C/2014 Q1 peaked at about 3rd magnitude at perihelion on July 6, when it missed the Sun by just 28 million miles (45 million km). Australian observers Michael Mattiazzo and Paul Camilleri pegged it at magnitude +5.2 on July 15-16. Although it wasn't visible with the naked eye because of a bright sky, binoculars and small telescopes provided wonderful views. Embedded within this was the main dust tail about half a degree long to the east and an unusual feature at right angles to the main tail— a broad dust trail 1° long to the north. Comet C/2014 Q1 PanSTARRS is best seen from the southern hemisphere during the winter months of July and August.

<hr>

[Visit Link](http://phys.org/news/2015-07-three-tailed-comet-q1-panstarrs-southern.html){:target="_blank" rel="noopener"}


