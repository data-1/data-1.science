---
layout: post
title: "New data bolsters Higgs boson discovery"
date: 2014-06-24
categories:
author: Rice University
tags: [Higgs boson,Particle physics,Standard Model,Compact Muon Solenoid,Large Hadron Collider,Top quark,Collider,Physics,Physical sciences,Theoretical physics,Nuclear physics,Nature,Subatomic particles,Quantum field theory,Quantum mechanics,Science]
---


Further analysis of collisions in 2011 and 2012 has found evidence that the Higgs also decays into fermion particles, according to a new paper in Nature Physics to which Rice University scientists contributed. The European Organization for Nuclear Research (CERN) published research in Nature Physics this week that details evidence of the direct decay of the Higgs boson to fermions, among the particles anticipated by the Standard Model of physics. The CMS is one of two main experiments at the collider. As we find more evidence, it looks more like a Standard Model Higgs boson. Ecklund said the upgraded LHC should provide the necessary energy to produce many more top quarks and Higgs bosons.

<hr>

[Visit Link](http://phys.org/news322741906.html){:target="_blank" rel="noopener"}


