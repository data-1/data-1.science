---
layout: post
title: "'Littlest' quark-gluon plasma revealed by physicists using Large Hadron Collider"
date: 2016-06-29
categories:
author: "University of Kansas"
tags: [Quarkgluon plasma,Particle physics,High-energy nuclear physics,Theoretical physics,Physics,Physical sciences,Nuclear physics,Nature,Applied and interdisciplinary physics,Subatomic particles,Quantum mechanics,Quantum field theory,Standard Model,Quantum chromodynamics]
---


LAWRENCE -- Researchers at the University of Kansas working with an international team at the Large Hadron Collider have produced quark-gluon plasma -- a state of matter thought to have existed right at the birth of the universe -- with fewer particles than previously thought possible. Wang performed key analysis for a paper about the experiment recently published in APS Physics. Indeed, these collisions were being studied as a reference for collisions of two lead nuclei to explore the non-quark-gluon-plasma aspects of the collisions, Wang said. This is probably the first evidence that the smallest droplet of quark gluon plasma is produced in proton-lead collisions. Wang said such experiments might help scientists to better understand cosmic conditions in the instant following the Big Bang.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/uok-qp090315.php){:target="_blank" rel="noopener"}


