---
layout: post
title: "Quantifying the effects of climate change"
date: 2017-09-23
categories:
author: ""
tags: [Sea level rise,Earth,Climate,Satellite,Climatology,Global natural environment,Geography,Oceanography,Human impact on the environment,Societal collapse,Earth sciences,Physical geography,Climate variability and change,Environmental issues with fossil fuels,Global environmental issues,Earth phenomena,Environmental impact,Nature,Applied and interdisciplinary physics,Climate change,Natural environment]
---


Applications Quantifying the effects of climate change 05/06/2017 12001 views 145 likes  Last year was the hottest on record, Arctic sea ice is on the decline and sea levels continue to rise. In this context, satellites are providing us with an unbiased view of how our climate is changing and the effects it is having on our planet. Estimates show that the global sea level is rising by about 3 mm a year. But the long-term series of data needed by climate scientists –30 years or more – is significantly longer than the lifetime of satellite missions. To overcome this, ESA created the Climate Change Initiative, or CCI, which integrates datasets derived from different Earth-observing missions to produce the most comprehensive global, long-term records possible for each factor influencing Earth at large – called Essential Climate Variables.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Space_for_our_climate/Quantifying_the_effects_of_climate_change){:target="_blank" rel="noopener"}


