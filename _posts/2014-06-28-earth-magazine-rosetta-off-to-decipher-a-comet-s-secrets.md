---
layout: post
title: "EARTH Magazine: Rosetta off to decipher a comet's secrets"
date: 2014-06-28
categories:
author: American Geosciences Institute 
tags: [Rosetta (spacecraft),Discovery and exploration of the Solar System,Space research,Planetary science,Space probes,Science,Outer space,Space science,Spaceflight,Astronomy,Astronautics,Spacecraft,Solar System,Flight,Space exploration,Bodies of the Solar System,Space vehicles]
---


Alexandria, Va. — Hello World. Rosetta — the ESA spacecraft currently on a 10-year mission to orbit and land on a comet — awoke in January after a three-year hibernation, and was ready to get to work. Read all about Rosetta's mission and what scientists hope to learn about our origins in the July issue of EARTH Magazine: http://bit.ly/TpKFcP. For more stories about the science of our planet, check out EARTH Magazine online or subscribe at http://www.earthmagazine.org. Published by the American Geosciences Institute, EARTH is your source for the science behind the headlines.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/agi-emr062714.php){:target="_blank" rel="noopener"}


