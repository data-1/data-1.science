---
layout: post
title: "Bats bolster brain hypothesis, maybe technology, too"
date: 2015-10-29
categories:
author: Brown University
tags: [Animal echolocation,Perception,Bat,Cognitive science,Neuroscience,Cognition]
---


These harmonic pairs reflect off of objects and back to the bat's ears, triggering a response from neurons in its brain. Objects that reflect these harmonic pairs back in perfect synchrony are the ones that stand out clearly for the bat. The real question is how a target object would stand out. The temporal binding hypothesis, predicts that the distant or peripheral objects with these out-of-synch signals will be perceived as the background while front-and-center objects that reflect back both harmonics with equal strength will rise above their desynchronized competitors. Simmons works with the U.S. Navy on applications of bat echolocation to navigation technology.

<hr>

[Visit Link](http://phys.org/news327315726.html){:target="_blank" rel="noopener"}


