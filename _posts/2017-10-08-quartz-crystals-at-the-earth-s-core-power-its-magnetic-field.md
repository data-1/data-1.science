---
layout: post
title: "'Quartz' crystals at the Earth's core power its magnetic field"
date: 2017-10-08
categories:
author: "Tokyo Institute of Technology"
tags: [Planetary core,Silicon,Structure of Earth,Earth,Crystal,Alloy,Silicon dioxide,Iron,Earths magnetic field,Diamond,Oxygen,Energy,Quartz,Magnetism,Chemistry,Nature,Materials,Applied and interdisciplinary physics,Physical sciences]
---


In the past, most research on iron alloys in the core has focused only on the iron and a single alloy, says Hirose. The researchers were surprised to find that when they examined the samples in an electron microscope, the small amounts of silicon and oxygen in the starting sample had combined together to form silicon dioxide crystals (Fig. We were excited because our calculations showed that crystallization of silicon dioxide crystals from the core could provide an immense new energy source for powering the Earth's magnetic field. Crystallization changes the composition of the core by removing dissolved silicon and oxygen gradually over time. Even if you have silicon present, you can't make silicon dioxide crystals without also having some oxygen available says ELSI scientist George Helffrich, who modeled the crystallization process for this study.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/tiot-ca022017.php){:target="_blank" rel="noopener"}


