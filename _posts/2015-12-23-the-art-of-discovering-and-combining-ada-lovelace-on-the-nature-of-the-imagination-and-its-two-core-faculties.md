---
layout: post
title: "The Art of Discovering and Combining: Ada Lovelace on the Nature of the Imagination and Its Two Core Faculties"
date: 2015-12-23
categories:
author: Maria Popova
tags: []
---


The Art of Discovering and Combining: Ada Lovelace on the Nature of the Imagination and Its Two Core Faculties  The human imagination is the seedbed of everything we know to be beautiful and true — it created the Mona Lisa’s smile and recreates its mystery anew with each viewing; it envisioned the existence of a strange particle and sparked the myriad scientific breakthroughs that made its discovery possible half a century later in the Higgs boson; it allows us to perform the psychoemotional acrobatics at the heart of compassion as we imagine ourselves in another’s shoes. In early January of 1841, two years before she applied her formidable imagination to writing the first paper on computer science and forever changing the course of technology, Lovelace considered the nature of the imagination and its two core faculties, combining and discovering, in a magnificent letter found in Ada, the Enchantress of Numbers: A Selection from the Letters of Lord Byron’s Daughter and Her Description of the First Computer (public library) — the same volume that gave us Lovelace on science and religion. First: it is the Combining Faculty. It is that which penetrates into the unseen worlds around us, the worlds of Science. But to use & apply that language we must be able fully to appreciate, to feel, to seize, the unseen, the unconscious.

<hr>

[Visit Link](https://www.brainpickings.org/2015/12/10/ada-lovelace-imagination/){:target="_blank" rel="noopener"}


