---
layout: post
title: "Student project leads to founding of startup DIANA Biotechnologies"
date: 2018-07-27
categories:
author: "Institute of Organic Chemistry and Biochemistry of the Czech Academy of Sciences (IOCB Prague)"
tags: [Drug discovery,Chemistry,Biochemistry,Assay,Research,Enzyme,Science,Technology,Organic chemistry,American Association for the Advancement of Science,Biotechnology,Drug development,Enzyme inhibitor,Infection,Antibody,Biology,Life sciences,Physical sciences,Branches of science,Medicine]
---


A new startup, DIANA Biotechnologies, has been founded at the Institute of Organic Chemistry and Biochemistry of the Czech Academy of Sciences (IOCB Prague) to develop a unique technology created by postgraduate student Václav Navrátil with the potential to significantly improve disease diagnostics and accelerate the development of new drugs. The new company will develop a technology known as DIANA, or DNA-linked Inhibitor Antibody Assay, which was developed by Navrátil and colleagues in Jan Konvalinka's laboratory at IOCB Prague. Our technology allows for the detection of enzymes in blood with sensitivity up to several orders higher than that of contemporary methods, and that's why we see a range of new and important applications for it, explains Navrátil, CEO of the new startup. DIANA Biotechnologies isn't our first joint project with the IOCB Prague. Scientific article on the DIANA technology: Václav Navrátil, Ji?í Schimer, Jan Tykvart, Tomáš Knedlík, Viktor Vik, Pavel Majer, Jan Konvalinka, Pavel Šácha; DNA-linked Inhibitor Antibody Assay (DIANA) for sensitive and selective enzyme detection and inhibitor screening, Nucleic Acids Research, Volume 45, Issue 2, 25 January 2017, Pages e10, https://doi.org/10.1093/nar/gkw853.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/iooc-spl072618.php){:target="_blank" rel="noopener"}


