---
layout: post
title: "Taking plants off-planet – how do they grow in zero gravity?"
date: 2016-05-16
categories:
author: Anna-Lisa Paul And Robert Ferl, The Conversation
tags: [Plant,International Space Station,Spaceflight,Root,Astronaut,NASA,Biology,Protein,Weightlessness,Micro-g environment,Experiment]
---


Already they've given us some surprises about growing in zero gravity – and shaken up some of our thinking about how plants grow on Earth. A typical experiment begins on Earth in our lab with the planting of dormant Arabidopsis seeds in Petri plates containing a nutrient gel. Credit: Anna-Lisa Paul, CC BY  We found a number of genes involved in making and remodeling cell walls are expressed differently in space-grown plants. These patterns of genes and proteins tell a story – in microgravity, plants respond by loosening their cell walls, along with creating new ways to sense their environment. Explore further What happens to plant growth when you remove gravity?

<hr>

[Visit Link](http://phys.org/news/2015-08-off-planet-gravity.html){:target="_blank" rel="noopener"}


