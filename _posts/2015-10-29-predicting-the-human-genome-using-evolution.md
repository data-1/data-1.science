---
layout: post
title: "Predicting the human genome using evolution"
date: 2015-10-29
categories:
author: SMBE Journals (Molecular Biology and Evolution and Genome Biology and Evolution) 
tags: [Evolution,Human genome,Genome,Mutation,Species,Human,Genome project,Biology,Life sciences,Genetics,Biotechnology,Evolutionary biology,Biological evolution,Bioinformatics,Molecular biology,Biochemistry,Branches of genetics,Biological engineering]
---


The alternative, which is the basis of our approach, is to compile all genome data from other species and predict what the human sequence reference should be. By observing evolution's greatest hits (and misses) and the history of the major themes and patterns of genome conservation (and divergence) across many species, Kumar's approach predicts probable mutations that will be found among people and the fate of human variation. They applied their new method on all protein-coding genes in the human genome (more than 10 million positions). Consistent with the knowledge that most mutations are harmful, they found very low EPs (lower than 0.05) for a vast majority of potential mutations (94.4 percent). Their eVar was also compared against available human sequence data from the 1,000 Genomes Project to look at benign and disease mutations, and found that the use of EPs could correctly diagnose them.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/mbae-pth102815.php){:target="_blank" rel="noopener"}


