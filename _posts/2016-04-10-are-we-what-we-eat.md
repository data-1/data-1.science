---
layout: post
title: "Are we what we eat?"
date: 2016-04-10
categories:
author: SMBE Journals (Molecular Biology and Evolution and Genome Biology and Evolution)
tags: [Mutation,Omega-3 fatty acid,Gene,Arachidonic acid,Molecular biology,Life sciences,Biotechnology,Biochemistry,Biology,Genetics]
---


In a new evolutionary proof of the old adage, 'we are what we eat', Cornell University scientists have found tantalizing evidence that a vegetarian diet has led to a mutation that -- if they stray from a balanced omega-6 to omega-3 diet -- may make people more susceptible to inflammation, and by association, increased risk of heart disease and colon cancer. Tom Brenna, Kumar Kothapalli, and Alon Keinan provides the first evolutionary detective work that traces a higher frequency of a particular mutation to a primarily vegetarian population from Pune, India (about 70 percent), when compared to a traditional meat-eating American population, made up of mostly Kansans (less than 20 percent). The mutation, called rs66698963 and found in the FADS2 gene, is an insertion or deletion of a sequence of DNA that regulates the expression of two genes, FADS1 and FADS2. The insertion mutation may be favored in populations subsisting primarily on vegetarian diets and possibly populations having limited access to diets rich in polyunsaturated fats, especially fatty fish. This is the most unique scenario of local adaptation that I had the pleasure of helping uncover, says Alon Keinan, a population geneticist who led the evolutionary study.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/mbae-aww032416.php){:target="_blank" rel="noopener"}


