---
layout: post
title: "New report examines implications of growing gap in life span by income for entitlement programs"
date: 2016-07-04
categories:
author: "National Academy of Sciences"
tags: [Cost of living,Social Security (United States),Life expectancy,Welfare,Social programs in the United States,Medicaid,Retirement,News aggregator,Economy,Politics]
---


The projections for women show a similar pattern, in that life expectancy gains have been larger for higher earners than lower earners. The simulation found that for men born in 1930, lifetime entitlement benefits received after age 50 are roughly similar across income groups. For men born in 1960, however, high earners are projected to receive markedly more -- $132,000 more -- in lifetime benefits from entitlement programs than is projected for men in the bottom earnings category. Increasing the earliest eligibility age for Social Security from 62 to 64 would not generate significant savings for the Social Security system and would slightly widen the gap in benefits received between high earners and low earners. Increasing the eligibility age for Medicare from 65 to 67 would result in a slightly larger decline in net benefits for the lowest earners than for the highest earners -- thus slightly increasing the gap in benefits.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150917141418.htm){:target="_blank" rel="noopener"}


