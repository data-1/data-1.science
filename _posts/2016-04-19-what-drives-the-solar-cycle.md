---
layout: post
title: "What drives the solar cycle?"
date: 2016-04-19
categories:
author: David Dickinson
tags: [Sun,Sunspot,Solar cycle,Star,Exoplanet,Jupiter,Astronomical objects,Space science,Astronomy,Stellar astronomy,Solar System,Physical sciences,Bodies of the Solar System,Outer space,Astronomical objects known since antiquity,Nature,Space plasmas,Astrophysics]
---


This cycle is actually is 22 years in length (that's 11 years times two), as the sun flips polarity each time. We're currently in the midst of solar cycle #24, and the measurement of solar cycles dates all the way back to 1755. Another key mystery is why the current solar cycle is so weak… it has even been proposed that solar cycle 25 and 26 might be absent all together. Credit: NASA/Marshall Spaceflight Center  Another major mystery is why the sun has this 22/11 year cycle of activity in the first place. Credit: Carl Smith/Wikimedia Commons  There are ideas out there that Jupiter drives the solar cycle.

<hr>

[Visit Link](http://phys.org/news346920501.html){:target="_blank" rel="noopener"}


