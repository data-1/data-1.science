---
layout: post
title: "Whole-genome sequencing of endangered mountain gorillas"
date: 2016-04-22
categories:
author: Wellcome Trust Sanger Institute
tags: [Gorilla,Mountain gorilla,Inbreeding,Genetics,Conservation biology,Whole genome sequencing,Biology]
---


Three years on from sequencing the gorilla reference genome, we can now compare the genomes of all gorilla populations, including the critically endangered mountain gorilla, and begin to understand their similarities and differences, and the genetic impact of inbreeding. Since then, conservation efforts led by the Rwanda Development Board and conservation organizations like the Gorilla Doctors (a partnership between the non-profit Mountain Gorilla Veterinary Project and the UC Davis Wildlife Health Center), and supported by tourists keen to see the gorillas made famous by late primatologist Dian Fossey, have bolstered numbers to approximately 480 among the Virunga population. This new understanding of genetic diversity and demographic history among gorilla populations provides us with valuable insight into how apes and humans, their closely related cousins, adapt genetically to living in small populations, says Dr Aylwyn Scally, corresponding author from the Department of Genetics at the University of Cambridge. By analysing the variations in each genome, researchers also discovered that mountain gorillas have survived in small numbers for thousands of years. We worried that the dramatic decline in the 1980s would be catastrophic for mountain gorillas in the long term, but our genetic analyses suggest that gorillas have been coping with small population sizes for thousands of years, says Dr Yali Xue, first author from the Sanger Institute.

<hr>

[Visit Link](http://phys.org/news347801465.html){:target="_blank" rel="noopener"}


