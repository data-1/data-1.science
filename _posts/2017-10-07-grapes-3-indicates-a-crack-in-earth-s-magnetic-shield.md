---
layout: post
title: "GRAPES-3 indicates a crack in Earth's magnetic shield"
date: 2017-10-07
categories:
author: "Tata Institute of Fundamental Research"
tags: [Cosmic ray,Geomagnetic storm,Magnetosphere,Earth,Science,Astrophysics,Electromagnetism,Physics,Nature,Physical sciences,Astronomy,Space science]
---


The GRAPES-3 muon telescope located at TIFR's Cosmic Ray Laboratory in Ooty recorded a burst of galactic cosmic rays of about 20 GeV, on 22 June 2015 lasting for two hours. The burst occurred when a giant cloud of plasma ejected from the solar corona, and moving with a speed of about 2.5 million kilometers per hour struck our planet, causing a severe compression of Earth's magnetosphere from 11 to 4 times the radius of Earth. It triggered a severe geomagnetic storm that generated aurora borealis, and radio signal blackouts in many high latitude countries. Earth's magnetosphere extends over a radius of a million kilometers, which acts as the first line of defence, shielding us from the continuous flow of solar and galactic cosmic rays, thus protecting life on our planet from these high intensity energetic radiations. Earth's magnetic field bent these particles about 180 degree, from the day-side to the night-side of the Earth where it was detected as a burst by the GRAPES-3 muon telescope around mid-night on 22 June 2015.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/tiof-gia110216.php){:target="_blank" rel="noopener"}


