---
layout: post
title: "JPL scientists predict future space probes will have artificial intelligence to operate autonomously"
date: 2017-09-15
categories:
author: "Bob Yirka"
tags: [Jet Propulsion Laboratory,Chemistry and Camera complex,Science,Space science,Outer space,Technology,Spaceflight]
---


Steve Chien and Kiri Wagstaff suggest that future space probes will be given enough intelligence to carry out much of their mission without prompts from people back on Earth. Doing so, they suggest, will require more probes, which means they will have to be a lot smarter—in many situations, they may have to carry out their entire mission without intervention from humans back on Earth. Explore further NASA develops AI for future exploration of extraterrestrial subsurface oceans  More information: Robotic space exploration agents, Science Robotics (2017). The AEGIS (Autonomous Exploration for Gathering Increased Science) autonomous targeting system has been in routine use on NASA's Curiosity Mars rover since May 2016, selecting targets for the ChemCam remote geochemical spectrometer instrument. AEGIS operates in two modes; in autonomous target selection, it identifies geological targets in images from the rover's navigation cameras, choosing for itself targets that match the parameters specified by mission scientists the most, and immediately measures them with ChemCam, without Earth in the loop.

<hr>

[Visit Link](https://phys.org/news/2017-06-jpl-scientists-future-space-probes.html){:target="_blank" rel="noopener"}


