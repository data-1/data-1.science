---
layout: post
title: "10 tips for better documentation"
date: 2015-08-07
categories:
author: "Nigel Babu"
tags: [Documentation,Wiki,Markup language,Translation,Computer programming,HTML,Intellectual works,Digital media,Computer science,Communication,Software engineering,Software,Software development,Computing,Technology]
---


Pick a toolchain that leads from input to final docs consistently  The key is that the tool needs to produce the output automatically and consistently. Always have translations in mind  If you don't need translations now, you still might in the future. If English is the language in which you write documentation, have every other translation based off English so keeping up with changes in documentation is easy. It's useful for your dev team to define programming language-specific style guides to keep things consistent. There must be a process through which the documentation team can know of new features that need documentation.

<hr>

[Visit Link](http://opensource.com/business/15/7/10-tips-better-documentation){:target="_blank" rel="noopener"}


