---
layout: post
title: "How will people interact with technology in the future?"
date: 2016-05-10
categories:
author: University of Bristol
tags: [Wearable computer,Electromyography,Technology,Computing,Branches of science,Computer science]
---


A team of researchers led by Professor Mike Fraser and Dr Anne Roudaut from Bristol University's Bristol Interaction Group (BIG) group, will present six papers at the international conference. Other devices that people may have with them, such as a smartwatch or camera, may have sufficient battery to support this emergency task. This paper shows that combining EMG and pressure data sensed only at the wrist can support accurate classification of hand gestures. However, nowadays significant environmental impact comes from the infrastructure which provides services the device enables. A workshop with researchers who are developing smart health technologies and a focus group with end users were run, where the focus was on generating a shared language.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/uob-hwp050916.php){:target="_blank" rel="noopener"}


