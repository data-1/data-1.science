---
layout: post
title: "Will rising carbon dioxide levels really boost plant growth?"
date: 2018-04-22
categories:
author: "Stuart Thompson, The Conversation"
tags: [Carbon dioxide,Plant,C4 carbon fixation,Photosynthesis,RuBisCO,Carbon dioxide in Earths atmosphere,Climate change,Biomass,Grassland,Natural environment,Earth sciences,Nature,Physical geography,Chemistry]
---


But some groups opposed to limiting our emissions claim that higher levels of carbon dioxide (CO₂) will boost plants' photosynthesis and so increase food production. This wasn't a problem when RuBisCO first evolved. Meanwhile, the RuBisCO in C4 plants already gets enough CO₂ and so increases should have little effect on them. These results are from grasses that survive and continue to grow year on year. We can't expect that our food security problems will be solved by C4 crop yields increasing in response to CO₂ as they did in the experiment.

<hr>

[Visit Link](https://phys.org/news/2018-04-carbon-dioxide-boost-plantgrowth.html){:target="_blank" rel="noopener"}


