---
layout: post
title: "New research replicates a folding human brain in 3-D"
date: 2016-02-05
categories:
author: Harvard John A. Paulson School of Engineering and Applied Sciences
tags: [Cerebral cortex,Gyrification,Brain,Neuroscience]
---


This simple evolutionary innovation, with iterations and variations, allows for a large cortex to be packed into a small volume, and is likely the dominant cause behind brain folding, known as gyrification, said Mahadevan, who is also a core faculty member of the Wyss Institute for Biologically Inspired Engineering, and member of the Kavli Institute for Bionano Science and Technology, at Harvard University. Within minutes of being immersed in liquid solvent, the resulting compression led to the formation of folds similar in size and shape to real brains. The geometry of the brain is really important because it serves to orient the folds in certain directions, said Chung. The largest folds seen in the model gel brain are similar in shape, size and orientation to what is seen in the fetal brain, and can be replicated in multiple gel experiments. Brains are not exactly the same from one human to another, but we should all have the same major folds in order to be healthy, said Chung.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/hjap-nrr012716.php){:target="_blank" rel="noopener"}


