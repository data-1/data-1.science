---
layout: post
title: "Chile's salt flat"
date: 2016-05-27
categories:
author:  
tags: [Chili pepper,Earth sciences]
---


The region pictured lies around 200km east of the Chilean city of Antofagasta on the Pacific coast (not pictured), and is virtually devoid of vegetation. At the top of the image we can see part of Chile’s largest salt flat, the Salar de Atacama. In one of them, extracted salt brines have unrivalled concentration levels of potassium and lithium. The multispectral instrument on Sentinel-2 uses parts of the infrared spectrum to analyse mineral composition where vegetation is sporadic. The satellite is the first in the twin satellite Sentinel-2 mission for Europe’s Copernicus programme, and carries a wide-swath high-resolution multispectral imager with 13 spectral bands, for a new angle on our land and vegetation.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2016/05/Chile_s_salt_flat){:target="_blank" rel="noopener"}


