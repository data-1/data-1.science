---
layout: post
title: "Building a Business on Open Source"
date: 2018-06-28
categories:
author: "John Mark Walker, About The Author"
tags: [Open source,Supply chain,Supply chain management,OpenShift,Open-source software,Proprietary software,Red Hat Enterprise Linux,Software,Free software,OpenStack,Open-core model,Application software,Free software movement,Kubernetes,Innovation,Patch (computing),Venture capital,Startup company,Price,Business,Software engineering,Computing,Technology,Software development]
---


WHAT VC INVESTORS WANT  Many companies have adopted a particular model around creating and selling open source software: create an open source platform or base and sell proprietary software that integrates with the underlying open source platform. For one thing, the Open Core approach has never resulted in a successful product. Creating a Product  What is the value of an open source platform? Indeed, how does someone use an open source platform? Making open source products is no different.

<hr>

[Visit Link](https://www.linuxfoundation.org/open-source-management/building-a-business-on-open-source/){:target="_blank" rel="noopener"}


