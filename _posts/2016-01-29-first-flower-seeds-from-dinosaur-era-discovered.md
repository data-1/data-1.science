---
layout: post
title: "First Flower Seeds from Dinosaur Era Discovered"
date: 2016-01-29
categories:
author: Mindy Weisberger
tags: [Flowering plant,Seed,Plant,Dormancy,Embryo,Flower,Cretaceous,CT scan,Fossil,Nature]
---


These puny pips offer a glimpse into the biology powering the ancient predecessors of all modern flowers. [Photos: Ancient Flowering Plant May Have Lived with Dinosaurs]  Around half of the fossil seeds they examined contained preserved cell structures within their seed coats, and about 50 seeds held partial or complete embryos. Once they had 2D images of the embryos, they used software to model the embryos' shapes in 3D, finding that their size and shape varied between seeds. (Image credit: Else Marie Friis)  These observations give us new insights into the early part of the life cycle of early angiosperms, which is important for understanding the ecology of flowering plants during the emergence and dramatic radiation through the early Cretaceous, Friis said in a video statement (opens in new tab). The embryos were so tiny — less than one-fourth of a millimeter — that they would need to grow more inside the seed before they could germinate.

<hr>

[Visit Link](http://www.livescience.com/53309-cretaceous-flower-seeds-found.html){:target="_blank" rel="noopener"}


