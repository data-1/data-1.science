---
layout: post
title: "The Planet Saturn"
date: 2016-05-16
categories:
author: Matt Williams
tags: [Moons of Saturn,Saturn,Rings of Saturn,Titan (moon),CassiniHuygens,Natural satellite,Enceladus,Planet,Jupiter,Planetary core,Rhea (moon),Dione (moon),Ring system,Solar System,Tethys (moon),Atmosphere,Sun,Gas giants,Astronomy,Outer planets,Planetary science,Space science,Astronomical objects known since antiquity,Bodies of the Solar System,Moons,Planets of the Solar System,Physical sciences,Planets,Astronomical objects,Outer space]
---


Image credit: NASA/JPL-Caltech/SSI  Atmosphere:  The outer atmosphere of Saturn contains 96.3% molecular hydrogen and 3.25% helium by volume. Such storms had not been observed on any planet other than Earth – even on Jupiter. The Inner Large Moons, which orbit within the E Ring (see below), includes the larger satellites Mimas, Enceladus, Tethys, and Dione. At 1066 km in diameter, Tethys is the second-largest of Saturn's inner moons and the 16th-largest moon in the Solar System. Saturn’s rings.

<hr>

[Visit Link](http://phys.org/news/2015-08-planet-saturn.html){:target="_blank" rel="noopener"}


