---
layout: post
title: "Moment of impact: A journey into the Chicxulub Crater"
date: 2017-10-24
categories:
author: Geological Society of America
tags: [Chicxulub crater,Impact crater,Earth sciences,Geology,Planetary science,Physical geography,Nature,Physical sciences]
---


The expedition targeted Chicxulub crater's peak ring and overlying rock sequences. Highlights of IODP-ICDP Expedition 364 are featured in the October 2017 issue of GSA Today, and on Tuesday, 24 October, an entire keynote session at the GSA Annual Meeting will be devoted to the expedition. Release of gas from the impact is connected to cooling of over 20 degrees C, with sub-freezing temperatures for three years (8:45 a.m.). Drilling confirmed that the peak ring at Chicxulub consists of granite, brought up from mid-crustal depths. Headquartered in Boulder, Colorado, GSA encourages cooperative research among earth, life, planetary, and social scientists, fosters public dialogue on geoscience issues, and supports all levels of earth science education.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/gsoa-moi102317.php){:target="_blank" rel="noopener"}


