---
layout: post
title: "Earth organisms survive under low-pressure Martian conditions"
date: 2016-05-07
categories:
author: University of Arkansas, Fayetteville
tags: [Methanogen,Methane,Mars,Archaea,Nature,Physical sciences,Chemistry]
---


Methanogens, microorganisms in the domain Archaea, use hydrogen as their energy source and carbon dioxide as their carbon source, to metabolize and produce methane, also known as natural gas. They don't require organic nutrients, either, and are non-photosynthetic, indicating they could exist in sub-surface environments and therefore are ideal candidates for life on Mars. These organisms are ideal candidates for life on Mars, Mickol said. This work represents a stepping-stone toward determining if methanogens can exist on Mars. Mickol, who has previously found that two species of methanogens survived Martian freeze-thaw cycles, conducted both studies with Timothy Kral, professor of biological sciences in the Arkansas Center for Space and Planetary Sciences and lead scientist on the project.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150602125843.htm){:target="_blank" rel="noopener"}


