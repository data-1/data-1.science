---
layout: post
title: "New gene map reveals cancer's Achilles heel"
date: 2015-12-21
categories:
author: University of Toronto
tags: [Cancer,Gene,Health,Medical specialties,Health sciences,Biochemistry,Medicine,Clinical medicine,Biology,Life sciences,Biotechnology,Genetics,Molecular biology]
---


Scientists have mapped out the genes that keep our cells alive, creating a long-awaited foothold for understanding how our genome works and which genes are crucial in disease like cancer. A team of Toronto researchers, led by Professor Jason Moffat from the University of Toronto's Donnelly Centre, with a contribution from Stephane Angers from the Faculty of Pharmacy, have switched off, one by one, almost 18,000 genes -- 90 per cent of the entire human genome -- to find the genes that are essential for cell survival. The data, published in Cell on November 25, revealed a core set of more than 1,500 essential genes. By turning genes off in five different cancer cell lines, including brain, retinal, ovarian, and two kinds of colorectal cancer cells, the team uncovered that each tumour relies on a unique set of genes that can be targeted by specific drugs. To do this, scientists realized they would have to switch genes off, one by one across the entire genome to determine what processes go wrong in the cells.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-11/uot-ngm112515.php){:target="_blank" rel="noopener"}


