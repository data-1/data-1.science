---
layout: post
title: "Israeli solar power plant to generate electricity around the clock"
date: 2015-07-04
categories:
author: Joshua Marks
tags: []
---


Israeli alternative energy company Brenmiller Energy has solved one of the biggest issues with solar technology — how to generate electricity when the sun sets. Biomass will be used as a backup during the four hours when the solar power system is not generating electricity. Continue reading below Our Featured Videos  “Solar power stations integrating storage and backed up by biomass are the best solution for producing electricity in Israel,” said Brenmiller Energy CEO Avi Brenmiller. “Biomass alone cannot meet electricity demand but combining it with solar energy and storage represents the cheapest and cleanest alternative. The firm said the project will create about 150 jobs in the area.

<hr>

[Visit Link](http://inhabitat.com/israeli-solar-power-plant-to-generate-electricity-around-the-clock/){:target="_blank" rel="noopener"}


