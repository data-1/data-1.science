---
layout: post
title: "NASA's Lunar mission captures solar eclipse as seen from the moon"
date: 2017-09-24
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Lunar Reconnaissance Orbiter,Moon,Goddard Space Flight Center,NASA,Solar eclipse,Eclipse,Spaceflight,Astronomical objects known since antiquity,Space science,Space exploration,Space program of the United States,Solar System,Bodies of the Solar System,Spacecraft,Flight,Astronautics,Astronomy,Outer space,Space vehicles,Planetary science]
---


During the total solar eclipse on Aug. 21, NASA's Lunar Reconnaissance Orbiter, or LRO, captured an image of the Moon's shadow over a large region of the United States, centered just north of Nashville, Tennessee. The spacecraft's Narrow Angle Camera began scanning Earth at 2:25:30 p.m. EDT (18:25:30 UTC) and completed the image 18 seconds later. The Narrow Angle Camera is part of the Lunar Reconnaissance Orbiter Camera system. The Narrow Angle Camera builds up an image line by line rather than the more typical instantaneous framing that occurs with digital or cell-phone cameras. While the thrill of the total eclipse was in experiencing the shadow of the Moon sweep across us on Earth, on the Moon this was just another day.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-08/nsfc-nlm082917.php){:target="_blank" rel="noopener"}


