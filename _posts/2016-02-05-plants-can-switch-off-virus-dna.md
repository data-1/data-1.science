---
layout: post
title: "Plants can 'switch off' virus DNA"
date: 2016-02-05
categories:
author: Wageningen University
tags: [Virus,RNA interference,DNA,Tomato yellow leaf curl virus,Gene,Cucumber mosaic virus,Genetics,Antiviral drug,Plant virus,Life sciences,Biology,Medical specialties,Molecular biology,Biotechnology,Biochemistry,Virology,Microbiology]
---


Therefore antiviral RNAi defence to these viruses has to happen somewhat different. TYLCV is one of the most economically important plant viruses in the world; for this virus a number of resistance genes (Ty-1 to Ty-6) are available to commercial plant breeders. Their recent publication in the journal PNAS shows that although Ty-1 resistance depends on RNAi, instead of the genetic material being chopped up, it is being 'blocked' by methylation of the virus DNA. The latter ensures that the defence mechanisms in plants are activated and provide 'cross protection' against more harmful, related viruses. To their great surprise, the Wageningen researchers discovered that infection with CMV, a virus that contains RNA as genetic material and that, as a result, is not affected by the Ty-1 resistance mechanism, actually compromised resistance to the TYLCV virus.

<hr>

[Visit Link](http://phys.org/news327915896.html){:target="_blank" rel="noopener"}


