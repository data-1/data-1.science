---
layout: post
title: "Magnonic interferometer paves way toward energy-efficient information processing devices"
date: 2018-07-01
categories:
author: "Lisa Zyga"
tags: [Magnon,Interferometry,Materials science,Electromagnetism,Technology,Electrical engineering,Physics,Electricity,Applied and interdisciplinary physics]
---


Although magnon signals have discrete phases that normally cannot be changed continuously, the magnonic interferometer can generate a continuous change of the magnon signal. Controlling magnons, however, requires the ability to continuously change the magnon signal, which has been challenging. In the new paper, the researchers achieve this by fabricating a waveguide made of artificial magnonic crystals composed of the magnetic insulator yttrium-iron garnet, which is patterned with triangular holes. Manipulating the beam in this way, the researchers could achieve a continuous change of the magnonc signal at a detector located at the end of one of the beam paths. The researchers expect that, in the future, the interferometer's ability to control magnonic signals in this way could lead to the design of magnonic information processing devices that can avoid the losses that plague conventional electronic devices.

<hr>

[Visit Link](https://phys.org/news/2018-05-magnonic-interferometer-paves-energy-efficient-devices.html){:target="_blank" rel="noopener"}


