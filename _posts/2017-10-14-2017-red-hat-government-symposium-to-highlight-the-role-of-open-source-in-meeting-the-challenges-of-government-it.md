---
layout: post
title: "2017 Red Hat Government Symposium to Highlight the Role of Open Source in Meeting the Challenges of Government IT"
date: 2017-10-14
categories:
author: ""
tags: [Red Hat,Forward-looking statement,Cloud computing,Intellectual property,Risk,Open source,Technology,Computing]
---


This year’s Red Hat Government Symposium is expected to bring together more than 650 government technology and industry professionals to discuss how open source collaboration can help government agencies get the most out of emerging technologies and priorities, including modernization, accelerating application deployment and implementing DevOps and containers in government agencies. As a result, agencies are looking to open source technologies to help deliver the solutions that will not only meet today’s needs, but will better prepare for the future. This year marks the ninth Red Hat Government Symposium and will celebrate the power of open source to enable choice and greater security for every level of government. Supporting Quote  Paul Smith, senior vice president and general manager, U.S. Public Sector, Red Hat  “Expectations and demands are increasing for greater IT efficiency at every level of government. As a result, agencies are looking to open source technologies to help deliver the solutions that will not only meet today’s needs but will better prepare for the future.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/2017-red-hat-government-symposium-highlight-role-open-source-meeting-challenges-government-it){:target="_blank" rel="noopener"}


