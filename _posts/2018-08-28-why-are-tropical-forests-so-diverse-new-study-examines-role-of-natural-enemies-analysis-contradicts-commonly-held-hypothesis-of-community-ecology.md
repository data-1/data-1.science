---
layout: post
title: "Why are tropical forests so diverse? New study examines role of 'natural enemies': Analysis contradicts commonly held hypothesis of community ecology"
date: 2018-08-28
categories:
author: "Yale School of Forestry & Environmental Studies"
tags: [JanzenConnell hypothesis,Ecology,Natural environment,Biogeochemistry,Nature]
---


A new study by Yale researchers finds that this phenomenon, known as the Janzen-Connell hypothesis, does indeed promote coexistence of tropical species -- except when it doesn't. The study was co-authored by Liza Comita, an assistant professor of tropical forest ecology at F&ES. According to their findings, communities are less stable if rare plant species are more susceptible to natural enemies, particularly when dispersal is low. In a previous analysis of long-term data on seedling survival in the Barro Colorado Island forest, Comita and colleagues reported in the journal Science that rare species tend to be more susceptible to conspecific negative density dependence compared to more common species in the community. Thus, contrary to the commonly held assumption that conspecific negative density dependence always promotes tree diversity, our new theoretical study suggests that it may actually reduce the number of species that coexist, she said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180823110013.htm){:target="_blank" rel="noopener"}


