---
layout: post
title: "How sharks and other animals evolved electroreception to find their prey"
date: 2018-08-02
categories:
author: "Benedict King And John Long, The Conversation"
tags: [Electroreception and electrogenesis,Fish,Evolution of fish,Coelacanth,Zoology,Ichthyology,Vertebrate zoology,Animals]
---


Today’s sharks are known to use electroreception to find their prey. It also reveals how completely new kinds of sensory organs were present in the ancient relatives of sharks and bony fishes, the extinct placoderm fishes. Fossil evidence for electroreception  High-resolution CT scans allowed us to digitally dissect well-preserved fossils and reveal sensory systems preserved inside the bones. One of the best known sensory systems in fossil fishes is the lateral line system, which detects pressure changes in water. These ancient electroreceptor systems appear to have been particularly elaborate in fossil lungfish.

<hr>

[Visit Link](https://phys.org/news/2018-02-sharks-animals-evolved-electroreception-theirprey.html){:target="_blank" rel="noopener"}


