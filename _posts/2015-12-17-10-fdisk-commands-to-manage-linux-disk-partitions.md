---
layout: post
title: "10 fdisk Commands to Manage Linux Disk Partitions"
date: 2015-12-17
categories:
author: Ravi Saive
tags: [Device file,Disk partitioning,Fdisk,Booting,Cylinder-head-sector,Hard disk drive,Memory paging,Ioctl,Computers,Data management,Computing,Computer architecture,System software,Technology,Software,Operating system families,Operating system technology,Computer science,Computer engineering,Information technology management,Computer data storage,Computer data,Computer hardware,Utility software,Storage software]
---


Print all Partition Table in Linux  To print all partition table of hard disk, you must be on command mode of specific hard disk say /dev/sda. You must be in fdisk command mode to do this. Command (m for help): d Partition number (1-4): 4 Command (m for help): w The partition table has been altered! Type the following command to enter into command mode of specific hard disk. After entering in command mode, now press “n” command to create a new partition under /dev/sda with specific size.

<hr>

[Visit Link](http://www.tecmint.com/fdisk-commands-to-manage-linux-disk-partitions/){:target="_blank" rel="noopener"}


