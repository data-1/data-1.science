---
layout: post
title: "Video and research release: How insects become queens or workers"
date: 2015-10-20
categories:
author: Biotechnology and Biological Sciences Research Council 
tags: [Epigenetics,Eusociality,Gene,Ant,Species,Cellular differentiation,Insect,Genome,Seirian Sumner,Genetics,Biology,Life sciences,Biotechnology]
---


Two insect species from Latin America, the dinosaur ant and the red paper wasp, have been used to uncover the molecular mechanisms underpinning queen and worker roles in social insects. Researchers from the University of Bristol, the Babraham Institute (Cambridge, UK) and the Centre for Genomic Regulation (Barcelona, Spain) analysed individual wasp and ant brains from queens and workers of both species to see whether caste differences could be explained by variations in how the genome is 'read' and regulated. Comparing the molecular differences between queens and workers of both species was surprising. We found very few differences in gene expression and gene functional specialisation between queens and workers in both the ant and the wasp, said Dr Solenn Patalano from the Epigenetics Programme at the Babraham Institute and lead author on the paper. The sequencing of the first wasp genome completes the trio of the social Hymenoptera (bees, wasps and ants), giving us a more balanced understanding of the molecular basis of sociality in insects, and opens up exciting new avenues of research into a somewhat neglected group of insects.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/babs-var101915.php){:target="_blank" rel="noopener"}


