---
layout: post
title: "Solar Overtakes Wind As Australia’s Number One Renewable Energy"
date: 2015-10-19
categories:
author: Joshua S Hill, David Waterworth, Written By
tags: [Renewable energy,Renewable energy commercialization,Photovoltaics,Energy and the environment,Energy,Nature,Sustainable energy,Renewable resources,Physical quantities,Sustainable technologies,Climate change mitigation,Sustainable development,Power (physics),Natural resources,Electric power,Economy,Technology,Environmental technology]
---


Originally published on Solar Love  Solar PV became Australia’s number one source of renewable energy in 2014, passing 4 GW of installed capacity, overtaking wind’s 3.8 GW. According to GlobalData’s accompanying press release, the report “states that renewables have become an integral part of the energy policy in Australia” — which could be argued, depending on GlobalData’s meaning here. While renewable energy is indeed an integral part of energy policy here in Australia, this is only the case insomuch as you consider the amount of time spent arguing about renewable’s place in energy policy here in Australia. GlobalData also believes that, while solar and wind will continue their role as the two main renewable energy technologies in Australia, bioenergy is next to make a move. Bioenergy capacity amounted to 573.9 Megawatts in 2014 and is expected to more than treble to 1.8 GW by the end of 2025.”  Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2015/10/14/solar-overtakes-wind-as-australias-number-one-renewable-energy/){:target="_blank" rel="noopener"}


