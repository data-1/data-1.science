---
layout: post
title: "Russian cosmonaut back after record 879 days in space"
date: 2016-06-30
categories:
author: ""
tags: [Astronaut,Gennady Padalka,International Space Station,Scott Kelly (astronaut),Mikhail Kornienko,Mir,Human spaceflight,Spaceflight technology,Space industry,Space program of Russia,Spaceflight,Aerospace,Astronautics,Space vehicles,Human spaceflight programs,Space exploration,Crewed spacecraft,Spacecraft,Life in space,Flight,Outer space,Space programs]
---


Russian cosmonaut Gennady Padalka, pictured entering the International Space Station in March, has spent a total of 879 days in space over five separate trips  Russian cosmonaut Gennady Padalka returned safely to Earth with two other astronauts from the International Space Station Saturday with the record for having spent the most time in space. Mogensen, the first Dane in Space and Aimbetov, the third cosmonaut from his country, had a comparatively short stay at the ISS having entered space in the Soyuz TMA-18M on September 2 and docking two days later on September 4. Andreas Mogensen, the first Dane in space, had a comparatively short stay at the ISS having entered space in the Soyuz TMA-18M on September 2 and docking two days later on September 4  Padalka made four trips to ISS in total. His first ever journey into space was to visit Russia's Mir space station in 1998. The three-man crew completed a perfect de-orbit burn to re-enter the earth's atmosphere at just after 00.00 GMT according to NASA television before a bullseye landing roughly 146 kilometers (90 miles) southeast of the Kazakh settlement of Dzhezkazgan less than an hour later.

<hr>

[Visit Link](http://phys.org/news/2015-09-russian-cosmonaut-days-space.html){:target="_blank" rel="noopener"}


