---
layout: post
title: "Most comprehensive study to date reveals evolutionary history of citrus"
date: 2016-04-23
categories:
author: SMBE Journals (Molecular Biology and Evolution and Genome Biology and Evolution)
tags: [Citrus,Evolutionary biology,Evolution,Fruit,Crops originating from Asia,Edible fruits,Tropical fruit,Biological evolution,Biology,Aurantioideae,Sour fruits,Genetics]
---


Citrus fruits -- delectable oranges, lemons, limes, kumquats and grapefruits -- are among the most important commercially cultivated fruit trees in the world, yet little is known of the origin of the citrus species and the history of its domestication. Now, Joaquin Dopazo et al, in a new publication in the journal Molecular Biology and Evolution, have performed the largest and most detailed genomic analysis on 30 species of Citrus, representing 34 citrus genotypes, and used chloroplast genomic data to reconstruct its evolutionary history. Another result from the study was the remarkable level of heteroplasmy, or hybridization seen, an event that the authors showed occurred frequently in Citrus evolution. Further radiation of Fortunella, sour and sweet oranges, lemons, and mandarins took place later (1.5-0.2 Mya). On a finer scale, the group also identified 6 genes that may be general hotspots of natural genetic variation in Citrus.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/mbae-mcs040815.php){:target="_blank" rel="noopener"}


