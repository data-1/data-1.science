---
layout: post
title: "Photosynthesis gene can help crops grow in adverse conditions"
date: 2015-09-19
categories:
author: Oxford University
tags: [Plant,Chloroplast,Photosynthesis,Reactive oxygen species,Chemistry,Biology,Nature,Physical sciences,Biochemistry]
---


In separate experiments, the three types of plant were exposed to different stressful conditions: high salt concentrations, drought, and the herbicide paraquat, which stimulates production of the toxic reactive oxygen compounds. The team found high levels of hydrogen peroxide in the mutant plants but low levels in the normal plants and even less in the SP1 overexpressors 'In fact, the overexpressors were indistinguishable from healthy, unstressed control plants,' explains Professor Jarvis. The team found that the movement of proteins used in photosynthesis into the chloroplast was significantly reduced in the overexpressors. 'All plants have the SP1 gene,' explains Professor Jarvis. 'Now it's just a question of getting plants to over-express it so that they can survive in adverse conditions.'

<hr>

[Visit Link](http://phys.org/news/2015-09-photosynthesis-gene-crops-adverse-conditions.html){:target="_blank" rel="noopener"}


