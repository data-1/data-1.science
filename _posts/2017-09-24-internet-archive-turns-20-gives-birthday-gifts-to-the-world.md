---
layout: post
title: "Internet Archive turns 20, gives birthday gifts to the world"
date: 2017-09-24
categories:
author: "VM (Vicky) Brasseur
(Alumni)"
tags: [Wayback Machine,Internet Archive,World Wide Web,Cyberspace,Internet,Digital media,Software,Communication,Technology,Computing]
---


The Internet Archive team did not disappoint, presenting some important and impressive advances which they've released in the past year, including but not limited to:  IA servers by John Blyberg; CC BY (on Flickr)  Of all the projects announced during the event though, by far one of the most exciting and impressive is the newly released ability to search the complete contents of all text items on the Internet Archive. The index at the moment is 4-5TB and it contains around 9 million documents...growing every day. Users can access the new feature by selecting the Search full text of books option below the search bar on the search results page. Full text isn't the only search feature launched by the Internet Archive last week. You can read more about these filtering options in a blog post published by the Archive.

<hr>

[Visit Link](https://opensource.com/life/16/11/internet-archive-turns-20){:target="_blank" rel="noopener"}


