---
layout: post
title: "The First 10 Years of Software Defined Networking"
date: 2018-05-26
categories:
author: "Esther Shein"
tags: [Linux,Open source,Software-defined networking,Nick McKeown,Computer network,OpenFlow,Computing,Information technology management,Software engineering,Information Age,Software development,Information technology,Computer networking,Telecommunications,Computer architecture,Software,Computers,Computer engineering,Computer science,Technology]
---


In 2008, if you wanted to build a network, you had to build it from the same switch and router equipment that everyone else had, according to Nick McKeown, co-founder of Barefoot Networks, speaking as part of a panel of networking experts at Open Networking Summit North America. Separate Planes  “Martin just simply showed that if you lift the control up and out of the switches, up into servers, you could replace the 2,000 CPUs with one CPU centrally managed and it would perform exactly how you wanted, could administered by about 10 people instead of 200. On the heels of that came the idea of slicing a production network using OpenFlow and a simple piece of software, he said. That brought about open source tools like Open vSwitch, “so we could build a type of network topology that we needed in virtualization.”  Confluence of Events  In the beginning, there was much hype about SDN and desegregation and OpenFlow, Wright said. The Next 10 Years  Looking ahead, the networking community “risks a bit of fragmentation as we will go off in different directions,’’ said McKeown.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/the-first-10-years-of-software-defined-networking/){:target="_blank" rel="noopener"}


