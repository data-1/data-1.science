---
layout: post
title: "Cool the Planet? Geoengineering Is Easier Said Than Done"
date: 2017-08-31
categories:
author: "Tracy Staedter"
tags: [Cirrus cloud,Cloud,Climate change,Climate engineering,Atmosphere of Earth,Stratospheric sulfur aerosols,Earth,Climate,Greenhouse effect,Atmosphere,Precipitation,Atmospheric sciences,Earth phenomena,Climate variability and change,Natural environment,Physical sciences,Nature,Applied and interdisciplinary physics,Physical geography,Earth sciences]
---


The idea, Lohmann said, is to inject solid particles, like desert dust, into the atmosphere at spots slightly lower than where cirrus clouds would naturally form. The researchers' computer models have shown that if done correctly, cirrus thinning could reduce global temperatures by 0.9 degrees F (0.5 degrees C), Lohmann said. But if done incorrectly, the activity could produce cirrus clouds where none existed before, contributing to the very problem it's meant to solve, she added. Although computer models tend to agree that it’s best to inject the aerosols into the stratosphere above the tropics or subtropics, and that the aerosols would disperse globally, the models differ on the extent of injection required for a given level of cooling, the authors wrote. And no method designed to cool the planet deals with the gases in the atmosphere that are the sources of the problem and are contributing to increasing levels of acid in the oceans, the researchers said.

<hr>

[Visit Link](https://www.livescience.com/59901-geoengineering-methods-to-cool-planet.html){:target="_blank" rel="noopener"}


