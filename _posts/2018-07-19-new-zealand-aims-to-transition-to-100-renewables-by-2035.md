---
layout: post
title: "New Zealand Aims to Transition to 100% Renewables by 2035"
date: 2018-07-19
categories:
author: ""
tags: [Renewable energy,Electricity,Low-carbon economy,Sustainable technologies,Energy,Nature,Sustainable development,Natural environment,Sustainable energy,Climate change mitigation,Physical quantities,Economy and the environment,Energy and the environment,Climate change,Economy]
---


Electric Shock  Last month, New Zealand named Jacinda Ardern as its 40th prime minister. The nation is well on its way to ending its reliance on fossil fuels, as it already harvests more than 80 percent of its electricity from renewable sources. Figures from 2016 suggest that 60 percent of New Zealand's electricity is generated via hydropower. Transitioning New Zealand's electricity grid to renewables is just one part of her plan. In November 2016, 47 of the world's most economically disadvantaged countries committed to a future powered by renewable energy, and France plans to eliminate fossil fuel production by 2040.

<hr>

[Visit Link](https://futurism.com/new-zealand-transition-100-renewables-2035/){:target="_blank" rel="noopener"}


