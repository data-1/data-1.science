---
layout: post
title: "Turning political pledges into action on climate change"
date: 2015-10-08
categories:
author: Norwegian University Of Science
tags: [Reducing emissions from deforestation and forest degradation,Tropical rainforest,Deforestation,Greenhouse gas,Forest,Politics of climate change,Climate change mitigation,Climate change,Nature,Environmental issues with fossil fuels,Societal collapse,Natural environment,Climate variability and change,Environmental impact,Global environmental issues,Environmental issues]
---


In the end, someone must make the decision to launch climate initiatives – and in most political systems, that decision making requires support from the key players. An oil nation's climate obligations  With just 5 million people, Norway seems an unlikely player on the world climate policy stage. Credit: JK Røttereng  The first way is to store carbon dioxide in trees, by protecting tropical forests. Røttereng said that the difficulties of cutting greenhouse gas emissions at home have led Norway to support alternative approaches to cutting emissions – like tropical forest protection abroad and global initiatives to advance storing carbon in geological formations. A determined optimist  Røttereng isn't sure that the 190 countries headed to Paris later this year will be able to come up with an agreement that will achieve the emissions reductions the world needs.

<hr>

[Visit Link](http://phys.org/news/2015-10-political-pledges-action-climate.html){:target="_blank" rel="noopener"}


