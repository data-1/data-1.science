---
layout: post
title: "ESA experimental spaceplane completes research flight"
date: 2015-09-03
categories:
author: "$author"   
tags: [Intermediate eXperimental Vehicle,European Space Agency,Vega (rocket),Launch vehicle,Outer space,Transport,European space programmes,Space policy,Space agencies,Space exploration,Space access,Vehicles,Space traffic management,Technology,Rocketry,Flight,Astronautics,Space vehicles,Aerospace,Spacecraft,Space science,Space industry,Spaceflight technology,Space programs,Astronomy,Spaceflight]
---


IXV opens a new chapter for ESA “ESA and its Member States, together with European space industry, are now ready to take up new challenges in several fields of space transportation, in future launchers, robotic exploration or human spaceflight.” “This mission will teach us a lot about the technologies we need to apply in new launch systems, in particular when we think about reusable systems,” notes Gaele Winters, ESA Director of Launchers. “The cutting-edge technology we validated today, and the data gathered from the sensors aboard IXV, will open numerous opportunities for Europe to develop ambitious plans in space transportation for a multitude of applications.” ESA will provide footage of the recovery when it is available from the ship in the Pacific Ocean. Since its introduction in 2012, the launcher has reduced operational costs and delivered its first commercial customers into orbit, as well as demonstrating numerous capabilities such as dual payloads and different orbits. Learn more about Vega at http://www.esa.int/Our_Activities/Launchers/Launch_vehicles/Vega  About the European Space Agency The European Space Agency (ESA) provides Europe’s gateway to space. ESA has Cooperation Agreements with six other Member States of the EU.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Launchers/IXV/ESA_experimental_spaceplane_completes_research_flight){:target="_blank" rel="noopener"}


