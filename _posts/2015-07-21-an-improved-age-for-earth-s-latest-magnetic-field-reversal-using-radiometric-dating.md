---
layout: post
title: "An improved age for Earth's latest magnetic field reversal using radiometric dating"
date: 2015-07-21
categories:
author: Research Organization of Information and Systems 
tags: [Geology,Earths magnetic field,Physical sciences,Earth sciences]
---


The latest reversal is called by geologists the Matuyama-Brunhes boundary (MBB), and occurred approximately 780,000 years ago. The MBB is extremely important for calibrating the ages of rocks and the timing of events that occurred in the geological past; however, the exact age of this event has been imprecise because of uncertainties in the dating methods that have been used. The team studied volcanic ash that was deposited immediately before the MBB. Some of these crystals formed at the same time as the ash; thus, radiometric dating of these zircons using the uranium-lead method provided the exact age of the ash. Dr. Yusuke Suganuma of the National Institute of Polar Research, Tokyo, who is the lead author on the paper, commented: This study is the first direct comparison of radiometric dating, dating of sediments, and the geomagnetic reversal for the Matuyama-Brunhes boundary.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/rooi-aia070615.php){:target="_blank" rel="noopener"}


