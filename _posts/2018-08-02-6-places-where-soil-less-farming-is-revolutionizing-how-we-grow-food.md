---
layout: post
title: "6 places where soil-less farming is revolutionizing how we grow food"
date: 2018-08-02
categories:
author: "Greg Beach"
tags: []
---


If it seems like “hydroponic systems” are everywhere, that’s because they are. These are then absorbed, along with water, through a plant’s roots. Some systems even feed nutrients to plants through the air! From water-less deserts to the sun-less underground, soil-less farming is offering new possibilities to feed an increasingly urban, growing global population in a more Earth-friendly way. The mushrooms grow in a special medium and, through their respiration, provide valuable CO2 for the plants to thrive.

<hr>

[Visit Link](https://inhabitat.com/6-places-where-soil-less-farming-is-changing-how-we-grow-food){:target="_blank" rel="noopener"}


