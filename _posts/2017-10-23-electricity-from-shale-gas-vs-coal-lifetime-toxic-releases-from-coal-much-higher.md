---
layout: post
title: "Electricity from shale gas vs. coal: Lifetime toxic releases from coal much higher"
date: 2017-10-23
categories:
author: University of Michigan
tags: [Shale gas,Hydraulic fracturing,Fossil fuel power station,Air pollution,Natural gas,Coal,Environmental issues,Human impact on the environment,Natural environment,Environment,Chemistry,Fossil fuels,Environmental science,Nature,Technology]
---


ANN ARBOR -- Despite widespread concern about potential human health impacts from hydraulic fracturing, the lifetime toxic chemical releases associated with coal-generated electricity are 10 to 100 times greater than those from electricity generated with natural gas obtained via fracking, according to a new University of Michigan study. It looks at the amount of toxic chemicals released into the air, soil and water during both the resource extraction and electricity generation phases of both technologies and concludes that the potential human health impacts of electricity from coal are much higher. For the hydraulic fracturing system, the study estimated the toxicity of the fracturing fluid chemicals used to crack rock and release natural gas, as well as the wastewater associated with shale-gas extraction. In both systems, particulate matter released into the air from power plants during electricity generation was the dominant toxicity contributor and outweighed chemical releases that may occur during extraction. Data from 2,900 hydraulically fractured wells in the state were used to estimate potential releases of fracturing fluid chemicals and wastewater.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uom-efs102017.php){:target="_blank" rel="noopener"}


