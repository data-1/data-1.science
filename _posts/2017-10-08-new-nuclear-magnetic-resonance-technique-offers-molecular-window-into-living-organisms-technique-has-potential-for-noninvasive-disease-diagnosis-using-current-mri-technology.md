---
layout: post
title: "New nuclear magnetic resonance technique offers 'molecular window' into living organisms: Technique has potential for noninvasive disease diagnosis using current MRI technology"
date: 2017-10-08
categories:
author: "University of Toronto"
tags: [Magnetic resonance imaging,Nuclear magnetic resonance,Medical imaging,Molecule,Science,Chemistry,Technology]
---


In a way we've developed this molecular window that can look inside a living system and extract a full metabolic profile, says Professor Andre Simpson, who led research into developing the new technique that uses nuclear magnetic resonance (NMR) technology. Simpson and his team were able to overcome the magnetic distortion problem by creating tiny communication channels based on something called long-range dipole interactions between molecules. It also has the potential to tell us how the brain works. The new technique can potentially be used to look inside those locations and reveal the chemicals actually causing the response. Since you can see metabolites in a sample that you weren't able to see before, you can now identify molecules that may indicate there's a problem, he says.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/05/170502112531.htm){:target="_blank" rel="noopener"}


