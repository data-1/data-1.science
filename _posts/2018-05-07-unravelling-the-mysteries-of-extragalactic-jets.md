---
layout: post
title: "Unravelling the mysteries of extragalactic jets"
date: 2018-05-07
categories:
author: "University of Leeds"
tags: [Galaxy,Black hole,Active galactic nucleus,Astrophysical jet,American Association for the Advancement of Science,Astronomy,College and university rankings,Space science,Science,Physical sciences,Astrophysics,Physics,Astronomical objects]
---


University of Leeds researchers have mathematically examined plasma jets from supermassive black holes to determine why certain types of jets disintegrate into huge plumes. Their study, published in Nature Astronomy, has found that these jets can be susceptible to an instability never before considered as important to the jet's flow and is similar to an instability that often develops in water flowing inside a curved pipe or a rotating cylindrical vessel. Instability starts at the curved boundary, travels upstream on the jet and then converges at one point -- what we refer to as the 'reconfinement point'. The jets and their plumes are so bright that sometimes they outshine their host galaxies and are always more easily spotted than black holes, which are inferred indirectly, in space observations. Additionally, the University was awarded a Gold rating by the Government's Teaching Excellence Framework in 2017, recognising its 'consistently outstanding' teaching and learning provision.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/uol-utm120817.php){:target="_blank" rel="noopener"}


