---
layout: post
title: "Psychotropic drug prescriptions: Therapeutic advances or fads?"
date: 2016-04-11
categories:
author: University of Montreal
tags: [Psychiatry,Medical prescription,Mental disorder,Medication,Diagnostic and Statistical Manual of Mental Disorders,Drug,Health sciences,Health,Medicine,Health care,Clinical medicine,Mental disorders,Mental health,Psychology,Medical specialties,Medical treatments]
---


She recalls that for much of the nineteenth century, physicians based their diagnostic and therapeutic approach on the specificity of patients, and not diseases. Psychiatry was thus dominated by the primacy of patient specificity. The primacy of the universality of mental disease and the standardization of diagnostics and drug treatments has become dominant in mental health, Collin said. Although in this case, prescription practices are based on complex clinical reasoning, Collin shows that the therapeutic strategies of psychiatrists are directed against specific symptoms and not against mental illnesses as separate entities. Collin highlights the strong tension between the reality of medical and psychiatric practice, and the reference to the Diagnostic and Statistical Manual of Mental Disorder (DSM).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-11/uom-pdp111114.php){:target="_blank" rel="noopener"}


