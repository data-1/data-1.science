---
layout: post
title: "'Origami' is reshaping DNA's future"
date: 2016-07-09
categories:
author: "The Kavli Foundation"
tags: [DNA origami,DNA,Protein,Virus,Molecule,Biology,Antibody,Life sciences,Applied and interdisciplinary physics,Materials science,Biochemistry,Biotechnology,Materials,Chemistry,Physical sciences,Technology]
---


Three leading researchers discuss how DNA may be used as a building material to help us develop a new generation of medicines, build electronic devices and probe the mysteries of proteins  Ten years after its introduction, DNA origami, a fast and simple way to assemble DNA into potentially useful structures, is finally coming into its own. In addition to using DNA origami to create optical devices, Shih is interested in using it to make new types of medicines. Instead, he envisions exposing the immune system to DNA origami scaffolds that are holding pieces of virus. DNA origami could allow us to arrange proteins in ways that give us access to the language of the immune system. This would free chemists from having to crystallize proteins, a process that is time-consuming and often fails, to understand their structure.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/tkf-ir070516.php){:target="_blank" rel="noopener"}


