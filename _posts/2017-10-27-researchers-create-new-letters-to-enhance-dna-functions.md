---
layout: post
title: "Researchers create new 'letters' to enhance DNA functions"
date: 2017-10-27
categories:
author: Agency For Science, Technology, Research, A Star
tags: [Base pair,DNA,Genetics,Aptamer,Molecular biology,Technology,Chemistry,Biotechnology,Biology,Biochemistry,Life sciences]
---


Now, researchers from the Institute of Bioengineering and Nanotechnology (IBN) of the Agency for Science, Technology and Research (A*STAR) have created a DNA technology with two new genetic letters that could better detect infectious diseases, such as dengue and Zika. IBN is developing a diagnostic kit using the new DNA base pair (Ds-Px) to improve dengue and Zika detection. They found that the structure of the new artificial base pair was strikingly similar to a natural base pair. We did not know the actual molecular structure of our Ds-Px pair during DNA replication until the recent study with our collaborators at University of Konstanz. Using this genetic alphabet expansion technology, IBN is developing DNA aptamers, which are modified DNA molecules that can bind to molecular targets in the body.

<hr>

[Visit Link](https://phys.org/news/2017-10-letters-dna-functions.html){:target="_blank" rel="noopener"}


