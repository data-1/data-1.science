---
layout: post
title: "Quantifying the impact of volcanic eruptions on climate"
date: 2015-09-07
categories:
author: Université de Genève 
tags: [Volcano,Climate,Types of volcanic eruptions,1257 Samalas eruption,Atmosphere,Climate variability and change,Applied and interdisciplinary physics,Natural environment,Nature,Physical geography,Earth sciences]
---


To quantify the temporary cooling induced by the largest eruptions over the last 1500 years, whose magnitude exceeded Mount Pinatubo's, scientists usually adopt two approaches : dendroclimatology which relies on the analysis of tree-ring based proxies and climate model simulations in response to the volcanic particles effect. But until now these two approaches delivered results that were quite contradictory, and this prevented scientists from accurately assessing the impact of major volcanic eruptions on climate. In this multidisciplinary team, dendrochronologists came up with a new reconstruction of the Northern Hemisphere summer temperature in the last 1'500 years. Results show that the year following a large eruption is characterised by a greater cooling than asserted in previous reconstructions and that this cooling does not last for more than three years at an hemispheric scale. For the first time, results provided by reconstructions and climate models about the intensity of cooling converge and demonstrate that the Tampora and Samalas eruptions generated an average drop in temperature in the Northern Hemisphere fluctuating between 0.8 and 1.3°C during the summer 1258 and 1816.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/udg-qti083115.php){:target="_blank" rel="noopener"}


