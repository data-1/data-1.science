---
layout: post
title: "Climate change: How bad can it get?"
date: 2015-12-18
categories:
author: "$author" 
tags: [Sea level rise,Climate change,Earth phenomena,Global environmental issues,Human impact on the environment,Nature,Climate,Applied and interdisciplinary physics,Societal collapse,Environmental issues,Environmental impact,Global natural environment,Environmental issues with fossil fuels,Climate variability and change,Natural environment,Physical geography,Earth sciences]
---


Heatwaves  Some 1.5 billion people will be exposed to heatwaves every year by 2100 under 2C, according to Avoid 2, a British government-funded consortium of climate change research institutes. Even a 2C rise as targeted by the UN would submerge land currently occupied by 280 million people, according to Climate Central, a US-based research group. The change could take a few hundred years, or up to 2,000 years. Treasures of nature, civilisation  From the glimmering coral of the Great Barrier Reef to Mount Fuji and the canal-crossed city of Venice, global warming may spell the ruin of some of the most precious jewels of nature and civilisation, UNESCO warns. Climate change threatens a range of other treasures as well, says UNESCO.

<hr>

[Visit Link](http://phys.org/news/2015-12-climate-bad.html){:target="_blank" rel="noopener"}


