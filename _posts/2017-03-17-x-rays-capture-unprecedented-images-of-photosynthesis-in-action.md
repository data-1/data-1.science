---
layout: post
title: "X-rays capture unprecedented images of photosynthesis in action"
date: 2017-03-17
categories:
author: "Lawrence Berkeley National Laboratory"
tags: [Photosystem II,Thylakoid,Photosystem,Photosynthesis,Chemical reaction,Applied and interdisciplinary physics,Physical sciences,Chemistry]
---


The experiments, led by the Department of Energy's Lawrence Berkeley National Laboratory (Berkeley Lab), are helping researchers narrow down the process by which the protein, called photosystem II, uses light energy to split water and create oxygen. advertisement  We have been trying for decades to understand how plants split water into oxygen, protons, and electrons, said Yachandra. Capturing data before destruction  The ability to peek into the process of splitting water at room temperature has been hindered by the fact that most imaging or crystallography technology using X-ray lasers blasts the samples to bits before meaningful data can be collected. We're trying to see the process at extremely tiny length scales, and this is the first time we're getting a spatial resolution that even approaches that. Putting the pieces together  Software algorithms developed by Sauter, Paul Adams (also of the Molecular Biophysics and Integrated Bioimaging Division at Berkeley Lab), and their respective groups were then used to translate the diffraction readings into the 3-D rendering of photosystem II.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/11/161121130953.htm){:target="_blank" rel="noopener"}


