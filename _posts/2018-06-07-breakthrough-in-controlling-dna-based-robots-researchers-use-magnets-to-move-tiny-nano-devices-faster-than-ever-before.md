---
layout: post
title: "Breakthrough in controlling DNA-based robots: Researchers use magnets to move tiny nano-devices faster than ever before"
date: 2018-06-07
categories:
author: "Ohio State University"
tags: [Machine,Nanorobotics,Nanotechnology,Robot,DNA,Molecular machine,News aggregator,Magnetism,Chemistry,Technology,Physical sciences,Applied and interdisciplinary physics]
---


Not only does the discovery represent a significant improvement in speed, this work and one other recent study herald the first direct, real-time control of DNA-based molecular machines. Previously, researchers could only move DNA indirectly, by inducing chemical reactions to coax it to move certain ways, or introducing molecules that reconfigure the DNA by binding with it. That was the case with earlier methods for controlling DNA nano-machines, said Castro, associate professor of mechanical and aerospace engineering. The tweezers were actually made of groups of magnetic particles that moved in sync to nudge the cells where people wanted them to go. Castro said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180601134740.htm){:target="_blank" rel="noopener"}


