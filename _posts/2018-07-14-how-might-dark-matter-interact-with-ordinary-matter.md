---
layout: post
title: "How might dark matter interact with ordinary matter?"
date: 2018-07-14
categories:
author: "University of California - Riverside"
tags: [Dark matter,Weakly interacting massive particles,Particle physics,Matter,Physics,Physical cosmology,Astrophysics,Physical sciences,Nature,Science,Astronomy]
---


In the search for direct detection of dark matter, the experimental focus has been on WIMPs, or weakly interacting massive particles, the hypothetical particles thought to make up dark matter. The researchers then search for a signal that identifies this interaction. Now, we believe PandaX-II, one of the world's most sensitive direct detection experiments, is poised to validate the SIDM model when a dark matter particle is detected. One of only three xenon-based dark matter direct detection experiments in the world, PandaX-II is one of the frontier facilities to search for extremely rare events where scientists hope to observe a dark matter particle interacting with ordinary matter and thus better understand the fundamental particle properties of dark matter. Now with the PandaX-II experimental collaboration, Yu has shown how self-interacting dark matter theories may be distinguished at the PandaX-II experiment.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180713093545.htm){:target="_blank" rel="noopener"}


