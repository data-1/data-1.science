---
layout: post
title: "NASA's Hubble observations suggest underground ocean on Jupiter's largest moon"
date: 2015-12-07
categories:
author: "$author" 
tags: [Ganymede (moon),Hubble Space Telescope,Moon,Jupiter,Aurora,Space Telescope Science Institute,NASA,Goddard Space Flight Center,Space science,Planets of the Solar System,Spaceflight,Bodies of the Solar System,Astronomical objects,Science,Solar System,Physical sciences,Planetary science,Outer space,Astronomy]
---


NASA's Hubble Space Telescope has the best evidence yet for an underground saltwater ocean on Ganymede, Jupiter's largest moon. Ganymede is the largest moon in our solar system and the only moon with its own magnetic field. A team of scientists led by Joachim Saur of the University of Cologne in Germany came up with the idea of using Hubble to learn more about the inside of the moon. Because aurorae are controlled by the magnetic field, if you observe the aurorae in an appropriate way, you learn something about the magnetic field. ###  NASA's Hubble Space Telescope is celebrating 25 years of groundbreaking science on April 24.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/nsfc-nho031215.php){:target="_blank" rel="noopener"}


