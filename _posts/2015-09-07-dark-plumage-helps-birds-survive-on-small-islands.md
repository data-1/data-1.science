---
layout: post
title: "Dark plumage helps birds survive on small islands"
date: 2015-09-07
categories:
author: American Ornithological Society Publications Office 
tags: [Feather,Ornithology,Bird,Animals,Biology]
---


One less-studied pattern of evolution on islands is the tendency for animal populations to develop melanism--that is, dark or black coloration. J. Albert Uy and Luis Vargas-Castro of the University of Miami found an ideal species to study this phenomenon in the Chestnut-bellied Monarch (Monarcha castaneiventris), a bird found in the Solomon Islands. After visiting 13 islands of varying sizes to survey their Chestnut-bellied Monarch populations, Uy and Vargas-Castro confirm in a new paper published this week in The Auk: Ornithological Advances that island size predicts the frequency of melanic birds, with populations on smaller islands including more dark individuals. Previous experiments with other Monarcha castaneiventris subspecies using taxidermied birds and recorded songs have shown that melanic birds react more aggressively than their chestnut-bellied counterparts when they perceive a threat to their territory. It took me over a decade to finally manage to get to the Solomons, and I've been working on these flycatchers now for nearly 10 years.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/copo-dph072215.php){:target="_blank" rel="noopener"}


