---
layout: post
title: "Image: Chaos on watery Europa"
date: 2015-05-11
categories:
author: European Space Agency
tags: [Europa (moon),Jupiter Icy Moons Explorer,Planetary science,Astronomical objects known since antiquity,Astronomical objects,Planets of the Solar System,Planets,Ancient astronomy,Jupiter,Planemos,Moons,Space science,Gas giants,Outer space,Outer planets,Astronomy,Solar System,Bodies of the Solar System]
---


Although it is thought to be mostly made up of rocky material, the moon is wrapped in a thick layer of water – some frozen to form an icy crust, some potentially pooled in shallow underground lakes or layers of slush, and vast quantities more lurking even deeper still in the form of a giant subsurface ocean. This false-colour image from NASA's Galileo spacecraft shows a disrupted part of Europa's crust known as Conamara Chaos. In the case of Conamara Chaos, for example, a large 26 km-diameter crater named Pwyll lies 1000 km to the south, and a handful of smaller, 500 m-diameter craters are scattered throughout the region, likely formed by lumps of ice thrown up by the impact that created Pwyll. ESA's JUpiter ICy moons Explorer (Juice) mission aims to do just that when it arrives in the Jovian system in 2030. The image combines data taken by Galileo's Solid State Imaging system during three orbits through the Jovian system in 1996 and 1997.

<hr>

[Visit Link](http://phys.org/news350554382.html){:target="_blank" rel="noopener"}


