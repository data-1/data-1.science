---
layout: post
title: "Digging Deeper, Why Renewables are Beating Coal and Gas in Some Parts of the World"
date: 2015-10-14
categories:
author: Jennifer Runyon, Jennifer Runyon Has Been Studying, Reporting About The World'S Transition To Clean Energy Since As Editor Of The World'S Largest Renewable Energy Publication, Renewable Energy World, She Observed, Interviewed Experts About, Reported On Major Clean Energy Milestones Including Germany'S Explosive Growth Of Solar Pv, The Formation, Development Of The U.S. Onshore Wind Industry, The U.K. Offshore Wind Boom
tags: [Levelized cost of energy,Wind power,Solar power,Capacity factor,Renewable energy,Photovoltaics,Coal,Energy conversion,Sustainable development,Power (physics),Sustainable energy,Electric power,Sustainable technologies,Electricity,Environmental technology,Physical quantities,Technology,Climate change mitigation,Energy and the environment,Energy technology,Renewable resources,Natural resources,Energy,Nature]
---


This is why LCOE is useful – because while in a wind farm, the fuel is free, the output is much less than a coal plant, which could theoretically run 90 percent of the time (this is called its capacity factor or utilization rate). It shows not only that the LCOE for wind and solar is beating coal and natural gas in some regions of the world, but that other renewable technologies such as geothermal, biomass incineration and small hydro also have very low LCOEs and in many regions are cost competitive or cheaper than fossil or nuclear energy. Nuclear, like coal and gas, has very different LCOE levels from one region of the world to another, but both the Americas and the Europe, Middle East and Africa region saw increases in levelized costs, to $261 and $158 per MWh respectively. In the UK, onshore wind comes in on average at $85 per MWh in the second half of 2015, compared to $115 for combined-cycle gas and $115 for coal-fired power; in Germany, onshore wind is at $80, compared to $118 for gas and $106 for coal. In the US, coal and gas are still cheaper, at $65 per MWh, against onshore wind at $80 and PV at $107.

<hr>

[Visit Link](http://www.renewableenergyworld.com/articles/2015/10/digging-deeper-why-renewables-are-beating-coal-and-gas-in-some-parts-of-the-world.html){:target="_blank" rel="noopener"}


