---
layout: post
title: "The properties of pre-stellar cores"
date: 2016-01-29
categories:
author: Harvard-Smithsonian Center For Astrophysics
tags: [Star formation,Star,Cosmic dust,Stellar core,Stellar astronomy,Physical sciences,Astronomy,Space science,Nature,Physics,Science]
---


A new infrared study of 3218 cores in various stages of development has enabled astronomers to categorize the temperatures, densities, and evolutionary characters of young stellar nurseries. Credit: NASA/Spitzer and P. Myers  Stars like the Sun begin their lives as cold, dense cores of dust and gas that collapse under the influence of gravity until nuclear fusion is ignited. Although the groups overlap in their properties, the large sample enables the scientists to conclude that, on average, in the quiescent clumps the dust temperature increases towards the outer regions, whereas the temperatures in protostellar and ionized hydrogen cores increase towards the inner region, consistent with the idea that they are being internally heated. This study has also identified a population of particularly cold and infrared-dark objects that are probably still in the stages of contraction, or else for some reason have had their star formation aborted. Far-Infrared Dust Temperatures and Column Densities of the MALT90 Molecular Clump Sample.

<hr>

[Visit Link](http://phys.org/news/2016-01-properties-pre-stellar-cores.html){:target="_blank" rel="noopener"}


