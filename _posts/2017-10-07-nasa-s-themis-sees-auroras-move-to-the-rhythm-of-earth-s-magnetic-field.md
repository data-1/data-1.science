---
layout: post
title: "NASA's THEMIS sees Auroras move to the rhythm of Earth's magnetic field"
date: 2017-10-07
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Aurora,THEMIS,Magnetosphere,Solar wind,Space weather,Goddard Space Flight Center,Space science,Astronomy,Physical sciences,Nature,Outer space,Planetary science,Physics,Electromagnetism,Science]
---


Using data from NASA's Time History of Events and Macroscale Interactions during Substorms, or THEMIS, scientists have observed Earth's vibrating magnetic field in relation to the northern lights dancing in the night sky over Canada. advertisement  However, under the right conditions, some solar particles and energy can penetrate the magnetosphere, disturbing Earth's magnetic field in what's known as a substorm. In this unstable environment, electrons in near-Earth space stream rapidly down magnetic field lines towards Earth's poles. To map the auroras' electric dance, the scientists imaged the brightening and dimming aurora over Canada with all-sky cameras. Further out in space, the five THEMIS probes were well-positioned to collect data on the motion of the disrupted field lines.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/09/160912141948.htm){:target="_blank" rel="noopener"}


