---
layout: post
title: "Precise and programmable biological circuits"
date: 2015-12-04
categories:
author: ETH Zurich 
tags: [Sensor,Biology,Cell (biology),Signal,Synthetic biology,Biological computing,Cancer,DNA,Biosensor,Gene,Protein,Biotechnology,Genetics,Branches of science,Chemistry,Molecular biology,Biochemistry,Technology,Life sciences]
---


The researchers recently published their work in the scientific journal Nature Chemical Biology. In the controllable biosensor developed by Lapique, the gene responsible for the output signal is not active in its basic state, as it is installed in the wrong orientation in the circuit DNA. To date, the researchers have tested the function of their activation-ready sensor in cell culture of human kidney and cancer cells. Nevertheless, they are already looking ahead to further developing the sensor so that it can be used in a more complex bio-computer that detects and kills cancer cells. Versatile signal converter developed  Still, combining various biological components to form more complex bio-computers constitutes a further challenge for bio-engineers.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/ez-pap102314.php){:target="_blank" rel="noopener"}


