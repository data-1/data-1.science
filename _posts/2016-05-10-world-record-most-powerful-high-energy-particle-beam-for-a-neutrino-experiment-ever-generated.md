---
layout: post
title: "World record: Most powerful high-energy particle beam for a neutrino experiment ever generated"
date: 2016-05-10
categories:
author: Fermi National Accelerator Laboratory
tags: [Fermilab,Neutrino,Deep Underground Neutrino Experiment,Particle accelerator,Science,Experimental physics,Physics,Particle physics]
---


Fermilab's Main Injector accelerator, one of the most powerful particle accelerators in the world, has just achieved a world record for high-energy beams for neutrino experiments. The next goal for the laboratory's two-mile-around Main Injector accelerator—the final and most powerful in Fermilab's accelerator chain—is to deliver 700-kilowatt beams to the laboratory's various experiments. Scientists then use particle detectors to catch as many of those neutrinos as possible and record their interactions. The more particles in that beam, the more opportunities researchers will have to study these rare interactions. Since 2011, Fermilab has made significant upgrades to its accelerators and reconfigured the complex to provide the best possible particle beams for neutrino and muon experiments.

<hr>

[Visit Link](http://phys.org/news355581828.html){:target="_blank" rel="noopener"}


