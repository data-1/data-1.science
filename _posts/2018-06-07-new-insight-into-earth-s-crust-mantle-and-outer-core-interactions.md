---
layout: post
title: "New insight into Earth's crust, mantle and outer core interactions"
date: 2018-06-07
categories:
author: "University of Liverpool"
tags: [Earth,Plate tectonics,Subduction,Geomagnetic reversal,Planetary core,Earths magnetic field,Structure of the Earth,Earth sciences,Geology,Geophysics,Nature,Applied and interdisciplinary physics,Planets of the Solar System,Terrestrial planets,Planetary science,Physical sciences,Global natural environment]
---


Using previously unavailable data, researchers confirm a correlation between the movement of plate tectonics on the Earth's surface, the flow of mantle above the Earth's core and the rate of reversal of the Earth's magnetic field which has long been hypothesised. In a paper published in the journal Tectonophysics, they suggest that it takes around 120-130 million years for slabs of ancient ocean floor to sink (subduct) from the Earth's surface to a sufficient depth in the mantle where they can cool the core, which in turn causes the liquid iron in the Earth's outer core to flow more vigorously and produce more reversals of the Earth's magnetic field. Liverpool palaeomagnetist, Professor Andy Biggin, said: Until recently we did not have good enough records of how much global rates of subduction had changed over the last few hundreds of millions of years and so we had nothing to compare with the magnetic records. When we were able to compare them, we found that the two records of subduction and magnetic reversal rate do appear to be correlated after allowing for a time delay of 120-130 million years for the slabs of ocean floor to go from the surface to a sufficient depth in the mantle where they can cool the core. For example, today such magnetic reversals occur on average four times per million years but one hundred million years ago, the field essentially stayed in the same polarity for nearly 40 million years.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180605103432.htm){:target="_blank" rel="noopener"}


