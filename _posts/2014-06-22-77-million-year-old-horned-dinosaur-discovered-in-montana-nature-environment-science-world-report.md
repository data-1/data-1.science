---
layout: post
title: "77-Million-Year-Old Horned Dinosaur Discovered in Montana : Nature & Environment : Science World Report"
date: 2014-06-22
categories:
author: Science World Report
tags: [Mercuriceratops,Ceratopsia,Dinosaurs,Paleontology,Late Cretaceous life,Taxa]
---


Scientists have unearthed fossilized remains of a horned dinosaur in Montana from the late Cretaceous Period. Gemini refers to the identical twin specimens that were discovered in north central Montana and the Dinosaur Provincial Park-UNESCO World Heritage Site in Alberta. It definitively would have stood out from the herd during the Late Cretaceous, said lead author Dr. Michael Ryan, curator of vertebrate paleontology at the Cleveland Museum of Natural History. They scientists believe that the wing like projection on the sides of its frill gave the male Mercuriceratops a competitive advantage in attracting mates. The two fossils -- squamosal bones from the side of the frill -- have all the features you would expect, just presented in a unique shape.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15530/20140619/77-million-year-old-new-species-of-horned-dinosaur-discovered-in-montana.htm){:target="_blank" rel="noopener"}


