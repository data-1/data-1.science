---
layout: post
title: "Google and The UN Team Up To Study The Effects of Climate Change"
date: 2018-07-27
categories:
author: ""
tags: [Ecosystem,United Nations Environment Programme,Biodiversity,Natural environment,Earth sciences,Global environmental issues,Environmental science,Physical geography,Environmental social science,Nature,Environment]
---


The United Nations' environmental agency has landed itself a powerful partner in the fight against climate change: Google. The tech company has agreed to partner with UN Environment to increase the world's access to valuable environmental data. The partnership will first focus on freshwater ecosystems, such as mountains, wetlands, and rivers. Collecting and analyzing satellite data is neither cheap nor easy, but Google is already doing it to power platforms such as Google Maps and Google Earth. READ MORE: UN Environment and Google Announce Ground-Breaking Partnership to Protect Our Planet [UN Environment]  More on freshwater: Climate Change Is Acidifying Our Lakes and Rivers the Same Way It Does With Oceans

<hr>

[Visit Link](https://futurism.com/un-environment-google-partnership/){:target="_blank" rel="noopener"}


