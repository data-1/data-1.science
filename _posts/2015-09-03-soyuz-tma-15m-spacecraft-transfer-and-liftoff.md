---
layout: post
title: "Soyuz TMA-15M spacecraft transfer and liftoff"
date: 2015-09-03
categories:
author: "$author"   
tags: [European Space Agency,Soyuz (spacecraft),Spacecraft,NASA,Rocketry,Life in space,Space vehicles,Human spaceflight,Flight,Space programs,Outer space,Astronautics,Spaceflight,Human spaceflight programs,Aerospace,Space exploration,Space program of the United States,Space industry,Space science,Space research,Crewed spacecraft,Space agencies,Scientific exploration,Space-based economy,Spaceflight technology]
---


This timelapse video shows the Soyuz TMA-15M spacecraft during transfer from the MIK 40 integration facility to Baikonur Cosmodrome launch pad 31, as well as the launch on 23 November 2014 with ESA astronaut Samantha Cristoforetti and her crewmates to the International Space Station where they will live and work for five months. With Samantha are Russian Soyuz commander Anton Shkaplerov and NASA astronaut Terry Virts. All three are part of the Station’s Expedition 42/43 crew. On this mission, Samantha is flying as an ESA astronaut for Italy’s ASI space agency under a special agreement between ASI and NASA.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2014/12/Soyuz_TMA-15M_spacecraft_transfer_and_liftoff){:target="_blank" rel="noopener"}


