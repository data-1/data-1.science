---
layout: post
title: "Why Humans Don't Have More Neanderthal Genes"
date: 2017-03-17
categories:
author: "Charles Q. Choi"
tags: [Interbreeding between archaic and modern humans,Neanderthal,Evolution,Mutation,Neanderthal genetics,Gene,Genetics,Biology,Branches of genetics,Population genetics,Biological evolution,Evolutionary biology]
---


If offspring of modern humans and Neanderthals had Neanderthal DNA segments that possessed some so-called deleterious alleles — that is, harmful genetic variants — then they'd be less likely to have kids, and so less likely to pass on those Neanderthal segments to future generations, Juric told Live Science. This suggests that natural selection weeded out Neanderthal variants of those genes, Juric said. Given the amount of time that has passed between interbreeding between Neanderthals and modern humans and the amounts and locations of Neanderthal DNA now found in the modern human genome, their findings suggest that many Neanderthal gene variants that entered the modern human genome after Neanderthals and modern humans interbred were weakly deleterious — that is, they are being slowly removed by natural selection, Juric said. However, when these gene variants made their way into modern humans, modern humans' larger populations — and thus bigger gene pools — were better at winnowing out these deleterious alleles. Future research could investigate which genetic variants from extinct relatives of modern humans were weeded out of the modern human genomes.

<hr>

[Visit Link](http://www.livescience.com/56800-why-humans-dont-have-more-neanderthal-genes.html){:target="_blank" rel="noopener"}


