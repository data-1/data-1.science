---
layout: post
title: "New project to develop a drug to fight cognitive impairment associated with schizophrenia"
date: 2015-12-07
categories:
author: Center for Genomic Regulation 
tags: [Schizophrenia,Mental disorder,Neuropsychopharmacology,Health,Disease,Drug,Life sciences,Health care,Clinical medicine,Medicine,Health sciences]
---


The goal of «Spark» is to carry out the first preclinical stage of the development of a first-in-class drug that will prevent, stop and reverse the progression of cognitive impairment associated with schizophrenia and other mental disorders, and to be able to start first-in-humans trials by 2016  The project, which has just won a 500,000 euro endorsement from the Ministry of Economy and Finance through the Retos-Colaboración (Challenges-Collaboration) subprogram- will be developed by a consortium led by the biotech Iproteos -located at the Parc Cientific de Barcelona- and comprised by the company Ascil-Biopharm, IRB Barcelona, the Centre for Genomic Regulation and the University of the Basque Country  Currently there is no drug targettingthe cognitive impairment associated with schizophrenia, the third most disabling disease even above paraplegia and blindness- according to the WHO and that affects about 24 million people worldwide. It is also the fifth disease with a higher annual cost to society, with more than 35,000 million euros cost only in the Europe Union  A public-private consortium led by the biotech Iproteos -based at Parc Científic de Barcelona (PCB)-, and comprised by the biopharmaceutical company Ascil-Biopharma, the Institute for Biomedical Research (IRB Barcelona), the Centre for Genomic Regulation (CRG) and the University of the Basque Country (UPV/EHU) has launched a project to advance the development of a new neuroprotective drug for the treatment of the cognitive impairment associated with schizophrenia and other mental disorders. ###  About Iproteos  Iproteos, located at the Parc Cientific de Barcelona, is a spin-off created in 2011 by two Catalan leading scientists in the field of therapeutic peptides, Teresa Tarragó and Ernest Giralt, based on the transfer of technology developed at IRB Barcelona and the University of Barcelona. For further information http://www.ascil-biopharm.com  About IRB Barcelona  Created in 2005 by the Generalitat de Catalunya, IRB Barcelona is a Severo Ochoa Centre of Excellence since 2011. For further information: http://www.crg.eu  About the Neuropsychopharmacology Group of the UPV/EHU  The Neuropsychopharmacology Research Group at the UPV / EHU is an international leader in the study of the biological substrates of mental illnesses and the development of new and more effective treatments for these diseases.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/cfgr-npt012715.php){:target="_blank" rel="noopener"}


