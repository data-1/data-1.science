---
layout: post
title: "Neurogenesis -- discovery of a new regulatory mechanism"
date: 2016-01-31
categories:
author: VIB (the Flanders Institute for Biotechnology)
tags: [Development of the nervous system,Gene expression,Nervous system,Neuron,Neurogenesis,Cellular differentiation,Neural stem cell,Brain,Protein,Stem cell,Drosophila melanogaster,Biology,Cell biology,Neuroscience,Biotechnology,Life sciences,Biological processes,Biochemistry,Molecular biology,Cellular processes,Genetics]
---


Bassem Hassan's* team at VIB/KU Leuven has discovered a previously unknown mechanism that is highly conserved between species and which regulates neurogenesis through precise temporal control of the activity of a family of proteins essential for brain development: the proneural proteins. How does a very small set of proteins expressed over a very limited period of time control the generation of such a high and diverse number of neurons? And finally, is this mechanism, which enables the development of the nervous system, conserved between species? How can a protein that auto-activates its own expression disappear at the peak of that expression? These results, confirmed in another experimental model, thus reveal the existence of a mechanism that is both highly conserved between species and universal, and which regulates neurogenesis and the generation of a sufficient and diverse number of neurons during brain development.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/vfi-n-d012916.php){:target="_blank" rel="noopener"}


