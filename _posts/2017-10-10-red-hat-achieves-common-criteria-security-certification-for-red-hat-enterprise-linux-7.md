---
layout: post
title: "Red Hat Achieves Common Criteria Security Certification for Red Hat Enterprise Linux 7"
date: 2017-10-10
categories:
author: ""
tags: [Common Criteria,Red Hat,Cloud computing,Red Hat Enterprise Linux,ProLiant,Information security,IBM Z,Linux,Evaluation Assurance Level,Operating system,Forward-looking statement,Computing,Technology,Computer science,Information Age,Computers]
---


Not only does the Common Criteria certification demonstrate that Red Hat Enterprise Linux offers industry-leading security features, this achievement also marks our flagship operating system as the first to bring a framework for Linux container technology into the world of more secure, certified computing. This certification provides government agencies, financial institutions, and customers in other security-sensitive environments the assurance that Red Hat Enterprise Linux 7.1 meets clear, specific security standards used by the federal government. In addition to Linux Container Framework Support, Red Hat Enterprise Linux 7 has also been certified to include functionality for:  Advanced Management (MLS mode only)  Labeled Security (MLS mode only)  Runtime protection against programming errors, encompassing address space layout randomization (ASLR), stack smashing protector strong and others  Packet Filter  This combined functionality makes Red Hat Enterprise Linux 7 the most secure platform that Red Hat has ever certified via Common Criteria. Not only does the Common Criteria certification demonstrate that Red Hat Enterprise Linux offers industry-leading security features, this achievement also marks our flagship operating system as the first to bring a framework for Linux container technology into the world of more secure, certified computing.”  Helmut Kurth, vice president and chief scientist, atsec information security corp.  “atsec has been working with Red Hat and the Linux community for many years to improve the security of the Linux Operating System. Achieving the EAL 4+ certification across the entire line of HPE ProLiant servers with Red Hat Enterprise Linux ensures our customers meet stringent government security standards.”  Jim Wasko, vice president, Open Systems, IBM  The Red Hat Common Criteria Certification provides enterprise level security validation for large scale IBM Power and Z Systems computing environments leveraging open source solutions.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/red-hat-achieves-common-criteria-security-certification-red-hat-enterprise-linux-7){:target="_blank" rel="noopener"}


