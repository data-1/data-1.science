---
layout: post
title: "CERN's revamped particle smasher ready to push physics into unknown"
date: 2016-04-17
categories:
author:  
tags: [Large Hadron Collider,CERN,Compact Muon Solenoid,Collider,Particle physics,Supersymmetry,Physics,Science,Physical sciences]
---


A May 31, 2007 file photo shows a view of the LHC (large hadron collider) in its tunnel at CERN (European particle physics laboratory) near Geneva, Switzerland. It was a good test, he said. CERN now plans to begin rebooting the machine within the next two week, allowing beams containing billions of protons travelling at 99.9 percent the speed of light to shoot through the massive ring-shaped collider. The CMS (Compact Muon Solenoid) Cavern at the European Organisation for Nuclear Research (CERN) in Meyrin, near Geneva, Switzerland, on February 10, 2015  We have to prepare our detectors in order to be ready to also look for the unexpected, so we can see whatever may exceed the standard model as we know it, he said, insisting that the LHC was at the frontier of what can be done in our field. It is high time to find a crack in the standard model, he said, pointing out that there is 95 percent of the universe that is still unknown to us.

<hr>

[Visit Link](http://phys.org/news345383666.html){:target="_blank" rel="noopener"}


