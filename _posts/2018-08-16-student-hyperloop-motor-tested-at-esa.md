---
layout: post
title: "Student Hyperloop motor tested at ESA"
date: 2018-08-16
categories:
author: ""
tags: [Hyperloop,Technology,Transport,Manufactured goods,Vehicles]
---


The team visited ESA’s Test Centre in Noordwijk, the Netherlands – normally employed for satellite testing – to see how their electric motor, battery, sensors and brakes performed while running within a vacuum chamber. Their test rig was slid inside the Centre’s 3.5m-long and 2m-wide VTC-1.5 Space Simulator chamber to be operated for around half an hour at a time. “We also want to monitor the temperature of the motor controllers as they run, as well as the performance of the carbon fibre pressure housing around our lithium polymer batteries, which are very high-density batteries but not qualified to work at low pressure.”  Delft Hyperloop team's pod The Hyperloop is a proposed method of high-speed transportation within near-vacuum tubes. Sealing the vacuum chamber The 37-strong Delft Hyperloop team submitted an initial design to SpaceX last year. “We made a lot of tests of components and systems, and have iterated time and time again.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/Student_Hyperloop_motor_tested_at_ESA){:target="_blank" rel="noopener"}


