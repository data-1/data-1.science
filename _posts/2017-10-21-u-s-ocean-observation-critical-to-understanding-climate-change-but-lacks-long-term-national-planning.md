---
layout: post
title: "U.S. ocean observation critical to understanding climate change, but lacks long-term national planning"
date: 2017-10-21
categories:
author: National Academies Of Sciences, Engineering
tags: [Ocean,Climate change,Climate,Weather,Research,Weather forecasting,Natural environment,Nature,Technology]
---


Additional benefits of this observational system include improvements in weather forecasting, marine resource management, and maritime navigation. The reports also identifies other challenges that impact sustained observations, such as the declining investment in new technological development, increasing difficulty in retaining and replenishing the human resources associated with sustained ocean observing, and a decreasing number of global and ocean-class research vessels. The report says maintenance of a capable fleet of global and ocean-class research vessels are an essential component of the U.S. effort to sustain ocean observing. In addition, philanthropic organizations have provided support for technology and capacity building initiatives that benefit ocean observing. The committee concluded that establishing an organization to enhance partnerships across sectors with an interest in ocean-observing, particularly nonprofits, philanthropic organizations, academia, U.S. federal agencies, and the commercial sector, would be an effective mechanism to increase engagement and coordination.

<hr>

[Visit Link](https://phys.org/news/2017-10-ocean-critical-climate-lacks-long-term.html){:target="_blank" rel="noopener"}


