---
layout: post
title: "Robotic sonar system inspired by bats"
date: 2015-12-10
categories:
author: Acoustical Society of America
tags: [Sonar,Animal echolocation,Acoustics,Bat,Sound,Science,Technology]
---


WASHINGTON, D.C., May 20, 2015 -- Engineers at Virginia Tech have taken the first steps toward building a novel dynamic sonar system inspired by horseshoe bats that could be more efficient and take up less space than current man-made sonar arrays. They are presenting a prototype of their dynamic biomimetic sonar at the 169th Meeting of the Acoustical Society of America held May 18-22, 2015 in Pittsburgh. Bats use biological sonar, called echolocation, to navigate and hunt, and horseshoe bats are especially skilled at using sounds to sense their environment. For comparison, modern naval sonar arrays can have receivers that measure several meters across and many hundreds of separate receiving elements for detecting incoming signals. To register, visit: http://www.aipwebcasting.com/  ABOUT THE ACOUSTICAL SOCIETY OF AMERICA  The Acoustical Society of America (ASA) is the premier international scientific society in acoustics devoted to the science and technology of sound.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/asoa-rss050815.php){:target="_blank" rel="noopener"}


