---
layout: post
title: "Important study of how climate affects biodiversity"
date: 2016-04-23
categories:
author: Uppsala University
tags: [Species,Pleistocene,Earth sciences,Natural environment,Nature]
---


Now researchers from Uppsala University can, for the first time, give us a detailed picture of natural variation through a major study published today in the leading scientific journal Current Biology. The impact of climate change on species occurrence and distribution is a central issue in the climate debate, since human influence on the climate risks posing threats to biodiversity. By studying the genetic variation of DNA molecules, they have succeeded in estimating how common these species were at various points in time, from several million years ago to historical times. During the Quaternary Period (the past two million years, including the Pleistocene epoch, i.e. up to some 11,500 years ago), inland ice periodically spread across large land areas of Earth. Rising and falling species numbers thus seem to result naturally from climate variation.

<hr>

[Visit Link](http://phys.org/news348397107.html){:target="_blank" rel="noopener"}


