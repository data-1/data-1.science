---
layout: post
title: "Using CRISPR, biologists find a way to comprehensively identify anti-cancer drug targets"
date: 2015-12-10
categories:
author: Cold Spring Harbor Laboratory
tags: [Cold Spring Harbor Laboratory,Protein,Cell (biology),ENCODE,Cancer,Genome,CRISPR,Genetics,Molecular biology,Biology,Biotechnology,Life sciences,Biochemistry]
---


That experience got us thinking, explains Vakoc, Is there a way we can look at hundreds or thousands of proteins at a time in a given cancer cell type - proteins with druggable binding pockets -- and in a single experiment find more BRD4s? More broadly, what we provide is a way to comprehensively identify specific vulnerabilities in cancer cells, across cancer types. My lab is focused on finding a small number of these binding pockets - ones whose function cancer cells are addicted to, Vakoc says. We can't tell if any particular pocket will lead to a fully effective drug; but this is a way to annotate every critical pocket in cancer cells. The paper can be obtained at: http://www.nature.com/nbt/research/index.html?articles=aop About Cold Spring Harbor Laboratory  Celebrating its 125th anniversary in 2015, Cold Spring Harbor Laboratory has shaped contemporary biomedical research and education with programs in cancer, neuroscience, plant biology and quantitative biology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/cshl-ucb050815.php){:target="_blank" rel="noopener"}


