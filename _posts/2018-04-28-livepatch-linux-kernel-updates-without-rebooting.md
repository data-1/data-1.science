---
layout: post
title: "Livepatch: Linux kernel updates without rebooting"
date: 2018-04-28
categories:
author: "Michael Boelen"
tags: [Linux kernel,Patch (computing),KGraft,Ksplice,Ubuntu,DTrace,Kpatch,Arch Linux,Computing,Technology,Software,System software,Software development,Software engineering,Computer architecture,Operating system technology,Computer engineering,Computer science,Computers,Linux,Information technology management,Free software,Linus Torvalds,Monolithic kernels,Open-source movement,Operating system families,Operating system kernels,Free content,Free system software]
---


Live kernel patching is the process of applying security patches to a running Linux kernel without the need for a system reboot. Secondly, your system needs a client tool to retrieve kernel patches and load them. Live patching of the Linux kernel  To enable live patching, we need a client to perform this duty. As this is an old kernel, we know there are some patches available. As this is an old kernel, we know there are some patches available.

<hr>

[Visit Link](https://linux-audit.com/livepatch-linux-kernel-updates-without-rebooting/){:target="_blank" rel="noopener"}


