---
layout: post
title: "Coral on a chip cracks coral mysteries"
date: 2016-03-25
categories:
author: Weizmann Institute of Science
tags: [Coral reef,Coral,Coral bleaching,American Association for the Advancement of Science,Nature,Systems ecology,Natural environment,Environmental science,Biology,Oceanography,Ecology]
---


The tiny - often less than a millimeter in diameter - animals that build coral reefs create a thin layer of living tissue surrounding the calcium-based skeleton. For the first time, the scientists were able to examine living coral polyps in the lab, under highly controlled conditions. Vardi's lab group is already in the process of adapting the coral-on-a-chip system to track the nutrient and carbon cycles of reef-building corals, as well as delving further into disease and bleaching processes. Our method can help researchers investigate everything from the coral genes that affect survival, to the strategies coral use to build reefs, to their effects on the marine carbon cycle. The Weizmann Institute of Science in Rehovot, Israel, is one of the world's top-ranking multidisciplinary research institutions.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/wios-coa031616.php){:target="_blank" rel="noopener"}


