---
layout: post
title: "Finding supports model on cause of DNA's right-handed double helix"
date: 2016-03-26
categories:
author: Tom Simons, University Of Nebraska-Lincoln
tags: [Nucleic acid double helix,DNA,Electron,Physical sciences,Physics,Science,Nature,Chemistry]
---


New research by University of Nebraska-Lincoln physicists and published in the Sept. 12 online edition of Physical Review Letters now gives support to a long-posited but never-proven hypothesis that electrons in cosmic rays—which are mostly left-handed—preferentially destroyed left-handed precursors of DNA on the primordial Earth. Joan M. Dreiling and Timothy J. Their experiment proved the principle underlying the Vester-Ulbricht hypothesis that the primarily left-handed spinning electrons in cosmic rays could have preferentially destroyed left-handed precursors of DNA, leaving only right-handed DNA. The sculpture illustrates DNA's right-handed double helix. We have done several different checks with our experiment and I am totally confident that the asymmetry exists, Dreiling said.

<hr>

[Visit Link](http://phys.org/news330241976.html){:target="_blank" rel="noopener"}


