---
layout: post
title: "Distortions in curvature of space-time"
date: 2016-03-16
categories:
author:  
tags: []
---


Just as waves spread out when a boat moves in water or a stone is thrown in a pond, gravitational waves are distortions in the curvature of space-time caused by motions of matter that propagate with the speed of light. Unlike waves that one is familiar with, such as sound and light, gravitational waves do not travel through space; the fabric of space-time itself is oscillating in response to the disturbances caused by moving masses. According to Einstein’s theory, the presence of a mass warps the space-time structure in its neighbourhood just as a ball does when placed on a rubber sheet . The curvature of the warp determines its gravitational force. So when large masses move around or rapidly accelerate, the disturbance causes distortions in the static but warped space-time.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/gravitational-waves-are-distortions-in-the-curvature-of-spacetime/article8224712.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


