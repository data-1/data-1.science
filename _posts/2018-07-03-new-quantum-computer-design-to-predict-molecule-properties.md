---
layout: post
title: "New quantum computer design to predict molecule properties"
date: 2018-07-03
categories:
author: "Erik Arends, Leiden Institute Of Physics"
tags: [Majorana fermion,Quantum computing,Matter,Quantum mechanics,Elementary particle,Fermion,Quantum chemistry,Boson,Chemistry,Physics,Theoretical physics,Science]
---


Credit: Leiden Institute of Physics  The standard approach to building a quantum computer with majoranas as building blocks is to convert them into qubits. Physicists from Leiden and Delft propose to turn majoranas directly into fermions, making computations more efficient. Efficient simulation of quantum chemistry is a task beyond the reach of classical computers, with quantum computers being a promising alternative. Trying to simulate fermions (matter) using bosons (qubits) is inefficient, because of the differences between these particle types. On the one hand, their new scheme requires using fewer majoranas to simulate the same molecule, since you only need two majoranas to make a fermion instead of four or six for a qubit.

<hr>

[Visit Link](https://phys.org/news/2018-05-quantum-molecule-properties.html){:target="_blank" rel="noopener"}


