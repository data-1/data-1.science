---
layout: post
title: "How to Record and Replay Linux Terminal Sessions using ‘script’ and ‘scriptreplay’ Commands"
date: 2015-09-26
categories:
author: Aaron Kili
tags: [Command (computing),Filename,Command-line interface,Computer file,Logging (software),Computing,Computer data,Computer architecture,Information technology,Computers,Software,Technology,Software engineering,Software development,Information technology management,System software,Digital media,Information Age,Computer science,Computer engineering]
---


In this guide we are going to look at how to use a script and scriptreplay commands in Linux that can help you to record commands and their output printed on your terminal during a given session. How to Record Linux Terminal Using script Command  The script command stores terminal activities in a log file that can be named by a user, when a name is not provided by a user, the default file name, typescript is used. [email protected] ~ $ exit Script done, file is history_log.txt  If the script can not write to the named log file then it shows an error. [email protected] ~ $ script -c 'hostname' script.log Script started, file is script.log tecmint.com Script done, file is script.log  If you want script to run in a quiet mode then you can use the -q option. When the log_file replayed using the timing information, the commands recorded are run and their output is displayed at the same time the original output was displayed while being recorded.

<hr>

[Visit Link](http://www.tecmint.com/record-and-replay-linux-terminal-session-commands-using-script/){:target="_blank" rel="noopener"}


