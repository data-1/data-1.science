---
layout: post
title: "Electrical waves travel through bioengineered tissue – Physics World"
date: 2016-07-21
categories:
author: ""
tags: [Sodium channel,Potassium channel,Sodium]
---


X)  A genetically engineered tissue that can be electrically excited by light has been developed by researchers in the US. The researchers edited the cells’ genomes so that the cells developed four ion channels – pores that can open or close to allow specific ions in or out of the cell, altering its electrical potential. Second, they added a sodium-ion channel – also present in the real heart – that opens when the potential increases. One of these channels raises a cell’s potential in response to blue light, while the other creates a voltage-dependent fluorescence in response to red light. Together, these allowed the team to trigger and image the electrical waves in the tissue culture.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2016/jul/12/electrical-waves-travel-through-bioengineered-tissue){:target="_blank" rel="noopener"}


