---
layout: post
title: "DNA analysis reveals there are no wild horses left in the world"
date: 2018-06-17
categories:
author: "Kristine Lofgren"
tags: []
---


Researchers are examining the DNA of horses – and what they’ve discovered upends everything we thought we knew. Continue reading below Our Featured Videos  Przewalski’s horses, thought to be the last truly wild horses on Earth, are actually descended from domesticated horses, which means that the last wild horses probably went extinct hundreds or thousands of years ago. It is these horses that all modern Przewalski’s horses descend from. SIGN UP  Until now, scientists believed that modern domesticated horses descended from horses bred by the Botai people. But if Przewalski’s horses come from the Botai, it means that modern domesticated horses don’t.

<hr>

[Visit Link](https://inhabitat.com/dna-analysis-reveals-there-are-no-wild-horses-left-in-the-world){:target="_blank" rel="noopener"}


