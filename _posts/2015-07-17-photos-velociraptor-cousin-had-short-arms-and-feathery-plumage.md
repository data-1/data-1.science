---
layout: post
title: "Photos: Velociraptor Cousin Had Short Arms and Feathery Plumage"
date: 2015-07-17
categories:
author: Laura Geggel
tags: [Dinosaur,Feather,Velociraptor,Zhenyuanlong,Stephen L Brusatte,Paleontology,Taxa]
---


One of Velociraptor's cousins had an armful of feathers, even if its arms were short, a new study finds. (Image credit: Junchang Lü.) Arm plumage  This photo shows a close-up of the wing feathers on Zhenyuanlong suni. Dino geeks  Steve Brusatte (left) and Junchang Lü (right), a professor at the Chinese Academy of Geological Sciences in Beijing, stand next to the fossil of the feathered dinosaur. Follow Live Science @livescience, Facebook & Google+.

<hr>

[Visit Link](http://www.livescience.com/51572-feathered-velociraptor-cousin-photos.html){:target="_blank" rel="noopener"}


