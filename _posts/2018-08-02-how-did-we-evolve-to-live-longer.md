---
layout: post
title: "How did we evolve to live longer?"
date: 2018-08-02
categories:
author: "Newcastle University"
tags: [Reactive oxygen species,Autophagy,Neurodegenerative disease,Cell biology,Biology]
---


Research shows a collection of small adaptations in stress activated proteins, accumulated over millennia of human history, could help to explain our increased natural defences and longer lifespan. The importance of protein, p62  In the study the authors were able to identify how a protein called p62 is activated to induce autophagy. This ability of p62 to sense ROS allows the cell to remove the damage and to survive this stress. These 'humanised' flies survived longer in conditions of stress. The discovery of these adaptations allows a better understanding of how we can protect against and treat age-related diseases.

<hr>

[Visit Link](https://phys.org/news/2018-01-evolve-longer.html){:target="_blank" rel="noopener"}


