---
layout: post
title: "New research shows Earth's tilt influences climate change"
date: 2015-12-18
categories:
author: Louisiana State University
tags: [Axial tilt,Earth,Milankovitch cycles,Applied and interdisciplinary physics,Earth sciences,Nature,Physical geography,Planetary science,Science,Physical sciences]
---


LSU paleoclimatologist Kristine DeLong contributed to an international research breakthrough that sheds new light on how the tilt of the Earth affects the world's heaviest rainbelt. With research collaborators at the University of Science and Technology of China and National Taiwan University, DeLong looked at sediment cores from off the coast of Papua New Guinea and stalagtite samples from ancient caves in China. DeLong's data analysis revealed obliquity in both the paleontological record and computer model data. The standard assumptions about how the variations in the Earth's orbit influences changes in climate are called Milankovitch cycles. Additionally, climate scientists have begun to recognize that rather than shifting north and south, the ITCZ expands and contracts, based on this information.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/lsu-nrs121415.php){:target="_blank" rel="noopener"}


