---
layout: post
title: "Thermally driven spin current in DNA"
date: 2018-07-01
categories:
author: "American Institute Of Physics"
tags: [Spintronics,Thermoelectric effect,Electromagnetism,Materials,Technology,Electricity,Chemistry,Materials science,Applied and interdisciplinary physics,Electrical engineering]
---


Credit: Long Bai  An emerging field that has generated a wide range of interest, spin caloritronics, is an offshoot of spintronics that explores how heat currents transport electron spin. Spin caloritronics researchers are particularly interested in how waste heat could be used to power next-generation spintronic devices. But, until now, researchers have not explored how heat bias can control the spin current in a dsDNA molecule. They discovered that their theoretical dsDNA-based device can act as a spin (charge)-Seebeck diode, switch or transistor. This DNA structure aligns electrons in one direction as the temperature gradient drives the electrons from the hotter ferromagnetic material to the cooler nonferrous metal.

<hr>

[Visit Link](https://phys.org/news/2018-03-thermally-driven-current-dna.html){:target="_blank" rel="noopener"}


