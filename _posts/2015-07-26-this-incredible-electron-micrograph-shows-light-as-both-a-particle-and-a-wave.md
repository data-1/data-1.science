---
layout: post
title: "This incredible electron micrograph shows light as both a particle and a wave"
date: 2015-07-26
categories:
author: Russell Brandom, Mar
tags: [Electron,Light,Microscope,Photon,Computing,Experiment,Waveparticle duality,Scientific theories,Atomic molecular and optical physics,Scientific method,Applied and interdisciplinary physics,Physical sciences,Theoretical physics,Physics,Science,Quantum mechanics]
---


This image was taken by turning an electron microscope on a laser-charged nanowire. Light moved down the wire in both directions, creating a standing wave of photons, which the electron microscope was able to capture by focusing on the wire. The result is an image that captures both the wave and particle nature of light. The experiment was possible in large part because of the increasing speed and precision of electron microscopes, giving a new window into quantum behaviors. The research could result in new insights in a range of fields, but it's particularly relevant to scientists looking to isolate and tame quantum particles for quantum computing projects, which both Google and Microsoft have pursued.

<hr>

[Visit Link](http://www.theverge.com/2015/3/2/8133819/light-particle-wave-quantum-experiment){:target="_blank" rel="noopener"}


