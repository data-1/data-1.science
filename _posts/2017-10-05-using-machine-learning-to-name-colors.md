---
layout: post
title: "Using machine learning to name colors"
date: 2017-10-05
categories:
author: "Michael A. Alcorn"
tags: [Word embedding,Language,Artificial neural network,RGB color model,Color,Convolutional neural network,Cognitive science,Technology,Cognition,Computing,Cybernetics]
---


A lot of the color names were nonsensical (i.e., not actual words), and the pairings between the generated names and the colors seemed pretty random, so it wasn't clear whether the model was actually learning a meaningful function to map colors to language. I wondered whether the idea could be improved, so, exclusively using open source software, I built a model to play around with; the code for the project can be found on my GitHub. Using these word embeddings, I built two different models: one that maps a name to a color, and one that maps a color to a name. For example, when providing RGB values of 161, 85, and 130 (a fuchsia-like color) as input to the model, it generates the following:  opensource.com  Word #1 candidates  roses_hydrangeas  lacy_foliage  tussie  sweetly_fragrant  coppery_pink  grevillea  oriental_poppies  maidenhair_ferns  oak_leaf_hydrangea  blue_hydrangeas  Word #2 candidates  violet  lavender_flowers  wonderfully_fragrant  fuchsia  anise_hyssop  monarda  purple  purple_blooms  cornflowers  hibiscuses  Word #3 candidates  purple  pink  violet  fuchsia  roses  purple_blooms  lavender_flowers  lavender  Asiatic_lilies  monarda  Word #4 candidates  roses  purple  pink  red  violet  fuchsia  violets  Asiatic_lilies  purple_blooms  flowers  I personally like sweetly fragrant fuchsia. Anyway, it seems like the word embeddings allow the model to learn pretty interesting relationships between colors and language.

<hr>

[Visit Link](https://opensource.com/article/17/9/color-naming-word-embeddings){:target="_blank" rel="noopener"}


