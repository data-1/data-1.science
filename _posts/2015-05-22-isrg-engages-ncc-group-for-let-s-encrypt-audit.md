---
layout: post
title: "ISRG Engages NCC Group for Let's Encrypt Audit"
date: 2015-05-22
categories:
author: ""    
tags: [Lets Encrypt,Cybercrime,Secure communication,Computer security,Cryptography,Cyberwarfare,Computing,Cyberspace,Security engineering,Internet,Information Age,Espionage techniques,Internet architecture,Telecommunications,Digital rights,Application layer protocols,Internet protocols,Computer networking,Security technology,Internet Standards,Technology,Military communications,Secrecy,Crime prevention,Transport Layer Security,Computer standards,Networking standards,Public key infrastructure,Computational trust,Network architecture,Computer network security,Network protocols,Protocols,Computer science,Information technology,Computer security standards,Internet security,Cryptographic protocols,Communications protocols]
---


ISRG has engaged the NCC Group Crypto Services team to perform a security review of Let’s Encrypt’s certificate authority software, boulder, and the ACME protocol. NCC Group’s team was selected due to their strong reputation for cryptography expertise, which brought together Matasano Security, iSEC Partners, and Intrepidus Group. The NCC Group audit will take place prior to the general availability of Let’s Encrypt’s service, and is intended to provide additional assurance that our systems are secure. “I’m very much looking forward to the general availability of Let’s Encrypt - lowering the bar both technically and financially for people to deploy TLS will result in a more encrypted Internet that helps increase security and preserve people’s privacy,” said Tom Ritter, Practice Director, NCC Group. It’s largely written in the go language and makes use of CloudFlare’s CFSSL tools, which are also written in go.

<hr>

[Visit Link](https://letsencrypt.org//2015/04/14/ncc-group-audit.html){:target="_blank" rel="noopener"}


