---
layout: post
title: "Two Large Hadron Collider experiments first to observe rare subatomic process"
date: 2016-05-02
categories:
author: DOE/Fermi National Accelerator Laboratory
tags: [LHCb experiment,Particle physics,Matter,Large Hadron Collider,Standard Model,Compact Muon Solenoid,Fermilab,Subatomic particle,Quark,CERN,Universe,Hadron,Collider,Antimatter,Physics,Physical sciences,Nature,Nuclear physics,Theoretical physics,Science]
---


LHCb and CMS both study the properties of particles to search for cracks in the Standard Model, our best description so far of the behavior of all directly observable matter in the universe. Many theories that propose to extend the Standard Model also predict an increase in this Bs decay rate, said Fermilab's Joel Butler of the CMS experiment. ###  The U.S. Department of Energy Office of Science provides funding for the U.S. contributions to the CMS experiment. Together, the CMS and LHCb collaborations include more than 4,500 scientists from more than 250 institutions in 44 countries. Fermilab is America's premier national laboratory for particle physics and accelerator research.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/dnal-tlh051315.php){:target="_blank" rel="noopener"}


