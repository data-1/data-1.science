---
layout: post
title: "How El Niño impacts global temperatures"
date: 2016-06-12
categories:
author: "Australian National University"
tags: [El NioSouthern Oscillation,Meteorology,Hydrology,Climate change,Hydrography,Climatology,Atmosphere,Earth sciences,Physical geography,Climate,Natural environment,Earth phenomena,Applied and interdisciplinary physics,Oceanography,Nature,Climate variability and change,Atmospheric sciences]
---


El Niño oscillations in the Pacific Ocean may have amplified global climate fluctuations for hundreds of years at a time  Scientists have found past El Niño oscillations in the Pacific Ocean may have amplified global climate fluctuations for hundreds of years at a time. The team uncovered century-scale patterns in Pacific rainfall and temperature, and linked them with global climate changes in the past 2000 years. For example, northern hemisphere warming and droughts between the years 950 and 1250 corresponded to an El Niño-like state in the Pacific, which switched to a La Niña-like pattern during a cold period between 1350 and 1900. Our work is a significant piece in the grand puzzle. The international team of scientists was led by Dr Michael Griffiths of William Patterson University in New Jersey, along with PhD candidate Alena Kimbrough and Dr Michael Gagan at the ANU, Professor Wahyoe Hantoro of the Indonesian Institute of Sciences and colleagues at the University of Melbourne and the University of Arizona.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/anu-hen060916.php){:target="_blank" rel="noopener"}


