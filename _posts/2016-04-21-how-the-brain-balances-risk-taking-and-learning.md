---
layout: post
title: "How the brain balances risk-taking and learning"
date: 2016-04-21
categories:
author: Salk Institute
tags: [Dopamine,Memory,Learning,Neuroscience,Neuron,Salk Institute for Biological Studies,American Association for the Advancement of Science,Nervous system,Behavioural sciences,Cognitive science]
---


Now, Salk scientists have discovered the basis for how animals balance learning and risk-taking behavior to get to a more predictable environment. The research reveals new details on the function of two chemical signals critical to human behavior: dopamine--responsible for reward and risk-taking--and CREB--needed for learning. With this new work, scientists now have a fundamental model of how dopamine signaling leads the worm to take more risks and explore new environments. Using genetics, imaging, behavioral analysis and other techniques, researchers found that when worms are on small patches, the two pairs of high-threshold neurons respond to the greater variation and signal leading to increased dopamine. About the Salk Institute for Biological Studies:  The Salk Institute for Biological Studies is one of the world's preeminent basic research institutions, where internationally renowned faculty probes fundamental life science questions in a unique, collaborative, and creative environment.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/si-htb040315.php){:target="_blank" rel="noopener"}


