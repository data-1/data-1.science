---
layout: post
title: "NASA's PACE mission will uncover new information about health of our oceans"
date: 2016-07-21
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Ocean color,Phytoplankton,Algal bloom,Ocean,Hydrology,Environmental science,Applied and interdisciplinary physics,Earth sciences,Nature,Oceanography,Natural environment,Hydrography,Physical geography]
---


PACE will use a wide spectrum of wavelengths from an ocean color instrument to provide scientists with this information. We are going to go beyond just seeing that Earth's climate is changing to better understanding why the change is occurring. The primary instrument for this mission is named the Ocean Color Instrument (OCI), which will collect hyperspectral measurements from the ultraviolet to the shortwave infrared--a range that is broader than its predecessor satellite instruments, SeaWiFS, MODIS, and VIIRS -- to examine and monitor how phytoplankton communities in the ocean are changing in space and time. They are the base of the marine food chain and, like land plants, produce much of the oxygen we breathe and play a role in reducing atmospheric carbon dioxide levels. The unique information that this mission will provide, in combination with climate models, will allow for scientists to monitor the health of our oceans and their response to climate change like never before.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/nsfc-npm072016.php){:target="_blank" rel="noopener"}


