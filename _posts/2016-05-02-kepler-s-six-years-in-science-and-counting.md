---
layout: post
title: "Kepler's six years in science (and counting)"
date: 2016-05-02
categories:
author: Whitney Clavin, Jet Propulsion Laboratory
tags: [Kepler space telescope,Exoplanet,Methods of detecting exoplanets,Outer space,Planetary science,Space science,Astronomy,Stellar astronomy,Physical sciences,Exoplanetology,Astronomical objects,Science]
---


NASA's Kepler: Six Years of Science. Credit: NASA/JPL-Caltech  NASA's Kepler spacecraft began hunting for planets outside our solar system on May 12, 2009. Its mission was to survey a portion of our galaxy to determine what fraction of stars might harbor potentially habitable, Earth-sized exoplanets, or planets that orbit other stars. From these dimmings, or transits, and information about the parent star, researchers can determine a planet's size (radius), the time it takes to orbit its star and the amount of energy received from the host star. The spacecraft continues to collect data in its new mission.

<hr>

[Visit Link](http://phys.org/news350717883.html){:target="_blank" rel="noopener"}


