---
layout: post
title: "Physicists create a magnetic wormhole in the lab – Physics World"
date: 2016-05-28
categories:
author: ""
tags: [Magnetism,Superconductivity,Wormhole,Space,Magnetic field,Cloaking device,Spacetime,Transformation optics,Physical sciences,Applied and interdisciplinary physics,Electrical engineering,Science,Electromagnetism,Physics]
---


The team has built a spherical wormhole, made from ferromagnetic and superconducting components, that is magnetically cloaked. The superconductor repels external magnetic fields, thereby magnetically isolating the hose within; while the array compensates for the fact that the superconductor distorts any external field as it repels it. Sanchez points out that the new device is not the world’s first magnetic cloak, but he believes it is the first such cloak to work in three dimensions, rather than two. “Our main motivation was scientific,” says Sanchez, “but since magnetic fields are used in so many different things, our device could have many potential applications.”  You see the apparatus with your eyes, but magnetically it is undetectable. It is like the field lines have gone through another spatial dimension  Alvaro Sanchez  Tie Jun Cui, an electrical engineer at Southeast University in China who was not involved in the work, agrees with Sanchez, and believes that it might now be possible to make analogous devices for sound or heat, for example.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/aug/20/physicists-create-a-magnetic-wormhole-in-the-lab){:target="_blank" rel="noopener"}


