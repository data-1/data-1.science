---
layout: post
title: "Open Source Skills Soar In Demand According to 2018 Jobs Report"
date: 2018-06-28
categories:
author: "The Linux Foundation"
tags: [Open source,Linux,Linux Foundation,Employment,Cloud computing,Business,Economy]
---


Container technology is rapidly growing in popularity and importance, with 57% of hiring managers seeking those skills, up from 27% last year. Hiring open source talent is a priority for 83% of hiring managers, up from 76% in 2017. Career Building  In terms of job seeking and job hiring, the report shows high demand for open source skills and a strong career benefit from open source experience. Hiring managers say they are specifically looking to recruit in the following areas:  Diversity Diversity  This year’s survey included optional questions about companies’ initiatives to increase diversity in open source hiring, which has become a hot topic throughout the tech industry. Overall, the 2018 Open Source Jobs Report indicates a strong market for open source talent, driven in part by the growth of cloud-based technologies.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/open-source-skills-soar-in-demand-according-to-2018-jobs-report/){:target="_blank" rel="noopener"}


