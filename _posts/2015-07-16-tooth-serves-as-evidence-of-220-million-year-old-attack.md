---
layout: post
title: "Tooth serves as evidence of 220-million-year-old attack"
date: 2015-07-16
categories:
author: University of Tennessee at Knoxville 
tags: [Phytosaur,Rauisuchia,Predation,Tooth,Reptile]
---


Stephanie Drumheller, an earth and planetary sciences lecturer, and her Virginia Tech colleagues Michelle Stocker and Sterling Nesbitt examined 220-million-year-old bite marks in the thigh bones of an old reptile and found evidence that two predators at the top of their respective food chains interacted—with the smaller potentially having eaten the larger animal. A tooth of a semi-aquatic phytosaur lodged in the thigh bone of a terrestrial rauisuchid. To find a phytosaur tooth in the bone of a rauisuchid is very surprising. This, along with an examination of the bite marks, revealed a story of multiple struggles. The team found tissue surrounding bite marks illustrating that the rauisuchid was attacked twice and survived.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/uota-tsa092914.php){:target="_blank" rel="noopener"}


