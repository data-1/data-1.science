---
layout: post
title: "What is Mars made of?"
date: 2016-04-14
categories:
author: Matt Williams
tags: [Mars,Earth,Planetary core,Impact event,Planet,North Polar Basin (Mars),Plate tectonics,Crust (geology),Astronomical objects,Nature,Solar System,Planetary science,Space science,Outer space,Bodies of the Solar System,Terrestrial planets,Planets of the Solar System,Planets,Astronomical objects known since antiquity,Astronomy,Physical sciences]
---


The interior of Mars, showing a molten liquid iron core similar to Earth and Venus. As a result, the planet lacks a magnetic field and is constantly bombarded by radiation. Credit: NASA/Mars Exploration  Despite there being no magnetic field at present, there is evidence that Mars had a magnetic field at one time. By contrast, Earth's crust averages 40 km (25 mi) and is only one third as thick as Mars's, relative to the sizes of the two planets. Credit: NASA/JPL/USGS  Though not yet confirmed to be an impact event, the current theory is that this basin was created when a Pluto-sized body collided with Mars about four billion years ago.

<hr>

[Visit Link](http://phys.org/news344165345.html){:target="_blank" rel="noopener"}


