---
layout: post
title: "Researchers find red fluorescence in fish is for vision not UV protection"
date: 2015-10-03
categories:
author: Bob Yirka
tags: [Fluorescence,Ultraviolet,Optics,Electromagnetic radiation]
---


Credit: Nico Michiels, University of Tübingen  (Phys.org) —A team of researchers with members from Germany and Egypt has found that red fluorescence as found in many species of fish appears to be for visual purposes, rather than as a UV protection mechanism. Thus, without fluorescence, red fish would not appear red in deep water. But why do some fish have red fluorescence at all? All of the specimens were taken to a lab where they were tested to see how strong they fluoresce—the thinking was that if fluorescence was for UV protection, then those that lived in deeper waters would fluoresce less as they wouldn't need as much protection. In analyzing their results, the researchers found the opposite—fish that lived in deeper waters fluoresced stronger than those that lived in shallow waters, indicating that fluorescence, at least for them, was not tied to UV protection.

<hr>

[Visit Link](http://phys.org/news324711414.html){:target="_blank" rel="noopener"}


