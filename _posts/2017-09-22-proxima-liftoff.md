---
layout: post
title: "Proxima Liftoff"
date: 2017-09-22
categories:
author: ""
tags: [Space agencies,Human spaceflight,Flight,Space programs,Outer space,Astronautics,Spaceflight,Space exploration,Life in space,Spacecraft,Space science,Space research,Scientific exploration,Space vehicles,Aerospace,Spaceflight technology,Space industry,Space-based economy,Astronomy,Crewed spacecraft,Space program of Russia]
---


Thomas, Peggy and Oleg will spend six months in space working and living on the International Space Station. The Proxima mission is the ninth long-duration mission for an ESA astronaut. It is named after the closest star to the Sun, continuing a tradition of naming missions with French astronauts after stars and constellations. Follow Thomas and his mission via thomaspesquet.esa.int and go to the mission blog for updates. Proxima launch – full replay  Proxima docking  Proxima Hatch opening

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2016/11/Proxima_Liftoff){:target="_blank" rel="noopener"}


