---
layout: post
title: "Cells adapt ultra-rapidly to zero gravity"
date: 2017-10-08
categories:
author: "University Of Zurich"
tags: [Weightlessness,International Space Station,Science]
---


Previously, many experiments exhibited cell changes – after hours or even days in zero gravity. Based on real-time readings on the ISS, UZH scientists can now reveal that cells are able to respond to changes in gravitational conditions extremely quickly and keep on functioning. For Ullrich and Thiel, the direct evidence of a rapid and complete adaptation to zero gravity in less than a minute begs the question as to whether previous cell changes measured after hours or days were also the result of an adaptation process. Therefore, the results raise more questions regarding the robustness of life and its astonishing adaptability. Explore further Testing immune cells on the International Space Station

<hr>

[Visit Link](https://phys.org/news/2017-02-cells-ultra-rapidly-gravity.html){:target="_blank" rel="noopener"}


