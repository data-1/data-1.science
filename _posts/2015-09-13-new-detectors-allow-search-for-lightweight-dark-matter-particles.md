---
layout: post
title: "New detectors allow search for lightweight dark matter particles"
date: 2015-09-13
categories:
author: Technical University Munich
tags: [Dark matter,Cryogenic Rare Event Search with Superconducting Thermometers,Weakly interacting massive particles,Matter,Electronvolt,Cosmic ray,Science,Particle physics,Nature,Physics,Physical sciences]
---


By carefully re-engineering a detector for heavy dark matter particles, so-called WIMPS, the CRESST collaboration succeeded in improving the detector to measure lightweight dark matter particles with masses in the range of a proton mass. Theoretical models and astrophysical observations leave hardly any doubt that dark matter exists: Its share is five times more than all visible material. So far a likely candidate for the dark matter particle was thought to be a heavy particle, the so-called WIMP, explains Dr. Federica Petricca, a researcher at the Max Planck Institute for Physics and spokesperson of the CRESST experiment (Cryogenic Rare Event Search with Superconducting Thermometers). Measurement record for light particles of dark matter  Now CRESST has achieved an important step toward tracking down these potential lightweights: In a long-term experiment with one detector, the researchers achieved an energy threshold of 307 eV. When a particle hits one of the three crystal atoms (calcium, tungsten, and oxygen), the detectors simultaneously measure energy and light signals from the collision that deliver information about the nature of the impinging particle.

<hr>

[Visit Link](http://phys.org/news/2015-09-detectors-lightweight-dark-particles.html){:target="_blank" rel="noopener"}


