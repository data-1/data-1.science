---
layout: post
title: "​Supercomputers: All Linux, all the time"
date: 2018-06-28
categories:
author: "Steven Vaughan-Nichols, Senior Contributing Editor, June"
tags: []
---


Every last one is running Linux. That was the first time all of the TOP500 machines were running Linux. For example, the new supercomputing champ Summit is powered by boards running with two IBM Power9 CPUs and six V100 GPUs. According to NVIDIA, 95 percent of the Summit's peak performance (187.7 petaflops) is derived from the system's 27,686 GPUs. NVIDIA ran the numbers for the less powerful, and somewhat less GPU-intensive Sierra, which now ranks as the third fastest supercomputer in the world at 71.6 Linpack petaflops.

<hr>

[Visit Link](https://www.zdnet.com/article/supercomputers-all-linux-all-the-time/#ftag=RSSbaffb68){:target="_blank" rel="noopener"}


