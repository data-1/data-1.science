---
layout: post
title: "Scientists enlist supercomputers, machine learning to automatically identify brain tumors"
date: 2017-10-06
categories:
author: "University of Texas at Austin, Texas Advanced Computing Center"
tags: [Texas Advanced Computing Center,Machine learning,Radiology,Image segmentation,Supercomputer,Voxel,Research,K-nearest neighbors algorithm,Statistical classification,Medical imaging,Algorithm,Technology,Branches of science,Computing,Science,Computer science,Applied mathematics]
---


Biros' team tested their new method in the Multimodal Brain Tumor Segmentation Challenge 2017 (BRaTS'17), an annual competition where research groups from around the world present methods and results for computer-aided identification and classification of brain tumors, as well as different types of cancerous regions, using pre-operative MR scans. TRAINING AND TESTING THE PREDICTION PIPELINE  For the challenge, Biros and his team of more than a dozen students and researchers, were provided in advance with 300 sets of brain images on which all teams calibrated their methods (what is called training in machine learning parlance). In the final part of the challenge, groups were given data from 140 patients and had to identify the location of tumors and segment them into different tissue types over the course of just two days. Most teams had to limit the amount of training data they used or apply more simplified classifier algorithms on the whole training set, but priority access to TACC's ecosystem of supercomputers meant Biros' team could explore more complex methods. But this was the first time we put everything together for an application to make our method work for a really challenging problem.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uota-ses100517.php){:target="_blank" rel="noopener"}


