---
layout: post
title: "Major breakthrough identifies new mechanism for the development of schizophrenia"
date: 2017-10-11
categories:
author: "Trinity College Dublin"
tags: [Schizophrenia,News aggregator,Human diseases and disorders,Epidemiology,Causes of death,Medical specialties,Medicine,Health sciences,Diseases and disorders,Health,Clinical medicine]
---


Scientists from Trinity College Dublin and the Royal College of Surgeons in Ireland (RCSI) have discovered that abnormalities of blood vessels in the brain may play a major role in the development of schizophrenia, a debilitating condition that affects around 1% of people in Ireland. Scientists working in the Smurfit Institute of Genetics at Trinity College Dublin and the Department of Psychiatry, RCSI, have discovered that abnormalities in the integrity of the BBB may be a critical component in the development of schizophrenia and other brain disorders. A gene termed Claudin-5 is located within this region, and it is changes in the levels of this component of the BBB that are associated with the presence of schizophrenia. Assistant Professor in Neurovascular Genetics at Trinity, Dr Matthew Campbell, said: Our recent findings have, for the first time, suggested that schizophrenia is a brain disorder associated with abnormalities of brain blood vessels. The concept of tailoring drugs to regulate and treat abnormal brain blood vessels is a novel treatment strategy and offers great potential to complement existing treatments of this debilitating disease.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171010105629.htm){:target="_blank" rel="noopener"}


