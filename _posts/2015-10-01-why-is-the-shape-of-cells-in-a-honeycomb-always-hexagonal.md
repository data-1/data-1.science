---
layout: post
title: "Why is the shape of cells in a honeycomb always hexagonal?"
date: 2015-10-01
categories:
author: "$author"  
tags: []
---


Sinivasa, Bengaluru  In fact the bees simply make cells made of wax that are circular in cross-section. These circular cells are packed together like a layer of bubbles. The wax is softened by the heat of the bees’ bodies and then gets pulled into hexagonal cells by surface tension at the junctions where three walls meet. A regular geometric array of identical cells with simple polygonal cross-sections can take only one of three forms: triangular, square or hexagonal. When first made, the comb cells of the honeybee ( Apis mellifera ) are circular, but after two days they look more hexagonal.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/why-is-the-shape-of-cells-in-a-honeycomb-always-hexagonal/article7692306.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


