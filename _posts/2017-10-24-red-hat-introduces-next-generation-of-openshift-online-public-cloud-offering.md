---
layout: post
title: "Red Hat Introduces Next Generation of OpenShift Online Public Cloud Offering"
date: 2017-10-24
categories:
author:  
tags: [OpenShift,Red Hat,Cloud computing,Software as a service,Information technology management,Digital media,Software architecture,Systems engineering,Information Age,Computers,Information technology,Computer engineering,Computer science,Software development,Software engineering,Software,Technology,Computing]
---


Based on the same Linux container- and Kubernetes-based foundation as the award-winning Red Hat OpenShift Container Platform, Red Hat OpenShift Online gives developers the ability to more quickly and easily build, deploy and scale cloud-native applications in a public cloud environment. Middleware services – Red Hat OpenShift Application Services provide the powerful capabilities of products in the Red Hat JBoss Middleware portfolio as cloud-based services on OpenShift. These services can be used by developers to build applications, integrate with other systems, orchestrate using rules and processes, and then deploy across hybrid environments. Pro: A paid service that adds additional resources for $25 per month per gigabyte of memory or storage. Red Hat OpenShift Online enables organizations to accelerate the path to production for their container-based applications by giving developers the ability to quickly begin new development projects and easily bring them into production in the enterprise.”  Al Gillen, group vice president, Software Development and Open Source, IDC  “Cloud-native application development is the key to unlocking digital transformation.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/red-hat-introduces-next-generation-openshift-online-public-cloud-offering){:target="_blank" rel="noopener"}


