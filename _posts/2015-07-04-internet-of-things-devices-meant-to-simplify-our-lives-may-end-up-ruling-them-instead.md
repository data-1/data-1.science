---
layout: post
title: "Internet of things devices meant to simplify our lives may end up ruling them instead"
date: 2015-07-04
categories:
author: Andy Tattersall, The Conversation
tags: [Internet of things,Internet,Technology,Privacy,Patch (computing),Computing]
---


Certainly inventions of the past century such as the washing machine and combustion engine have brought leisure time to the masses. Technology growth in the workplace can lead to loss of productivity; taken to the home it could take a bite out of leisure time too. Once you bought a television, turned it on and it entertained you. But the problems that affect anything that's computerised and internet-connected re-appear: patches, updates, backups and security. Perhaps we can realistically only manage so many devices and accounts before it gets too much.

<hr>

[Visit Link](http://phys.org/news347698759.html){:target="_blank" rel="noopener"}


