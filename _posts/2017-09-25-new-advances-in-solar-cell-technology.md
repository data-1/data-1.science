---
layout: post
title: "New advances in solar cell technology"
date: 2017-09-25
categories:
author: "Okinawa Institute Of Science"
tags: [Perovskite solar cell,Solar cell,Solar energy,Energy development,Nature,Technology,Chemistry,Materials,Energy]
---


Recent publications from Prof. Qi's lab cover three different areas of innovation in perovskite film research: a novel post annealing treatment to increase perovskite efficiency and stability, a discovery of the decomposition products of a specific perovskite, and a new means of producing perovskites that maintains solar efficiency when scaled up. Dr. Jiang's novel post annealing treatment produced solar cells that had fused grain boundaries, reduced charge recombination, and displayed an outstanding conversion efficiency of 18.4%. Previous research on MAPbI3 perovskite films led to the conclusion that the gas products of thermal degradation of this material were methylamine (CH3NH2) and hydrogen iodide (HI). His work uses chemical vapor deposition, a cost-effective process commonly used in industry, to create large solar cells and modules of FAPbI3 perovskites. These solar modules show enhanced thermal stability and relatively high efficiencies, which is impressive as many perovskite solar cells lose efficiency drastically as they are scaled up, making this type of research useful for commercial purposes.

<hr>

[Visit Link](http://phys.org/news/2016-10-advances-solar-cell-technology.html){:target="_blank" rel="noopener"}


