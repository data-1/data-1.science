---
layout: post
title: "A timeline of comet probe's 11-year journey"
date: 2016-05-08
categories:
author:  
tags: [Rosetta (spacecraft),Philae (spacecraft),Lander (spacecraft),Local Interstellar Cloud,Space vehicles,Space exploration,Discovery and exploration of the Solar System,Space probes,Spacecraft,Planetary science,Flight,Bodies of the Solar System,Astronautics,Solar System,Spaceflight,Space science,Outer space,Astronomy]
---


The European Space Agency said Sunday that its comet lander Philae has woken up from hibernation and managed to send data back to Earth for the first time in seven months. Feb. 25, 2007: Rosetta carries out a close flyby of Mars. The spacecraft loses its radio signal for 90 minutes as planned during the flyby of the Steins asteroid, also known as Asteroid 2867. July 10, 2010: Between Mars and Jupiter, Rosetta transmits its first pictures from the largest asteroid ever visited by a satellite after it flies by Lutetia as close as 1,900 miles (3,200 kilometers). June 13, 2015: Philae communicates with Earth for the first time in seven months in a sign that it has come out of hibernation.

<hr>

[Visit Link](http://phys.org/news353513079.html){:target="_blank" rel="noopener"}


