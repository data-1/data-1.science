---
layout: post
title: "New Antarctic rift data has implications for volcanic evolution"
date: 2018-08-22
categories:
author: "American Associates, Ben-Gurion University Of The Negev"
tags: [Plate tectonics,Antarctica,Structure of the Earth,Earth sciences,Geophysics,Lithosphere,Geology]
---


Ben-Gurion University of the Negev researchers recorded new data during two excursions on the L'Astrolabe French icebreaker that revealed Antarctica's two tectonic plates fused into one around 11 million years ago, roughly 15 million years later than previously assumed. Credit: BGU  New data revealing two tectonic plates fused to form a single Antarctic Plate 15 million years later than originally predicted and this extra motion has major implications for understanding of the tectono-volcanic activity surrounding the Pacific Ocean, from the Alpine mountains in New Zealand to the California geological setting, according to research from Ben-Gurion University of the Negev (BGU). In a study published in Nature Communications, Dr. Roi Granot of BGU's Department of Geological and Environmental Sciences, and Dr. Jérôme Dyment from the Institut de Physique du Globe de Paris, France, present marine magnetic data collected near the northern edge of the West Antarctic rift system that shows motion between East and West Antarctica, which was assumed to have ended abruptly 26 million years ago, actually continued for another 15 million years. Since Antarctica tectonically connects the Pacific Plate to the rest of the world, these results have important ramifications for understanding the tectonic evolution around the Pacific Ocean—the rise of New Zealand's Alpine Mountains, motions along the San Andreas Fault in California, and more, says Dr. Granot. According to the researchers, one of the key unanswered question was: How did the plates drift relative to each other over the last 26 million years and when did the rift stop being active?

<hr>

[Visit Link](https://phys.org/news/2018-08-antarctic-rift-implications-volcanic-evolution.html){:target="_blank" rel="noopener"}


