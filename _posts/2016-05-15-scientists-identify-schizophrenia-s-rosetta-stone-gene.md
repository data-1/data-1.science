---
layout: post
title: "Scientists identify schizophrenia's 'Rosetta Stone' gene"
date: 2016-05-15
categories:
author: Cardiff University
tags: [DISC1,Schizophrenia,Brain,Health sciences,Health,Neuroscience]
---


The breakthrough has revealed a vulnerable period in the early stages of the brain's development that researchers hope can be targeted for future efforts in reversing schizophrenia. The gene is known as 'disrupted in schizophrenia-1' (DISC-1). However, the researchers were able to pinpoint a seven-day window early on in the brain's development - one week after birth - where failure to bind had an irreversible effect on the brain's plasticity later on in life. We have identified a critical period during brain development that directs us to test whether other schizophrenia risk genes affecting different regions of the brain create their malfunction during their own critical period. Cardiff University  Cardiff University is recognised in independent government assessments as one of Britain's leading teaching and research universities and is a member of the Russell Group of the UK's most research intensive universities.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/cu-sis072315.php){:target="_blank" rel="noopener"}


