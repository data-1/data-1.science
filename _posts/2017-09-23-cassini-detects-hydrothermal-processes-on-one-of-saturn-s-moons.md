---
layout: post
title: "Cassini detects hydrothermal processes on one of Saturn's moons"
date: 2017-09-23
categories:
author: "American Association for the Advancement of Science (AAAS)"
tags: [American Association for the Advancement of Science,CassiniHuygens,Saturn,Solar System,Planemos,Outer planets,Gas giants,Moons,Physical sciences,Planets,Astronomical objects,Planets of the Solar System,Outer space,Bodies of the Solar System,Space science,Planetary science,Astronomy,Astronomical objects known since antiquity,Science]
---


In 2015, during the Cassini spacecraft's deepest-ever dive into the plume of spray that emanates from cracks in the south polar region of the ice-covered Saturnian moon Enceladus, instruments detected the presence of molecular hydrogen in the plume vapor. The results are reported by Hunter Waite and colleagues, who go on to demonstrate that the only plausible source of this hydrogen is hydrothermal reactions between hot rocks and water in the ocean beneath the moon's icy surface. (On Earth, the same process provides energy for whole ecosystems around hydrothermal vents.) During further analyses, Waite and his team infer the concentrations of volatile species in Enceladus' sub-surface ocean from the plume abundances. The researchers suggest that the vapor and particle material Cassini flew through contained up to 1.4 volume percent molecular hydrogen, and up to 0.8 volume percent carbon dioxide - critical ingredients for a process known as methanogenesis, a reaction that sustains microbes in deep, dark undersea environments on Earth.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/aaft-cdh041017.php){:target="_blank" rel="noopener"}


