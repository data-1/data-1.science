---
layout: post
title: "A matter of gravity—understanding how plants grow in space"
date: 2018-07-04
categories:
author: "Tom Rickey, Environmental Molecular Sciences Laboratory"
tags: [Plant,International Space Station,NASA,Natural environment,Micro-g environment,Nature]
---


of supplies was a handful of seeds designed to open new windows into our knowledge of how plants grow in space – information that could lead to growing fresh food in space for people aboard the space station or producing biofuel on our own planet. To do that, we need to understand how plants grow in space. Seeds and growth medium in preparation to be sent to the International Space Station. Those plants were part of recent studies in the Lewis lab that included EMSL scientist Kim Hixson, who received her Ph.D. in the Lewis lab just last month. Explore further Zero gravity plant growth experiments delivered to space station

<hr>

[Visit Link](https://phys.org/news/2018-07-gravityunderstanding-space.html){:target="_blank" rel="noopener"}


