---
layout: post
title: "NASA’s Jet Propulsion Laboratory Powers Planetary Exploration with Red Hat OpenStack Platform"
date: 2016-04-26
categories:
author:  
tags: [Cloud computing,OpenStack,Jet Propulsion Laboratory,Red Hat,Information technology,Computing,Technology]
---


JPL engineers built an on-site OpenStack cloud for the large and flexible cloud computing capacity they could offer mission scientists and engineers, planning to move critical compute activities that needed to be on-site into a more efficient private cloud architecture. Red Hat is an OpenStack leader - both in contributions to the upstream OpenStack community and its work to deliver a production-ready OpenStack platform to enterprise customers. Red Hat OpenStack Platform, a highly scalable, production-ready Infrastructure-as-a-Service (IaaS) solution, has emerged as an open source cloud platform of choice for a growing number of global organizations. Co-engineered with Red Hat Enterprise Linux and backed by Red Hat's support lifecycle, Red Hat OpenStack Platform offers an open foundation for cloud deployments. Supporting Quote  Radhesh Balakrishnan, General Manager, OpenStack, Red Hat  “NASA JPL is at the forefront of technology-powered innovation and we're excited about the computing capacity needed for their exploratory missions being powered by Red Hat OpenStack Platform.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/nasa%E2%80%99s-jet-propulsion-laboratory-powers-planetary-exploration-red-hat-openstack-platform){:target="_blank" rel="noopener"}


