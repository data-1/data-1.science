---
layout: post
title: "Forest losses increase local temperatures"
date: 2016-02-05
categories:
author: European Commission Joint Research Centre
tags: [Climate,Deforestation,Forest,Evapotranspiration,Carbon cycle,Earth phenomena,Climate variability and change,Environmental issues,Environmental science,Climate change,Natural resources,Environment,Applied and interdisciplinary physics,Systems ecology,Global environmental issues,Earth sciences,Natural environment,Physical geography,Nature]
---


These effects are most obvious in arid zones, followed by temperate, tropical and boreal zones. Forests affect climate in two ways: by absorbing carbon dioxide and storing large carbon pools in tree biomass and forest soils, and by modulating the biophysical surface properties and affecting the land-atmosphere fluxes of energy. This research focuses on the biophysical effects and is based on joint Earth observations of global changes in forest cover and of surface temperature performed during the decade 2003-2012. Results show the key role of evapotranspiration (the sum of evaporation and plant transpiration from the Earth's land surface to the atmosphere) on the biophysical impacts of forest on local climate. Forest clearing produces a marked increase of mean annual maximum air surface temperatures, slight changes in minimum temperatures and an overall increase of mean temperatures.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/ecjr-fli020516.php){:target="_blank" rel="noopener"}


