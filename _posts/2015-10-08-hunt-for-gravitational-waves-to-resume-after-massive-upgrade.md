---
layout: post
title: "Hunt for gravitational waves to resume after massive upgrade"
date: 2015-10-08
categories:
author: Castelvecchi, Davide Castelvecchi, You Can Also Search For This Author In
tags: []
---


LIGO experiment now has better chance of detecting ripples in space-time. Einstein predicted that accelerating masses such as colliding neutron stars or black holes would disturb that fabric and produce gravitational ripples that propagate through the Universe. Advanced LIGO is already three times more sensitive than its predecessor, but in three months’ time it will shut down for more improvements that will make it ten times more sensitive. When it reopens around 9 months later, it should be able to spot cosmic ripples from cataclysmic events — such as the collisions of black holes — up to 120 megaparsecs (326 million light years) away on a regular basis and sample a volume of space 1,000 times greater than the original observatory. “But it could be 10 times higher or 100 times lower.”  “The first detections will be quite dramatic for us,” says Rainer Weiss, a theoretical physicist at the Massachusetts Institute of Technology in Cambridge who was one of LIGO’s founders.

<hr>

[Visit Link](http://www.nature.com/news/hunt-for-gravitational-waves-to-resume-after-massive-upgrade-1.18359){:target="_blank" rel="noopener"}


