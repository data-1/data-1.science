---
layout: post
title: "Study shows machine learning can improve catalytic design"
date: 2018-07-03
categories:
author: "Rice University"
tags: [Catalysis,Metal,Chemical reaction,Chemistry,Nature,Materials,Physical chemistry,Applied and interdisciplinary physics,Physical sciences]
---


Machine learning was used (upper panel) to search the database for hidden patterns that designers can use to make cheaper, more efficient catalysts. The binding energy between the metal and substrate is of particular interest because the stronger the bond, the less likely the metal atom is to dislodge, Janik said. The machine learning algorithm looks for the combinations of those descriptors that correlate with the observed data on binding energies, Jonayat said. For example, Senftle said one correlation that kept appearing in the study was the importance of the direct interaction between the catalytic metals and the metal atoms in the support. Explore further Researchers convert CO to CO2 with a single metal atom

<hr>

[Visit Link](https://phys.org/news/2018-07-machine-catalytic.html){:target="_blank" rel="noopener"}


