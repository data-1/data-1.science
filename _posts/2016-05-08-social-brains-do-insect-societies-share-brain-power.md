---
layout: post
title: "Social brains: Do insect societies share brain power?"
date: 2016-05-08
categories:
author: Drexel University
tags: [Sociality,Brain,Eusociality,Evolution,Insect,Species,Wasp,Nest,Cognition,Cognitive science,Behavioural sciences,Zoology,Animals,Interdisciplinary subfields]
---


Credit: Sean O'Donnell  The society you live in can shape the complexity of your brain—and it does so differently for social insects than for humans and other vertebrate animals. Essentially, O'Donnell says the cooperative or integrative aspects of insect colonies, such as information sharing among colony mates, can reduce the need for individual cognition in these societies. The idea in such social challenge hypotheses is that competition between individuals drives the evolution of sharper intelligence, as vertebrate societies tend to involve associations between unrelated individuals who experience both conflict over resources and opportunities to form alliances. They compared brains of 29 related wasp species from Costa Rica, Ecuador and Taiwan, including both solitary species and social species with varied colony structures and sizes. This nest from eastern Ecuador is one of the largest, most impressive, intimidating wasp colonies I ever encountered, said Sean O'Donnell, Ph.D., professor at Drexel University whose new study focuses on the evolution of social behavior and brain structures for complex cognition in different wasp species, including these, Polybia dimidiata.

<hr>

[Visit Link](http://phys.org/news353669991.html){:target="_blank" rel="noopener"}


