---
layout: post
title: "Optical approach offers faster and less expensive method for carbon dating"
date: 2016-04-11
categories:
author: Optica
tags: [Radiocarbon dating,Light,Laser,Mass spectrometry,Carbon-14,Chemistry,Physical chemistry,Optics,Applied and interdisciplinary physics,Electromagnetic radiation,Science,Physical sciences]
---


The instrument, which uses a new approach called saturated-absorption cavity ring-down (SCAR), is described in The Optical Society's journal for high impact research, Optica. The researchers estimate the SCAR instrument is about 100 times smaller and 10 times cheaper than the instrumentation required for accelerator mass spectrometry. We developed a very general spectroscopic technique and showed that it can be used to detect radiocarbon dioxide, said Giovanni Giusfredi, a member of the research team. How it works  The SCAR device detects radiocarbon levels by measuring how laser light interacts with the carbon dioxide that is produced when a given sample is burned. There, a light beam emitted from a quantum cascade laser at 4.5 microns - an ideal wavelength for sensitive gas detection - interacts with the carbon dioxide inside a 1-meter-long optical cavity with highly reflective mirrors on each end.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/tos-oao040116.php){:target="_blank" rel="noopener"}


