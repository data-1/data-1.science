---
layout: post
title: "NASA's Aquarius instrument returns global maps of soil moisture"
date: 2014-08-18
categories:
author: Nasa'S Goddard Space Flight Center
tags: [Aquarius (SAC-D instrument),Soil Moisture Active Passive,Soil Moisture and Ocean Salinity,Earth sciences,Physical geography,Nature,Applied and interdisciplinary physics]
---


Satellite readings of soil moisture will help scientists better understand the climate system and have potential for a wide range of applications, from advancing climate models, weather forecasts, drought monitoring and flood prediction to informing water management decisions and aiding in predictions of agricultural productivity. This visualization shows soil moisture measurements taken by NASA's Aquarius instrument. These areas where the microwave signals are difficult to interpret are shown in dark gray in the Aquarius soil moisture animation. This approach will provide a footprint of 5.6 miles (9 kilometers), and it will produce worldwide soil moisture maps every three days. Soil moisture data from NASA's Aquarius microwave radiometer are now available at the National Snow and Ice Data Center.

<hr>

[Visit Link](http://phys.org/news324029771.html){:target="_blank" rel="noopener"}


