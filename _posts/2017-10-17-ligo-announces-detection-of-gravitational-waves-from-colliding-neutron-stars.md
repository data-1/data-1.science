---
layout: post
title: "LIGO announces detection of gravitational waves from colliding neutron stars"
date: 2017-10-17
categories:
author: "University of Chicago"
tags: [Gravitational wave,LIGO,Neutron,Neutron star,Universe,Star,Astrophysics,Astronomy,Physical sciences,Physics,Space science,Science,Nature,Physical cosmology,Cosmology]
---


The result is the first measurement of a gravitational wave event in multiple mediums--optical, gamma ray and X-ray as well as gravitational waves--and scientists said the combination opens a wealth of new scientific discovery. And they used gravitational waves to directly calculate the rate at which the universe is expanding. Any one of these findings would be groundbreaking on its own merits, and here we got all the pieces together in the span of 12 hours, said Daniel Holz, an associate professor of physics and astrophysics who led the UChicago team, which was involved in both the LIGO and Dark Energy Survey discoveries. Originally suggested by famed astronomer and UChicago alumnus Edwin Hubble, this number, called the Hubble constant, is important to such central questions in astrophysics as the age of the universe and the nature of dark matter and dark energy. We'll be mining this data for a long time, he said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uoc-lad101617.php){:target="_blank" rel="noopener"}


