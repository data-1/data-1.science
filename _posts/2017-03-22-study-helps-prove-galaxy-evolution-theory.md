---
layout: post
title: "Study helps prove galaxy evolution theory"
date: 2017-03-22
categories:
author: "Texas A M University"
tags: [Milky Way,Astronomy,Star,Star formation,Hubble Space Telescope,Atacama Large Millimeter Array,Observatory,Submillimetre astronomy,National Optical Astronomy Observatory,Spitzer Space Telescope,Galaxy formation and evolution,Universe,Space science,Physical sciences,Astronomical objects,Science]
---


Using the National Radio Astronomy Observatory's Atacama Large Millimeter/submillimeter Array (ALMA)—a huge, highly sophisticated radio telescope array situated at 16,500 feet altitude in the high desert of Chile—a Papovich-led team of astronomers studied four very young versions of galaxies like the Milky Way that are 9 billion light-years distant, meaning the team could see them as they looked approximately 9 billion years ago. Credit: National Radio Astronomy Observatory  Though the relative abundance of star-forming gas is extreme in these galaxies, Papovich says they are not yet fully formed and rather small compared to the Milky Way as we see it today. Our current research shows that Milky Way-mass galaxies appear to accumulate most of their gas during their first few billion years of history. More information: C. Papovich et al. Large molecular gas reservoirs in ancestors of Milky Way-mass galaxies nine billion years ago, Nature Astronomy (2016). C. Papovich et al. Large molecular gas reservoirs in ancestors of Milky Way-mass galaxies nine billion years ago,(2016).

<hr>

[Visit Link](http://phys.org/news/2016-12-galaxy-evolution-theory.html){:target="_blank" rel="noopener"}


