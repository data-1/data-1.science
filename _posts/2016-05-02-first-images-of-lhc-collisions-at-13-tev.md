---
layout: post
title: "First images of LHC collisions at 13 TeV"
date: 2016-05-02
categories:
author:  
tags: [Large Hadron Collider,ALICE experiment,ATLAS experiment,LHCb experiment,Compact Muon Solenoid,Collider,Particle physics facilities,Experimental particle physics,Particle physics,Physics,Experimental physics,Accelerator physics,Science]
---


These test collisions were to set up systems that protect the machine and detectors from particles that stray from the edges of the beam. This set-up will give the accelerator team the data they need to ensure that the LHC magnets and detectors are fully protected. The LHC Operations team will continue to monitor beam quality and optimisation of the set-up. This is an important part of the process that will allow the experimental teams running the detectors ALICE, ATLAS, CMS and LHCb to switch on their experiments fully. Credit: ATLAS  Protons collide at 13 TeV sending showers of particles through the LHCb detector.

<hr>

[Visit Link](http://phys.org/news351410095.html){:target="_blank" rel="noopener"}


