---
layout: post
title: "Tibet’s 510-MW Zangmu, world’s highest elevation hydroelectric facility, is online"
date: 2015-10-19
categories:
author: Gregory Poindexter, Gregory B. Poindexter Is An Associate Editor For Hydroworld.Com . He Also Provides Social Media Updates Via Hydroworld.Com'S Facebook, Twitter, Linkedin Accounts., Root, --Ppa-Color-Scheme, --Ppa-Color-Scheme-Active
tags: [Dam,Power station,River,Tibet,Renewable energy,Sustainable development,Energy,Energy and the environment,Technology,Electricity,Sustainable technologies,Power (physics),Renewable resources,Electric power,Sustainable energy,Physical quantities,Nature]
---


At more than 10,800 ft above sea level, the 510-MW Zangmu Hydropower Station is fully operational. Published reports in India, China and Tibet consider the facility to be the largest in Tibet and one of the highest elevation hydroelectricity facilities in the world. According to China Gezhouba Group, the facility’s contractor, the facility includes:  • A concrete gravity dam 381 feet in height by and 1,276 feet in length;  • A spillway, plunge pool and bottom outlet on the right bank;  • A retaining dam section 262 feet in height on the left bank;  • A 70,208 acre-feet reservoir at 10,860 ft above sea level; and  • A powerhouse that has six 85-MW Francis turbine-generators. “It will alleviate the electricity shortage in central Tibet and empower the development of the electricity-strapped region. The river’s name changes to Brahmaputra River as it flows into north-eastern India.

<hr>

[Visit Link](http://www.renewableenergyworld.com/articles/hydro/2015/2015/10/tibet-s-510-mw-zangmu-world-s-highest-elevation-hydroelectric-facility-is-online.html){:target="_blank" rel="noopener"}


