---
layout: post
title: "Health: New protein aggregation measurement tool: First simple, standardized tool for studying phenomenon in living cells"
date: 2017-10-23
categories:
author: Boston University College of Engineering
tags: [Prion,Protein,News aggregator,Amyloid,Alzheimers disease,Disease,Protein aggregation,Biotechnology,Biochemistry,Biology,Cell biology,Molecular biology,Genetics,Life sciences]
---


This thread is known as protein aggregation and happens when proteins clump together. These complexes are a hallmark of many diseases, but have recently been linked to beneficial functions as well. Using the new tool, Khalil and the team created sensors to track aggregation of prions and other proteins, manipulated prions to engineer synthetic memories in cells, identified genes that can cure cells of prions and enabled high-throughput studies to learn what can influence protein aggregation and its consequences. Khalil and his team also demonstrated how the tool can be used to study other proteins, including RNA-binding proteins. All of these functions yTRAP provides will allow researchers to discover new protein aggregates, track their complicated behaviors, and look for factors and drugs that alter protein aggregation as potential treatments for currently incurable neurodegenerative diseases, or, on the flip side, figure out how to turn on a beneficial function of an aggregate.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171020160018.htm){:target="_blank" rel="noopener"}


