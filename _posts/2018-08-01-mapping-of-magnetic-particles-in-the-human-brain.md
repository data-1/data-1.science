---
layout: post
title: "Mapping of magnetic particles in the human brain"
date: 2018-08-01
categories:
author: "Ludwig-Maximilians-Universität München"
tags: [Magnetism,Science,Brain,Magnetometer,Human brain,Neuroscience]
---


Whether or not humans are capable of sensing magnetism is the subject of debate. Now a team led by Stuart A. Gilder (a professor at LMU's Department of Earth and Environmental Sciences) and Christoph Schmitz (a professor at LMU's Department of Neuroanatomy) has systematically mapped the distribution of magnetic particles in human post mortem brains. The asymmetric distribution of the magnetic particles is therefore compatible with the idea that humans might have a magnetic sensor. In further experiments, the LMU team plans to characterize the properties of the magnetic particles found in human brains. We want determine whether we can detect magnetic particles in the brains of whales, and if so whether they are also asymmetrically distributed says Schmitz.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/lm-mom073118.php){:target="_blank" rel="noopener"}


