---
layout: post
title: "Quantum physics offers insight into music expressivity"
date: 2017-09-17
categories:
author: "Queen Mary University of London"
tags: [Music,Vibrato,Erhu,Violin,Resonance,Signal,Computer science,Technology,Branches of science,Science]
---


The new approach to vibrato analysis, published in the Journal of Mathematics and Music, describes for the first time the use of the Filter Diagonalisation Method (FDM) in music signal processing. We are now one step closer to understanding the mechanics of music communication, the nuances that performers introduce to the music, and the logic behind them, said project supervisor and co-author Professor Elaine Chew from the Centre for Digital Music at QMUL's School of Electronic Engineering and Computer Science (EECS). The technique's ability to detect and estimate characteristics from very fine slivers of information comes in particularly handy in vibrato analysis and allows researchers to analyse music signals with greater precision than before. In fact, we found that, because they oscillate with time, the harmonics in musical signals can be more complicated to analyse than their quantum counterparts, he added. The research emerged from a project to model the differences between playing on violin and erhu, a two-stringed Chinese fiddle.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/qmuo-qpo031317.php){:target="_blank" rel="noopener"}


