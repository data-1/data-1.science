---
layout: post
title: "A Solar-Powered Hydroponic Food Belt Could Green the Entire Arabian Peninsula"
date: 2015-07-04
categories:
author: Lidija Grozdanic
tags: []
---


Continue reading below Our Featured Videos  OAXIS is designed to incorporate aprefabricated modular structures with recycled steel. Several passive strategies would control the building’s environment while roof-mounted photovoltaic panels would supply enough energy for the facilities, the artificial LED lighting and the underground conveyor belt system. Excess energy would be fed into the city electric grid. Related: 6 Great Green Designs that Put South Africa on the Map  Recycled water coming from the hydroponic farming technology will be used forirrigationof the exterior vegetation, allowing plants and biodiversity to expand around the facilities. SIGN UP I agree to receive emails from the site.

<hr>

[Visit Link](http://inhabitat.com/oaxis-project-aims-to-green-the-entire-arabian-peninsula-with-a-solar-powered-hydroponic-food-belt/){:target="_blank" rel="noopener"}


