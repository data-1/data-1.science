---
layout: post
title: "What teeth reveal about the lives of modern humans"
date: 2017-10-05
categories:
author: "Ohio State University"
tags: [Tooth,Fossil,Human,Wisdom tooth,Human evolution,Evolution,Food]
---


She is a professor of anthropology at The Ohio State University who studies fossilized teeth to answer questions about the life history, growth, and diet of primates and our human ancestors, as well as the relationships between different species. In a new book, What Teeth Reveal About Human Evolution (Cambridge University Press, 2016), she gives a broad overview of what scientists have learned about our ancestors from studying fossilized teeth. As for the teeth of humans living today - well, it is a good thing we have modern dentistry. Much of Guatelli-Steinberg's own research has focused on using patterns of tooth growth to assess what life was like for the individuals under study. Regardless of what new techniques are developed to study teeth, Guatelli-Steinberg said she expects future anthropologists will likely have a field day studying modern human teeth.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/osu-wtr010917.php){:target="_blank" rel="noopener"}


