---
layout: post
title: "A clue to generate electric current without energy consumption at room temperature"
date: 2015-12-31
categories:
author: Hiroshima University
tags: [Ferromagnetism,Magnetism,Electric current,Atom,Topological insulator,Insulator (electricity),American Association for the Advancement of Science,Magnet,Magnetic field,Science,Nature,Physical chemistry,Chemical product engineering,Physics,Electromagnetism,Physical sciences,Applied and interdisciplinary physics,Chemistry,Materials,Electrical engineering,Materials science,Electricity]
---


A group of researchers in Japan and China identified the requirements for the development of new types of extremely low power consumption electric devices by studying Cr-doped (Sb, Bi)2Te3 thin films. At extremely low temperatures, an electric current flows around the edge of the film without energy loss, and under no external magnetic field. Hopefully, this achievement will lead to the creation of novel materials that operate at room temperature in the future, said Akio Kimura, a professor at Hiroshima University and a member of the research group. That's why we selected the material as the object of our study, said Professor Kimura. When the N-S orientations of Cr atoms in Cr-doped (Sb, Bi)2Te3 are aligned in parallel, the material exhibits ferromagnetism.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/hu-act122915.php){:target="_blank" rel="noopener"}


