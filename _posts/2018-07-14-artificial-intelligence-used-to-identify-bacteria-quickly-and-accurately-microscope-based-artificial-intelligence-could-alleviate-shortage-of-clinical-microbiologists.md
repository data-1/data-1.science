---
layout: post
title: "Artificial intelligence used to identify bacteria quickly and accurately: Microscope-based artificial intelligence could alleviate shortage of clinical microbiologists"
date: 2018-07-14
categories:
author: "Beth Israel Deaconess Medical Center"
tags: [Medical microbiology,Artificial intelligence,Infection,News aggregator,Bacteria,Bloodstream infections,Medical specialties,Technology,Clinical medicine,Branches of science,Medicine]
---


In a paper published in the Journal of Clinical Microbiology, the scientists demonstrated that an automated AI-enhanced microscope system is highly adept at identifying images of bacteria quickly and accurately. The automated system could help alleviate the current lack of highly trained microbiologists, expected to worsen as 20 percent of technologists reach retirement age in the next five years. In this case, blood samples taken from patients with suspected bloodstream infections were incubated to increase bacterial numbers. By cropping these images -- in which the bacteria had already been identified by human clinical microbiologists -- the researchers generated more than 100,000 training images. The machine intelligence learned how to sort the images into the three categories of bacteria (rod-shaped, round clusters, and round chains or pairs), ultimately achieving nearly 95 percent accuracy.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/12/171215143252.htm){:target="_blank" rel="noopener"}


