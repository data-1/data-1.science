---
layout: post
title: "Tuning into quantum: Scientists unlock signal frequency control of precision atom qubits"
date: 2018-07-14
categories:
author: "University Of New South Wales"
tags: [Atom,Electron,Qubit,Scanning tunneling microscope,Molecule,Quantum computing,Microscope,Michelle Simmons,Spin (physics),Technology,Quantum mechanics,Science]
---


Credit: Dr. Sam Hile  Australian scientists have achieved a new milestone in their approach to creating a quantum computer chip in silicon, demonstrating the ability to tune the control frequency of a qubit by engineering its atomic configuration. The results showed that when changing the frequency of the signal used to control the electron spin, the single atom had a dramatically different control frequency compared to the electron spin in the molecule of two phosphorus atoms. Credit: CQC2T  Individually addressing each qubit when they are so close is challenging, says UNSW Scientia Professor Michelle Simmons, Director CQC2T and co-author of the paper. Each molecule can be operated individually by selecting the frequency that controls its electron spin. By engineering the atomic placement of the atoms within the qubits in the silicon chip, the molecules can be created with different resonance frequencies.

<hr>

[Visit Link](https://phys.org/news/2018-07-tuning-quantum-scientists-frequency-precision.html){:target="_blank" rel="noopener"}


