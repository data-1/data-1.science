---
layout: post
title: "Key psychiatric drug target comes into focus"
date: 2017-10-20
categories:
author: University of California - San Francisco
tags: [Dopamine,University of California San Francisco,Receptor (biochemistry),Dopamine receptor,Health sciences,Health]
---


Despite their importance, very little is known about the structures of the vast majority of GPCRs, including D4 and other dopamine receptors, making it challenging to design more precise drugs with fewer side effects. Once we had that, we teamed up with our UCSF colleagues to computationally screen for compounds that might potentially bind to this receptor but not others. Once they had identified the top ten candidate compounds that computer modeling pointed to as likely binding partners with the D4 receptor, they sent them back to Wang and Wacker to test experimentally in the lab. Upon testing this compound in the lab, Wang confirmed the molecule could bind to the D4 receptor 1000-times more powerfully than the initial virtual compounds. This work has implications beyond D4, Wacker said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uoc--kpd101817.php){:target="_blank" rel="noopener"}


