---
layout: post
title: "Computer model simulates Neolithic transition from egalitarianism to leadership and despotism"
date: 2015-10-28
categories:
author: Bob Yirka
tags: [Privacy,Computer simulation,Simulation,Leadership,Science,Branches of science,Cognitive science]
---


But then, something changed, people began living in much larger communities which were run by one person, or small groups of people, resulting in less freedom of choice for everyone else. As with any group of people, leaders arose along with associated followers. Over time, as projects grew larger, so too did the number of people required to build them and the leaders gained even more power. Explore further How leaders evolve  More information: An evolutionary model explaining the Neolithic transition from egalitarianism to leadership and despotism, Proceedings of the Royal Society B, rspb.royalsocietypublishing.or … nt/281/1791/20141349 An evolutionary model explaining the Neolithic transition from egalitarianism to leadership and despotism,  Abstract  The Neolithic was marked by a transition from small and relatively egalitarian groups to much larger groups with increased stratification. We show that voluntary leadership without coercion can evolve in small groups, when leaders help to solve coordination problems related to resource production.

<hr>

[Visit Link](http://phys.org/news326533603.html){:target="_blank" rel="noopener"}


