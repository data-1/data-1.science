---
layout: post
title: "Intelligent neuroprostheses mimic natural motor control"
date: 2015-12-09
categories:
author: Cognitive Neuroscience Society
tags: [Neuroprosthetics,Braincomputer interface,Brain,Prosthesis,Electroencephalography,Technology,Cognitive psychology,Psychological concepts,Cognitive neuroscience,Psychology,Branches of science,Cognition,Neuroscience,Cognitive science,Interdisciplinary subfields]
---


In new work, researchers have tested a range of brain-controlled devices - from wheelchairs to robots to advanced limbs - that work with their users to intelligently perform tasks. The use of shared control - new to neuroprostheses - empowers users to perform complex tasks, says José del R. Millán, who presented the new work at the Cognitive Neuroscience Society (CNS) conference in San Francisco today. And what more direct than decoding user's intention from their brain signals? So, Millán began working on brain-computer interfaces (BCIs), designing devices that use people's own brain activity to restore hand grasping and locomotion, or provide mobility via wheelchairs or telepresence robots, using people's own brain activity. By designing the intelligent device to control the lower-level movements in concert with the higher-level brain activity from the BCI, the neuroprostheses come closer to natural motor control.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/cns-inm032715.php){:target="_blank" rel="noopener"}


