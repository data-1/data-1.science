---
layout: post
title: "In living color: Seeing cells from outside the body with synthetic bioluminescence"
date: 2018-04-20
categories:
author: "RIKEN"
tags: [Bioluminescence,News aggregator,Luciferin,Luciferase,Biology,Neuroscience]
---


Now, scientists in Japan have supercharged these molecules, making them hundreds of times brighter in deep tissues and allowing for imaging of cells from outside the body. Bioluminescence is the result of a partnership: an enzyme -- in this case luciferase derived from fireflies -- catalyzes the substrate D-luciferin, creating a green-yellow glow in the process. The resulting Akaluc protein is both a more efficient catalyst for the substrate and more abundantly expressed by cells. In the mouse brain, this combination of Akaluc catalyzing AkaLumine-HCl, dubbed AkaBLI, resulted in a bioluminescence signal 1000 times stronger than that from the natural luciferase-luciferin reaction. With AkaBLI, how brain activity and structures change with behavior can be directly observed over time.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/02/180222145018.htm){:target="_blank" rel="noopener"}


