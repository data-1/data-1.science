---
layout: post
title: "MBARI researchers discover deepest high-temperature hydrothermal vents in Pacific Ocean"
date: 2015-07-12
categories:
author: Monterey Bay Aquarium Research Institute 
tags: [Hydrothermal vent,Monterey Bay Aquarium Research Institute,Seabed,Earth sciences,Geology,Physical geography,Hydrography,Hydrology,Oceanography]
---


They are also the only vents in the Pacific known to emit superheated fluids rich in both carbonate minerals and hydrocarbons. The vents have been colonized by dense communities of tubeworms and other animals unlike any other known vent communities in the in the eastern Pacific. Like another vent field in the Gulf that MBARI discovered in 2012, the Pescadero Basin vents were initially identified in high-resolution sonar data collected by an autonomous underwater vehicle (AUV). The AUV team, led by MBARI engineer David Caress, pored over the detailed bathymetric map they created from the AUV data and saw a number of mounds and spires rising up from the seafloor. Clague's and Vrijenhoek's dives revealed at least three different types of hydrothermal vents in the southern Gulf of California -- black smokers, carbonate chimneys, and hydrothermal seeps.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/mbar-mrd060215.php){:target="_blank" rel="noopener"}


