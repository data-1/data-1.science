---
layout: post
title: "Machine learning and neural networks recognize exotic insulating phases in quantum materials"
date: 2018-07-03
categories:
author: "Us Department Of Energy"
tags: [Topological order,Machine learning,Quantum mechanics,Neural network,Topology,Condensed matter physics,Matter,Physics,Electron,Information,Science,Branches of science,Technology]
---


Machine learning is a powerful tool for pattern recognition and thus could help identify phases of matter. It detects with high efficiency an exotic phase where electricity is conducted around the material's surface but not through the middle. The technique connects neural networks to the theory of the quantum world. The key challenge is to extract essential information from the electronic density (aka many-body wave function). They extracted the essential data by using a quantum loop topography bridge.

<hr>

[Visit Link](https://phys.org/news/2018-01-machine-neural-networks-exotic-insulating.html){:target="_blank" rel="noopener"}


