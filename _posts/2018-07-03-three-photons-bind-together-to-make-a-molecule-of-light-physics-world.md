---
layout: post
title: "Three photons bind together to make a ‘molecule’ of light – Physics World"
date: 2018-07-03
categories:
author: "Hamish Johnston"
tags: [Photon,Physics,Gas,Light,Molecule,Atom,Photonics,Physical chemistry,Applied and interdisciplinary physics,Atomic molecular and optical physics,Quantum mechanics,Physical sciences,Theoretical physics,Natural philosophy,Electromagnetism,Atomic physics,Electromagnetic radiation,Science,Optics,Chemistry]
---


The photon triplets were made by firing laser light into an atomic gas and the researchers believe that their technique could be useful for creating entangled photons for quantum-information systems. Propagating polariton  Their technique involves the light creating a “Rydberg polariton” in the atomic gas. Instead, and under certain conditions, the index of refraction of the gas near the Rydberg polariton is modified such that other photons will bunch-up with the polariton. This is effectively an attractive interaction between photons that creates molecules of light. The team is now looking at how it could create repulsive interactions between photons.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2018/feb/20/three-photons-bind-together-to-make-a-molecule-of-light){:target="_blank" rel="noopener"}


