---
layout: post
title: "Ancient Jellies Had Spiny Skeletons, No Tentacles"
date: 2015-07-14
categories:
author: Mindy Weisberger
tags: [Ctenophora,Jellyfish,Predation,Tentacle,Animal,Taxa,Animals]
---


[See Images of the Ancient Jellies & Other Wacky Cambrian Creatures]  Some of the fossils in the study are new to science, while others were originally described years ago and reclassified following this new analysis. Voracious predators  Almost as surprising as the skeletons was something the ancient jellies didn't have: tentacles. But not all ctenophores possess tentacles, so perhaps the ancient jellies hunted like those tentacle-free animals, known as lobate ctenophores, do. The Cambrian comb jellies could have done the same, engulfing prey that may even have included other ctenophores. Armor for a Cambrian arms race  As for why the ancient comb jellies were so armored, the researchers suggest the bony structures could have supported jellies' vulnerable bodies, and protected them against predators and environmental damage.

<hr>

[Visit Link](http://www.livescience.com/51515-ancient-comb-jellies-had-skeletons.html){:target="_blank" rel="noopener"}


