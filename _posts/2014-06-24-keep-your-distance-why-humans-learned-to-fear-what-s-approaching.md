---
layout: post
title: "Keep your Distance: Why Humans Learned to Fear What's Approaching"
date: 2014-06-24
categories:
author: Science World Report
tags: [Human,Feeling,Psychology,Social psychology,Communication,Psychological concepts,Cognitive science,Behavioural sciences,Interdisciplinary subfields,Branches of science,Concepts in metaphysics,Cognition,Primate behavior]
---


Now, a recent study published in the Journal of Personality and Social Psychology, shows how even in modern times, negative feelings about things that approach us still may linger, though they're not typically a danger. In order to survive, humans have developed a tendency to guard against animals, people and objects that come near them, said lead study author Professor Christopher K. Hsee of the university, in a news release. In a similar fashion, those who tend to move closer and closer toward their audiences during speeches may also think twice about what they're doing. Approach avoidance is a general tendency, humans don't seem to adequately distinguish between times they should use it and when they should not, Hsee added. They tend to fear approaching things and looming events even if objectively they need not fear.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15616/20140623/keep-your-distance-why-humans-learned-to-fear-whats-approaching.htm){:target="_blank" rel="noopener"}


