---
layout: post
title: "How the Squid Lost Its Shell"
date: 2017-10-25
categories:
author:  
tags: [Cephalopod,Giant squid,Animals,Taxa,Cephalopods]
---


Giant squid are the sea’s best monsters, tentacles down. Megalodon showed up a mere 23 million years ago. But how did we get from Endoceras to Architeuthis? And squid? Well, the shell remnant of a squid has no chambers and no calcium.

<hr>

[Visit Link](http://www.deepseanews.com/2017/10/how-the-squid-lost-its-shell/){:target="_blank" rel="noopener"}


