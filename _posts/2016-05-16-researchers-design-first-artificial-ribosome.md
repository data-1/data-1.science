---
layout: post
title: "Researchers design first artificial ribosome"
date: 2016-05-16
categories:
author: University of Illinois at Chicago
tags: [Ribosome,Cell (biology),Messenger RNA,Protein,RNA,News aggregator,Biochemistry,Branches of genetics,Molecular biophysics,Nucleic acids,Molecular genetics,Structural biology,Macromolecules,Biomolecules,Genetics,Chemistry,Cell biology,Life sciences,Biology,Biotechnology,Molecular biology]
---


When the cell makes a protein, mRNA (messenger RNA) is copied from DNA. No one has ever developed something of this nature. The researchers devised a novel designer ribosome with tethered subunits -- Ribo-T.  What we were ultimately able to do was show that by creating an engineered ribosome where the ribosomal RNA is shared between the two subunits and linked by these small tethers, we could actually create a dual translation system, Jewett said. Jewett and Mankin were surprised by this. Scientists had previously believed that the ability of the two ribosomal subunits to separate was required for protein synthesis.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150729215735.htm){:target="_blank" rel="noopener"}


