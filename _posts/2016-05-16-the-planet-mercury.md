---
layout: post
title: "The planet Mercury"
date: 2016-05-16
categories:
author: Fraser Cain
tags: [Mercury (planet),Planet,Sun,Solar System,Mariner 10,MESSENGER,Mariner program,Planetary core,Earth,Atmosphere,Planets,Astronomy,Space science,Planetary science,Physical sciences,Outer space,Bodies of the Solar System,Astronomical objects,Planets of the Solar System]
---


Credit: NASA/Johns Hopkins University Applied Physics Laboratory/Carnegie Institution of Washington  Mercury is the closest planet to our sun, the smallest of the eight planets, and one of the most extreme worlds in our solar systems. Mercury also has the most eccentric orbit of any planet in the solar system. Most of the planet has been mapped by the Arecibo radar telescope, with 5 km resolution, including polar deposits in shadowed craters of what was believed to be water ice. The second NASA mission to Mercury was the MErcury Surface, Space ENvironment, GEochemistry, and Ranging (or MESSENGER) space probe. Most of the hemisphere not imaged by Mariner 10 was mapped during these fly-bys.

<hr>

[Visit Link](http://phys.org/news/2015-08-planet-mercury.html){:target="_blank" rel="noopener"}


