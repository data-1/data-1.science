---
layout: post
title: "MicroBooNE experiment sees first cosmic muons"
date: 2016-05-26
categories:
author: Fermi National Accelerator Laboratory (Fermilab)
tags: [Neutrino,Muon,MicroBooNE,Cosmic ray,Particle physics,Fermilab,Elementary particles,Physics,Physical sciences,Leptons,Nuclear physics,Science,Nature]
---


This is the first detector of this size and scale we've ever launched in the U.S. for use in a neutrino beam, so it's a very important milestone for the future of neutrino physics, said Sam Zeller, co-spokesperson for the MicroBooNE collaboration. Picking up cosmic muons is just one brief stop during MicroBooNE's expedition into particle physics. One of MicroBooNE's goals is to measure how often a neutrino that interacts with an argon atom will produce certain types of particles. MicroBooNE will carry signals up to two and a half meters across the detector, the longest drift ever for a LArTPC in a neutrino beam. The field has chosen liquid argon as its future technology, and all eyes are on us to see if our detector will work.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150812170047.htm){:target="_blank" rel="noopener"}


