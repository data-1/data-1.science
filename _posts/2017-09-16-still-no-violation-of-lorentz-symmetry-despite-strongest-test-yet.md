---
layout: post
title: "Still no violation of Lorentz symmetry, despite strongest test yet"
date: 2017-09-16
categories:
author: "Lisa Zyga"
tags: [Standard-Model Extension,Lunar Laser Ranging experiment,General relativity,Theory of relativity,Moon,Applied and interdisciplinary physics,Scientific theories,Physics,Physical sciences,Theoretical physics,Science]
---


Astronomers all over the world have reflected laser light off the reflector to precisely measure the Earth-Moon distance. In the new study, the researchers analyzed data from more than 20,000 reflected laser beams sent between 1969 and 2013 by five LLR stations located at different places on the Earth. The framework for this ephemeris comes from a theory called the standard-model extension (SME), which combines general relativity and the Standard Model of particle physics, and allows for the possibility of Lorentz symmetry breaking. Overall, the researchers' analysis shows that LLR data are sensitive to certain combinations of the SME coefficients, but found no evidence that LLR depends on the velocity or the direction of its reference frame, indicating no Lorentz symmetry breaking. In general, improving these constraints means that any violation of Lorentz symmetry must be very small, if it exists at all.

<hr>

[Visit Link](http://phys.org/news/2016-12-violation-lorentz-symmetry-strongest.html){:target="_blank" rel="noopener"}


