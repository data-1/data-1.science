---
layout: post
title: "Image: Saturn's sponge-like moon Hyperion"
date: 2016-04-24
categories:
author: European Space Agency
tags: [Hyperion (moon),CassiniHuygens,Sun,Space science,Astronomy,Planetary science,Outer space,Bodies of the Solar System,Solar System,Planets of the Solar System,Physical sciences,Astronomical objects known since antiquity,Astronomical objects]
---


Credit: NASA/JPL/Space Science Institute  The subject of this image bears a remarkable resemblance to a porous sea sponge, floating in the inky black surroundings of the deep sea. Indeed, the cold, hostile and lonely environment of deep water is not too far removed from deep space, the actual setting for this image in which one of Saturn's outer moons, Hyperion, can be seen in incredible detail. This image was taken by Cassini when the spacecraft performed a flyby of the small moon on 26 September 2005. During the flyby, Cassini got more than it bargained for as Hyperion unleashed a burst of charged particles towards the spacecraft, effectively delivering a giant 200-volt electric shock. While astronomers expected many bodies throughout the Solar System to be charged, the data from the Cassini flyby represent the first-ever experience of a charged natural object in space other than our own Moon.

<hr>

[Visit Link](http://phys.org/news349429441.html){:target="_blank" rel="noopener"}


