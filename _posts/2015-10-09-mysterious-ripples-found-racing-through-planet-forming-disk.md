---
layout: post
title: "Mysterious ripples found racing through planet-forming disk"
date: 2015-10-09
categories:
author: "$author" 
tags: [AU Microscopii,European Southern Observatory,Space Telescope Science Institute,Microscopium,Planetary science,Stellar astronomy,Science,Outer space,Astronomical objects,Physical sciences,Space science,Astronomy]
---


Astronomers using NASA's Hubble Space Telescope and the European Southern Observatory's (ESO) Very Large Telescope in Chile have discovered never-before-seen features within the dusty disk surrounding the young, nearby star AU Microscopii (AU Mic).The fast-moving, wave-like structures are unlike anything ever observed, or even predicted in a circumstellar disk, said researchers of a new analysis. The images reveal a train of wave-like arches, resembling ripples in water. After spotting the features in the SPHERE data the team turned to earlier Hubble images of the disk, taken in 2010 and 2011. One of these flares could perhaps have triggered something on one of the planets -- if there are planets -- like a violent stripping of material, which could now be propagating through the disk, propelled by the flare's force. The Space Telescope Science Institute (STScI) in Baltimore, Maryland, conducts Hubble science operations.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/nsfc-mrf100715.php){:target="_blank" rel="noopener"}


