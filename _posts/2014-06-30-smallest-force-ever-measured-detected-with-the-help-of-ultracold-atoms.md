---
layout: post
title: "Smallest Force Ever Measured Detected with the Help of Ultracold Atoms"
date: 2014-06-30
categories:
author: Science World Report
tags: [Force,Optics,Theoretical physics,Electromagnetic radiation,Physical chemistry,Physics,Science,Quantum mechanics,Physical sciences,Atomic molecular and optical physics,Applied and interdisciplinary physics]
---


Using a combination of lasers and a unique optical trapping system that provides a cloud of ultracold atoms, the scientists measured a force of just 42 yoctonewtons. In fact, there are about 3 x 10^23 yoctonewtons in just one ounce of force. In theory, a combination of colder atoms and improved optical detection efficiency should allow researchers to detect an even smaller force. This could mean that, eventually, physicists may be able to measure the SQL itself-or at least very close to it. It took about 30 years longer than predicted, but we now have an experiment set-up capable both of reaching very close to the SQL and of showing the onset of different kinds of obscuring noise away from that SQL.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15710/20140627/smallest-force-measured-detected-help-ultracold-atoms.htm){:target="_blank" rel="noopener"}


