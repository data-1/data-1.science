---
layout: post
title: "Self-replicating nanostructures made from DNA"
date: 2016-05-03
categories:
author: Heather Zeiger
tags: [DNA,Self-replication,Self-assembly,DNA replication,Toehold mediated strand displacement,Nanotechnology,System,Nucleic acid thermodynamics,Technology,Macromolecules,Physical sciences,Genetics,Biotechnology,Biochemistry,Molecular biology,Life sciences,Biology,Chemistry]
---


However, Junghoon Kim, Junwye Lee, Shogo Hamada, Satoshi Murata, and Sung Ha Park of Sungkyunkwan University and Tohoku University have designed a controllable self-replicating system that does not require proteins. The toeholds indicate that the ring or motif is fertilized. This process continues through two different replication pathways. The authors verified that the DNA ring populations grew through this toehold-mediated process with AFM and absorbance studies. Here, we show that DNA T-motifs, which self-assemble into ring structures, can be designed to self-replicate through toehold-mediated strand displacement reactions.

<hr>

[Visit Link](http://phys.org/news352007343.html){:target="_blank" rel="noopener"}


