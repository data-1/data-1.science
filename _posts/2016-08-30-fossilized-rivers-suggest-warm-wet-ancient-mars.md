---
layout: post
title: "Fossilized rivers suggest warm, wet ancient Mars"
date: 2016-08-30
categories:
author: "University College London"
tags: [Mars,Oxia Palus quadrangle,Mars Reconnaissance Orbiter,Space science,Spaceflight,Earth sciences,Bodies of the Solar System,Planets,Solar System,Astronomy,Outer space,Planetary science,Planets of the Solar System,Terrestrial planets,Astronomical objects known since antiquity]
---


Extensive systems of fossilised riverbeds have been discovered on an ancient region of the Martian surface, supporting the idea that the now cold and dry Red Planet had a warm and wet climate about 4 billion years ago, according to UCL-led research. The study, published in Geology and funded by the Science & Technology Facilities Council and the UK Space Agency, identified over 17,000km of former river channels on a northern plain called Arabia Terra, providing further evidence of water once flowing on Mars. We've now found evidence of extensive river systems in the area which supports the idea that Mars was warm and wet, providing a more favourable environment for life than a cold, dry planet, explained lead author, Joel Davis (UCL Earth Sciences). The inverted channels are similar to those found elsewhere on Mars and Earth. We think the rivers were active 3.9-3.7 billion years ago, but gradually dried up before being rapidly buried and protected for billions of years, potentially preserving any ancient biological material that might have been present, added Joel Davis.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/ucl-frs082316.php){:target="_blank" rel="noopener"}


