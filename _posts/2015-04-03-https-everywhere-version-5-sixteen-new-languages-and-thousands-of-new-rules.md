---
layout: post
title: "HTTPS Everywhere Version 5: Sixteen New Languages and Thousands of New Rules"
date: 2015-04-03
categories:
author: Jacob Hoffman-Andrews
tags: [HTTPS,HTTPS Everywhere,World Wide Web,Internet privacy,Information technology,Technology,Software,Computing,Internet,Cyberspace,Software development,Digital media,Information Age,Hypertext,Software engineering,Information technology management,Computer science,Computer security,Communication]
---


This week we released the latest version of HTTPS Everywhere to all of our users. If you don't have HTTPS Everywhere installed, you can get it here. HTTPS Everywhere is a browser extension that improves your privacy and security when using the web by sending you to the HTTPS-secured version of web sites listed in its rules. But today many sites offer HTTPS only as an afterthought, and don't send their users there by default. If you find a site that doesn't work with HTTPS Everywhere, please report a bug, either through GitHub, or by emailing https-everywhere@lists.eff.org.

<hr>

[Visit Link](https://www.eff.org/deeplinks/2015/03/https-everywhere-version-5-sixteen-new-languages-and-thousands-new-rules){:target="_blank" rel="noopener"}


