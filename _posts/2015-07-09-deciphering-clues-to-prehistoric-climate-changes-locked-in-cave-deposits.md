---
layout: post
title: "Deciphering clues to prehistoric climate changes locked in cave deposits"
date: 2015-07-09
categories:
author: Vanderbilt University 
tags: [Speleothem,Climate,Atmospheric sciences,Atmosphere,Natural environment,Applied and interdisciplinary physics,Earth phenomena,Earth sciences,Nature,Physical geography]
---


Variations in the thickness of the layers is determined by a combination of the amount of water seeping into the cave and the concentration of carbon dioxide in the cave's atmosphere so, when conditions are right, they can provide a measure of how the amount of precipitation above the cave varies over time. When the researchers analyzed the Mawmluh stalagmite record, the results were consistent with the historical record. The Holocene Climate Optimum was a period of global climate warming that occurred between six to nine thousand years ago. The amount of information about the climate that scientists can extract from the stalagmites and stalactites in a cave is amazing. Efforts to use the mineral deposits in caves as climate proxies began in the 1990's.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/vu-dct052215.php){:target="_blank" rel="noopener"}


