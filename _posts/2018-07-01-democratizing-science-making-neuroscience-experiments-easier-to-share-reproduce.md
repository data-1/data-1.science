---
layout: post
title: "Democratizing science: Making neuroscience experiments easier to share, reproduce"
date: 2018-07-01
categories:
author: "University of Washington"
tags: [Nervous system,Research,Brain,Diffusion MRI,Neuroscience,Magnetic resonance imaging,Neuroscientist,White matter,Replication crisis,Reproducibility,Big data,Science,Branches of science,Cognitive science]
---


Researchers at the University of Washington have developed a set of tools to make one critical area of big data research -- that of our central nervous system -- easier to share. In a paper published online March 5 in Nature Communications, the UW team describes an open-access browser they developed to display, analyze and share neurological data collected through a type of magnetic resonance imaging study known as diffusion-weighted MRI. But using AFQ-Browser, we eliminate those requirements and make uploading, sharing and analyzing diffusion-weighted MRI data a simple, straightforward process. Diffusion MRI research on brain connectivity has fundamentally changed the way neuroscientists understand human brain function: The state, organization and layout of white matter tracts are at the core of cognitive functions such as memory, learning and other capabilities. In addition, the format is easy for scientists from a variety of backgrounds to use and understand -- so that neuroscientists, statisticians and other researchers can collaborate, view data and share methods toward greater reproducibility.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-03/uow-dsm031518.php){:target="_blank" rel="noopener"}


