---
layout: post
title: "Modern Europeans descended from three groups of ancestors"
date: 2015-07-20
categories:
author: Howard Hughes Medical Institute 
tags: [Ancient North Eurasian,Genetics,Human populations]
---


Those include hunter-gatherers from western Europe, the early farmers who brought agriculture to Europe from the Near East, and a newly identified group of ancient north Eurasians who arrived in Europe sometime after the introduction of agriculture. But the genomes of present-day Europeans show signs that they come from more than just the indigenous hunter-gatherers and these early farmers. What we find is unambiguous evidence that people in Europe today have all three of these ancestries: early European farmers who brought agriculture to Europe, the indigenous hunter-gatherers who were in Europe prior to 8,000 years ago, and these ancient north Eurasians, Reich says. Although DNA from ancient north Eurasians is present in nearly all modern Europeans, Reich's team did not find it in their ancient hunter-gatherers or the ancient farmers. Ancient DNA from Basal Europeans, if found, might lead to new revelations about early human history.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/hhmi-med091614.php){:target="_blank" rel="noopener"}


