---
layout: post
title: "Magnetic bacteria target hard-to-treat tumours – Physics World"
date: 2017-10-07
categories:
author: "Isabelle Dumé"
tags: [Radiation therapy,Health sciences,Medicine,Clinical medicine,Chemistry]
---


(Courtesy: NanoRobotics Laboratory/Polytechnique Montreal)  Bacteria that respond to magnetic fields and low oxygen levels may soon join the fight against cancer. With further development, the method could be used to treat a variety of solid tumours, which account for roughly 85% of all cancers. Cancer cells in a growing tumour consume large amounts of oxygen and parts of the tumour will become starved of oxygen – or hypoxic. Now, a team led by Sylvain Martel of the NanoRobotics Laboratory at the Polytechnique Montréal – including researchers at McGill University – has developed a method that exploits the magnetotactic bacteria Magnetoccus marinus (MC-1) to overcome this problem. Indeed, the oxygen levels found in hypoxic regions of a tumour – about 0.5% – are perfect for MC-1.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2016/aug/23/magnetic-bacteria-target-hard-to-treat-tumours){:target="_blank" rel="noopener"}


