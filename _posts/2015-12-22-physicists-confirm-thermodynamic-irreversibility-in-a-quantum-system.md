---
layout: post
title: "Physicists confirm thermodynamic irreversibility in a quantum system"
date: 2015-12-22
categories:
author: Lisa Zyga
tags: [Entropy,Irreversible process,Quantum mechanics,Thermodynamics,Thermodynamic equilibrium,Physics,Mechanics,Mathematical physics,Continuum mechanics,Applied mathematics,Systems theory,Applied and interdisciplinary physics,Theoretical physics,Chemistry,Physical chemistry,Science,Physical sciences]
---


Forward and reverse magnetic pulses are applied to the sample, which drives the carbon nuclear spins out of equilibrium and produces irreversible entropy. Credit: Batalhão, et al. ©2015 American Physical Society  (Phys.org)—For the first time, physicists have performed an experiment confirming that thermodynamic processes are irreversible in a quantum system—meaning that, even on the quantum level, you can't put a broken egg back into its shell. The results have implications for understanding thermodynamics in quantum systems and, in turn, designing quantum computers and other quantum information technologies. The measurements of the spins indicated that entropy was increasing in the isolated system, showing that the quantum thermodynamic process was irreversible. The microscopic laws allow reversible processes only because they begin with a genuine equilibrium process for which the entropy production vanishes at all times, the scientists write in their paper.

<hr>

[Visit Link](http://phys.org/news/2015-12-physicists-thermodynamic-irreversibility-quantum.html){:target="_blank" rel="noopener"}


