---
layout: post
title: "New insights into the family tree of modern turtles"
date: 2015-12-22
categories:
author: Universitaet Tübingen
tags: [Jehol Biota,Turtle,Fossil,Dinosaur,Taxa,Animals]
---


Credit: Chang-Fu Zhou  Today's sea turtles are the sole survivors of a once diverse ecosystem of marine reptiles from the age of dinosaurs. However, the first vertebrate of the Jehol Biota to be described in 1942 was one of the many turtles found there. Therefore, the researchers say, the earliest known sea turtles are likely to have looked much like the species found in the Jehol Biota. It was a highly successful adaptation and it is truly depressing to see that the last surviving marine reptiles are threatened with extinction after more than 130 million years, says Rabi. Credit: Chang-Fu Zhou and Márton Rabi  Explore further Oldest fossil sea turtle discovered

<hr>

[Visit Link](http://phys.org/news/2015-11-insights-family-tree-modern-turtles.html){:target="_blank" rel="noopener"}


