---
layout: post
title: "The mind behind Linux"
date: 2016-04-10
categories:
author: Linus Torvalds
tags: [TED (conference),Linux,Git,Linus Torvalds,Linux kernel,Software,Technology,Computing,Software development,Unix variants,System software,Software engineering,Free software]
---


Linus Torvalds transformed technology twice -- first with the Linux kernel, which helps power the Internet, and again with Git, the source code management system used by developers worldwide. In a rare interview with TED Curator Chris Anderson, Torvalds discusses with remarkable openness the personality traits that prompted his unique philosophy of work, engineering and life. I am not a visionary, I'm an engineer, Torvalds says. I'm perfectly happy with all the people who are walking around and just staring at the clouds ... but I'm looking at the ground, and I want to fix the pothole that's right in front of me before I fall in.

<hr>

[Visit Link](http://www.ted.com/talks/linus_torvalds_the_mind_behind_linux){:target="_blank" rel="noopener"}


