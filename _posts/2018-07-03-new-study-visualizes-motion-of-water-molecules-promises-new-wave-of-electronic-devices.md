---
layout: post
title: "New study visualizes motion of water molecules, promises new wave of electronic devices"
date: 2018-07-03
categories:
author: "DOE/Oak Ridge National Laboratory"
tags: [Molecule,Water,Atom,Chemical bond,Liquid,X-ray,Hydrogen,Properties of water,Nature,Atomic physics,Science,Chemistry,Physical sciences,Applied and interdisciplinary physics,Physical chemistry,Physics,Atomic molecular and optical physics]
---


A team of researchers led by the Department of Energy's Oak Ridge National Laboratory used a high-resolution inelastic X-ray scattering technique to measure the strong bond involving a hydrogen atom sandwiched between two oxygen atoms. When the bond between water molecules is disrupted, the strong hydrogen bonds work to maintain a stable environment over a specific period of time. We found that the amount of time it takes for a molecule to change its 'neighbor' molecule determines the water's viscosity, Egami said. Egami views the current work as a springboard to more advanced research that will leverage neutron scattering techniques at the Spallation Neutron Source at ORNL, a DOE Office of Science User Facility, to further determine the origin of viscosity and other dynamic properties of liquids. The researchers' approach could also be used to characterize the molecular behavior and viscosity of ionic, or salty, liquids and other liquid substances, which would aid in the development of new types of semiconductor devices with liquid electrolyte insulating layers, better batteries and improved lubricants.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/drnl-nsv122217.php){:target="_blank" rel="noopener"}


