---
layout: post
title: "SA fossil is the long-awaited link to the evolution of turtles"
date: 2015-09-04
categories:
author: University of the Witwatersrand 
tags: [Eunotosaurus,Turtle,Reptile,Evolutionary biology]
---


Known as the diapsid condition, this pair of openings is also found in lizards, snakes, crocodilians and birds. The skull of modern turtles is anapsid - without openings - with the chamber housing the jaw muscles fully enclosed by bone. Although the new study represents a major step towards understanding the reptile tree of life, Bever emphasises that it will not be the final chapter in the science of turtle origins. They have been particularly interested in understanding the biology of Eunotosaurus, which is known from Middle Permian rocks of the South African Karoo Supergroup. What were the ecological conditions that led to the evolution of the turtle's shell and anapsid skull?

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/uotw-sfi090315.php){:target="_blank" rel="noopener"}


