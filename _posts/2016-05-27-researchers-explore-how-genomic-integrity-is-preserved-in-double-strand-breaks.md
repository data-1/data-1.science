---
layout: post
title: "Researchers explore how genomic integrity is preserved in double-strand breaks"
date: 2016-05-27
categories:
author: Christopher Packham
tags: [DNA repair,DNA,DNA replication,Genome,Endonuclease,Base pair,Alu element,Restriction enzyme,Genome instability,MUS81,Mutation,Mutagenesis,Molecular biology,Genetics,Biotechnology,Biochemistry,Biology,Molecular genetics,Branches of genetics,Macromolecules,Nucleic acids,Life sciences,Nucleotides,Biomolecules,Cell biology]
---


Researchers studying eukaryotic DNA reporting this week in Science have found strong experimental evidence that an endonuclease called Mus81 operating at double strand breaks suppresses the template switches associated with genome instability. To determine the role of the Mus81 endonuclease in suppressing mutations during BIR events, the researchers bred a Mus81 knockout variant of a particular eukaryotic cell and induced double strand DNA breaks. The two resulting branches become the template for the resulting replicated strands as polymerases match up nucleotides with their compliments. Mus81 and converging forks limit the mutagenicity of replication fork breakage.14 August 2015: Vol. Mus81 suppresses template switches between both homologous sequences and diverged human Alu repetitive elements, highlighting its importance for stability of highly repetitive genomes.

<hr>

[Visit Link](http://phys.org/news/2015-08-explore-genomic-double-strand.html){:target="_blank" rel="noopener"}


