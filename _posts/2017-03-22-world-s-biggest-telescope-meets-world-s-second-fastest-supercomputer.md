---
layout: post
title: "World's biggest telescope meets world's second fastest supercomputer"
date: 2017-03-22
categories:
author: "International Centre For Radio Astronomy Research, Icrar"
tags: [Square Kilometre Array,Computer science,Technology,Computing,Information technology,Information Age]
---


Credit: Prof. Yutong Lu  A prototype part of the software system to manage data from the Square Kilometre Array (SKA) telescope has run on the world's second fastest supercomputer in China. Professor Wicenec said the novel execution framework of the science data processor is data activated, meaning individual data items are wrapped in an active piece of software that automatically triggers the applications needed to process it. Professor An said the prototype was initially run on 500 compute nodes of the supercomputer and then extended to 1000 nodes. Professor Wicenec said the system is now running 66,000 items and the next stage will be a few million. Then we'll run between 50 and 60 million items on 8500 or 10,000 nodes, he said.

<hr>

[Visit Link](http://phys.org/news/2016-08-world-biggest-telescope-fastest-supercomputer.html){:target="_blank" rel="noopener"}


