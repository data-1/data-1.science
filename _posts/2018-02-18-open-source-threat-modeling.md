---
layout: post
title: "Open Source Threat Modeling"
date: 2018-02-18
categories:
author: "The Linux Foundation"
tags: [Threat model,Cross-platform software,Database,Open-source software,GitHub,Application software,Desktop environment,Free and open-source software,Design,Linux,World Wide Web,Microsoft Windows,Digital signature,Computer engineering,Computing,Technology,Information Age,Software,Systems engineering,Information technology management,Computer science,Software engineering,Software development,Information technology]
---


What is threat modeling? Application threat modeling is a structured approach to identifying ways that an adversary might try to attack an application and then designing mitigations to prevent, detect or reduce the impact of those attacks. Why threat modeling? My preferred type of diagram is a Data Flow Diagram with trust boundaries:  Identify threats – In this stage, the threat modeling team ask questions about the component parts of the application and (very importantly) the interactions or data flows between them to guess how someone might try to attack it. If you want a tool to help you, try OWASP Threat Dragon!

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/open-source-threat-modeling/){:target="_blank" rel="noopener"}


