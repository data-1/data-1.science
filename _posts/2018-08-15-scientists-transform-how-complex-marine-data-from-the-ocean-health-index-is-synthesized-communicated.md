---
layout: post
title: "Scientists transform how complex marine data from the Ocean Health Index is synthesized, communicated"
date: 2018-08-15
categories:
author: "Julie Cohen, University Of California - Santa Barbara"
tags: [Science,National Center for Ecological Analysis and Synthesis,Data,Computing,Branches of science,Technology]
---


Employing several free software solutions that upgrade collaborative research, the team has been able to make its entire workflow more transparent and streamlined while enhancing the reproducibility of their data methods. We combine best practices from ocean sciences and data science to help inform the management of coastlines around the world, says co-author Benjamin Halpern, NCEAS director and OHI chief scientist. We became data scientists, which means, among other things, coding collaboratively the way software developers do. We've used these tools to make our methods more repeatable, but what has been game-changing is that we can also use them to build interactive reports and websites so that others can build from our methods directly. We've been able to reimagine the way our science is done, communicated, shared and used, Lowndes said.

<hr>

[Visit Link](https://phys.org/news/2017-05-scientists-complex-marine-ocean-health.html){:target="_blank" rel="noopener"}


