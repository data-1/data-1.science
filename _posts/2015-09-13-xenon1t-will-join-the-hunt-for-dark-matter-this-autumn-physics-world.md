---
layout: post
title: "XENON1T will join the hunt for dark matter this autumn – Physics World"
date: 2015-09-13
categories:
author: "$author"   
tags: [XENON,Weakly interacting massive particles,Physical sciences,Astroparticle physics,Astronomy,Astrophysics,Nature,Science,Particle physics,Physics]
---


The goal of most direct-detection dark-matter experiments is to observe a weakly interacting massive dark-matter particle (WIMP) as it scatters off the nucleus of an atom. If dark matter is a particle at all, then WIMPs are our current best candidate. Our first challenge is that the energy you expect is extremely low,” explains Aprile. While the observation of such scattering events has yet to be confirmed in any detector, physicists can put limits on certain properties of WIMPs by running experiments for several years. First data collection begins this autumn.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/owJxIsUzOFU/xenon1t-will-join-the-hunt-for-dark-matter-this-autumn){:target="_blank" rel="noopener"}


