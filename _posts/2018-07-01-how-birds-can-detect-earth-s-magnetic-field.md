---
layout: post
title: "How birds can detect Earth's magnetic field"
date: 2018-07-01
categories:
author: "Lund University"
tags: [Magnetoreception]
---


The receptors that sense the Earth's magnetic field are probably located in the birds' eyes. Now, researchers at Lund University have studied different proteins in the eyes of zebra finches and discovered that one of them differs from the others: only the Cry4 protein maintains a constant level throughout the day and in different lighting conditions. This is something we expect from a receptor that is used regardless of the time of day, explains Atticus Pinzón-Rodríguez, one of the researchers behind the study. The conclusion is thus that this specific protein helps the magnetic sense to function, while other cryptochromes, whose levels in the body vary at different times of the day, take care of the biological clock instead. Last year, Atticus Pinzón-Rodríguez and his colleagues noted that not only migratory birds navigate using a magnetic compass.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180406091756.htm){:target="_blank" rel="noopener"}


