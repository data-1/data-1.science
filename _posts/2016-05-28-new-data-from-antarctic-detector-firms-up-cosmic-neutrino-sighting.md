---
layout: post
title: "New data from Antarctic detector firms up cosmic neutrino sighting"
date: 2016-05-28
categories:
author: Terry Devitt, University Of Wisconsin-Madison
tags: [IceCube Neutrino Observatory,Neutrino,Neutrino astronomy,Astrophysics,Nature,Science,Particle physics,Space science,Astronomy,Physics,Physical sciences]
---


In the new study, the detection of 21 ultra high-energy muons—secondary particles created on the very rare occasions when neutrinos interact with other particles —provides independent confirmation of astrophysical neutrinos from our galaxy as well as cosmic neutrinos from sources outside the Milky Way. The particles created in these events, including neutrinos and cosmic rays, are accelerated to energy levels that exceed the record-setting earthbound accelerators such as the Large Hadron Collider (LHC) by a factor of more than a million. Credit: University of Wisconsin-Madison  The latest observations were made by pointing the Ice Cube Observatory—composed of thousands of optical sensors sunk deep beneath the Antarctic ice at the South Pole—through the Earth to observe the Northern Hemisphere sky. It was detected by the IceCube Neutrino Observatory at the South Pole on Oct. 28, 2010. Credit: IceCube Collaboration  Albrecht Karle, a UW-Madison professor of physics and a senior author of the Physical Review Letters report, notes that while the neutrino-induced tracks recorded by the IceCube detector have a good pointing resolution, within less than a degree, the IceCube team has not observed a significant number of neutrinos emanating from any single source. It is sound confirmation that the discovery of cosmic neutrinos from beyond our galaxy is real.

<hr>

[Visit Link](http://phys.org/news/2015-08-antarctic-detector-firms-cosmic-neutrino.html){:target="_blank" rel="noopener"}


