---
layout: post
title: "New type of photosynthesis discovered"
date: 2018-06-17
categories:
author: "Imperial College London"
tags: [Photosynthesis,Chlorophyll,Cyanobacteria,Infrared,Light,Chemistry,Nature]
---


As scientists have now discovered, it also occurs in a cupboard fitted with infrared LEDs in Imperial College London. The new research shows that instead chlorophyll-f plays the key role in photosynthesis under shaded conditions, using lower-energy infrared light to do the complex chemistry. This is photosynthesis 'beyond the red limit'. Co-author Dr Andrea Fantuzzi, from the Department of Life Sciences at Imperial, said: Finding a type of photosynthesis that works beyond the red limit changes our understanding of the energy requirements of photosynthesis. This provides insights into light energy use and into mechanisms that protect the systems against damage by light.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/icl-nto061418.php){:target="_blank" rel="noopener"}


