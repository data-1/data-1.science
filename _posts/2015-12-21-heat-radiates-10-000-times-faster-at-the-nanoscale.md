---
layout: post
title: "Heat radiates 10,000 times faster at the nanoscale"
date: 2015-12-21
categories:
author: University of Michigan
tags: [Thermal radiation,Heat transfer,Heat,Electromagnetic radiation,Nanotechnology,Light,Physical quantities,Physics,Applied and interdisciplinary physics,Physical sciences,Science,Chemistry,Physical phenomena,Physical chemistry,Electromagnetism]
---


We've shown, for the first time, the dramatic enhancements of radiative heat fluxes in the extreme near-field, said Pramod Reddy, associate professor of mechanical engineering and materials science and engineering. Our experiments and calculations imply that heat flows several orders of magnitude faster in these ultra small gaps. The phenomenon the researchers studied is radiative heat--the electromagnetic radiation, or light, that all matter above absolute zero emits. His model accurately describes heat transfer across large to relatively small voids, reaching to 10 micrometers at room temperature. When creating nanoscale gaps such as those required for our nanoscale heat radiation experiments, the slightest perturbation can ruin an experiment.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/uom-hr1121015.php){:target="_blank" rel="noopener"}


