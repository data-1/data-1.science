---
layout: post
title: "LHC's objective—maximum intensity"
date: 2017-09-20
categories:
author: "Corinne Pralavorio"
tags: [Large Hadron Collider,Particle accelerator,Electron,Collider,Science,Particle physics,Applied and interdisciplinary physics,Physics]
---


Trains of proton bunches have been circulating in the machine for the past week. Despite the ultra-high vacuum, residual gas molecules and electrons remain trapped on the walls of the vacuum chambers. This phenomenon, known as the electron cloud, is amplified by the large number of proton bunches and the short distance between the bunches in the beam. To mitigate the impact of these clouds, the vacuum chamber can be conditioned with the beam itself. Increasing the number of circulating bunches frees as many molecules of gas as can be sustained and causes a massive release of electron clouds.

<hr>

[Visit Link](https://phys.org/news/2017-06-lhc-objectivemaximum-intensity.html){:target="_blank" rel="noopener"}


