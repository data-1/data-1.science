---
layout: post
title: "Rise in oxygen levels links to ancient explosion of life, researchers find"
date: 2017-11-30
categories:
author: "Washington University in St. Louis"
tags: [Ordovician,Ocean,Biodiversity,Cambrian explosion,Physical sciences,Geology,Natural environment,Physical geography,Nature,Earth sciences,Oceanography]
---


A team of researchers, including a faculty member and postdoctoral fellow from Washington University in St. Louis, found that oxygen levels appear to increase at about the same time as a three-fold increase in biodiversity during the Ordovician Period, between 445 and 485 million years ago, according to a study published Nov. 20 in Nature Geoscience. Asteroid impacts were among the many disruptions studied as the reasons for such an explosion of change. They cite a nearly 80-percent increase in oxygen levels where oxygen constituted about 14 percent of the atmosphere during the Darriwilian Stage (Middle Ordovician 460-465 million years ago) and increased to as high as 24 percent of the atmosphere by the mid-Katian (Late Ordovician 450-455 million years ago). This study suggests that atmospheric oxygen levels did not reach and maintain modern levels for millions of years after the Cambrian explosion, which is traditionally viewed as the time when the ocean-atmosphere was oxygenated, Edwards said. Oxygen and animal life have always been linked, but most of the focus has been on how animals came to be, said Saltzman, professor and school director of Earth Sciences at Ohio State.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/wuis-rio111717.php){:target="_blank" rel="noopener"}


