---
layout: post
title: "Researchers develop data bus for quantum computer"
date: 2018-04-26
categories:
author: "University of Innsbruck"
tags: [Quantum error correction,Quantum mechanics,Quantum computing,Quantum information,News aggregator,Theoretical physics,Theoretical computer science,Applied mathematics,Branches of science,Science,Information Age,Computer science,Technology,Physics,Computing]
---


Although systems can be protected from noise in principle, researchers have been able to build only small prototypes of quantum computers experimentally. These logical quantum bits or qubits are more robust against noise. Physicists Hendrik Poulsen Nautrup and Hans Briegel from the Institute of Theoretical Physics of the University of Innsbruck and Nicolai Friis, now at the Institute of Quantum Optics and Quantum Information in Vienna, have found a technique to transfer quantum information between systems that are encoded differently. Scientists have already built small-scale quantum processors and memories experimentally, and they have used different protocols to encode logical qubits: For example, for quantum processors they use so-called color codes and for quantum memories surface codes. Similar to a data bus in a conventional computer, scientists can use this technique to connect the components of a quantum computer, explains Poulsen Nautrup.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/11/171106100525.htm){:target="_blank" rel="noopener"}


