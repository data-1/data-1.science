---
layout: post
title: "Endless oscillations: A theoretical study on quantum systems"
date: 2015-07-26
categories:
author: International School Of Advanced Studies, Sissa
tags: [Quantum mechanics,Entropy,Second law of thermodynamics,Laws of thermodynamics,Physics,Energy,Time,Thermodynamic equilibrium,Thermodynamics,Information,Applied mathematics,Science,Physical sciences,Applied and interdisciplinary physics,Theoretical physics]
---


A quantum system never relaxes. An isolated system (like a cloud of cold atoms trapped in optical grids) will endlessly oscillate between its different configurations without ever finding peace. In practice, these types of systems are unable to dissipate energy in any form. This is the exact opposite of what happens in classical physics, where the tendency to reach a state of equilibrium is such a fundamental drive that is has been made a fundamental law of physics, i.e., the second law of thermodynamics, which introduces the concept of entropy. This profound difference is the subject of a study published in Physical Review A, conducted with the collaboration of the International School of Advanced Studies (SISSA) of Trieste and the University of Oxford.

<hr>

[Visit Link](http://phys.org/news352002810.html){:target="_blank" rel="noopener"}


