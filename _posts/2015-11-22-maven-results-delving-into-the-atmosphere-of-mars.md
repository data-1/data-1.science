---
layout: post
title: "MAVEN results: Delving into the atmosphere of Mars"
date: 2015-11-22
categories:
author: American Association for the Advancement of Science (AAAS) 
tags: [Aurora,Mars,Atmosphere,Atmosphere of Earth,MAVEN,Solar wind,Atmosphere of Mars,Magnetosphere,Earth,Geophysics,Nature,Space science,Astronomy,Planetary science,Physical sciences,Outer space,Planets,Astronomical objects,Planets of the Solar System,Bodies of the Solar System,Solar System]
---


This issue of Science features four studies highlighting results from the Mars Atmosphere and Volatile Evolution (MAVEN) mission, designed to study Mars' upper atmosphere, ionosphere, and magnetosphere. Measurements from the upper atmosphere of Mars reveal that the escape rate of ions is enhanced during solar bursts, hinting at how substantial atmospheric loss could have occurred in early Martian history. During this event, instruments on MAVEN that were monitoring Mars' magnetic field detected strong magnetic rotations that fluxed in rope-like tendrils up to 5,000 kilometers (3,107 miles) into space. Meanwhile, instruments that monitor atmospheric ionization noted dramatic spikes as this ICME struck the Red Planet, where planetary ions spewed into space, concentrated along the flux ropes of the affected magnetic field. AGU will also issue a press release at that time about the complementary research.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-11/aaft-mrd110215.php){:target="_blank" rel="noopener"}


