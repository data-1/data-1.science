---
layout: post
title: "‘Quantum radio’ could improve communications in difficult environments – Physics World"
date: 2018-06-21
categories:
author: ""
tags: [Magnetometer,Electrical engineering,Technology,Telecommunications engineering,Electronics,Electromagnetism,Electricity,Telecommunications,Information and communications technology,Science]
---


Currently being developed by Dave Howe and colleagues at the National Institute of Standards and Technology (NIST) in Maryland, US, the system uses the quantum properties of specialized magnetic field sensors to transmit and receive signals. However, these signals are strongly attenuated in materials like metal, concrete, soil and water, making communication difficult in built-up or underwater environments. High-bandwidth communications  “The best magnetic field sensitivity is obtained using quantum sensors,” says Howe. “The increased sensitivity leads in principle to longer communications range,” he explains, adding that “The quantum approach also offers the possibility to get high-bandwidth communications like a cellphone has.”  The NIST team is building its quantum radio using specialized atomic magnetometers. An alternating current would then be induced in the detector, allowing for clear communication signals.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2018/jan/12/quantum-radio-could-improve-communications-in-difficult-environments){:target="_blank" rel="noopener"}


