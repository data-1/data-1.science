---
layout: post
title: "Scientists generate a new type of human stem cell that has half a genome"
date: 2016-03-25
categories:
author: The Hebrew University of Jerusalem
tags: [Ploidy,Genetics,Cell (biology),Genome,Reproduction,Stem cell,Gene,Chromosome,Biology,Life sciences,Health sciences]
---


Scientists from The Hebrew University of Jerusalem, Columbia University Medical Center (CUMC) and The New York Stem Cell Foundation Research Institute (NYSCF) have succeeded in generating a new type of embryonic stem cell that carries a single copy of the human genome, instead of the two copies typically found in normal stem cells. The only exceptions are reproductive (egg and sperm) cells, known as 'haploid' cells because they contain a single set of 23 chromosomes. Previous efforts to generate embryonic stem cells using human egg cells had resulted in diploid stem cells. One of the greatest advantages of using haploid human cells is that it is much easier to edit their genes, explained Ido Sagi, the PhD student who led the research at the Azrieli Center for Stem Cells and Genetic Research at the Hebrew University of Jerusalem. The lead authors are Nissim Benvenisty and Ido Sagi, The Azrieli Center for Stem Cells and Genetic Research, The Hebrew University, Jerusalem, and Dieter Egli, Columbia University Medical Center and New York Stem Cell Foundation Research Institute.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/thuo-sga031416.php){:target="_blank" rel="noopener"}


