---
layout: post
title: "An earlier revolution: genetic and genomic analyses reveal pre-existing cultural differences leading to Neolithization"
date: 2017-09-12
categories:
author: "Leonardi, Department Of Life Sciences, Biotechnology, University Of Ferrara, Ferrara, Centre For Geogenetics, Natural History Museum Of Denmark, University Of Copenhagen, Copenhagen, Barbujani"
tags: []
---


Our global panel of populations revealed marked differences in inferred N e between food producers and hunter-gatherers: as expected, the latter show larger effective population size. Indeed, even if the individual trajectories may vary between NeON and MSMC, both datasets show the same signal of a difference between foragers and food producers that started before food production in agricultural communities. Whatever the reason of this apparent decline could be, our analyses show that effective population sizes began to become larger in the ancestors of today’s food producers than in the ancestors of today’s foragers before the Neolithic transition (Fig. An early increase in N e that predates Neolithization has been interpreted as capturing early societal changes that might have favored the later development of food production49. However, the key result from our analysis is that, even when using a combination of genomes and a large amount of genome-wide data from a globally-distributed panel of populations, populations that later adopted food productions differed from those who remained hunter-gatherers well before their lifestyle changed.

<hr>

[Visit Link](http://www.nature.com/articles/s41598-017-03717-6?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


