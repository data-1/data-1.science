---
layout: post
title: "Open Source Election System Certified"
date: 2018-04-22
categories:
author: ""
tags: [Open source,Open-source software,Open Source Initiative,Free software movement,Intellectual property law,Information technology,Software,Applied ethics,Open-source movement,Technology,Computing,Intellectual works]
---


OSI Affiliate Member, The National Association of Voting Officials (NAVO), announced this week the certification of the Prime lll open source election system for the State of Ohio. NAVO spokesperson Brent Turner stated the ballot delivery system is, “the first step toward appropriately secure voting systems replacing the ‘secret software‘ systems that have plagued our democracy“. “We have been focused on Florida, California, and New York to upgrade security and reduce costs as well. Now is the historic moment for all the states to step up and defend our democracy. Recently State of Hawaii Congresswoman Tulsi Gabbard announced Federal legislation embracing the movement toward open source / publicly owned paper ballot systems (see, Rep. Tulsi Gabbard Introduces Securing America’s Elections Act to Ensure Integrity of 2018 Elections, https://gabbard.house.gov/secureelections)  Submitted by Brent Turner, The National Association of Voting Officials

<hr>

[Visit Link](https://opensource.org/node/929){:target="_blank" rel="noopener"}


