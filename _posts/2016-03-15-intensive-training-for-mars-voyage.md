---
layout: post
title: "Intensive training for Mars voyage"
date: 2016-03-15
categories:
author:  
tags: [Trace Gas Orbiter,ExoMars,Spacecraft,Sky,Mars,Astronomical objects known since antiquity,Space probes,Exploration of Mars,Missions to the planets,Space missions,Spaceflight,Outer space,Astronautics,Flight,Space vehicles,Space exploration,Space science,Space programs,Astronomy,Spaceflight technology,Discovery and exploration of the Solar System,Aerospace,Rocketry,Space agencies,Space research]
---


The complex and challenging mission will be operated by teams at ESA’s ESOC control centre in Darmstadt, Germany, where, after months of simulations covering all phases of the journey to Mars, training is in the final, intensive phase. “While we’ll be monitoring TGO’s liftoff and the boost phase very closely, in fact, for us, the most critical moment occurs after the spacecraft separates from the launcher upper stage, when it sends its first signals,” says ExoMars Spacecraft Operations Manager Peter Schmitz. That’s when we’ll have a mission.” Acquisition of first signal – AOS – is expected at 21:28 GMT (22:28 CET) on 14 March, just 12 hours after liftoff. It takes a team of teams Flight dynamics team Peter and the 14-strong Flight Control Team will be supported by additional experts from across the centre and the project team at ESA’s ESTEC technical centre in the Netherlands, providing specialised knowledge and capabilities in areas such as deep-space mission operations, flight dynamics, ground stations and software and systems. ExoMars 2016: launch to Mars  Additional simulations are planned during the interplanetary journey, focusing on critical activities such as the mid-course trajectory correction, Schiaparelli’s separation and the TGO’s insertion into Mars orbit.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Operations/Intensive_training_for_Mars_voyage){:target="_blank" rel="noopener"}


