---
layout: post
title: "LHCb unveils new particles"
date: 2016-07-10
categories:
author: ""
tags: [LHCb experiment,Quark,Hadron,Particle physics,Meson,Jpsi meson,Nuclear physics,Science,Physics,Standard Model,Quantum field theory,Quantum mechanics,Subatomic particles,Physical sciences,Quantum chromodynamics,Theoretical physics]
---


Credit: Claudia Marcelloni/CERN  On 28 June, the LHCb collaboration reported the observation of three new exotic particles and the confirmation of the existence of a fourth one in data from the Large Hadron Collider (LHC). But, in the last decade several collaborations have found evidence of the existence of particles formed by more than three quarks. For example, in 2009 the CDF collaboration found one of these, called X(4140) – where the number in parentheses is its reconstructed mass in megaelectronvolts. The LHCb collaboration could determine the X(4140) quantum numbers with high precision. Even though the four particles all contain the same quark composition, they each have a unique internal structure, mass and their own sets of quantum numbers.

<hr>

[Visit Link](http://phys.org/news/2016-07-lhcb-unveils-particles.html){:target="_blank" rel="noopener"}


