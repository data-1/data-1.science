---
layout: post
title: "The science behind the smell of the sea (video)"
date: 2016-05-09
categories:
author: American Chemical Society
tags: [American Chemical Society,Academia,Physical sciences,Earth sciences,Science]
---


WASHINGTON, June 22 -- There's nothing like the smell of salty sea air over summer vacation. Kimberly Prather, Ph.D., and her team have discovered in research published in ACS Central Science that SSAs have a huge impact on the planet's climate. Take a break from the beach and check out the video here: http://youtu.be/qBX4Vl6-SMM. ###  Subscribe to the ACS channel at http://bit.ly/ACSYoutube and follow us on Twitter at @ACSPressroom. The American Chemical Society is a nonprofit organization chartered by the U.S. Congress.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/acs-tsb062215.php){:target="_blank" rel="noopener"}


