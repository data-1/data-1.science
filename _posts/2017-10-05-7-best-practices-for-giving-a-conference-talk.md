---
layout: post
title: "7 best practices for giving a conference talk"
date: 2017-10-05
categories:
author: "VM (Vicky) Brasseur
(Alumni)"
tags: [Public speaking,Experience,Internet Archive]
---


Believe me, your audience can tell and they don't like it. When you do deliver your talk, don't read from your slides or speaker notes. If the demo does not work and you shrug and say, Well, we'll just have to get back to that later if there's time and then move on with the rest of the content, the audience is unlikely to remember the failure. It's okay if your demo fails—it happens all the time and audiences are used to it. Do you have slides?

<hr>

[Visit Link](https://opensource.com/article/17/9/7-best-practices-giving-conference-talk){:target="_blank" rel="noopener"}


