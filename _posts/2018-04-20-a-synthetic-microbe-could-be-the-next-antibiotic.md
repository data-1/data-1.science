---
layout: post
title: "A Synthetic Microbe Could Be the Next Antibiotic"
date: 2018-04-20
categories:
author: ""
tags: [Antimicrobial resistance,Antibiotic,Bacteria,Drug resistance,Infection,Medical specialties,Microbiology,Clinical medicine,Immunology,Biology,Health,Medicine,Chemistry,Life sciences,Health sciences]
---


Scientists at IBM Research, the world's largest industrial research organization, have come up with a synthetic molecule that works as a last ditch effort against superbugs that have already spread to every organ in the body, travelling through the blood. Usually, the immune system attacks bacteria by destroying their protective membranes, and IBM's synthetic molecule had been designed to work the same way. How it kills superbugs. It basically just comes in, kills the bacteria, degrades, and leaves, Hedrick told Popular Science, adding that this approach could also mitigate antibiotic resistance or work against extremely resistant strains even when the bacteria has evolved. So far, the IBM researchers have successfully tested their synthetic molecule on mice infected with five difficult-to-treat, multi-drug resistant superbugs that can be commonly acquired in hospitals and often lead to sepsis or even death.

<hr>

[Visit Link](https://futurism.com/synthetic-microbe-next-antibiotic/){:target="_blank" rel="noopener"}


