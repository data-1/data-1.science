---
layout: post
title: "Happy 24th Birthday, Debian!"
date: 2018-07-12
categories:
author: "Marius Nestor, Aug"
tags: []
---


Today, August 16, 2017, Debian, the universal, Unix-like computer operating system powered by the Linux kernel turns 24 years of existence since the late Ian Murdock first announced the Debian Project back in 1993. Since then, the Debian Project decided to set the day of August 16 as the Debian Day, to celebrate the project's anniversary each year with organized social gatherings in various parts of the world. The latest release of Debian GNU/Linux is Stretch, version 9.1.0, whose Live and installable ISO images can be downloaded through our web portal or from any of the official mirrors from the project's homepage. The OS is officially supported on ten hardware architectures. On this occasion, we'd like to wish Debian a happy birthday, and we can't wait to see what the upcoming Debian GNU/Linux 10 Buster release has in store for fans of the Linux-based operating system.

<hr>

[Visit Link](https://linux.softpedia.com/blog/happy-24th-birthday-debian-517413.shtml){:target="_blank" rel="noopener"}


