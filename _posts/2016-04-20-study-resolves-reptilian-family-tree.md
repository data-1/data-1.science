---
layout: post
title: "Study resolves reptilian family tree"
date: 2016-04-20
categories:
author: Daniel Stolte, University Of Arizona
tags: [Mosasaur,Lizard,Squamata,Snake,Evolution,Reptile,Biology,Taxa,Animals]
---


Credit: Wikimedia Commons  A new study has helped settle the controversial relationships among the major groups of lizards and snakes, and it sheds light on the origins of a group of giant fossil lizards. Anatomical data put iguanians at the base of the tree, whereas molecular data suggest that the iguanians evolved more recently and are closely related to snakes and a group including the monitor and alligator lizards, called the anguimorphs, said John J. Wiens, a professor in the Department of Ecology and Evolutionary Biology in the UA College of Science. When they analyzed this reduced molecular dataset with all 691 anatomical characters, the results still supported the molecular hypothesis, placing iguanians with snakes and anguimorphs instead of at the base of the tree. The new study combined data from both living and fossil species and revealed mosasaurs to be close relatives of snakes, and only distantly related to monitor lizards and species at the base of the tree. Our results show how combining molecular and anatomical data can reveal evolutionary relationships of fossil species that one might not predict from the anatomical data alone.

<hr>

[Visit Link](http://phys.org/news347519818.html){:target="_blank" rel="noopener"}


