---
layout: post
title: "Earth’s first missions to a binary asteroid, for planetary defence"
date: 2018-08-16
categories:
author: ""
tags: [AIDA (international space cooperation),Asteroid,Hera (space mission),Dimorphos,Double Asteroid Redirection Test,65803 Didymos,Asteroid impact avoidance,Solar System,Asteroids,Planetary defense,Spacecraft,Spaceflight,Astronautics,Bodies of the Solar System,Local Interstellar Cloud,Discovery and exploration of the Solar System,Space exploration,Planetary science,Flight,Outer space,Minor planets,Science,Astronomy,Space science,Astronomical objects]
---


ESA’s proposed Hera mission would also be Europe’s contribution to an ambitious planetary defence experiment. “Such a binary asteroid system is the perfect testbed for a planetary defence experiment but is also an entirely new environment for asteroid investigations. Fortunately we can count on the unique experience of ESA’s Rosetta operations team which is an incredible asset for the Hera mission.”  Asteroid collision The smaller Didymoon is Hera’s main focus: the spacecraft would perform high-resolution visual, laser and radio science mapping of the moon, which will be the smallest asteroid visited so far, to build detailed maps of its surface and interior structure. The impact will lead to a change in the duration of Didymoon’s orbit around the main body. “Hera’s survey will give us the mass of Didymoon, the shape of the crater, as well as physical and dynamical properties of Didymoon.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/Hera/Earth_s_first_mission_to_a_binary_asteroid_for_planetary_defence){:target="_blank" rel="noopener"}


