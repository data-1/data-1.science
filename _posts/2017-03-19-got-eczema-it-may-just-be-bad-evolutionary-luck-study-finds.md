---
layout: post
title: "Got eczema? It may just be bad evolutionary luck, study finds"
date: 2017-03-19
categories:
author: "University at Buffalo"
tags: [Mutation,Evolution,Genetic disorder,Gene,Dermatitis,News aggregator,Genetics,Biology,Evolutionary biology,Biological evolution]
---


Unlike other disease variants, such as those linked to sickle cell anemia or psoriasis, the one we studied is just not that important from the standpoint of evolution. advertisement  The study found that loss of function mutations are significantly more common in filaggrin than in other human genes. However, despite this prevalence, the variants don't appear to serve an adaptive purpose: The loss of function creates a susceptibility to eczema, but it does not appear to have an effect on the reproductive success of modern humans, Gokcumen says. A genomic analysis showed that this trend may have been an artifact of evolutionary hitchhiking: In East Asia, many people share a genetic profile that includes a loss-of-function mutation for filaggrin, paired with a notable mutation in a nearby gene that carries instructions for making hornerin, another human protein. While the filaggrin mutation did not appear to have importance from the standpoint of evolution and adaptation, the hornerin mutation did.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/10/161005124344.htm){:target="_blank" rel="noopener"}


