---
layout: post
title: "Glitter from silver lights up Alzheimer's dark secrets"
date: 2016-06-14
categories:
author: "Tata Institute of Fundamental Research"
tags: [Alzheimers disease,News aggregator,Amyloid beta,Cell membrane,Chemistry]
---


While the origin of Alzheimer's Disease, one that robs the old of their memory, is still hotly debated, it is likely that a specific form of the Amyloid beta molecule, which is able to attack cell membranes, is a major player. Everybody wants to make the key to solve Alzheimer's Disease, but we don't know what the lock looks like. The lock looks like a bunch of amyloid beta molecules in the shape of a hairpin, but with a twist. This may allow these bunch of amyloid beta molecules to form toxic pores in the cell membranes. They studied a tiny laser-induced signal from the amyloid beta which reported their shape.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150825141317.htm){:target="_blank" rel="noopener"}


