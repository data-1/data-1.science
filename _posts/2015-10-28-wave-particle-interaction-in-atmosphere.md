---
layout: post
title: "Wave-particle interaction in atmosphere"
date: 2015-10-28
categories:
author: Dartmouth College
tags: [Van Allen radiation belt,Electron,Plasma (physics),Radiation,Earth,Wave,Phases of matter,Physical chemistry,Astronomy,Physical phenomena,Electrical engineering,Space science,Applied and interdisciplinary physics,Nature,Electromagnetism,Physics,Physical sciences]
---


The findings are important because relativistic electrons can lead to ozone depletion and threaten orbital satellites, spacecraft and astronauts, and understanding the evolution of Earth's radiation belts could help lessen the effects of these particles. In their new paper, the researchers studied the resonance of relativistic electrons with electromagnetic ion cyclotron waves in the Earth's radiation belts -- in other words, how these waves affect the electrons' motion. Also known as the Van Allen radiation belts, these giant concentric layers of charged particles are held in place by the Earth's magnetic field. Previously, high density was thought to lower the minimum energy of radiation belt particles for resonance. We show that high density is not the most important factor, though it can indirectly have an effect on the minimum resonant energy by causing a particular kind of ion cyclotron wave to grow.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151027143023.htm){:target="_blank" rel="noopener"}


