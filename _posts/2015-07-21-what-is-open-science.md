---
layout: post
title: "What is open science?"
date: 2015-07-21
categories:
author: "Bryan Behrenshausen"
tags: [Open source,Science,Open science,Black hole,Technology]
---


In his autobiography, Just for Fun, Linux creator Linus Torvalds argues that the open source process tends to mirror the scientific enterprise. And like science, Torvalds suggests, open source drives innovation: It is creating things that until recently were considered impossible, and opening up unexpected new markets. Lately, however, we've been hearing more about ways the relationship between open source and science might in fact be reciprocal—how science might begin to look more like open source. Be sure to read and share our newest resource guide—What is Open Science?—to learn more about the ways openness, transparency, rapid prototyping, and collaboration are impacting the ways science is both practiced and funded. Open  Science  A collection of articles on the topic of open source software, tools, hardware, philosophies, and more in science.

<hr>

[Visit Link](http://opensource.com/life/15/7/what-open-science){:target="_blank" rel="noopener"}


