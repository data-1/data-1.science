---
layout: post
title: "Modern Melanesians have retained Denisovan DNA"
date: 2016-03-23
categories:
author: American Association for the Advancement of Science (AAAS)
tags: [American Association for the Advancement of Science,Scientific societies,Learned societies,Washington DC,Non-profit organizations,Human populations,Supraorganizations,Clubs and societies,Professional associations,Learned societies of the United States,Science and technology,Scientific organizations,Scientific supraorganizations,Science,Economy of the United States,United States]
---


In the past, ancestors of many modern human populations interbred with other hominin species that have since become extinct, such as the Neandertals and Denisovans. Mapping the gene flow of surviving genetic sequences from these species, as well as other species of hominin, helps shed light on how past interbreeding has affected human evolution. While previous studies have documented Neandertal gene flow in modern humans, much less is known about the characteristics of Denisovan DNA that persist in humans today. The researchers then mapped out the genetic flow of Neandertal and Denisovan sequences, finding that Neandertal admixture, or gene flow, occurred at least three distinct times in modern human history. These findings provide new insights into human evolution and gene flow.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/aaft-mmh031416.php){:target="_blank" rel="noopener"}


