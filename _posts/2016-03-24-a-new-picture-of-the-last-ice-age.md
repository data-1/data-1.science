---
layout: post
title: "A new picture of the last ice age"
date: 2016-03-24
categories:
author: University Of Bergen
tags: [Last Glacial Period,Ice age,Ice sheet,Ice,Eurasia,Physical geography,Glaciology,Cryosphere,Earth sciences]
---


A new reconstruction of this ice sheet shows the interaction between climate and glaciers—how the ice sheet grows and retreats  It has been several years of painstaking research. The result is the first version of the database DATED (DATabase of Eurasian Deglaciation), now available for the scientific community at the Pangaea data publisher web site (doi.pangaea.de/10.1594/PANGAEA.848117). On the basis of the new compilation, the geologists have constructed a series of maps showing how ice sheets grew and retreated across northern Europe, Russia and the Barents Sea 40,000-10,000 years ago. We need a synthesis of all the information from these sites, to truly understand the ice sheet behaviour; in particular numerical modellers need a picture of the whole ice sheet, Hughes explains. Anna L. C. Hughes et al.

<hr>

[Visit Link](http://phys.org/news/2016-03-picture-ice-age.html){:target="_blank" rel="noopener"}


