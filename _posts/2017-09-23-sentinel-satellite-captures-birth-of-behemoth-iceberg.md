---
layout: post
title: "Sentinel satellite captures birth of behemoth iceberg"
date: 2017-09-23
categories:
author: ""
tags: [Larsen Ice Shelf,Ice shelf,Cryosphere,Glaciology,Earth sciences,Physical geography,Oceanography,Hydrography,Hydrology,Earth phenomena]
---


In January 2017 alone it travelled 20 km, reaching a total length of about 175 km. Scientists from Project MIDAS, an Antarctic research consortium led by Swansea University in the UK, used radar images from the Copernicus Sentinel-1 mission to keep a close eye on the rapidly changing situation. We will continue to monitor both the impact of this calving event on the Larsen C ice shelf, and the fate of this huge iceberg,” added Prof. Luckman. Ice crack seen by Sentinel-2A With the calving of the iceberg, about 10% of the area of the ice shelf has been removed. Previous events further north on the Larsen A and B shelves, captured by ESA’s ERS and Envisat satellites, indicate that when a large portion of an ice shelf is lost, the flow of glaciers behind can accelerate, contributing to sea-level rise.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-1/Sentinel_satellite_captures_birth_of_behemoth_iceberg){:target="_blank" rel="noopener"}


