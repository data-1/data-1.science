---
layout: post
title: "Researchers align atomic friction experiment"
date: 2016-05-10
categories:
author: University Of California - Merced
tags: [Friction,Atomic force microscopy,Stick-slip phenomenon,Simulation,Privacy,Force,Science,Technology]
---


Working together to study friction on the atomic scale, researchers at UC Merced and the University of Pennsylvania have conducted the first atomic-scale experiments and simulations of friction at overlapping speeds. To get around this problem, friction researchers often use the tip of an atomic force microscope (AFM), an ultra-sensitive instrument capable of measuring nanonewton forces, as one point of contact. Because an AFM tip works much like a record needle, researchers can measure the friction the tip experiences while it is dragged over a surface. Martini and Ye found a way to slow down their model tips, while the Penn researchers sped up their real ones. They go to Penn for a few weeks each year and work directly with their partner researchers, which helps them prepare for life after grad school.

<hr>

[Visit Link](http://phys.org/news354343847.html){:target="_blank" rel="noopener"}


