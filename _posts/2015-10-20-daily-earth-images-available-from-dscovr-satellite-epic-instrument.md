---
layout: post
title: "Daily Earth images available from DSCOVR satellite EPIC instrument"
date: 2015-10-20
categories:
author: "$author" 
tags: [Deep Space Climate Observatory,Space science,Outer space,Astronomy,Spaceflight,Science,Spacecraft,Space program of the United States,Astronautics,Bodies of the Solar System,Sky,Flight]
---


The images are taken by a NASA camera one million miles away on the Deep Space Climate Observatory (DSCOVR), a partnership between NASA, the National Oceanic and Atmospheric Administration (NOAA) and the U.S. Air Force. Once a day NASA will post at least a dozen new color images of Earth acquired from 12 to 36 hours earlier by NASA's Earth Polychromatic Imaging Camera (EPIC). Each daily sequence of images will show the Earth as it rotates, thus revealing the whole globe over the course of a day. EPIC's images of Earth allow scientists to study daily variations over the entire globe in such features as vegetation, ozone, aerosols, and cloud height and reflectivity. The DSCOVR spacecraft orbits around the L1 Lagrange point directly between Earth and the sun.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/nsfc-dei101915.php){:target="_blank" rel="noopener"}


