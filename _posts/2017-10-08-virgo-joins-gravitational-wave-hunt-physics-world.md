---
layout: post
title: "VIRGO joins gravitational-wave hunt – Physics World"
date: 2017-10-08
categories:
author: "Michael Banks"
tags: [LIGO,Virgo interferometer,Gravitational wave,Gravitational-wave observatory,Science,Gravity,General relativity,Space science,Gravitational-wave astronomy,Physical cosmology,Optics,Celestial mechanics,Physical sciences,Physics,Astrophysics,Astronomy]
---


The detector will now join in the search for gravitational waves with the US-based Advanced Laser Interferometer Gravitational-wave Observatory (aLIGO), which has two detectors located in Hanford, Washington and Livingston, Louisiana. These 40 kg mirrors, or “test masses”, are located at the end of each 3 km arm and bounce laser beams back and forth. If the light travels exactly the same distance down both arms, then two combining light waves interfere destructively, cancelling each other so that no light is observed at the photodetector. VIRGO was initially expected to join this run in March, but issues with the fragility of the glass fibres meant that engineers had to replace them with metal wires, reducing the sensitivity of the instrument. Exciting and challenging  After 25 August, VIRGO will switch off for further improvements that include replacing the metals wires with glass fibres.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/aug/02/virgo-joins-gravitational-wave-hunt){:target="_blank" rel="noopener"}


