---
layout: post
title: "International team observes first radio counterpart to gravitational waves"
date: 2017-10-17
categories:
author: "Naval Research Laboratory"
tags: [Gravitational wave,LIGO,Radio astronomy,Gravitational-wave observatory,Radio telescope,Astrophysics,Physics,Physical phenomena,Waves,Electromagnetic radiation,Science,Physical sciences,Space science,Astronomy]
---


Naval Research Laboratory (NRL), Radio Astrophysics and Sensing Section, in collaboration with an international team from the Caltech-led Global Relay of Observatories Watching Transients Happen (GROWTH) project, measured the first ever detection of radio emission from colliding neutron stars nearly 124 million light years from Earth. The team discovered radio frequency emission in the range of 3 and 6 GHz in data taken Sept. 2 -3, 2017. Recognizing the power of the new receiver, NRL and NRAO researchers developed the VLA Low Band Ionosphere and Transient Experiment (VLITE) to continuously tap into the new broadband low frequency receivers. VLITE collects data during nearly all normal VLA observations at higher frequencies, and has been monitoring the ionosphere for travelling ionospheric disturbances since 2014. It is also an excellent tool for detecting EM transients, said Dr. Tracy Clarke, radio astronomer and VLITE Project Scientist, NRL.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/nrl-ito101617.php){:target="_blank" rel="noopener"}


