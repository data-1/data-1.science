---
layout: post
title: "Researchers uncover epigenetic switches that turn stem cells into blood vessel cells"
date: 2016-05-10
categories:
author: Sharon Parmet, University Of Illinois At Chicago
tags: [Epigenetics,Stem cell,Histone,Cellular differentiation,Gene expression,Molecular biology,Genetics,Biology,Biotechnology,Cell biology,Life sciences,Biochemistry,Biological processes]
---


Changes to the proteins around which DNA is wound, called histones, can up-regulate the expression of genes by exposing them to the cellular machinery that translates their DNA. The UIC research team, led by Asrar Malik, professor and head of pharmacology in the UIC College of Medicine, studied mice to look at how several of these enzymes, known as histone demethylases, alter gene expression in embryonic stem cells undergoing transformation into mature endothelial cells. Without the two enzymes, the embryos were unable to form blood vessels. The genes that were regulated by the enzymes turned out to be promoters, or genes that turn on other genes, and were specific to endothelial cells. We only looked at a few of the genes activated by the epigenetic switches that guide stem cells into becoming endothelial cells, he said.

<hr>

[Visit Link](http://phys.org/news354470420.html){:target="_blank" rel="noopener"}


