---
layout: post
title: "What's beneath Hawaii's most active volcano?"
date: 2016-04-15
categories:
author: Katie Jacobs, Pennsylvania State University
tags: [Interferometric synthetic-aperture radar,Klauea,Deformation (volcanology),Volcano,The Volcano (British Columbia),Types of volcanic eruptions,Earth sciences]
---


Kilauea is the most active of the five volcanoes that make up the island of Hawaii. The volcano has been erupting for 31 years, so obviously there's a lot of magma coming from below, said Wauthier. Credit: Christelle Wauthier  InSAR is a remote-sensing technique that combines radar data taken from satellites to create images that show subtle movements in the ground's surface, said Wauthier. Credit: Christelle Wauthier  Basically, we use what's happening on the surface of the volcano to find a 'best fit model' for what's happening underground, said Wauthier. InSAR image showing ground deformation at Kilauea.

<hr>

[Visit Link](http://phys.org/news344584850.html){:target="_blank" rel="noopener"}


