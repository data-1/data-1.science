---
layout: post
title: "How light is detected affects the atom that emits it"
date: 2016-05-14
categories:
author: Washington University in St. Louis
tags: [Light,Spontaneous emission,Photon,Emission spectrum,Atom,Quantum mechanics,Waveparticle duality,Electromagnetic radiation,Energy level,Optics,Atomic physics,Nature,Chemistry,Physical phenomena,Radiation,Physical chemistry,Electrodynamics,Electromagnetism,Applied and interdisciplinary physics,Theoretical physics,Atomic molecular and optical physics,Science,Physical sciences,Physics,Waves]
---


All that a photon detector can tell you about spontaneous emission is whether an atom is in its excited state or its ground state. You'd never see that if you were detecting photons, Murch said. By looking at the wave nature of light, we are able see this lovely diffusive evolution between the states, Murch said. The fact that an atom's average excitation can increase even when it decays is a sign that how we look at light might give us some control over the atoms that emitted the light, Murch said. It is possible only because of one of the weirdest of all the quantum effects: When an atom emits light, quantum physics requires the light and the atom to become connected, or entangled, so that measuring a property of one instantly reveals the value of that property for the other, no matter how far away it is.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/wuis-hli051216.php){:target="_blank" rel="noopener"}


