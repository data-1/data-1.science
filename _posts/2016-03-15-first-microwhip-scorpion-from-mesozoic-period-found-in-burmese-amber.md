---
layout: post
title: "First microwhip scorpion from Mesozoic period found in Burmese amber"
date: 2016-03-15
categories:
author:  
tags: [Fossil,Burmese amber,Amber,Palpigradi,Paleontology]
---


Electrokoenenia yaksha. Meet Electrokoenenia yaksha, a newly described type of microwhip scorpion, or palpigrade, from Myanmar, whose minute fossilised remains have been found, trapped in Burmese amber. It has been described by an international team led by Michael S. Engel of the University of Kansas and the American Museum of Natural History in the US and Diying Huang of the Nanjing Institute of Geology and Palaeontology in the People's Republic of China in Springer's journal The Science of Nature. Engel discovered a single example of Electrokoenenia yaksha while investigating the diversity of arthropods preserved in pieces of Burmese amber from the Hukawng Valley in northern Myanmar. It is the first microwhip scorpion fossil from this period to be found, and also the only one of its order known of to be contained in amber.

<hr>

[Visit Link](http://phys.org/news/2016-03-microwhip-scorpion-mesozoic-period-burmese.html){:target="_blank" rel="noopener"}


