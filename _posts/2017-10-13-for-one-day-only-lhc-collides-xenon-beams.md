---
layout: post
title: "For one day only, LHC collides xenon beams"
date: 2017-10-13
categories:
author: "Corinne Pralavorio"
tags: [Large Hadron Collider,High-energy nuclear physics,Quarkgluon plasma,Proton,Particle accelerator,Physics,Particle physics,Nuclear physics,Physical sciences,Chemistry,Nature,Applied and interdisciplinary physics]
---


The team working on the ion run in the CERN control centre as the xenon run begins. Normally, lead nuclei, which have a much greater mass, are used. The experiments will conduct the same kind of analyses with xenon ions as they do with lead ions, but, because the xenon nuclei have less mass, the geometry of the collision is different, explains Jamie Boyd, LHC programme coordinator, who is responsible for liaison between the LHC machine and experiment teams. After the xenon run in the LHC lasting a few hours, xenon nuclei will continue to circulate in the accelerator complex, but only as far as the SPS. Credit: CERN  Explore further Accelerating argon

<hr>

[Visit Link](https://phys.org/news/2017-10-day-lhc-collides-xenon.html){:target="_blank" rel="noopener"}


