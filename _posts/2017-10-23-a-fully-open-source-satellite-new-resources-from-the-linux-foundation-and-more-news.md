---
layout: post
title: "A fully open source satellite, new resources from the Linux Foundation, and more news"
date: 2017-10-23
categories:
author: "Robin Muilwijk"
tags: [Free and open-source software,Red Hat,Open-source software,Linux Foundation,Internet of things,Linux,Information technology,Open content,Information technology management,Information Age,Digital media,Free content,Intellectual works,Computer engineering,Free software,Open-source movement,Software development,Software engineering,Computer science,Software,Technology,Computing]
---


In this edition of our open source news roundup, we take a look at the first open source satellite, The Linux Foundation's new IoT-focused group, and more. Open source news roundup for April 15-28, 2017  Launching the first open source satellite  Last week, the first open source satellite made its way into space. The open source cube satellite, called UPSat, was sent to the International Space Station on the Orbital ATK launch. This is why the Linux Foundation, along with 50 companies, has announced The EdgeX Foundry. The Linux Foundation and FSFE introduces new OSS resources  In an effort to simplify free and open-source software license identification and compliance, The Linux Foundation and Free Software Foundation Europe (FSFE) released multiple new resources.

<hr>

[Visit Link](https://opensource.com/article/17/4/news-april-29){:target="_blank" rel="noopener"}


