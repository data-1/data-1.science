---
layout: post
title: "Solar Observatory discovers its 3,000th comet"
date: 2016-07-02
categories:
author: "Karen C. Fox, Nasa'S Goddard Space Flight Center"
tags: [Solar and Heliospheric Observatory,Sun,Comet,Goddard Space Flight Center,Astronomy,Outer space,Solar System,Bodies of the Solar System,Astronomical objects,Space science,Physical sciences,Local Interstellar Cloud,Science,Spaceflight,Stellar astronomy]
---


This is the 3,000th comet discovered in the data from that space telescope since it launched in 1995. Red represents the Kreutz family of comets, which count for 85 percent of all the comets found by the Solar and Heliospheric Observatory. Credit: NASA's Goddard Space Flight Center/Bridgman/Duberstein  SOHO's great success as a comet finder is dependent on the people who sift through its data - a task open to the world as the data is publicly available online in near-real time. Credit: NASA's Goddard Space Flight Center/Duberstein  Watching these sungrazing comets also help us learn about our sun. At almost 20 years old, the SOHO mission is a respected elder in NASA's Heliophysics System Observatory - the fleet of spacecraft that both watches the sun and measure its effects near Earth and throughout the solar system.

<hr>

[Visit Link](http://phys.org/news/2015-09-solar-observatory-3000th-comet.html){:target="_blank" rel="noopener"}


