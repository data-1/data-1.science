---
layout: post
title: "Homo sapiens developed a new ecological niche that separated it from other hominins"
date: 2018-08-01
categories:
author: "Max Planck Society"
tags: [Homo,Human,Ecology,Human evolution,Hominidae,Ecological niche,Neanderthal,Homo erectus]
---


Defining the 'generalist specialist' niche for Pleistocene Homo sapiens. 10.1038/s41562-018-0394-4  Critical review of growing archaeological and palaeoenvironmental datasets relating to the Middle and Late Pleistocene (300-12 thousand years ago) hominin dispersals within and beyond Africa, published today in Nature Human Behaviour, demonstrates unique environmental settings and adaptations for Homo sapiens relative to previous and coexisting hominins such as Homo neanderthalensis and Homo erectus. In contrast to our ancestors and contemporary relatives, our species not only colonized a diversity of challenging environments, including deserts, tropical rainforests, high altitude settings, and the palaeoarctic, but also specialized in its adaptation to some of these extremes. Deserts, rainforests, mountains, and the arctic  In contrast to these other members of the genus Homo, our species - Homo sapiens - had expanded to higher-elevation niches than its hominin predecessors and contemporaries by 80-50,000 years ago, and by at least 45,000 years ago was rapidly colonizing a range of palaeoarctic settings and tropical rainforest conditions across Asia, Melanesia, and the Americas. 10.1038/s41562-018-0394-4  Finding the origins of this ecological 'plasticity', or the ability to occupy a number of very different environments, currently remains difficult in Africa, particularly back towards the evolutionary origins of Homo sapiens 300-200,000 years ago.

<hr>

[Visit Link](https://phys.org/news/2018-07-homo-sapiens-ecological-niche-hominins.html){:target="_blank" rel="noopener"}


