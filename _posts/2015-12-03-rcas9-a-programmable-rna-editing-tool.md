---
layout: post
title: "RCas9: A programmable RNA editing tool"
date: 2015-12-03
categories:
author: "$author" 
tags: [Cas9,CRISPR,DNA,RNA,Jennifer Doudna,Biology,Life sciences,Molecular genetics,Nucleic acids,Chemistry,Biochemistry,Molecular biology,Genetics,Branches of genetics,Nucleotides,Macromolecules,Biomolecules,Structural biology,Biotechnology]
---


A powerful scientific tool for editing the DNA instructions in a genome can now also be applied to RNA, the molecule that translates DNA's genetic instructions into the production of proteins. A team led by Jennifer Doudna, biochemist and leading authority on the CRISPR/Cas9 complex, showed how the Cas9 enzyme can work with short DNA sequences known as PAM, for protospacer adjacent motif, to identify and bind with specific site of single-stranded RNA (ssRNA). Using specially designed PAM-presenting oligonucleotides, or PAMmers, RCas9 can be specifically directed to bind or cut RNA targets while avoiding corresponding DNA sequences, or it can be used to isolate specific endogenous messenger RNA from cells, says Doudna, who holds joint appointments with Berkeley Lab's Physical Biosciences Division and UC Berkeley's Department of Molecular and Cell Biology and Department of Chemistry, and is also an investigator with the Howard Hughes Medical Institute (HHMI). Just as Cas9 can be used to cut or bind DNA in a sequence-specific manner, RCas9 can cut or bind RNA in a sequence-specific manner, says Mitchell O'Connell, a member of Doudna's research group and the lead author of a paper in Nature that describes this research titled Programmable RNA recognition and cleavage by CRISPR/Cas9. The University of California manages Berkeley Lab for the U.S. Department of Energy's Office of Science.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/dbnl-rap100314.php){:target="_blank" rel="noopener"}


