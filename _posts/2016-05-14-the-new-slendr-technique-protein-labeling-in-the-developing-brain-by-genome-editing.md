---
layout: post
title: "The new SLENDR technique: Protein labeling in the developing brain by genome editing"
date: 2016-05-14
categories:
author: Max Planck Florida Institute for Neuroscience
tags: [Protein,Cell (biology),Gene,DNA,Cas9,CRISPR,CRISPR gene editing,Genome editing,Homology directed repair,Bacteria,Non-homologous end joining,Brain,DNA repair,Cell biology,Chemistry,Branches of genetics,Molecular genetics,Biochemistry,Genetics,Molecular biology,Life sciences,Biology,Biotechnology]
---


When this damage is done, there are two methods that the cell uses to repair its DNA. If a cell uses HDR to repair itself, scientists can include a desired gene in the CRISPR system that will be inserted into the DNA to replace the damaged gene. Despite the impressive power of the CRISPR system, there has been little success in manipulating DNA in brain cells, because by the time the brain has formed, its cells are no longer dividing. While scientists can use CRISPR relatively easily to knock out certain genes in the brain, the lack of cell division has made it very difficult for them to knock in desired genes, through HDR, with reliable precision. Yasuda and his team developed a method, which they call SLENDR (single-cell labeling of endogenous proteins by CRISPRCas9-mediated homology-directed repair), that can be used to precisely modify neuronal DNA in living samples.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/mpfi-tns051116.php){:target="_blank" rel="noopener"}


