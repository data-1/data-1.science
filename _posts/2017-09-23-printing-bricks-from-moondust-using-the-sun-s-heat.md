---
layout: post
title: "Printing bricks from moondust using the Sun’s heat"
date: 2017-09-23
categories:
author: ""
tags: [Lunar soil,Moon,3D printing,In situ resource utilization,Brick,Space science,Technology,Astronomical objects known since antiquity,Planetary science,Nature,Astronomy,Outer space,Bodies of the Solar System]
---


Enabling & Support Printing bricks from moondust using the Sun’s heat 03/05/2017 14959 views 140 likes  Bricks have been 3D printed out of simulated moondust using concentrated sunlight – proving in principle that future lunar colonists could one day use the same approach to build settlements on the Moon. “We took simulated lunar material and cooked it in a solar furnace,” explains materials engineer Advenit Makaya, overseeing the project for ESA. We can complete a 20 x 10 x 3 cm brick for building in around five hours.”  3D-printing moondust bricks with focused solar heat  As raw material, the test used commercially available simulated lunar soil based on terrestrial volcanic material, processed to mimic the composition and grain sizes of genuine moondust. The solar furnace at the DLR German Aerospace Center facility in Cologne has two working setups. Advenit adds: “Our demonstration took place in standard atmospheric conditions, but RegoLight will probe the printing of bricks in representative lunar conditions: vacuum and high-temperature extremes.”  Side view of brick ESA’s effort follows a previous lunar 3D printing project, but that approach required a binding salt.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/Printing_bricks_from_moondust_using_the_Sun_s_heat){:target="_blank" rel="noopener"}


