---
layout: post
title: "Major study documents nutritional and food safety benefits of organic farming"
date: 2015-07-06
categories:
author: Washington State University 
tags: [Organic food,Food industry,Nutrition,Agriculture,Health,Food and drink]
---


The study looked at an unprecedented 343 peer-reviewed publications comparing the nutritional quality and safety of organic and conventional plant-based foods, including fruits, vegetables, and grains. In general, the team found that organic crops have several nutritional benefits that stem from the way the crops are produced. Without the synthetic chemical pesticides applied on conventional crops, organic plants also tend to produce more phenols and polyphenols to defend against pest attacks and related injuries. While crops harvested from organically managed fields sometimes contain pesticide residues, the levels are usually 10-fold to 100-fold lower in organic food, compared to the corresponding, conventionally grown food. We benefited from a much larger and higher quality set of studies than our colleagues who carried out earlier reviews, said Carlo Leifert, a Newcastle University professor and the project leader.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/wsu-msd070914.php){:target="_blank" rel="noopener"}


