---
layout: post
title: "Hazardous contaminated sites in the North and the Baltic Sea"
date: 2018-08-02
categories:
author: ""
tags: [Naval mine,Ammunition,Bomb disposal,Explosive,Bomb,Explosives,Technology,Hazards]
---


Credit: Schleswig-Holstein ordnance disposal team teaching materials  Millions of tons of old ordnance and poison gas grenades lie at the bottom of the North and the Baltic Sea – dangerous legacies of two world wars. Mines in shipping channels  Diver with moored mines. This disperses some of the toxic explosive throughout a large volume of water. The project's long-term goal is to render underwater ordnance harmless directly where it is found in a semi-automated process and then dispose of it in an environmentally friendly manner, explains Paul Müller from Fraunhofer ICT. To avoid spontaneous detonation, they must make sure ordnance is handled with the greatest caution.

<hr>

[Visit Link](https://phys.org/news/2018-08-hazardous-contaminated-sites-north-baltic.html){:target="_blank" rel="noopener"}


