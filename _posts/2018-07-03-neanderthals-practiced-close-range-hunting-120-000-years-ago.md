---
layout: post
title: "Neanderthals practiced close-range hunting 120,000 years ago"
date: 2018-07-03
categories:
author: "Johannes Gutenberg Universitaet Mainz"
tags: [Neanderthal,Hunting,News aggregator]
---


An international team of scientists reports the oldest unambiguous hunting lesions documented in the history of humankind. The lesions were found on skeletons of two large-sized extinct fallow deer killed by Neandertals about 120,000 years ago around the shores of a small lake (Neumark-Nord 1) near present-day Halle in the eastern part of Germany. The study was led by Professor Sabine Gaudzinski-Windheuser of the Department of Ancient Studies at Johannes Gutenberg University Mainz (JGU) and was now published in the journal Nature Ecology and Evolution. The study constitutes a significant step forward in our knowledge of the Neandertal niche. It demonstrates how Neandertals obtained their prey, first and foremost in terms of their much debated hunting equipment while also shedding light on their hunting skills.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180702111122.htm){:target="_blank" rel="noopener"}


