---
layout: post
title: "Deep space atomic clock"
date: 2016-04-24
categories:
author: Elizabeth Landau
tags: [Deep Space Atomic Clock,Europa (moon),Jet Propulsion Laboratory,Flight,Astronautics,Astronomy,Space science,Spaceflight,Outer space,Science,Spacecraft,Technology]
---


Work is progressing on the Deep Space Atomic Clock, a miniaturized, ultra-precise mercury-ion atomic clock that is far more stable than today's best navigation clocks. NASA is making great strides to develop the Deep Space Atomic Clock, or DSAC for short. At its core, DSAC is a paradigm-shifting technology demonstration mission to exhibit how to navigate spacecraft better, collect more data with better precision and boost the ability for a spacecraft to brake itself more accurately into an orbit or land upon other worlds. Estimation of Europa's gravitational tide, Ely says, provides an example of the use of DSAC-enabled tracking data for Europa gravity science. Explore further NASA to test new atomic clock

<hr>

[Visit Link](http://phys.org/news349508679.html){:target="_blank" rel="noopener"}


