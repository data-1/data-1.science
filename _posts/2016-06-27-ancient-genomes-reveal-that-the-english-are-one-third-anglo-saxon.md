---
layout: post
title: "Ancient genomes reveal that the English are one-third Anglo-Saxon"
date: 2016-06-27
categories:
author: "Wellcome Trust Sanger Institute"
tags: [Anglo-Saxons,DNA sequencing,Archaeology,Oxford Archaeology,Genetics,Wellcome Sanger Institute,United Kingdom,University of Central Lancashire]
---


For the first time, researchers have been able to directly estimate the Anglo-Saxon ancestry of the British population from ancient skeletons, showing how Anglo-Saxon immigrants mixed with the native population. Comparing these ancient genomes with sequences of hundreds of modern European genomes, we estimate that 38% of the ancestors of the English were Anglo-Saxons. Modern British and continental European genomes from the UK10K project and the 1000 Genomes Project were compared with the genomes from the ancient skeletons. Researchers discovered that the Anglo-Saxon immigrants were genetically very similar to modern Dutch and Danish, and that they contributed 38% of the DNA of modern people from East England, and 30% for modern Welsh and Scottish. Oxford Archaeology East, 15 Trafalgar Way, Bar Hill, Cambridge, CB23 8SQ, UK  Oxford Archaeology South, Janus House, Osney Mead, Oxford, OX2 0ES, UK  University of Central Lancashire, Preston, PR1 2HE, UK  Max Planck Institute for the Science of Human History, Department for Archaeogenetics, Kahlaische Strasse 10, 07745 Jena, Germany  The Genome Analysis Centre, Norwich Research Park, Norwich, NR4 7UH, UK  Selected Websites:  Australian Centre for Ancient DNA (ACAD)  A leading ancient DNA research facility at the University of Adelaide specialising in the analysis of evolutionary processes, human evolution, and the impacts of environmental and climate change through time.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/wtsi-agr011516.php){:target="_blank" rel="noopener"}


