---
layout: post
title: "Scientists from Atapuerca Revolutionizing Human Evolution"
date: 2015-09-04
categories:
author: Mariló T.A.
tags: [Neanderthal,Human,Archaeology,Fossil,Human evolution,Science,Early modern human]
---


Skull number 5 of the Pit of Bones, as it appeared in the campaign of 1992. ( Wikimedia Commons )  The remains of the individuals have been dated to around 430,000 years ago, with an average height of 1.63 meters and a body mass of about 69 kilos, which means that they were more heavily built than the later Neanderthals. Ignacio Martinez , a professor of paleontology at the University of Alcala de Henares and co-author of PNAS explained that The fact that humans of the Pit of Bones had smaller brains than Neanderthals means that that body size increased in parallel with and independently from the increase registered in the brain of the modern human species, which is considered a feature that is uniquely ours. New Human Evolution and Neanderthal Revolution  The work and studies of the last 20 years on the fossils have led the scientific team to establish four major stages in the evolution of humans over more than four million years. Now, with Neanderthals, we have a mirror species , another intelligent species that originated independently of ours, and this helps us to study ourselves, Martinez remarked to “El Pais.”  Members of the Atapuerca research team working on one of the sites.

<hr>

[Visit Link](http://www.ancient-origins.net/news-evolution-human-origins/scientists-atapuerca-revolutionizing-human-evolution-003737){:target="_blank" rel="noopener"}


