---
layout: post
title: "EFF Joins Nearly 150 Organizations, Security Experts, and Companies to Urge President Obama to Support Strong Encryption"
date: 2015-05-20
categories:
author: Dia Kayyali, Nate Cardozo
tags: [National Security Agency,Computer security,Encryption,Backdoor (computing),Surveillance,Privacy,Electronic Frontier Foundation,Cryptography,National security,Issues in ethics,Cybercrime,Information technology,Security,Cyberspace,Technology,Cyberwarfare,Computing,Crime prevention,Information Age]
---


That’s why EFF joined the nearly 150 privacy and human rights organizations, technology companies and trade associations, and individual security and policy experts who sent a letter urging President Obama to  reject any proposal that U.S. companies deliberately weaken the security of their products. As the letter points out, “Strong encryption is the cornerstone of the modern information economy’s security.” And it’s under threat. These backdoors enable access to and warrantless searches of the contents of communications and other data. And as we, and many others have pointed out, the government can get a warrant, use traditional investigative techniques, or gather data from the vast array of sources available to them in the modern world instead of relying on back doors. Yet the government continues to insist that back doors are necessary, ignoring the fact that the protection against criminal and national security threats provided by encryption would be:  undermined by the mandatory insertion of any new vulnerabilities into encrypted devices and services.

<hr>

[Visit Link](https://www.eff.org/deeplinks/2015/05/eff-joins-nearly-150-organizations-security-experts-and-companies-urge-president){:target="_blank" rel="noopener"}


