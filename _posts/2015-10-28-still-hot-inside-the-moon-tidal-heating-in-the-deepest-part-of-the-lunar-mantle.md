---
layout: post
title: "Still hot inside the Moon: Tidal heating in the deepest part of the lunar mantle"
date: 2015-10-28
categories:
author: National Astronomical Observatory Of Japan
tags: [Moon,Tide,SELENE,Planetary core,Earth,Geology of the Moon,Tidal force,Astronomy,Physical sciences,Space science,Planetary science,Bodies of the Solar System,Planets of the Solar System,Science,Nature]
---


However, models of the internal structure of the Moon as derived from past research could not account for the deformation precisely observed by the above lunar exploration programs. The previous studies indicated that there is the possibility that a part of the rock at the deepest part inside the lunar mantle may be molten. The research team believes that the soft layer is now warming the core of the Moon as the core seems to be wrapped by the layer, which is located in the deepest part of the mantle, and which efficiently generates heat. This is estimate value of the Moon’s interior viscosity structure replicate well the observational results in this research. That means this research not only shows us the actual state of the deep interior of the Moon, but also gives us a clue for learning about the history of the system including both the Earth and the Moon.

<hr>

[Visit Link](http://phys.org/news326692241.html){:target="_blank" rel="noopener"}


