---
layout: post
title: "Shark's sixth sense aid attacks from below"
date: 2016-04-15
categories:
author: Aaron Bryans, Science Network Wa
tags: [Shark,Electroreception and electrogenesis,Predation,Angelshark,Spotted wobbegong,Animals]
---


In addition to dorsal and ventral electrosensory pores, wobbegong and angel sharks posses a rare hyoid cluster that the scientists believe assists in predator detection. We found that both species have independently evolved a similarly unique distribution of electrosensory pores, unlike any other shark species, that would certainly assist in their ambush feeding strategy. These differences likely reflect the different behaviors of each species, and so we can learn a lot about their feeding habits just by looking at the distribution and abundance of these electroreceptors, Ms Egeberg says. In addition to dorsal and ventral electrosensory pores, wobbegong and angel sharks posses a rare hyoid cluster that the scientists believe assists in predator detection. Given the posterior position of these electroreceptors on the head, we believe this cluster may assist in the detection of predators rather than prey, and will be particularly useful to benthic species that spend long periods of time motionless on the seabed, Dr Kempster says.

<hr>

[Visit Link](http://phys.org/news344595134.html){:target="_blank" rel="noopener"}


