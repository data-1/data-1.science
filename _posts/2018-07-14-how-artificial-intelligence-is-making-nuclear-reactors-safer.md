---
layout: post
title: "How Artificial Intelligence Is Making Nuclear Reactors Safer"
date: 2018-07-14
categories:
author: ""
tags: [Nuclear power,Nuclear reactor,Artificial intelligence,Nuclear energy,Energy,Technology,Nature,Nuclear technology,Energy technology,Nuclear physics]
---


Naïve Neural Networks  Engineers at Purdue University in Lafayette, Indiana are developing a new system for keeping nuclear reactors safe with artificial intelligence (AI). “However, current practice is time-consuming, tedious, and subjective and involves human technicians reviewing inspection videos to identify cracks in reactors.”  Trained using a dataset of some 300,000 crack and non-crack patches, Purdue's AI works by viewing video images of the reactors, which are often submerged under water to keep them cool, making them even more difficult to inspect manually. The AI scans each and detects cracks in overlapping patches of the video frames. The Holy Grail of Renewable Energy  As the world continues to shift towards more renewable sources of energy, nuclear has presented itself as an option. “One important factor behind these incidents has been cracking that can lead to leaking,” Jahanshahi explained.

<hr>

[Visit Link](https://futurism.com/researchers-training-ai-make-nuclear-reactors-safer/){:target="_blank" rel="noopener"}


