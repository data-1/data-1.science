---
layout: post
title: "Simple is beautiful in quantum computing"
date: 2018-04-26
categories:
author: "Us Department Of Energy"
tags: [Quantum computing,Nitrogen-vacancy center,Qubit,Computer,Computing,Quantum information,Bit,Physics,Quantum mechanics,Technology,Theoretical physics]
---


Quantum computers use electron spin orientation at a defect site in diamond to store information. The electron spin can be up (+1), down (-1), or anything in between. To solve a problem, a quantum computer uses logic gates to couple multiple qubits and output new information. Similar to classical computers, quantum computers are designed to operate on quantum bits. Gates are used to prepare and manipulate the electronic transitions of qubits.

<hr>

[Visit Link](https://phys.org/news/2017-11-simple-beautiful-quantum.html){:target="_blank" rel="noopener"}


