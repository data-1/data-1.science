---
layout: post
title: "Image: Sun's magnetic field modeled"
date: 2018-08-18
categories:
author: ""
tags: [Solar Dynamics Observatory,Technology,Computing,Communication,Information technology]
---


Credit: NASA/GSFC/Solar Dynamics Observatory  NASA's Solar Dynamics Observatory (SDO) scientists used their computer models to generate a view of the Sun's magnetic field on August 10, 2018. The bright active region right at the central area of the Sun clearly shows a concentration of field lines, as well as the small active region at the Sun's right edge, but to a lesser extent. Magnetism drives the dynamic activity near the Sun's surface.

<hr>

[Visit Link](https://phys.org/news/2018-08-image-sun-magnetic-field.html){:target="_blank" rel="noopener"}


