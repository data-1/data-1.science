---
layout: post
title: "The magnetic compass of birds is affected by polarised light"
date: 2016-01-29
categories:
author: Lund University
tags: [Polarization (waves),Privacy,Technology,Communication]
---


Credit: R. Muheim  The magnetic compass that birds use for orientation is affected by polarised light. This previously unknown phenomenon was discovered by researchers at Lund University in Sweden. The discovery that the magnetic compass is affected by the polarisation direction of light was made when trained zebra finches were trying to find food inside a maze. The birds were only able to use their magnetic compass when the direction of the polarised light was parallel to the magnetic field, not when perpendicular to the magnetic field. Explore further Image: The magnetic field along the galactic plane

<hr>

[Visit Link](http://phys.org/news/2016-01-magnetic-compass-birds-affected-polarised.html){:target="_blank" rel="noopener"}


