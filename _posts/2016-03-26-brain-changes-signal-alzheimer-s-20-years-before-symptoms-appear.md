---
layout: post
title: "Brain Changes Signal Alzheimer's 20 Years Before Symptoms Appear"
date: 2016-03-26
categories:
author:  
tags: [Alzheimers disease,Amyloid plaques,General Electric,Astrocyte,Technology]
---


Top image and above: These illustrations show neurons with amyloid plaques. Neuron with amyloid plaques (yellow). Researchers in Sweden have uncovered changes in the brain that foretell the development of the brain disorder up to two decades before symptoms occur.A team at the Karolinska Institute watched brain cells called astrocytes, which protect and support the information-carrying neurons, increase in number years before people genetically predisposed to the disease showed any symptoms. “Astrocyte activation peaks roughly 20 years before the expected symptoms and then goes into decline, in contrast to the accumulation of amyloid plaques, which increases constantly over time until clinical symptoms show,” Nordberg says. Nordberg says the next steps for research should focus on how tau proteins relate to the astrocyte-amyloid relationship.“To treat Alzheimer's disease, we must first understand the course of its progression over time and develop diagnostic markers to detect it,” she says.

<hr>

[Visit Link](http://www.gereports.com/brain-changes-signal-alzheimers-20-years-before-symptoms-appear/){:target="_blank" rel="noopener"}


