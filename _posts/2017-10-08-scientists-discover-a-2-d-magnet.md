---
layout: post
title: "Scientists discover a 2-D magnet"
date: 2017-10-08
categories:
author: "University Of Washington"
tags: [Magnetism,Ferromagnetism,Technology,Electrical engineering,Materials science,Physics,Chemistry,Electromagnetism]
---


A team led by the University of Washington and the Massachusetts Institute of Technology has for the first time discovered magnetism in the 2-D world of monolayers, or materials that are formed by a single atomic layer. 2-D monolayers alone offer exciting opportunities to study the drastic and precise electrical control of magnetic properties, which has been a challenge to realize using their 3-D bulk crystals, said Xu. There, you can get even more exotic phenomena not seen in the monolayer alone or in the 3-D bulk crystal. At the interface between the two materials, his team searches for new physical phenomena or new functions to allow potential applications in computing and information technologies. Xu and his team would next like to investigate the magnetic properties unique to 2-D magnets and heterostructures that contain a CrI3 monolayer or bilayer.

<hr>

[Visit Link](https://phys.org/news/2017-06-scientists-d-magnet.html){:target="_blank" rel="noopener"}


