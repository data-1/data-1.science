---
layout: post
title: "Report highlights opportunities and risks associated with synthetic biology and bioengineering"
date: 2017-11-30
categories:
author: "University of Cambridge"
tags: [Synthetic biology,Genome editing,Engineering,Gene drive,Technology,Human microbiome,Disease,Biology,Life sciences]
---


Rapid developments in the field of synthetic biology and its associated tools and methods, including more widely available gene editing techniques, have substantially increased our capabilities for bioengineering - the application of principles and techniques from engineering to biological systems, often with the goal of addressing 'real-world' problems. In a report published in the open access journal eLife, an international team of experts led by Dr Bonnie Wintle and Dr Christian R. Boehm from the Centre for the Study of Existential Risk at the University of Cambridge, capture perspectives of industry, innovators, scholars, and the security community in the UK and US on what they view as the major emerging issues in the field. The rise of open source, off-patent tools could enable widespread sharing of knowledge within the biological engineering field and increase access to benefits for those in developing countries. Dr Christian R. Boehm concludes: As these technologies emerge and develop, we must ensure public trust and acceptance. ###  The research was made possible by the Centre for the Study of Existential Risk, the Synthetic Biology Strategic Research Initiative (both at the University of Cambridge), and the Future of Humanity Institute (University of Oxford).

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/uoc-rho112117.php){:target="_blank" rel="noopener"}


