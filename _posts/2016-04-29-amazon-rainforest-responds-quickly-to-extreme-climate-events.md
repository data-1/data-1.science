---
layout: post
title: "Amazon rainforest responds quickly to extreme climate events"
date: 2016-04-29
categories:
author: Stanford's School of Earth, Energy & Environmental Sciences 
tags: [Amazon rainforest,Climate change,Greenhouse gas,Carbon dioxide,Climate,Effects of climate change,Ecosystem,Tropical rainforest,Drought,Earth,Carbon dioxide in Earths atmosphere,Natural environment,Physical geography,Climate variability and change,Nature,Earth sciences,Global environmental issues,Environmental impact,Atmosphere,Applied and interdisciplinary physics,Earth phenomena,Environmental issues with fossil fuels,Human impact on the environment,Global natural environment,Societal collapse,Environmental issues,Environmental science,Change]
---


A new study examining carbon exchange in the Amazon rainforest following extremely hot and dry spells reveals tropical ecosystems might be more sensitive to climate change than previously thought. If climate change causes tropical forests to emit large amounts of CO2 into the atmosphere, that carbon loss could amplify the global warming effects of fossil fuel emissions. The study's lead author, Caroline Alden, began the study at the University of Colorado's Cooperative Institute for Research in Environmental Sciences (CIRES) and continued the research as a postdoctoral researcher working with Diffenbaugh at Stanford. Alden and her team found evidence of very large shifts toward carbon loss to the atmosphere in the Amazon after periods of extreme heat and drought in 2010. Taken together, these results suggest the Amazon may be more sensitive to heat and drought conditions than previously thought - a finding that does not bode well for tropical ecosystems in the coming decades as the effects of climate change are expected to intensify.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/ssoe-arr042816.php){:target="_blank" rel="noopener"}


