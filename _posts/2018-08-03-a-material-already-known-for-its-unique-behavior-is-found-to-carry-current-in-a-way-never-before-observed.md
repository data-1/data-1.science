---
layout: post
title: "A material already known for its unique behavior is found to carry current in a way never before observed"
date: 2018-08-03
categories:
author: "Florida State University"
tags: [Cuprate superconductor,Superconductivity,National High Magnetic Field Laboratory,Electrical resistivity and conductivity,Physics,Electromagnetism,Materials,Physical sciences,Electrical engineering,Electricity,Chemistry,Applied and interdisciplinary physics]
---


Credit: National MagLab  Scientists at the Florida State University-headquartered National High Magnetic Field Laboratory have discovered a behavior in materials called cuprates that suggests they carry current in a way entirely different from conventional metals such as copper. This normal state of cuprates is known as a strange or bad metal, in part because the electrons don't conduct electricity particularly well. In other words, as the temperature goes up, LSCO's resistance to electrical current goes up proportionately, which is not the case in conventional metals. This new evidence suggests that LSCO in its normal conducting state may also carry current using something other than independent quasiparticles—although it's not superconductivity, either. We need to find a new language to think about these materials.

<hr>

[Visit Link](https://phys.org/news/2018-08-material-unique-behavior-current.html){:target="_blank" rel="noopener"}


