---
layout: post
title: "Shedding light on the energy-efficiency of photosynthesis"
date: 2018-07-04
categories:
author: "University of California - Davis"
tags: [Photorespiration,RuBisCO,Physical sciences,Chemistry,Nature,Earth sciences,Biology,Natural environment]
---


It's long been thought that more than 30 percent of the energy produced during photosynthesis is wasted in a process called photorespiration. Understanding the regulation of these processes is critical for sustaining food quality under climate change, said lead author Arnold Bloom in the Department of Plant Sciences at the UC Davis College of Agricultural and Environmental Sciences. The study was published July 2 in the journal Nature Plants. Rubisco, thus, seemed to act like the molecular equivalent of a good friend with a bad habit. When Rubisco associates with manganese, photorespiration proceeds along an alternative biochemical pathway, generates energy for nitrate assimilation, and promotes protein synthesis.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180703110039.htm){:target="_blank" rel="noopener"}


