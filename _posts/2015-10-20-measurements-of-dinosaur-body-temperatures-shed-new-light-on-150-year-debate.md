---
layout: post
title: "Measurements of dinosaur body temperatures shed new light on 150-year debate"
date: 2015-10-20
categories:
author: University Of California, Los Angeles
tags: [Dinosaur,Endotherm,Oviraptoridae,Ectotherm,Thermoregulation,Animals]
---


New research by UCLA scientists indicates that some dinosaurs, at least, had the capacity to elevate their body temperature using heat sources in the environment, such as the sun. Cold-blooded animals, or ectotherms, including alligators, crocodiles and lizards, rely on external environmental heat sources to regulate their body temperature. The dinosaurs, at least the oviraptorid theropods, had the ability to elevate their body temperature above the environmental temperature. Eagle, Tripati and their colleagues initially measured modern eggshells from 13 bird species and nine reptiles to establish their ability to measure body temperature from the chemistry of eggshells. They studied the chemistry of fossil teeth to measure the body temperature of titanosaur sauropods, and determined their body temperature was between approximately 95 and 100.5 degrees Fahrenheit.

<hr>

[Visit Link](http://phys.org/news/2015-10-dinosaur-body-temperatures-year-debate.html){:target="_blank" rel="noopener"}


