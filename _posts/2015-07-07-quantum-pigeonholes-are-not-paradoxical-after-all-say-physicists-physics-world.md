---
layout: post
title: "Quantum pigeonholes are not paradoxical after all, say physicists – Physics World"
date: 2015-07-07
categories:
author: Hamish Johnston
tags: [Electron,Pigeonhole principle,Physics,Interferometry,Quantum mechanics,MachZehnder interferometer,Theoretical physics,Science,Applied and interdisciplinary physics,Scientific theories,Physical sciences,Scientific method]
---


Peak pigeon: Calculations of the arrival patterns for one electron trajectory made at four different interaction strengths (d). When all three electrons are detected, these tiny deflections should give a pattern of electron arrivals at the detectors that would reveal that two or more electrons had travelled along the same arm of the interferometer. Possible paths  Rae and Forgan have now analysed the outcome of such a hypothetical experiment, and have shown that applying quantum mechanics and the classical pigeonhole principle can explain the apparent paradox. If the interaction strength were zero, on the other hand, then only three peaks – one for each electron – would be measured in the detector. Their calculations suggest that for 40 keV electrons, an experimentalist would have to discern patterns in their detectors across distances of about 10–13 m. This is about 1000 times smaller than the distance between atoms in a solid, making the measurement extraordinarily difficult if not impossible, according to the researchers.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/D3uC2guvctI/quantum-pigeonholes-are-not-paradoxical-after-all-say-physicists){:target="_blank" rel="noopener"}


