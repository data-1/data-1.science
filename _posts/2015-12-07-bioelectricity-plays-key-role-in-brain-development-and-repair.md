---
layout: post
title: "Bioelectricity plays key role in brain development and repair"
date: 2015-12-07
categories:
author: Tufts University 
tags: [Brain,Notch signaling pathway,Development of the nervous system,Birth defect,Neuroscience,Anatomy,Developmental biology,Life sciences,Cell biology,Biology]
---


More than on/off switch, electric signals tell cells where and how to grow  MEDFORD/SOMERVILLE, Mass. (March 11, 2015) -- Research reported today by Tufts University biologists shows for the first time that bioelectrical signals among cells control and instruct embryonic brain development and manipulating these signals can repair genetic defects and induce development of healthy brain tissue in locations where it would not ordinarily grow. Prior work in the Levin lab revealed roles for bioelectric gradients in eye, limb and visceral organ patterning, and the new paper found that natural embryonic voltage gradients instruct the formation of the brain. The research team found that using molecular techniques to force proper bioelectrical states in cells enabled them to override the defects induced by Notch malfunction, resulting in a much more normal brain despite a genetically-defective Notch protein. Endogenous Gradients of Resting Potential Instructively Pattern Embryonic Neural Tissue via Notch Signaling and Regulation of Proliferation, Vaibhav Pai, Joan M. Lemire, Jean-Francois Pare´, Gufa Lin, Ying Chen, and Michael Levin, Journal of Neuroscience, March 11, 2015, DOI:10.1523/JNEUROSCI.1877-14.2015  Tufts University, located on three Massachusetts campuses in Boston, Medford/Somerville and Grafton, and in Talloires, France, is recognized among the premier research universities in the United States.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/tu-bpk030415.php){:target="_blank" rel="noopener"}


