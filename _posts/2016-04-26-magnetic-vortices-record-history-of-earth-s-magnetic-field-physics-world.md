---
layout: post
title: "Magnetic vortices record history of Earth's magnetic field – Physics World"
date: 2016-04-26
categories:
author:  
tags: [Magnetism,Paleomagnetism,Geology,Magnetite,Curie temperature,Rock (geology),Nature,Physical sciences,Applied and interdisciplinary physics,Earth sciences,Physics,Electromagnetism]
---


(Courtesy: Imperial College London)  The vortex-like structures in grains of magnetite can reliably preserve magnetic information, remaining unaltered by temperature changes, thereby recording the history of the Earth’s magnetic field. The work could help us to investigate the nature of the Earth’s magnetic field as it has evolved and changed over billions of years, as well as improve our understanding of the Earth’s core and plate tectonics. Cool imprint  The most magnetic natural mineral is the aptly named magnetite, a commonly occurring oxide of iron. These so-called single-domain structures are magnetically near-uniform, found in grains less than 80 nm long, and are valued for their thermal stability – being able to retain magnetic signals, even if heated, over geologically useful lengths of time. To do this, the team used an imaging method called electron holography, mapping out the vortex structures of magnetite grains of various sizes as they were heated to 550 °C – just short of their Curie temperature – and then allowed to cool.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2016/apr/25/magnetic-vortices-record-history-of-earths-magnetic-field){:target="_blank" rel="noopener"}


