---
layout: post
title: "New evidence of cultural diversification between neighboring chimpanzee communities"
date: 2015-09-07
categories:
author: University of Cambridge 
tags: [Chimpanzee,Species,Ant,Culture,Human,Tool use by animals,Biodiversity,Anthropology,American Association for the Advancement of Science,Animals,Branches of science,Biology,Behavioural sciences]
---


For centuries it has been thought that culture is what distinguishes humans from other animals, but over the past decade this idea has been repeatedly called into question. We compared neighbouring chimpanzee groups living under similar environmental conditions, which allows for the investigation of fine scale cultural differences, whilst keeping genetics constant, said Koops. So Koops compared the availability of the different species of army ants and the length of dipping tools used in the two adjacent chimpanzee communities. The researchers found that M-group tools were significantly longer than S-group tools, despite identical army ant species availability. Given the close evolutionary relationship between chimpanzees and humans, insights into what drives cultural diversification in our closest living relatives will in turn shed light on how cultural differences emerge and are maintained between adjacent groups in human societies, said Koops, who conducted the work at Cambridge University's Division of Biological Anthropology and at Zurich University's Anthropological Institute and Museum.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/uoc-neo072115.php){:target="_blank" rel="noopener"}


