---
layout: post
title: "Artificial leaf goes more efficient for hydrogen generation"
date: 2017-03-17
categories:
author: "Ulsan National Institute of Science and Technology(UNIST)"
tags: [Artificial photosynthesis,Photosynthesis,Environmental technology,Technology,Physical sciences,Nature,Energy,Sustainable technologies,Sustainable energy,Chemistry]
---


Their new artificial leaf mimics the natural process of underwater photosynthesis of aquatic plants to split water into hydrogen and oxygen, which can be harvested for fuel. Because using hydrogen produced by artificial leaf as fuel, does not generate carbon dioxide emissions. However, it is difficult to receive the full sunlight deep under the sea. Therefore, they are subjected to various types of photosynthesis that selectively utilize wavelengths reaching their depths. Journal Reference  Jin Hyun Kim, Ji-Wook Jang, Yim Hyun Jo, Fatwa F. Abdi, Young Hye Lee, Roel van de Krol, and Jae Sung Lee, Hetero-type dual photoanodes for unbiased solar water splitting with extended light harvesting, Nature (2016).

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/unio-alg010317.php){:target="_blank" rel="noopener"}


