---
layout: post
title: "Scientists gain first glimpse of new concepts developing in the brain"
date: 2016-05-07
categories:
author: Carnegie Mellon University
tags: [Brain,News aggregator,Dementia,Science,Concept,Learning,Information,Behavioural sciences,Psychology,Cognitive psychology,Branches of science,Interdisciplinary subfields,Cognition,Cognitive science,Neuroscience]
---


'Millions of people read the information about the olinguito and in doing so permanently changed their own brains,' said Just, the D.O. Just added, 'The new knowledge gained from the Smithsonian's announcement became encoded in the same brain areas in every person that learned the new information, because all brains appear to use the same filing system.' advertisement  For the study, Andrew Bauer, a Ph.D. student in psychology, and Just taught 16 study participants diet and dwelling information about extinct animals to monitor the growth of the neural representations of eight new animal concepts in the participants' brains. As the new properties were taught, the activation levels in the eating regions and the dwelling regions changed. But even though the animals had unique activation signatures, the animals that shared similar properties (such as a similar habitat) had similar activation signatures.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150609141738.htm){:target="_blank" rel="noopener"}


