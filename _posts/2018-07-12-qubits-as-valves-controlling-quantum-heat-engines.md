---
layout: post
title: "Qubits as valves: Controlling quantum heat engines"
date: 2018-07-12
categories:
author: "Aalto University"
tags: [Quantum heat engines and refrigerators,Heat,Heat engine,Quantum mechanics,Engine,Scientific theories,Applied mathematics,Chemistry,Thermodynamics,Physical chemistry,Physical sciences,Applied and interdisciplinary physics,Theoretical physics,Physics,Science]
---


Researchers from Aalto University are designing nano-sized quantum heat engines to explore whether they may be able to outperform classical heat engines in terms of power and efficiency  Researchers from Aalto University are designing nano-sized quantum heat engines to explore whether they may be able to outperform classical heat engines in terms of power and efficiency. 'We have realised a miniature heat valve in a quantum system composed of an artificial atom, a superconducting qubit--the basic building block of both quantum computing and quantum heat engines,' explains Professor Pekola. While in quantum computers the qubit has to be decoupled from the noisy external world to sustain a fragile quantum state, in quantum heat engines, the system needs to be coupled to its dissipative surroundings, to heat baths. A quantum heat engine transforms heat into useful work or, in reverse, operates as a refrigerator. The experimental research was carried out at the OtaNano national research infrastructure for micro, nano and quantum technologies in Finland.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/au-qav070918.php){:target="_blank" rel="noopener"}


