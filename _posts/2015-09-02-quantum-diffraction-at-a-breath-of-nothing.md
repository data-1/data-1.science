---
layout: post
title: "Quantum diffraction at a breath of nothing"
date: 2015-09-02
categories:
author: "$author"   
tags: [Quantum mechanics,Diffraction grating,Molecule,Diffraction,Waveparticle duality,Uncertainty principle,Electron,Max Planck Institute of Quantum Optics,Matter wave,Science,Atomic physics,Materials science,Theoretical physics,Optics,Chemistry,Applied and interdisciplinary physics,Physical sciences,Physical chemistry,Atomic molecular and optical physics,Physics]
---


Quantum diffraction at a breath of nothing    by Staff Writers    Vienna, Austria (SPX) Aug 27, 2015    Modern fabrication methods allow to make atomically thin nanomasks which prove to be sufficiently robust for experiments in molecular quantum optics. It can only be understood if we take the particles' quantum mechanical wave nature into account. At the technological limit  In a European collaboration (NANOQUESTFIT) together with partners around Professor Ori Cheshnovsky at Tel Aviv University (where all nanomasks were written), as well as with support by groups in Jena (growth of biphenyl membranes, Prof. Turchanin), and Vienna (High-Resolution Electron Microscopy, Prof. Meyer) they now demonstrated for the first time that such gratings can be fabricated even from the thinnest conceivable membranes. If this were the case the grating bars could reveal the molecular path through the grating and quantum interference should be destroyed. The solution to this riddle is again provided by Heisenberg's uncertainty principle: Although the molecules give the grating a little kick in the diffraction process this recoil remains always smaller than the quantum mechanical momentum uncertainty of the grating itself.

<hr>

[Visit Link](http://www.spacedaily.com/reports/Quantum_diffraction_at_a_breath_of_nothing_999.html){:target="_blank" rel="noopener"}


