---
layout: post
title: "Top 10 Machine Learning Use Cases (Part 2)"
date: 2018-06-29
categories:
author: "Nov."
tags: []
---


Here in Part 2, we'll look beyond the role of ML in oncology and radiology to its influence on mental health, whole-life health, and reducing re-admission rates. Helping Reduce Readmission Rates With ML  Hospital and clinics assess their own effectiveness in many ways, from average patient stay to ER wait times to direct surveys of service. But now, a healthcare system in the US is using machine learning to help create a lens into the factors that impact readmission. That's the mandate of a research institution in the northeastern US that worked with IBM to organize, maintain, and mine their research for trends in the data that could inform the interventions they undertake. Let's continue to evangelize for machine learning — and to keep sharing the new visions for bringing it to bear.

<hr>

[Visit Link](https://dzone.com/articles/top-10-machine-learning-use-cases-part-2?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


