---
layout: post
title: "EOVSA reveals new insights into solar flares' explosive energy releases"
date: 2018-06-07
categories:
author: "New Jersey Institute of Technology"
tags: [Solar flare,Sun,Coronal mass ejection,Owens Valley Solar Array,Stellar corona,Science,Astrophysics,Physical phenomena,Space science,Nature,Astronomy,Electromagnetism,Physical sciences,Space plasmas,Scientific phenomena,Physics]
---


Last September, a massive new region of magnetic field erupted on the Sun's surface next to an existing sunspot. By measuring the radio spectrum at different places in the solar atmosphere, especially when it is able to do so fast enough to follow changes during solar flares, it becomes a powerful diagnostic of the fast-changing solar environment during these eruptions. EOVSA makes that possible at radio wavelengths for the first time. EOVSA's new results have sparked lots of interest at the TESS meeting, said Bin Chen, assistant professor of physics at CSTR, who is chairing a session focused on the intense solar activity that occurred last September. With new funding from NASA, Gary and colleagues will measure the spatially-resolved radio spectrum of solar flares, determine the particle and plasma parameters as a function of position and time, and then use 3-dimensional modeling, which his group has developed, to fully understand the initial acceleration and subsequent transport of high-energy particles.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/05/180524174537.htm){:target="_blank" rel="noopener"}


