---
layout: post
title: "Neanderthals disappeared from the Iberian Peninsula before than from the rest of Europe"
date: 2015-07-20
categories:
author: Spanish Foundation for Science and Technology 
tags: [Human evolution,Neanderthal,Human populations,Pleistocene]
---


Until a few months ago different scientific articles, including those published in 'Nature', dated the disappearance of the Neanderthals (Homo neanderthalensis) from Europe at around 40,000 years ago. However, in the Iberian Peninsula the Neanderthals may have disappeared 45,000 years ago. Until now, there was no direct dating in Spain on the Neanderthal human remains which produced recent dates. In doing so the team of scientists provided data that referred specifically to the final occupations in El Salt, a very robust archaeological context in terms of the reliability of the remains, says the scientist. These new dates indicate a possible disappearance of the regional Neanderthal populations around 45,000 years ago, indicates the study's research team.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/f-sf-ndf020515.php){:target="_blank" rel="noopener"}


