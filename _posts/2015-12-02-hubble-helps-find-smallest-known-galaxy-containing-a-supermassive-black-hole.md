---
layout: post
title: "Hubble helps find smallest known galaxy containing a supermassive black hole"
date: 2015-12-02
categories:
author: "$author" 
tags: [Milky Way,Black hole,Star,Hubble Space Telescope,Supermassive black hole,Dwarf galaxy,Space Telescope Science Institute,M60-UCD1,Science,Astronomy,Space science,Physical sciences,Astronomical objects,Outer space,Sky,Stellar astronomy,Physical cosmology]
---


The black hole is five times the mass of the one at the center of our Milky Way galaxy. Seth's team of astronomers used the Hubble Space Telescope and the Gemini North 8-meter optical and infrared telescope on Hawaii's Mauna Kea to observe M60-UCD1 and measure the black hole's mass. Supermassive black holes -- those with the mass of at least one million stars like our sun -- are thought to be at the centers of many galaxies. The black hole at the center of our Milky Way galaxy has the mass of four million suns. By comparison, the supermassive black hole at the center of M60-UCD1, which has the mass of 21 million suns, is a stunning 15 percent of the small galaxy's total mass.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/nsfc-hhf091714.php){:target="_blank" rel="noopener"}


