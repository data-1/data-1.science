---
layout: post
title: "Best precision yet for neutrino measurements at Daya Bay"
date: 2016-06-30
categories:
author: "Brookhaven National Laboratory"
tags: [Neutrino,Neutrino oscillation,Daya Bay Reactor Neutrino Experiment,Physical sciences,Theoretical physics,Science,Particle physics,Physics]
---


Measurements of these properties by the Daya Bay Collaboration are the most precise to date, an improvement of about a factor of two over previous measurements published by the collaboration in early in 2014. The amplitude of neutrino oscillations gives scientists information about the rate at which neutrinos transform into different flavors, known as the mixing angle. The Neutrino Net  To study neutrino oscillations, the Daya Bay Collaboration has immersed eight detectors in three large underground pools of water. Credit: Berkeley Lab/Roy Kaltschmidt  Based on the data collected over 217 days with six of the Daya Bay detectors and 404 days using all eight of the Daya Bay detectors, the research team has determined the value for a specific mixing angle, called theta13 (pronounced theta-one-three), to a precision two times better than previous results. At the end of 2017 it will have roughly four times more data to further improve precision for both the mixing angle of theta13 and the corresponding mass splitting.

<hr>

[Visit Link](http://phys.org/news/2015-09-precision-neutrino-daya-bay.html){:target="_blank" rel="noopener"}


