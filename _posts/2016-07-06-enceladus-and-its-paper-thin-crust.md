---
layout: post
title: "Enceladus and its paper-thin crust"
date: 2016-07-06
categories:
author: ""
tags: [Enceladus,CassiniHuygens,Europa (moon),Moon,Planemos,Planetary-mass satellites,Gas giants,Physical sciences,Outer planets,Moons,Ancient astronomy,Planetary science,Astronomy,Solar System,Bodies of the Solar System,Space science,Planets of the Solar System,Outer space,Astronomical objects known since antiquity,Astronomical objects,Planets]
---


Of all the icy moons in the Solar System, Saturn’s moon Enceladus is probably the ‘hottest’ when measured for its potential to host life. Buried beneath its icy crust is a global ocean of water, much like the one scientists are convinced lies inside Jupiter’s moon Europa. At its south poles, huge geysers of water jet into space. The rest is ocean and the ice crust, with the ice crust itself having an average thickness of 18–22 km. This water and the chemicals were then transported from the floor to the base of the ice crust, and subsequently jetted through and out into space.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2016/07/Enceladus_and_its_paper-thin_crust){:target="_blank" rel="noopener"}


