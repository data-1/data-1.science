---
layout: post
title: "Origins of photosynthesis in plants dated to 1.25 billion years ago"
date: 2018-04-19
categories:
author: "Mcgill University"
tags: [Plant,Fossil,Eukaryote,Photosynthesis,Evolution,Chloroplast,Molecular clock,Nature,Biology,Biological evolution,Organisms,Earth sciences]
---


Bangiomorpha pubescens fossils occur in this roughly 500-meter thick rock formation. Credit: Timothy Gibson  The world's oldest algae fossils are a billion years old, according to a new analysis by earth scientists at McGill University. Based on this finding, the researchers also estimate that the basis for photosynthesis in today's plants was set in place 1.25 billion years ago. The study, published in the journal Geology, could resolve a long-standing mystery over the age of the fossilized algae, Bangiomorpha pubescens, which were first discovered in rocks in Arctic Canada in 1990. This will enable scientists to make more precise assessments of the early evolution of eukaryotes, the celled organisms that include plants and animals.

<hr>

[Visit Link](https://phys.org/news/2017-12-photosynthesis-dated-billion-years.html){:target="_blank" rel="noopener"}


