---
layout: post
title: "Prehistoric super salamander was top predator, fossils suggest"
date: 2015-07-21
categories:
author: University of Edinburgh 
tags: [Amphibian,Triassic,Dinosaur,Fossil,Paleontology,Taxa]
---


Palaeontologists identified the prehistoric species - which looked like giant salamanders - after excavating bones buried on the site of an ancient lake in southern Portugal. The species was part of a wider group of primitive amphibians that were widespread at low latitudes 220-230 million years ago, the team says. The creatures grew up to 2m in length and lived in lakes and rivers during the Late Triassic Period, living much like crocodiles do today and feeding mainly on fish, researchers say. Most members the group of giant salamander-like amphibians was wiped out during a mass extinction 201 million years ago, long before the death of the dinosaurs. Dr Steve Brusatte, of the University of Edinburgh's School of GeoSciences, who led the study, said: This new amphibian looks like something out of a bad monster movie.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/uoe-pss032315.php){:target="_blank" rel="noopener"}


