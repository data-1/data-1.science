---
layout: post
title: "Scientists Discover Dying Sun-Like Stars Hold Molecule Vital for Creating Water"
date: 2014-06-22
categories:
author: Science World Report
tags: [Star,Planetary nebula,Sun,Nebula,Radiation,Interstellar medium,Physics,Physical sciences,Astronomy,Space science,Nature,Astronomical objects,Stellar astronomy,Outer space,Chemistry,Astrophysics,Sky]
---


These stars cast off their outer layers of dust and gas into space, creating intricate patterns known as planetary nebulas. When a star finally becomes a planetary nebula, intense radiation from a hot white dwarf may destroy molecules that had been previously ejected by the star and that are bound up in the clumps or rings of material seen in the periphery of planetary nebulas. Using Herschel, astronomers have found that OH+, a molecule vital to the formation of water, seems to be present in this harsh environment. The findings reveal a bit more about the chemical reactions that occur in sun-like stars after they die. This, in turn, can tell us a bit about our own sun.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15518/20140619/scientists-discover-dying-sun-stars-hold-molecule-vital-creating-water.htm){:target="_blank" rel="noopener"}


