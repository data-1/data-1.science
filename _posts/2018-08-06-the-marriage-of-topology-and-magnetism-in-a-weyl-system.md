---
layout: post
title: "The marriage of topology and magnetism in a Weyl system"
date: 2018-08-06
categories:
author: "Max Planck Institute for Chemical Physics of Solids"
tags: [Weyl semimetal,Semimetal,Topological order,Physics,Physical sciences,Applied and interdisciplinary physics,Condensed matter physics,Nature,Electromagnetism,Chemical product engineering,Physical chemistry,Phases of matter,Materials,Materials science,Quantum mechanics,Theoretical physics,Chemistry,Condensed matter]
---


The interplay of symmetry, relativistic effects and, in magnetic materials, the magnetic structure, allows for the realization of a wide variety of topological phases through Berry curvature design. Weyl points and other topological electronic bands can be manipulated by various external perturbations (magnetic field, pressure ...), which results in exotic local properties such as the chiral or gravitational anomaly, and large topological Hall effects, concepts which were developed in other fields of physics such as high energy physics and astrophysics. In the recent study, scientists from the Max Planck Institute for Chemical Physics of Solids in Dresden, in collaboration with the Technische Universita?t Dresden, scientists from Beijing, Princeton, Oxford, and others found evidence for Weyl physics in the magnetic Shandites Co 3 Sn 2 S 2 . Our work provides a clear path to the observation of a quantum anomalous hall effect at room temperature by exploring families of magnetic Weyl semimetals. This study, for the first time, realizes the giant anomalous Hall effects by using a magnetic Weyl semimetal, which establishes the magnetic Weyl semimetal candidate Co 3 Sn 2 S 2 as a key class of materials for fundamental research and applications connecting the topological physics and spintronics.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/mpif-tmo080318.php){:target="_blank" rel="noopener"}


