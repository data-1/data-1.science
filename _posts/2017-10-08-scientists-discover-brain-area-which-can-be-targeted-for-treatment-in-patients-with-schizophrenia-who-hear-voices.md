---
layout: post
title: "Scientists discover brain area which can be targeted for treatment in patients with schizophrenia who 'hear voices'"
date: 2017-10-08
categories:
author: "European College of Neuropsychopharmacology"
tags: [Auditory hallucination,Transcranial magnetic stimulation,Schizophrenia,Abnormal psychology,Psychology,Medical specialties,Health sciences,Cognitive science,Causes of death,Human diseases and disorders,Medicine,Diseases and disorders,Neuroscience,Mental health,Clinical medicine,Health,Mental disorders]
---


They have been able to show in a controlled trial that targeting this area with magnetic pulses can improve the condition in some patients. This early clinical work is presented at the ECNP conference in Paris on Tuesday 5th September, with later publication in Schizophrenia Bulletin*. This is the first controlled trial to precisely determine an anatomically defined brain area where high frequency magnetic pulses can improve the hearing of voices, said lead researcher, Professor Sonia Dollfus (University of Caen, CHU, France). One of the best-known is hearing voices, also known as Auditory Verbal Hallucination (AVH), which around 70% of people with schizophrenia experience at some point. This is the first controlled trial to show an improvement in these patients by targeting a specific area of the brain and using high frequency TMS.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/09/170904181720.htm){:target="_blank" rel="noopener"}


