---
layout: post
title: "Copernicus Sentinel-5P releases first data"
date: 2018-08-16
categories:
author: ""
tags: [Sentinel-5,Copernicus Programme,Air pollution,Atmosphere of Earth,Outer space,Nature,Atmosphere,Earth sciences]
---


Following months of tests and careful evaluation, the first data on air pollutants from the Copernicus Sentinel-5P satellite have been released. These first maps show a range of trace gases that affect air quality such as carbon monoxide, nitrogen dioxide and ozone. It is part of the fleet of Sentinel missions that ESA develops for the European Union’s environmental monitoring Copernicus programme managed by the European Commission. “These first data are another milestone for our Copernicus programme. They show how Sentinel-5P is set to make a real difference in monitoring air quality and highlight European Union’s contribution to combatting the global issue of air pollution.”  As poor air quality continues to prematurely claim the lives of millions of people every year, it is more important than ever that we find better and more accurate ways of monitoring the air we breathe.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-5P/Copernicus_Sentinel-5P_releases_first_data){:target="_blank" rel="noopener"}


