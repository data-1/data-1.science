---
layout: post
title: "The Open Source Program at Microsoft: How Open Source Thrives"
date: 2018-04-29
categories:
author: "Pam Baker"
tags: [Open source,GitHub,Linux,Microsoft,Intellectual works,Computing,Technology,Software,Software development,Software engineering,Open-source movement]
---


“Someone has to think about policy and how all the open source efforts would be coordinated, the processes and tools they would use, how would we keep track of projects, etc. So, we created what is now known as the Open Source Programs Office to handle all those issues.”  Some of the technical people from the earlier open source group moved to the newly formed program office, while the others joined engineering groups pertinent to their work. We give them tools and processes that embody those policies to make it super simple for them to execute in a coherent yet specific way.”  The tools to manage  Microsoft’s policies boil down into processes that then are tooled accordingly to handle the workload. “We’ve got a bunch of tooling around our presence on GitHub, where we manage something like 10,000+ repos across about 100 organizations with about 12,000 Microsoft people interacting in that space,” said McAffer. “To that end, we’ve written a lot of tools internally to discover, track, and monitor what’s going on there and report on the use of open source,” he continued.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/open-source-program-microsoft-open-source-thrives/){:target="_blank" rel="noopener"}


