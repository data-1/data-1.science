---
layout: post
title: "ESA’s Proba-3 will create artificial solar eclipses"
date: 2017-09-23
categories:
author: ""
tags: [Coronagraph,Sun,PROBA-3,Stellar corona,Satellite,Local Interstellar Cloud,Space science,Astronomy,Outer space,Bodies of the Solar System,Solar System,Physical sciences,Astronomical objects,Astronomical objects known since antiquity,Spaceflight,Nature,Stellar astronomy]
---


Enabling & Support ESA’s Proba-3 will create artificial solar eclipses 17/08/2017 7416 views 107 likes  Astrophysicists are joining sightseers in watching Monday’s total solar eclipse across North America but, in the decade to come, they will be viewing eclipses that last for hours instead of a few minutes – thanks to a pioneering ESA space mission. Aiming for launch in late 2020, Proba-3 is not one but two small metre-scale satellites, lining up to cast a precise shadow across space to block out the solar disc for six hours at a time, and give researchers a sustained view of the Sun’s immediate vicinity. Total eclipses occur thanks to a remarkable cosmic coincidence: Earth’s Moon is about 400 times smaller than our parent star, which is about 400 times further away. Researchers seek ways to increase the corona’s visibility, chiefly through ‘coronagraphs’ – telescopes bearing discs to block out the direct light of the Sun. Proba-3's pair of satellites “Instead, Proba-3’s coronagraph uses two craft: a camera satellite and a disc satellite.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/Proba_Missions/ESA_s_Proba-3_will_create_artificial_solar_eclipses){:target="_blank" rel="noopener"}


