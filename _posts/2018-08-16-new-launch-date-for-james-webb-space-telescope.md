---
layout: post
title: "New launch date for James Webb Space Telescope"
date: 2018-08-16
categories:
author: ""
tags: [James Webb Space Telescope,Spacecraft,Astronomy,Space science,Outer space,Spaceflight,Science,Astronautics,Space exploration,Physical sciences,Flight,Space programs,Space research,Space agencies,Space vehicles]
---


Science & Exploration New launch date for James Webb Space Telescope 28/06/2018 59534 views 279 likes  After completion of an independent review, a new launch date for the James Webb Space Telescope has been announced: 30 March 2021. The wait will be a little longer now but the breakthrough science that it will enable is absolutely worth it, says Günther Hasinger, ESA Director of Science. The new launch date in early 2021 was estimated taking this into account and incorporating the recommendations from the review board. This module, which completed its tests last year, includes the NIRSpec and MIRI instruments – part of Europe’s contribution to the observatory. The James Webb Space Telescope is an international project led by NASA with its partners, ESA and the Canadian Space Agency.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/New_launch_date_for_James_Webb_Space_Telescope){:target="_blank" rel="noopener"}


