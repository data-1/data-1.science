---
layout: post
title: "New project aims to establish a human colony on Mars"
date: 2016-05-03
categories:
author: Tomasz Nowakowski, Astrowatch.Net
tags: [SpaceX Dragon,Mars,SpaceX,Falcon Heavy,Spaceflight,Spaceflight technology,Astronomy,Space vehicles,Spacecraft,Space science,Flight,Astronautics,Outer space,Aerospace]
---


The rover is slated to be launched in 2018, 2 years after the start of NASA's InSight mission on which the teams' robot would be based on. Credit: MarsPolar  In 2020 the Mars Transportation Vehicle (MTV) will take 35 tons of payload to the Low Earth Orbit (LEO). MTV will be load with food, water and oxygen supplies as well as more than 20 tons of hardware for future Mars explorers like: Dragon spacecraft, habitats, life support system, solar arrays, spacesuits or water extraction unit. Every 2 years another spacecraft could be send to the Red Planet as there is only one launch window every 26 months, when Mars and Earth line up and the journey takes only 6-7 months. Nevertheless, the team has prepared a concept of the Earth Return Vehicle (ERV), which may be used in the future.

<hr>

[Visit Link](http://phys.org/news351840875.html){:target="_blank" rel="noopener"}


