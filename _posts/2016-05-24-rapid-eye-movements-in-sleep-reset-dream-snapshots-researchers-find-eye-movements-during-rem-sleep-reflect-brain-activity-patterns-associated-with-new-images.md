---
layout: post
title: "Rapid eye movements in sleep reset dream 'snapshots': Researchers find eye movements during REM sleep reflect brain activity patterns associated with new images"
date: 2016-05-24
categories:
author: American Friends of Tel Aviv University
tags: [Electroencephalography,Rapid eye movement sleep,Brain,Sleep,Psychology,Neuroscience,Cognitive science,Cognition,Mental processes,Physiology,Cognitive psychology,Psychological concepts]
---


Because REM sleep is associated with dreaming, on the one hand, and eye movement, on the other, it has been tempting to assume that each movement of the eye is associated with a specific dream image. When we move our eyes in REM sleep, according to the study, specific brain regions show sudden surges of activity that resemble the pattern that occurs when we are introduced to a new image -- suggesting that eye movements during REM sleep are responsible for resetting our dream snapshots. Deep down in the brain  Our goal was to examine what happens deep in the human brain during REM sleep, specifically when rapid eye movements occur, said Dr. Nir. Electrodes were implanted deep inside the patients' brains to monitor their brain activity over the course of 10 days. Images, awake and asleep  The electrical brain activity during rapid eye movements in sleep were highly similar to those occurring when people were presented with new images, said Dr. Nir.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150812131924.htm){:target="_blank" rel="noopener"}


