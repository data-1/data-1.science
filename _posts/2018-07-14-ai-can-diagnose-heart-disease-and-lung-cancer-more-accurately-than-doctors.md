---
layout: post
title: "AI Can Diagnose Heart Disease and Lung Cancer More Accurately Than Doctors"
date: 2018-07-14
categories:
author: ""
tags: [Medical diagnosis,Artificial intelligence,National Health Service,Cancer,Cardiology,Health sciences,Medicine,Social programs,Public services,Clinical medicine,Health care,Health,Medical specialties]
---


Improved Diagnosis  Artificial intelligence (AI) has already proven useful in the healthcare industry, and now, two newly developed AI diagnostics systems could change how doctors diagnose heart disease and lung cancer. To determine whether or not something's wrong with a patient's heart, a cardiologist will assess the timing of their heartbeat in scans. That system has also been tested in various trials, and the company's chief science and technology officer, Timor Kadir, told BBC News that the results suggest it could diagnose as many as 4,000 lung cancer patients per year earlier than doctors can. Saving Lives and Money  Not only could these AI diagnostics systems save lives by providing earlier diagnosis of heart problems and lung cancer, they could also save money that could then be put toward anything from hiring more doctors, nurses, and hospital staff to new equipment. AI may be the thing that saves the NHS.

<hr>

[Visit Link](https://futurism.com/ai-diagnose-heart-disease-lung-cancer-more-accurately-doctors/){:target="_blank" rel="noopener"}


