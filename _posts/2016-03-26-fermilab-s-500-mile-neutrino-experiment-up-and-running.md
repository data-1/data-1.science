---
layout: post
title: "Fermilab's 500-mile neutrino experiment up and running"
date: 2016-03-26
categories:
author: Fermi National Accelerator Laboratory
tags: [NOvA,Fermilab,Neutrino,Particle physics,Physics,Science]
---


A view across the top of the NOvA far detector in Ash River, Minnesota. Electronics that make up part of the data acquisition system are installed along the top and sides of the detector. Using the world's most powerful beam of neutrinos , generated at the U.S. Department of Energy's Fermi National Accelerator Laboratory near Chicago, the NOvA experiment can precisely record the telltale traces of those rare instances when one of these ghostly particles interacts with matter. NOvA's particle detectors were both constructed in the path of the neutrino beam sent from Fermilab in Batavia, Illinois , to northern Minnesota. The 14,000-ton far detector - constructed in Ash River, Minnesota, near the Canadian border – spots those neutrinos after their 500-mile trip, and allows scientists to analyze how they change over that long distance.

<hr>

[Visit Link](http://phys.org/news331835909.html){:target="_blank" rel="noopener"}


