---
layout: post
title: "Which technologies are poised to take over in open source?"
date: 2017-10-23
categories:
author: "Scott Hirleman"
tags: [Computing,Communication,Software engineering,World Wide Web,Software development,Information technology,Information technology management,Software,Technology]
---


When you think of open source technologies, you probably think of the stalwarts, the technologies that have been around for years and years. We compared our survey results from the past three years to identify trends, and our data shows that newer technologies are gaining significant ground on established technologies. NGINX, used by 14% of survey respondents, is gaining quickly on Apache HTTP Server (18%), which seems to correlate with overall market share trends. Apache Spark (15%) is gaining strongly on the older Apache Hadoop, which was used by 27% of tech professionals participating in our 2015 survey, but only 17% of them in 2017—a decrease of 58%. Many of them are contenders to be big players, but the number of tools people are using also continues to expand.

<hr>

[Visit Link](https://opensource.com/article/17/5/what-technologies-are-poised-take-over){:target="_blank" rel="noopener"}


