---
layout: post
title: "Newly discovered algal enzyme for more efficient photosynthesis, better crops"
date: 2017-03-17
categories:
author: "Lawrence Berkeley National Laboratory"
tags: [Photosynthesis,Photoprotection,Plant,Enzyme,Xanthophyll,Chemistry,Biology,Nature]
---


The plant dissipates the excess light energy to prevent damage and oxidative stress, and a process called the xanthophyll cycle helps to flip the switch between energy dissipation and energy utilization. The alga produced an enzyme called Chlorophycean VDE (CVDE) that was completely different from the other plant enzymes in the xanthophyll cycle. To confirm whether this new enzyme performed the same role as the other xanthophyll enzymes, the researchers inserted the CVDE gene into mutant forms of algae and plants that do not produce zeaxanthin. By analyzing its evolutionary history, the researchers found that CVDE most likely evolved from an ancient enzyme that was present in the common ancestor of green algae and plants. The researchers said the CVDE enzyme could be used to optimize photoprotection and thereby improve photosynthesis and crop productivity.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/09/160920135224.htm){:target="_blank" rel="noopener"}


