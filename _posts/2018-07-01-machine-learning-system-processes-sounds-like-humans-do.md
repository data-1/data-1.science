---
layout: post
title: "Machine-learning system processes sounds like humans do"
date: 2018-07-01
categories:
author: "Anne Trafton"
tags: [Auditory cortex,Neural network,Perception,Brain,Cerebral cortex,Deep learning,Neuroscientist,Hierarchy,Branches of science,Cognitive psychology,Cognitive neuroscience,Interdisciplinary subfields,Cognition,Mental processes,Neuroscience,Cognitive science]
---


Using a machine-learning system known as a deep neural network, MIT researchers have created the first model that can replicate human performance on auditory tasks such as identifying a musical genre. “What these models give us, for the first time, is machine systems that can perform sensory tasks that matter to humans and that do so at human levels,” says Josh McDermott, the Frederick A. and Carole J. Middleton Assistant Professor of Neuroscience in the Department of Brain and Cognitive Sciences at MIT and the senior author of the study. Modeling the brain  When deep neural networks were first developed in the 1980s, neuroscientists hoped that such systems could be used to model the human brain. The MIT researchers trained their neural network to perform two auditory tasks, one involving speech and the other involving music. To see if the model stages might replicate how the human auditory cortex processes sound information, the researchers used functional magnetic resonance imaging (fMRI) to measure different regions of auditory cortex as the brain processes real-world sounds.

<hr>

[Visit Link](http://news.mit.edu/2018/machine-learning-system-processes-sounds-humans-do-0419){:target="_blank" rel="noopener"}


