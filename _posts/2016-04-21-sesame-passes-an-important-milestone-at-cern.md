---
layout: post
title: "SESAME passes an important milestone at CERN"
date: 2016-04-21
categories:
author:  
tags: [Synchrotron-Light for Experimental Science and Applications in the Middle East,Technology,Science]
---


An engineer tests the installation of a vacuum chamber for SESAME, at CERN's magnet-testing facility SM18. Credit: Maximilien Brice/CERN  The SESAME project has reached an important milestone: the first complete cell of this accelerator for the Middle East has been assembled and successfully tested at CERN. Within this project, CERN has been collaborating with SESAME to design, test and characterize the components of the magnetic system, which is now in production. 8 sextupoles assembled in Cyprus and Pakistan based on CERN/SESAME design. After acceptance tests, these components will be shipped in batches to SESAME by the end of the year, where installation and commissioning of the main synchrotron is planned for 2016.

<hr>

[Visit Link](http://phys.org/news347698652.html){:target="_blank" rel="noopener"}


