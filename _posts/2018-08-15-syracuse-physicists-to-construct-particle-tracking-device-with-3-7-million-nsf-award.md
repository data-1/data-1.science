---
layout: post
title: "Syracuse physicists to construct particle tracking device with $3.7 million NSF award"
date: 2018-08-15
categories:
author: "Syracuse University"
tags: [Quark,LHCb experiment,Matter,Particle physics,Large Hadron Collider,Standard Model,Hadron,Universe,Physics,Fundamental interaction,Atomic nucleus,Physical sciences,Nature,Science,Nuclear physics,Theoretical physics,Quantum mechanics,Quantum field theory]
---


The High-Energy Physics (HEP) Group in the College of Arts and Sciences (A&S) is the recipient of a three-year, $3.7 million NSF award, supporting ongoing research into the fundamental forces and particles in the universe. Whereas light quarks make up protons and neutrons in the nucleus of an atom, heavy quarks form other nuclei and mesons (i.e., particles with two quarks). Heavy quarks are produced in the Large Hadron Collider beauty (LHCb) experiment at the CERN laboratory in Geneva, Switzerland. Integral to HEP's work is the Standard Model, a theory describing all matter and forces, except for gravity, in the universe. As a result, we have to look beyond the Standard Model to understand what the universe is made of and how it has come to be.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/su-spt081418.php){:target="_blank" rel="noopener"}


