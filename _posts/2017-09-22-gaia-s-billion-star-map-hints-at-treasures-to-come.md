---
layout: post
title: "Gaia’s billion-star map hints at treasures to come"
date: 2017-09-22
categories:
author: ""
tags: [Gaia (spacecraft),Star,Hipparcos,Star cluster,Milky Way,Astrometry,RR Lyrae variable,Astronomy,Parallax,Variable star,Astronomical objects,Physical sciences,Science,Stellar astronomy,Outer space,Space science]
---


On its way to assembling the most detailed 3D map ever made of our Milky Way galaxy, Gaia has pinned down the precise position on the sky and the brightness of 1142 million stars. “But with Gaia’s first data, it is now possible to measure the distances and motions of stars in about 400 clusters up to 4800 light-years away. “Variable stars like Cepheids and RR Lyraes are valuable indicators of cosmic distances,” explains Gisella Clementini from INAF and the Astronomical Observatory of Bologna, Italy. The variability pattern is easy to measure and can be combined with the apparent brightness of a star to infer its true brightness. Scientific exploitation of the data will only take place once they are openly released to the community.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Gaia/Gaia_s_billion-star_map_hints_at_treasures_to_come){:target="_blank" rel="noopener"}


