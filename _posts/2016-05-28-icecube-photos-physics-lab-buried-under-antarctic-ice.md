---
layout: post
title: "IceCube Photos: Physics Lab Buried Under Antarctic Ice"
date: 2016-05-28
categories:
author: Jeanna Bryner
tags: [IceCube Neutrino Observatory,Astronomy,Physical sciences,Science]
---


A gigantic observatory called IceCube lurks beneath Antarctic ice at the South Pole, where detectors scan the cosmos for ghostly, near-massless neutrinos. The network of people who make the lab run (called the IceCube Collaboration) includes about 300 physicists hailing from 45 institutions and 12 countries. (Photo Credit: Dag Larsen, IceCube/NSF)  Gorgeous strings  The part of the observatory that's buried beneath the ice holds 5,160 sensors called digital optical modules (DOMs). (Photo Credit: Jim Haugen, IceCube/NSF)  Leaving a mark  The team of scientists, engineers and drillers who deployed the observatory in December 2010 signed the last sensor before it was buried beneath a mile of Antarctic ice. (Photo Credit: DESY)  Sista  Here the last DOM, which was assembled at Stockholm University.

<hr>

[Visit Link](http://www.livescience.com/51924-icecube-observatory-photos.html){:target="_blank" rel="noopener"}


