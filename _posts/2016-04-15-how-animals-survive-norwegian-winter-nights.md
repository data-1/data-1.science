---
layout: post
title: "How animals survive Norwegian winter nights"
date: 2016-04-15
categories:
author: Norwegian University Of Science
tags: [Thermoregulation,Bird,Snow,Animals]
---


For birds and animals that must live out in Norway's frigid winters, every second has to be spent finding enough food not just to survive the day, but also the long hard night. During periods of intense cold, the birds dig down into the snow, where they stay to survive and protect themselves from the cold. Small birds burrow too  A willow tit in deep sleep in a small cave. Air pockets can be hard to find  To prevent heat loss through the course of the night, birds will try to find well insulated spots to sleep, such as an air pocket in the snow or a sheltered spot under a tree branch. During cold nights, willow tits can reduce their body temperature from the average 41 degrees to 31 degrees C, thereby drastically reducing damaging heat loss.

<hr>

[Visit Link](http://phys.org/news344508293.html){:target="_blank" rel="noopener"}


