---
layout: post
title: "Bioengineers put human hearts on a chip to aid drug screening"
date: 2016-04-17
categories:
author: University Of California - Berkeley
tags: [Organ-on-a-chip,Heart,Life sciences,Biology]
---


The study authors noted a high failure rate associated with the use of nonhuman animal models to predict human reactions to new drugs. Shown is human heart tissue, derived from adult stem cells, before and after exposure to isoproterenol, a drug used to treat bradycardia (slow heart rate) and other heart problems. Credit: Video by Dr. Anurag Mathur/Healy Lab  The heart cells were derived from human-induced pluripotent stem cells, the adult stem cells that can be coaxed to become many different types of tissue. The researchers designed their cardiac microphysiological system, or heart-on-a-chip, so that its 3-D structure would be comparable to the geometry and spacing of connective tissue fiber in a human heart. The researchers put the system to the test by monitoring the reaction of the heart cells to four well-known cardiovascular drugs: isoproterenol, E-4031, verapamil and metoprolol.

<hr>

[Visit Link](http://phys.org/news345051201.html){:target="_blank" rel="noopener"}


