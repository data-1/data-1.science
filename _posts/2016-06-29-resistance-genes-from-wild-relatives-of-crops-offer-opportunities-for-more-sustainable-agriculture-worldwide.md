---
layout: post
title: "Resistance genes from wild relatives of crops offer opportunities for more sustainable agriculture worldwide"
date: 2016-06-29
categories:
author: "Wageningen University"
tags: [Phytophthora infestans,Plant disease resistance,Agriculture,Potato]
---


These were the findings of an international scientific conference held in Wageningen on 3 September on the conclusion of a study into more durable resistance against Phytophthora through the use of resistance genes from related wild potato species (DuRPh), which was funded by the Dutch government for ten years. The cultivation of potatoes requires far more pesticides than with other crops and growers use them in particular to protect crops against the late blight caused by Phytophthora infestans. Environmental impact  Conventional potato cultivation requires fungicides to be applied 10 to 15 times a year to keep the disease at bay. The study also helped develop effective resistance management, which monitors the genetic variation of the pathogen throughout the Netherlands. Smart combination within cultivars of sets of multiple resistance genes from related species, introduced into the crop via for instance genetic engineering, combined with proper monitoring of the pathogen and timely application of combined resistance genes will allow the resistant cultivars to retain their resistance to the disease in the long term for these plants too.

<hr>

[Visit Link](http://phys.org/news/2015-09-resistance-genes-wild-relatives-crops.html){:target="_blank" rel="noopener"}


