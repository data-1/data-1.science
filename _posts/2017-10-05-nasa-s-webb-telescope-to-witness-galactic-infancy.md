---
layout: post
title: "NASA's Webb Telescope to witness galactic infancy"
date: 2017-10-05
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Hubble Ultra-Deep Field,Hubble Space Telescope,Galaxy,James Webb Space Telescope,Spitzer Space Telescope,NIRCam,Great Observatories program,Telescope,Space telescope,Spectroscopy,Astronomical observatories,Science,Physical sciences,Space science,Electromagnetic radiation,Optics,Astronomy,Observational astronomy,Scientific observation,Outer space,Telescopes,Astronomical imaging]
---


Scientists will use NASA's James Webb Space Telescope to study sections of the sky previously observed by NASA's Great Observatories, including the Hubble Space Telescope and the Spitzer Space Telescope, to understand the creation of the universe's first galaxies and stars. The group of scientists will primarily use Webb's mid-infrared instrument (MIRI) to examine a section of HUDF, and Webb's near infrared camera (NIRCam) to image part of GOODS. Pérez-González said they will use the instrument to observe a section of HUDF in 5.6 microns, which Spitzer is capable of, but that Webb will be able to see objects 250 times fainter and with eight times more spatial resolution. Pérez-González said in the area of HUDF they will observe, Hubble was able to see about 4,000 galaxies. Pérez-González explained they will use it to observe a section of GOODS in the 1.15 micron band, which Hubble is capable of, but that Webb will be able to see objects 50 times fainter and with two times more spatial resolution.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/nsfc-nwt100417.php){:target="_blank" rel="noopener"}


