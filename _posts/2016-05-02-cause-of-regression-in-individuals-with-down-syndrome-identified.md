---
layout: post
title: "Cause of regression in individuals with Down syndrome identified"
date: 2016-05-02
categories:
author: University of Missouri-Columbia
tags: [Catatonia,Mental disorder,Down syndrome,Syndrome,Autism,Health sciences,Health,Clinical medicine,Diseases and disorders,Human diseases and disorders,Causes of death,Mental disorders,Medicine,Medical specialties,Mental health,Epidemiology,Abnormal psychology,Health care]
---


Now, a researcher at the University of Missouri has found that Catatonia, a treatable disorder, may cause regression in patients with Down syndrome. Individuals with regressive Down syndrome who were treated for Catatonia showed improvement, the researcher found. Our recognition that Catatonia occurs in young adults and adolescents with Down syndrome means these individuals who before were relegated to lives of incapacity may now receive treatments that restore them to their usual levels of activity. The wonderful thing is that Catatonia is a treatable cause of regression, said Miles, a pediatrician who led the Down syndrome clinic at MU for many years. Miles said she wants families, physicians, teachers and therapists to know that Catatonia causes regression in Down syndrome so individuals with the disorder can receive accurate diagnosis and treatment.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/uom-cor051215.php){:target="_blank" rel="noopener"}


