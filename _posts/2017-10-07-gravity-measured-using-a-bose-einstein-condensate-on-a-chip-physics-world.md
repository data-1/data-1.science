---
layout: post
title: "Gravity measured using a Bose–Einstein condensate on a chip – Physics World"
date: 2017-10-07
categories:
author: "Hamish Johnston"
tags: [BoseEinstein condensate,Gravimetry,Physics,Physical chemistry,Theoretical physics,Metrology,Applied and interdisciplinary physics,Physical sciences,Science]
---


Atoms can be used to measure the acceleration due to gravity by cooling a gas of them to near absolute zero and then dropping them along two different paths in an interferometer. While these ultracold atom gravimeters are on a par with conventional absolute gravimeters based on macroscopic falling masses, their accuracy could be improved a lot by using a BEC. The BEC is then allowed to fall about 1 cm through the chamber and, as it does so, a series of laser pulses is fired at the BEC, deflecting the atoms into different paths to create an interferometer. Rasel and colleagues were able to measure the acceleration of the atoms with an accuracy of about one part in 107 – at least an order of magnitude worse than commercial gravimeters. By reducing the vibrations and doing other improvements, Rasel and colleagues reckon the gravimeter could operate with an uncertainty of less than one part in 109 and fit inside of a backpack.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2016/nov/22/gravity-measured-using-a-bose-einstein-condensate-on-a-chip){:target="_blank" rel="noopener"}


