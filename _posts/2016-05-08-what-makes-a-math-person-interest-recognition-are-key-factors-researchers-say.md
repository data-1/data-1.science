---
layout: post
title: "What makes a math person? Interest, recognition are key factors, researchers say"
date: 2016-05-08
categories:
author: Chrystian Tejedor, Florida International University
tags: [Education,Branches of science,Communication,Science]
---


Now, National Science Foundation-funded research conducted by Florida International University Professor Zahra Hazari shows that's not really the case. Their research, published recently in the journal Child Development, suggests that interest and recognition are key factors that can help students become math persons, while confidence in one's abilities is not enough. Data analysis revealed that while belief in one's competence and performance is indeed a factor to seeing oneself as a math person, it is secondary to the individual's interest in the subject and the recognition received from teachers, parents, relatives or friends. If we want to empower students and provide access to STEM careers, it can't just be about confidence and performance. Believing they can do challenging math is another way of recognizing them.

<hr>

[Visit Link](http://phys.org/news353749712.html){:target="_blank" rel="noopener"}


