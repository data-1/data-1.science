---
layout: post
title: "Hottest lava eruption linked to growth of first continents"
date: 2014-06-24
categories:
author: David Stacey, University Of Western Australia
tags: [Craton,Earth,Geology,Earth sciences,Nature,Physical sciences,Planetary science]
---


(Phys.org) —A collaborative research team has discovered an important link between the eruption of Earth's hottest lavas, the location of some of the largest ore deposits and the emergence of the first land masses on the planet - the continents - more than 2500 million years ago. They are the signature rock type of a hotter Earth in the primordial stages of its evolution, and provide the most direct link between the Earth's interior and the Earth's surface, Professor Fiorentini said. This 'cratonisation' process formed deep roots to the continental land masses, extending more than 200km deep into the Earth, he said. The ability to map these continental blocks through time points to the location where major metal deposits formed in these lavas. As a result, the dynamic evolution of the early continents directly influenced where deep mantle material was added to the Archean crust, oceans and atmosphere.

<hr>

[Visit Link](http://phys.org/news322806930.html){:target="_blank" rel="noopener"}


