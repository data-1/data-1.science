---
layout: post
title: "60 mn people worldwide hit by El Nino: UN"
date: 2016-04-29
categories:
author:  
tags: [El Nio,Physical geography]
---


Stephen O'Brien, Under-Secretary-General for Humanitarian Affairs and Emergency Relief, speaks during the meeting on the humanitarian situation in Syria January 27, 2016 at the UN in New York  Some 60 million people worldwide need assistance due to havoc wreaked by the El Nino climate phenomenon, but a shortage of funding could threaten the delivery of life-saving aid, the UN warned Tuesday. The numbers are truly alarming, UN humanitarian chief Stephen O'Brien told reporters in Geneva. The El Nino effect, which comes with warming sea surface temperatures in the equatorial Pacific, causes heavy rains in some parts of the world and drought elsewhere. In addition to the some 60 million people directly affected by El Nino, there will be millions more who are at risk, O'Brien said, following a meeting in Geneva with representatives of affected countries and aid organisations. Making matters worse, the communities still reeling from the impact of El Nino are likely to get slammed again later this year by a return swing of the pendulum with its opposite number, La Nina.

<hr>

[Visit Link](http://phys.org/news/2016-04-mn-people-worldwide-el-nino.html){:target="_blank" rel="noopener"}


