---
layout: post
title: "RIT to develop hybrid biological cell separations technology for lab-on-chip devices"
date: 2017-10-11
categories:
author: "Rochester Institute of Technology"
tags: [Chromatography,Lab-on-a-chip,Dielectrophoresis,Microfluidics,Chemistry,Technology,Physical sciences,Laboratory techniques,Laboratories,Scientific techniques,Applied and interdisciplinary physics,Biotechnology,Manufacturing]
---


Blanca Lapizco-Encinas, a faculty-researcher in RIT's Kate Gleason College of Engineering, is improving the process of separating biological cells and biomolecules using chromatography principles, a well-established technique for separating proteins, combined with a newer technique called dielectrophoresis, a process that uses electrical current to separate biomolecules. You put into a device a sample with six or seven different types of particles and you can separate them, in some cases in less than two minutes, just by applying electric fields, Lapizco-Encinas explained. In chromatography, for example, particles enter a chromatographic column, and different particles are retained in different degrees. Through past research, she and her team advanced device system designs and determined an optimal threshold of electrical fields applied to adequately manipulate the fluids and ensure that live cells are not damaged. We have preliminary designs we are currently testing, and those designs and experiments help us to improve the model and move forward to the next generation.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/riot-rtd100917.php){:target="_blank" rel="noopener"}


