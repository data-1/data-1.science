---
layout: post
title: "An Overview of Google DeepMind in Just 20 Minutes [Video]"
date: 2018-07-14
categories:
author: "Jan."
tags: []
---


An Overview of Google DeepMind in Just 20 Minutes [Video]  Join the DZone community and get the full member experience. So much has been said about AI and machine learning — and so much of it is hyperbole. But instead of hypotheticals, let's hear it from the horse's mouth. In this video, Juan Silveira gives an overview of all the work that DeepMind has been doing. DeepMind Google (verb)  Published at DZone with permission of Laurence Moroney, DZone MVB.

<hr>

[Visit Link](https://dzone.com/articles/a-20-minute-overview-of-google-deepmind-in-just-20?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


