---
layout: post
title: "Borneo deforested 30 percent over past 40 years"
date: 2015-10-03
categories:
author: Public Library Of Science
tags: [Forest,Technology]
---


The native forests of Borneo have been increasingly impacted by logging, fire, and conversion to plantations since the early 1970s. To better understand long-term forest cover and logging patterns, the researchers in this study analyzed LANDSAT satellite images from 1973 to 2010. The authors found that in the early 1970s, ~75% of Borneo was forested, and from 1973 to 2010, the forest area declined by ~30%, which is nearly twice as fast as a rate compared to the rest of the world's humid tropical forests. Over 389,000 km2 of Borneo remains either completely or partially forested, and the authors hope that understanding forest change patterns may aid in future conservation planning, particularly in selectively logged forests. Explore further 80 percent of Malaysian Borneo degraded by logging

<hr>

[Visit Link](http://phys.org/news324720897.html){:target="_blank" rel="noopener"}


