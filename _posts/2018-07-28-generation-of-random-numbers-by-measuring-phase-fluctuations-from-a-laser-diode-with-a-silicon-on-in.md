---
layout: post
title: "Generation of random numbers by measuring phase fluctuations from a laser diode with a silicon-on-in"
date: 2018-07-28
categories:
author: "Optica"
tags: [Silicon photonics,Laser,Photonics,Optics,Random number generation,Integrated circuit,Electronics,Light,Photon,Hardware random number generator,Atomic molecular and optical physics,Science,Electromagnetic radiation,Electricity,Electrical engineering,Technology,Electromagnetism]
---


In The Optical Society (OSA) journal Optics Express, the researchers report a quantum random number generator based on randomly emitted photons from a diode laser. Silicon photonics  The new chip was enabled by developments in silicon photonics technology, which uses the same semiconductor fabrication techniques used to make computer chips to fabricate optical components in silicon. These waveguides can be integrated onto a chip with electronics and integrated detectors that operate at very high speeds to convert the light signals into information. We demonstrated random number generation using about a tenth of the power used in other chip-based quantum random number generator devices, said Raffaelli. Optics Express is an open-access journal and is available at no cost to readers online at: OSA Publishing.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/tos-gor072318.php){:target="_blank" rel="noopener"}


