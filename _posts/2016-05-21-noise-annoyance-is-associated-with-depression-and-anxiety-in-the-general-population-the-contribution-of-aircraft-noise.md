---
layout: post
title: "Noise Annoyance Is Associated with Depression and Anxiety in the General Population- The Contribution of Aircraft Noise"
date: 2016-05-21
categories:
author: Manfred E. Beutel, Department Of Psychosomatic Medicine, Psychotherapy, University Medical Center Of The Johannes Gutenberg University Mainz, Mainz, Claus Jünger, Medical Clinic For Cardiology, Angiology, Intensive Care Medicine, Eva M. Klein
tags: []
---


Interpretation Strong noise annoyance was associated with a two-fold higher prevalence of depression and anxiety in the general population. Nearly twice the number of women reported anxiety disorders or depression compared to men. More than half (52.8%) of the population were at least moderately annoyed and thus at an increased risk for depression and for generalized anxiety; the intensity of the risk increased steadily with the degree of annoyance amounting to a more than two-fold risk (2.12 for depression, 2.28 for generalized anxiety) at the level of extreme annoyance. Limitations of the study refer to the age range (35 to 74 years) and the participation rate. Further analyses will help to specify the contributions of specific sources of noise to annoyance during the day and in the sleep.

<hr>

[Visit Link](http://journals.plos.org/plosone/article?id=10.1371%2Fjournal.pone.0155357){:target="_blank" rel="noopener"}


