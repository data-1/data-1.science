---
layout: post
title: "Syracuse physicists confirm existence of rare pentaquarks discovery"
date: 2016-05-12
categories:
author: Syracuse University
tags: [Hadron,Strong interaction,Quark,Meson,Theoretical physics,Particle physics,Physics,Nuclear physics,Standard Model,Quantum field theory,Quantum chromodynamics,Subatomic particles,Quantum mechanics,Physical sciences,Hadrons,Nature]
---


The statistical evidence of these new pentatquark states is beyond question, says Sheldon Stone, Distinguished Professor of Physics, who helped engineer the discovery. In addition to Stone, the research team includes other physicists with ties to Syracuse: Tomasz Skwarnicki, professor of physics; Nathan Jurik G'16, a Ph.D. student; and Liming Zhang, a former University research associate who is now an associate professor at Tsinghua University in Beijing, China. One of them is the issue of how quarks bind together. The color-neutral meson and baryon feel a residual strong force [that is] similar to the one binding nucleons to form nuclei. Adds Stone: The theory of strong interactions is the only strongly coupled theory we have.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/su-spc071415.php){:target="_blank" rel="noopener"}


