---
layout: post
title: "NASA Has Developed Autonomous Space Navigation That Uses Pulsars"
date: 2018-06-21
categories:
author: ""
tags: [Neutron Star Interior Composition Explorer,NASA,Pulsar,Navigation,Space science,Astronomy,Outer space,Spaceflight,Astronautics,Science,Flight,Physical sciences,Space exploration]
---


X-Ray Navigation  NASA may have just improved our potential for deep space exploration by inventing a new type of autonomous space navigation. Known as Station Explorer for X-Ray Timing and Navigation Technology, or SEXTANT, the technology uses pulsars — rotating neutron stars that emit electromagnetic radiation — to determine the location of objects in space. The way SEXTANT uses pulsars has been compared to how GPS navigation can provide drivers with positioning and accurate navigation using satellites orbiting around Earth. Within eight hours, SEXTANT was able to autonomously determine NICER's location in Earth's orbit within a 10-mile radius. This successful demonstration firmly establishes the viability of X-ray pulsar navigation as a new autonomous navigation capability, Mitchell added in the press release.

<hr>

[Visit Link](https://futurism.com/nasa-autonomous-space-navigation/){:target="_blank" rel="noopener"}


