---
layout: post
title: "Sentinel-3B liftoff replay"
date: 2018-08-16
categories:
author: ""
tags: [Sentinel-3,Earth sciences,Outer space,Spaceflight,Space science,Spacecraft,Satellites,Astronautics,Space vehicles]
---


The second Copernicus Sentinel-3 satellite, Sentinel-3B, lifted off on a Rockot from the Plesetsk Cosmodrome in northern Russia at 17:57 GMT (19:57 CEST) on 25 April 2018. Sentinel-3B joins its twin, Sentinel-3A, in orbit. The pairing of identical satellites provides the best coverage and data delivery for Europe’s Copernicus programme – the largest environmental monitoring programme in the world. The satellites carry the same suite of cutting-edge instruments to measure oceans, land, ice and atmosphere. Feeding a new generation of data products, the Sentinel-3 mission is at the heart of operational oceanography.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2018/04/Sentinel-3B_liftoff_replay){:target="_blank" rel="noopener"}


