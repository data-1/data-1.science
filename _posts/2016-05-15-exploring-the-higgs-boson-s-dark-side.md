---
layout: post
title: "Exploring the Higgs boson's dark side"
date: 2016-05-15
categories:
author: Pete Wilton, Oxford University
tags: [Elementary particle,Large Hadron Collider,Higgs boson,Supersymmetry,ATLAS experiment,High Luminosity Large Hadron Collider,Particle physics,Standard Model,Matter,Spin (physics),Electron,Quantum mechanics,Scientific theories,Nature,Applied and interdisciplinary physics,Quantum field theory,Physical sciences,Subatomic particles,Science,Physics,Theoretical physics]
---


In 2012 CERN's Large Hadron Collider (LHC) discovered the Higgs boson, the 'missing piece' in the jigsaw of particles predicted by the Standard Model. 'We do not know what we will find next and that makes the new run even more exciting,' Daniela Bortoletto of Oxford University's Department of Physics, a member of the team running the LHC's ATLAS experiment, tells me. 'We hope to finally find some cracks in the Standard Model as there are many questions about our universe that it does not answer.' 'We have finally discovered the Higgs boson: this special particle, a particle that does not carry any spin, might decay to dark matter particles and may even explain why the Universe is matter dominated.' 'The Higgs is the first spin 0 particle, or as particle physicists would say the first 'scalar particle' we've found, so the Higgs is neither matter nor force.'

<hr>

[Visit Link](http://phys.org/news/2015-07-exploring-higgs-boson-dark-side.html){:target="_blank" rel="noopener"}


