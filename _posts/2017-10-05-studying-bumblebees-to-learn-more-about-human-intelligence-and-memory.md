---
layout: post
title: "Studying bumblebees to learn more about human intelligence and memory"
date: 2017-10-05
categories:
author: "Bob Yirka"
tags: [Memory,Bumblebee,Brain,Intelligence,Human intelligence,Learning,Nervous system,Psychology,Behavioural sciences,Mental processes,Interdisciplinary subfields,Cognitive psychology,Cognition,Cognitive science,Neuroscience]
---


The team noted how long it took the individual bees to figure out which type of flower would offer a reward and which would not. The researchers suggest theirs is the first study to show that learning, at least visually, can be correlated to increased nerve connection density in some parts of the brain. And while the study was meant to enhance understanding of the factors present in differing levels of intelligence in humans, it is not clear if the results of these experiments are applicable. Here, we explored how the density of microglomeruli (synaptic complexes) within specific regions of the bumblebee (Bombus terrestris) brain relates to both visual learning and inter-individual differences in learning and memory performance on a visual discrimination task. Further experiments indicated experience-dependent changes in neural circuitry: learning a colour-reward contingency with 10 colours (but not two colours) does result, and exposure to many different colours may result, in changes to microglomerular density in the collar region of the mushroom bodies.

<hr>

[Visit Link](https://phys.org/news/2017-10-bumblebees-human-intelligence-memory.html){:target="_blank" rel="noopener"}


