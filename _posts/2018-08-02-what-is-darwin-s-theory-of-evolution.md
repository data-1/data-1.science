---
layout: post
title: "What is Darwin's Theory of Evolution?"
date: 2018-08-02
categories:
author: "Ker Than, Ashley P. Taylor, Tom Garner"
tags: [Evolution,Natural selection,Mutation,Lamarckism,Gene,Genetics,Charles Darwin,Adaptation,Species,Evolution of cetaceans,Jean-Baptiste Lamarck,Ungulate,Even-toed ungulate,Genetic variation,Organism,Biology,Evolutionary biology,Biological evolution,Nature]
---


He did not know about genetics , the mechanism by which genes encode for certain traits and those traits are passed from one generation to the next. In natural selection, it's the natural environment, rather than a human being, that does the selecting. (Image credit: Illustration by Robert W. Boessenecker)  However, since the early 1990s, scientists have found evidence from paleontology, developmental biology and genetics to support the idea that whales evolved from land mammals. Other theories of evolution  Darwin wasn't the first or only scientist to develop a theory of evolution. Such changes are called mutations .

<hr>

[Visit Link](https://www.livescience.com/474-controversy-evolution-works.html){:target="_blank" rel="noopener"}


