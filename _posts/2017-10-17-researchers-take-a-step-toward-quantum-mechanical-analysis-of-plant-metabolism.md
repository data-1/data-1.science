---
layout: post
title: "Researchers take a step toward quantum mechanical analysis of plant metabolism"
date: 2017-10-17
categories:
author: Karlsruhe Institute Of Technology
tags: [Quantum mechanics,Physics,Quantum simulator,Quantum computing,Superconducting quantum computing,Photon,Theoretical physics,Computer,Light,Physical sciences,Branches of science,Science]
---


Contrary to a quantum computer, however, it is not able to make any calculations, but is designed for the solution of a certain problem, says Jochen Braumüller of KIT's Physikalisches Institut (Institute of Physics). Together with scientists of the Institut für Theoretische Festkörperphysik (TFP, Institute for Theoretical Solid-State Physics), he demonstrated for the first time in an experiment that quantum simulations of the interaction between light and matter work in principle. Hence, quantum computers or the simpler quantum simulators can solve the problem more quickly and efficiently. Braumüller and his co-authors have now developed one of the first functioning components for a quantum simulator of light-matter interaction: Superconducting circuits as quantum bits represent the atoms, while electromagnetic resonators represent the photons. If the planned quantum mechanics simulation is successful, this will be a milestone on the way towards a universal quantum computer.

<hr>

[Visit Link](https://phys.org/news/2017-10-quantum-mechanical-analysis-metabolism.html){:target="_blank" rel="noopener"}


