---
layout: post
title: "Physicists demonstrate new method to make single photons"
date: 2018-07-28
categories:
author: "Erik Arends, Leiden Institute Of Physics"
tags: [Photon,Light,Laser,Physicist,Quantum dot,Optics,Quantum computing,Physical chemistry,Electromagnetism,Scientific theories,Electromagnetic radiation,Physics,Theoretical physics,Quantum mechanics,Science,Atomic molecular and optical physics,Applied and interdisciplinary physics,Physical sciences]
---


Credit: Leiden Institute of Physics  Scientists need individual photons for quantum cryptography and quantum computers. But if you use single photons, you gain access to a hidden level of information. Physicists create single photons as follows: A laser shines on a large artificial atom—a quantum dot—inside an optical cavity. Leiden researchers have now provided experimental evidence for a different way to produce single photons. In this method—called unconventional photon blockade—the quantum dot within the cavity is excited by light of a certain polarization.

<hr>

[Visit Link](https://phys.org/news/2018-07-physicists-method-photons.html){:target="_blank" rel="noopener"}


