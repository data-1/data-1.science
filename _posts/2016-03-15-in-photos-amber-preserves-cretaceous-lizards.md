---
layout: post
title: "In Photos: Amber Preserves Cretaceous Lizards"
date: 2016-03-15
categories:
author: Mindy Weisberger
tags: [CT scan,Science,Skeleton,Amber,Fossil]
---


Colorful scales  (Image credit: Daza et al. Sci. 2016; 2 : e1501080)  Soft tissues rarely survive in fossils preserved in rock, but amber can protect delicate structures, keeping them intact for millions of years. The lovely bones  (Image credit: Daza et al. Sci. 2016; 2 : e1501080)  Even if the lizards' bones weren't preserved, sometimes they left air bubbles behind in the amber that held their former shape. On the tip of his tongue  (Image credit: Daza et al. Sci.

<hr>

[Visit Link](http://www.livescience.com/53945-photos-amber-preserves-cretaceous-lizards.html){:target="_blank" rel="noopener"}


