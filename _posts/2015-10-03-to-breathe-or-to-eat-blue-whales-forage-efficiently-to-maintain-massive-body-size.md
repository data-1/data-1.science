---
layout: post
title: "To breathe or to eat: Blue whales forage efficiently to maintain massive body size"
date: 2015-10-03
categories:
author: NOAA Fisheries West Coast Region 
tags: [Whale,Blue whale,Aquatic feeding mechanisms,Filter feeder,Krill,Foraging,Marine mammal,Animals]
---


As the largest animals to have ever lived on Earth, blue whales maintain their enormous body size through efficient foraging strategies that optimize the energy they gain from the krill they eat, while also conserving oxygen when diving and holding their breath, a new study has found. The feeding pattern that focuses more effort on the densest krill patches provides a new example of blue whale foraging specializations that support the animals' tremendous size. This kind of lunge-feeding takes a lot more effort, but the increase in the amount of energy they get from increased krill consumption more than makes up for it, noted Jeremy Goldbogen, a marine biologist from Stanford University and co-author on the study. But we now know they don't take in that water indiscriminately. In their study, the researchers found a threshold for krill that determined how intensively the blue whales fed.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/nfwc-tbo092515.php){:target="_blank" rel="noopener"}


