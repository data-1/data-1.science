---
layout: post
title: "Image: Studying the genetics of organisms in space"
date: 2017-10-31
categories:
author:  
tags: [Technology]
---


Credit: NASA  At NASA's Kennedy Space Center, organisms in a Petri plate are exposed to blue excitation lighting in a Spectrum prototype unit. Scientists and engineers working on the Spectrum project are developing new hardware for the International Space Station, to support experiments demonstrating how different organisms, such as plants, microbes or worms, develop under conditions of microgravity. Results from the Spectrum project will shed light on which living things are best suited for long-duration flights into deep space. The Spectrum experiments are important because genes control the physical and functional similarity between generations of plants. However, genes do not determine the structure of an organism alone.

<hr>

[Visit Link](https://phys.org/news/2017-10-image-genetics-space.html){:target="_blank" rel="noopener"}


