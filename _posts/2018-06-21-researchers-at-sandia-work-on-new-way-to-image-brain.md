---
layout: post
title: "Researchers at Sandia work on new way to image brain"
date: 2018-06-21
categories:
author: "Sandia National Laboratories"
tags: [Magnetoencephalography,Magnetometer,SQUID,Brain,Neuroscience]
---


Credit: Randy Montoya  Sandia National Laboratories researchers want to use small magnetic sensors to image the brain in a way that's simpler and less expensive than the magnetoencephalography system now used. Magnetoencephalography is a noninvasive way to measure tiny magnetic fields produced by the brain's electrical activity. The team wants to image more of the brain in the future by developing an array that covers the whole head, like today's SQUID systems. Operators combine information to localize the source of the magnetic field to find where the brain is active. Sandia's team is using the measured signals to localize sources in the brain.

<hr>

[Visit Link](https://phys.org/news/2018-01-sandia-image-brain.html){:target="_blank" rel="noopener"}


