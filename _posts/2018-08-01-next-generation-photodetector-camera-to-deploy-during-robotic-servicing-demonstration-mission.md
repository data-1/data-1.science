---
layout: post
title: "Next-generation photodetector camera to deploy during robotic servicing demonstration mission"
date: 2018-08-01
categories:
author: "Lori Keesey, Nasa'S Goddard Space Flight Center"
tags: [Quantum well infrared photodetector,Landsat 8,Goddard Space Flight Center,Infrared,NASA,Technology]
---


The advanced detector technology that will be demonstrated on NASA's upcoming robotic servicing demonstration mission. Strained-Layer Superlattice Technology Enables CTI  CTI's enabling technology is a relatively new photodetector technology known as Strained-Layer Superlattice, or SLS. QWIP Based  SLS is based on the Quantum Well Infrared Photodetector, or QWIP, technology that Jhabvala and his government and industry collaborators spent more than two decades refining. Therefore, detectors designed to measure infrared wavelengths must be cooled to prevent heat generated inside an instrument or spacecraft from contaminating the measurements of the object being observed. Because Jhabvala and his team have created an array that can operate at warmer temperatures, its cooling system is smaller and consumes less power.

<hr>

[Visit Link](https://phys.org/news/2018-07-next-generation-photodetector-camera-deploy-robotic.html){:target="_blank" rel="noopener"}


