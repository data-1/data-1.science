---
layout: post
title: "Why universal basic income costs far less than you think"
date: 2018-08-15
categories:
author: "Elizaveta Fouksman, The Conversation"
tags: [Tax,Universal basic income,Poverty,Economy,Finance,Government finances]
---


But this is not how much UBI costs. This also resolves UBI's billionaire's dilemma – why give someone like Bill Gates a basic income? Cost estimates that consider the difference between upfront and real cost are a fraction of inflated gross cost estimates. Karl's simplified scheme has people slowly start contributing back their UBI in taxes to the common pot as they earn, with net beneficiaries being anyone individually earning less than US$24,000 a year. The numbers are out there – we can pay for a basic income.

<hr>

[Visit Link](https://phys.org/news/2018-08-universal-basic-income.html){:target="_blank" rel="noopener"}


