---
layout: post
title: "Researchers sequence entire genome of seahorse, investigate essential mechanisms of evolution"
date: 2017-03-22
categories:
author: "University Of Konstanz"
tags: [Gene,Seahorse,Evolution,Lined seahorse,Genetics,Gene duplication,Animals,Biology]
---


They obtained new molecular evolutionary results that are relevant for biodiversity research: the loss and duplication of genes as well as the loss of regulative elements in its genome have both contributed to the rapid evolution of the seahorse. This is how the researchers around evolutionary biologist Professor Axel Meyer were able to identify the genetic basis for the disappearance of the seahorse's teeth: several genes that are present in many fish as well humans and contribute to the development of teeth, were lost in seahorses. Transcriptome of the male brood pouch at different stages of pregnancy of the lined seahorse (Hippocampus erectus) was sequenced and analysed. In addition to gene losses, gene duplications during the evolution of the seahorse were also detected. According to this study in Nature, evolution does not only act through changing major roles of genes, but it also influences regulatory elements (genetic switches) during evolution.

<hr>

[Visit Link](http://phys.org/news/2016-12-sequence-entire-genome-seahorse-essential.html){:target="_blank" rel="noopener"}


