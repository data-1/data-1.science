---
layout: post
title: "A Hyperloop-Inspired Transportation System is Being Built in a Major U.S. City"
date: 2018-06-10
categories:
author: ""
tags: [Hyperloop,Virgin Hyperloop,Arrivo,Transport,Vehicles,Service industries,Sustainable transport,Transportation engineering,Technology]
---


Almost Like a Hyperloop  The state of Colorado is in talks with Virgin Hyperloop One to build a hyperloop transportation system, but its Department of Transportation (DOT) doesn't seem content with exploring just one form of advanced transportation. This week the E-470 Public Highway Authority, Colorado's DOT, and transportation company Arrivo announced a new partnership with plans to construct a hyperloop-inspired network in the city of Denver. Concept image of Arrivo's transportation system. Arrivo will end traffic and future-proof regional mobility. Arrivo's transportation pods will max out at 321 kph (200 mph), which the company believes will help cut the trip from the Denver International Airport to Downtown Denver down from 1 hour and 10 minutes to a mere 9 minutes.

<hr>

[Visit Link](https://futurism.com/denver-hyperloop-transportation/){:target="_blank" rel="noopener"}


