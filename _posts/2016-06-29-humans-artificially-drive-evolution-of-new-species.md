---
layout: post
title: "Humans artificially drive evolution of new species"
date: 2016-06-29
categories:
author: "University Of Copenhagen"
tags: [Extinction,Species,Evolution,Human impact on the environment,Biodiversity,Domestication,Speciation,Conservation biology,Hybrid (biology),Natural environment,Nature]
---


Credit: Walkabout12 via Wikimedia Commons  Species across the world are rapidly going extinct due to human activities, but humans are also causing rapid evolution and the emergence of new species. It highlights numerous examples of how human activities influence species' evolution. For instance: as the common house mosquito adapted to the environment of the underground railway system in London, it established a subterranean population. We also see examples of domestication resulting in new species. In the same period, humans have relocated almost 900 known species and domesticated more than 470 animals and close to 270 plant species.

<hr>

[Visit Link](http://phys.org/news/2016-06-humans-artificially-evolution-species.html){:target="_blank" rel="noopener"}


