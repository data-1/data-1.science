---
layout: post
title: "Broad Institute-MIT team identifies highly efficient new cas9 for in vivo genome editing"
date: 2015-12-09
categories:
author: Broad Institute of MIT and Harvard 
tags: [Cas9,CRISPR gene editing,Adeno-associated virus,Genome editing,CRISPR,PCSK9,Viral vector,Broad Institute,Gene,Genetics,Biology,Biotechnology,Life sciences,Molecular biology,Biochemistry,Molecular genetics]
---


The Broad/MIT team, led by Feng Zhang, core member of the Broad Institute and investigator at the McGovern Institute for Brain Research at MIT, along with collaborators at MIT, led by MIT Institute Professor Phillip Sharp, and the NCBI led by Eugene Koonin, set out to identify smaller Cas9 enzymes that could replicate the efficiency of the current SpCas9 system, while allowing packaging into delivery vehicles such as AAV. Again, SaCas9 and SpCas9 demonstrated comparable DNA targeting accuracy. This study highlights the power of using comparative genome analysis to expand the CRISPR-Cas9 toolbox, said Zhang. Researchers can now harness the engineered system to home in on specific nucleic acid sequences and cut the DNA at those precise targets. For further information about the Broad Institute, go to broadinstitute.org.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/biom-bit033015.php){:target="_blank" rel="noopener"}


