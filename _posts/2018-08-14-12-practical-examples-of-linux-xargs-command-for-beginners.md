---
layout: post
title: "12 Practical Examples of Linux Xargs Command for Beginners"
date: 2018-08-14
categories:
author: "Aaron Kili"
tags: [Standard streams,Computer file,Command-line interface,Command (computing),Information technology management,Software,Software engineering,Computers,System software,Computer engineering,Computer science,Software development,Computer architecture,Information Age,Computer programming,Digital media,Computer data,Technology,Computing]
---


Xargs is a great command that reads streams of data from standard input, then generates and executes command lines; meaning it can take output of a command and passes it as argument of another command. $ find Pictures/tecmint/ -name *.png -type f -print0 | xargs -0 tar -cvzf images.tar.gz  2. You can also convert muti-line output from ls command into single line using xargs as follows. Assuming you have a list of files, and you wish to know the number of lines/words/characters in each file in the list, you can use ls command and xargs for this purpose as follows. $ find .

<hr>

[Visit Link](https://www.tecmint.com/xargs-command-examples/){:target="_blank" rel="noopener"}


