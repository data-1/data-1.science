---
layout: post
title: "Clues on how giraffe neck evolved: Fossil evidence shows vertebrae elongated in 2 stages"
date: 2015-10-08
categories:
author: New York Institute of Technology
tags: [Giraffe,Taxa]
---


The second stage was the elongation of the back portion of the C3 neck vertebra. The modern giraffe is the only species that underwent both stages, which is why it has a remarkably long neck. Solounias and Melinda Danowitz, a medical student in the school's Academic Medicine Scholars program, studied 71 fossils of nine extinct and two living species in the giraffe family. We also found that the most primitive giraffe already started off with a slightly elongated neck, said Danowitz. Solounias and Danowitz found the cranial end of the vertebra stretched initially around 7 million years ago in the species known as Samotherium, an extinct relative of today's modern giraffe.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151007033229.htm){:target="_blank" rel="noopener"}


