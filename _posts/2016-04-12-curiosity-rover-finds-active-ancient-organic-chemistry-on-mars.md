---
layout: post
title: "Curiosity rover finds active, ancient organic chemistry on Mars"
date: 2016-04-12
categories:
author: Dwayne Brown
tags: [Curiosity (rover),Atmosphere of Mars,Mars,Gale (crater),Planetary science,Physical sciences,Outer space,Space science,Chemistry]
---


NASA's Curiosity Mars rover has detected fluctuations in methane concentration in the atmosphere, implying both types of activity occur on modern Mars. Curiosity also detected different Martian organic chemicals in powder drilled from a rock dubbed Cumberland, the first definitive detection of organics in surface materials of Mars. SAM analyzed hydrogen isotopes from water molecules that had been locked inside a rock sample for billions of years and were freed when SAM heated it, yielding information about the history of Martian water. It's really interesting that our measurements from Curiosity of gases extracted from ancient rocks can tell us about loss of water from Mars, said Paul Mahaffy, SAM principal investigator of NASA's Goddard Space Flight Center in Greenbelt, Maryland, and lead author of a report published online this week by the journal Science. In order to go back in time and see how the deuterium-to-hydrogen ratio in Martian water changed over time, researchers can look at the ratio in water in the current atmosphere and water trapped in rocks at different times in the planet's history.

<hr>

[Visit Link](http://phys.org/news337964159.html){:target="_blank" rel="noopener"}


