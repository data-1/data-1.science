---
layout: post
title: "Integral catches dead star exploding in a blaze of glory"
date: 2015-09-03
categories:
author: "$author"   
tags: [Supernova,SN 2014J,Star,White dwarf,Type Ia supernova,Radioactive decay,Science,Physics,Nature,Space science,Astronomy,Physical sciences]
---


The explosions in question are known as Type Ia supernovae, long suspected to be the result of a white dwarf star blowing up because of a disruptive interaction with a companion star. Diehl and his colleagues detected gamma rays from the decay of radioactive nickel just 15 days after the explosion. This was unexpected, because during the early phase of a Type Ia supernova, the explosion debris is thought to be so dense that the gamma rays from the nickel decay should be trapped inside. This outer layer detonated, forming the observed nickel and then triggering the internal explosion that became the supernova. “Regardless of the fine details of how these supernovae are triggered, Integral has proved beyond doubt that a white dwarf is involved in these stellar cataclysms,” says Erik Kuulkers, ESA’s Integral Project Scientist.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/INTEGRAL_catches_dead_star_exploding_in_a_blaze_of_glory){:target="_blank" rel="noopener"}


