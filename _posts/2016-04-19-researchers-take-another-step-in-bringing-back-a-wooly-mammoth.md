---
layout: post
title: "Researchers take another step in bringing back a wooly mammoth"
date: 2016-04-19
categories:
author: Bob Yirka
tags: [Woolly mammoth,Cloning,Mammoth,Extinction,Genetics]
---


Model at the Royal BC Museum. Credit: Wikipedia  (Phys.org)—A team of researchers working at Harvard University has taken yet another step towards bringing to life a reasonable facsimile of a woolly mammoth—a large, hairy elephant-like beast that went extinct approximately 3,300 years ago. The work by the team has not been published as yet, because as team lead George Church told The Sunday Times, recently, they believe they have more work to do before they write up their results. What they have done, however, is build healthy living elephant cells with mammoth DNA in them. As Church explains, the team prioritizes which genes are replicated and inserted, based on such factors as hairiness, ear size, and subcutaneous fat, which the animal needed to survive in its harsh cold environment.

<hr>

[Visit Link](http://phys.org/news346324889.html){:target="_blank" rel="noopener"}


