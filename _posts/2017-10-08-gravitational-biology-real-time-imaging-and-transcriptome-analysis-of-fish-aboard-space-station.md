---
layout: post
title: "Gravitational biology: Real time imaging and transcriptome analysis of fish aboard space station"
date: 2017-10-08
categories:
author: "Tokyo Institute of Technology"
tags: [Bone,Osteoblast,Micro-g environment,Osteoclast,Gravitational biology,International Space Station,Sp7 transcription factor,Transcriptomics technologies,Transcriptome,Green fluorescent protein,Cell biology,Molecular biology,Biochemistry,Biotechnology,Biology,Life sciences]
---


For example, researches clearly show that astronauts undergo a significant drop in bone mineral density during space missions, but the precise molecular mechanisms responsible for such changes in bone structure are unclear. They found increases in both osteoblast and osteoclast specific promoter-driven GFP and DsRed signals one day after launch, and continued for up to eight days. In their experiments, the team used four different double medaka transgenic lines focusing on up-regulation of fluorescent signals of osteoblasts and osteoclasts to clarify the effect of gravity on the interaction of osteoblast-osteoclast. These findings suggest that exposure to microgravity induced an immediate dynamic alteration of gene expressions in osteoblasts and osteoclasts. advertisement  Live-imaging of osteoblasts showed the intensity of osterix- and osteocalcin-DsRed in pharyngeal bones to increase one day after launch.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/01/170110101631.htm){:target="_blank" rel="noopener"}


