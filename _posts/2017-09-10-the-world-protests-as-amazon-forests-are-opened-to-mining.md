---
layout: post
title: "The world protests as Amazon forests are opened to mining"
date: 2017-09-10
categories:
author: "Beatriz Garcia, The Conversation"
tags: [Amazon rainforest,Deforestation,Climate change,Brazil,Natural environment,Nature]
---


The Amazon is the largest rainforest in the world. Political and economic turbulence  Brazil is currently in the middle of the largest corruption scandals in its history. According to the Intergovernmental Panel on Climate Change, land use, including deforestation and forest degradation, is the second-largest source of global emissions after the energy sector. But with 14 million Brazilians unemployed, further assistance is required to ensure that they can protect their forests. How will the international community honour their commitments to keep global warming below 2℃, if countries begin rolling back their environmental protections?

<hr>

[Visit Link](https://phys.org/news/2017-08-world-protests-amazon-forests.html){:target="_blank" rel="noopener"}


