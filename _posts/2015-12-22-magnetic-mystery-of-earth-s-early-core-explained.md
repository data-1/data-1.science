---
layout: post
title: "Magnetic mystery of Earth's early core explained"
date: 2015-12-22
categories:
author: Witze, Alexandra Witze, You Can Also Search For This Author In
tags: []
---


San Francisco, California  Studies suggest that Earth's magnetic field arose more than 4 billion years ago. Each relies on minerals crystallizing out of the molten Earth, a process that would have generated a magnetic field by churning the young planet's iron core. Silicon dioxide is the choice of Kei Hirose, a geophysicist at the Tokyo Institute of Technology who runs high-pressure experiments to simulate conditions deep within the Earth. In unpublished work, Stevenson proposes that magnesium oxide, settling out of the molten early Earth, could have set up the buoyancy differences that would drive an ancient geodynamo. He found that silicon and oxygen crystallized out together as silicon dioxide whenever both were present.

<hr>

[Visit Link](http://www.nature.com/news/magnetic-mystery-of-earth-s-early-core-explained-1.19058){:target="_blank" rel="noopener"}


