---
layout: post
title: "Neanderthal-Derived Genetic Variation Shapes Modern Human Cranium and Brain"
date: 2017-08-31
categories:
author: "Gregory, Michael D., Section On Integrative Neuroimaging, Clinical, Translational Neuroscience Branch, National Institute Of Mental Health Intramural Research Program, National Institutes Of Health, Bethesda, Kippenhan, J. Shane"
tags: []
---


In this work, we describe relationships between Neanderthal-derived genetic variation and co-localized cranial and brain morphology in modern humans. 1), as well as regional changes in brain morphology underlying these skull changes, specifically in the IPS and visual cortex. This finding provides crucial validation of the NeanderScore measurement and suggests that even in the context of the modern H. sapiens genome, Neanderthal genetic variation is associated with patterns of skull dimensions that mirror known Neanderthal phenotypes. Finally, our analyses of the relationship of our findings to specific Neanderthal-derived gene variants, revealed a single 53 kb LD block that was significantly associated with the shared variance of the identified Neanderthal-derived brain and skull changes and that encodes for the gene GPR26. By the same token, we suggest that Neanderthal gene flow into modern humans is not only of evolutionary interest, but may also be functional in the living H. sapiens brain, revealing novel genetic influences on neurodevelopment of the visuospatial system upon which a fuller account of molecular mechanisms of IPS-driven normative mental functions, such as visuospatial integration and tool manipulation, can be built.

<hr>

[Visit Link](http://www.nature.com/articles/s41598-017-06587-0?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


