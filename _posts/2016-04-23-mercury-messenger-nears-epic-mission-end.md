---
layout: post
title: "Mercury MESSENGER nears epic mission end"
date: 2016-04-23
categories:
author: University Of Michigan
tags: [MESSENGER,Mercury (planet),Solar wind,Sun,Atmosphere,Mariner 10,Astronomy,Outer space,Solar System,Physical sciences,Planetary science,Bodies of the Solar System,Astronomical objects,Spaceflight,Astronomical objects known since antiquity,Sky,Science,Planets,Planets of the Solar System,Space science]
---


As an engineer at U-M's Space Physics Research Lab, Raines helped design FIPS. The gases that make up Mercury's outer atmosphere are mostly charged because of how the sun influences the planet and its environment, including through its solar wind. FIPS helped scientists understand what makes up Mercury's atmosphere, and where that atmosphere comes from. The sun is pulling on it. We study Mercury because it's part of our history, Zurbuchen said.

<hr>

[Visit Link](http://phys.org/news348632096.html){:target="_blank" rel="noopener"}


