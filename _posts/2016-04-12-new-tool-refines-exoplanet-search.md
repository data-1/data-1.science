---
layout: post
title: "New tool refines exoplanet search"
date: 2016-04-12
categories:
author: Carnegie Institution for Science
tags: [Methods of detecting exoplanets,Carnegie Institution for Science,Exoplanet,NASA Exoplanet Science Institute,Planetary science,Outer space,Physical sciences,Astronomy,Space science,Science]
---


New work led by Carnegie's Jonathan Gagné, Caltech's Peter Gao, and Peter Plavchan from Missouri State University reports on a technological upgrade for one method of finding planets or confirming other planetary detections. One of the most-popular and successful techniques for finding and confirming planets is called the radial velocity method. A planet is obviously influenced by the gravity of the star it orbits; that's what keeps it in orbit. As a result, astronomers are able to detect the tiny wobbles the planet induces as its gravity tugs on the star. But looking in the near-infrared will allow us to reject false positives caused by sunspots and other phenomena that will not look the same in near-infrared as they do in visible light,  Radial velocity work in the near-infrared wavelengths has been conducted before, but it has trailed behind planet hunting in the visible spectrum, partially due to technical challenges.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/cifs-ntr041116.php){:target="_blank" rel="noopener"}


