---
layout: post
title: "Ant study sheds light on the evolution of workers and queens"
date: 2018-08-06
categories:
author: "Rockefeller University"
tags: [Ooceraea biroi,Ant,Eusociality,Reproduction,Insulin,Biology,Zoology,Animals,Behavioural sciences]
---


Credit: Daniel Kronauer  Next, the researchers studied the role of insulin in the clonal raider ant Ooceraea biroi. These results indicate that the presence of larvae suppresses the production of insulin; and without sufficient levels of this peptide, the ants cannot reproduce. Credit: © Daniel Kronauer  Yes, queen  This research offers clues about how ants evolved from solitary organisms to social species with specialized castes. Kronauer proposes that, first, insulin signaling became responsive to the presence of larvae, resulting in reproductive cycles reminiscent of those observed in O. biroi. Such an adaption makes good sense, as ants caring for offspring must prioritize food finding over egg laying—and both behaviors are known to be regulated by insulin.

<hr>

[Visit Link](https://phys.org/news/2018-07-ant-evolution-workers-queens.html){:target="_blank" rel="noopener"}


