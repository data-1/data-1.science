---
layout: post
title: "Colourful dunes on wind-swept Mars"
date: 2017-10-05
categories:
author: ""
tags: [Dune,Barchan,Geology,Planetary science,Astronomical objects known since antiquity,Terrestrial planets,Mars,Planets of the Solar System,Geomorphology,Physical geography,Earth sciences]
---


Science & Exploration Colourful dunes on wind-swept Mars 05/10/2017 9864 views 153 likes  Dunes are prominent indicators of prevailing winds, as can be seen on this crater floor on Mars, imaged by ESA’s Mars Express on 16 May. Dune field in a crater, plan view Depressions such as impact craters can act as traps for sediments that have been blown in from elsewhere, accumulating in various patterns whipped up by strong winds. A smoothly distributed sand sheet stretches between the dunes and the western wall of the crater. In this example, a southeasterly wind at the time of dune formation can be assumed. Dune-filled crater topography To the south of the dune field in the large crater, a single elongated transverse dune extends beyond the main field for several kilometres.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Mars_Express/Colourful_dunes_on_wind-swept_Mars){:target="_blank" rel="noopener"}


