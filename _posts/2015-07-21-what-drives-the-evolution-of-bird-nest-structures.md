---
layout: post
title: "What drives the evolution of bird nest structures?"
date: 2015-07-21
categories:
author: American Ornithological Society Publications Office 
tags: [Ornithology,Zoology,Animals]
---


Build a dome over them! There is tremendous diversity among the nests of birds, in nest location, structure, materials, and more, but we know very little about the forces that shaped the evolution of this incredible variety. In a new paper published this week in The Auk: Ornithological Advances, Zachary Hall, Sally Street, Sam Auty, and Susan Healy of the University of St. Andrews in Scotland test the hypothesis that domed-shaped nests arose as a result of some species transitioning to nesting on the ground, where the risk from predators is greater. Hall was completing his Ph.D. work on the neurobiology of nest-building behavior when he noticed that very little work had been done on trying to understand why different bird species build such drastically different nest structures. About the journal: The Auk: Ornithological Advances is a peer-reviewed, international journal of ornithology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/copo-wdt050615.php){:target="_blank" rel="noopener"}


