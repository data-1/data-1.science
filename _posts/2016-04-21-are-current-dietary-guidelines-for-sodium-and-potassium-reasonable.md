---
layout: post
title: "Are current dietary guidelines for sodium and potassium reasonable?"
date: 2016-04-21
categories:
author: University of Washington School of Medicine/UW Medicine
tags: [Potassium,Food,Diet (nutrition),Sodium,Dietary supplement,Health,Nutrition,Health sciences,Food and drink,Health care,Self-care,Determinants of health]
---


Yet to get to 3510 mg a day you would have to eat about six potatoes day. They found that at best, only 0.3% of Americans, or about three in a thousand achieve the WHO dietary goals. The data confirm that we eat too much sodium and not enough potassium, said Drewnowski. Milk has sodium in it, so if you want to reduce your sodium intake you can drink less milk. Reformulating foods to lower their sodium content would be one strategy to reduce sodium intake.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/uowh-acd040715.php){:target="_blank" rel="noopener"}


