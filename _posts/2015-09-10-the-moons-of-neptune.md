---
layout: post
title: "The moons of Neptune"
date: 2015-09-10
categories:
author: Matt Williams
tags: [Irregular moon,Neptune,Moons of Neptune,Triton (moon),Natural satellite,Bodies of the Solar System,Physical sciences,Planemos,Astronomy,Local Interstellar Cloud,Ice giants,Gas giants,Planetary science,Moons,Planets of the Solar System,Planets,Astronomical objects,Outer space,Solar System,Outer planets,Space science]
---


Neptune and its moons. Outer (Irregular) Moons:  Neptune's irregular moons consist of the planet's remaining satellites (including Triton). With the exception of Triton and Nereid, Neptune's irregular moons are similar to those of other giant planets and are believed to have been gravitationally captured by Neptune. Triton and Nereid:  Triton and Nereid are unusual irregular satellites and are thus treated separately from the other five irregular Neptunian moons. First of all, they are the largest two known irregular moons in the Solar System.

<hr>

[Visit Link](http://phys.org/news/2015-09-moons-neptune.html){:target="_blank" rel="noopener"}


