---
layout: post
title: "Laser Focus: Computer Vision and Machine Learning Are Speeding Up 3D Printing"
date: 2017-10-20
categories:
author:  
tags: [3D printing,General Electric,Technology,Printer (computing),Computer network,Computer vision,Engine,Innovation,Artificial intelligence,Analytics,Electrical grid,Computer,Branches of science]
---


If something goes wrong near the end of the process, precious machine time and money could go to waste.The GE researchers are building a system that could speed up the process and eventually achieve “100 percent yield,” an engineer’s Nirvana where machines only produce good parts, beginning with the very first build. In this way, we will have a better chance of getting to the 100 percent yield, faster.”3D printing and other additive manufacturing methods print parts directly from a computer file. They include variation in the size of the powder particles, as well as “the complex dynamic of adding new powder layers,” which can be as thin as a human hair. They are working with other members of the team to loop the defect-spotting ability into machine controls to make the 3D printer smarter. “It’s telling me that if I use these parameters to build a part from a certain material, I can expect this kind of behavior in the final product,” Vinciquerra says.The next generation of the twin will be able to gather computer vision data but also collect information from other sensors monitoring the printing process, such as the shape of the tiny pool of metal rendered molten by the laser.Vinciquerra notes that some of GE’s most advanced 3D-printed parts for GE Aviation can take more than a week to complete and require several more hours after that for processing and validation.

<hr>

[Visit Link](https://www.ge.com/reports/laser-focus-computer-vision-machine-learning-speeding-3d-printing/){:target="_blank" rel="noopener"}


