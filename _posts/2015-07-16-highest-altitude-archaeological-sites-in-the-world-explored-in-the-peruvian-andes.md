---
layout: post
title: "Highest altitude archaeological sites in the world explored in the Peruvian Andes"
date: 2015-07-16
categories:
author: University of Calgary 
tags: [Archaeology,American Association for the Advancement of Science,Hunting,Adaptation,Tool,Andes]
---


There is also a Pucuncho workshop site where stone tools were made at 4,355 metres above sea level. And yet, the findings indicate that people were living in these high altitude zones for extended periods of time. We don't know if people were living there year round, but we strongly suspect they were not just going there to hunt for a few days, then leaving, says Zarrillo. Archaeological evidence found at Cuncaicha includes signs of habitation such as human skull fragments, animal remains and stone tools. We don't know for certain, says Zarrillo.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/uoc-haa102214.php){:target="_blank" rel="noopener"}


