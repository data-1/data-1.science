---
layout: post
title: "Improving clinical trials with machine learning"
date: 2018-07-14
categories:
author: "University College London"
tags: [Machine learning,Stroke,Statistics,Brain,Brain damage,Health sciences,Medicine,Branches of science,Science,Cognitive science]
---


Machine learning could improve our ability to determine whether a new drug works in the brain, potentially enabling researchers to detect drug effects that would be missed entirely by conventional statistical tests, finds a new UCL study published in Brain. With conventional low-dimensional models, the intervention would need to shrink the lesion by 78.4% of its volume for the effect to be detected in a trial more often than not, while the high-dimensional model would more than likely detect an effect when the lesion was shrunk by only 55%. Conventional statistical models will miss an effect even if the drug typically reduces the size of the lesion by half, or more, simply because the complexity of the brain's functional anatomy--when left unaccounted for--introduces so much individual variability in measured clinical outcomes. The researchers say their findings demonstrate that machine learning could be invaluable to medical science, especially when the system under study--such as the brain--is highly complex. We can now capture the complex relationship between anatomy and outcome with high precision, said Dr Nachev.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/ucl-ict111417.php){:target="_blank" rel="noopener"}


