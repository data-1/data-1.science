---
layout: post
title: "Soybean science blooms with supercomputers"
date: 2017-03-22
categories:
author: "Jorge Salazar, University Of Texas At Austin"
tags: [Soybean,Plant breeding,Samuel Bowen,Genomics,Bioinformatics,Computing,Technology]
---


Scientists at the University of Missouri-Columbia developed SoyKB. Resequencing data analysis takes most of our computing time on XSEDE. SoyKB is a web resource for all soybean data, from molecular data to field data including several analytical tools . Credit: SoyKB  MU scientists Trupti Joshi and Dong Xu were both on the team that in 2010 sequenced the first reference soybean genome. This is a great training environment for students, Trupti Joshi of MU said.

<hr>

[Visit Link](http://phys.org/news/2016-08-soybean-science-blooms-supercomputers.html){:target="_blank" rel="noopener"}


