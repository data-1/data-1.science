---
layout: post
title: "Artificial agent designs quantum experiments"
date: 2018-07-03
categories:
author: "University Of Innsbruck"
tags: [Intelligent agent,Experiment,Artificial intelligence,Anton Zeilinger,Science,Technology,Branches of science,Cognitive science,Cognition,Computing]
---


Credit: Harald Ritsch  On the way to an intelligent laboratory, physicists from Innsbruck and Vienna present an artificial agent that autonomously designs quantum experiments. This shows how machines could play a more creative role in research in the future. They used a projective simulation model for artificial intelligence to enable a machine to learn and act creatively. Reinforcement learning is what distinguishes our model from the previously studied automated search, which is governed by unbiased random search, says Alexey Melnikov from the Department of Theoretical Physics at the University of Innsbruck. Some of these structures are already known to physicists as useful tools from modern quantum optical laboratories.

<hr>

[Visit Link](https://phys.org/news/2018-01-artificial-agent-quantum.html){:target="_blank" rel="noopener"}


