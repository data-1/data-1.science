---
layout: post
title: "Sticky Amber Preserved Dinosaur-Age Insects for Millions of Years"
date: 2016-01-29
categories:
author: Laura Geggel
tags: [Mantis,Amber]
---


The Cretaceous-age mantis <i>Burmantis zherikhini</i>, found in Myanmar, is about 97 million years old. The three specimens were found in modern-day Lebanon, Myanmar and Spain. (Image credit: Group AMBARES)  The mantis discovered in Myanmar (Burmantis zherikhini), one of the newly discovered species, is an adult. A close-up of the 128-million-year old mantis Burmantis libanica, the oldest known mantis species preserved in amber. Follow Live Science @livescience, Facebook & Google+.

<hr>

[Visit Link](http://www.livescience.com/53311-cretaceous-mantis-amber.html){:target="_blank" rel="noopener"}


