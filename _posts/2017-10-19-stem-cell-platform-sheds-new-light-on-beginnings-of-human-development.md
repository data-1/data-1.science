---
layout: post
title: "Stem cell platform sheds new light on beginnings of human development"
date: 2017-10-19
categories:
author: University Of Toronto
tags: [Embryo,Stem cell,Embryonic stem cell,Development of the human body,Regenerative medicine,Developmental biology,Research,Gastrulation,Fertilisation,Reproduction,Biology,Life sciences]
---


Since it's an artificial, engineered environment that can mimic some of the fundamental organizing principles that we think are important for human embryo development, our platform has the potential to alleviate some of the challenges associated with studying human embryos in the lab and provide very useful hints as to how the initial stages of human development start, said Mukul Tewary, a PhD candidate at U of T's Institute of Biomaterials & Biomedical Engineering (IBBME). While a few studies in the U.S. have previously demonstrated synthetic embryo patterning, Tewary's platform advances the technology in important ways that he thinks will enable researchers to start to test assumptions they have made about early human developmental biology based on animal studies and mathematical modelling. Up until recently, this wasn't something bioethicists had even considered, said Tewary. Over the past three years, several research teams have modelled early embryonic development in the lab by placing human embryonic stem cells on micro-patterned surfaces, which allow them to control where and how the cells stick. DOI: 10.1242/dev.149658  Insoo Hyun et al. Embryology policy: Revisit the 14-day rule, Nature (2016).

<hr>

[Visit Link](https://phys.org/news/2017-10-stem-cell-platform-human.html){:target="_blank" rel="noopener"}


