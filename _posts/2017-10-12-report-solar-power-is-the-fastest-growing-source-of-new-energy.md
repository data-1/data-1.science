---
layout: post
title: "Report: solar power is the fastest growing source of new energy"
date: 2017-10-12
categories:
author: "Amanda Froelich"
tags: []
---


In 2016, renewable energy accounted for two-thirds of all new power added to the world’s grid, according to a report by the International Energy Agency (IEA) — with more solar brought on board than any other technology. We expect that solar PV capacity growth will be higher than any other renewable technology up to 2022.”  The agency said it has “underestimated” the speed solar is growing. Based on figures from 2016, the IEA now expects one-third more solar in China and India by 2022. Offshore wind farms are expected to account for most of the UK’s renewable energy growth. Paolo Frankl, head of the renewable energy division at the IEA, said, “Renewables may well become even cheaper than fossil fuel alternatives [over the next five years].” Within five years, coal may still be the biggest source of power.

<hr>

[Visit Link](https://inhabitat.com/report-solar-power-is-the-fastest-growing-source-of-new-energy){:target="_blank" rel="noopener"}


