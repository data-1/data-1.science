---
layout: post
title: "New Horizons spacecraft begins intensive data downlink phase"
date: 2016-06-29
categories:
author: "Tricia Talbert"
tags: [New Horizons,NASA Deep Space Network,Space science,Outer space,Spaceflight,Astronomy,Solar System,Science,Spacecraft,Bodies of the Solar System,Astronautics,Space exploration]
---


This iconic image of the mountains, informally named Norgay Montes (Norgay Mountains) was captured about 1 ½ hours before New Horizons’ closest approach to Pluto, when the craft was 47,800 miles (77,000 kilometers) from the surface of the icy body. The image easily resolves structures smaller than a mile across. The process moves into high gear on Saturday, Sept. 5, with the entire downlink taking about one year to complete. Credit: NASA  During the data downlink phase, the spacecraft transmits science and operations data to NASA's Deep Space Network (DSN) of antenna stations, which also provide services to other missions, like Voyager. The Johns Hopkins University Applied Physics Laboratory in Laurel, Maryland, designed, built, and operates the New Horizons spacecraft and manages the mission for NASA's Science Mission Directorate.

<hr>

[Visit Link](http://phys.org/news/2015-09-horizons-spacecraft-intensive-downlink-phase.html){:target="_blank" rel="noopener"}


