---
layout: post
title: "Global freshwater consumption crossing its planetary boundary"
date: 2016-05-08
categories:
author: Stockholm University
tags: [Planetary boundaries,News aggregator,Evapotranspiration,Global environmental issues,Earth phenomena,Environment,Physical geography,Nature,Earth sciences,Natural environment]
---


These activities have led to an increased loss of freshwater to the atmosphere due to human-driven increase of evapotranspiration, which includes evaporation from surface and soil water and transpiration by plants. On average over Earth's land surface, the combined effect of the different human activities is a net consumption of freshwater, which is greater than the freshwater planetary boundary. Parallel new results, which were also published by Fernando Jaramillo and Georgia Destouni, in Geophysical Research Letters (December 2014), show that climate-driven and direct human-driven changes in evapotranspiration counteract each other globally and in most continents. This counteraction dampens the net total freshwater change, compared to only climate-driven change or only human-driven consumption. These results put into question an isolated use of only human water consumption as a freshwater planetary boundary, says Georgia Destouni, professor at the Department of Physical Geography, Stockholm University.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150616071910.htm){:target="_blank" rel="noopener"}


