---
layout: post
title: "How to weigh stars with gravitational lensing"
date: 2018-07-28
categories:
author: "Astronomy & Astrophysics"
tags: [Star,Astronomical objects,Astrophysics,Physical cosmology,Celestial mechanics,Theory of relativity,Science,Physics,Physical sciences,Space science,Astronomy]
---


This distortion of the light by the foreground star is called gravitational lensing: the light of the background star is deviated or focused into a smaller angle, and the star appears brighter. The main effect is the change in the star's apparent position on the sky because the deviation shifts the centre of light relative to other more distant stars. Both of these effects depend on only one thing, the mass of the lensing body, in this case that of the foreground star. Thus, gravitational lensing is a method for weighing stars. These data were used by Jonas Klüter, who is doing a PhD at Heidelberg University, to search for such close passages of stars.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/aa-htw072018.php){:target="_blank" rel="noopener"}


