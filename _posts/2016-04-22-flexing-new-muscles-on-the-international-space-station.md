---
layout: post
title: "Flexing new muscles on the International Space Station"
date: 2016-04-22
categories:
author:  
tags: [Center for the Advancement of Science in Space,International Space Station,Radiation,3D printing,Robot,Technology]
---


Lenore Rasmussen, Ph.D., principal investigator of the Synthetic Muscle investigation, checks the pressure during the oxygen plasma treatment of titanium metal support mounts at the US Department of Energy's Princeton Plasma Physics Laboratory. As astronauts on the International Space Station (ISS) study how the material reacts to the space environment, scientists on Earth are investigating the application of these same muscles in prosthetics, allowing fabrication of extremely realistic replacements for people who have lost limbs. Astronauts there will expose the material to cosmic and solar radiation to evaluate its durability in the harsh environment. The Earth benefits of synthetic muscle appealed to the Center for the Advancement of Science in Space (CASIS) in Melbourne, Florida, which manages commercial investigations on the ISS National Laboratory. The polymer material can be designed for more flexibility and degrees of motion, or it can be formulated to provide more strength and durability.

<hr>

[Visit Link](http://phys.org/news348163667.html){:target="_blank" rel="noopener"}


