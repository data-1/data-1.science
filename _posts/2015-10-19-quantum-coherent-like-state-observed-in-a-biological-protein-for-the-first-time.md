---
layout: post
title: "Quantum coherent-like state observed in a biological protein for the first time"
date: 2015-10-19
categories:
author: American Institute Of Physics
tags: [X-ray crystallography,Radiation,Protein,Photon,Terahertz radiation,Physics,Applied and interdisciplinary physics,Physical chemistry,Physical sciences,Science,Nature,Chemistry]
---


In 1968 physicist Herbert Fröhlich predicted that a similar process at a much higher temperature could concentrate all of the vibrational energy in a biological protein into its lowest-frequency vibrational mode. Observing Fröhlich condensation opens the door to a much wider-ranging study of what terahertz radiation does to proteins, said Gergely Katona, a senior scientist at the University of Gothenburg in Sweden. The Long Path from Theory to Observation  The theoretical underpinnings of Fröhlich condensation are relatively simple, Katona noted. In contrast, other models predict that the protein will quickly dissipate the energy from the photon in the form of heat. Katona and his colleagues aimed short bursts of 0.4 terahertz radiation at the lysozyme crystals while simultaneously gathering X-ray crystallography data.

<hr>

[Visit Link](http://phys.org/news/2015-10-quantum-coherent-like-state-biological-protein.html){:target="_blank" rel="noopener"}


