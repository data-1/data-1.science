---
layout: post
title: "Skeletons developed as chemistry of oceans changed, study shows"
date: 2018-08-15
categories:
author: "University of Edinburgh"
tags: [Exoskeleton,Fossil,Life,Cambrian explosion,Nature,Earth sciences]
---


Skeletons and shells first came into being 550 million years ago as the chemical make-up of seawater changed, a study suggests. However, researchers at the University of Edinburgh have found that the earliest lifeforms with hard body parts co-existed with closely related soft-bodied species. They concluded that hard-bodied lifeforms were first present only in such environments where high levels of calcium carbonate allowed organisms to develop primitive hard parts. Around 10m years later, the diversity of life of Earth increased rapidly -- a period known as the Cambrian explosion -- and hard-bodied life began to thrive. The study is published in the journal Proceedings of the Royal Society B.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/uoe-sda040417.php){:target="_blank" rel="noopener"}


