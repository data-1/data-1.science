---
layout: post
title: "Proton magnetic moment measurement is most precise yet – Physics World"
date: 2018-06-10
categories:
author: "Hamish Johnston"
tags: [Antiproton,Antimatter,Physics,Proton,Theoretical physics,Physical sciences,Particle physics,Science]
---


Double trouble: the traps used to measure the proton magnetic moment  The magnetic moment of the proton has been measured to a precision of 0.3 parts per billion by physicists in Germany. Equal and opposite  Schneider and several of his colleagues are also part of a team working on the BASE antiproton experiment at CERN. The team now aims to improve the double-trap technique so that even more precise measurements can be made. They also plan to implement the double-trap technique at CERN, so it can be used on antiprotons. If physicists discover that magnetic moments of the proton and antiproton are indeed different in magnitude, it could point to physics beyond the Standard Model and explain, for example, why there is much more matter than antimatter in the universe.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/nov/24/proton-magnetic-moment-measurement-is-most-precise-yet){:target="_blank" rel="noopener"}


