---
layout: post
title: "Plastic to outweigh fish in oceans by 2050, study warns"
date: 2016-01-29
categories:
author: David Williams
tags: [Plastic pollution,Plastic,Recycling,Economy]
---


Volunteers clean up plastics from a beach in Hong Kong on June 7, 2009  Plastic rubbish will outweigh fish in the oceans by 2050 unless the world takes drastic action to recycle the material, a report warned Tuesday on the opening day of the annual gathering of the rich and powerful in the snow-clad Swiss ski resort of Davos. The study, which drew on multiple sources, proposed setting up a new system to slash the leaking of plastics into nature, especially the oceans, and to find alternatives to crude oil and natural gas as the raw material of plastic production. At least eight million tonnes of plastics find their way into the ocean every year—equal to one garbage truckful every minute, said the report by the Ellen MacArthur Foundation, which included analysis by the McKinsey Centre for Business and Environment. This report demonstrates the importance of triggering a revolution in the plastics industrial ecosystem and is a first step to showing how to transform the way plastics move through our economy, said Dominic Waughray of the World Economic Forum, the hosts of the annual talks in Davos who jointly released the report. Re-usable plastics could become a valuable commodity in a circular economy that relied on recycling, Stuchtey said.

<hr>

[Visit Link](http://phys.org/news/2016-01-plastic-outweigh-fish-oceans.html){:target="_blank" rel="noopener"}


