---
layout: post
title: "Solving a long-standing atomic mass difference puzzle paves way to the neutrino mass"
date: 2015-12-21
categories:
author: Johannes Gutenberg Universitaet Mainz
tags: [Neutrino,Radioactive decay,Electron,Nuclear physics,Mass,Electron capture,Standard Model,Physics,Massenergy equivalence,Electronvolt,Atomic physics,Particle physics,Science,Chemistry,Physical sciences]
---


To measure the mass of neutrinos, scientists study radioactive decays in which they are emitted. This decay energy must be known with highest precision. Neutrinos are everywhere. Within the ECHo Collaboration, a research group headed by Professor Christoph E. Düllmann of the Institute of Nuclear Chemistry together with colleagues at the TRIGA research reactor at Mainz University have been responsible for the production and preparation of the needed supply of 163Ho. Our successful production of 163Ho samples for these studies is an important step towards the preparation of samples suitable for a sensitive measurement of the neutrino mass, said Düllmann.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/jgum-sal081115.php){:target="_blank" rel="noopener"}


