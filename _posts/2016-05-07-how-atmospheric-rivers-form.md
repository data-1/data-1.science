---
layout: post
title: "How atmospheric rivers form"
date: 2016-05-07
categories:
author: American Institute Of Physics
tags: [Atmospheric river,Lagrangian coherent structure,River,Nature,Earth sciences,Physical geography,Applied and interdisciplinary physics]
---


Based on simulations using real weather data in the Atlantic Ocean, the work was focused specifically on the transport of water from the tropics of the Caribbean to the Iberian Peninsula in Spain, but it suggests a more general way to study the transport of tropical water vapor globally. Given that atmospheric rivers over the Atlantic and Pacific oceans appear as coherent filaments of water vapor lasting for up to a week, and that Lagrangian coherent structures have turned out to explain the formation of other geophysical flows, we wondered whether Lagrangian coherent structures might somehow play a role in the formation of atmospheric rivers, said study coauthor Vicente Perez-Munuzuri, a physicist at the University of Santiago de Compostela in Spain. Through computer simulations, Perez-Munuzuri and his colleagues have shown that Lagrangian coherent structures and atmospheric rivers could indeed be linked. The Lagrangian coherent structures serve as a kind of temporary scaffolding around which an atmospheric river can grow and lengthen, Perez-Munuzuri said. Here we show that the rivers can be classified by whether or not they start as Lagrangian coherent structures and also what shapes those structures take.

<hr>

[Visit Link](http://phys.org/news353059942.html){:target="_blank" rel="noopener"}


