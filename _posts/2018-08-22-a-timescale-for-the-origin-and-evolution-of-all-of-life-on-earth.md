---
layout: post
title: "A timescale for the origin and evolution of all of life on Earth"
date: 2018-08-22
categories:
author: "University of Bristol"
tags: [History of life,Life,Fossil,Last universal common ancestor,News aggregator,Science,Biology,Evolutionary biology,Biological evolution,Nature]
---


Palaeontologists have long sought to understand ancient life and the shared evolutionary history of life as a whole. Fossil evidence for the early history of life is so fragmented and difficult to evaluate that new discoveries and reinterpretations of known fossils have led to a proliferation of conflicting ideas about the timescale of the early history of life. Co-author Dr Tom Williams, from Bristol's School of Biological Sciences, said: Combining fossil and genomic information, we can use an approach called the 'molecular clock' which is loosely based on the idea that the number of differences in the genomes of two living species (say a human and a bacterium) are proportional to the time since they shared a common ancestor. Co-author Professor Davide Pisani said: Using this approach we were able to show that the Last Universal Common Ancestor all cellular life forms, 'LUCA', existed very early in Earth's history, almost 4.5 Billion years ago -- not long after Earth was impacted by the planet Theia, the event which sterilised Earth and led to the formation of the Moon. Professor Pisani added: It is rather humbling to think we belong to a lineage that is billions of years younger than life itself.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180820113113.htm){:target="_blank" rel="noopener"}


