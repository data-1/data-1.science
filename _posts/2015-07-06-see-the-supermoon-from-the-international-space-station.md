---
layout: post
title: "See the Supermoon from the International Space Station"
date: 2015-07-06
categories:
author: ""   
tags: [Alexander Gerst,NASA programs,Human spaceflight programs,Spacecraft,NASA,Space exploration,Flight,Space program of the United States,Life in space,Space programs,Human spaceflight,Astronautics,Outer space,Spaceflight,Space science]
---


Astronaut Alexander Gerst tweeted this amazing picture of the above-earth view of a supermoon early Monday, when the moon was still behind the horizon. Gerst is a German astronaut aboard the International Space Station and among many astronauts using the social media site to share incredible pictures of the view from space. Gerst often uses the hashtag #bluedot, a nod to the iconic image of Earth taken from Saturn. And earlier in July, NASA astronaut Reid Wiseman tweeted above-earth images of Super Typhoon Neoguri. Three astronauts recently spoke to TIME about life above the earth, read their comments in this week’s issue.

<hr>

[Visit Link](http://time.com/2981417/supermoon-photo-international-space-station/){:target="_blank" rel="noopener"}


