---
layout: post
title: "Researchers use stars to infer mass of Milky Way"
date: 2016-05-07
categories:
author: Columbia University
tags: [Milky Way,Star,Star cluster,Globular cluster,Stellar kinematics,Galaxy,Space science,Science,Astronomical objects,Physical sciences,Astronomy]
---


With any ordinary scale every patient can do better at home. The Milky Way consists of roughly 100 billion stars that form a huge stellar disk with a diameter of 100-200 thousand light years. An international team of scientists led by Columbia University researcher Andreas Küpper used stars outside this disk, which orbit around the Milky Way in a stream-like structure, to weigh the Milky Way to high precision. The unique pattern of the density wiggles helped significantly to rule out models of the Milky Way, which were either too heavy or too skinny. Our new measurement breaks these ambiguities by exploiting the unique density pattern that Palomar 5 created as it orbited around the Milky Way for the past 11 billion years.

<hr>

[Visit Link](http://phys.org/news352476045.html){:target="_blank" rel="noopener"}


