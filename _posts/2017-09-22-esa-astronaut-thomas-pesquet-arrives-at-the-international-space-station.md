---
layout: post
title: "ESA astronaut Thomas Pesquet arrives at the International Space Station"
date: 2017-09-22
categories:
author: ""
tags: [Thomas Pesquet,European Space Agency,Spaceflight technology,Human spaceflight,Space programs,Outer space,Life in space,Flight,Spacecraft,Space exploration,Crewed spacecraft,Human spaceflight programs,Space vehicles,Aerospace,Space industry,Space research,Scientific exploration,Space-based economy,Astronautics,Space program of Russia,Space science,Astronauts,Space program of the United States,Spaceflight]
---


ESA astronaut Thomas Pesquet, NASA astronaut Peggy Whitson and Roscosmos commander Oleg Novitsky docked with the International Space Station today after a two-day flight in their Soyuz MS-03 spacecraft. The trio was launched from the Baikonur cosmodrome in Kazakhstan 17 November at 20:20 GMT and enjoyed a routine flight to catch up with the Space Station 400 km up. Despite the modernisation, for the crew it was like spending two days in a small car. Throughout the journey the astronauts kept in radio contact with Moscow ground control. The six will maintain the Station and work on scientific experiments that cannot be done anywhere else, exploiting the weightlessness that is unique to the space laboratory.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Proxima/ESA_astronaut_Thomas_Pesquet_arrives_at_the_International_Space_Station){:target="_blank" rel="noopener"}


