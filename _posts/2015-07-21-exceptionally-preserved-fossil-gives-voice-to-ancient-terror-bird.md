---
layout: post
title: "Exceptionally preserved fossil gives voice to ancient terror bird"
date: 2015-07-21
categories:
author: Society of Vertebrate Paleontology 
tags: [Phorusrhacidae,Bird,Paleontology,Predation,Taxa,Animals]
---


The new species, described in the latest issue of the Journal of Vertebrate Paleontology, is the most complete terror bird ever discovered, with more than 90% of the skeleton exquisitely preserved. These birds were the predominant predators during the Cenozoic Age in South America and certainly one of the most striking groups that lived during that time. The discovery of this species reveals that terror birds were more diverse in the Pliocene than previously thought. ABOUT THE JOURNAL OF VERTEBRATE PALEONTOLOGY  The Journal of Vertebrate Paleontology (JVP) is the leading journal of professional vertebrate paleontology and the flagship publication of the Society. Citation: Federico J. Degrange, Claudia P. Tambussi, Matías L. Taglioretti, Alejandro Dondas & Fernando Scaglia (2015): A new Mesembriornithinae (Aves, Phorusrhacidae) provides new insights into the phylogeny and sensory capabilities of terror birds, Journal of Vertebrate Paleontology, e912656.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/sovp-epf040215.php){:target="_blank" rel="noopener"}


