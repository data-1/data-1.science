---
layout: post
title: "Human Aspects of Machine Learning"
date: 2018-07-18
categories:
author: "May."
tags: []
---


Businesses are optimizing the offers based on each individual's propensity for next actions. For example, when using a ride-share application, I might get one user interface that provides various the ride options, while my friend may simultaneously get premium options at different price points. Real-time: Continuously optimized. On the other hand, Company 2 is more empowered to influence individualized actions through a set of application user interfaces based on the dynamic locational data, real-time context, weather data, social and news feeds, and other insights. Imagine a scenario where business analysts are working with data scientists and developers on the correlations between weathers and air quality.

<hr>

[Visit Link](https://dzone.com/articles/human-aspects-of-machine-learning?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


