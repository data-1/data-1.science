---
layout: post
title: "Probability that the quantum world obeys local realism is less than one in a billion, experiment shows"
date: 2017-09-20
categories:
author: "Lisa Zyga"
tags: [Bell test,Quantum entanglement,Bells theorem,Photon,Science,Applied and interdisciplinary physics,Scientific method,Scientific theories,Theoretical physics,Physical sciences,Quantum mechanics,Physics]
---


Three Bell tests  The test reported here is the latest loophole-free Bell test: one that simultaneously closes the two biggest loopholes, the locality loophole and the detection loophole. It is good that similar experiments were performed with different systems (photons, NV centers) essentially at the same time, so all results together can be taken as truly conclusive. To detect this entanglement, the researchers performed measurements on the photons, repeating the measurements over and over for tens of thousands of photon pairs. To attempt to close this loophole, the researchers used a high-speed quantum random number generator that chooses measurement settings that are truly random—almost. The problem is that there is a very slight possibility that the random number generators could have communicated with each other or the rest of the experiment before the experiment began.

<hr>

[Visit Link](https://phys.org/news/2017-07-probability-quantum-world-local-realism.html){:target="_blank" rel="noopener"}


