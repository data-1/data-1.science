---
layout: post
title: "Moon's tidal forces affect amount of rainfall on Earth"
date: 2016-02-02
categories:
author: University of Washington
tags: [Rain,Humidity,Moon,Atmosphere of Earth,Precipitation,Atmospheric pressure,Atmosphere,Earth sciences,Planets of the Solar System,Physical sciences,Meteorology,Planetary science,Applied and interdisciplinary physics,Nature,Physical geography,Space science,Sky,Astronomy,Planets,Atmospheric sciences]
---


New University of Washington research to be published in Geophysical Research Letters shows that the lunar forces affect the amount of rain - though very slightly. Kohyama was studying atmospheric waves when he noticed a slight oscillation in the air pressure. When the moon is overhead or underfoot, the air pressure is higher, Kohyama said. Higher pressure increases the temperature of air parcels below. For more information, contact Kohyama at kohyama@uw.edu.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/uow-pot012916.php){:target="_blank" rel="noopener"}


