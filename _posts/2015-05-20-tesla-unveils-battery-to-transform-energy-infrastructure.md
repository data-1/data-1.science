---
layout: post
title: "Tesla unveils battery to 'transform energy infrastructure'"
date: 2015-05-20
categories:
author: ""    
tags: []
---


Tesla unveils battery to 'transform energy infrastructure'    by Staff Writers    Los Angeles (AFP) May 1, 2015    Electric car pioneer Tesla unveiled a home battery Thursday which its founder Elon Musk said would help change the entire energy infrastructure of the world. The Tesla Powerwall can store power from solar panels, from the electricity grid at night when it is typically cheaper, and provide a secure backup in the case of a power outage. In theory the device, which typically would fit on the wall of a garage or inside a house, could make solar-powered homes completely independent of the traditional energy grid. Examples of the sleak device -- available in a range of colors -- were lined up along one side of the hall. This allows you to be completely off grid.

<hr>

[Visit Link](http://feedproxy.google.com/~r/DiscoveryNews-Top-Stories/~3/LkVjwTZtbWs/what-the-first-snake-looked-like-150519.htm){:target="_blank" rel="noopener"}


