---
layout: post
title: "As fins evolve to help fish swim, so does the nervous system"
date: 2017-09-11
categories:
author: "University Of Chicago Medical Center"
tags: [Fish fin,Wrasse,Evolution,Nervous system,Fish,Convergent evolution,Sensory nervous system,Species,Privacy]
---


Credit: Brett Aiello, University of Chicago  The sensory system in fish fins evolves in parallel to fin shape and mechanics, and is specifically tuned to work with the fish's swimming behavior, according to new research from the University of Chicago. As pectoral fins evolve different shapes, behaviors, and mechanical properties, we've shown that the sensory system is also evolving with them, said Brett Aiello, a PhD student in the Department of Organismal Biology and Anatomy, and the lead author of the study. Aiello and his colleagues collected fin aspect ratio measurements from hundreds of Labrid species at the Field Museum, and combined that data with a genetic phylogeny of 340 Labrids developed by Mark Westneat, PhD, professor of Organismal Biology and Anatomy and co-author on the study. With this history of fin evolution in place, the researchers also tested the mechanical properties and sensory system sensitivity in the pectoral fins of four pairs of closely related Labrid species, one with low AR fins and one with independently evolved high AR fins. A lot of the problems that engineers run into are similar to the type of things that animals have already evolved solutions to over time, Aiello said.

<hr>

[Visit Link](https://phys.org/news/2017-04-fins-evolve-fish-nervous.html){:target="_blank" rel="noopener"}


