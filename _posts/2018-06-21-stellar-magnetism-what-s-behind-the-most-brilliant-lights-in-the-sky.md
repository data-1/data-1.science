---
layout: post
title: "Stellar magnetism: What's behind the most brilliant lights in the sky?"
date: 2018-06-21
categories:
author: "University of Wisconsin-Madison"
tags: [Magnetic reconnection,Sun,Aurora,Solar wind,Star,Solar flare,Magnetosphere,Galaxy,Space science,Astronomy,Physical sciences,Physics,Nature,Astrophysics,Physical phenomena,Astronomical objects,Electromagnetism,Stellar astronomy,Outer space]
---


The data on so-called magnetic reconnection came from a quartet of new spacecraft that measure radiation and magnetic fields in high Earth orbit. When the law is violated, we can get an explosion. In the 1970s, telescopes orbiting above earth's sheltering magnetic field and atmosphere began returning data on X-rays and other non-visible types of radiation. Supernovas, which release energy visible across the galaxies when they explode. Almost everything we know about the universe comes from the light that reaches us, says Cary Forest, also a professor of physics at UW-Madison.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/uow-smw013018.php){:target="_blank" rel="noopener"}


