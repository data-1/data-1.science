---
layout: post
title: "Single photon reveals quantum entanglement of 16 million atoms: Researchers have demonstrated the entanglement between 16 million atoms in a crystal crossed by a single photon, confirming the theory b"
date: 2017-10-13
categories:
author: "Université de Genève"
tags: [Quantum entanglement,Quantum mechanics,Photon,Physics,Atom,News aggregator,Branches of science,Scientific theories,Applied and interdisciplinary physics,Physical sciences,Theoretical physics,Science]
---


Scientists at the University of Geneva (UNIGE), Switzerland, recently reengineered their data processing, demonstrating that 16 million atoms were entangled in a one-centimetre crystal. Imagine a physicist who always wears two socks of different colours. There is a correlation, in other words, between the two socks. Each scientist has a quantum particle, a photon, for example. They will find that the polarisation of the photons is always opposite (as with the socks in the above example), and that the photon has no intrinsic polarisation.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171013091023.htm){:target="_blank" rel="noopener"}


