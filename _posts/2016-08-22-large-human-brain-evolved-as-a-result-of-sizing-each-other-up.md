---
layout: post
title: "Large human brain evolved as a result of 'sizing each other up'"
date: 2016-08-22
categories:
author: "Cardiff University"
tags: [Evolution,Brain,American Association for the Advancement of Science,Human,Dunbars number,Research,Science,Psychology,Branches of science,Neuroscience,Cognitive science,Behavioural sciences,Cognition,Interdisciplinary subfields]
---


Lead author of the study Professor Roger Whitaker, from Cardiff University's School of Computer Science and Informatics, said: Our results suggest that the evolution of cooperation, which is key to a prosperous society, is intrinsically linked to the idea of social comparison - constantly sizing each up and making decisions as to whether we want to help them or not. In each round of the donation game, two simulated players were randomly selected from the population. Humans also have the largest cerebral cortex of all mammals, relative to the size of their brains. The research team propose that making relative judgements through helping others has been influential for human survival, and that the complexity of constantly assessing individuals has been a sufficiently difficult task to promote the expansion of the brain over many generations of human reproduction. Founded by Royal Charter in 1883, today the University combines impressive modern facilities and a dynamic approach to teaching and research.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/cu-lhb081216.php){:target="_blank" rel="noopener"}


