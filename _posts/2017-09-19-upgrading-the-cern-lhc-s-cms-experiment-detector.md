---
layout: post
title: "Upgrading the CERN LHC's CMS experiment detector"
date: 2017-09-19
categories:
author: "Fermi National Accelerator Laboratory"
tags: [Compact Muon Solenoid,Large Hadron Collider,CERN,Fermilab,Science,Physics,Particle physics,Technology]
---


The CMS experiment is one such detector. But to do this, the CMS detector needed an upgrade. The flexible copper cables emanating from the pixel modules bring the data collected by the silicon sensors to the readout electronics (which are hidden behind the yellow covers.) Over time, scientists have increased the rate of particle collisions at the LHC. The CMS detector is currently open so that scientists can install the pixel detector into the very center of the experiment (around the beam pipe).

<hr>

[Visit Link](https://phys.org/news/2017-03-cern-lhc-cms-detector.html){:target="_blank" rel="noopener"}


