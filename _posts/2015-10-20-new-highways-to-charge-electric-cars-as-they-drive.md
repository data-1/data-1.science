---
layout: post
title: "New highways to charge electric cars as they drive"
date: 2015-10-20
categories:
author: Tom Bailey, Monday, October
tags: [Electric vehicle,Car,Tram,Charging station,Road,Electric battery,Bus,Electric car,Vehicle technology,Electrification,Energy,Electric vehicles,Electrical engineering,Service industries,Electric power,Transport,Technology,Vehicles,Transportation engineering,Land transport,Electricity,Sustainable transport,Manufactured goods]
---


It was announced in August that the UK government would go ahead with an 18-month trial of highways that will be able to charge electrically powered cars as they drive along. Current of history  The basic idea behind the electric highway is to lay electric cables under the road. “Some consumers don’t want to buy an electric car without a full infrastructure for charging in place.”  In the UK, a trip to the petrol station is routine, but outside London, the infrastructure needed to support electric cars is all but non-existent. As The Telegraph said: “[T]he London Borough of Camden admits that it’s struggling to keep more than 70 percent of its charging points operational at any one time, leaving significant holes in the network.”  One Financial Times journalist recounted: “On a recent weekday morning, I walked to the nearest charging point to my home in Camberwell, southeast London, but it was out of service. No one wants to get caught short, half a mile from the nearest charging point.

<hr>

[Visit Link](http://www.theneweconomy.com/energy/new-highways-to-charge-electric-cars-as-they-drive){:target="_blank" rel="noopener"}


