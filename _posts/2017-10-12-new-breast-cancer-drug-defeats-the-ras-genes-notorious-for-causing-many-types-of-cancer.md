---
layout: post
title: "New breast cancer drug defeats the Ras genes notorious for causing many types of cancer"
date: 2017-10-12
categories:
author: "Virginia Commonwealth University"
tags: [Afatinib,Epidermal growth factor receptor,HER2neu,Ras GTPase,Neratinib,Cancer,KRAS,Non-small-cell lung carcinoma,Autophagy,Neoplasms,Clinical medicine,Diseases and disorders,Causes of death,Cell biology,Biochemistry,Biotechnology,Health,Medical specialties,Medicine]
---


This attachment permanently blocks the function of the receptors, causing the cell to target them for degradation. We thought, 'if neratinib can take down c-MET even though it doesn't attach to it, maybe it can take down other membrane proteins.' In addition to the afatinib-resistant NSCLC cells, the researchers tested the neratinib and valproic acid combination on cell lines derived from human pancreatic and ovarian cancers containing K-Ras mutations and N-Ras mutations, respectively. As he works to better understand the biology of neratinib-induced Ras inhibition, Dent hopes his laboratory findings can be validated in clinical trials testing a combination of neratinib and valproic acid in patients with afatinib-resistant NSCLC. In addition, the study uncovered other exciting clinical possibilities for neratinib.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171010152857.htm){:target="_blank" rel="noopener"}


