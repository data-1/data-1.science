---
layout: post
title: "Hubble, the telescope that revolutionized our view of space"
date: 2016-04-23
categories:
author: Jean-Louis Santini
tags: [Hubble Space Telescope,Outer space,NASA,Physical sciences,Science,Space science,Astronomical objects,Sky,Astronomy]
---


Launched on April 24, 1990 aboard the Space Shuttle Discovery, Hubble orbits the Earth. The telescope launched aboard the Space Shuttle Discovery in 1990 and is named after US astronomer Edwin Hubble (1889-1953). It was repaired in 1993 by astronauts who traveled to the telescope aboard the space shuttle. A model of the James Webb Space Telescope, Hubble's successor, pictured at NASA Goddard Space Flight Center in Greenbelt, Maryland on April 2, 2015  However, it has peered into the very distant past, to locations more than 13.4 billion light years from Earth. Hubble's cost at launch time was $1.5 billion.

<hr>

[Visit Link](http://phys.org/news348802392.html){:target="_blank" rel="noopener"}


