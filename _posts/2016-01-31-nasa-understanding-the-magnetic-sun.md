---
layout: post
title: "NASA: Understanding the magnetic sun"
date: 2016-01-31
categories:
author: NASA/Goddard Space Flight Center
tags: [Sun,Stellar corona,Electromagnetism,Astrophysics,Science,Electrical engineering,Bodies of the Solar System,Solar System,Physical phenomena,Outer space,Physics,Nature,Space science,Astronomy,Physical sciences]
---


Getting a handle on what drives that magnetic system is crucial for understanding the nature of space throughout the solar system: The sun's magnetic field is responsible for everything from the solar explosions that cause space weather on Earth - such as auroras - to the interplanetary magnetic field and radiation through which our spacecraft journeying around the solar system must travel. First, we observe the material on the sun. We can observe the shape of the magnetic fields above the sun's surface because they guide the motion of that plasma - the loops and towers of material in the corona glow brightly in EUV images. Next, scientists turn to models. They combine their observations - measurements of the magnetic field strength and direction on the solar surface - with an understanding of how solar material moves and magnetism to fill in the gaps.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/nsfc-nut012916.php){:target="_blank" rel="noopener"}


