---
layout: post
title: "Time flies: Insect fossils in amber shed light on India's geological history"
date: 2017-10-06
categories:
author: ""
tags: [India,Fly,Amber,Gondwana,Fossil,Species]
---


Palaeognoriste orientale, a new species of Lygistorrhinidae in Indian amber, which has its closest relatives in European Baltic amber. Credit: Frauke Stebner  A new species of fungus gnat in Indian amber closely resembles its fossil relatives from Europe, disproving the concept of a strongly isolated Indian subcontinent. Researchers have identified three new species of insects encased in Cambay amber dating from over 54 million years ago. Given the rareness of this group Indian amber has revealed a surprising diversity with three species in three different fossil and modern genera. Cambay amber from India has only been studied for a few years, but is already providing an important role in uncovering secrets regarding the origins of India´s fauna.

<hr>

[Visit Link](https://phys.org/news/2017-05-flies-insect-fossils-amber-india.html){:target="_blank" rel="noopener"}


