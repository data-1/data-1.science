---
layout: post
title: "NASA spacecraft discovers new magnetic process in turbulent space"
date: 2018-07-01
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Magnetic reconnection,Goddard Space Flight Center,Magnetosphere,Spaceflight,NASA,Space weather,Spacecraft,Nature,Space science,Astronomy,Outer space,Solar System,Astronautics,Science,Electromagnetism,Physical sciences,Flight]
---


Magnetic reconnection is one of the most important processes in the space -- filled with charged particles known as plasma -- around Earth. The new discovery found reconnection where it has never been seen before -- in turbulent plasma. Previously, scientists didn't know if reconnection even could occur there, as the plasma is highly chaotic in that region. MMS uses four identical spacecraft flying in a pyramid formation to study magnetic reconnection around Earth in three dimensions. Crucially, MMS scientists were able to leverage the design of one instrument, the Fast Plasma Investigation, to create a technique to interpolate the data -- essentially allowing them to read between the lines and gather extra data points -- in order to resolve the jets.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/nsfc-nsd050918.php){:target="_blank" rel="noopener"}


