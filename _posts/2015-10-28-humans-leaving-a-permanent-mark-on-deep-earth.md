---
layout: post
title: "Humans leaving a permanent mark on deep Earth"
date: 2015-10-28
categories:
author: Alex Peel, Planetearth Online
tags: [Anthropocene,Geology,Earth,Rock (geology),Mining,Earth sciences]
---


Human forays deep underground, such as boreholes, mines and nuclear bomb tests, are leaving a mark on the planet's geology that will last for hundreds of millions of years, say scientists. In a new report, published in the journal Anthropocene, they say we are altering Earth's rocks in a way that's unique in the planet's 4.6 billion-year history. The phenomenon adds weight to the 'Anthropocene' concept – the idea that we have changed the planet so dramatically that it has now entered a new, distinctive phase in its history. The world's deepest borehole, the Kola Superdeep borehole in Russia, extends 12 kilometres into the Earth's crust. 'If the geologists of future civilisations are very lucky, they will find a distinct surface layer with lots of cross-cutting features extending down for a number of kilometres,' says Zalasiewicz.

<hr>

[Visit Link](http://phys.org/news326448302.html){:target="_blank" rel="noopener"}


