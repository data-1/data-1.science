---
layout: post
title: "Researcher explores how the universe creates reason, morality"
date: 2016-04-12
categories:
author: Kelly Smith, Clemson University
tags: [Life,Rationality,Theory,Complexity,Universe,Evolution,Privacy,Complex system,Morality,Branches of science,Science,Philosophy,Concepts in metaphysics]
---


Life then seems to exhibit its own pattern of increasing complexity, with simple organisms getting more complex over evolutionary time until they eventually develop rationality and complex culture. And recent theoretical developments in Biology and complex systems theory suggest this trend may be real, arising from the basic structure of the universe in a predictable fashion. For example, does believing the universe is structured to produce complexity in general, and rational creatures in particular, constitute a religious belief? If evolution tends to favor the development of sociality, reason, and culture as a kind of package deal, then it's a good bet that any smart extraterrestrials we encounter will have similar evolved attitudes about their basic moral commitments. And such universal agreement, argues Smith, could be the foundation for a truly universal system of ethics.

<hr>

[Visit Link](http://phys.org/news341229624.html){:target="_blank" rel="noopener"}


