---
layout: post
title: "Neuron responsible for alcoholism found: Researchers find neuron responsible for alcohol consumption, could stop cycle of alcoholism"
date: 2016-06-29
categories:
author: "Texas A&M University"
tags: [Addiction,Brain,Alcohol (drug),Dopamine,Neuron,Memory,Alcoholism,Neurotransmitter,Neuroscience,Health]
---


Using an animal model, the researchers determined that alcohol actually changes the physical structure of medium spiny neurons, the main type of cell in the striatum. If these neurons are excited, you will want to drink alcohol, Wang said. This then creates a cycle, where drinking causes easier activation, and activation causes more drinking. Because there was no difference in the number of each type of spine in the D2 (no-go) neurons of alcohol-consuming and control models, the researchers realized there was a specific relationship between D1 neurons and alcohol consumption. My ultimate goal is to understand how the addicted brain works, Wang said, and once we do, one day, we'll be able to suppress the craving for another round of drinks and ultimately, stop the cycle of alcoholism.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150902155642.htm){:target="_blank" rel="noopener"}


