---
layout: post
title: "What can brain-controlled prosthetics tell us about the brain?"
date: 2015-12-09
categories:
author: Drexel University
tags: [Neuroprosthetics,Brain,Neuroscience,Braincomputer interface,Cognitive science,Branches of science,Science]
---


More than 15 years after that neuroscience benchmark, Moxon's lab is showing that it's now possible to glean new insight about how the brain stores and accesses information, and into the causes of pathologies like epilepsy and Parkinson's disease. This allows researchers to pinpoint a causal relationship between neural activity and the subject's behavior rather than one that is indirectly correlative. Subjects can be viewed as indirect observers of their own neurophysiological activity during neuroprosthetic experiments, Moxon said. As they see the movement of the prosthetic their brain adjusts in real time to continue planning the movement, but doing it without the normal feedback from the moving body part--as the prosthetic technology is standing in for that part of the body. While the past 15 years have witnessed tremendous advancements in neuroprosthetic technology and our basic understanding of brain function, the brain-machine-interface approach is still expanding the landscape of neuroscienctific inquiry, Moxon said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/du-wcb040815.php){:target="_blank" rel="noopener"}


