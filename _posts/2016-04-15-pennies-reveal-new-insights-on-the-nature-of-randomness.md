---
layout: post
title: "Pennies reveal new insights on the nature of randomness"
date: 2016-04-15
categories:
author: Princeton  University
tags: [Salvatore Torquato,Randomness,Science,Branches of science,Physics,Physical sciences]
---


The concept of randomness appears across scientific disciplines, from materials science to molecular biology. Now, theoretical chemists at Princeton have challenged traditional interpretations of randomness by computationally generating random and mechanically rigid arrangements of two-dimensional hard disks, such as pennies, for the first time. It's amazing that something so simple as the packing of pennies can reveal to us deep ideas about the meaning of randomness or disorder, said Salvatore Torquato, professor of chemistry at Princeton and principal investigator of the report published on December 30 in the journal Proceedings of the National Academy of Sciences. In two dimensions, conventional wisdom held that the most random arrangements of pennies were those most likely to form upon repeated packing, or in other words, most entropically favored. We're saying that school of thought is wrong because you can find much lower density states that have a high degree of disorder, even if they are not seen in typical experiments, Torquato said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/pu-prn030315.php){:target="_blank" rel="noopener"}


