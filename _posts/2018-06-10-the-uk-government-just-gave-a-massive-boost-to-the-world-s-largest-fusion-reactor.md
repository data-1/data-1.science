---
layout: post
title: "The UK Government Just Gave a Massive Boost to the World's Largest Fusion Reactor"
date: 2018-06-10
categories:
author: ""
tags: [Fusion power,United Kingdom Atomic Energy Authority,Nuclear reactor,ITER,Nuclear fusion,Energy development,Energy,Fuel,Sustainable technologies,Nuclear energy,Nature,Nuclear technology,Nuclear physics,Energy technology,Physical quantities,Technology,Power (physics),Nuclear chemistry,Radioactivity,Nuclear power,Forms of energy,Energy conversion]
---


Created for the UK Atomic Energy Authority's (UKAEA) fusion research programme at the Science Centre, many hope that this £86m government investment will help to develop fusion reactor technology for the first nuclear fusion power plants. In one, researchers will explore how to process and store tritium, which is a potential fuel source for commercial fusion reactors. But, while these are separate efforts, they are part of a much larger initiative, known as ITER, to elevate the potential of nuclear fusion and make it a realistically viable power source. And, as opposed to traditional nuclear energy, there is little-to-no nuclear waste created from energy production. Existing commercial, renewable energy sources are providing people around the world with the ability to break free from fossil fuels.

<hr>

[Visit Link](https://futurism.com/uk-government-massive-boost-worlds-largest-fusion-reactor/){:target="_blank" rel="noopener"}


