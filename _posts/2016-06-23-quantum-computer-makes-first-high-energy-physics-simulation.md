---
layout: post
title: "Quantum computer makes first high-energy physics simulation"
date: 2016-06-23
categories:
author: "Castelvecchi, Davide Castelvecchi, You Can Also Search For This Author In"
tags: []
---


The trap researchers used to perform their quantum simulation. To understand exactly what their theories predict, physicists routinely do computer simulations. The team used a tried-and-tested type of quantum computer in which an electromagnetic field traps four ions in a row, each one encoding a qubit, in a vacuum. “The stronger the field, the faster we can create particles and antiparticles,” Martinez says. “We are not yet there where we can answer questions we can’t answer with classical computers,” Martinez says, “but this is a first step in that direction.” Quantum computers are not strictly necessary for understanding the electromagnetic force.

<hr>

[Visit Link](http://www.nature.com/news/quantum-computer-makes-first-high-energy-physics-simulation-1.20136?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


