---
layout: post
title: "Key genetic event underlying fin-to-limb evolution"
date: 2015-08-25
categories:
author: Tokyo Institute Of Technology
tags: [Fish fin,Limb bud,Anatomical terms of location,Forelimb,Tiktaalik,Evolution,Limb development,Tetrapod,Gene expression,Biology,Genetics]
---


A study of catsharks reveals how alterations in the expression and function of certain genes in limb buds underlie the evolution of fish fins to limbs. To determine whether shifts in the balance of anterior and posterior field occurred during fin-to-limb evolution, Onimaru and his colleagues carefully compared the expression, function and regulation of genes involved in anterior-posterior patterning in pectoral fins of catsharks, with those of mice. When the researchers experimentally posteriorised pectoral fin buds of catsharks, the fins lost anterior skeletal elements, and showed a single bone connected to the pectoral girdle, as seen in fossil Tiktaalik pectoral fins (Fig. This suggested that anterior-posterior patterning, and the genes which determine such positioning of skeletal elements, may play a key role in fin-to-limb evolution. When Onimaru experimentally posteriorised the catshark fins, the resulting fins lost parts of their anterior skeletal elements and showed a single basal bone connected to the pectoral girdle.

<hr>

[Visit Link](http://phys.org/news/2015-08-key-genetic-event-underlying-fin-to-limb.html){:target="_blank" rel="noopener"}


