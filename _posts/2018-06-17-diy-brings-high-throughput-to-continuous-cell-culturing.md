---
layout: post
title: "DIY brings high throughput to continuous cell culturing"
date: 2018-06-17
categories:
author: "Rice University"
tags: [Rice University,American Association for the Advancement of Science,Synthetic biology,Technology]
---


A lot of times, antibiotic resistance shows up when you administer the drug continuously. Where do you see it not evolve? The group struggled to find code that could report, record and track all variables across all time scales in each culture chamber but eventually struck gold with software that was originally designed to track stock prices. (Image courtesy of C. Bashor/Rice University)  http://news.rice.edu/files/2018/06/0611_BASHOR-cb10-lg-1563qzl.jpg  CAPTION: Caleb Bashor is assistant professor of bioengineering at Rice University. Located on a 300-acre forested campus in Houston, Rice University is consistently ranked among the nation's top 20 universities by U.S. News & World Report.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/ru-dbh061018.php){:target="_blank" rel="noopener"}


