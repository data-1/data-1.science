---
layout: post
title: "Using CRISPR, biologists find a way to comprehensively identify anti-cancer drug targets"
date: 2016-04-30
categories:
author: Cold Spring Harbor Laboratory
tags: [Protein,Cell (biology),ENCODE,Cold Spring Harbor Laboratory,Cancer,CRISPR,Genome,Biochemistry,Genetics,Molecular biology,Life sciences,Biology,Biotechnology]
---


The spikes indicate the degree to which blocking specific receptors will cause cancer cells to die. Credit: Vakoc Lab, CSHL  Imagine having a complete catalog of the best drug targets to hit in a particularly deadly form of cancer. That experience got us thinking, explains Vakoc, Is there a way we can look at hundreds or thousands of proteins at a time in a given cancer cell type - proteins with druggable binding pockets—and in a single experiment find more BRD4s? More broadly, what we provide is a way to comprehensively identify specific vulnerabilities in cancer cells, across cancer types. My lab is focused on finding a small number of these binding pockets - ones whose function cancer cells are addicted to, Vakoc says.

<hr>

[Visit Link](http://phys.org/news350556748.html){:target="_blank" rel="noopener"}


