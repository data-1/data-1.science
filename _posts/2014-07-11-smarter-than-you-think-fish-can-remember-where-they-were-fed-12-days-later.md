---
layout: post
title: "Smarter than you think: Fish can remember where they were fed 12 days later"
date: 2014-07-11
categories:
author: Society for Experimental Biology 
tags: [Memory,Fish,Food,Cichlid,Behavioural sciences,Neuroscience,Psychological concepts,Interdisciplinary subfields,Mental processes,Cognitive psychology,Cognition,Psychology,Cognitive science]
---


Canadian scientists, however, have demonstrated that this is far from true – in fact, fish can remember context and associations up to twelve days later. The researchers studied African Cichlids (Labidochromis caeruleus), a popular aquarium species. It was found that the cichlids showed a distinct preference for the area associated with the food reward, suggesting that they recalled the previous training experiences. Furthermore, the fish were able to reverse this association after further training sessions where the food reward was associated with a different stimulus. For fish living in the wild, ability to associate locations with food could be vital for survival.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/sfeb-sty062614.php){:target="_blank" rel="noopener"}


