---
layout: post
title: "New satellite to measure plant health"
date: 2015-11-23
categories:
author: "$author"  
tags: [Photosynthesis,Earth,Atmosphere,Carbon cycle,Plant,Soil Moisture and Ocean Salinity,Energy,Satellite,Natural environment,Space science,Physical sciences,Nature,Earth sciences]
---


ESA plans to track the health of the world’s vegetation by detecting and measuring the faint glow that plants give off as they convert sunlight and the atmosphere’s carbon dioxide into energy. Following a rigorous selection process, the satellite will be ESA’s eighth Earth Explorer, planned for launch by 2022. As well as yielding important information about plant health, the Fluorescence Explorer – FLEX – satellite will improve our understanding of the way carbon moves between plants and the atmosphere and how photosynthesis affects the carbon and water cycles. Working in sequence, there are two different ‘solar power systems’ inside plant and algae cells. In addition, the process involves a faint fluorescence, subject to environmental conditions and the health of the plant.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/New_satellite_to_measure_plant_health){:target="_blank" rel="noopener"}


