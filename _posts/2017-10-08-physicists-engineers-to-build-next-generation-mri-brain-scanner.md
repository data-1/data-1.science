---
layout: post
title: "Physicists, engineers to build next-generation MRI brain scanner"
date: 2017-10-08
categories:
author: "University Of California - Berkeley"
tags: [Functional magnetic resonance imaging,Magnetic resonance imaging,Brain,Cerebral cortex,Medical imaging,Neuroscience,Cognitive science]
---


Our innovation in MRI technology requires a total redesign of nearly all of the scanner components, not just an incremental change, said lead researcher David Feinberg, an adjunct professor in the Helen Wills Neuroscience Institute at UC Berkeley and president of Advanced MRI Technologies. With the ability to pinpoint activity to within a volume 0.4 millimeters on a side, they will be able to image functional regions in which most neurons are involved in the same type of processing. Clinical MRI is typically used to look for abnormalities in blood flow in the brain; fMRI is used primarily to research brain function, locating areas that are active during processes such as perception or memorization. While clinical MRIs require large coils to image deep in the brain, Feinberg designed an fMRI system with a much larger number of smaller coils that provide a much stronger signal, yielding the higher resolution in the outer surface of the brain needed to identify key layers of the cortex. The BRAIN Initiative award to Feinberg is the largest of four five-year grants totaling $39.7 million announced last week by the National Institute of Biomedical Imaging and Bioengineering, awarded to researchers developing non-invasive imaging tools to study the human brain  Each project is based on novel concepts, representing the kinds of tools we need for the future of non-invasive imaging for the neuroscience community, said Guoying Liu, director of the MRI program at the NIBIB.

<hr>

[Visit Link](https://phys.org/news/2017-10-physicists-next-generation-mri-brain-scanner.html){:target="_blank" rel="noopener"}


