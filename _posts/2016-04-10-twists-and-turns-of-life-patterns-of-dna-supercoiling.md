---
layout: post
title: "Twists and turns of life: Patterns of DNA supercoiling"
date: 2016-04-10
categories:
author: National Centre for Biological Sciences
tags: [DNA,DNA supercoil,Gene,Bacteria,Genome,Cell (biology),Biology,Genetics,Nucleic acid double helix,Gene expression,Biotechnology,Molecular biology,Biochemistry,Life sciences,Nucleic acids,Macromolecules,Chemistry]
---


The molecule that holds the codes of life is capable of further winding itself into myriad complex shapes called 'supercoils' that are capable of affecting gene expression patterns. But this 'packed' DNA that fits neatly into a cell also needs to be 'unpacked' periodically for gene expression and replication. This requires DNA to be unwound from its double helix - a process that causes further twisting and coiling or 'overwinding' in regions of DNA elsewhere on the genome. Researchers from Aswin S. N. Seshasayee's group at NCBS and Prof. Sankar Adhya's team at NIH have currently applied these methods to study DNA supercoiling in bacteria at a fine-scale level. In order to study the effect of environmental stimuli on the supercoiling status of the bacterial genome, two populations of E. coli were used to simulate two different external conditions.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/ncfb-tat040316.php){:target="_blank" rel="noopener"}


