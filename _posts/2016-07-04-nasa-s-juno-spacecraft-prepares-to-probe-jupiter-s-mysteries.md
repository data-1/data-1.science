---
layout: post
title: "NASA’s Juno spacecraft prepares to probe Jupiter’s mysteries"
date: 2016-07-04
categories:
author: "Witze, Alexandra Witze, You Can Also Search For This Author In"
tags: []
---


Jupiter’s Great Red Spot also reveals a mosaic of currents that swirl through the planet’s atmosphere. Picking up where Galileo left off, Juno is designed to answer basic questions about Jupiter, including what its water content is, whether it has a core and what is happening at its rarely seen poles (see ‘Mission to Jupiter’). As Juno probes deeper and deeper into the planet’s atmosphere, researchers hope to get information on a layer of hydrogen compressed into a liquid by increasing pressures. To avoid the most dangerous radiation belts that surround the gas giant — which over the lifetime of the mission could fry the spacecraft with the equivalent of more than 100 million dental X-rays— Juno will take a long elliptical dive around the planet on every orbit. The only other mission planned to the gas giant is the European Space Agency’s Jupiter Icy Moons Explorer (JUICE) spacecraft, which could launch as early as 2022 and will focus mainly on the moon Ganymede.

<hr>

[Visit Link](http://www.nature.com/news/nasa-s-juno-spacecraft-prepares-to-probe-jupiter-s-mysteries-1.20179){:target="_blank" rel="noopener"}


