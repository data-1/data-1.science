---
layout: post
title: "Study of birds' sense of smell reveals important clues for behavior and adaptation"
date: 2015-09-07
categories:
author: SMBE Journals (Molecular Biology and Evolution and Genome Biology and Evolution) 
tags: [Sense of smell,Olfactory bulb,Neuroscience,Animals]
---


Now, a large comparative genomic study of the olfactory genes tied to a bird's sense of smell has revealed important differences that correlate with their ecological niches and specific behaviors. The study suggest that specific OR genes are used not only to detect a range of chemicals governing a bird's ability to smell, but that in birds, specialized olfactory skills, such as those found in birds of prey or aquatic birds, was mirrored by the genetic diversity of their OR gene families. In the study, drastic expansion of specific OR's gene families, such as OR51 and OR52, were seen in sea turtles and aquatic birds, which the author's proposed are use to detect water-loving (hydrophilic) compounds. These were also the largest OR families observed in alligators, which like birds of prey, depend on hunting or scavenging for food, which also suggests that these genes are needed for adaptation in carnivores. Overall, different ecological partitioning and specialized groups of birds, such as vocal learners, birds of prey, water birds and land birds, was strongly correlated with OR differences suggesting that the OR families may contribute towards olfactory adaptations.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/mbae-sob072915.php){:target="_blank" rel="noopener"}


