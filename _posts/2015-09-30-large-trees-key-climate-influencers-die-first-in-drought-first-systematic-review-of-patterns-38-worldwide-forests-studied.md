---
layout: post
title: "Large trees -- key climate influencers -- die first in drought: First systematic review of patterns, 38 worldwide forests studied"
date: 2015-09-30
categories:
author: "$author" 
tags: [Forest,Tree,Drought,News aggregator,Carbon cycle,Transpiration,Ecosystem,Stoma,Leaf,Water,Research,Canopy (biology),Climate change,Natural environment,Earth sciences,Nature,Physical geography,Earth phenomena,Ecology,Systems ecology]
---


Additionally, large trees with crowns high in the canopy are exposed to higher solar radiation, and the ability to transport water to their foliage is lower. In all of the seasonal tropical forests analyzed by the team, the growth rate of understory trees actually increased in response to drought, possibly due to the increased light reaching smaller trees caused in part by the larger trees losing foliage under stress. Researchers sample, analyze 38 forests worldwide  The research project included both natural and experimental droughts. Accounting for standard growth/mortality rates, the team analyzed tree size-related variation responses based on diameter growth and death under drought and normal conditions; and the percentage of drought-related death. Researchers note that the global consequences on ecosystem function, biodiversity, and the carbon cycle that begets climate change could be great.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150929142248.htm){:target="_blank" rel="noopener"}


