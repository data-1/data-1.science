---
layout: post
title: "SpaceChain, Arch Aim to Archive Human Knowledge in Space"
date: 2018-05-01
categories:
author: ""
tags: []
---


Join us August 23 at 1PM ET for a free online event to learn how to make your site accessible and start increasing your sales. SpaceChainon Monday announced that it has entered a partnership with the Arch Mission Foundation to use open source technology to launch an ambitious project involving the storage of large data sets in spacecraft and on other planets. “The ambitious goal of disseminating this knowledge throughout the solar system is finally achievable today, thanks to greatly reduced launch costs through new space launch providers.”  SpaceChain’s decision to support the Arch mission to archive human data in space will help launch the Earth Library — a ring of backup data orbiting around the Earth — said Nova Spivack, cofounder of the Arch Mission Foundation. The goal of the foundation is to “preserve and disseminate humanity’s most important information across time and space, for the benefit of future generations,” he told LinuxInsider. The Arch Mission included its first library, including the Isaac Asimov Foundation Trilogy, in that Falcon Heavy payload.

<hr>

[Visit Link](http://www.linuxinsider.com/story/85196.html?rss=1){:target="_blank" rel="noopener"}


