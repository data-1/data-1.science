---
layout: post
title: "Blades of grass inspire advance in organic solar cells"
date: 2015-07-24
categories:
author: University of Massachusetts Amherst 
tags: [Crystal,Organic solar cell,Anisotropy,Polymer,Solar cell,Graphene,Chemistry,Materials,Materials science,Applied and interdisciplinary physics,Physical chemistry,Technology,Condensed matter physics,Physical sciences,Chemical product engineering,Manufacturing,Building engineering]
---


Briseno explains, For decades scientists and engineers have placed great effort in trying to control the morphology of p-n junction interfaces in organic solar cells. We report here that we have at last developed the ideal architecture composed of organic single-crystal vertical nanopillars. The advance not only addresses the problem of dead ends or discontinuous pathways that make for inefficient energy transfer, but it also solves some instability problems, where the materials in mixed blends of polymers tend to lose their phase-separated behavior over time, degrading energy transfer, the polymer chemist says. Stacked compounds are ideal for charge transport since this configuration has the largest charge transport anisotropy. In this case, the anisotropy is along the nanopillar, perpendicular to the substrate.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/uoma-bog093014.php){:target="_blank" rel="noopener"}


