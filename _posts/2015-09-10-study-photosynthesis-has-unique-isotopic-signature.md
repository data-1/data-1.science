---
layout: post
title: "Study: Photosynthesis has unique isotopic signature"
date: 2015-09-10
categories:
author: Rice University 
tags: [Isotope,Oxygen,Molecule,Biosignature,Atom,Rice University,Oxygen-18,Chemical element,Earth sciences,Nature,Chemistry,Physical sciences,Chemical elements,Atoms]
---


We've found a new type of biosignature, said co-lead author Laurence Yeung, an assistant professor of Earth science at Rice University. Most atmospheric oxygen exists as O2, stable molecules that each contain two oxygen atoms. Even in cases where a heavy isotope is paired with O-16, the atomic weight of O2 is never greater than 34. In the new study, Yeung and his UCLA colleagues examined clumped oxygen isotopes, O2 molecules that contain two heavy isotopes. High-resolution IMAGES are available for download at:  http://news.rice.edu/wp-content/uploads/2015/04/0330_ISOTOPE-trio-lg.jpg  CAPTION: Using stable isotopic analysis, Laurence Yeung, Jeanine Ash, and Edward Young discovered that plants and plankton impart a unique biosignature on the oxygen they produce during photosynthesis.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/ru-sph042115.php){:target="_blank" rel="noopener"}


