---
layout: post
title: "NASA rover Opportunity completes marathon milestone on Mars"
date: 2016-04-19
categories:
author:  
tags: [Opportunity (rover),Rover (space exploration),Spirit (rover),Astronautics,Spaceflight,Outer space,Space science,Mars,Astronomical objects known since antiquity]
---


Credit: NASA  NASA says the Opportunity rover has passed the marathon mark for driving on Mars. The space agency said Tuesday the rover's odometer checked in at 26.2 miles—the distance of a marathon. The official time? Eleven years and two months. Spirit's mission ended in 2011 not long after it got stuck in Martian sand.

<hr>

[Visit Link](http://phys.org/news346439334.html){:target="_blank" rel="noopener"}


