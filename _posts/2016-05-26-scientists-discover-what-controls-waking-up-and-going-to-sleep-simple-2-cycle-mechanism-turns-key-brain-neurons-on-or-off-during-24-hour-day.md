---
layout: post
title: "Scientists discover what controls waking up and going to sleep: Simple 2-cycle mechanism turns key brain neurons on or off during 24-hour day"
date: 2016-05-26
categories:
author: Northwestern University
tags: [Circadian rhythm,CLOCK,Ravi Allada,Diurnality,Biology,Animal physiology,Zoology,Chronobiology,Sleep,Animals,Physiology,Neuroscience]
---


The clock's mechanism, it turns out, is much like a light switch. In a study of brain circadian neurons that govern the daily sleep-wake cycle's timing, Allada and his research team found that high sodium channel activity in these neurons during the day turn the cells on and ultimately awaken an animal, and high potassium channel activity at night turn them off, allowing the animal to sleep. That the researchers found the two pedals -- a sodium current and potassium currents -- active in both the simple fruit fly and the more complex mouse was unexpected. What is amazing is finding the same mechanism for sleep-wake cycle control in an insect and a mammal, said Matthieu Flourakis, the lead author of the study. When he joined Allada's team, Flourakis had wondered if the activity of the fruit fly's circadian neurons changed with the time of day.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150813142719.htm){:target="_blank" rel="noopener"}


