---
layout: post
title: "Link between magnetic field strength and temperature"
date: 2018-08-22
categories:
author: "American Institute of Physics"
tags: [Photoluminescence,Magnetic field,Applied and interdisciplinary physics,Condensed matter physics,Quantum mechanics,Atomic molecular and optical physics,Materials science,Materials,Physical chemistry,Electromagnetism,Chemistry,Physics,Physical sciences,Electrical engineering]
---


Researchers recently discovered that the strength of the magnetic field required to elicit a particular quantum mechanical process, such as photoluminescence and the ability to control spin states with electromagnetic (EM) fields, corresponds to the temperature of the material. Negatively charged vacancy centers are also photoluminescent and produce a detectable glow when exposed to certain wavelengths of light. A team of Russian and German researchers created a system that can measure temperatures and magnetic fields at very small resolutions. The scientists produced crystals of silicon carbide with vacancies similar to the nitrogen-vacancy centers in diamonds. Then, they exposed the silicon carbide to infrared laser light in the presence of a constant magnetic field and recorded the resulting photoluminescence.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180820113027.htm){:target="_blank" rel="noopener"}


