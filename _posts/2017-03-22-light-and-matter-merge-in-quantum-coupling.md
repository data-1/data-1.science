---
layout: post
title: "Light and matter merge in quantum coupling"
date: 2017-03-22
categories:
author: "Rice University"
tags: [Photon,Condensed matter physics,Light,Quantum electrodynamics,Vacuum,Cavity quantum electrodynamics,Electron,Matter,Physics,Optics,Nonlinear optics,Theoretical physics,Physical sciences,Science,Applied and interdisciplinary physics,Materials science,Condensed matter,Nature,Physical chemistry,Electromagnetic radiation,Chemistry,Atomic molecular and optical physics,Quantum mechanics,Electromagnetism]
---


Where light and matter interact so strongly that they become one, they illuminate a world of new physics, according to Rice University scientists. In more than 99 percent of previous studies of light-matter coupling in cavities, this value is a negligibly small fraction of the photon energy of the light used, said Xinwei Li, a co-author and graduate student in Kono's group. But in a quantum sense, a vacuum is full of fluctuating photons, having so-called zero-point energy. These vacuum photons are actually what we are using to resonantly excite electrons in our cavity. In cavity QED, the cavity enhances the light so that matter in the cavity resonantly interacts with the vacuum field.

<hr>

[Visit Link](http://phys.org/news/2016-08-merge-quantum-coupling.html){:target="_blank" rel="noopener"}


