---
layout: post
title: "Image: Messenger's iridescent Mercury"
date: 2016-05-08
categories:
author: European Space Agency
tags: [Mercury (planet),BepiColombo,JAXA,Local Interstellar Cloud,Planets of the Solar System,Astronomical objects known since antiquity,Flight,Sky,Astronautics,Astronomical objects,Bodies of the Solar System,Spaceflight,Planetary science,Solar System,Outer space,Astronomy,Space science]
---


Swathes of iridescent blue, sandy-coloured plains and delicate strands of greyish white, create an ethereal and colourful view of our Solar System's innermost planet. These regions take on a light blue hue for a different reason: their youthfulness. The yellowish, tan-coloured regions are intermediate terrain. On 30 April this year, Messenger's four-year stint in orbit around Mercury came to an end when the probe ploughed into the surface. Messenger was launched in 2004 and in 2011 became the first spacecraft ever to orbit Mercury.

<hr>

[Visit Link](http://phys.org/news353656079.html){:target="_blank" rel="noopener"}


