---
layout: post
title: "ESA congratulations on gravitational wave discovery"
date: 2016-03-15
categories:
author:  
tags: [Laser Interferometer Space Antenna,Gravitational wave,LIGO,Gravitational-wave observatory,LISA Pathfinder,Astrophysics,Gravity,General relativity,Physics,Physical cosmology,Theory of relativity,Space science,Celestial mechanics,Outer space,Astronomy,Science,Physical sciences]
---


The discovery was announced today by scientists from the Laser Interferometer Gravitational-Wave Observatory (LIGO) collaboration. LISA Pathfinder in space “This is tremendous news for everyone studying gravity and general relativity, and we send our warmest congratulations to colleagues in the LIGO collaboration for their outstanding result,” says Paul McNamara, LISA Pathfinder project scientist at ESA. LISA Pathfinder is ESA’s technology demonstration mission for possible future missions to observe gravitational waves from space. The two monstrous bodies, with masses equivalent to 36 and 29 times the mass of the Sun, respectively, merged to form a single, even more gigantic black hole of 62 solar masses, releasing the remaining 3 solar masses in gravitational waves. That’s where the move to space will make the difference,” says Oliver Jennrich, LISA Pathfinder deputy project scientist at ESA.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/ESA_congratulations_on_gravitational_wave_discovery){:target="_blank" rel="noopener"}


