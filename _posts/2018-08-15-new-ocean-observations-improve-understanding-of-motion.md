---
layout: post
title: "New ocean observations improve understanding of motion"
date: 2018-08-15
categories:
author: "University of Hawaii at Manoa"
tags: [Ocean,Sea,Sea level,Geostrophic wind,Ocean current,Eddy (fluid dynamics),Hydrology,Nature,Hydrography,Oceanography,Physical geography,Earth sciences,Applied and interdisciplinary physics]
---


Oceanographers commonly calculate large scale surface ocean circulation from satellite sea level information using a concept called geostrophy, which describes the relationship between oceanic surface flows and sea level gradient. New research led by University of Hawai'i at Mānoa (UHM) oceanographer Bo Qiu has determined from observational data the length scale at which using sea level height no longer offers a reliable calculation of circulation. Prior to this study, published in Nature Communications, oceanographers knew that sea level can be used to provide a picture of circulation in a general way but not in very fine detail. However, the specific level of detail that can be provided using this approach was not known, until this study. However, in areas where motion is dominated by internal waves, satellite sea level can only be used to infer motion on a very large scale (resolution of 125 miles).

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/uoha-noo013017.php){:target="_blank" rel="noopener"}


