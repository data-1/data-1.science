---
layout: post
title: "Scientists watch bacteria 'harpoon' DNA to speed their evolution"
date: 2018-08-06
categories:
author: "Indiana University"
tags: [Pilus,Bacteria,Cell (biology),DNA,Antimicrobial resistance,Evolution,Biology,Medical specialties,Life sciences,Microbiology,Biotechnology]
---


The upper images show a bacterial pilus (in green) latching onto a piece of DNA (in red) and pulling it back into the cell, the first steps in the DNA uptake process. Credit: Ankur Dalia, Indiana University  Indiana University scientists have made the first direct observation of a key step in the process that bacteria use to rapidly evolve new traits, including antibiotic resistance. The pili then reel the DNA into the bacterial cell through the same pore. Ankur Dalia. This method invented at IU is really opening up our basic understanding about a whole range of bacterial functions.

<hr>

[Visit Link](https://phys.org/news/2018-06-scientists-bacteria-harpoon-dna-evolution.html){:target="_blank" rel="noopener"}


