---
layout: post
title: "10 open source task management and time tracking applications for Android"
date: 2017-09-24
categories:
author: "Joshua Allen Holm
(Alumni)"
tags: [Mobile app,F-Droid,Android (operating system),Technology,Computing,Software,Digital media,Software development,Intellectual works]
---


To add to the conversation Jason and Jen started, I would like to share with you 10 apps for your Android phone that can help you get organized, keep track of the time you have spent on a task, and make sure you meet your deadlines. Of the two Pomodoro apps in the F-Droid repository, it is the one with the better user experience and design. Due dates are set using due: YYYY-MM-DD . Having the timer running for multiple items at the same time is possible, but the app does not provide a detailed report for all time spent on tasks. The closest thing TasClock has is a detailed view for each individual task, which you can access by long-pressing on the task in the list.

<hr>

[Visit Link](https://opensource.com/article/17/1/task-management-time-tracking-android){:target="_blank" rel="noopener"}


