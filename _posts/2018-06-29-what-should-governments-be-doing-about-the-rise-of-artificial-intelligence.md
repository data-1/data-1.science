---
layout: post
title: "What should governments be doing about the rise of artificial intelligence?"
date: 2018-06-29
categories:
author: "David Glance, The Conversation"
tags: [Artificial intelligence,Machine learning,Deep learning,Bias,Learning,Issues in ethics,Computing,Cognition,Cognitive science,Branches of science,Technology]
---


The single biggest problem in understanding AI however has been making it clear how current AI techniques (like deep learning) differ from human intelligence. Deep learning software learns to be able to recognise patterns from data. The concerns about data used for AI applications  With the use of large amounts of data, questions immediately arise as to from where the data is collected, and what exactly it is being used for. So, what should government being doing about AI? There are many more areas of discussion that become important for governments and the public in considering the role of AI in their societies.

<hr>

[Visit Link](https://phys.org/news/2017-10-artificial-intelligence_1.html){:target="_blank" rel="noopener"}


