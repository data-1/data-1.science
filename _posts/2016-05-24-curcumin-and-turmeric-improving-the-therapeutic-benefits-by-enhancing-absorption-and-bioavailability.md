---
layout: post
title: "Curcumin and turmeric: Improving the therapeutic benefits by enhancing absorption and bioavailability"
date: 2016-05-24
categories:
author: Taylor & Francis
tags: [Curcuminoid,Bioavailability,Curcumin]
---


Success in translating this potential into tangible benefits has been limited by inherently poor intestinal absorption, rapid metabolism, and limited systemic bioavailability. Seeking to overcome these limitations, food ingredient formulators have begun to employ a variety of approaches to enhance absorption and bioavailability. Curcumin is currently being actively researched. When asked about the future of this field of research, author Dallas Clouatre said, I would like to see and perhaps be involved in research on improving bioavailability. The article authors conclude, Delivery strategies can significantly improve the bioavailability of curcuminoids.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150812134254.htm){:target="_blank" rel="noopener"}


