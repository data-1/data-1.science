---
layout: post
title: "How early mammals evolved night vision to avoid predators"
date: 2016-06-21
categories:
author: "Cell Press"
tags: [Retina,Photoreceptor cell,Cone cell,Rod cell,Evolution,Eye,Mammal,Biology,Genetics]
---


To investigate the origin of rods in mammals, Swaroop and his team examined rod and cone cells taken from mice at different stages of development. A majority of the photoreceptors (97%) in the retina are rods (black). Credit: Jung-Woong Kim  The researchers saw that in early stages, two days after the mice were born, developing rod cells expressed genes normally seen in mature short-wavelength cones (which are used in other animals to detect ultraviolet light). (Counter-intuitively, humans depend more on cones for our vision, but that's because our ancestors later evolved to take advantage of the daylight hours again.) The evolution of rod-dominant retinas was a critical adaptation, allowing mammalian ancestors to survive a nocturnal bottleneck.

<hr>

[Visit Link](http://phys.org/news/2016-06-early-mammals-evolved-night-vision.html){:target="_blank" rel="noopener"}


