---
layout: post
title: "Mars Triptych"
date: 2016-05-26
categories:
author:  
tags: [Mars,Hubble Space Telescope,Astronomical objects known since antiquity,Planets,Planets of the Solar System,Physical sciences,Astronomical objects,Bodies of the Solar System,Solar System,Planetary science,Outer space,Space science,Astronomy]
---


The image at left was taken on 22 May by amateur astrophotographer Dylan O’Donnell from his home-built backyard observatory in Byron Bay, New South Wales. On 22 May, Mars was at opposition with respect to Earth, meaning the Sun and Mars were on exact opposite sides of Earth. The image at right is the ESA/NASA Hubble space telescope image that Dylan used for comparison with his own photo, and was acquired on 12 May (just 10 days before the opposition described above) when the planet was 80 million km from Earth, revealing details as small as 32–48 km across. The image at centre was acquired by the Visual Monitoring Camera on Mars Express about 10 000 kms from the surface at 22:17 GMT on 22 May, some eight hours after Dylan conducted his observation. On 30 May, Mars will be the closest it has been to Earth in 11 years, at a distance of 75.3 million km.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2016/05/Mars_Triptych){:target="_blank" rel="noopener"}


