---
layout: post
title: "Major breakthrough in understanding Alzheimer's disease"
date: 2016-07-04
categories:
author: "Trinity College Dublin"
tags: [Bloodbrain barrier,Amyloid beta,Health sciences,Neuroscience,Causes of death,Health,Clinical medicine,Medicine,Medical specialties,Diseases and disorders]
---


Alzheimer's disease is characterized, in part, by the build-up of a small protein ('amyloid-beta') in the brains of patients. Unlike blood vessels anywhere else in the body, those in the brain have properties that strictly regulate what gets in and out of the delicate tissue -- this is what is known as the blood-brain barrier (BBB). The BBB functions as a tightly regulated site of energy and metabolite exchange between the brain tissue and the bloodstream. We think that this alteration could be an entrained mechanism to allow for the clearance of toxic amyloid-beta from the brain in those living with Alzheimer's disease, said postdoctoral researcher in Trinity's School of Genetics and Microbiology, Dr James Keaney, who spearheaded the study. Scientists in the laboratories of Dr Matthew Campbell, Professor Peter Humphries, Trinity's Smurfit Institute of Genetics, Professor Dominic Walsh of Harvard University, and Professor Michael Farrell, Consultant Pathologist and Director of the Dublin Brain Bank at Beaumont Hospital, collaborated on this study.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150918152000.htm){:target="_blank" rel="noopener"}


