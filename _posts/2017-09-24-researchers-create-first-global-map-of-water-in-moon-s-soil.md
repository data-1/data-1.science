---
layout: post
title: "Researchers create first global map of water in moon's soil"
date: 2017-09-24
categories:
author: "Brown University"
tags: [Moon,Lunar water,Water,Nature,Solar System,Physical sciences,Outer space,Bodies of the Solar System,Astronomical objects known since antiquity,Earth sciences,Planets of the Solar System,Planetary science,Space science,Astronomy]
---


The signature of water is present nearly everywhere on the lunar surface, not limited to the polar regions as previously reported, said the study's lead author, Shuai Li, who performed the work while a Ph.D. student at Brown University and is now a postdoctoral researcher at the University of Hawaii. Now that we have these quantitative maps showing where the water is and in what amounts, we can start thinking about whether or not it could be worthwhile to extract, either as drinking water for astronauts or to produce fuel. We don't know exactly what the mechanism is for this fluctuation, but it tells us that the process of water formation in the lunar soil is active and happening today, Milliken said. Many scientists think these permanently shadowed regions, such as the floors on impact craters in the Moon's polar regions, could hold large deposits or water ice. But these results show us what the range of water availability across the surface is so we can start thinking about where we might want to go to get it and whether it makes economic sense to do so.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-09/bu-rcf091317.php){:target="_blank" rel="noopener"}


