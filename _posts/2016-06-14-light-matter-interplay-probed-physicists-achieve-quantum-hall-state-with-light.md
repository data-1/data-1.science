---
layout: post
title: "Light-matter interplay probed: Physicists achieve quantum Hall state with light"
date: 2016-06-14
categories:
author: "University of Chicago"
tags: [Quantum mechanics,Electron,Light,Waveparticle duality,Matter,Photon,Quantum Hall effect,Mirror,Magnetism,News aggregator,Physics,Science,Scientific theories,Electromagnetism,Nature,Applied and interdisciplinary physics,Theoretical physics,Physical sciences]
---


We make the photons spin, which leads to a force that has the same effect as a magnetic field, explained Schine. While the light is trapped, it behaves like the electrons in a quantum Hall material. First, Simon's group demonstrated that they had a quantum Hall material of light. Despite 20 years of interest, this is the first time an experiment has observed the behavior of quantum materials in curved space. The researchers say this could be useful for characterizing a certain type of quantum computer that is built of quantum Hall materials.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/06/160613144700.htm){:target="_blank" rel="noopener"}


