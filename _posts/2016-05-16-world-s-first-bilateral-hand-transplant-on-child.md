---
layout: post
title: "World's first bilateral hand transplant on child"
date: 2016-05-16
categories:
author: Children's Hospital of Philadelphia
tags: [Surgery,Hand transplantation,Childrens Hospital of Philadelphia,Kidney transplantation,Perelman School of Medicine at the University of Pennsylvania,Organ transplantation,Medicine,News aggregator,Organ donation,Medical specialties,Clinical medicine,Health care,Health]
---


The success of Penn's first bilateral hand transplant on an adult, performed in 2011, gave us a foundation to adapt the intricate techniques and coordinated plans required to perform this type of complex procedure on a child. Zion was initially referred to Shriners Hospitals for Children for their expertise in pediatric orthopaedic care, including surgery and rehabilitation. As with all types of transplant, surgeries such as this one could not take place without the generosity of a donor and a donor family. During the surgery, the hands and forearms from the donor were attached by connecting bone, blood vessels, nerves, muscles, tendons and skin. Zion is being cared for by CHOP's nephrology and kidney transplant team, as well as his hand transplant surgical team.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150728162422.htm){:target="_blank" rel="noopener"}


