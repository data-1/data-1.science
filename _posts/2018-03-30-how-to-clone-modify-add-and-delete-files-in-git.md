---
layout: post
title: "How to clone, modify, add, and delete files in Git"
date: 2018-03-30
categories:
author: "Kedar Vijay Kulkarni
(Red Hat)"
tags: [Computer file,Git,GitHub,Diff,README,Software engineering,Computing,Technology,Software,Information Age,Computer data,Information technology management,Software development,Computers,Utility software,System software,Information technology,Data management,Digital media,Computer architecture,Computer files]
---


Open the terminal and type the following command to clone your GitHub repo to your computer:  git clone https://github.com/<your_username>/Demo.git  Then, to see the list of files in the Demo directory, enter the command:  ls Demo/  Your terminal should look like this:  opensource.com  Modify files  Now that we have cloned the repo, let's modify the files and update them on GitHub. Add a new file to Git  Now that we have modified a file and updated it on GitHub, let's create a new file, add it to Git, and upload it to GitHub. git add can be used when we are adding a new file to Git, modifying contents of an existing file and adding it to Git, or deleting a file from a Git repo. As soon as you commit this change and push it to GitHub, the file will be removed from the repo on GitHub as well. Do this by running:  git commit -m Delete file.txt  git push -u origin master  Now your terminal looks like this:  opensource.com  And your GitHub looks like this:  opensource.com  Now you know how to clone, add, modify, and delete Git files from your repo.

<hr>

[Visit Link](https://opensource.com/article/18/2/how-clone-modify-add-delete-git-files){:target="_blank" rel="noopener"}


