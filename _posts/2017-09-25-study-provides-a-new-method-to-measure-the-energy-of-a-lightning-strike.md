---
layout: post
title: "Study provides a new method to measure the energy of a lightning strike"
date: 2017-09-25
categories:
author: "University of South Florida"
tags: [Lightning,Fulgurite,Energy,Heat,Nature,Physical quantities]
---


By investigating 'fossilized' sand cylinders made by lightning strikes, sometimes thousands of years old, a University of South Florida professor provides a unique history of lightning and the energy contained in a single strike  TAMPA, Fla. (Aug. 3, 2016) - Florida, often recognized as the lightning capital of the United States, is a great place to study the amount of energy released by a lightning strike. The team of Pasek and Hurst is the first to investigate the energy in lightning strikes by using geology after-the-fact research, rather than measuring energy during a strike. By conducting this lightning strike archaeology, the researchers were able to measure the energy in a bolt of lightning that struck Florida sand thousands of years ago. According to Pasek, the energy released by lightning is measured in megajoules, also expressed as MJ/m. While we presented a new method for measuring by using fossilized lightning rocks, we also found - for the first time - that lightning strikes follow something called a 'lognormal trend, explained Pasek.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/uosf-spa080216.php){:target="_blank" rel="noopener"}


