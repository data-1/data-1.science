---
layout: post
title: "Detecting the birth and death of a phonon"
date: 2018-07-03
categories:
author: "Ecole Polytechnique Fédérale de Lausanne"
tags: [Photon,Raman scattering,Raman spectroscopy,Energy,Electromagnetism,Electrodynamics,Radiation,Science,Applied and interdisciplinary physics,Physics,Physical chemistry,Quantum mechanics,Atomic molecular and optical physics,Optics,Chemistry,Atomic physics,Electromagnetic radiation,Physical sciences,Theoretical physics]
---


First, one such pulse is shot onto a diamond crystal to excite a single phonon inside it. Thanks to another detector, they now record photons that have reabsorbed the energy of the vibration. These photons are witnesses that the phonon was still alive, meaning that the crystal was still vibrating with exactly the same energy. In the language of quantum mechanics, the act of measuring the system after the first pulse creates a well-defined quantum state of the phonon, which is probed by the second pulse, says Christophe Galland. It can also be refined to create more exotic vibrational quantum states, such as entangled states where energy is delocalized over two vibrational modes.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/epfd-dtb060518.php){:target="_blank" rel="noopener"}


