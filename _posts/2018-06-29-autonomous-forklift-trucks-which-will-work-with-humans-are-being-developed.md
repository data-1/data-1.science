---
layout: post
title: "Autonomous forklift trucks which will work with humans are being developed"
date: 2018-06-29
categories:
author: "University of Lincoln"
tags: [Robotics,Vehicular automation,Forklift,Automation,Automated guided vehicle,Robot,Warehouse,Manufacturing,Technology,Branches of science]
---


The consortium is led by Örebro University in Sweden, and includes University of Lincoln, UK, University of Pisa, Italy, and Leibniz University, Hannover, Germany, working with major industrial partners Bosch, Kollmorgen Automation, ACT Operations Research, Logistic Engineering Services and Orkla Foods. ILIAD will deliver significant technological advances into a single integrated system ready for easy, low-cost deployment and without the need for major infrastructure investments. A key requirement is that each robot is 'human aware' - equipped with advanced computer vision and artificial intelligence to detect, track and predict the behaviour of humans and to plan movements based on the machines' own observations of warehouse lay-outs and patterns of activity. Professor Tom Duckett is Director of the Lincoln Centre for Autonomous Systems (L-CAS) at University of Lincoln and a Principal Investigator on the ILIAD project: Our goal is to deliver an economical, flexible robotic solution that can be easily deployed and integrated into current warehouse facilities and which guarantees efficient and safe operation in environments shared with humans. He said: In addition to the development of self-deploying and self-optimising fleets of autonomous vehicles that are able to unwrap pallets and manipulate stacked goods, this project is crucially interested in safety and the collaboration of robot and humans.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/uol-aft110317.php){:target="_blank" rel="noopener"}


