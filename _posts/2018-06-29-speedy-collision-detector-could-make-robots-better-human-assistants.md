---
layout: post
title: "Speedy collision detector could make robots better human assistants"
date: 2018-06-29
categories:
author: "University of California - San Diego"
tags: [Robotics,Machine learning,Robot,Collision detection,News aggregator,Computational neuroscience,Computer science,Artificial intelligence,Cognitive science,Computing,Cybernetics,Applied mathematics,Emerging technologies,Cognition,Systems science,Branches of science,Technology]
---


Electrical engineers at the University of California San Diego have developed a faster collision detection algorithm that uses machine learning to help robots avoid moving objects and weave through complex, rapidly changing environments in real time. A team of engineers, led by Michael Yip, a professor of electrical and computer engineering and member of the Contextual Robotics Institute at UC San Diego, will present the new algorithm at the first annual Conference on Robot Learning Nov. 13 to 15 at Google headquarters in Mountain View, Calif. They spend a lot of time specifying all the points in a given space -- the specific 3D geometries of the robot and obstacles -- and performing collision checks on every single point to determine whether two bodies are intersecting at any given time. The result was Fastron, an algorithm that uses machine learning strategies -- which are traditionally used to classify objects -- to classify collisions versus non-collisions in dynamic environments. As obstacles move, the classification boundary changes.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/11/171114123406.htm){:target="_blank" rel="noopener"}


