---
layout: post
title: "How open science evolved in 2017"
date: 2018-02-18
categories:
author: "Ben Cotton
(Alumni, Red Hat)"
tags: [Red Hat,Technology,Computing,Intellectual works,Software,Communication,Open-source movement,Information technology]
---


This year, Opensource.com contributors shared some of the ways open source software and science teamed up. Why aren't more researchers using open source? Developing the scientific code alongside the scientific research shortens the time to discovery. Peering into complex, tiny structures with 3D analysis tool tomviz  Physicists, biologists, and many others incorporate 3D visualizations of microscopic objects into their scientific work. What Grey's Anatomy taught me about open scientific research  Open inspiration can come from the strangest places.

<hr>

[Visit Link](https://opensource.com/article/17/12/best-open-science){:target="_blank" rel="noopener"}


