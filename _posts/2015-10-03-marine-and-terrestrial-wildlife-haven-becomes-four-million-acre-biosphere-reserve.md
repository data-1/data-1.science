---
layout: post
title: "Marine and terrestrial wildlife haven becomes four million-acre biosphere reserve"
date: 2015-10-03
categories:
author: Wildlife Conservation Society
tags: [Valdes Peninsula,Chubut Province,Patagonia,Magellanic penguin,Animals]
---


WCS has worked to protect wildlife in this region since the 1960s. A rugged peninsula in Argentina's Patagonia region teeming with wildlife, including southern right whales, Magellanic penguins, massive elephant seals, flightless Darwin's rheas, and camel-like guanacos, has been declared a Biosphere Reserve by the United Nations Environmental, Scientific, and Cultural Organization (UNESCO). Península Valdés on the Atlantic coast of Patagonia in Southern Argentina in Chubut Province has the largest breeding colony of southern elephant seals in South America. WCS has worked to protect wildlife in this region since the 1960s. Making this incredible area region a UNESCO Biosphere Reserve is the culmination of years of hard work by many great partners.

<hr>

[Visit Link](http://phys.org/news324711903.html){:target="_blank" rel="noopener"}


