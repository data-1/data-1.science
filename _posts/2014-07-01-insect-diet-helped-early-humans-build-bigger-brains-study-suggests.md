---
layout: post
title: "Insect diet helped early humans build bigger brains, study suggests"
date: 2014-07-01
categories:
author: Washington University In St. Louis
tags: [Robust capuchin monkey,Capuchin monkey,Gracile capuchin monkey,Animals]
---


A steady diet of abundant ripe fruit and leaf-crawling insects may explain why Cebus can’t hold a stick to their Sapajus capuchin cousins when it comes to tool usage and other higher-level cognitive skills. It notes that many human populations also eat embedded insects on a seasonal basis and suggests that this practice played a key role in human evolution. Previous research has shown that fallback foods help shape the evolution of primate body forms, including the development of strong jaws, thick teeth and specialized digestive systems in primates whose fallback diets rely mainly on vegetation. This study suggests that fallback foods can also play an important role in shaping brain evolution among primates that fall back on insect-based diets, and that this influence is most pronounced among primates that evolve in habitats with wide seasonal variations, such as the wet-dry cycles found in some South American forests. We predict that the last common ancestor of Cebus and Sapajus had a level of SMI more closely resembling extant Cebus monkeys, and that further expansion of SMI evolved in the robust lineage to facilitate increased access to varied embedded fallback foods,  An adult female tufted capuchin monkey of the Sapajus lineage using a stone tool and a sandstone anvil to crack a palm nut as her infant hangs on.

<hr>

[Visit Link](http://phys.org/news322982872.html){:target="_blank" rel="noopener"}


