---
layout: post
title: "Spoiler alert: Computer simulations provide preview of next week's eclipse"
date: 2017-10-08
categories:
author: "University of Texas at Austin, Texas Advanced Computing Center"
tags: [Stellar corona,Sun,Space weather,Solar Dynamics Observatory,Weather,Coronal mass ejection,Magnetohydrodynamics,Solar eclipse,Nature,Space science,Astronomy,Science,Solar System,Bodies of the Solar System,Physical sciences,Outer space]
---


Using massive supercomputers, including Stampede2 at the Texas Advanced Computing Center (TACC), Comet at the San Diego Supercomputer Center (SDSC), and NASA's Pleiades supercomputer, the researchers completed a series highly-detailed solar simulations timed to the moment of the eclipse. This model better reproduces the underlying physical processes in the corona and has the potential to produce a more accurate eclipse prediction. It also gives high performance computing researchers who model high energy plasmas the unique ability to test our understanding of magnetohydrodynamics at a scale and environment not possible anywhere else. From corona predictions to solar weather forecasting  Making predictions about the appearance of the corona during an eclipse is a way to test complex, three-dimensional computational models of the sun against visible reality. But doing so means understanding how the visible surface of the sun (the corona) relates to the mass ejections of plasma that cause space weather.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-08/uota-sac081817.php){:target="_blank" rel="noopener"}


