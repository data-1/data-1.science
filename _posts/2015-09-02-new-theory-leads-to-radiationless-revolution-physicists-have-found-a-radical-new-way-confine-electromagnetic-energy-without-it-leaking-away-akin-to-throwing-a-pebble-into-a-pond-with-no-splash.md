---
layout: post
title: "New theory leads to 'radiationless revolution': Physicists have found a radical new way confine electromagnetic energy without it leaking away, akin to throwing a pebble into a pond with no splash"
date: 2015-09-02
categories:
author: Australian National University 
tags: [Electromagnetic radiation,Electron configuration,Quantum mechanics,Matter,Electron,Physics,Electromagnetism,Chemistry,Physical sciences,Applied and interdisciplinary physics,Theoretical physics,Science,Electrical engineering,Atomic molecular and optical physics]
---


Physicists have found a radical new way confine electromagnetic energy without it leaking away, akin to throwing a pebble into a pond with no splash. However, it appears to contradict a fundamental tenet of electrodynamics, that accelerated charges create electromagnetic radiation, said lead researcher Dr Andrey Miroshnichenko from The Australian National University (ANU). This problem has puzzled many people. The fundamental new theory could be used in quantum computers, lead to new laser technology and may even hold the key to understanding how matter itself hangs together. Eventually we realised the Cartesian description was missing the toroidal components, Dr Miroshnichenko said.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150827083417.htm){:target="_blank" rel="noopener"}


