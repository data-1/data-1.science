---
layout: post
title: "What's the difference between open source software and free software?"
date: 2018-02-18
categories:
author: "Scott K Peterson
(Red Hat)"
tags: [Free and open-source software,Open-source software,Gratis versus libre,Free software,Technology,Software licenses,Open content,Intellectual property activism,Intellectual works,Free content,Software engineering,Open-source movement,Computing,Software,Software development,Applied ethics,Technology development,Product management,Information technology,Digital media,Intellectual property law,Copyright law,Free goods and services,Sharing,Information technology management,Software law,Computer science,Software distribution,Criticism of intellectual property,Product development,Copyright licenses]
---


Do you use open source software or free software? Although there are different rules for free software licenses (four freedoms) and open source licenses (Open Source Definition), what is not apparent from those two sets of rules is:  Both terms refer to essentially the same set of licenses and software, and Each term implies different underlying values. Different values? Awkwardly, there is no broadly accepted term that refers to the licenses or the software that's neutral about the values implied by each term. It may be that open source was initially expected to be a neutral term; however, it has developed its own implied values.

<hr>

[Visit Link](https://opensource.com/article/17/11/open-source-or-free-software){:target="_blank" rel="noopener"}


