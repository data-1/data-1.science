---
layout: post
title: "Minerology on Mars points to a cold and icy ancient climate"
date: 2018-06-09
categories:
author: "Purdue University"
tags: [Compact Reconnaissance Imaging Spectrometer for Mars,Mars,Volcano,Earth,Space science,Astronomical objects known since antiquity,Terrestrial planets,Planets of the Solar System,Planetary science,Earth sciences,Outer space,Physical geography,Planets,Astronomy]
---


But climate models of the planet's deep past haven't been able to produce warm enough conditions to allow liquid water on the surface. While a lot of people are using climate models, we're coming at this from a unique perspective - what does the volcanic record of Mars tell us? Ackiss' team hopes their findings can be used as a reference point for other regions on Mars with a volcanic history. Even if Mars was a cold and icy wasteland, these volcanic eruptions interacting with ice sheets could have created a little happy place for microbes to exist, Horgan said. This is the kind of place you'd want to go to understand how life would've survived on Mars during that time.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/pu-mom060718.php){:target="_blank" rel="noopener"}


