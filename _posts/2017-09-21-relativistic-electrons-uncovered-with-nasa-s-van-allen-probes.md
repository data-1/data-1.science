---
layout: post
title: "Relativistic electrons uncovered with NASA's Van Allen Probes"
date: 2017-09-21
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Van Allen radiation belt,Van Allen Probes,Electron,Space science,Spaceflight,Outer space,Science,Nature,Physics,Physical sciences,Astronomy]
---


Now, new observations from NASA's Van Allen Probes mission show that the fastest, most energetic electrons in the inner radiation belt are not present as much of the time as previously thought. But by using a special instrument, the Magnetic Electron and Ion Spectrometer -- MagEIS -- on the Van Allen Probes, the scientists could look at the particles separately for the first time. By separating the electrons from the protons, the scientists could understand which particles were contributing to the population of particles in the inner belt. Given the rarity of the storms, which can inject relativistic electrons into the inner belt, the scientists now understand there to typically be lower levels of radiation there -- a result that has implications for spacecraft flying in the region. For example, we can now investigate under what circumstances these electrons penetrate the inner region and see if more intense geomagnetic storms give electrons that are more intense or more energetic.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/nsfc-reu031517.php){:target="_blank" rel="noopener"}


