---
layout: post
title: "Dead or Alive, Schrödinger's Cat Can Be in 2 Boxes at Once"
date: 2016-06-12
categories:
author: "Tia Ghose"
tags: [Schrdingers cat,Quantum mechanics,Science,Physics,Theoretical physics,Applied and interdisciplinary physics,Scientific theories,Physical sciences]
---


Not only can the quantum cat be alive and dead at the same time — but it can also be in two places at once, new research shows. Cat experiment  The famous paradox was laid out by physicist Erwin Schrödinger in 1935 to elucidate the notion of quantum superposition, the phenomenon in which tiny subatomic particles can be in multiple states at once. The famous Schrodinger's cat can be in two boxes at once, while being dead and alive at the same time. This cat can only be observed in its entirety by opening both boxes, but not one of the boxes. The quality of these things determines once you put a single excitation into the system, how long does it live, or does it die away, Wang told Live Science.

<hr>

[Visit Link](http://www.livescience.com/54890-schrodinger-cat-can-be-in-two-places.html){:target="_blank" rel="noopener"}


