---
layout: post
title: "Forecasting the development of breakthrough technologies to enable novel space missions"
date: 2014-07-03
categories:
author: European Science Foundation 
tags: [European Science Foundation,Research,Science,Branches of science,Technology]
---


The report was not prepared to serve as a definitive guide for very specific technologies to be developed for future space missions but rather to inform on, and flag up, the main developments in various technological and scientific areas outside space that may hold promise for use in the space domain. There is the firm hope that they will be used throughout ESA's Directorates as a novel categorisation of programme concepts and useful red thread to guide the reflexion about future missions and related technological maturation. The report Technological Breakthroughs for Scientific Progress (TECHBREAK) is available online at http://www.esf.org/publications  ###  Note to Editors  For more information, please contact Dr Emmanouil Detsis  +33 (0) 3 88 76 71 54  edetsis[at]esf.org  About The European Science Foundation  The European Science Foundation (ESF) was established in 1974 to provide a common platform for its Member Organisations – the main research funding and research performing organisations in Europe – to advance European research collaboration and explore new directions for research. ESF provides valuable services to the scientific and academic communities – such as peer review, evaluation, career tracking, conferences, implementation of new research support mechanisms and the hosting of high-level expert boards and committees – with the aim of supporting and driving the future of a globally competitive European Research Area. In the context of the current re-organisation of ESF and hand-over of science policy activities to Science Europe, there will be no new Forward Look calls launched in the future.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/esf-ftd070314.php){:target="_blank" rel="noopener"}


