---
layout: post
title: "Support the FSF for thirty more years — Free Software Foundation — Working together for free software"
date: 2016-01-31
categories:
author: "" 
tags: [Free Software Foundation,Open content,Free software,Software engineering,Software development,Technology,Intellectual property activism,Intellectual works,Free content,Applied ethics,Open-source movement,Computing,Software]
---


This post was written by FSF board member and co-founder of CivicActions Henry Poole. While a stand for free software was impersonal thirty years ago, in another thirty years it will be very personal. Operating systems will likely be embedded in our bodies, and the rights to use, study, modify, and share that software to stay alive and connect with others should be a fundamental human right. Stand with us for another thirty years. The FSF has a patron program for businesses that support and use free software.

<hr>

[Visit Link](http://www.fsf.org/blogs/community/support-the-fsf-for-30-more-years){:target="_blank" rel="noopener"}


