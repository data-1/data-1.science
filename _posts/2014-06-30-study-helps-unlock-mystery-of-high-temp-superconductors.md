---
layout: post
title: "Study helps unlock mystery of high-temp superconductors"
date: 2014-06-30
categories:
author: Binghamton University
tags: []
---


This illustration shows the d-orbital shape that Binghamton physicist Michael Lawler and his colleagues observed in the density wave of a high-temperature superconductor. Michael Lawler, assistant professor of physics at Binghamton, is part of an international team of physicists with an ongoing interest in the mysterious pseudogap phase, the phase situated between insulating and superconducting phases in the cuprate phase diagram. (The electron density near each copper atom looks a bit like a daisy in the crystallized pattern.) That's especially surprising because most density waves have an s-orbital structure; their electron density is isotropic. Superconductors conduct electricity without resistance below a certain temperature.

<hr>

[Visit Link](http://www.wired.co.uk/news/archive/2014-06/30/supercooling-organs){:target="_blank" rel="noopener"}


