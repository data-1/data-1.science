---
layout: post
title: "Link seen between seizures and migraines in the brain"
date: 2015-12-04
categories:
author: Penn State 
tags: [Seizure,Brain,Migraine,Neuron,Energy,Ion,Physics,Neuroscience]
---


Seizures and migraines have always been considered separate physiological events in the brain, but now a team of engineers and neuroscientists looking at the brain from a physics viewpoint discovered a link between these and related phenomena. They kept track of the energy required to run a nerve cell, and kept count of the ions passing into and out of the cells. The brain needs a constant supply of oxygen to keep everything running because it has to keep pumping the ions back across cell membranes after each electrical spike. We know that some people get both seizures and migraines, said Schiff. Adding basic conservation principles to the older models immediately demonstrated that spikes, seizures and spreading depression were all part of a spectrum of nerve cell behavior.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/ps-lsb103014.php){:target="_blank" rel="noopener"}


