---
layout: post
title: "Google will be powered by 100% renewable energy by 2018"
date: 2017-10-14
categories:
author: "Greg Beach"
tags: []
---


The corporate giant made quick progress towards meeting its goal, which was set in 2016 and will be fulfilled by 2018. In its 2017 Environmental Report, Google declared that it had pioneered “new energy purchasing models that others can follow” and “helped drive wide-scale global adoption of clean energy.” “We believe Google can build tools to improve people’s lives while reducing our dependence on natural resources and fossil fuels,” said Google executive Urs Hölzle. Continue reading below Our Featured Videos  Google’s rapid shift to clean energy is welcome not only for the influence it may have on other companies but also for its impact on Google’s energy consumption, which was estimated in 2015 to be as large as the city of San Francisco. In line with its sustainability focus, Google has also launched an initiative to add air quality sensors to Google Street View vehicles and plans to change its waste disposal systems to ensure that the company adds nothing to landfills. Join Our Newsletter Receive the latest in global news and designs building a better future.

<hr>

[Visit Link](https://inhabitat.com/google-will-be-powered-by-100-renewable-energy-by-2018){:target="_blank" rel="noopener"}


