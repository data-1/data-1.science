---
layout: post
title: "Vitamin B2 (Riboflavin): Sources, benefits and dosage"
date: 2016-05-31
categories:
author: "Alina Bradford, Ben Biggs"
tags: [Riboflavin,Vitamin,Dose (biochemistry),Folate,Nutrition,Health,Food and drink,Health sciences,Clinical medicine]
---


Vitamin B2 is also important for eye health. For example, riboflavin changes vitamin B6 and folate (vitamin B9) into forms that the body can use. B2 may be important to maintaining a healthy pregnancy diet, as well. A study by the department of neurology of Humboldt University of Berlin (opens in new tab) found that those taking high doses of riboflavin had significantly fewer migraines. A higher dose of 3 mg per day can help to prevent cataracts.

<hr>

[Visit Link](http://www.livescience.com/51966-vitamin-b2-riboflavin.html){:target="_blank" rel="noopener"}


