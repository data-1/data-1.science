---
layout: post
title: "Researchers gain new understanding of Triceratops evolution"
date: 2014-07-01
categories:
author: Marcia Malory
tags: [Triceratops,Hell Creek Formation,Taxa,Evolutionary biology]
---


When the team studied Triceratops skulls' morphology and position in the strata, they found that skulls showing only features of T. horridus appeared only in the lower section, while skulls exhibiting only T. prorsus features appeared only in the upper section. This placement shows that the two species lived in the same place at different times. Explore further Study finds Triceratops, Torosaurus were different stages of one dinosaur  More information: Evolutionary trends in Triceratops from the Hell Creek Formation, Montana, PNAS, www.pnas.org/cgi/doi/10.1073/pnas.1313334111 Evolutionary trends in Triceratops from the Hell Creek Formation, Montana,  Abstract  The placement of over 50 skulls of the well-known horned dinosaur Triceratops within a stratigraphic framework for the Upper Cretaceous Hell Creek Formation (HCF) of Montana reveals the evolutionary transformation of this genus. Specimens referable to the two recognized morphospecies of Triceratops, T. horridus and T. prorsus, are stratigraphically separated within the HCF with the T. prorsus morphology recovered in the upper third of the formation and T. horridus found lower in the formation. Variation among specimens from this critical stratigraphic zone may indicate a branching event in the Triceratops lineage.

<hr>

[Visit Link](http://phys.org/news323415271.html){:target="_blank" rel="noopener"}


