---
layout: post
title: "Physicists set quantum record by using photons to carry messages from electrons almost 2 kilometers apart"
date: 2015-12-22
categories:
author: Bethany Augliere, Stanford University
tags: [Quantum entanglement,Photon,Electron,Quantum mechanics,Spin (physics),Quantum network,Optics,HongOuMandel effect,Optical fiber,Applied and interdisciplinary physics,Physical sciences,Electromagnetic radiation,Atomic molecular and optical physics,Technology,Science,Theoretical physics,Physics]
---


Electrons spin in one of two characteristic directions, and if they are entangled, those two electrons' spins are linked. Scientists can establish a necessary condition of entanglement, called quantum correlation, to correlate photons to electrons, so that the photons can act as the messengers of an electron's spin. Electron spin is the basic unit of a quantum computer, Yu said. To do this, Yu and his team had to make sure that the correlation could be preserved over long distances – a key challenge given that photons have a tendency to change orientation while traveling in optical fibers. If they have different wavelengths, they cannot interfere, Yu said.

<hr>

[Visit Link](http://phys.org/news/2015-11-physicists-quantum-photons-messages-electrons.html){:target="_blank" rel="noopener"}


