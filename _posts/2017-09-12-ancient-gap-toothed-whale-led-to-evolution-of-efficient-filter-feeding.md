---
layout: post
title: "Ancient Gap-Toothed Whale Led to Evolution of Efficient Filter Feeding"
date: 2017-09-12
categories:
author: "Jen Viegas"
tags: [Baleen,Whale,Filter feeder,Predation,Tooth,Baleen whale,Animals]
---


In this case, if the spaces between the whale’s teeth were too large, prey could have escaped. The large teeth (in Corondon) also blocked parts of the sides of the mouth, so they initially might have helped to keep prey inside the mouth while the jaw was closing, Geisler said. (Image credit: Geisler et al. Current Biology)  Geisler and colleagues Robert Boessenecker, Mace Brown, and Brian Beatty suspect that over time the spacing between the whale's teeth became filled with baleen hair. As filter-feeding continued to evolve over millions of years, the baleen got longer, the teeth became smaller, and the former spaces decreased. Since baleen whales evolved from toothed whales, then teeth could form a bridge between whales that used their sharp teeth to snag prey and later toothless whales that had baleen.

<hr>

[Visit Link](https://www.livescience.com/59672-ancient-gap-toothed-whale-discovered.html){:target="_blank" rel="noopener"}


