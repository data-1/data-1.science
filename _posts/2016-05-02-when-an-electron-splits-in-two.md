---
layout: post
title: "When an electron splits in two"
date: 2016-05-02
categories:
author: Lisa Zyga
tags: [Electron,Electric charge,Quantum decoherence,Electric current,Photon,Interferometry,Scientific theories,Electromagnetism,Physical sciences,Science,Applied and interdisciplinary physics,Physics,Quantum mechanics,Theoretical physics]
---


Our work is the first to combine single-electron resolution—which allows us to address the fractionalization process at the elementary scale—with time resolution to directly visualize the fractionalization process. The technique that the researchers used is called the Hong-Ou-Mandel experiment, which can be used to measure the degree of resemblance between two photons, or in this case electron charge pulses, in an interferometer. The researchers first analyzed the propagation of a single electron in the interferometer's outer one-dimensional wire, and then when that electron fractionalized, they could observe the interaction between its two charge pulses in the inner one-dimensional wire. As the researchers explain, when the original electron travels along the outer wire, Coulomb interactions (interactions between charged particles) between excitations in the outer and inner wires produce two types of excitation pairs: two pulses of the same sign (carrying a net charge) and two pulses of opposite signs (which together are neutral). In the future, the researchers plan to perform further experiments with the Hong-Ou-Mandel interferometer in order to better understand why fractionalization leads to electron destruction, and possibly how to suppress fractionalization.

<hr>

[Visit Link](http://phys.org/news350628970.html){:target="_blank" rel="noopener"}


