---
layout: post
title: "Waves on Sun give NASA new insight into space weather forecasting"
date: 2017-10-08
categories:
author: "Mara Johnson-Groh, Nasa'S Goddard Space Flight Center"
tags: [Sun,STEREO,Space weather,Weather,Solar Dynamics Observatory,Solar flare,Coronal mass ejection,Stellar corona,Goddard Space Flight Center,Earth,Outer space,Space plasmas,Astronomical objects,Physical phenomena,Solar System,Astronomy,Space science,Nature,Science,Bodies of the Solar System,Physical sciences,Astronomical objects known since antiquity]
---


Sometimes the sun releases solar flares and coronal mass ejections—huge eruptions of charged particles—which contribute to space weather and can interfere with satellites and telecommunications on Earth. These atmospheric currents are a type of Rossby wave, movements driven by the planet's rotation. Credit: NCAR High Altitude Observatory  These missions allowed the researchers to see the entire sun for over three years, something that would not be possible without the STEREO mission, said Terry Kuchera, STEREO project scientist at NASA's Goddard Space Flight Center in Greenbelt, Maryland. The researchers also found the brightpoints shed light on the solar cycle—the sun's 22-year activity cycle, driven by the constant movement of magnetic material inside the sun. When terrestrial satellites were first used to observe the jet stream on Earth, it allowed huge advances in predictive weather forecasting.

<hr>

[Visit Link](https://phys.org/news/2017-03-sun-nasa-insight-space-weather.html){:target="_blank" rel="noopener"}


