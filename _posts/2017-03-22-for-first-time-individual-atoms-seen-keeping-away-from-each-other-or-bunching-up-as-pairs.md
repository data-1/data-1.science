---
layout: post
title: "For first time, individual atoms seen keeping away from each other or bunching up as pairs"
date: 2017-03-22
categories:
author: "Massachusetts Institute of Technology"
tags: [Superconductivity,Electron,Gas,Atom,Hubbard model,High-temperature superconductivity,Condensed matter physics,Physics,Physical sciences,Applied and interdisciplinary physics,Chemistry,Quantum mechanics,Physical chemistry,Theoretical physics,Science,Phases of matter,Nature]
---


Using a high-resolution microscope, the researchers took images of the cooled atoms residing in the lattice. Atoms as stand-ins for electrons  Today, it is impossible to model the behavior of high‐temperature superconductors, even using the most powerful computers in the world, as the interactions between electrons are very strong. Carving out personal space  Zwierlein's team sought to design an experiment to realize the Fermi-Hubbard model with atoms, in hopes of seeing behavior of ultracold atoms analogous to that of electrons in high-temperature superconductors. At such ultracold temperatures, the atoms slowed down enough for researchers to capture them in images for the first time, as they interacted across the lattice. This, Zwierlein says, is reminiscent of a mechanism proposed for high-temperature superconductivity, in which electron pairs resonating between adjacent lattice sites can zip through the material without friction if there is just the right amount of empty space to let them through.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/09/160915162930.htm){:target="_blank" rel="noopener"}


