---
layout: post
title: "People in leadership positions may sacrifice privacy for security"
date: 2015-10-03
categories:
author: Penn State
tags: [Decision-making,Privacy,Cognition,Cognitive science,Science]
---


In two separate experiments, the researchers examined how people with high-status job assignments evaluated security and privacy and how impulsive or patient they were in making decisions. In a follow-up experiment, people appointed as supervisors also showed a more patient, long-term approach to decision-making, added Grossklags, who worked with Nigel J. Barradale, assistant professor of finance, Copenhagen Business School. Social status shapes how privacy and security issues are settled in the real world, said Grossklags. In the first experiment, they randomly assigned 146 participants roles as either a supervisor or a worker to determine how those assignments changed the way leaders approached security or privacy during a task. As in the previous experiment, the researchers divided the group into high-status supervisors and low-status workers.

<hr>

[Visit Link](http://phys.org/news324720459.html){:target="_blank" rel="noopener"}


