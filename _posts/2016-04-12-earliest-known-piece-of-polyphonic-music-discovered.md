---
layout: post
title: "Earliest known piece of polyphonic music discovered"
date: 2016-04-12
categories:
author: University Of Cambridge
tags: [Polyphony,Music]
---


It is the earliest practical example of a piece of polyphonic music – the term given to music that combines more than one independent melody – ever discovered. The piece was discovered by Giovanni Varelli, a PhD student from St John's College, University of Cambridge, while he was working on an internship at the British Library. Treatises which lay out the theoretical basis for music with two independent vocal parts survive from the early Middle Ages, but until now the earliest known examples of a practical piece written specifically for more than one voice came from a collection known as The Winchester Troper, which dates back to the year 1000. This changes how we understand that development precisely because whoever wrote it was breaking those rules. It shows that music at this time was in a state of flux and development, the conventions were less rules to be followed, than a starting point from which one might explore new compositional paths.

<hr>

[Visit Link](http://phys.org/news338017110.html){:target="_blank" rel="noopener"}


