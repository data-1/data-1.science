---
layout: post
title: "Quantum Criticality in life's proteins (Update)"
date: 2015-07-26
categories:
author: John Hewitt
tags: [Hertz,Orchestrated objective reduction,Quantum critical point,Physical sciences,Physics,Chemistry,Applied and interdisciplinary physics]
---


Quantum criticality in proteins. In suggesting that biomolecules, or at least most of them, are quantum critical conductors, Kauffman and his group are claiming that their electronic properties are precisely tuned to the transition point between a metal and an insulator. Photosynthesis shows us that quantum coherence occurs among pi resonance rings within such non-polar interiors of proteins at ambient temperatures. Which proteins? Mudur GS (2015) 'Deep inside cells, a clue to the mind' Calcutta Telegraph www.telegraphindia.com/1150403 … 399.jsp#.VSfJ3EtKh4M  ————————  Explore further Evidence mounts for quantum criticality theory  More information: Quantum Criticality at the Origin of Life, arXiv:1502.06880 [cond-mat.dis-nn] Quantum Criticality at the Origin of Life, arXiv:1502.06880 [cond-mat.dis-nn] arxiv.org/abs/1502.06880  Abstract  Why life persists at the edge of chaos is a question at the very heart of evolution.

<hr>

[Visit Link](http://phys.org/news348144521.html){:target="_blank" rel="noopener"}


