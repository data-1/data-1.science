---
layout: post
title: "Exposed to space and back on Earth"
date: 2016-07-06
categories:
author: ""
tags: [EXPOSE,Aerospace,Space exploration,Science,Space programs,Spacecraft,Spaceflight,Outer space,Astronautics,Space science,Flight,Astronomy,Human spaceflight,Life in space]
---


Science & Exploration Exposed to space and back on Earth 05/07/2016 11303 views 118 likes  In the excitement of watching Tim Peake, Yuri Malenchenko and Tim Kopra land on Earth on 18 June after 186 days in space, all attention was focused on the astronauts and their bumpy ride. Most have now been distributed to the 30 researchers from 11 laboratories in the Netherlands, Italy, France and USA, who are eager to probe how they have reacted to living in space. Expose-R2 samples back on Earth A ground-based set of control samples will continue running for another two months, subjected to much the same conditions as their counterparts in space but without the cosmic radiation and weightlessness, which are unique to the Space Station. ESA has a long history of ‘exposure’ experiments. This third sortie for Expose is the last for now, but many new research proposals have been submitted for review to continue exploring the limits of life in our Solar System and how spaceflight can change organic chemicals.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Principia/Exposed_to_space_and_back_on_Earth){:target="_blank" rel="noopener"}


