---
layout: post
title: "Atomic clocks prove the stability of a fundamental physical constant"
date: 2016-04-11
categories:
author: Physikalisch-Technische Bundesanstalt
tags: [Atomic clock,Electron,Proton,Physikalisch-Technische Bundesanstalt,Mass,Physics,Science,Physical sciences,Metrology]
---


Comparisons between atomic clocks with caesium and ytterbium, respectively, confirm the constancy of the mass ratio of protons to electrons. Are the fundamental constants really constant? To obtain this result, physicists from PTB compared caesium and ytterbium atomic clocks with each other for 7 years. This method was used at PTB to check the constancy of an essential physical quantity – the mass ratio of protons to electrons – by comparing an optical clock with a trapped ytterbium ion and caesium atomic clocks. 5 billion years), this means a change in this fundamental constant of less than one part in a million, so that it can still be considered a universal and stable quantity.

<hr>

[Visit Link](http://phys.org/news335524895.html){:target="_blank" rel="noopener"}


