---
layout: post
title: "Reinterpreting dark matter"
date: 2014-07-07
categories:
author: University of the Basque Country  
tags: [Dark matter,Matter,Universe,Cold dark matter,Physics,Galaxy,Astrophysics,Theoretical physics,Space science,Cosmology,Physical cosmology,Physical sciences,Astronomy,Science,Nature]
---


Research published in Nature Physics opens up the possibility that it could be regarded as a very cold quantum fluid governing the formation of the structure across the whole universe  This news release is available in Spanish. This theory can be used to suggest that all the galaxies in this context should have at their centre large stationary waves of dark matter called solitons, which would explain the puzzling cores observed in common dwarf galaxies. The research also makes it possible to predict that galaxies are formed relatively late in this context in comparison with the interpretation of standard particles of cold dark matter. The results are very promising as they open up the possibility that dark matter could be regarded as a very cold quantum fluid that governs the formation of the structure across the whole Universe. This is not Thomas Broadhurst's first publication in the prestigious journal Nature.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/uotb-rdm070214.php){:target="_blank" rel="noopener"}


