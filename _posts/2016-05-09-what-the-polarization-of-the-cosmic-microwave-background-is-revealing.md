---
layout: post
title: "What the polarization of the cosmic microwave background is revealing"
date: 2016-05-09
categories:
author: Johannes Hubmayr, National Institute Of Standards
tags: [Cosmic microwave background,Chronology of the universe,Physics,Physical sciences,Astronomy,Science,Astrophysics,Nature,Space science,Physical cosmology,Electromagnetic radiation,Cosmology]
---


Polarization of the Cosmic Microwave Background. The intensity anisotropy largely shaped our current `standard model' of cosmology, and constrained the six numbers that parameterize this model, such as the age of the universe and the average density of matter that fills space, says Hubmayr. But the new NIST detector design is not only more sensitive, but also multi-chroic − that is, it can measure the properties of photons at two different frequencies at the same time in the same pixel. Atacama Cosmology Telescope. This array is the first of its kind, Hubmayr says.

<hr>

[Visit Link](http://phys.org/news354268757.html){:target="_blank" rel="noopener"}


