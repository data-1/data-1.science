---
layout: post
title: "Five myths about gravitational waves"
date: 2015-09-08
categories:
author: Siri Chongchitnan, The Conversation
tags: [Gravitational wave,Cosmic microwave background,BICEP and Keck Array,Gravitational-wave observatory,Big Bang,Inflation (cosmology),Universe,General relativity,Space science,Astronomy,Physics,Astrophysics,Physical sciences,Physical cosmology,Science,Cosmology,Physical phenomena]
---


Credit: NASA/ESA/wikimedia  The scientists behind the BICEP2 (Background Imaging of Cosmic Extragalactic Polarization) telescope, last year made an extraordinary claim that they had detected gravitational waves, which are ripples in space-time. Scientists have therefore been trying to demonstrate the existence of gravitational waves by looking at how nearby objects are affected. The pulsar provided indirect evidence of gravitational waves as it was losing energy at a rate which had been predicted by the general theory of relativity (the waves themselves were not seen). They will be able to measure CMB to scales beyond the reach of Planck, and would have learned valuable lessons from BICEP in the modelling of dust and other contaminants. We just need one experiment to detect them  Strong statistical evidence for gravitational waves will certainly require more than one experiment.

<hr>

[Visit Link](http://phys.org/news/2015-09-myths-gravitational.html){:target="_blank" rel="noopener"}


