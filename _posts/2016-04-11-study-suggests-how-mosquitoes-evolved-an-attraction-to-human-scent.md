---
layout: post
title: "Study suggests how mosquitoes evolved an attraction to human scent"
date: 2016-04-11
categories:
author: Rockefeller University
tags: [Mosquito,Odor,Leslie B Vosshall,Insect,Human,Aedes aegypti,Evolution,Sense of smell,Animals]
---


Those collected in the forest were black and tended to prefer the laboratory guinea pigs. Researchers report that the yellow fever mosquito sustains its taste for human blood thanks in part to a genetic tweak that makes it more sensitive to human odor. Credit: Carolyn McBride, Department of Ecology and Evolutionary Biology and the Princeton Neuroscience Institute  We knew that these mosquitoes had evolved a love for the way we smell, Vosshall says. The researchers came up with one match, a chemical called sulcatone that was not found in pantyhose worn by guinea pigs. Credit: Carolyn McBride, Department of Ecology and Evolutionary Biology and the Princeton Neuroscience Institute  Sulcatone is an important odor that gives humans our distinctive scent, but there are likely other odors and other genes that help explain mosquitoes' attraction to humans.

<hr>

[Visit Link](http://phys.org/news335009422.html){:target="_blank" rel="noopener"}


