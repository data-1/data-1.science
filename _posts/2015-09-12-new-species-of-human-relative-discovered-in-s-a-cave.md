---
layout: post
title: "New species of human relative discovered in S.A. cave"
date: 2015-09-12
categories:
author: University of the Witwatersrand 
tags: [Homo naledi,Rising Star Cave]
---


The discovery of a new species of human relative was announced today, 10 September 2015, by the University of the Witwatersrand (Wits University), the National Geographic Society and the Department of Science and Technology (DST) and the National Research Foundation of South Africa (NRF). So far, the team has recovered parts of at least 15 individuals of the same species, a small fraction of the fossils believed to remain in the chamber. The fossils -- which consist of infants, children, adults and elderly individuals -- were found in a room deep underground that the team named the Dinaledi Chamber, or Chamber of Stars. ###  Reference:  The papers Homo naledi, a new species of the genus Homo from the Dinaledi Chamber, South Africa and Geological and taphonomic context for the new hominin species Homo naledi from the Dinaledi Chamber, South Africa can be freely accessed online at http://dx.doi.org/10.7554/eLife.09560 and http://dx.doi.org/10.7554/eLife.09561. Funding:  The research was supported by Wits University, the National Geographic Society and South African DST/NRF.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/uotw-ns091015.php){:target="_blank" rel="noopener"}


