---
layout: post
title: "NASA moon data provides more accurate 2017 eclipse path"
date: 2017-09-23
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Moon,Lunar Reconnaissance Orbiter,Solar eclipse,Goddard Space Flight Center,Digital elevation model,Shadow,Earth,NASA,Topography,Eclipse,Map,Planetary science,Bodies of the Solar System,Science,Solar System,Astronomical objects known since antiquity,Physical sciences,Planets of the Solar System,Space science,Outer space,Astronomy]
---


The path of this shadow, also known as the path of totality, is where observers will see the moon completely cover the sun. For slightly more accurate maps, people use elevation tables and plots of the lunar limb -- the edge of the visible surface of the moon as seen from Earth. A new look at an ancient phenomenon  https://youtu.be/MJgXaqW3md8  Using LRO elevation maps, NASA visualizer Ernie Wright at Goddard Space Flight Center in Greenbelt, Maryland, created a continuously varying lunar limb profile as the moon's shadow passes over the United States as it will during the upcoming eclipse. We've known for a while now about the effects of the lunar limb and the elevation of observers on the Earth, but this is the first time we've really seen it in this way, Wright said. The total solar eclipse on Monday, Aug. 21, 2017 will cross the continental United States beginning in Oregon and ending in South Carolina.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/nsfc-nmd010517.php){:target="_blank" rel="noopener"}


