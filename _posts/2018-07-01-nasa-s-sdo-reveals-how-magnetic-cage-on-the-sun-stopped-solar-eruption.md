---
layout: post
title: "NASA's SDO reveals how magnetic cage on the Sun stopped solar eruption"
date: 2018-07-01
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Sun,Solar flare,Solar Dynamics Observatory,Coronal mass ejection,Stellar corona,Nature,Space science,Astronomy,Space plasmas,Bodies of the Solar System,Stellar astronomy,Solar System,Astronomical objects known since antiquity,Astrophysics,Physical sciences,Ancient astronomy,G-type main-sequence stars,Stars,Science,Physics,Plasma physics,Outer space,Physical phenomena,Astronomical objects]
---


The work highlights the role of the Sun's magnetic landscape, or topology, in the development of solar eruptions that can trigger space weather events around Earth. Using data from NASA's Solar Dynamics Observatory, or SDO, the scientists examined an October 2014 Jupiter-sized sunspot group, an area of complex magnetic fields, often the site of solar activity. The model reveals a battle between two key magnetic structures: a twisted magnetic rope -- known to be associated with the onset of CMEs -- and a dense cage of magnetic fields overlying the rope. But the rope never erupted from the surface: Their model demonstrates it didn't have enough energy to break through the cage. By changing the conditions of the cage in their model, the scientists found that if the cage were weaker that day, a major CME would have erupted on Oct. 24, 2014.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/nsfc-nsr022318.php){:target="_blank" rel="noopener"}


