---
layout: post
title: "Formula calculates thickness of bombproof concrete"
date: 2015-12-02
categories:
author: Fraunhofer-Gesellschaft 
tags: [Shock tube,Pipe (fluid conveyance),Concrete,Applied and interdisciplinary physics,Technology]
---


We can simulate detonations of different blasting forces – from 100 to 2,500 kilograms TNT at distances from 35 to 50 meters from buildings. And that's without even having to use explosives, says Stolz. With conventional concrete, the impact pressure ripped out parts of the sample concrete wall, which failed almost instantly, while the ductile and more flexible security version of the concrete merely deformed. There was no debris, and the material remained intact, says Stolz. Our formula allows us to calculate the exact thickness of the concrete required to meet the safety considerations posed by such a special building, says Stolz.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/f-fct072414.php){:target="_blank" rel="noopener"}


