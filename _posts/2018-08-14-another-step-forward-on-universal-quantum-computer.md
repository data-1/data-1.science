---
layout: post
title: "Another step forward on universal quantum computer"
date: 2018-08-14
categories:
author: "Yokohama National University"
tags: [Nitrogen-vacancy center,Quantum logic gate,Quantum computing,Quantum mechanics,Physics,Theoretical physics,Applied and interdisciplinary physics,Science,Quantum information science,Applied mathematics]
---


Yokohama, Japan - Researchers have demonstrated holonomic quantum gates under zero-magnetic field at room temperature, which will enable the realization of fast and fault-tolerant universal quantum computers. Researchers are currently working on the next step in quantum computing: building a universal quantum computer. The geometric phase gate or holonomic quantum gate has been experimentally demonstrated in several quantum systems including nitrogen-vacancy (NV) centers in diamond. To avoid unwanted interference, we used a degenerate subspace of the triplet spin qutrit to form an ideal logical qubit, which we call a geometric spin qubit, in an NV center. The scheme renders a purely holonomic gate without requiring an energy gap, which would have induced dynamic phase interference to degrade the gate fidelity, and thus enables precise and fast control over long-lived quantum memories, for realizing quantum repeaters interfacing between universal quantum computers and secure communication networks.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/ynu-asf081018.php){:target="_blank" rel="noopener"}


