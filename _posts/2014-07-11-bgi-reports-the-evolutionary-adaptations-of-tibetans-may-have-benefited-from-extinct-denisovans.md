---
layout: post
title: "BGI reports the evolutionary adaptations of Tibetans may have benefited from extinct denisovans"
date: 2014-07-11
categories:
author: BGI Shenzhen 
tags: [BGI Group,Denisovan,Interbreeding between archaic and modern humans,Biology,Genetics]
---


Shenzhen, July 2, 2014---An international team, led by researchers from BGI and University of California, presented their latest significant finding that the altitude adaptation in Tibet might be caused by the introgression of DNA from extinct Denisovans or Denisovan-related individuals into humans. Other important collaborators of this study include the scientists from The People's Hospital of Lhasa, South China University of Technology, among others. Recently, the genetic studies on Tibetans' adaptation to high altitude indicated that a hypoxia pathway gene, EPAS1, had the most extreme signature of positive selection in Tibetans, and was shown to be associated with differences in hemoglobin concentration at high altitude. Moreover, this gene-flow from Denisovan to Tibetans may facilitate Tibetans to adapt the harsh high-altitude environments, which sheds new light on the gene-based study on human evolution and adaptation. The finding of Tibetans's selected EPAS1 haplotype in Denisovans not only demonstrates the possibility of ancient gene-flow from Denisovans- or -like population to ancestors of Tibetans , but also shows the importance of such events in local adaptation of modern humans   About BGI  BGI was founded in 1999 with the mission of being a premier scientific partner to the global research community.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/bs-brt070114.php){:target="_blank" rel="noopener"}


