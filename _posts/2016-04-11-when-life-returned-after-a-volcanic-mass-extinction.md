---
layout: post
title: "When life returned after a volcanic mass extinction"
date: 2016-04-11
categories:
author: University of Utah
tags: [Pangaea,Marine sediment,Nature,Physical geography,Earth sciences,Geology,Natural environment,Oceanography]
---


A worldwide mass extinction 201.5 million years ago wiped out 60 percent of sea life, including coral reefs and shelled marine animals. On land, the dead included many plants as well as giant reptiles that competed with the earliest dinosaurs. The mass extinction apparently was caused by huge carbon dioxide emissions from gargantuan volcanic eruptions in the center of the supercontinent Pangea, which eventually broke into today's modern continents. The eruptions were in what is named the Central Atlantic Magmatic Province because the Atlantic Ocean eventually formed there as Pangea broke apart. In the April 6 issue of the journal Nature Communications, a new study used fossils and mercury isotopes from volcanic gas deposited in ancient proto-Pacific Ocean sediment deposits in Nevada to determine when life recovered following the mass extinction at the end of the Triassic Period.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/04/160408112409.htm){:target="_blank" rel="noopener"}


