---
layout: post
title: "Water and land plants control their photosynthesis similarly, regardless of their origin"
date: 2018-08-12
categories:
author: "Technische Universität Kaiserslautern"
tags: [Plant,Photosynthesis,Translation (biology),Protein,Genetics,RNA,Ribosome,Gene,Chloroplast,Biology,Nature,Chemistry,Life sciences,Biotechnology,Biochemistry,Molecular biology]
---


The transcript serves as a kind of blueprint with the help of which large molecule complexes, the so-called ribosomes, assemble proteins from individual amino acids, says lead author Dr. Raphael Trösch, who is part of the Willmund research group on the Kaiserslautern campus. This process is also known as translation. For all three plant species, we found that the same amounts of proteins are formed during translation that play a role in photosynthesis, explains Zoschke. However, the researchers have also found that there are differences in the molecular processes that occur before and during translation. Nevertheless, over the course of evolution, the different plants have developed mechanisms to produce the same photosynthesis components in equal amounts during translation, says Willmund.

<hr>

[Visit Link](https://phys.org/news/2018-08-photosynthesis-similarly.html){:target="_blank" rel="noopener"}


