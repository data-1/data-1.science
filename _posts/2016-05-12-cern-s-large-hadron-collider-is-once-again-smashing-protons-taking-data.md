---
layout: post
title: "CERN's Large Hadron Collider is once again smashing protons, taking data"
date: 2016-05-12
categories:
author: Southern Methodist University
tags: [Large Hadron Collider,ATLAS experiment,Particle physics,Higgs boson,Standard Model,CERN,Physics,Science,Physical sciences,Applied and interdisciplinary physics,Nature]
---


SMU physicists work on the LHC's ATLAS experiment. As part of Run 2, physicists on the Large Hadron Collider's experiments are analyzing new proton collision data to unravel the structure of the Higgs. SMU physicists also study Higgs-boson interactions with the most massive known particle, the top-quark, said Robert Kehoe, SMU associate professor. This is the second year the LHC will run at a collision energy of 13 TeV. With the 2016 data the experiments will be able to perform improved measurements of the Higgs boson and other known particles and phenomena, and look for new physics with an increased discovery potential.

<hr>

[Visit Link](http://phys.org/news/2016-05-cern-large-hadron-collider-protons.html){:target="_blank" rel="noopener"}


