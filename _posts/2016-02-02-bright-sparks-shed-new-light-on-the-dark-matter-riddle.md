---
layout: post
title: "Bright sparks shed new light on the dark matter riddle"
date: 2016-02-02
categories:
author: Springer
tags: [Dark matter,Matter,Elementary particle,Weakly interacting massive particles,Astronomy,Physics,Physical sciences,Nature,Particle physics,Science,Theoretical physics,Astrophysics,Physical cosmology,Applied and interdisciplinary physics,Cosmology]
---


The origin of matter in the universe has puzzled physicists for generations. Until quite recently, the so-called WIMP - Weakly Interacting Massive Particle - was the preferred candidate for a new elementary particle to explain dark matter. The detection method is based on the fact that the scattering would heat up a calcium tungstate (CaWO4) crystal. C 76: 25, DOI 10.1140/epjc/s10052-016-3877-3  Results on low mass WIMPs using an upgraded CRESST-II detector. G. Angloher , A. Bento, C. Bucci, L. Canonica, A. Erb, F. von Feilitzsch, N. Ferreiro Iachellini , P. Gorla3, A. Gütlein, D. Hauff , P. Huff, J. Jochum, M. Kiefer , C. Kister , H. Kluck, H. Kraus, J.-C. Lanfranchi, J. Loebell, A. Münster, F. Petricca, W. Potzel, F. Pröbst, F. Reindl, S. Roth, K. Rottler, C. Sailer, K. Schäffner, J. Schieck, J. Schmaler, S. Scholl, S. Schönert, W. Seidel, M. von Sivers, L. Stodolsky, C. Strandhagen, R. Strauss, A. Tanzke, M. Uffinger, A. Ulrich, I. Usherov, S. Wawoczny, M. Willers, M. Wüstrich, A. Zöller (2014), Eur.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/s-bss020116.php){:target="_blank" rel="noopener"}


