---
layout: post
title: "Securing mount points on Linux"
date: 2015-09-30
categories:
author: Michael Boelen
tags: [Device file,Computers,Software development,Information retrieval,Data,Computing,Technology,System software,Computer architecture,Information technology management,Operating system technology,Software,Data management,Utility software,Computer data,Computer science,Computer engineering,Storage software,Information technology,Information Age,Software engineering]
---


By default the mount options are not focused on security, which gives us a room to further improve hardening of the system. Via mount options we can apply additional security controls to protect our data. Mount points  Let’s have a look at our /etc/fstab file. rw = read write  auto = mount automatically  nouser = do not allow a user to mount the file system  async = asynchronous saving of data, to improve performance  Since this is a virtual file system, which has no user data or binaries stored, we leave it with the defaults option. Useful for: /boot /dev/shm /home /tmp /var and data partitions  Not suitable for: root (/)  Apply system hardening  To harden mount points, replace the defaults entry and add the related options to the related field.

<hr>

[Visit Link](http://linux-audit.com/securing-mount-points-on-linux/){:target="_blank" rel="noopener"}


