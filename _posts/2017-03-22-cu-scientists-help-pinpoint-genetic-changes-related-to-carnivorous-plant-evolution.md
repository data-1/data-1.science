---
layout: post
title: "CU scientists help pinpoint genetic changes related to carnivorous plant evolution"
date: 2017-03-22
categories:
author: "University of Colorado Anschutz Medical Campus"
tags: [Carnivorous plant,Anschutz Medical Campus,Pitcher plant,Evolution,Convergent evolution,Genetics,Biology,Life sciences]
---


AURORA, Colo. (Feb. 6, 2017) - A team of scientists that includes researchers from the University of Colorado Anschutz Medical Campus have sequenced the genome of the Australian pitcher plant and discovered a key to the mystery of how those plants became predatory. Australian pitcher plants evolved to include carnivorous pitcher leaves and non-carnivorous flat leaves and that feature allowed the scientists to analyze the genetic basis for carnivory by comparing the two types of leaves. The physiological convergence of leaf form and function is associated with convergent evolution in enzyme repertoires, gene expression patterns, and most strikingly, amino acid substitutions. Excess convergence is strong evidence of natural selection, especially when found on multiple scales, said Pollock, who is a coauthor of the article along with graduate Structural Biology and Biochemistry PhD student Stephen Pollard. About the University of Colorado School of Medicine  Faculty at the University of Colorado School of Medicine work to advance science and improve care.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/uoca-csh020317.php){:target="_blank" rel="noopener"}


