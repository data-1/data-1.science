---
layout: post
title: "Galileo liftoff replay"
date: 2015-09-03
categories:
author: "$author"   
tags: [Outer space,European Space Agency,Space traffic management,Space agencies,Aerospace,European space programmes,Spaceflight technology,Rocketry,Space programs,Spacecraft,Flight,Space vehicles,Astronautics,Spaceflight,Space industry,Space access,Space launch vehicles,Space exploration,Space policy of the European Union,Space organizations,Pan-European scientific organizations,Rockets and missiles,Space science,Space policy]
---


The liftoff of Soyuz flight VS11 from Europe’s Spaceport in French Guiana took place as scheduled on 21:46:18 GMT (22:46:18 CET) on Friday 27 March 2015. The launcher was carrying Europe’s seventh and eighth Galileo navigation satellites, due to separate from their Fregat upper stage into their assigned orbit on 3 h 47 min after lift-off.

<hr>

[Visit Link](http://www.esa.int/spaceinvideos/Videos/2015/03/Galileo_liftoff_replay){:target="_blank" rel="noopener"}


