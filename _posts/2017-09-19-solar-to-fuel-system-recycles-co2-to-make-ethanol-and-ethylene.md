---
layout: post
title: "Solar-to-fuel system recycles CO2 to make ethanol and ethylene"
date: 2017-09-19
categories:
author: "DOE/Lawrence Berkeley National Laboratory"
tags: [Ethanol,Hydrocarbon,Carbon dioxide,Fuel,Carbon,Oxide,Lawrence Berkeley National Laboratory,Artificial photosynthesis,Materials,Chemical substances,Physical sciences,Nature,Technology,Energy,Physical chemistry,Chemistry]
---


Having largely achieved that task using several types of devices, JCAP scientists doing solar-driven carbon dioxide reduction began setting their sights on achieving efficiencies similar to those demonstrated for water splitting, considered by many to be the next big challenge in artificial photosynthesis. In a study published today, they describe a new catalyst that can achieve carbon dioxide to multicarbon conversion using record-low inputs of energy. Among the new components developed by the researchers are a copper-silver nanocoral cathode, which reduces the carbon dioxide to hydrocarbons and oxygenates, and an iridium oxide nanotube anode, which oxidizes the water and creates oxygen. Bullock works in the lab of study co-author Ali Javey, Berkeley Lab senior faculty scientist and a UC Berkeley professor of electrical engineering and computer sciences. The University of California manages Berkeley Lab for the U.S. Department of Energy's Office of Science.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-09/dbnl-ssr091817.php){:target="_blank" rel="noopener"}


