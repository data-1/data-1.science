---
layout: post
title: "Degenerating neurons respond to gene therapy treatment for Alzheimer's disease: Postmortem brain studies suggest nerve growth factor safely triggered functional cell growth"
date: 2016-06-14
categories:
author: "University of California - San Diego"
tags: [Nerve growth factor,Neurodegenerative disease,Alzheimers disease,Neurology,Clinical trial,Neuron,Nerve,Brain,Gene therapy,Diseases and disorders,Neuroscience,Medicine,Clinical medicine,Health,Health sciences,Medical specialties,Nervous system,Health care,Life sciences]
---


Degenerating neurons in patients with Alzheimer's disease (AD) measurably responded to an experimental gene therapy in which nerve growth factor (NGF) was injected into their brains, report researchers at University of California, San Diego School of Medicine in the current issue of JAMA Neurology. The findings are derived from postmortem analyses of 10 patients who participated in phase I clinical trials launched in 2001 to assess whether injected NGF -- a protein essential to cellular growth, maintenance and survival -- might safely slow or prevent neuronal degeneration in patients with AD. The published findings come from AD patients who participated in safety trials from March 2001 to October 2012 at UC San Diego Medical Center. This means that growth factors as a class consistently result in activation of dying cells in human neurodegenerative disorders. Currently, there is no effective treatment or cure for AD.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150827154415.htm){:target="_blank" rel="noopener"}


