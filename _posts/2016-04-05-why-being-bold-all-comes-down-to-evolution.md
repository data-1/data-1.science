---
layout: post
title: "Why being bold all comes down to evolution"
date: 2016-04-05
categories:
author: Matthew Creasey, University Of Exeter, The Conversation
tags: [Evolution,Charles Blondin,Aggression,Risk,Species,Human,Behavioural sciences]
---


In the same year as Blondin's first Niagara crossing, Charles Darwin published On The Origin of Species by Natural Selection. The difference illustrates how individuals have remarkably different attitudes to risk. One answer is that in some cases, bolder individuals have higher reproductive success. A recent debate over whether young children should be allowed to tackle while playing rugby at school provoked a huge and mixed response from the public. In this case, the researchers suggest that individuals who are more exposed to risk cope with this by responding more aggressively and so more boldly to threats.

<hr>

[Visit Link](http://phys.org/news/2016-03-bold-evolution.html){:target="_blank" rel="noopener"}


