---
layout: post
title: "Impact of climate change on microbial biodiversity"
date: 2017-03-17
categories:
author: "University of Helsinki"
tags: [Biodiversity,Natural environment,Climate change,Effects of climate change,Climate,Physical geography,Climate variability and change,Earth phenomena,Environment,Environmental social science,Global natural environment,Earth sciences,Global environmental issues,Environmental science,Human impact on the environment,Nature,Environmental issues,Ecology,Systems ecology,Biogeochemistry]
---


Climate change affects biodiversity most strongly in the most natural environments, as well as the most nutrient enriched environments -- This means that these extremes are most susceptible to future changes in temperatures  The scientists discovered that climate change affects biodiversity most strongly in the most natural environments, as well as the most nutrient enriched environments. The results are just published in the highly regarded journal Nature Communications. The results indicate that the bacteria in elevated tropical areas are similar to e.g. those in arctic areas. The typically austere, i.e. nutrient-poor, waters in the north, for example, are extremely susceptible to temperature variations, and as the climate warms up, species that have adapted to the cold will decline. ###  More details:  Associate Professor Janne Soininen  Department of Geosciences and Geography, University of Helsinki  janne.soininen@helsinki.fi  050-3185245  Researcher Jianjun Wang  Department of Geosciences and Geography, University of Helsinki  jianjun.j.wang@helsinki.fi  Video: Climate and nutrients affect biodiversity of bacteria https://youtu.be/7XyZdG7z84o

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-12/uoh-ioc122116.php){:target="_blank" rel="noopener"}


