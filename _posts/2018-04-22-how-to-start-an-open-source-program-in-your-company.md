---
layout: post
title: "How to start an open source program in your company"
date: 2018-04-22
categories:
author: "Chris Aniszczyk"
tags: [Open source,Policy,Business,Technology,Computing,Management,Economy]
---


Many internet-scale companies, including Google, Facebook, and Twitter, have established formal open source programs (sometimes referred to as open source program offices, or OSPOs for short), a designated place where open source consumption and production is supported inside a company. How to start an open source program  Although each open source office will be customized to a specific organization’s needs, there are standard steps that every company goes through. Engineering-driven organizations may choose to place the office in an engineering department, especially if the focus of the office is to improve developer productivity. Engineering-driven organizations may choose to place the office in an engineering department, especially if the focus of the office is to improve developer productivity. The TODO Group offers examples of other open source policies organizations can use as resources.

<hr>

[Visit Link](https://opensource.com/article/18/1/how-start-open-source-program-your-company){:target="_blank" rel="noopener"}


