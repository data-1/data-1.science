---
layout: post
title: "Satellite Data Gives Scientists New Look at Underground Water Sources"
date: 2014-06-22
categories:
author: Science World Report
tags: [Groundwater,Interferometric synthetic-aperture radar,Water,Hydroxides,Earth sciences,Hydrology,Liquid dielectrics,Physical geography,Refrigerants,Lubricants]
---


Groundwater is important to our freshwater systems. That's why scientists decided to see if there was another way to monitor water levels. This allowed them to compile water-level measurements for confined aquifers at three sampling locations that matched the data from nearby monitoring wells. As satellite data improves, scientists and monitors could use the information in order to estimate how much water is available in a certain region in order to better manage the resource. The findings are published in the journal Water Resources Research.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15510/20140618/satellite-data-gives-scientists-new-look-underground-water-sources.htm){:target="_blank" rel="noopener"}


