---
layout: post
title: "Astronomers discover new distant dwarf planet beyond Neptune"
date: 2016-07-21
categories:
author: "University of British Columbia"
tags: [Planet,Solar System,Dwarf planet,Outer Solar System Origins Survey,Neptune,Planets beyond Neptune,Sun,Astronomy,Orbit,(523794) 2015 RR245,Pluto,Trans-Neptunian object,Substellar objects,Local Interstellar Cloud,Science,Planets,Bodies of the Solar System,Physical sciences,Space science,Outer space,Planetary science,Astronomical objects,Planemos]
---


An international team of astronomers including researchers from the University of British Columbia has discovered a new dwarf planet orbiting in the disk of small icy worlds beyond Neptune. The new object is about 700 km in diameter -- roughly one-and-a-half times the size of Vancouver Island -- and has one of the largest orbits for a dwarf planet. Designated 2015 RR245 by the International Astronomical Union's Minor Planet Center, it was found using the Canada-France-Hawaii Telescope on Maunakea, Hawaii, as part of the ongoing Outer Solar System Origins Survey (OSSOS). RR245 is one of the few dwarf planets that survived to the present day, along with Pluto and Eris, the largest known dwarf planets. RR245 is the largest discovery and the only dwarf planet found by OSSOS, which has discovered more than five hundred new trans-Neptunian objects.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/07/160711150825.htm){:target="_blank" rel="noopener"}


