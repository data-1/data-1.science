---
layout: post
title: "First gravitational waves form after 10 million years"
date: 2017-09-21
categories:
author: "University of Zurich"
tags: [Laser Interferometer Space Antenna,Gravitational wave,Black hole,Astronomy,Theory of relativity,Gravity,General relativity,Astronomical objects,Cosmology,Celestial mechanics,Outer space,Physical cosmology,Science,Physics,Physical sciences,Astrophysics,Space science]
---


And the research on gravitational waves - and thus the origin of the universe - continues: From 2034 three satellites are to be launched into space in a project headed by the European Space Agency (ESA) to measure gravitational waves at even lower frequency ranges from space using the Evolved Laser Interferometer Space Antenna (eLISA). An international team of astrophysicists from the University of Zurich, the Institute of Space Technology Islamabad, the University of Heidelberg and the Chinese Academy of Sciences has now calculated this for the first time using an extensive simulation. The result is surprising, explains Lucio Mayer from the Institute for Computational Science of the University of Zurich: The merging of the two black holes already triggered the first gravitational waves after 10 million years - around 100 times faster than previously assumed. Year-long supercomputer calculation  The computer simulation, which took more than a year, was conducted in China, Zurich and Heidelberg. The who's who of gravitational astrophysics, high-ranking experts from the European and American space authorities and specialists from the space mission LISA will all convene on the Irchel campus to report on the extensive present tests on the LISA pathfinder mission's technology, which have by far exceeded all expectations until now.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-09/uoz-fgw090516.php){:target="_blank" rel="noopener"}


