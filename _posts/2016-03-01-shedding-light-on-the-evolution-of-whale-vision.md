---
layout: post
title: "Shedding light on the evolution of whale vision"
date: 2016-03-01
categories:
author: University Of Toronto
tags: [Evolution,Whale,Mutation,Protein,Eye,Gene,Natural selection,Biology,Adaptation,Life sciences,Biotechnology,Nature]
---


Researchers from the Department of Ecology and Evolutionary Biology at the University of Toronto used a combination of statistical and experimental methods to determine how the gene coding for the visual protein known as rhodopsin has evolved differently in whales and dolphins relative to terrestrial mammals. By using killer whale rhodopsin as an experimental model, their results show that not only is the rhodopsin gene under natural selection pressure in whales, but also that naturally selected mutations in the gene confer greater sensitivity towards blue-shifted underwater light. The killer whale is blue-shifted compared to the cow (a terrestrial mammal), and this difference is accounted for by mutations in the gene sequence. But the mutation experiments show us what those evolutionary changes actually do to the protein's function – that they shift rhodopsin's sensitivity to different parts of the light spectrum. The study's results, published in Molecular Biology and Evolution, show computational and experimental evidence of natural selection in whale rhodopsin for vision in underwater environments, illuminating one facet of how these remarkable creatures have adapted to life in the sea.

<hr>

[Visit Link](http://phys.org/news/2016-02-evolution-whale-vision.html){:target="_blank" rel="noopener"}


