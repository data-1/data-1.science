---
layout: post
title: "New strategies, tools offered for genome editing: Bioengineer Gang Bao and team explore CRISPR-Cas9 alternatives"
date: 2016-03-01
categories:
author: Rice University
tags: [CRISPR gene editing,Genome editing,Cas9,Genetics,Genomics,Modification of genetic information,Omics,Bioinformatics,Genetics techniques,Genetic engineering,Molecular biology,Molecular genetics,Biochemistry,Biotechnology,Biology,Biological engineering,Branches of genetics,Life sciences]
---


CRISPR-Cas9, a naturally occurring defense system in bacteria, allows researchers to design a short sequence of RNA called guide RNA that targets a specific section of genetic code (DNA) in a cell. That's how bacteria use CRISPR-Cas9 to immunize themselves from disease. For this study, Cas9 proteins from Spy were replaced with Streptococcus thermophiles (Sth) proteins that also recognize longer PAMs. We found that even with DNA or RNA bulges, the Cas9 protein can still cut, he said. They have needs to design CRISPR systems for different applications, but there are a lot of common issues.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2016/02/160208135449.htm){:target="_blank" rel="noopener"}


