---
layout: post
title: "Ancient shellfish remains rewrite 10,000-year history of El Nino cycles"
date: 2015-07-04
categories:
author: University of Washington 
tags: [El Nio,El NioSouthern Oscillation,Ocean,Earth,Nature,Applied and interdisciplinary physics,Physical geography,Oceanography,Earth sciences]
---


The results, from the University of Washington and University of Montpellier, question how well computer models can reproduce historical El Niño cycles, or predict how they could change under future climates. Our data contradicts the hypothesis that El Niño activity was very reduced 10,000 years ago, and then slowly increased since then, said first author Matthieu Carré, who did the research as a UW postdoctoral researcher and now holds a faculty position at the University of Montpellier in France. The shells provide 1- to 3-year-long records of monthly temperature of the Pacific Ocean along the coast of Peru. The new record shows that 10,000 years ago the El Niño cycles were strong, contradicting the current leading interpretations. Around 6,000 years ago most of the ice age floes would have finished melting, so the effect of Earth's orbital geometry might have taken over then to cause the period of weak El Niños.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/uow-asr080814.php){:target="_blank" rel="noopener"}


