---
layout: post
title: "XENON1T Experimental data establishes most stringent limit on dark matter"
date: 2018-06-02
categories:
author: "Rensselaer Polytechnic Institute"
tags: [XENON,Dark matter,Science,Physical sciences,Particle physics,Physics]
---


But because the dark matter particles known as weakly interacting massive particles, or WIMPs, cannot be seen and seldom interact with ordinary matter, their existence has never been confirmed. Several astronomical measurements have corroborated the existence of dark matter, leading to a worldwide effort to directly observe dark matter particle interactions with ordinary matter. Although dark matter interactions are rare, interactions with other forms of matter are common, and a sensitive detector is designed to minimize those interactions. Even with shielding from the outside world, contaminants seep into the xenon from the materials used in the detector itself and, among his contributions, Brown is responsible for a sophisticated purification system that continually scrubs the xenon in the detector. ###  About Rensselaer Polytechnic Institute  Rensselaer Polytechnic Institute, founded in 1824, is America's first technological research university.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/rpi-xed052918.php){:target="_blank" rel="noopener"}


