---
layout: post
title: "'Tree of life' for 2.3 million species released"
date: 2015-10-01
categories:
author: Duke University 
tags: [Phylogenetic tree,Evolution,Life,Species,Science,Biology]
---


Tens of thousands of smaller trees have been published over the years for select branches of the tree of life -- some containing upwards of 100,000 species -- but this is the first time those results have been combined into a single tree that encompasses all of life. Rather than build the tree of life from scratch, the researchers pieced it together by compiling thousands of smaller chunks that had already been published online and merging them together into a gigantic supertree that encompasses all named species. There's a pretty big gap between the sum of what scientists know about how living things are related, and what's actually available digitally, Cranston said. As important as showing what we do know about relationships, this first tree of life is also important in revealing what we don't know, said co-author Douglas Soltis of the University of Florida. It's critically important to share data for already-published and newly-published work if we want to improve the tree.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/du-ol091815.php){:target="_blank" rel="noopener"}


