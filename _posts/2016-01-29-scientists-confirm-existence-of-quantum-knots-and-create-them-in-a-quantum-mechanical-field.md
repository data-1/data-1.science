---
layout: post
title: "Scientists confirm existence of quantum knots and create them in a quantum-mechanical field"
date: 2016-01-29
categories:
author: Aalto University
tags: [BoseEinstein condensate,Quantum mechanics,Knot,Physics,Science,Theoretical physics,Physical sciences,Applied and interdisciplinary physics,Scientific theories]
---


This required them to initialize the quantum field to point in a particular direction, after which they suddenly changed the applied magnetic field to bring an isolated null point, at which the magnetic field vanishes into the center of the cloud. Topological structure of a quantum-mechanical knot soliton. The white ring is the core of the soliton (field pointing down), and the surrounding colored bands define a set of nested tori that illustrate the linked structure of its field lines. Credit: David Hall  Mathematically speaking, the created quantum knot realizes a mapping referred to as Hopf fibration that was discovered by Heinz Hopf in 1931. Such system would allow for detailed studies of the stability of the knot itself, says Mikko Möttönen.

<hr>

[Visit Link](http://phys.org/news/2016-01-scientists-quantum-quantum-mechanical-field.html){:target="_blank" rel="noopener"}


