---
layout: post
title: "NASA Spacecraft Wakes Up as It Approaches Pluto"
date: 2015-07-14
categories:
author: Justin Worland
tags: [New Horizons,Pluto,Planemos,Astronomical objects,Discovery and exploration of the Solar System,Flight,Spacecraft,Space exploration,Astronautics,Bodies of the Solar System,Planetary science,Solar System,Spaceflight,Astronomy,Outer space,Space science,Space vehicles,Science,Local Interstellar Cloud,Space missions,Planets,Space program of the United States]
---


A NASA spacecraft has emerged from hibernation in preparation for completing its nine-year, 2.9-billion mile journey to observe Pluto from up close, the space agency said. Sending its signal at the speed of light, the New Horizons ship beamed a report down to Earth that it was back in active mode as of Dec. 6. “Technically, this was routine, since the wake-up was a procedure that we’d done many times before,” said Glen Fountain, the mission’s project manager. “Symbolically, however, this is a big deal. It means the start of our pre-encounter operations.”  After tests early next year, the spacecraft will collect data and images about Pluto and its surrounding moons.

<hr>

[Visit Link](http://time.com/3623081/nasa-spacecraft-wakes-up-as-it-approaches-pluto/){:target="_blank" rel="noopener"}


