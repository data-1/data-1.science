---
layout: post
title: "Expanding the DNA alphabet: 'Extra' DNA base found to be stable in mammals"
date: 2016-05-09
categories:
author: University Of Cambridge
tags: [DNA,Epigenetics,Gene,Nucleobase,Biology,Genetics,Biochemistry,Molecular biology,Biotechnology,Life sciences,Chemistry,Cell biology]
---


While its exact function is yet to be determined, 5fC's physical position in the genome makes it likely that it plays a key role in gene activity. Since the structure of DNA was discovered more than 60 years ago, it's been known that there are four DNA bases: G, C, A and T (Guanine, Cytosine, Adenine and Thymine). However, this new research has found that 5fC can actually be stable in living tissue, making it likely that it plays a key role in the genome. Using high-resolution mass spectrometry, the researchers examined levels of 5fC in living adult and embryonic mouse tissues, as well as in mouse embryonic stem cells - the body's master cells which can become almost any cell type in the body. They found that 5fC is present in all tissues, but is very rare, making it difficult to detect.

<hr>

[Visit Link](http://phys.org/news354170766.html){:target="_blank" rel="noopener"}


