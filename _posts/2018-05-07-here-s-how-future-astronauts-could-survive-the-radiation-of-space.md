---
layout: post
title: "Here's How Future Astronauts Could Survive the Radiation of Space"
date: 2018-05-07
categories:
author: ""
tags: [Health threat from cosmic rays,Outer space,Science,Astronautics,Space science,Spaceflight]
---


As the NASA Space Radiation Health Project explains, in addition to the more immediate effects — like acute radiation sickness — exposure to radiation also increases astronauts' risk of several cancers, genetic mutations, nervous system damage, and even cataracts. Radioresistant Humans  That's where the international team of researchers comes in. Such travel, taking one or more years outside the Earth's magnetosphere, would take a high toll on astronauts' health due to exposure to cosmic radiation. Ways to reduce health risks from space radiation during deep space travels. The team's research, then, isn't only of benefit to humans destined for life off-Earth.

<hr>

[Visit Link](https://futurism.com/future-astronauts-survive-radiation/){:target="_blank" rel="noopener"}


