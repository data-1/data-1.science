---
layout: post
title: "Machine Learning: In Plain English"
date: 2018-08-01
categories:
author: "Jul."
tags: []
---


In this article, I will describe how analytics is related to Machine Learning. What Is Machine Learning? Data. It uses Machine Learning techniques to learn how to identify spam from millions of mail messages. The Machine Learning Process  Unlike the futuristic image of machines learning to play chess, most Machine Learning is (currently) quite laborious, and illustrated in the diagram below:  It’s likely in the future machine learning will be applied to help speed the process, especially in the area of data collection and cleaning, but the main steps remain:  Define the Problem: As indicated in my other article, always start with a clearly defined problem and objective in mind.

<hr>

[Visit Link](https://dzone.com/articles/machine-learning-in-plain-english?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


