---
layout: post
title: "New technique to detect magnetic fields inside stars"
date: 2015-10-27
categories:
author: "$author"  
tags: []
---


The stars and the faint arm of the Milky Way are visible over a wind farm just north of Medicine Bow, Wyo. on Thursday night, Jan. 3, 2013 just before moonrise at 11:30 p.m. (AP Photo/Casper Star-Tribune, Kyle Grantham)  Using a technique similar to medical ultrasound, astrophysicists have for the first time developed a way to determine the presence of strong magnetic fields deep inside pulsating giant stars. The researchers used asteroseismology — a discipline similar to seismology — to track waves traveling through stars in order to determine their inner properties. “This is exciting as internal magnetic fields play an important role both for the evolution of stars and for the properties of their remnants,” he said.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/new-technique-to-detect-magnetic-fields-inside-stars/article7796304.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


