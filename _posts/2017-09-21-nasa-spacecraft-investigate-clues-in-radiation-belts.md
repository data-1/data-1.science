---
layout: post
title: "NASA spacecraft investigate clues in radiation belts"
date: 2017-09-21
categories:
author: "Mara Johnson-Groh, Nasa'S Goddard Space Flight Center"
tags: [Van Allen radiation belt,Van Allen Probes,Astronomy,Spaceflight,Physical sciences,Science,Outer space,Space science,Nature,Astronautics]
---


The twin Van Allen Probes orbit one behind the other, investigating clues in a way a single spacecraft never could. The two twin Van Allen Probe spacecraft orbit one behind the other, investigating clues in a way a single spacecraft never could. Where did these particles come from? With the help of computer models, they deduced that the particles had originated on the night side of Earth before being energized and accelerated through interactions with Earth's magnetic field. The unique double observations of the Van Allen Probes help untangle the complex workings of Earth's magnetic environment.

<hr>

[Visit Link](https://phys.org/news/2017-03-nasa-spacecraft-clues-belts.html){:target="_blank" rel="noopener"}


