---
layout: post
title: "NASA's James Webb Space Telescope primary mirror fully assembled"
date: 2016-02-05
categories:
author: NASA/Goddard Space Flight Center
tags: [Goddard Space Flight Center,James Webb Space Telescope,Telescope,NASA,Mirror,Astronautics,Spaceflight,Outer space,Space science,Astronomy,Space program of the United States,Science,Spacecraft,Space exploration,Flight,Telescopes,Space programs,Astronomical imaging]
---


The 18th and final primary mirror segment is installed on what will be the biggest and most powerful space telescope ever launched. The final mirror installation Wednesday at NASA's Goddard Space Flight Center in Greenbelt, Maryland marks an important milestone in the assembly of the agency's James Webb Space Telescope. Now that the mirror is complete, we look forward to installing the other optics and conducting tests on all the components to make sure the telescope can withstand a rocket launch, said Bill Ochs, James Webb Space Telescope project manager. Harris Corporation leads integration and testing for the telescope. It will be the most powerful space telescope ever built.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/nsfc-njw020416.php){:target="_blank" rel="noopener"}


