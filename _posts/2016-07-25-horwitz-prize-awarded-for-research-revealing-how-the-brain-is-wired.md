---
layout: post
title: "Horwitz Prize awarded for research revealing how the brain is wired"
date: 2016-07-25
categories:
author: "Columbia University Irving Medical Center"
tags: [DSCAM,Brain,Nervous system,Neuron,Life sciences]
---


NEW YORK, NY (September 22, 2015) -- Columbia University will award the 2015 Louisa Gross Horwitz Prize to S. Lawrence Zipursky, PhD, for discovering a molecular identification system that helps neurons to navigate and wire the brain. said Lee Goldman, MD, Harold and Margaret Hatch Professor of the University, dean of the Faculties of Health Sciences and Medicine, and chief executive of Columbia University Medical Center. From this search, Zipursky's team discovered a gene called Dscam, a fruit fly gene related to the human Down Syndrome Cell Adhesion Molecule (DSCAM) gene, which helps neurons choose the right paths to take as they extend through the developing nervous system. Of the 93 Horwitz Prize winners to date, 43 have gone on to receive Nobel prizes. Lecture #2, Molecular Diversity, Cell Recognition, and the Assembly of Neural Circuits, will be presented at 3:30 pm in the Alumni Auditorium, College of Physicians & Surgeons, 650 West 168th St., at Columbia University Medical Center.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/cumc-hpa092215.php){:target="_blank" rel="noopener"}


