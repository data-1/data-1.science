---
layout: post
title: "Extraterrestrial oceans – beneath the surface"
date: 2016-05-21
categories:
author: Umea University
tags: [Jupiter,Comet,Solar System,Callisto (moon),Planet,Ceres (dwarf planet),Sun,Astronomy,Physical sciences,Planets,Local Interstellar Cloud,Astronomical objects,Bodies of the Solar System,Space science,Planetary science,Outer space]
---


Icy objects in our solar system have large oceans under their surfaces and here life could evolve and flourish. There has long been speculation as to whether Jupiter's large, icy moon Callisto has an ocean beneath its surface. If you find an ocean beneath the surface of one moon, perhaps the same is true of other icy objects in space, says Jesper Lindkvist. Outflow of water vapour has been detected from the surface of the dwarf planet Ceres, which is in the asteroid belt between Mars and Jupiter. Explore further Dwarf planet Haumea's lunar system smaller than anticipated  More information: Plasma Interactions with Icy Bodies in the Solar System: Plasma Interactions with Icy Bodies in the Solar System: umu.diva-portal.org/smash/record.jsf?pid=diva2 %3A925397&dswid=-2255

<hr>

[Visit Link](http://phys.org/news/2016-05-extraterrestrial-oceans-beneath-surface.html){:target="_blank" rel="noopener"}


