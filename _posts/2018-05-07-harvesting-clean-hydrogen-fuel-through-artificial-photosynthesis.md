---
layout: post
title: "Harvesting clean hydrogen fuel through artificial photosynthesis"
date: 2018-05-07
categories:
author: "University of Michigan"
tags: [Artificial photosynthesis,Solar energy,Hydrogen,Water,Photosynthesis,Fuel,Hydrogen production,Solar cell,Materials,Physical chemistry,Sustainable energy,Sustainable technologies,Chemistry,Energy technology,Chemical substances,Energy,Technology,Artificial materials,Physical sciences,Nature]
---


The device could also be reconfigured to turn carbon dioxide back into fuel. Hydrogen is the cleanest-burning fuel, with water as its only emission. The method advanced by the new device, called direct solar water splitting, only uses water and light from the sun. If we can directly store solar energy as a chemical fuel, like what nature does with photosynthesis, we could solve a fundamental challenge of renewable energy, said Zetian Mi, a professor of electrical and computer engineering at the University of Michigan who led the research while at McGill University in Montreal. Mi and his team, however, achieved more than 3 percent solar-to-hydrogen efficiency.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/uom-hch050318.php){:target="_blank" rel="noopener"}


