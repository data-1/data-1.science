---
layout: post
title: "Tracking Japan's asteroid impact mission"
date: 2016-04-29
categories:
author: European Space Agency
tags: [Hayabusa2,JAXA,European Space Operations Centre,Outer space,Space programs,Aerospace,Scientific exploration,Solar System,Space research,Spacecraft,Space exploration,Flight,Astronomy,Astronautics,Space science,Spaceflight]
---


The fragments will be returned to Earth in 2020. Credit: ESA  Telecommands from mission controllers at the Japan Aerospace and Exploration Agency (JAXA) will be fed to the station via ESOC, ESA's European Space Operations Centre, Germany. This is the first time we've supported a Japanese deep-space mission, so we've been working closely with JAXA in the past months to establish technical links between the ground station and the Hayabusa mission systems, said Maite Arza, ESA's Service Manager for Hayabusa at ESOC. Credit: ESA/ J. Mai  World-class tracking technology  Together with similar stations in Spain and Australia, the Malargüe site comprises the deep-space tracking capability of ESA's Estrack network. The network is controlled from ESOC, and provides tracking support to ESA and partner agency missions 24 hours/day, 365 days/year.

<hr>

[Visit Link](http://phys.org/news349594208.html){:target="_blank" rel="noopener"}


