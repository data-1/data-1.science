---
layout: post
title: "Deep learning stretches up to scientific supercomputers"
date: 2018-08-15
categories:
author: "Us Department Of Energy"
tags: [National Energy Research Scientific Computing Center,Supercomputer,Computing,Cognitive science,Science,Information Age,Branches of science,Information technology,Computer science,Technology]
---


Researchers delivered a 15-petaflop deep-learning software and ran it on Cori, a supercomputer at the National Energy Research Scientific Computing Center, a Department of Energy Office of Science user facility. However, the use of machine learning in high performance computing for science has been limited. Using machine learning techniques on supercomputers, scientists could extract insights from large, complex data sets. To address this problem a collaboration among Intel, the National Energy Research Scientific Computing Center, and Stanford University has been working to solve problems that arise when using deep learning techniques, a form of machine learning, on terabyte and petabyte data sets. The runs used physics- and climate-based data sets on Cori, a supercomputer located at the National Energy Research Scientific Computing Center.

<hr>

[Visit Link](https://phys.org/news/2018-08-deep-scientific-supercomputers.html){:target="_blank" rel="noopener"}


