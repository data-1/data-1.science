---
layout: post
title: "The evolution of beer"
date: 2015-08-25
categories:
author: Molecular Biology and Evolution (Oxford University Press) 
tags: [Saccharomyces eubayanus,Beer,Yeast,Brewing,Lager,Saccharomyces pastorianus,Saccharomyces cerevisiae,Fermented drinks,Biology,Food and drink preparation,Biotechnology products,Fermented foods]
---


Lager yeasts are hybrid strains, made of two different yeast species, S. cerevisiae and S. eubayanus, which was discovered in 2011. They compared it to domesticated hybrids that are used to brew lager style beers, allowing for the first time the ability of study the complete genomes of both parental yeast species contributing to lager beer. advertisement  They show two independent origin events for S. cerevisiae and S. eubanyus hybrids that brew lager beers. The findings show that domestication for beer making has placed yeast on similar evolutionary trajectories multiple times. Lager yeasts did not just originate once.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150811182818.htm){:target="_blank" rel="noopener"}


