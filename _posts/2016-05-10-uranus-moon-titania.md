---
layout: post
title: "Uranus' moon Titania"
date: 2016-05-10
categories:
author: Matt Williams
tags: [Titania (moon),Oberon (moon),Moons of Uranus,Uranus,Umbriel (moon),Natural satellite,Planetary-mass satellites,Outer planets,Planemos,Gas giants,Planets,Moons,Space science,Ice giants,Astronomical objects,Astronomy,Outer space,Planetary science,Planets of the Solar System,Bodies of the Solar System,Solar System]
---


In fact, astronomers can now account for 27 moons in orbit around Uranus. Discovery and Naming:  Titania was discovered by William Herschel on January 11th, 1787, the English astronomer who had discovered Uranus in 1781. 520 km (320 mi), which would mean the core accounts for 66% of the radius of the moon, and 58% of its mass. The surface of Titania is less heavily cratered than the surface of either Oberon or Umbriel, suggesting that its surface is much younger. These include craters, faults (or scarps) and what are known as grabens (sometimes called canyons).

<hr>

[Visit Link](http://phys.org/news355389755.html){:target="_blank" rel="noopener"}


