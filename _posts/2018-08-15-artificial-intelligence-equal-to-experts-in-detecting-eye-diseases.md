---
layout: post
title: "Artificial intelligence equal to experts in detecting eye diseases"
date: 2018-08-15
categories:
author: "University College London"
tags: [DeepMind,Artificial intelligence,National Health Service,Health,National Institute for Health and Care Research,Patient,Ophthalmology,Moorfields Eye Hospital,Visual impairment,Health care,UCL Institute of Ophthalmology,Research,Clinical trial,Mustafa Suleyman,Social programs,Medicine,Health sciences]
---


Credit: University College London  An artificial intelligence (AI) system, which can recommend the correct referral decision for more than 50 eye diseases, as accurately as experts has been developed by Moorfields Eye Hospital NHS Foundation Trust, DeepMind Health and UCL. The study, launched in 2016, brought together leading NHS eye health professionals and scientists from the National Institute for Health Research (NIHR) and UCL with some of the UK's top technologists at DeepMind to investigate whether AI technology could help improve the care of patients with sight-threatening diseases, such as age-related macular degeneration and diabetic eye disease. Using two types of neural network – mathematical systems for identifying patterns in images or data – the AI system quickly learnt to identify ten features of eye disease from highly complex optical coherence tomography (OCT) scans. In addition, Moorfields can also use DeepMind's trained AI model for future non-commercial research efforts, which could help advance medical research even further. These incredibly exciting results take us one step closer to that goal and could, in time, transform the diagnosis, treatment and management of patients with sight threatening eye conditions, not just at Moorfields, but around the world.

<hr>

[Visit Link](https://phys.org/news/2018-08-artificial-intelligence-equal-experts-eye.html){:target="_blank" rel="noopener"}


