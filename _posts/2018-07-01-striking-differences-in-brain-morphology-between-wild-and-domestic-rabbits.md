---
layout: post
title: "Striking differences in brain morphology between wild and domestic rabbits"
date: 2018-07-01
categories:
author: "Uppsala University"
tags: [Rabbit,Domestic rabbit,Brain,Domestication,Amygdala,Genetics,Magnetic resonance imaging,Animal,Cerebral cortex,Human,Cognitive science,Neuroscience]
---


The most characteristic feature of domestic animals is their tame behaviour. Secondly, domestic rabbits have a reduced amygdala and an enlarged medial prefrontal cortex. Thirdly, we noticed a generalized reduction in white matter structure in domestic rabbits.' Our results show that an area involved in sensing fear (the amygdala) is smaller in size while an area controlling the response to fear (the medial prefrontal cortex) is larger in domestic rabbits. 'No previous study on animal domestication has explored changes in brain morphology between wild and domestic animals in such depth as we have done in this study,' says Leif Andersson, Uppsala University, Swedish University of Agricultural Sciences and Texas A&M University.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/uu-sdi062018.php){:target="_blank" rel="noopener"}


