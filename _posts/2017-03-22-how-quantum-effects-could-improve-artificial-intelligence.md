---
layout: post
title: "How quantum effects could improve artificial intelligence"
date: 2017-03-22
categories:
author: "Lisa Zyga"
tags: [Machine learning,Learning,Artificial intelligence,Quantum machine learning,Quantum mechanics,Intelligence,Research,Privacy,Cognition,Science,Cognitive science,Technology,Interdisciplinary subfields,Branches of science]
---


Physicists have shown that quantum effects have the potential to significantly improve a variety of interactive learning tasks in machine learning. In the new study, the researchers' main result is that quantum effects can help improve reinforcement learning, which is one of the three main branches of machine learning. This is, to our knowledge, the first work which shows that quantum improvements are possible in more general, interactive learning tasks, Dunjko said. But while in certain situations quantum effects have the potential to offer great improvements, in other cases classical machine learning likely performs just as well or better than it would with quantum effects. One of the open questions we are interested in is whether quantum effects can play an instrumental role in the design of true artificial intelligence.

<hr>

[Visit Link](http://phys.org/news/2016-10-quantum-effects-artificial-intelligence.html){:target="_blank" rel="noopener"}


