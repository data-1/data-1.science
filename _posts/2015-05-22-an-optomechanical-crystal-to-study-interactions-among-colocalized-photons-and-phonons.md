---
layout: post
title: "An optomechanical crystal to study interactions among colocalized photons and phonons"
date: 2015-05-22
categories:
author: Catalan Institute Of Nanoscience
tags: [Photon,Phonon,Cavity optomechanics,Electrodynamics,Radiation,Chemistry,Electrical engineering,Science,Theoretical physics,Physics,Electromagnetism,Physical chemistry,Atomic molecular and optical physics,Quantum mechanics,Optics,Electromagnetic radiation,Applied and interdisciplinary physics]
---


A photon is the quantum particle of light and electromagnetic radiation. Although it might seem surprising, electromagnetic radiation and mechanical vibrations of matter interact and exchange energy at the nanometric scale. Optomechanical crystals are simultaneously photonic and phononic crystals that confine in the same structure photons and phonons. One of the major issues when working with photon-phonon interaction is the difficulty to properly isolate quantum particles of vibration. Exchanging energy from photons to phonons might be useful, for instance, to manipulate heat dissipation with light.

<hr>

[Visit Link](http://phys.org/news325151923.html){:target="_blank" rel="noopener"}


