---
layout: post
title: "Svante Pääbo to receive the 2018 HFSP Nakasone Award"
date: 2017-10-19
categories:
author: Human Frontier Science Program
tags: [Human Frontier Science Program,Svante Pbo,Denisovan,Genetics,Biology,Science]
---


The Human Frontier Science Program Organization (HFSPO) has announced that the 2018 HFSP Nakasone Award has been awarded to Svante Pääbo of the Max Planck Institute for Evolutionary Anthropology in Leipzig (Germany) for his discovery of the extent to which hybridization with Neanderthals and Denisovans has shaped the evolution of modern humans, and his development of techniques for sequencing DNA from fossils. The HFSP Nakasone Award was established to honor scientists who have made key breakthroughs in fields at the forefront of the life sciences. In his recent work Pääbo sequenced the complete genomes of Neanderthals and their distant Asian relatives at unprecedented accuracy. These genome sequences revealed almost all the changes that have occurred in the genome of modern humans since their separation from the common ancestor shared with their closest evolutionary relatives - Neanderthals and Denisovans. The Human Frontier Science Program Organization was founded in 1989 to support international research and training at the frontier of the life sciences.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/hfsp-spt101917.php){:target="_blank" rel="noopener"}


