---
layout: post
title: "Black holes and the dark sector explained by quantum gravity"
date: 2015-07-26
categories:
author: World Scientific 
tags: [Graviton,Gravity,Black hole,Physics,Quantum gravity,Theories of gravitation,Science,Physical cosmology,Physical sciences,Scientific theories,Applied and interdisciplinary physics,Astrophysics,Theoretical physics,Cosmology,Particle physics,Astronomy,Nature,General relativity,Epistemology of science,Quantum mechanics,Scientific method,Theory of relativity]
---


Ask any theoretical physicist on what are the most profound mysteries in physics and you will be surprised if she mentions anything other than Quantum Gravity and the Dark Sector. The theory is known as Nexus in the sense that it provides a link between Quantum Theory and GR. The Nexus graviton is Dark Matter and constitutes space-time. This process manifests as Dark Energy and takes place throughout space-time as the theory explains. This paper is significant in the sense that it sheds some light on some of the most perplexing questions in physics which include a quantum description of Black Holes without singularities inherent in classical GR.The solutions provided in this paper will certainly open doors to new physics.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/ws-bha032015.php){:target="_blank" rel="noopener"}


