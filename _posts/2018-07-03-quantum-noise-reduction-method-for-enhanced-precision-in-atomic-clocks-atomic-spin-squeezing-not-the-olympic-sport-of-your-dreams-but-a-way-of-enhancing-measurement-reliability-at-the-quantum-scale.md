---
layout: post
title: "Quantum noise reduction method for enhanced precision in atomic clocks: Atomic Spin Squeezing: Not the Olympic sport of your dreams, but a way of enhancing measurement reliability at the quantum scale"
date: 2018-07-03
categories:
author: "Springer"
tags: [Squeezed coherent state,Quantum mechanics,Quantum noise,Spin squeezing,Spin (physics),Physics,Theoretical physics,Science,Applied and interdisciplinary physics,Scientific theories,Scientific method,Physical sciences]
---


Finding ways to reduce quantum noise can enhance the precision of measurement in atomic fountain clocks or in methods used for quantum information processing. Noise also occurs at the quantum scale and can e.g. interfere with the measurements of atomic fountain clocks or with quantum information processing. Now a team of physicists including Aranya Bhattacherjee from Jawaharlal Nehru University, New Delhi, India and colleagues are investigating ways of improving the analysis of quantum noise measurement in the case of spectroscopic investigations; their preliminary findings were released in a study in EPJ D.  This method, called atomic spin squeezing, works by redistributing the uncertainty unevenly between two components of spin in these measurements systems, which operate at the quantum scale. In this study, the authors develop a new approach that relies on spin squeezed states and is designed to accurately analyse the reduction of quantum noise in atomic systems associated with the spectroscopic measurements of atomic clocks. Their method involves reducing the spin fluctuations in one spin component perpendicular to the mean spin direction below the standard quantum limit.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/12/171222111428.htm){:target="_blank" rel="noopener"}


