---
layout: post
title: "Researchers shine a light on more accurate way to estimate climate change"
date: 2018-06-07
categories:
author: "University of New Hampshire"
tags: [Carbon cycle,Photosynthesis,Ecosystem,Earth,Climate change,Applied and interdisciplinary physics,Natural environment,Environmental science,Physical geography,Nature,Earth sciences]
---


This first-of-its-kind global analysis could have significance in providing more accurate data for scientists working to model carbon cycle and eventually help better project climate change. The importance of these results is that rather than look at several different types of data and computer-based models from information collected on the ground to monitor plant photosynthesis across the globe, using the satellite observations will provide a near real-time option that is simple, reliable and fast, said Jingfeng Xiao, a UNH research associate professor and the principal investigator on the study recently published in the journal Global Change Biology. To measure the amount of carbon taken up by plants through photosynthesis, known as gross primary productivity (GPP), scientists have increasingly been measuring the energy glow of plants, called solar-induced fluorescence (SIF). Researchers collected the SIF data for plants in eight major biomes, or ecosystem types, from the Orbiting Carbon Observatory-2 (OCO-2) satellite and found that it didn't matter where the plants were, that just like earlier studies in single areas, where there was more SIF, the plants took up more carbon from photosynthesis, and vice versa. Xiao's research establishes this universal relationship across eight major ecosystem types and shows that SIF can indeed serve as a proxy for more time-intensive calculations.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180605112113.htm){:target="_blank" rel="noopener"}


