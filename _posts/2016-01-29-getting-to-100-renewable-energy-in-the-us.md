---
layout: post
title: "Getting To 100% Renewable Energy In the US"
date: 2016-01-29
categories:
author: Adam Johnston, Guest Contributor, Tina Casey, Zachary Shahan, Written By
tags: [Renewable energy,Photovoltaics,Solar power,Clean technology,Electric power,Natural resources,Economy,Technology,Energy,Nature,Sustainable energy,Physical quantities,Sustainable technologies,Sustainable development,Climate change mitigation,Energy and the environment,Natural environment,Power (physics),Renewable resources]
---


This analysis shows that getting to 100% renewable energy within the US would consist of:  30.9% onshore wind  19.1% offshore wind  30.7% utility-scale solar photovoltaics (PV)  7.2% rooftop PV  7.3% concentrated solar power (CSP) with storage  1.25% geothermal  0.37% tidal/wave  3.01% hydroelectricity  Under a 100% renewable scenario based on these numbers, millions of jobs would be created. While tidal and wave energy make up a small fraction of the suggested renewable energy mix, for example, both have a lot to gain if they make further breakthroughs and cost improvements. A recent article from CBC discussed how a new Nature report points to how power plant production (including hydro) could see declines by 66.7% globally by 2040 & 2069, due (of course) to a changing climate. Nonetheless, this report and the tidy website give a roadmap and discussion for how the US can get to 100% renewables by 2050. Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2016/01/07/getting-100-renewable-energy-us/){:target="_blank" rel="noopener"}


