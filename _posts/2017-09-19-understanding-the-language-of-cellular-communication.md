---
layout: post
title: "Understanding the language of cellular communication"
date: 2017-09-19
categories:
author: "Lori Dajose, California Institute Of Technology"
tags: [Ligand (biochemistry),Receptor (biochemistry),Cell (biology),Cell biology,Biology,Biotechnology]
---


This language is thought to be common to many types of cellular communication and has implications for designing future therapies, according to scientists in the laboratory of Michael Elowitz, professor of biology and bioengineering at Caltech and an investigator with the Howard Hughes Medical Institute. The authors studied a major communication channel, called the bone morphogenetic protein (BMP) pathway, that operates in nearly all tissues and is a target for cancer therapies. This system uses many different ligands and receptors in various combinations. One day, I came to the lab and mixed two ligands together that, each on their own, activate cells very strongly, says Yaron Antebi, a Caltech postdoctoral scholar in biology and biological engineering. Different combinations of ligands encode different instructions for the cell.

<hr>

[Visit Link](https://phys.org/news/2017-09-language-cellular.html){:target="_blank" rel="noopener"}


