---
layout: post
title: "Spintronics: Controlling magnetic spin with electric fields"
date: 2018-07-01
categories:
author: "Ecole Polytechnique Fédérale de Lausanne"
tags: [Spintronics,Electron,Spin (physics),Multiferroics,Physics,Applied and interdisciplinary physics,Electricity,Materials science,Chemistry,Condensed matter physics,Electrical engineering,Physical sciences,Materials,Condensed matter,Chemical product engineering,Phases of matter,Technology,Quantum mechanics,Electromagnetism]
---


The field of spintronics has given rise to technological concepts of spintronic devices, which would run on electron spins, rather than their charge, used by traditional electronics. In order to build programmable spintronic devices we first need to be able to manipulate spins in certain materials. So far, this has been done with magnetic fields, which are not easy to integrate into everyday applications. By combining SARPES with the possibility to apply an electric field, the physicists demonstrate electrostatic spin manipulation in ferroelectric α-GeTe and multiferroic (GeMn)Te. In (GeMn)Te, the perpendicular spin component switches due to electric-field-induced magnetization reversal.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180619122400.htm){:target="_blank" rel="noopener"}


