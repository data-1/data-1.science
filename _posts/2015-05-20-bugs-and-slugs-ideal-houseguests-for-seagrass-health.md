---
layout: post
title: "Bugs and slugs ideal houseguests for seagrass health"
date: 2015-05-20
categories:
author: Uc Davis
tags: [Seagrass,Seagrass meadow,Biodiversity,Privacy,Biogeochemistry,Nature,Organisms,Systems ecology,Environmental science,Ecology,Oceanography,Earth sciences,Natural environment]
---


They gobble up algae that could smother the seagrass, keeping the habitat clean and healthy. Our results show that small marine invertebrates are really important, said Pamela Reynolds, a postdoctoral scholar at UC Davis and VIMS and the ZEN project coordinator. We found that the more diverse communities of these little algae-eating animals do a better job of keeping the seagrass clean and healthy. The researchers explored which of two known threats to seagrass has the greater impact on seagrass ecosystems: pollution from fertilizers or the loss of invertebrate species due, in part, to fishing. This map shows the 15 sites where scientists conducted simultaneous experiments of seagrass as part of the Zostera Experimental Network project.

<hr>

[Visit Link](http://phys.org/news351276852.html){:target="_blank" rel="noopener"}


