---
layout: post
title: "Brain area that recognises facial expressions found"
date: 2016-04-22
categories:
author:  
tags: []
---


Researchers, including Ramprakash Srinivasan from Ohio State University placed 10 college students into an fMRI machine and showed them more than 1,000 photographs of people making facial expressions. Researchers, including Ramprakash Srinivasan from Ohio State University placed 10 college students into an fMRI machine and showed them more than 1,000 photographs of people making facial expressions. “That suggests that our brains decode facial expressions by adding up sets of key muscle movements in the face of the person we are looking at,” said Aleix Martinez from Ohio State University. Using this fMRI data, researchers developed a machine learning algorithm that had about a 60 per cent success rate in decoding human facial expressions, regardless of the facial expression and regardless of the person viewing it. Researchers, including Ramprakash Srinivasan from Ohio State University placed 10 college students into an fMRI machine and showed them more than 1,000 photographs of people making facial expressions.

<hr>

[Visit Link](http://www.thehindu.com/news/brain-area-that-recognises-facial-expressions-found/article8498786.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


