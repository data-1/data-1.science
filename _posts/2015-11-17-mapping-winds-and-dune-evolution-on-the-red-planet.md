---
layout: post
title: "Mapping winds and dune evolution on the Red Planet"
date: 2015-11-17
categories:
author: Thomas Deane, Trinity College Dublin
tags: [Mars,HiRISE,Dune,Outer space,Science,Planetary science,Planets of the Solar System,Terrestrial planets,Astronomical objects known since antiquity,Spaceflight,Space science]
---


This is a subset of NASA HiRISE image ESP_011909 taken of a dune in Proctor Crater on Mars, showing various directions of ripples on the surface of a Martian dune (image is approximately 480 m across. The resolution of the HiRISE data is so fine that the team was able to view individual ripples in the sand on Mars 225 million kilometres away from Earth. Modelling the wind flow in 3D enabled the scientists, for the first time, to see how large dunes on Mars modify localised wind flow. Dr Bourke said: I have always wondered which way the winds blow on Mars. Credit: H Viles  The scientists' results showed that established thinking using large-scale global air circulation models is too crude for understanding how winds move sand on the more complex surfaces of Mars (e.g. over sand dunes).

<hr>

[Visit Link](http://phys.org/news/2015-11-dune-evolution-red-planet.html){:target="_blank" rel="noopener"}


