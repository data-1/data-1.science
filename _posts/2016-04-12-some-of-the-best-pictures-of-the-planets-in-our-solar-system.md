---
layout: post
title: "Some of the best pictures of the planets in our solar system"
date: 2016-04-12
categories:
author: Matt Williams
tags: [Jupiter,Planet,Saturn,Solar System,Uranus,Mars,Neptune,Sun,Gas giant,Rings of Saturn,CassiniHuygens,Voyager 2,Voyager program,Venus,Outer space,Planets,Astronomy,Astronomical objects,Bodies of the Solar System,Planetary science,Planets of the Solar System,Physical sciences,Planemos,Astronomical objects known since antiquity,Substellar objects,Space science]
---


Credit: NASA/JPL  Venus is the second planet from our Sun, and Earth's closest neighboring planet. Credit: NASA  Earth is the third planet from the Sun, the densest planet in our Solar System, and the fifth largest planet. It also has the most moons of any planet in the Solar System, with 67 confirmed satellites as of 2012. Credit: NASA/JPL  Another gas giant, Uranus is the seventh planet from our Sun and the third largest planet in our Solar System. In both cases, these storms and the planet itself were observed by the Voyager 2 spacecraft, the only one to capture images of the planet.

<hr>

[Visit Link](http://phys.org/news340879738.html){:target="_blank" rel="noopener"}


