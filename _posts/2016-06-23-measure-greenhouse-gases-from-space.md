---
layout: post
title: "Measure greenhouse gases from space"
date: 2016-06-23
categories:
author: ""
tags: [Physical sciences,Materials,Applied and interdisciplinary physics,Chemistry]
---


Directly bonded fused silica GRISM (Prism + Grating) with grating at the inner surface. To attain the highest possible resolution, grating and prism structures are combined: prisms deflect blue light the most intensively, while grating is best at bending red light. But so far it has hardly been possible to combine the two structures so that they would be suitable for space. We combine the optical elements with each other at the atomic scale, namely via oxygen bridges, explains Dr. Gerhard Kalkowski, scientist at the IOF. In this process, oxygen and hydrogen atoms are bonded to the wafer's surface.

<hr>

[Visit Link](http://phys.org/news/2016-06-greenhouse-gases-space.html){:target="_blank" rel="noopener"}


