---
layout: post
title: "Artificial Intelligence: Automating Decision-Making"
date: 2018-08-06
categories:
author: "Wolf Ruzicka, Chariman, Eastbanc Technologies, Cate Lawrence, Zone Leader, Akmal Chaudhri, Senior Technical Evangelist, Frederic Jacquet, Cino - Technology Evangelist, Tom Smith"
tags: []
---


The 2017 Guide to Java explores upcoming features of Java 9, how to make your apps backwards-compatible, a look into whether Microservices are right for you, and using the Futures API in Java. In the 2016 Guide to Modern Java, we cover how Java 8 improves the developer experience and preview features of Java 9. We also explore implementing hash tables and reactive microservices for a flexible architecture. The 2018 Guide to DevOps explores topics such as DevOps culture and philosophy, CI/CD and Sprint Planning, and Continuous Delivery Anti-Patterns. save DZone’s 2014 Guide to Big Data is the definitive resource for learning how industry experts are handling the massive growth and diversity of data.

<hr>

[Visit Link](https://dzone.com/guides/artificial-intelligence-automating-decision-making?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


