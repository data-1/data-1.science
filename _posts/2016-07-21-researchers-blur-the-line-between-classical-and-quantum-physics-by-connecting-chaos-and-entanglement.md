---
layout: post
title: "Researchers blur the line between classical and quantum physics by connecting chaos and entanglement"
date: 2016-07-21
categories:
author: "Sonia Fernandez, University Of California - Santa Barbara"
tags: [Quantum entanglement,Quantum mechanics,Physics,Chaos theory,Quantum computing,Quantum superposition,Qubit,Science,Applied and interdisciplinary physics,Theoretical physics,Physical sciences,Branches of science,Applied mathematics,Scientific theories]
---


Experimental link between quantum entanglement (left) and classical chaos (right) found using a small quantum computer. It consists of hundreds of years' worth of study including Newton's laws of motion, electrodynamics, relativity, thermodynamics as well as chaos theory—the field that studies the behavior of highly sensitive and unpredictable systems. All systems are fundamentally quantum systems, according Neill, but the means of describing in a quantum sense the chaotic behavior of, say, air molecules in an evacuated room, remains limited. And, it turns out that thermalization is the thing that connects chaos and entanglement. The study's findings have fundamental implications for quantum computing.

<hr>

[Visit Link](http://phys.org/news/2016-07-blur-line-classical-quantum-physics.html){:target="_blank" rel="noopener"}


