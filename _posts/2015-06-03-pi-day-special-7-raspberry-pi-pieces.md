---
layout: post
title: "Pi Day special: 7 Raspberry Pi pieces"
date: 2015-06-03
categories:
author: "Rikki Endsley"
tags: [Red Hat,Public sphere,Technology,Mass media,Communication,Computing]
---


Once a year, like clockwork, March 14 rolls around and people around the globe celebrate Pi Day. In the past, I've joined friends for a 3.14(ish) mile Pi Day run, visited a science museum (which included a pi exhibit) with my daughter, and simply indulged in a slice of Key Lime pie. What is Raspberry Pi? I'm glad you asked. Check out Coder, which converts Raspberry Pi into a friendly environment for learning web programming.

<hr>

[Visit Link](http://opensource.com/life/15/3/pi-day-special-7-raspberry-pi-pieces){:target="_blank" rel="noopener"}


