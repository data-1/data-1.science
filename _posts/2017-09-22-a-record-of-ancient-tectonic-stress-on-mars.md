---
layout: post
title: "A record of ancient tectonic stress on Mars"
date: 2017-09-22
categories:
author: ""
tags: [Tharsis,Rift,Earth sciences,Geology,Planetary science,Planets of the Solar System,Terrestrial planets,Tectonics,Lithosphere,Plate tectonics,Structural geology,Geomorphology,Structure of the Earth,Mars,Planetary geology]
---


Sets of ridges and troughs some 1000 km north of the giant Olympus Mons volcano contain a record of the intense tectonic stresses and strains experienced in the Acheron Fossae region on Mars 3.7–3.9 billion years ago. This scene, captured by ESA’s Mars Express on 4 May, focuses on the western part of Acheron Fossae, an isolated block of ancient terrain that covers an area about 800 km long and 280 km wide and stands up to 2 km higher than the surrounding plains. Acheron Fossae is part of a network of fractures that radiates from the Tharsis ‘bulge’ some 1000 km to the south, home to the largest volcanoes on Mars. As the Tharsis region swelled with hot material rising from deep inside Mars as the volcanoes formed, it stretched and pulled apart the crust along lines of weakness over a wide area. This process gave rise to the classic ‘horst and graben’ system – a series of depressions (graben) bounded by faults and uplifted blocks (horsts) either side of the graben.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Mars_Express/A_record_of_ancient_tectonic_stress_on_Mars){:target="_blank" rel="noopener"}


