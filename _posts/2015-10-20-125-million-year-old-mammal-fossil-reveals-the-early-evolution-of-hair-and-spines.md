---
layout: post
title: "125-million-year-old mammal fossil reveals the early evolution of hair and spines"
date: 2015-10-20
categories:
author: University of Chicago Medical Center
tags: [Mammal,Fossil,Hair,Paleontology,Vertebral column,Eutriconodonta,Fur,Dinosaur,Skin,Spine (zoology),Animals]
---


The discovery of a new 125-million-year-old fossil mammal in Spain has pushed back the earliest record of preserved mammalian hair structures and inner organs by more than 60 million years. The unusually well-preserved fossil also contains an external ear lobe, soft tissues of the liver, lung and diaphragm, and plate-like structures made of keratin known as dermal scutes. In 2011, the first mammal fossil at the site was discovered by a team led by Angela D. Buscalioni, PhD, professor of paleontology at the Autonomous University of Madrid, who partnered with collaborators including Luo and Thomas Martin, PhD, professor of paleontology at the University of Bonn, to study the rare specimen. advertisement  Spinolestes had remarkably modern mammalian hair and skin structures, such as compound follicles in which multiple hairs emerge from the same pore. The fossil of Spinolestes contains a large external ear, the earliest-known example in the mammalian fossil record, as well as dermal scutes -- plate-like structures made of skin keratin.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151014134535.htm){:target="_blank" rel="noopener"}


