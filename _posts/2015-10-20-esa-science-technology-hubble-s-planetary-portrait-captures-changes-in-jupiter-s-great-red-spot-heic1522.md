---
layout: post
title: "ESA Science & Technology - Hubble's planetary portrait captures changes in Jupiter's Great Red Spot [heic1522]"
date: 2015-10-20
categories:
author: "$author"  
tags: [Jupiter,Great Red Spot,Planet,Hubble Space Telescope,Solar System,Atmosphere,Voyager 2,Space science,Planets,Bodies of the Solar System,Planets of the Solar System,Sky,Physical sciences,Planemos,Science,Astronomical objects,Outer space,Astronomy,Planetary science]
---


Hubble's planetary portrait captures changes in Jupiter's Great Red Spot [heic1522]  13 October 2015  In this new image of Jupiter a broad range of features has been captured, including winds, clouds and storms. The storm, known as the Great Red Spot, is seen here swirling at the centre of the image of the planet. It has been decreasing in size at a noticeably faster rate from year to year for some time. The observations of Jupiter form part of the Outer Planet Atmospheres Legacy (OPAL) programme, which will allow Hubble to dedicate time each year to observing the outer planets. The collection of maps that will be built up over time will help scientists not only to understand the atmospheres of giant planets in the Solar System, but also the atmospheres of our own planet and of the planets that are being discovered around other stars.

<hr>

[Visit Link](http://sci.esa.int/hubble/56634-hubble-s-planetary-portrait-captures-changes-in-jupiter-s-great-red-spot-heic1522/){:target="_blank" rel="noopener"}


