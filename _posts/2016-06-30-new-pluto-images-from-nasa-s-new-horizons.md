---
layout: post
title: "New Pluto images from NASA's New Horizons"
date: 2016-06-30
categories:
author: "Tricia Talbert"
tags: [Pluto,New Horizons,Charon (moon),Sputnik Planitia,Moon,NASA,Bodies of the Solar System,Planetary science,Space science,Astronomy,Solar System,Outer space,Planemos,Planets of the Solar System,Astronomical objects,Astronomical objects known since antiquity,Planets]
---


Credit: NASA/Johns Hopkins University Applied Physics Laboratory/Southwest Research Institute  New close-up images of Pluto from NASA's New Horizons spacecraft reveal a bewildering variety of surface features that have scientists reeling because of their range and complexity. In the center of this 300-mile (470-kilometer) wide image of Pluto from NASA’s New Horizons spacecraft is a large region of jumbled, broken terrain on the northwestern edge of the vast, icy plain informally called Sputnik Planum, to the right. Credit: NASA/Johns Hopkins University Applied Physics Laboratory/Southwest Research Institute  This image of Pluto’s largest moon Charon, taken by NASA’s New Horizons spacecraft 10 hours before its closest approach to Pluto on July 14, 2015 from a distance of 290,000 miles (470,000 kilometers), is a recently downlinked, much higher quality version of a Charon image released on July 15. Credit: NASA/Johns Hopkins University Applied Physics Laboratory/Southwest Research Institute  This image of Pluto from NASA’s New Horizons spacecraft, processed in two different ways, shows how Pluto’s bright, high-altitude atmospheric haze produces a twilight that softly illuminates the surface before sunrise and after sunset, allowing the sensitive cameras on New Horizons to see details in nighttime regions that would otherwise be invisible. In the left version, faint surface details on the narrow sunlit crescent are seen through the haze in the upper right of Pluto’s disk, and subtle parallel streaks in the haze may be crepuscular rays- shadows cast on the haze by topography such as mountain ranges on Pluto, similar to the rays sometimes seen in the sky after the sun sets behind mountains on Earth.

<hr>

[Visit Link](http://phys.org/news/2015-09-pluto-images-nasa-horizons.html){:target="_blank" rel="noopener"}


