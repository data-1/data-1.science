---
layout: post
title: "Artificial intelligence diagnoses asthma, identifies subtypes"
date: 2018-07-14
categories:
author: ""
tags: []
---


The algorithm has 80% sensitivity and 75% specificity in identifying childhood asthma  Using machine learning, a field closely related to artificial intelligence, upon nuclear magnetic resonance (NMR) spectra of exhaled breath condensate, Delhi-based researchers have been able to improve the diagnosis of childhood asthma and even identify three asthma subtypes. The study included 89 asthmatic children below 18 years and 20 healthy individuals with no history or clinical manifestation of asthma to identify the NMR signatures of asthmatic children; the NMR spectra of 61 asthmatic children with clinical data were used for identifying the subtypes. Children belonging to subtype 1 showed a typical signature of ammonia metabolite but had no family history of asthma. “Not every chemical difference in exhaled breath will translate into clinical difference. Currently, there is no difference in treatment for children belonging to three subtypes.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/health/artificial-intelligence-diagnoses-asthma-identifies-subtypes/article22332586.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


