---
layout: post
title: "Why 2-D? Measuring thickness-dependent electronic properties"
date: 2018-08-02
categories:
author: ""
tags: [Tungsten ditelluride,Physical chemistry,Phases of matter,Electricity,Physics,Chemical product engineering,Electromagnetism,Physical sciences,Condensed matter,Condensed matter physics,Applied and interdisciplinary physics,Materials,Chemistry,Materials science]
---


A FLEET study published last week in Physical Review B quantifies the precise transition point in the promising material tungsten ditelluride (WTe2). Measurements found:  WTe2 thin films cross from 3-D to 2-D electronic systems at thickness of ~ 20 nm  overlap between conduction and valence bands decrease at thickness below ~12 nm, implying that even thinner samples might achieve a bandgap. Angle-dependent quantum oscillation measurements were performed in very high magnetic fields at FLEET CI Alex Hamilton's lab at UNSW, revealing how the material's band structure changed with decreasing thickness, with a 3-D–2-D crossover when the sample thickness was reduced below 26 nm. Tungsten ditelluride (WTe2) is a layered, transition metal dichalcogenide with several promising properties:  extremely large magnetoresistance, with potential for use in magnetic sensors  bulk WTe2 predicted to be a type-II Weyl semimetal  monolayer WTe2 is a high temperature topological insulator, a superconductor, and a ferroelectric. We refer to TMDs as '2-D' because of this layered crystal structure.

<hr>

[Visit Link](https://phys.org/news/2018-08-d-thickness-dependent-electronic-properties.html){:target="_blank" rel="noopener"}


