---
layout: post
title: "Researchers seeking to make computer brains smarter by making them more like our own"
date: 2016-05-02
categories:
author: Sonia Fernandez, University Of California - Santa Barbara
tags: [Memristor,Electronic circuit,Nervous system,Neural network,Brain,Artificial neural network,Memory,Technology,Electrical engineering,Electronics,Branches of science,Computing]
---


For the first time, a circuit of about 100 artificial synapses was proved to perform a simple version of a typical human task: image classification. For all its errors and potential for faultiness, the human brain remains a model of computational power and efficiency for engineers like Strukov and his colleagues, Mirko Prezioso, Farnood Merrikh-Bayat, Brian Hoskins and Gina Adam. In the researchers' demonstration, the circuit implementing the rudimentary artificial neural network was able to successfully classify three letters (z, v and n) by their images, each letter stylized in different ways or saturated with noise. Artificial synaptic circuit of the type used in the demonstration. In the meantime, the researchers will continue to improve the performance of the memristors, scaling the complexity of circuits and enriching the functionality of the artificial neural network.

<hr>

[Visit Link](http://phys.org/news350580891.html){:target="_blank" rel="noopener"}


