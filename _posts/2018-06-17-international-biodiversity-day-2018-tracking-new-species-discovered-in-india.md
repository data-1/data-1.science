---
layout: post
title: "International Biodiversity Day 2018: tracking new species discovered in India"
date: 2018-06-17
categories:
author: ""
tags: []
---


1/9  Nasikabatrachus bhupathi inhabits the eastern slopes of the Western Ghats, near the Srivilliputhur Grizzled Giant Squirrel Wildlife Sanctuary in Tamil Nadu. The discovery is significant as it constitutes additional evidence in favour of the theory of continental drift. So far, only five species of water striders under the subgenus Ptilomera were known in India. India is home to more than 230 balsam species. Three new species of eels were discovered along the northern Bay of Bengal coast.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/energy-and-environment/international-biodiversity-day-2018-tracking-new-species-discovered-in-india/article23958509.ece?utm_source=rss_feed&utm_medium=referral&utm_campaign=rss_syndication){:target="_blank" rel="noopener"}


