---
layout: post
title: "Mystery of how snakes lost their legs solved by reptile fossil"
date: 2015-12-22
categories:
author: University Of Edinburgh
tags: [Snake,Dinilysia,Reptile,Ear,Evolution,Taxa]
---


The 90 million-year-old skull is giving researchers vital clues about how snakes evolved. Comparisons between CT scans of the fossil and modern reptiles indicate that snakes lost their legs when their ancestors evolved to live and hunt in burrows, which many snakes still do today. Scientists used CT scans to examine the bony inner ear of Dinilysia patagonica, a 2-meter long reptile closely linked to modern snakes. Image and representation of brain case and inner ear of Dinilysia patagonica fossil, which scientists at the University of Edinburgh and American Museum of Natural History have used to show that modern snakes lost their legs when their ancestors became expert burrowers. Mark Norell, of the American Museum of Natural History, who took part in the study, said: This discovery would not have been possible a decade ago - CT scanning has revolutionised how we can study ancient animals.

<hr>

[Visit Link](http://phys.org/news/2015-11-mystery-snakes-lost-legs-reptile.html){:target="_blank" rel="noopener"}


