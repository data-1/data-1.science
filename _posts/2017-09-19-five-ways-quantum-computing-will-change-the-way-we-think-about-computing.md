---
layout: post
title: "Five ways quantum computing will change the way we think about computing"
date: 2017-09-19
categories:
author: ""
tags: [Quantum computing,Cloud computing,Computing,API,IBM Quantum Experience,Artificial intelligence,IBM,Software,Computer,Information technology,Information Age,Branches of science,Computer science,Technology]
---


In March 2017, IBM announced the industry's first initiative to build commercially available universal quantum computing systems. One of the first and most promising applications for quantum computing will be in the area of chemistry. Even for simple molecules like caffeine, the number of quantum states in the molecule can be astoundingly large –so large that all the conventional computing memory and processing power scientists could ever build could not handle the problem. The IBM Q systems promise to solve problems that today's computers cannot tackle, for example:  Drug and Materials Discovery: Untangling the complexity of molecular and chemical interactions leading to the discovery of new medicines and materials Supply Chain & Logistics: Finding the optimal path across global systems of systems for ultra-efficient logistics and supply chains, such as optimizing fleet operations for deliveries during the holiday season Financial Services: Finding new ways to model financial data and isolating key global risk factors to make better investments Artificial Intelligence: Making facets of artificial intelligence such as machine learning much more powerful when data sets can be too big such as searching images or video Cloud Security: Making cloud computing more secure by using the laws of quantum physics to enhance private data safety  As part of the IBM Q System, IBM has released a new API (Application Program Interface) for the IBM Quantum Experience that enables developers and programmers to begin building interfaces between its existing five quantum bit (qubit) cloud-based quantum computer and classical computers, without needing a deep background in quantum physics. IBM has also released an upgraded simulator on the IBM Quantum Experience that can model circuits with up to 20 qubits.

<hr>

[Visit Link](https://phys.org/news/2017-05-ways-quantum.html){:target="_blank" rel="noopener"}


