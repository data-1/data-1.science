---
layout: post
title: "Magnetoreception molecule found in the eyes of dogs and primates"
date: 2016-03-15
categories:
author: Max Planck Society
tags: [Cryptochrome,Magnetoreception,Sense,Cone cell,Eye,Retina,Neuroscience,Animals,Cognitive science,Biology]
---


Dogs and some primates can sense the earth magnetic field with the help of molecules in their eyes. In birds, cryptochromes are also involved in the light-dependent magnetic orientation response based on the Earth's magnetic field: cryptochrome 1a is located in photoreceptors in birds' eyes and is activated by the magnetic field. Surprisingly, this inclination compass in birds is linked to the visual system as the magnetic field activates the light-sensitive molecule cryptochrome 1a in the retina of the bird's eye. In all tested species of the other 16 mammalian orders, the researchers found no active cryptochrome 1 in the cone cells of the retina. The researchers thus suspect that some mammals may use the cryptochrome 1 to perceive the Earth's magnetic field.

<hr>

[Visit Link](http://phys.org/news/2016-02-magnetoreception-molecule-eyes-dogs-primates.html){:target="_blank" rel="noopener"}


