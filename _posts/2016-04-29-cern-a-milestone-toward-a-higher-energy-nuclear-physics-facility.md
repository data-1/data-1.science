---
layout: post
title: "CERN: A milestone toward a higher-energy nuclear physics facility"
date: 2016-04-29
categories:
author: Corinne Pralavorio
tags: [On-Line Isotope Mass Separator,Nuclear physics,Physical sciences,Physics,Chemistry]
---


The first HIE-ISOLDE acceleration module was assembled over the last few months in a new clean room. Credit: Maximilien Brice/CERN  CERN's nuclear physics facility ISOLDE will soon be producing radioactive ion beams at higher energies. The purpose of the HIE-ISOLDE (High Intensity and Energy ISOLDE) project, now in the advanced stages of construction at CERN, is to increase the energy and intensity of the ISOLDE beams. The unique ISOLDE facility is dedicated to the production of a large variety of radioactive ion beams for many fields of fundamental and applied research. The new acceleration module will allow HIE-ISOLDE to increase ISOLDE's beam energy from 3 MeV per nucleon to 4.3 MeV per nucleon by the end of 2015.

<hr>

[Visit Link](http://phys.org/news350028749.html){:target="_blank" rel="noopener"}


