---
layout: post
title: "Narrated tour of the International Space Station"
date: 2016-06-03
categories:
author: ""
tags: [Outer space,Space-based economy,Pan-European scientific organizations,European space programmes,European Space Agency,Life in space,Space agencies,Space vehicles,Space exploration,Space science,Human spaceflight,Spacecraft,Flight,Space programs,Astronautics,Spaceflight,Space policy of the European Union,Spaceflight technology,Aerospace,Astronomy,Space traffic management,Space research]
---


This 16-minute narrated tour of the International Space Station shows all the modules of humankind’s weightless laboratory orbiting Earth 400 km above. The video is available in English, Dutch, German, Italian, Spanish and French and is shown to visitors at ESA’s technical heart ESTEC in Noordwijk, The Netherlands. This video is also available in all languages in stereoscopic 3D on ESA's Youtube channel

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2016/05/Narrated_tour_of_the_International_Space_Station){:target="_blank" rel="noopener"}


