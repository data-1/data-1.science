---
layout: post
title: "New resource makes gene-editing technology even more user friendly"
date: 2015-12-21
categories:
author: University of California - San Diego
tags: [CRISPR gene editing,Cas9,CRISPR,Gene,Guide RNA,Genetics,Genomics,Bioinformatics,Omics,Modification of genetic information,Biological techniques and tools,Molecular genetics,Technology,Nucleic acids,Branches of genetics,Biological engineering,Biochemistry,Biology,Life sciences,Molecular biology,Biotechnology]
---


CRISPR/Cas9 is a relatively new genome engineering tool that can target a particular segment of DNA in living cells — such as a gene mutation — and replace it with a new genetic sequence. The CRISPR/Cas9 system has two components: a short “guide RNA” with a sequence matching a particular gene target, and a large protein called Cas9 that cuts DNA precisely at that target. However, finding the best guide RNA match for a specific gene target is a labor-intensive process. The end product is an interactive software for users to find guide RNAs that are predicted to be highly specific and highly active for their gene targets,” said Raj Chari, a research fellow working in the lab of Professor George Church in the Department of Genetics at Harvard Medical School, and a co-first author of the study. Full paper:  “Unraveling CRISPR-Cas9 genome engineering parameters via a library-on-library approach” Nature Methods 2015, published July 13.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/uoc--nrm071515.php){:target="_blank" rel="noopener"}


