---
layout: post
title: "Study grapples with ocean climate stress challenge"
date: 2014-06-24
categories:
author: University Of Queensland
tags: [Sea level rise,Coral reef,Sea,Seagrass,Climate change,Physical geography,Natural environment,Oceanography,Earth sciences,Environmental science,Nature,Applied and interdisciplinary physics,Hydrography,Human impact on the environment,Global environmental issues,Environmental issues,Ecology,Hydrology,Environment,Climate variability and change]
---


Credit: Tane Sinclair-Taylor  (Phys.org) —Researchers are struggling to solve the challenge of predicting climate change impacts on marine environments. UQ Global Change Institute researcher Dr Megan Saunders said the study, published in Nature Climate Change this week, was the first to measure the impact of environmental change on such interactions. As sea levels rise we can expect to see deeper waters over coral reefs, leading to larger waves, more erosion and shoreline damage, and ultimately harsher conditions for seagrass and other ocean communities that rely on wave protection provided by reefs, she said. The hundreds of millions of people who live near tropical coasts will be the first affected by sea level rise. Dr Saunders said the team studied wind data to determine likely changes in wave conditions from rising sea and modelled how the ecosystems might respond to sea level rise.

<hr>

[Visit Link](http://phys.org/news322808737.html){:target="_blank" rel="noopener"}


