---
layout: post
title: "Linux dominates supercomputers as never before"
date: 2014-06-25
categories:
author: Steven Vaughan-Nichols, Senior Contributing Editor, June
tags: []
---


So, it came as no surprise to anyone at the Linux Enterprise End-User Summit near Wall Street that once again the Top500 group found in its latest supercomputer ranking that Linux was the fastest of the fast operating systems. With 97 percent of the world's fastest supercomputers running Linux, the open-source operating system has eliminated almost all its rivals. In the June 2014 Top 500 supercomputer list, the top open-source operating system set a new high with 485 systems out of the fastest 500 running Linux. Of the remaining 16, 13 run Unix. 500 systems’ performance was 90 percent.

<hr>

[Visit Link](http://www.zdnet.com/linux-dominates-supercomputers-as-never-before-7000030890/#ftag=RSS510d04f){:target="_blank" rel="noopener"}


