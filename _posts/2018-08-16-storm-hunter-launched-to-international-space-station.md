---
layout: post
title: "Storm hunter launched to International Space Station"
date: 2018-08-16
categories:
author: ""
tags: [European Space Agency,Atmosphere-Space Interactions Monitor,Atmosphere of Earth,Space programs,Flight,Astronomy,Astronautics,Spacecraft,Human spaceflight,Science,Space science,Spaceflight,Outer space]
---


ESA’s observatory to monitor electrical discharges in the upper atmosphere is on its way to the International Space Station. The Atmosphere-Space Interactions Monitor is riding in the Dragon cargo vehicle that lifted off at 20:30 GMT (16:40 local time) from Kennedy Space Center in Florida, USA. A suite of instruments will search for high-altitude electrical discharges associated with stormy weather conditions. The Atmosphere-Space Interactions Monitor, or ASIM, will be mounted on Europe’s Columbus laboratory, looking straight down at Earth. The crew will install it using the Station’s robotic arm within nine days of arrival.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Storm_hunter_launched_to_International_Space_Station){:target="_blank" rel="noopener"}


