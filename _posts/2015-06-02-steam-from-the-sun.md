---
layout: post
title: "Steam from the Sun"
date: 2015-06-02
categories:
author: ""   
tags: [Solar energy,Water,Steam,Foam,Solar thermal energy,Graphite,Applied and interdisciplinary physics,Technology,Nature,Chemistry,Materials,Physical chemistry,Manufacturing,Energy,Chemical engineering,Physical quantities]
---


Jennifer Chu | MIT News Office  A new material structure developed at MIT generates steam by soaking up the sun. The structure — a layer of graphite flakes and an underlying carbon foam — is a porous, insulating material structure that floats on water. The new material is able to convert 85 percent of incoming solar energy into steam — a significant improvement over recent approaches to solar-powered steam generation. “That’s exciting for us because we’ve come up with a new approach to solar steam generation.”  From sun to steam  The approach itself is relatively simple: Since steam is generated at the surface of a liquid, Ghasemi looked for a material that could both efficiently absorb sunlight and generate steam at a liquid’s surface. As water seeps into the graphite layer, the heat concentrated in the graphite turns the water into steam.

<hr>

[Visit Link](http://theenergycollective.com/energyatmit/438941/steam-sun){:target="_blank" rel="noopener"}


