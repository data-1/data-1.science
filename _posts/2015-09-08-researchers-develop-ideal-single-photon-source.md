---
layout: post
title: "Researchers develop ideal single-photon source"
date: 2015-09-08
categories:
author: University Of Basel
tags: [Single-photon source,Photon,Quantum dot,Technology,Quantum mechanics,Science,Branches of science]
---


Semiconductor quantum dot emitting a stream of identical photons. With the help of a semiconductor quantum dot, physicists at the University of Basel have developed a new type of light source that emits single photons. A single-photon source never emits two or more photons at the same time. For the first time ever, the scientists have managed to control the nuclear spin to such an extent that even photons sent out at very large intervals are the same color. Quantum cryptography and quantum communication are two potential areas of application for single-photon sources.

<hr>

[Visit Link](http://phys.org/news/2015-09-ideal-single-photon-source.html){:target="_blank" rel="noopener"}


