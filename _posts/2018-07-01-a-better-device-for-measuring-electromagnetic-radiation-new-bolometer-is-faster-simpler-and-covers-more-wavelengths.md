---
layout: post
title: "A better device for measuring electromagnetic radiation: New bolometer is faster, simpler, and covers more wavelengths"
date: 2018-07-01
categories:
author: "Massachusetts Institute of Technology"
tags: [Bolometer,Electromagnetic radiation,Applied and interdisciplinary physics,Forms of energy,Science,Physical quantities,Optics,Physical sciences,Transport phenomena,Physical chemistry,Electrical engineering,Waves,Electrodynamics,Chemistry,Physics,Physical phenomena,Radiation,Electromagnetism]
---


We believe that our work opens the door to new types of efficient bolometers based on low-dimensional materials, says Englund, the paper's senior author. He says the new system, based on the heating of electrons in a small piece of a two-dimensional form of carbon called graphene, for the first time combines both high sensitivity and high bandwidth -- orders of magnitude greater than that of conventional bolometers -- in a single device. The new system also can operate at any temperature, he says, unlike current devices that have to be cooled to extremely low temperatures. This is the first device of this kind that has no limit on temperature, Efetov says. For astronomical observations, the new system could help by filling in some of the remaining wavelength bands that have not yet had practical detectors to make observations, such as the terahertz gap of frequencies that are very difficult to pick up with existing systems.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180611133440.htm){:target="_blank" rel="noopener"}


