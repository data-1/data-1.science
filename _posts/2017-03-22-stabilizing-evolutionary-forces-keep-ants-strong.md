---
layout: post
title: "Stabilizing evolutionary forces keep ants strong"
date: 2017-03-22
categories:
author: "Hokkaido University"
tags: [Natural selection,Species,Genetics,Genetic drift,Ant,Evolution,Evolutionary biology,Biological evolution,Biology]
---


A type of natural selection, called stabilizing selection, is thought to maintain functional characteristics in species. They also assumed that the less functionally important hind spur would be relatively unaffected by natural selection forces. The researchers found significant genetic differences between ten ant populations of the species, implying there is limited gene flow between them. This means that any observed morphological differences within and between the populations would reflect how strongly natural selection acted on each character. Comparing the spurs in the ant populations, they found significant variations in the lengths of the less important hind spurs, which corresponds to the random genetic drift that occurs over time.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-12/hu-sef122016.php){:target="_blank" rel="noopener"}


