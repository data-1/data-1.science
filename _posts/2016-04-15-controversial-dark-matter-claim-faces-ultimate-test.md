---
layout: post
title: "Controversial dark-matter claim faces ultimate test"
date: 2016-04-15
categories:
author: Castelvecchi, Davide Castelvecchi, You Can Also Search For This Author In
tags: []
---


Within three years, the experiments will be able to either confirm the existence of dark matter — or rule the claim out once and for all, say the physicists who work on them. All will use sodium iodide crystals to detect dark matter, which no full-scale experiment apart from DAMA’s has done previously. “But how to interpret that signal — whether it’s from dark matter or something else — is not clear.”  No other full-scale experiment has used sodium iodide in its detector, although the Korea Invisible Mass Search (KIMS), in South Korea, used caesium iodide. The KIMS and DM-Ice teams have built a sodium iodide detector together at Yangyang Underground Laboratory, 160 kilometres east of Seoul. Together, KIMS/DM-Ice and ANAIS will have about 200 kilograms of sodium iodide, and they will pool their data.

<hr>

[Visit Link](http://www.nature.com/news/controversial-dark-matter-claim-faces-ultimate-test-1.19684?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


