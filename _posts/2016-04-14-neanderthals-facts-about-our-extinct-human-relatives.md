---
layout: post
title: "Neanderthals: Facts About Our Extinct Human Relatives"
date: 2016-04-14
categories:
author: Jessie Szalay
tags: [Neanderthal,Interbreeding between archaic and modern humans,Human,Early modern human,Homo,Max Planck Institute for Evolutionary Anthropology]
---


It is theorized that for a time, Neanderthals probably shared the Earth with other Homo species. It took a little bit longer for the brain to grow in Neanderthals than in modern humans, said study co-lead author Antonio Rosas, chairman of the paleoanthropology group at Spain's National Museum of Natural Sciences in Madrid. Neanderthals typically lived to be about 30 years old, though some lived longer. Neanderthal expert Erik Trinhaus has long promoted the interbreeding hypothesis, but the theory really caught fire when a 2010 study published in Science magazine determined that Neanderthal DNA is 99.7 percent identical to modern human DNA (a chimp's is 99.8 percent identical). They discovered that the Neanderthal bones were more than 50,000 years old.

<hr>

[Visit Link](http://www.livescience.com/28036-neanderthals-facts-about-our-extinct-human-relatives.html){:target="_blank" rel="noopener"}


