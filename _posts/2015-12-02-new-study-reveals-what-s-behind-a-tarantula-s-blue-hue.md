---
layout: post
title: "New study reveals what's behind a tarantula's blue hue"
date: 2015-12-02
categories:
author: University of California - San Diego 
tags: [Scripps Institution of Oceanography,Science]
---


Researchers from Scripps Institution of Oceanography at UC San Diego and University of Akron found that many species of tarantulas have independently evolved the ability to grow blue hair using nanostructures in their exoskeletons, rather than pigments. In a cover article in the Nov. 10 of Chemistry of Materials, Deheyn and colleagues published new findings on the nanostructure of ragweed pollen, which shows interesting optical properties and has possible biomimicry applications. Our inspiration is to learn about how nature evolves unique traits that we could mimic to benefit future technologies. ###  Scripps Institution of Oceanography: scripps.ucsd.edu  Scripps News: scrippsnews.ucsd.edu  About Scripps Institution of Oceanography  Scripps Institution of Oceanography at the University of California, San Diego, is one of the oldest, largest, and most important centers for global science research and education in the world. Learn more at http://www.ucsd.edu.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-11/uoc--nsr113015.php){:target="_blank" rel="noopener"}


