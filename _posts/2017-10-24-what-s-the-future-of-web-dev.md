---
layout: post
title: "What's the Future of Web Dev?"
date: 2017-10-24
categories:
author: Oct.
tags: []
---


To gather insights on the state of web application development today, we spoke with 12 executives who are familiar with the current state of the industry and asked them, What’s the future of web applications from your point of view - where do the greatest opportunities lie? Building apps to serve end users. Know how the app is performing for each user. Progressive web application technology. What do you see as the future of web application development?

<hr>

[Visit Link](https://dzone.com/articles/whats-the-future-of-web-dev?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


