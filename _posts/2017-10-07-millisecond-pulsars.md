---
layout: post
title: "Millisecond pulsars"
date: 2017-10-07
categories:
author: "Harvard-Smithsonian Center For Astrophysics"
tags: [Pulsar,Star,Neutron star,Millisecond pulsar,47 Tucanae,Binary star,Supernova,Star cluster,Nature,Outer space,Physics,Stars,Astronomical objects,Physical sciences,Stellar astronomy,Space science,Astronomy]
---


Astronomers have measured the orbital parameters of four millisecond pulsars in the globular cluster 47 Tuc and modeled their possible formation and evolution paths. Credit: European Space Agency & Francesco Ferraro (Bologna Astronomical Observatory)  When a star with a mass of roughly ten solar masses finishes its life, it explodes as a supernova, leaving behind a neutron star as remnant ash. There are nearly 3000 known millisecond pulsars. Their crowded environments provide ideal conditions for forming binary stars, and nearly eighty percent of the pulsars in globular clusters are millisecond pulsars. The astronomers estimate that this binary pulsar probably formed when a neutron star encountered a binary star, captured its companion from the binary, and then began accreting material from it to become a pulsar.

<hr>

[Visit Link](http://phys.org/news/2016-10-millisecond-pulsars.html){:target="_blank" rel="noopener"}


