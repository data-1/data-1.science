---
layout: post
title: "The end-Cretaceous extinction unleashed modern shark diversity"
date: 2018-08-03
categories:
author: "Uppsala University"
tags: [Shark,CretaceousPaleogene extinction event,Biodiversity,Extinction,Extinction event,Dinosaur,Animals]
---


A study that examined the shape of hundreds of fossilized shark teeth suggests that modern shark biodiversity was triggered by the end-Cretaceous mass extinction event, about 66 million years ago. Ground sharks (Carcharhiniformes) are the most diverse shark group living today, with over 200 different species. Fortunately, shark teeth can tell us a lot about their biology, including information about diet, which can shed light on the mechanisms behind their extinction and survival. Going into this study, we knew that sharks underwent important losses in species richness across the extinction. Carcharhiniforms are the most common shark group today and it would seem that the initial steps towards this dominance started approximately 66 million years ago, said Mr. Bazzi, who remarks that further research is still needed to understand the diversity patterns of other shark groups, along with the relationship between diet and tooth morphology.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180802141635.htm){:target="_blank" rel="noopener"}


