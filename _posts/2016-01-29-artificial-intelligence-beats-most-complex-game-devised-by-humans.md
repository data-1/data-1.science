---
layout: post
title: "Artificial Intelligence Beats 'Most Complex Game Devised by Humans'"
date: 2016-01-29
categories:
author: Tia Ghose
tags: [AlphaGo,Demis Hassabis,Artificial intelligence,Deep learning,Go (game),Deep Blue (chess computer),DeepMind,Cognition,Computer science,Technology,Cognitive science,Computing,Branches of science]
---


What's more, the new system, called AlphaGo, defeated the human player by learning the game from scratch using an approach known as deep learning, the researchers involved say. As a result, the best AI systems, such as IBM's Deep Blue, have only managed to defeat amateur human Go players. AlphaGo trounced rival AI systems about 99.8 percent of the time, and defeated the reigning European Go champion, Fan Hui, in a tournament, winning all five games. You can think of him as the Roger Federer of the Go world, Hassabis said. My overall impression was that AlphaGo seemed stronger than Fan, but I couldn't tell by how much.

<hr>

[Visit Link](http://www.livescience.com/53497-ai-defeats-human-go-player.html){:target="_blank" rel="noopener"}


