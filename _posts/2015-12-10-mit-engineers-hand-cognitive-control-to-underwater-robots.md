---
layout: post
title: "MIT engineers hand 'cognitive' control to underwater robots"
date: 2015-12-10
categories:
author: Massachusetts Institute of Technology
tags: [NASA,Spaceflight,Flight,Space science,Outer space,Technology,Science,Spacecraft,Astronautics]
---


Using the system devised by the MIT team, the robot can then plan out a mission, choosing which locations to explore, in what order, within a given timeframe. The MIT researchers tested their system on an autonomous underwater glider, and demonstrated that the robot was able to operate safely among a number of other autonomous vehicles, while receiving higher-level commands. Williams, who at the time was working at NASA's Ames Research Center, was tasked with developing an autonomous system that would enable spacecraft to diagnose and repair problems without human assistance. By giving robots control of higher-level decision-making, Williams says such a system would free engineers to think about overall strategy, while AUVs determine for themselves a specific mission plan. For instance, with an autonomous system, robots may not have to be in continuous contact with engineers, freeing the vehicles to explore more remote recesses of the sea.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/miot-meh050815.php){:target="_blank" rel="noopener"}


