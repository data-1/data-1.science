---
layout: post
title: "Hot water activity on icy moon’s seafloor"
date: 2015-09-03
categories:
author: "$author"   
tags: [Enceladus,CassiniHuygens,Physical sciences,Planetary science,Astronomy,Space science,Outer space,Planets of the Solar System,Solar System,Bodies of the Solar System,Astronomical objects,Earth sciences,Nature,Planets]
---


Science & Exploration Hot water activity on icy moon’s seafloor 11/03/2015 36338 views 178 likes  Tiny grains of rock detected by the international Cassini spacecraft orbiting Saturn point to hydrothermal activity on the seafloor of its icy moon Enceladus. Enceladus plumes Using Cassini’s Cosmic Dust Analyser, scientists have discovered a population of tiny dust grains, just 2–8 nm in radius, in orbit around Saturn. They believe that these silicon-rich grains originate on the seafloor of Enceladus, where hydrothermal processes are at work. As the hot water travels upward, it comes into contact with cooler water, causing the minerals to condense out and form nano-grains of ‘silica’ floating in the water. The methane could also be produced by hydrothermal processes at the rock-water boundary at the bottom of Enceladus’s ocean, and/or by the melting of a type of methane-rich ice, before subsequently percolating to the surface.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Cassini-Huygens/Hot_water_activity_on_icy_moon_s_seafloor){:target="_blank" rel="noopener"}


