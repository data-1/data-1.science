---
layout: post
title: "Nanoscale imaging looks deep into magnetic materials – Physics World"
date: 2017-10-08
categories:
author: ""
tags: [Magnetism,X-ray,Polarization (waves),Magnetic domain,Circular polarization,Light,Circular dichroism,Tomography,Physics,Spin (physics),Magnetic monopole,Magnetic structure,Electrical engineering,Physical sciences,Electromagnetism,Atomic molecular and optical physics,Science,Chemistry,Optics,Applied and interdisciplinary physics,Electromagnetic radiation]
---


Twist and turn: reconstruction of a magnetic vortex  A new technique for taking nanoscale images of the magnetic properties of materials has been unveiled by researchers in Switzerland. Higher-energy “hard” X-rays penetrate much deeper into materials and are used in tomography. Using a computer algorithm of their own design, the researchers used the data to reconstruct the magnetism at each point in the pillar with a spatial resolution of around 100 nm. Bloch points  This allowed the team to make the first-ever direct experimental observations of Bloch points. These are monopole-like points where magnetic domains point in different directions.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/aug/03/nanoscale-imaging-looks-deep-into-magnetic-materials){:target="_blank" rel="noopener"}


