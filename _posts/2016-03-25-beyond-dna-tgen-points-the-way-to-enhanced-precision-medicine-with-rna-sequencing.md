---
layout: post
title: "Beyond DNA: TGen points the way to enhanced precision medicine with RNA sequencing"
date: 2016-03-25
categories:
author: The Translational Genomics Research Institute
tags: [RNA-Seq,DNA sequencing,Cancer,RNA,Genetics,DNA,Extracellular RNA,Genetic analysis,Virus,Genomics,Biochemistry,Life sciences,Medicine,Biotechnology,Clinical medicine,Biology,Health sciences,Health,Medical specialties]
---


Deeper genetic analysis with RNA sequencing provides better diagnostics and treatments for patients with everything from cancer to deadly viruses  PHOENIX, Ariz. -- March 21, 2016 -- Uncovering the genetic makeup of patients using DNA sequencing has in recent years provided physicians and their patients with a greater understanding of how best to diagnose and treat the diseases that plague humanity. Now, researchers at the Translational Genomics Research Institute (TGen) are showing how an even more detailed genetic analysis using RNA sequencing can vastly enhance that understanding, providing doctors and their patients with more precise tools to target the underlying causes of disease, and help recommend the best course of action. In their review, published today in the journal Nature Reviews Genetics, TGen scientists highlight the many advantages of using RNA-sequencing in the detection and management of everything from cancer to infectious diseases, such as Ebola and the rapidly spreading Zika virus. Commercial RNA-seq tests are now available, and provide the opportunity for clinicians to more comprehensively profile cancer and use this information to guide treatment selection for their patients, the review said. TGen is focused on helping patients with neurological disorders, cancer, and diabetes, through cutting edge translational research (the process of rapidly moving research towards patient benefit).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/ttgr-bdt032116.php){:target="_blank" rel="noopener"}


