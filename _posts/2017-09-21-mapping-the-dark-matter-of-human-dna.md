---
layout: post
title: "Mapping the 'dark matter' of human DNA"
date: 2017-09-21
categories:
author: "Saarland University"
tags: [Gene,Structural variation,Genome,Human genome,Genetics,DNA,Coding region,Protein,Biochemistry,Molecular biology,Biology,Life sciences,Biological engineering,Biotechnology,Bioinformatics]
---


These new data enable researchers from all over the world to study the DNA variants and use the results to better understand genetic diseases. However, large structural variants often occur just before or after the coding part of a gene. This particular gene is a member of the ZNF gene family that was known from the reference genomes of several species of apes. The fact that these and other pieces of dark matter now have been placed on the genetic map enables scientists worldwide to study them and use the results to better understand human genetic diseases. One of the main goals of the study is to map the genome of the Dutch population and all its variants.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/su-mt100716.php){:target="_blank" rel="noopener"}


