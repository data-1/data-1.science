---
layout: post
title: "Sweeping over the south pole of Mars"
date: 2016-06-30
categories:
author: "European Space Agency"
tags: [Mars,Mars Express,Hellas Planitia,Planum Australe,Image resolution,High Resolution Stereo Camera,Planets of the Solar System,Space science,Outer space,Planetary science,Astronomy,Planets,Astronomical objects known since antiquity,Bodies of the Solar System,Terrestrial planets]
---


This sweeping view by ESA’s Mars Express extends from the planet’s south polar ice cap and across its cratered highlands to the Hellas Basin (top left) and beyond. The image was created using data from the nadir channel, the field of view of which is aligned perpendicular to the surface of Mars, and the colour channels of HRSC. That is, for each colour channel, these markers are overlain to produce the colour image. Credit: ESA/DLR/FU Berlin, CC BY-SA 3.0 IGO  An unusual observation by Mars Express shows a sweeping view over the planet's south polar ice cap and across its ancient, cratered highlands. But in this unusual observation, known as a 'broom calibration' image, Mars Express turned such that its camera panned over the surface far above the planet, close to its furthest point along its orbit, in this case at around 9900 km.

<hr>

[Visit Link](http://phys.org/news/2015-09-south-pole-mars.html){:target="_blank" rel="noopener"}


