---
layout: post
title: "First dinosaurs arose in an evolutionary eye-blink"
date: 2015-12-22
categories:
author: Witze, Alexandra Witze, You Can Also Search For This Author In
tags: []
---


The team studied a rock formation known as the Chañares, which contains fossils of dinosaur ancestors. By dating the Chañares rocks precisely, the scientists hoped to better understand when the dinosaur ancestors preserved there gave way to Ischigualasto dinosaurs. Researchers dated rocks in Argentina's Chañares formation to investigate dinosaur origins. Credit: Adriana Mancuso  Fast break  The researchers studied two rock samples from the Chañares. There is little physical difference between the last dinosaur ancestors and the first dinosaurs, says Irmis.

<hr>

[Visit Link](http://www.nature.com/news/first-dinosaurs-arose-in-an-evolutionary-eye-blink-1.18953){:target="_blank" rel="noopener"}


