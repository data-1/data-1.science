---
layout: post
title: "How to Impose High CPU Load and Stress Test on Linux Using ‘Stress-ng’ Tool"
date: 2015-09-29
categories:
author: Aaron Kili
tags: [Yum (software),APT (software),Computer science,Operating system families,Digital media,Information Age,Software engineering,Software development,Computer engineering,Computers,Computer architecture,System software,Technology,Software,Computing,Operating system technology]
---


As a System Administrator, you may want to examine and monitor the status of your Linux systems when they are under stress of high load. How Do I use stress on Linux systems? Next, run the stress command to spawn 8 workers spinning on sqrt() with a timeout of 20 seconds. After running stress, again run the uptime command and compare the load average. To spwan one worker of malloc() and free() functions with a timeout of 60 seconds, run the following command.

<hr>

[Visit Link](http://www.tecmint.com/linux-cpu-load-stress-test-with-stress-ng-tool/){:target="_blank" rel="noopener"}


