---
layout: post
title: "Ocean acidification accelerates erosion of coral reefs: Researchers found that increased activity by worms and other organisms act on coral skeletons"
date: 2018-08-14
categories:
author: "University of Miami Rosenstiel School of Marine & Atmospheric Science"
tags: [Coral reef,Ocean acidification,Freshwater acidification,Ocean,News aggregator,Systems ecology,Environmental issues,Marine biology,Biogeochemistry,Aquatic ecology,Human impact on the environment,Ecology,Hydrology,Hydrography,Nature,Applied and interdisciplinary physics,Earth sciences,Physical geography,Oceanography,Natural environment,Environmental science]
---


Scientists studying naturally high carbon dioxide coral reefs in Papua New Guinea found that erosion of essential habitat is accelerated in these highly acidified waters, even as coral growth continues to slow. The new research by the University of Miami Rosenstiel School's Cooperative Institute for Marine and Atmospheric Studies (CIMAS), NOAA, and the Australian Institute of Marine Science has important implications for coral reefs around the world as the ocean become more acidic as a result of global change. The study, published in the journal Proceedings of the Royal Society B, measured changes in the structural habitat at two reefs situated in volcanically acidified water off remote Papau New Guinea and, for the first time, found increased activity of worms and other organisms that bore into the reef structure, resulting in a loss of the framework that is the foundation of coral reef ecosystems. These 'champagne reefs' are natural analogs of how coral reefs may look in 100 years if carbon dioxide continues to rise and ocean acidification conditions continue to worsen. This is the first study to demonstrate that ocean acidification is a two-front assault on coral reefs, simultaneously slowing the growth of skeleton, and speeding up the rate at which old reef habitats are eroded, said Ian Enochs, a coral ecologist at CIMAS and NOAA's Atlantic Oceanographic and Meteorological Laboratory and lead author of the study.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/11/161122123845.htm){:target="_blank" rel="noopener"}


