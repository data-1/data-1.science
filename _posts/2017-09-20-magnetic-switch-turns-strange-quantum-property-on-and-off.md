---
layout: post
title: "Magnetic switch turns strange quantum property on and off"
date: 2017-09-20
categories:
author: "National Institute of Standards and Technology (NIST)"
tags: [Geometric phase,Electron,Quantum mechanics,Graphene,Wave,Phase (waves),Physics,Applied and interdisciplinary physics,Physical sciences,Theoretical physics,Science,Chemistry,Electromagnetism]
---


But when an applied magnetic field reaches a critical value, it acts as a switch, altering the shape of the orbits and causing the electrons to possess different physical properties after completing a full circuit. The Berry phase is associated with the wave function of a particle, which in quantum theory describes a particle's physical state. advertisement  Although the Berry phase is a purely quantum phenomenon, it has an analog in non-quantum systems. Several members of the current research team -- based at the Massachusetts Institute of Technology and Harvard University -- developed the theory for the Berry phase switch. To study the Berry phase and create the switch, NIST team member Fereshte Ghahari built a high-quality graphene device to study the energy levels and the Berry phase of electrons corralled within the graphene.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/05/170525141541.htm){:target="_blank" rel="noopener"}


