---
layout: post
title: "Squashing inclusivity bugs in open source software"
date: 2018-08-03
categories:
author: "Anita Sarma"
tags: [Problem solving,Bias,Open source,Gender,Cognition,Branches of science]
---


But can the software itself be gender biased? So, how do you know if your software is biased? Research over the past 10 years across numerous populations has identified the following five problem-solving facets that impact how individuals use software:  Motivations for using the software Style of processing information Computer self-efficacy Attitudes toward technological risks Preferred styles for learning technology  The GenderMag method has identified inclusivity issues in real-world software teams. Our research shows that open source software would benefit from considering these individual differences in problem-solving styles in software design, as they might be contributing to open source communities' low diversity rates. Using the GenderMag cognitive walkthrough, the open source teams identified gender bias in more than 70% of the tool issues they uncovered.

<hr>

[Visit Link](https://opensource.com/article/18/8/inclusivity-bugs-open-source-software){:target="_blank" rel="noopener"}


