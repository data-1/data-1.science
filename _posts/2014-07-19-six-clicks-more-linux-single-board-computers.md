---
layout: post
title: "Six Clicks: More Linux single-board computers"
date: 2014-07-19
categories:
author: Steven Vaughan-Nichols, Senior Contributing Editor, July
tags: []
---


DIY and gadget fans alike love the Raspberry Pi. Now, they'll have more to love with the new Raspberry Pi B+. The improvements come with a more space efficient microSD card slot in place of the SDCard slot. It also has four USB 2.0 ports instead of the Model B's two. The new single-board computer (SBC) also has 40 general purpose Input/Output (GPIO) pins.

<hr>

[Visit Link](http://www.zdnet.com/six-clicks-more-linux-single-board-computers-7000031776/#ftag=RSS510d04f){:target="_blank" rel="noopener"}


