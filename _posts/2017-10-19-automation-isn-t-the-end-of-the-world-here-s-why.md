---
layout: post
title: "Automation Isn't The End Of The World -- Here's Why"
date: 2017-10-19
categories:
author:  
tags: [General Electric,Technology,Entrepreneurship,Electrical grid,Analytics,Computer network,Economic growth,Innovation,Culture,Sustainable energy,Energy development,Employment,3D printing,Infrastructure,Engine,Automation,Sustainability,Governance,Market (economics),Hybrid power,Economy]
---


What keeps you up at night? It is true:    It is true: if a routine task can be performed cheaper, faster and better by a robot, there is a chance it will be . What is also true – and even more certain – is that machines push us to specialize in our competitive advantages: more “human” work, creative and social intelligence, interpersonal and non-routine tasks are what makes us resilient and adaptive to change. The future of work is about much more than automation. For many people worldwide, the answer is unemployment.

<hr>

[Visit Link](http://www.gereports.com/automation-isnt-end-world-heres/){:target="_blank" rel="noopener"}


