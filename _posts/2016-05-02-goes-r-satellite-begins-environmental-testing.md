---
layout: post
title: "GOES-R satellite begins environmental testing"
date: 2016-05-02
categories:
author: Lauren Gaches, Nasa'S Goddard Space Flight Center
tags: [GOES-16,Satellite,Weather,Space weather,Weather forecasting,National Oceanic and Atmospheric Administration,Outer space,Spaceflight,Space science,Astronautics,Sky,Flight,Spacecraft,Science]
---


Credit: Lockheed Martin  The GOES-R satellite, slated to launch in 2016, is ready for environmental testing. The GOES-R satellite and its instruments will undergo a variety of rigorous tests which includes subjecting the satellite to vibration, acoustics and temperature testing as part of this process. Once launched, the satellite will be known as GOES-16 and will immediately be placed in a test location at 89.5 degrees West longitude for an extended checkout period. The GOES-R series satellites will provide continuous imagery and atmospheric measurements of Earth's Western Hemisphere and space weather monitoring to provide critical atmospheric, hydrologic, oceanic, climatic, solar and space data. Explore further All instruments for GOES-R satellite now integrated with spacecraft

<hr>

[Visit Link](http://phys.org/news351447470.html){:target="_blank" rel="noopener"}


