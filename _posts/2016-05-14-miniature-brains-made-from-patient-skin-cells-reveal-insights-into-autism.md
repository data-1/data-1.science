---
layout: post
title: "Miniature brains made from patient skin cells reveal insights into autism"
date: 2016-05-14
categories:
author: Cell Press
tags: [Organoid,Autism,Cerebral organoid,Brain,Disease,Medicine,Health,Life sciences,Health sciences,Clinical medicine,Neuroscience]
---


Understanding diseases like autism and schizophrenia that affect development of the brain has been challenging due to both the complexity of the diseases and the difficulty of studying developmental processes in human tissues. In a study published July 16 in Cell, researchers have made steps toward overcoming these challenges by converting skin cells from autism patients into stem cells and growing them into tiny brains in a dish, revealing unexpected mechanisms of the disease. Most autism research has taken the approach of combing through patient genomes for mutations that may underlie the disorder and then using animal or cell-based models to study the genes and their possible roles in brain development. When the researchers analyzed the patient organoids, they uncovered altered expression networks for genes controlling neuronal development. The authors are now using their data to home in on the difficult-to-find mutations or epigenetic changes responsible for the gene expression alterations and neuronal imbalance observed in the study.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150716123836.htm){:target="_blank" rel="noopener"}


