---
layout: post
title: "One step closer to crops with twice the yield"
date: 2017-11-19
categories:
author: "Wageningen University"
tags: [Plant,Photosynthesis,Agriculture,Plant breeding,Genetics,Arabidopsis thaliana]
---


Credit: Wageningen University  Scientists from Wageningen University & Research have found natural genetic variation for photosynthesis in plants and are unravelling it to the DNA level. In the long term, breeding on improved photosynthesis could make crops produce more yield with the same amount of soil, water and nutrients. We have known for some time that plants can respond differently to light, as is shown in the efficiency of their photosynthesis. Selection on photosynthesis in breeding  Nowadays, we breed crops in an environment that is far easier to control than the original natural conditions. So how come there is so little selection on more efficient photosynthesis in breeding?

<hr>

[Visit Link](https://phys.org/news/2017-11-closer-crops-yield.html){:target="_blank" rel="noopener"}


