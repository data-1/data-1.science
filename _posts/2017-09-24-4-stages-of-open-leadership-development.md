---
layout: post
title: "4 stages of open leadership development"
date: 2017-09-24
categories:
author: "DeLisa Alexander
(Red Hat)"
tags: [Leadership,Leadership development,Red Hat,Organization,Cognition,Branches of science,Communication,Cognitive science,Behavioural sciences,Psychological concepts]
---


In our open organization, we saw that leadership capabilities develop in different stages, based on influence and impact. A person is a leader when they take initiative and collaborate to get their work done, ultimately “giving” far more to the organization (via their contributions) than they “take” (in the form of support and help from other people). Team leadership  When a personal leader begins to extend their influence beyond their personal domain, they reach the next stage: team leadership. This is when their impact becomes additive; when they learn how to tap into, combine, and align the individual strengths of every member of a group to bring about a shared vision and focus on results. Developing leaders at all levels  Based on these leadership stages and the increasing needs of our growing organization, we realized that we needed to meet every Red Hatter where they are, understand and leverage their strengths, and support them in building additional leadership capabilities.

<hr>

[Visit Link](https://opensource.com/open-organization/17/2/4-stages-open-leadership-development){:target="_blank" rel="noopener"}


