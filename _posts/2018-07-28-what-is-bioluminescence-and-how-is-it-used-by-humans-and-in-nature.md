---
layout: post
title: "What is bioluminescence and how is it used by humans and in nature?"
date: 2018-07-28
categories:
author: "Catrin F. Williams, The Conversation"
tags: [Bioluminescence,Euprymna scolopes,Aliivibrio fischeri,Nature,Organisms,Biology]
---


What Darwin saw was bioluminescent sea creatures, flickering light in response to physical disruption. Bioluminescence, the production and emission of light by living organisms, became a sticking point for Darwin. Researchers have since found that this form of chemiluminescence, produces blue-green light as a result of the oxidation of a compound called luciferin (the light-bringer) by an enzyme called luciferase. It is perhaps the medical applications of bioluminescence that have attracted the most excitement. In 2008, the Nobel Prize in Chemistry was awarded for the discovery and development of green fluorescent protein (GFP).

<hr>

[Visit Link](https://phys.org/news/2018-07-bioluminescence-humans-nature.html){:target="_blank" rel="noopener"}


