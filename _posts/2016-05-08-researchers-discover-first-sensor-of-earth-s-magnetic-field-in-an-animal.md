---
layout: post
title: "Researchers discover first sensor of Earth's magnetic field in an animal"
date: 2016-05-08
categories:
author: University Of Texas At Austin
tags: [Caenorhabditis elegans,Brain,Earths magnetic field,Neuroscience]
---


Inside the head of the worm C. elegans, the TV antenna-like structure at the tip of the AFD neuron (green) is the first identified sensor for Earth's magnetic field. A team of scientists and engineers at The University of Texas at Austin has identified the first sensor of the Earth's magnetic field in an animal, finding in the brain of a tiny worm a big clue to a long-held mystery about how animals' internal compasses work. Chances are that the same molecules will be used by cuter animals like butterflies and birds, said Jon Pierce-Shimomura, assistant professor of neuroscience in the College of Natural Sciences and member of the research team. The neuron sporting a magnetic field sensor, called an AFD neuron, was already known to sense carbon dioxide levels and temperature. The researchers discovered the worms' magnetosensory abilities by altering the magnetic field around them with a special magnetic coil system and then observing changes in behavior.

<hr>

[Visit Link](http://phys.org/news353760864.html){:target="_blank" rel="noopener"}


