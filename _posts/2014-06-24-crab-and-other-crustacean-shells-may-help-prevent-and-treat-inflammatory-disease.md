---
layout: post
title: "Crab and other crustacean shells may help prevent and treat inflammatory disease"
date: 2014-06-24
categories:
author: Florida Atlantic University 
tags: [Inflammatory bowel disease,Inflammation,Crohns disease,Allergy,Health,Health care,Causes of death,Diseases and disorders,Immunology,Epidemiology,Health sciences,Medicine,Clinical medicine,Medical specialties]
---


Microparticles in crab, shrimp and lobster shells have anti-inflammatory mechanisims that could lead to the development of novel preventive and therapeutic strategies for Inflammatory Bowel Disease  Yoshimi Shibata, Ph.D., professor of biomedical science in the Charles E. Schmidt College of Medicine at Florida Atlantic University, has received a $380,552 grant from the National Institute of Complementary and Alternative Medicine of the National Institutes of Health (NIH) to further investigate how microparticles called chitin found in crab, shrimp and lobster shells have anti-inflammatory mechanisms that could lead to the development of novel preventive and therapeutic strategies for individuals who suffer from inflammatory bowel disease (IBD) and others diseases. Chitin microparticles are also non-toxic, biodegradable and non-allergenic, and therefore safe for oral ingestion as a dietary food supplement. Chronic inflammation on the other hand harms instead of heals because the immune system attack never stops. In this new study, we are going to focus on intestinal macrophages and how these mimetic microbes we have developed can produce anti-inflammatory activities, normalize the gut bacterial flora and ultimately improve the symptoms associated with inflammatory bowel disease, said Shibata. FAU is ranked as a High Research Activity institution by the Carnegie Foundation for the Advancement of Teaching.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/fau-cao062314.php){:target="_blank" rel="noopener"}


