---
layout: post
title: "Replay ATV-5 liftoff"
date: 2015-09-03
categories:
author: "$author"   
tags: [Automated Transfer Vehicle,Guiana Space Centre,European Space Agency,Space access,Space vehicles,Space agencies,Rocketry,Space policy of the European Union,Pan-European scientific organizations,Space traffic management,Spacecraft,Space organizations,Spaceflight technology,Space industry,Space policy,Astronautics,Space launch vehicles,Rockets and missiles,Space-based economy,Transport authorities,Human spaceflight,Aerospace,Vehicles,Flight,Government research,Transport,Space exploration,Spaceflight,Space programs,Outer space,European space programmes]
---


Liftoff of an Ariane 5 launcher from Europe’s spaceport in French Guiana with ESA’s last Automated Transfer vehicle to the Space Station. The fifth and final mission of ESA’s Automated Transfer Vehicle got off to a flying start with its launch from Europe’s Spaceport in Kourou, French Guiana, heading for the International Space Station. Georges Lemaître is the fifth ATV built and launched by ESA as part of Europe’s contribution to cover the operational costs for using the Space Station. Named after the Belgian scientist who formulated the Big Bang Theory, ATV Georges Lemaître lifted off at 23:47 GMT on 29 July (01:47 CEST 30 July, 20:47 local time 29 July) on an Ariane 5 ES rocket. The vehicle will deliver 6561 kg of freight, including 2628 kg of dry cargo and 3933 kg of water, propellants and gases.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2014/07/Replay_ATV-5_liftoff){:target="_blank" rel="noopener"}


