---
layout: post
title: "A quantum sense of smell – Physics World"
date: 2015-07-26
categories:
author: ""   
tags: [Sense of smell,Odor,Physical sciences,Science,Physics,Applied and interdisciplinary physics]
---


But what are you actually smelling? McFadden is a molecular geneticist who specializes in the study of tuberculosis. He thinks in pictures and concepts, and his laboratory at the University of Surrey in the UK is full of machines oscillating flasks and people monitoring colonies of bacteria. In this podcast, you will hear McFadden and Al-Khalili discuss a possible quantum solution to a long-standing biological puzzle: how does the nose “know” the difference between scent molecules? So when we slice into an orange and take a sniff, our noses may actually be sensing the vibrations of chemical bonds in a molecule called limonene, which is responsible for most of the orange’s citrusy scent.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/KfoH0WS60Qg/a-quantum-sense-of-smell){:target="_blank" rel="noopener"}


