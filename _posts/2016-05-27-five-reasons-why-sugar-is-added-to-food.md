---
layout: post
title: "Five reasons why sugar is added to food"
date: 2016-05-27
categories:
author: Institute of Food Technologists
tags: [Food,Taste,Food science,Fermentation,Sugar,Maillard reaction,Fruit preserves,Candy,Bread,Foods,Nutrition,Food and drink,Food industry,Food and drink preparation,Food ingredients]
---


In the September issue of Comprehensive Reviews in Food Science and Food Safety published by the Institute of Food Technologists (IFT), authors from the University of Minnesota write about the functional properties of sugar and why they are often added to foods. In addition, sugar plays an important role in contributing to the flavor profile of foods by interacting with other ingredients to enhance or lessen certain flavors. Sugar affects multiple chemical reactions that form the texture of baked goods, ice cream, candies, and jams, preserves and jellies. Preservation: The hygroscopic nature of sugar plays a crucial role in reducing water activity in foods. ###  This article in Comprehensive Reviews in Food Science and Food Safety also discusses the challenges of labeling added sugar and the technical issues associated with replacing added sugar in foods.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/ioft-5rw081815.php){:target="_blank" rel="noopener"}


