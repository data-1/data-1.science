---
layout: post
title: "A new way to discover DNA modifications"
date: 2016-03-16
categories:
author: Massachusetts Institute of Technology
tags: [DNA,DNA sequencing,Bacteria,Gene,Virus,Epigenetics,RNA,Genomics,Restriction enzyme,Organism,Cell (biology),Genome,Human microbiome,Biology,Biotechnology,Genetics,Life sciences,Biochemistry,Molecular biology,Chemistry,Nucleic acids]
---


Using their approach, which combines bioanalytical chemistry, comparative genomics, and a special type of DNA sequencing, the team has discovered a DNA modification that helps bacteria to protect their genomes from viral infection. Dedon and Valérie de Crécy-Lagard, a professor of microbiology and cell science at the University of Florida, decided to take a more systematic approach to discovering such modifications. Using comparative genomics, a technique for screening the genomes of different organisms for variants of specific DNA sequences, she found similar genes in multiple bacterial species, located in a specific gene cluster that contained genes for DNA modification. SMRT sequencing often picks up a signal when it encounters something other than the traditional four nucleosides. Antibiotic targets  In humans, there are only a few known DNA modifications, most of which weren't discovered until decades after the four traditional DNA bases were identified.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/miot-anw022916.php){:target="_blank" rel="noopener"}


