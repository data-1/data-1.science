---
layout: post
title: "Are cities affecting evolution?"
date: 2017-11-19
categories:
author: "University of Toronto"
tags: [Evolution,Fordham University,Adaptation,Species,Natural environment,Urbanization,Human,University of Toronto Mississauga,Ecology,Nature,Biology,Biological evolution,Evolutionary biology]
---


As we build cities, we have little understanding of how they are influencing organisms that live there, says Johnson, who is also director of the University of Toronto's Centre for Urban Environments (@CUE_UofT). It's good news that some organisms are able to adapt, such as native species that have important ecological functions in the environment. The study raises questions about which native species can persist during urbanization and whether those that adapt will influence the health of ecosystems and human beings. He and Munshi-South suggest that we need to think carefully about how we're altering our environment in unintended ways when we build cities, influencing the evolution of species that may, in turn, influence our lives. ###  Contact:  Marc Johnson, UTM  Associate Professor of Biology  Director, U of T Centre of Urban Environment  905-569-4484  Marc.johnson@utoronto.ca  http://www.evoeco.org/  Jason Munshi-South, Fordham University  Associate Professor of Biology  646-581-1469  jmunshisouth@fordham.edu  http://www.nycevolution.org/  Gina Vergel  Director of Communications, Fordham University  646-579-9957  gvergel@fordham.edu  Nicolle Wahl  Associate Director, Communications, UTM  905-469-4656  nicolle.wahl@utoronto.ca  About the University of Toronto Mississauga  Founded in 1967 and celebrating its 50th anniversary this year, the University of Toronto Mississauga (UTM) is one of three campuses of the University of Toronto - Canada's largest and most prestigious research-intensive university.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/uot-aca103017.php){:target="_blank" rel="noopener"}


