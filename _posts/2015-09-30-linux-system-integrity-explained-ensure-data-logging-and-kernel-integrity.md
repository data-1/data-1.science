---
layout: post
title: "Linux System Integrity Explained: Ensure Data, Logging and Kernel Integrity"
date: 2015-09-30
categories:
author: Michael Boelen
tags: [File system,Computer data storage,Computer file,Linux kernel,Lynis,Information security,Database,Information sensitivity,Booting,Image scanner,RAID,Kernel (operating system),Software,Operating system technology,Software engineering,Computing,Computers,Information technology management,Information technology,Computer science,Computer data,Information Age,Computer engineering,Technology,Computer architecture,Data management,Data]
---


Information security helps protecting this valuable data, by ensuring its availability, integrity, and confidentiality. What systems have the most valuable data stored? File system integrity  The last category worth mentioning is tooling which monitors file changes. Usually these are stored as binaries on disk. Options:  Use IMA/EVM for high sensitive systems  Implement file integrity monitoring  Log integrity  Logging is the process of storing events in a certain way, for later access.

<hr>

[Visit Link](http://linux-audit.com/linux-system-integrity-explained-ensure-data-logging-and-kernel-integrity/){:target="_blank" rel="noopener"}


