---
layout: post
title: "Getting started with regular expressions"
date: 2018-05-21
categories:
author: "Jet Anderson"
tags: [Regular expression,Sed,Computer science,Computing,Computer programming,Software engineering,Technology,Software development,Software]
---


Why we need regular expressions  As mentioned above, regular expressions are used to define a pattern to help us match on or find objects that match that pattern. Now, to start searching in vi, type the / character followed by your search pattern. Search for things at the beginning or end of a line  To find a line that starts with Simple, use this regex pattern:  /^Simple  Notice in the image below that only the line starting with Simple is highlighted. – zero or just one b {n} Given a number, find exactly that number ab{2} – the letter a followed by exactly two b's {n,} Given a number, find at least that number ab{2,} – the letter a followed by at least two b's {n,y} Given two numbers, find a range of that number ab{1,3} – the letter a followed by between one and three b's  Find classes of characters  The next step in regex training is to use classes of characters in our pattern matching. Some common classes of characters that are used as ranges are:  a-z – all lowercase characters  A-Z – all UPPERCASE characters  0-9 – numbers  Now, let's try a search similar to one we ran earlier:  /tT  Do you notice that it finds nothing?

<hr>

[Visit Link](https://opensource.com/article/18/5/getting-started-regular-expressions){:target="_blank" rel="noopener"}


