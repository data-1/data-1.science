---
layout: post
title: "Giant telescope takes close look at Jupiter's moon Io"
date: 2016-04-29
categories:
author: Daniel Stolte, University Of Arizona
tags: [Volcanism on Io,Io (moon),Loki Patera,Large Binocular Telescope,Physical sciences,Astronomy,Space science,Planetary science,Outer space,Bodies of the Solar System,Planets of the Solar System,Solar System,Astronomical objects]
---


Credit: NASA/JPL-CALTECH  With the first detailed observations through imaging interferometry of a lava lake on a moon of Jupiter, the Large Binocular Telescope Observatory places itself as the forerunner of the next generation of Extremely Large Telescopes. Io, the innermost of the four moons of Jupiter discovered by Galileo Galilei in 1610 and only slightly bigger than our own moon, is the most geologically active body in our solar system. A serendipitous movie of an occultation of Io by its neighboring moon Europa when the team re-observed Io on the night of March 7, 2015, for a follow-up to the work done for the published study. While we have seen bright emissions—always one unresolved spot—'pop up' at different locations in Loki Patera over the years, said Imke de Pater, a professor at the University of California, Berkeley, these exquisite images from the LBTI show for the first time in ground-based images that emissions arise simultaneously from different sites in Loki Patera. Studying the very dynamic volcanic activity on Io, which is constantly reshaping the moon 's surface, provides clues to the interior structure and plumbing of this moon, remarked team member Chick Woodward of the University of Minnesota, helping to pave the way for future NASA missions such as the Io Volcano Observer.

<hr>

[Visit Link](http://phys.org/news349684512.html){:target="_blank" rel="noopener"}


