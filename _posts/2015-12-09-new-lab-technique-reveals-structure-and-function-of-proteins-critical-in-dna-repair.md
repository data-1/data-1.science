---
layout: post
title: "New lab technique reveals structure and function of proteins critical in DNA repair"
date: 2015-12-09
categories:
author: University of Illinois Grainger College of Engineering
tags: [Helicase,Protein,DNA,Single-molecule experiment,Macromolecules,Scientific techniques,Physical sciences,Molecular biology,Biochemistry,Life sciences,Biotechnology,Biology,Chemistry]
---


We found that a single UvrD helicase can do something--it unwinds the DNA, but not very far. There are two distinct structures or states that are associated with UvrD, with the molecule organized in either an open or closed position. The function associated with each state has been debated among experts for several years. In Ha's laboratory, the team engineered a structurally homologous helicase protein called Rep, fastening it in either the closed or the open state by using a cross-linking molecule as tape. The team repeated the experiment on another structurally homologous helicase protein called PcrA, with the same result.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/uoic-nlt041715.php){:target="_blank" rel="noopener"}


