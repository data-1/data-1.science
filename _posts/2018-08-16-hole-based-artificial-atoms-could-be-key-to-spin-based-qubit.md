---
layout: post
title: "Hole-based artificial atoms could be key to spin-based qubit"
date: 2018-08-16
categories:
author: ""
tags: [Quantum dot,Electron,Electron hole,Spin (physics),MOSFET,Semiconductor,Atom,Quantum computing,Computing,Electromagnetism,Applied and interdisciplinary physics,Semiconductors,Electricity,Physical sciences,Theoretical physics,Quantum mechanics,Electronics,Electrical engineering,Physics,Technology]
---


Artificial atoms in quantum computing  The spin states of electrons confined to semiconductor quantum dots are a promising platform for quantum computation. Such a device is known as an artificial atom. However, this is not always true: in semiconductors, electricity can also be carried by a different type of particle, called holes. Credit: FLEET  However, despite over 50 years of research almost all technological developments have focused on artificial atoms that use electrons. By understanding and harnessing the unique properties of holes, we will have more tools to develop new semiconductor electronic devices, says Scott.

<hr>

[Visit Link](https://phys.org/news/2018-08-hole-based-artificial-atoms-key-spin-based.html){:target="_blank" rel="noopener"}


