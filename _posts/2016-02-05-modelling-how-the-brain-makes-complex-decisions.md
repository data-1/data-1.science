---
layout: post
title: "Modelling how the brain makes complex decisions"
date: 2016-02-05
categories:
author: University of Cambridge
tags: [Decision-making,Brain,Neuroscience,Cognitive science,Cognition,Interdisciplinary subfields,Cognitive psychology,Branches of science,Psychology,Psychological concepts]
---


The mathematical model, developed by researchers from the University of Cambridge, is the first biologically realistic account of the process, and is able to predict not only behaviour, but also neural activity. The results, reported in The Journal of Neuroscience, could aid in the understanding of conditions from obsessive compulsive disorder and addiction to Parkinson's disease. There are two main types of decisions: habit-based and goal-based. An example of a habit-based decision would be a daily commute, which is generally the same every day. The researchers also found that for making a goal-based decision, the synapses which connect the neurons together need to 'embed' the knowledge of how situations follow on from each other, depending on the actions that are chosen, and how they result in immediate reward.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/uoc-mht020416.php){:target="_blank" rel="noopener"}


