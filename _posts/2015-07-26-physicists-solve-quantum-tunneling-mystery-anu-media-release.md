---
layout: post
title: "Physicists solve quantum tunneling mystery: ANU media release"
date: 2015-07-26
categories:
author: Australian National University 
tags: [Quantum mechanics,Quantum tunnelling,Physics,Electron,Time,Atom,Scientific theories,Scientific method,Applied and interdisciplinary physics,Theoretical physics,Science,Physical sciences]
---


The new theory could lead to faster and smaller electronic components, for which quantum tunneling is a significant factor. Professor Kheifets and Dr. Igor Ivanov, from the ANU Research School of Physics and Engineering, are members of a team which studied ultrafast experiments at the attosecond scale (10-18 seconds), a field that has developed in the last 15 years. Until their work, a number of attosecond phenomena could not be adequately explained, such as the time delay when a photon ionised an atom. At that timescale the time an electron takes to quantum tunnel out of an atom was thought to be significant. The results give an accurate calibration for future attosecond-scale research, said Professor Kheifets.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/anu-psq052615.php){:target="_blank" rel="noopener"}


