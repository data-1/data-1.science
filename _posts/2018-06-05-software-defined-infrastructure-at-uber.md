---
layout: post
title: "Software-Defined Infrastructure at Uber"
date: 2018-06-05
categories:
author: "Esther Shein"
tags: [Computer network,Provisioning (telecommunications),Automation,Deployment environment,Software,Linux,Cloud computing,Computer engineering,Computer science,Computers,Information and communications technology,Technology,Information technology management,Computing,Information technology,Information Age]
---


The only way for Uber to deliver the required level of network performance and availability is through software and automation, said Justin Dustzadeh, the head of global network and software platform at Uber. The ride sharing company relies heavily on software to automate its infrastructure and thoroughly tests not only its software but also the test environment itself, Dustzadeh said, speaking at the recent Open Networking Summit. On the network side, for example, IT pushes intelligence to the devices to enable a distributed self-discovery model and enable zero-touch provisioning, he noted. This gives officials near real-time visibility into the state of the network, including network reachability, network latency, packet losses, and link utilization, he said. Resiliency  Uber views its network as a key enabler of its business, Dustzadeh said.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/software-defined-infrastructure-at-uber/){:target="_blank" rel="noopener"}


