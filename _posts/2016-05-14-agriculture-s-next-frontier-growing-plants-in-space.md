---
layout: post
title: "Agriculture's next frontier? Growing plants in space"
date: 2016-05-14
categories:
author: University of Florida Institute of Food and Agricultural Sciences
tags: [Plant,News aggregator,Research,NASA,Experiment,Science]
---


Paul explained how all this research helps us on planet Earth. During the experiments, NASA scientists sent plants to the International Space Station to test Paul and Ferl's ideas about how plants sense changes in their environment, and then how they respond to those changes. They found that not only do plants grow in space by adjusting their basic metabolism, they saw a big difference in how various plant parts respond to space flight. advertisement  This also gave us a clue about how roots may use light as a tool to guide growth when gravity is not available, and that is as an indication of which direction is 'away' from the leaves, Paul said. Was that different in space as well?

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150716092015.htm){:target="_blank" rel="noopener"}


