---
layout: post
title: "FAST's first discovery of a millisecond pulsar"
date: 2018-05-07
categories:
author: "Chinese Academy of Sciences Headquarters"
tags: [Pulsar,Fermi Gamma-ray Space Telescope,Gravitational wave,Stellar astronomy,Physical phenomena,Scientific observation,Astrophysics,Physics,Science,Astronomical objects,Physical sciences,Space science,Astronomy]
---


China's Five-hundred-meter Aperture Spherical radio Telescope(FAST), still under commissioning, discovered a radio millisecond pulsar (MSP) coincident with the unassociated gamma-ray source 3FGL J0318.1+0252 in the Fermi Large Area Telescope (LAT) point-source list. FAST, world's largest single-dish radio telescope, operated by the National Astronomical Observatory of the Chinese Academy of Sciences, has discovered more than 20 new pulsars so far. This first MSP discovery was made by FAST on Feb. 27 and later confirmed by the Fermi-LAT team in reprocessing of Fermi data on April 18th. It is not only expected to play an important role in understanding the evolution of neutron stars and the equation of state of condense matter, but also can be used to detect low-frequency gravitational waves. The pulsar timing array (PTA) attempts to detect low-frequency gravitational waves from merging supermassive black holes using the long-term timing of a set of stable millisecond pulsars.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-04/caos-ffd042718.php){:target="_blank" rel="noopener"}


