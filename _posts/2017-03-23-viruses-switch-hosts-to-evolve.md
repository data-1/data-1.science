---
layout: post
title: "Viruses switch hosts to evolve"
date: 2017-03-23
categories:
author: ""
tags: []
---


Edward Holmes and his colleagues at the University of Sydney in Australia compared the evolutionary histories of 19 virus families with those of their animal or plant hosts. They found that, in almost all cases, the trees of life for the viruses had very different branching patterns compared with the trees for the viruses' current hosts. This suggests that viruses jump between host species more often than expected. The authors report that RNA viruses — particularly the Rhabdoviridae (which includes the rabies virus) and Picornaviridae — switch host species more frequently than other viruses, whereas double-stranded DNA viruses do so the least.

<hr>

[Visit Link](http://www.nature.com/nature/journal/v543/n7646/full/543466b.html?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


