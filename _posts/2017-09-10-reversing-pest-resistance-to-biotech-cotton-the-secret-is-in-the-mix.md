---
layout: post
title: "Reversing pest resistance to biotech cotton: The secret is in the mix"
date: 2017-09-10
categories:
author: "University Of Arizona"
tags: [Pesticide resistance,Cotton,Bacillus thuringiensis,Bt cotton,Plant breeding,Hybrid (biology),Agriculture,Biotechnology,Maize]
---


Credit: Peng Wan  Insect pests that are rapidly adapting to genetically engineered crops threaten agriculture worldwide. Planting such non-Bt cotton refuges is credited with preventing evolution of resistance to Bt cotton by pink bollworm in Arizona for more than a decade. The ingenious strategy used in China entails interbreeding Bt cotton with non-Bt cotton, then crossing the resulting first-generation hybrid offspring and planting the second-generation hybrid seeds. This generates a random mixture within fields of 75 percent Bt cotton plants side-by-side with 25 percent non-Bt cotton plants. We know it works for millions of farmers in the Yangtze River Valley.

<hr>

[Visit Link](https://phys.org/news/2017-05-reversing-pest-resistance-biotech-cotton.html){:target="_blank" rel="noopener"}


