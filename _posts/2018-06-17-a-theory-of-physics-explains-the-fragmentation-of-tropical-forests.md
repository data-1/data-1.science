---
layout: post
title: "A theory of physics explains the fragmentation of tropical forests"
date: 2018-06-17
categories:
author: "Helmholtz Association Of German Research Centres"
tags: [Habitat fragmentation,Deforestation,Forest,Biodiversity,Theory,Percolation theory,Reforestation,Power law,Fractal,Earth sciences,Natural environment,Nature]
---


They found that forest fragmentation on all three continents is close to a critical point beyond which the fragment number will strongly increase. The fragment size distribution follows a power law with almost identical exponents on all three continents, says biophysicist Andreas Huth. This explains why all three continents show similar large-scale fragmentation patterns. The UFZ team compared the remote sensing data of the three topical regions with several predictions of percolation theory. A UFZ team led by Andreas Huth previously described in Nature Communications that fragmentation of once connected tropical forest areas could increase carbon emissions worldwide by another third, as many trees die and less carbon dioxide is stored in the edge of forest fragments.

<hr>

[Visit Link](https://phys.org/news/2018-02-theory-physics-fragmentation-tropical-forests.html){:target="_blank" rel="noopener"}


