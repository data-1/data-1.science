---
layout: post
title: "New MAVEN findings reveal how Mars' atmosphere was lost to space"
date: 2017-09-23
categories:
author: "University of Colorado at Boulder"
tags: [MAVEN,Atmosphere,Mars,Sputtering,Atmosphere of Mars,Atmosphere of Earth,Physical sciences,Space science,Nature,Chemistry,Planets,Planetary science,Astronomy,Outer space]
---


We've determined that most of the gas ever present in the Mars atmosphere has been lost to space, said Bruce Jakosky, principal investigator for MAVEN and a professor at the Laboratory for Atmospheric and Space Physics (LASP). Liquid water, essential for life, is not stable on Mars' surface today because the atmosphere is too cold and thin to support it. Jakosky and his team got the result by measuring the atmospheric abundance of two different isotopes of argon gas. We determined that the majority of the planet's CO2 also has been lost to space by sputtering, said Jakosky. The combined measurements enable a better determination of how much Martian argon has been lost to space over billions of years, said Paul Mahaffy of NASA's Goddard Space Flight Center in Greenbelt, Maryland.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/uoca-nmf032917.php){:target="_blank" rel="noopener"}


