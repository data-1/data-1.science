---
layout: post
title: "Soon, The Only Tech Jobs Will Be Design Or Data"
date: 2017-10-19
categories:
author:  
tags: [General Electric,Technology,Computer programming,Web application,Computer network,Amazon Web Services,Cloud computing,Software development,World Wide Web,Innovation,Automation,Analytics,Electrical grid,Sustainable energy,Culture,Application software,API,Energy development,3D printing,Engineering,Governance,Engine,Computing,Computer science]
---


We were talking a few years ago about how far tooling and automation had come along in software engineering. He suggested that, in the very near future, there would be only two jobs in application development: user experience design (UX) and data science. We’ve reached the point at which most web application programming is configuration. They were gatekeepers to the application developers, controlling when and how our code would be deployed to production. Now, we’re moving a level up again.

<hr>

[Visit Link](http://www.gereports.com/soon-tech-jobs-will-design-data/){:target="_blank" rel="noopener"}


