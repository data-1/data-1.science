---
layout: post
title: "Migrating birds use a magnetic map to travel long distances"
date: 2017-10-08
categories:
author: "Richard Holland, The Conversation"
tags: [Bird migration,Compass,Navigation]
---


Much of the research has focused on homing pigeons, which are famous for their ability to return to their lofts after long distance displacements. Evidence suggests that pigeons use a combination of olfactory cues to locate their position, and then the sun as a compass to head in the right direction. Over these vast distances, the cues that pigeons use may not work for migrating birds, and so scientists think they may require a more global mapping mechanism. To find out whether declination is used by migrating birds, we tested the orientation of migratory reed warblers. This was not consistent with a magnetic compass response, but was consistent with the birds thinking they had been displaced to Scotland, and correcting to return to their normal path.

<hr>

[Visit Link](https://phys.org/news/2017-08-migrating-birds-magnetic-distances.html){:target="_blank" rel="noopener"}


