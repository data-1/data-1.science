---
layout: post
title: "Successful heart transplant after using experimental artificial heart"
date: 2016-05-10
categories:
author: University of California, Los Angeles (UCLA), Health Sciences
tags: [Artificial heart,Organ transplantation,Heart,Heart transplantation,SynCardia Systems,Organ donation,Surgery,Organs (anatomy),Health care,Health,Medical specialties,Medicine,Clinical medicine,Health sciences]
---


A petite 44-year-old woman has received a successful heart transplant at Ronald Reagan UCLA Medical Center, thanks to an experimental Total Artificial Heart designed for smaller patients. The UCLA patient is the first person in California to receive the smaller Total Artificial Heart, and the first patient in the world with the device to be bridged to a successful heart transplant -- that is, to go from needing a transplant to receiving one. We were grateful to have this experimental technology available to save her life and help bridge her to a donor heart. advertisement  In addition to the high-tech medicine that kept her alive, Mrs. Kahala and her family have exemplified how a solid support system that includes loved ones and a compassionate medical team practicing what we at UCLA have termed 'Relational Medicine' plays an important role in surviving a medical crisis, said Dr. Mario Deng, professor of medicine and medical director of the Advanced Heart Failure, Mechanical Support and Heart Transplant program at UCLA. Since 2012, the UCLA Heart Transplant Program has implanted eight 70cc SynCardia Total Artificial Hearts.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150701140901.htm){:target="_blank" rel="noopener"}


