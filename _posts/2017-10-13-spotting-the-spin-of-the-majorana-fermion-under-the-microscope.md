---
layout: post
title: "Spotting the spin of the Majorana fermion under the microscope"
date: 2017-10-13
categories:
author: "Princeton  University"
tags: [Majorana fermion,Materials science,Physics,Quantum mechanics,Theoretical physics,Particle physics,Applied and interdisciplinary physics,Physical sciences,Science,Chemistry,Condensed matter physics,Condensed matter]
---


Their method involved detecting a distinctive quantum property known as spin, which has been proposed for transmitting quantum information in circuits that contain the Majorana particle. The experimental detection of this property provides a unique signature of this exotic particle. In these wires, Majoranas occur as pairs at either end of the chains, provided the chains are long enough for the Majoranas to stay far enough apart that they do not annihilate each other. Professor of Physics Andrei Bernevig and his team, who with Yazdani's group proposed the atomic chain platform, developed the theory that showed that spin-polarized measurements made using a scanning tunneling microscope can distinguish between the presence of a pair of ordinary quasi-particles and a Majorana. The quantum spin property of Majorana may also make them more useful for applications in quantum information.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171012143334.htm){:target="_blank" rel="noopener"}


