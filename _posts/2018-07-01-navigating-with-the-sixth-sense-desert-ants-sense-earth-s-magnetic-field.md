---
layout: post
title: "Navigating with the sixth sense: Desert ants sense Earth's magnetic field"
date: 2018-07-01
categories:
author: "University of Würzburg"
tags: [Ant,Cataglyphis,News aggregator,Cognitive science,Cognition,Branches of science,Science]
---


Researchers from the Biocenter of the University of Würzburg have now made the surprising discovery that the desert ant uses the Earth's magnetic field as orientation cue during these calibration trips. The researchers had known already that the ants rely on the position of the sun and landmarks as orientational cues and integrate this information with the steps travelled. advertisement  Experiments in Greece  Recent research results have shown, however, that the desert ant also looks back to the nest entrance during its learning walks in the absence of solar information or landscape cues. A surprising outcome  The result was unambiguous: When the scientists changed the orientation of the magnetic field, the desert ants no longer looked towards the real nest entrance but towards a predictable new location -- the fictive nest entrance. According to Wolfgang Rössler, this question takes you deep into the field of orientational and navigational research in insects.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180426130001.htm){:target="_blank" rel="noopener"}


