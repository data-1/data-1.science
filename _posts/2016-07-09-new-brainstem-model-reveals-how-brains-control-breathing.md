---
layout: post
title: "New brainstem model reveals how brains control breathing"
date: 2016-07-09
categories:
author: "eLife"
tags: [Breathing,Prostaglandin E2,Respiratory system,ELife,Research,Brain,Inflammation,Carbon dioxide,Physiology,Neuroscience,Medical specialties,Medicine,Clinical medicine]
---


Scientists from Karolinska Institutet, Sweden, have discovered how the brain controls our breathing in response to changing oxygen and carbon dioxide levels in the blood. Without an adequate response to increased carbon dioxide levels, people can suffer from breathing disturbances, sickness, and panic. Now, a new study in mice, to be published in the journal eLife, shows that when exposed to decreased oxygen or increased carbon dioxide levels, the brain releases a small molecule called Prostaglandin E2 (PGE2) to help protect itself and regulate breathing. Our novel brainstem culture first revealed that cells responsible for breathing operate in a small-world network. Our findings go some way to explaining how and why our breathing responses to imbalanced oxygen and carbon dioxide levels are impaired during infectious episodes.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/e-nbm070416.php){:target="_blank" rel="noopener"}


