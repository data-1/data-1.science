---
layout: post
title: "Physicists demonstrate a quantum Fredkin gate"
date: 2016-03-26
categories:
author: Griffith University
tags: [Quantum computing,Information Age,Technology,Computer science,Computing,Branches of science,Theoretical computer science,Applied mathematics,Quantum mechanics,Quantum information science]
---


Researchers from Griffith University and the University of Queensland have overcome one of the key challenges to quantum computing by simplifying a complex quantum logic operation. We demonstrate in our experiment how one can build larger quantum circuits in a more direct way without using small logic gates. This is a gate where two qubits are swapped depending on the value of the third. Usually the Fredkin gate requires implementing a circuit of five logic operations. The research team used the quantum entanglement of photons—particles of light—to implement the controlled-SWAP operation directly.

<hr>

[Visit Link](http://phys.org/news/2016-03-physicists-quantum-fredkin-gate.html){:target="_blank" rel="noopener"}


