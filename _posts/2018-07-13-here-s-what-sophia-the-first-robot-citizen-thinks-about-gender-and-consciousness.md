---
layout: post
title: "Here's What Sophia, the First Robot Citizen, Thinks About Gender and Consciousness"
date: 2018-07-13
categories:
author: "Chelsea Gohd"
tags: [Sophia (robot),Robot,Science,Consciousness,Interdisciplinary subfields,Cognition,Branches of science,Cognitive science]
---


In a video that's as unsettling as it is awe-inspiring, Sophia — the world's first robot citizen — breaks down everything from gender to ethical robot design. [Machine Dreams: 22 Human-Like Androids from Sci-Fi]  At Brain Bar, Sophia had a lot to say. However, Sophia did add that I'm learning first to be a good social robot and that she would of course sacrifice herself to save a human's life. While completely robotic, Sophia also addressed questions about gender and robots. When asked if she believed that robots could have gender, she answered, I think so.

<hr>

[Visit Link](https://www.livescience.com/63023-sophia-robot-citizen-talks-gender.html){:target="_blank" rel="noopener"}


