---
layout: post
title: "New network is installed to investigate space weather over South America"
date: 2018-06-07
categories:
author: "Peter Moon"
tags: [Solar flare,Solar phenomena,Earths magnetic field,Aurora,Applied and interdisciplinary physics,Physical phenomena,Electrical engineering,Electromagnetism,Space science,Nature]
---


Before Embrace MagNet, South American researchers depended on data from institutions in the U.S., Europe and Japan to study magnetic field disturbances over South America, according to INPE's head of space and atmospheric sciences, Clezio Marcos De Nardin. Interactions among energized solar particles and Earth's magnetic field cause disturbances around the globe, producing auroras in the stratosphere over the North and South Poles. It doesn't take a huge solar storm to damage the power grid, however. Any solar storm causes ground currents that affect transformers. The Embrace Magnetometer Network for South America: First Scientific Results, Radio Science (2018).

<hr>

[Visit Link](https://phys.org/news/2018-05-network-space-weather-south-america.html){:target="_blank" rel="noopener"}


