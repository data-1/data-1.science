---
layout: post
title: "Scientists prove feasibility of 'printing' replacement tissue"
date: 2016-03-16
categories:
author: Atrium Health Wake Forest Baptist
tags: [Regenerative medicine,Atrium Health Wake Forest Baptist,Wake Forest School of Medicine,Tissue engineering,Tissue (biology),Medicine,Medical specialties,Health sciences,Clinical medicine,Life sciences]
---


The Integrated Tissue and Organ Printing System (ITOP), developed over a 10-year period by scientists at the Institute for Regenerative Medicine, overcomes these challenges. Two months later, the shape of the implanted ear was well-maintained and cartilage tissue and blood vessels had formed. To demonstrate the ITOP can generate organized soft tissue structures, printed muscle tissue was implanted in rats. After five months, the bioprinted structures had formed vascularized bone tissue. The institution comprises the medical education and research components of Wake Forest School of Medicine, the integrated clinical structure and consumer brand Wake Forest Baptist Health, which includes North Carolina Baptist Hospital and Brenner Children's Hospital, the creation and commercialization of research discoveries into products that benefit patients and improve health and wellness, through Wake Forest Innovations, Wake Forest Innovation Quarter, a leading center of technological discovery, development and commercialization, as well as a network of affiliated community-based hospitals, physician practices, outpatient services and other medical facilities.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/wfbm-spf021116.php){:target="_blank" rel="noopener"}


