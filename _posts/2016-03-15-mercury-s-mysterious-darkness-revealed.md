---
layout: post
title: "Mercury's mysterious 'darkness' revealed"
date: 2016-03-15
categories:
author: Carnegie Institution for Science
tags: [Mercury (planet),MESSENGER,Carnegie Institution for Science,Impact crater,Moon,Astronomical objects,Planets,Astronomical objects known since antiquity,Planets of the Solar System,Astronomy,Science,Physical sciences,Bodies of the Solar System,Solar System,Outer space,Planetary science,Space science]
---


About a year ago, scientists proposed that Mercury's darkness was due to carbon that gradually accumulated from the impact of comets that traveled into the inner Solar System. Moreover, we used both neutrons and X-rays to confirm that the dark material is not enriched in iron, in contrast to the Moon where iron-rich minerals darken the surface. MESSENGER obtained its statistically robust data via many orbits on which the spacecraft passed lower than 60 miles (100 km) above the surface of the planet during its last year of operation. The data used to identify carbon included measurements taken just days before MESSENGER impacted Mercury in April 2015. The MESSENGER spacecraft launched on August 3, 2004, and entered orbit about Mercury on March 17, 2011 (March 18, 2011 UTC), to begin a yearlong study of its target planet.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/ci-mm030316.php){:target="_blank" rel="noopener"}


