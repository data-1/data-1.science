---
layout: post
title: "15 Tips On How to Use ‘Curl’ Command in Linux"
date: 2018-08-18
categories:
author: "Gabriel Cánepa"
tags: [CURL,File Transfer Protocol,HTTP cookie,Hypertext Transfer Protocol,Wide area networks,Hypertext,Information technology,Computer data,Information Age,Data transmission,Networking standards,Information technology management,Software engineering,Computer science,Network service,Utility software,Software development,Computer engineering,Telecommunications,Internet protocols,Network protocols,Communications protocols,Internet architecture,Network architecture,Internet Standards,Cyberspace,Protocols,Internet,IT infrastructure,Application layer protocols,Computer networking,Computing,Software,Technology,World Wide Web,Computer architecture,Distributed computing architecture,Computers]
---


View curl Version  The -V or --version options will not only return the version, but also the supported protocols and features in your current version. Download a File  If you want to download a file, you can use curl with the -O or -o options. $ curl -C - -O http://yourdomain.com/yourfile.tar.gz  4. Download Multiple Files  With the following command you will download info.html and about.html from http://yoursite.com and http://mysite.com, respectively, in one go. Download Files from an FTP Server with or without Authentication  If a remote FTP server is expecting connections at ftp://yourftpserver, the following command will download yourfile.tar.gz in the current working directory.

<hr>

[Visit Link](https://www.tecmint.com/linux-curl-command-examples/){:target="_blank" rel="noopener"}


