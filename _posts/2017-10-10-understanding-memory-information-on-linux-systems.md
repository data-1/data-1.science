---
layout: post
title: "Understanding memory information on Linux systems"
date: 2017-10-10
categories:
author: "Michael Boelen"
tags: [Linux kernel,Computer data storage,Memory paging,Kernel (operating system),Procfs,Cache (computing),Computer file,Computer program,Software engineering,Computer architecture,Computer science,Computers,Computer engineering,Software,Operating system technology,Computer data,System software,Technology,Information technology management,Computing,Information Age,Information technology,Data,Data management,Software development,Computer hardware]
---


Let’s discover how Linux manages its memory and how we can gather memory information. After reading this guide, you will be able to:  Show the total amount of memory  Display all memory details  Understand the details listed in /proc/meminfo  Use tools like dmesg, dmidecode, free, and vmstat  Linux memory information  Random access memory  When we talk about memory in this article, we usually mean random access memory (RAM). This is the memory which can be used for both showing and storing data. Typically we will find in this type of memory the programs that are running on the system, including the Linux kernel itself. Where does the kernel store the details regarding virtual memory management?

<hr>

[Visit Link](https://linux-audit.com/understanding-memory-information-on-linux-systems/){:target="_blank" rel="noopener"}


