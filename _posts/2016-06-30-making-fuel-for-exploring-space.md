---
layout: post
title: "Making fuel for exploring space"
date: 2016-06-30
categories:
author: "Leo Williams, Oak Ridge National Laboratory"
tags: [Plutonium-238,Plutonium,Isotopes of neptunium,Voyager program,Oak Ridge National Laboratory,Nuclear reactor,Neutron,Neptunium,Nuclear technology,Atoms,Energy technology,Nuclear energy,Radioactivity,Physical sciences,Nuclear chemistry,Chemistry,Nuclear physics]
---


Our objective is to stand up the capability to make 1.5 kilograms of plutonium oxide annually, explained ORNL's Bob Wham, who acts as the plutonium project's integration manager. New material produced by the project will be sent to Los Alamos National Laboratory, where existing plutonium-238 is already being stored and processed for use on spacecraft. A process in the making  At this point the process is still being worked out, Wham noted. Once it is in full production it will produce about 450 targets each year. We've been working over the last two or three years to get targets qualified to go in HFIR for irradiation.

<hr>

[Visit Link](http://phys.org/news/2015-09-fuel-exploring-space.html){:target="_blank" rel="noopener"}


