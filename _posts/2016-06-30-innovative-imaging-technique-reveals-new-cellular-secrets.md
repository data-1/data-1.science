---
layout: post
title: "Innovative imaging technique reveals new cellular secrets"
date: 2016-06-30
categories:
author: "Stowers Institute For Medical Research"
tags: [Spindle pole body,Cell biology,Optics,Chemistry]
---


Optical approaches cannot resolve objects below certain wavelength limits, while non-optical approaches like electron microscopy (EM) can only study nonliving cells. Now, a team of researchers from the Stowers Institute for Medical Research and the University of Colorado Boulder has devised a novel optical technique—a combination of structured illumination microscopy (SIM) and single-particle averaging (SPA)—to resolve individual components of SPB duplication in living yeast cells. In a study to be published on the eLife website on September 15, 2015, a team of researchers from the Stowers Institute for Medical Research and the University of Colorado Boulder combined two optical systems in a new way to get around the natural limits of optical microscopes. Using this method, the team found that SPBs duplicate and form some structures at different times than once thought. They also spotted a number of never-before-seen structures used in SPB duplication.

<hr>

[Visit Link](http://phys.org/news/2015-09-imaging-technique-reveals-cellular-secrets.html){:target="_blank" rel="noopener"}


