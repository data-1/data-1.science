---
layout: post
title: "Proton–antiproton equivalence confirmed by best-ever measurement of magnetic moment – Physics World"
date: 2017-11-01
categories:
author: "Marric Stephens"
tags: [Antiproton,Physics,Proton,Particle physics,Physical sciences,Science]
---


The result means that the magnetic moment of the antiproton is now known even more precisely than the magnetic moment of the proton itself. Crucial to the 350-fold improvement in precision was the simultaneous measurement of the cyclotron frequency of one antiproton and of the Larmor frequency of another antiproton. Anybody hoping for hints of new physics beyond the Standard Model will be disappointed, however, because the result is consistent with protons and antiprotons having magnetic moments that are opposite but equal. The researchers expect to achieve a further improvement in precision by upgrading the experiment’s magnetic shielding and cooling system, and by using a more homogeneous magnetic field in the precision trap. The experiment is described in Nature.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/oct/30/proton-antiproton-equivalence-confirmed-by-best-ever-measurement-of-magnetic-moment){:target="_blank" rel="noopener"}


