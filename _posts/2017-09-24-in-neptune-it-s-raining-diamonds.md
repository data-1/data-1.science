---
layout: post
title: "In Neptune, it's raining diamonds"
date: 2017-09-24
categories:
author: "Helmholtz-Zentrum Dresden-Rossendorf"
tags: [SLAC National Accelerator Laboratory,Planet,Ice giant,Diamond,Uranus,Neptune,Physical sciences,Chemistry,Astronomy,Science,Physics]
---


In cooperation with colleagues from Germany and the United States, researchers at the Helmholtz-Zentrum Dresden-Rossendorf (HZDR) have managed to demonstrate 'diamond showers' forming in the ice giants of our solar system. For a long time, astrophysicists have been speculating that the extreme pressure that reigns more than 10,000 kilometers beneath the surface of these planets splits the hydrocarbons causing diamonds to form, which then sink deeper into the planet's interior. And, for their part, these chemical processes inside the planet tell us something about its vital properties, Dominik Kraus continues. 400 | 01328 Dresden / Germany | http://www.hzdr.de  The Helmholtz-Zentrum Dresden-Rossendorf (HZDR) performs research in the fields of energy, health, and matter. To help answer these research questions, HZDR operates large-scale facilities, which are also used by visiting researchers: the Ion Beam Center, the High-Magnetic Field Laboratory Dresden, and the ELBE Center for High-Power Radiation Sources.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-08/hd-ini082117.php){:target="_blank" rel="noopener"}


