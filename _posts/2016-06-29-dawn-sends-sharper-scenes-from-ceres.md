---
layout: post
title: "Dawn sends sharper scenes from Ceres"
date: 2016-06-29
categories:
author: "NASA/Jet Propulsion Laboratory"
tags: [Dawn (spacecraft),Ceres (dwarf planet),NASA,Space probes,Space science,Outer space,Spaceflight,Astronomy,Space exploration,Solar System,Spacecraft,Planetary science,Astronautics,Science,Flight,Discovery and exploration of the Solar System,Bodies of the Solar System,Space vehicles,Space missions,Space program of the United States]
---


The spacecraft's view is now three times as sharp as in its previous mapping orbit, revealing exciting new details of this intriguing dwarf planet, said Marc Rayman, Dawn's chief engineer and mission director, based at NASA's Jet Propulsion Laboratory, Pasadena, California. At its current orbital altitude of 915 miles (1,470 kilometers), Dawn takes 11 days to capture and return images of Ceres' whole surface. Over the next two months, the spacecraft will map the entirety of Ceres six times. Dawn is the first mission to visit a dwarf planet, and the first to orbit two distinct solar system targets. Dawn's mission is managed by JPL for NASA's Science Mission Directorate in Washington.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150825111111.htm){:target="_blank" rel="noopener"}


