---
layout: post
title: "Palaeomagnetic field intensity variations suggest Mesoproterozoic inner-core nucleation"
date: 2015-10-08
categories:
author: Biggin, A. J., Department Of Earth, Ocean, Ecological Sciences, University Of Liverpool, Liverpool, Piispa, E. J., Department Of Geological
tags: []
---


The Earth’s inner core grows by the freezing of liquid iron at its surface. Recent computational and experimental studies1,2,3,4,5 have presented radically differing estimates of the thermal conductivity of the Earth’s core, resulting in estimates of the timing of inner-core nucleation ranging from less than half a billion to nearly two billion years ago. The nucleation of the core leads to a different convective regime6 and potentially different magnetic field structures that produce an observable signal in the palaeomagnetic record and allow the date of inner-core nucleation to be estimated directly. Previous studies searching for this signature have been hampered by the paucity of palaeomagnetic intensity measurements, by the lack of an effective means of assessing their reliability, and by shorter-timescale geomagnetic variations. This observation is most readily explained by the nucleation of the inner core occurring during this interval9; the timing would tend to favour a modest value of core thermal conductivity and supports a simple thermal evolution model for the Earth.

<hr>

[Visit Link](http://www.nature.com/nature/journal/v526/n7572/full/nature15523.html){:target="_blank" rel="noopener"}


