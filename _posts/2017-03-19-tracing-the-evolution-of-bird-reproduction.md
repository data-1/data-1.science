---
layout: post
title: "Tracing the evolution of bird reproduction"
date: 2017-03-19
categories:
author: "American Ornithological Society Publications Office"
tags: [Bird,Dinosaur,Ornithology,Egg,Zoology,Vertebrate zoology,Birds,Biology,Animals]
---


Those dinosaurs close to the ancestry of birds shared some of these traits, but they had two functional reproductive tracts, and their eggs were smaller relative to their body size and more elongated than those of modern birds. Fossils of primitive birds and eggs from the Mesozoic era place them midway between their dinosaur ancestors and their modern descendants, with eggs between those of pre-avian dinosaurs and modern birds in term of size and shape. In this way, David Varricchio and Frankie Jackson of the Montana State University are able to trace the evolution of bird reproduction through a series of distinct stages, from pre-avian dinosaurs to the birds of today. Reproduction in modern birds is distinct among living vertebrates. About the journal: The Auk: Ornithological Advances is a peer-reviewed, international journal of ornithology that began in 1884 as the official publication of the American Ornithologists' Union.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/copo-tte080516.php){:target="_blank" rel="noopener"}


