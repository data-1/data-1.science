---
layout: post
title: "What you need to know about open source forking"
date: 2017-09-24
categories:
author: "Paul Rubens"
tags: [Fork (software development),OpenOfficeorg,LibreOffice,Nextcloud,Technology]
---


In their study of software forks, researchers Gregorio Robles and Jesus M. Gonzalez-Barahona of the Universidad Rey Juan Carlos in Spain define a fork like this:  [ Also on CIO.com: What CIOs don't know about open source software ]  Forking occurs when a part of a development community (or a third-party not related to the project) starts a completely independent line of development based on the source code basis of the project. And a new developer community (disjoint with the original). Looking at forks from both sides now  Forks may not be all that common, but that doesn’t make the thought of one any less unnerving if the project in question is one your company relies on. If a fork is successful then companies will come in to provide support for the fork. In fewer than nine percent of such cases both the original project and the fork were discontinued.

<hr>

[Visit Link](http://www.networkworld.com/article/3127133/open-source-tools/what-cios-need-to-know-about-open-source-forking.html#tk.rss_opensourcesubnet){:target="_blank" rel="noopener"}


