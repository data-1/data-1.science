---
layout: post
title: "Making solar hydrogen generation more efficient in microgravity"
date: 2018-07-16
categories:
author: "Bob Yirka"
tags: [Photoelectrochemical cell,Hydrogen,Micro-g environment,Chemistry,Materials,Physical sciences,Nature,Technology,Applied and interdisciplinary physics]
---


Whereas H2 is formed at discretionary nucleation spots on the thin-film electrode surface (a) resulting in gas bubble coalescence and the formation of a bubble froth layer, the nanostructured Rh surface favours the formation of H2 gas bubbles at the induced Rh tips, catalytic hot spots (b). The first stage involves generating electricity using solar cells. The researchers note that this process works, but it is inefficient. Efficient solar hydrogen generation in microgravity environment, Nature Communications (2018). Efficient solar hydrogen generation in microgravity environment,(2018).

<hr>

[Visit Link](https://phys.org/news/2018-07-solar-hydrogen-efficient-microgravity.html){:target="_blank" rel="noopener"}


