---
layout: post
title: "Italy seeks to phase out coal power plants by 2025"
date: 2017-10-26
categories:
author: Amanda Froelich
tags: []
---


The latest country to prove its commitment to sustainable solutions is Italy. On Tuesday, the Italian Industry Minister announced that by 2025, the country plans to phase out coal power plants. Additionally, the country plans to meet 27 percent of “gross overall energy consumption” with renewable sources by the year 2030. Unlike other countries in Europe, Italy’s renewable sector is constantly growing. Hydro-electrical plants remain the biggest contributor (15.5 percent), and solar and wind sources have reached nearly 13 percent, according to ZME Science.

<hr>

[Visit Link](https://inhabitat.com/italy-seeks-to-phase-out-coal-power-plants-by-2025){:target="_blank" rel="noopener"}


