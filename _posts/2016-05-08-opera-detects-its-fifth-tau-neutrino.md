---
layout: post
title: "OPERA detects its fifth tau neutrino"
date: 2016-05-08
categories:
author: Cian O'Luanaigh
tags: [OPERA experiment,Neutrino,Laboratori Nazionali del Gran Sasso,Muon,Particle physics,Physics,Leptons,Elementary particles,Fermions,Subatomic particles,Nuclear physics,Neutrinos,Physical sciences]
---


A particle that left CERN as a muon neutrino underwent oscillation during its 730-kilometre journey to Gran Sasso, arriving as a tau neutrino. Researchers at Gran Sasso announced the result yesterday, naming it 5 sigma on the scale that particle physicists use to describe the certainty of results. OPERA was designed to search for tau neutrinos in the muon-neutrino beam from CERN, which ran during 2006-2012; detecting tau neutrinos in this beam is proof that oscillation occurred during their particles' 730 km long flight. The pions and kaons then decayed into muons and muon neutrinos in a 1-kilometre long pipe underground. After detecting the first few muon neutrinos produced at CERN in 2006, the experiment collected data from 2008 to the end of 2012.

<hr>

[Visit Link](http://phys.org/news353661759.html){:target="_blank" rel="noopener"}


