---
layout: post
title: "What are the different types of renewable energy?"
date: 2016-05-08
categories:
author: Matt Williams
tags: [Solar power,Wind power,Renewable energy,Geothermal power,Tidal power,Photovoltaics,Biofuel,Power station,Solar cell,Biomass,Concentrated solar power,Energy conversion,Technology,Environmental technology,Electric power,Natural resources,Climate change mitigation,Energy technology,Energy and the environment,Energy,Nature,Sustainable technologies,Sustainable energy,Renewable resources,Physical quantities,Sustainable development]
---


Meanwhile, solar-thermal power (another form of solar power) relies on mirrors or lenses to concentrate a large area of sunlight, or solar thermal energy (STE), onto a small area (i.e. a solar cell). Not only are they a relatively inexpensive source of energy where grid power is inconvenient, too expensive, or just plain unavailable; increases in solar cell efficiency and dropping prices are making solar power competitive with conventional sources of power (i.e. fossil fuels and coal). This report stated that worldwide, wind power could provide as much as 25 to 30% of global electricity by 2050. Geothermal:  Geothermal electricity is another form of alternative energy that is considered to be sustainable and reliable. Until very recently, running coal-fired or oil-powered plants was cheaper than investing millions in the construction of large solar, wind, tidal or geothermal operations.

<hr>

[Visit Link](http://phys.org/news353315097.html){:target="_blank" rel="noopener"}


