---
layout: post
title: "Injectable cryogel-based whole-cell cancer vaccines: A new minimally invasive vaccine that combines cancer cells and immune-enhancing factors offers a promising strategy against patient-specific cance"
date: 2016-06-14
categories:
author: "Wyss Institute for Biologically Inspired Engineering at Harvard"
tags: [Immunotherapy,Cancer,Immune system,Cancer immunotherapy,Vaccine,Medical treatments,Health sciences,Biotechnology,Biology,Medicine,Medical specialties,Clinical medicine]
---


Mooney, who leads a Wyss Institute team developing a broad suite of novel cancer vaccines and immunotherapies, is also the Robert P. Pinkas Family Professor of Bioengineering at the Harvard John A. Paulson School of Engineering and Applied Sciences. His team's latest approach differs from other cancer cell transplantation therapies -- which harvest tumor cells and then genetically engineer them to trigger immune responses once they are transplanted back into the patient's body -- in that the new cryogel vaccine's properties are used to evoke the immune response in a far simpler and more economical way. Instead of genetically engineering the cancer cells to influence the behavior of immune cells, we use immune-stimulating chemicals or biological molecules inserted alongside harvested cancer cells in the porous, sponge-like spaces of the cryogel vaccine, said Mooney. This has two consequences: immune cells become primed to mount a robust and destructive response against patient-specific tumor tissue and the immune tolerance developing within the tumor microenvironment is broken, said Sidi Bencherif, the study's co-first author and a Research Associate in Mooney's research group. This promising new approach is a great example of the power of collaboration across disciplines, bringing together expertise from the Wyss Institute and Dana-Farber spanning bioengineering, cancer biology and immunology, said Mooney.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150825125809.htm){:target="_blank" rel="noopener"}


