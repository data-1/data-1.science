---
layout: post
title: "From hummingbird to owl: New research decodes bird family tree"
date: 2015-10-20
categories:
author: Florida State University
tags: [Bird,Dinosaur,News aggregator,Biology,Genetics,Animals]
---


The rapid extinction of dinosaurs 65 million years ago gave rise to a stunning variety of bird species over the next few million years, according to Florida State University researchers. A study published in the journal Nature in coordination with Yale University resolved the bird family tree, something that has never been accomplished by scientists. “These birds just diversified rapidly after dinosaurs went extinct,” said Emily Moriarty Lemmon, assistant professor in the Department of Biological Science at FSU. This is the first time scientists have been able to show a complete family tree for birds. The Lemmons and their collaborators, in contrast, analyzed several hundred genes for four times the number of bird species at a fraction of the cost of the previous study and successfully uncovered the bird family tree.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151012181033.htm){:target="_blank" rel="noopener"}


