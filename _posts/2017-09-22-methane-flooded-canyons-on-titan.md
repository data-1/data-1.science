---
layout: post
title: "Methane-flooded canyons on Titan"
date: 2017-09-22
categories:
author: ""
tags: [Titan (moon),Vid Flumina,CassiniHuygens,Planets of the Solar System,Physical sciences,Planemos,Planets,Astronomical objects,Astronomical objects known since antiquity,Solar System,Bodies of the Solar System,Planetary science,Space science,Outer space,Astronomy]
---


The aptly named Titan, Saturn’s largest moon, is remarkably Earth-like. This image, taken with the radar on the Cassini spacecraft, shows just how similar the features in Titan’s surface are to Earth’s landforms. Aside from Earth, Titan is the only other body where we have found evidence of active erosion on a large scale. Researchers in Italy and the US analysed Cassini radar observations from May 2013 and recently revealed that the narrow channels that branch off Vid Flumina are deep, steep-sided canyons filled with flowing hydrocarbons. The Cassini–Huygens radar team is hoping to observe the Ligeia Mare and Vid Flumina region again in April 2017, during Cassini’s final approach to Titan.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2016/09/Methane-flooded_canyons_on_Titan){:target="_blank" rel="noopener"}


