---
layout: post
title: "Researchers successfully produce genome-edited pigs using revolutionary technology"
date: 2016-04-19
categories:
author: Sara Gavin, University Of Maryland
tags: [Pig,Biology,Life sciences]
---


Eighteen piglets born recently are the result of two years of intense research by scientists in the College of Agriculture and Natural Resources at the University of Maryland and represent a breakthrough in the field of genetic engineering. Bhanu Telugu, PhD, an assistant professor in the Department of Animal & Avian Sciences (ANSC) and Ki-Eun Park, PhD, a faculty research assistant in ANSC, successfully produced genome-edited pigs using a recently developed, groundbreaking technique called the CRISPR system. Known as a target and replace/modify function for DNA, the CRISPR system has dramatically improved scientists' ability to disable genes or modify their function inside any living cell. From a bio-medical standpoint, the pig is really one of the most important animals, said Telugu, noting that other large animal models like cows or sheep don't have digestive systems, diets or physiology similar enough to humans' to provide insight into human diseases. Now that the researchers have been successful using this revolutionary technology, they plan to look for applications that would improve animal welfare, including disease resistance.

<hr>

[Visit Link](http://phys.org/news346575344.html){:target="_blank" rel="noopener"}


