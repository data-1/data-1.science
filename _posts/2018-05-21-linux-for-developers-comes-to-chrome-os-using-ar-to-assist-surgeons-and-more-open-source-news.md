---
layout: post
title: "Linux for developers comes to Chrome OS, using AR to assist surgeons, and more open source news"
date: 2018-05-21
categories:
author: "Michelle Greenlee"
tags: [Red Hat,Augmented reality,Chrome OS,Open source,Linux,Computing,Communication,Software development,Intellectual works,Digital media,Software,Technology]
---


Linux for developers is coming to Chrome OS  Developers will soon have the option to run a Linux virtual machine within Chrome OS. A selection of popular code editors will also be available, including Android Studio IDE. Augmented reality amplifies X-ray images to help surgeons  A study published in the Journal of Medical Imaging proposes the addition of augmented reality to some minimally invasive surgeries. Ed Department rolling out free digital textbook program  The U.S. Department of Education plans to expand access to free, open digital textbooks and materials for colleges across the nation. Make sure to check out our event calendar to see what's happening next week in open source.

<hr>

[Visit Link](https://opensource.com/article/18/5/news-may-12){:target="_blank" rel="noopener"}


