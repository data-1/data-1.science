---
layout: post
title: "China Developer Survey Report 2017 Revisited"
date: 2018-08-02
categories:
author: "Aug."
tags: []
---


They also communicated their understanding and practices in multiple technical fields such as web development, front-end development, cloud computing, big data, artificial intelligence, blockchain, and security. Cloud Computing  Thanks to developers' preferences, three very similar significant cloud deployment alternatives exist, namely public cloud, hybrid cloud, and private cloud. The top three tools are:  Code management tools (37.9%) Deployment tools (31.1%) API management tools (26.2%)  Big Data  According to the survey, 23.6% of companies are now generating 1-10T of data every day, and over 20% are creating more than 10T per day. Security  75.4% of security teams in China have less than 10 employees. Conclusion  The Alibaba Cloud Developer Community hopes to paint a precise picture of developers and the software industry in China with the China Developer Survey Report.

<hr>

[Visit Link](https://dzone.com/articles/china-developer-survey-report-2017-revisited?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


