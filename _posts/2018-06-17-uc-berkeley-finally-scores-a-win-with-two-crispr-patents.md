---
layout: post
title: "UC Berkeley Finally Scores a Win With Two CRISPR Patents"
date: 2018-06-17
categories:
author: ""
tags: [Jennifer Doudna,CRISPR gene editing,Intellectual property law,Intellectual works,Patent law]
---


The U.S. Patent and Trademark Office (USPTO) just decided to grant not one, but two new CRISPR patents to UC Berkeley, home of biochemist Jennifer Doudna, who many consider the creator of CRISPR. A patent gives an inventor legal ownership of their unique invention or discovery. While the CRISPR-Cas9 patent currently owned by the Broad team is arguably the most valuable, not the only CRISPR patent out there. The USPTO will reportedly grant UC Berkeley the other patent, which the university applied for in 2015, next week, according to a STAT News report. UC Berkley sees a number of potential applications in research, diagnostics, and industry for their new CRISPR patent.

<hr>

[Visit Link](https://futurism.com/crispr-patents-uc-berkeley/){:target="_blank" rel="noopener"}


