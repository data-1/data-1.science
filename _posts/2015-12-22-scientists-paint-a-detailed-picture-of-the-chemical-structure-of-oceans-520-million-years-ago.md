---
layout: post
title: "Scientists paint a detailed picture of the chemical structure of oceans 520 million years ago"
date: 2015-12-22
categories:
author: Science China Press
tags: [Ocean,Anoxic waters,Redox,Life,Stratification (water),Biogeochemistry,Chemistry,Environmental engineering,Earth sciences,Physical geography,Nature,Physical sciences,Hydrography,Oceanography,Hydrology,Chemical oceanography,Applied and interdisciplinary physics,Environmental science,Geochemistry,Systems ecology,Natural environment]
---


Although it was known that the early oceans (>520 million years ago) were characterized by strong water-column stratification and limited oxidant availability, the detailed chemical structure of early Earth oceans has remained unclear. These features are also characteristic of sediment porewaters of modern productive continental margins and the seawater of modern anoxic restricted basins, in which multiple chemical zones are developed owing to limited oxygen availability. According to the researchers, one key difference between modern and ancient oceans is the spatial pattern of oxidant availability. Thus, our model not only provides a robust theoretical framework for future studies of the evolution of ocean chemistry and biogeochemical cycles in early Earth history, but also may be of significance in understanding how progressive oxygenation of the Earth oceans influenced the evolution of early life, Dr. Li said. 41172030); the U.S. National Science Foundation, the NASA Exobiology program, and the China University of Geosciences (Wuhan) (SKL-GPMR program GPMR201301, and SKL-BGEG program BGL21407).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/scp-spa121115.php){:target="_blank" rel="noopener"}


