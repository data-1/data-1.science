---
layout: post
title: "In Photos: Ada Lovelace Exhibit at London's Science Museum"
date: 2015-10-16
categories:
author: Live Science Staff
tags: [Analytical Engine,Ada Lovelace,Jacquard machine,Charles Babbage,Difference engine,Technology,Computer science,Science,Branches of science,Computing]
---


From that point on, she used the name she is known by today: Ada Lovelace. The machine was never completed, but Babbage did build this segment to demonstrate what the machine could do. (Credit: Science Museum/SSPL)  Woven portrait  By the early 19th century, many weaving looms used punched cards to control and determine the woven pattern. (Credit: Science Museum)  Analytical Engine  In 1834, Charles Babbage began work on a new calculating machine, called the Analytical Engine. (Credit: Science Museum)  New exhibit  A view of the Ada Lovelace exhibit at the Science Museum in London.

<hr>

[Visit Link](http://www.livescience.com/52463-ada-lovelace-museum-exhibit-photos.html){:target="_blank" rel="noopener"}


