---
layout: post
title: "Pioneering Psychologist William James on Attention, Multitasking, and the Mental Habit That Sets Great Minds Apart"
date: 2016-03-25
categories:
author: Maria Popova
tags: [Experience,Attention,Mind,Consciousness,Psychological concepts,Neuroscience,Science,Neuropsychology,Philosophical theories,Metaphysics,Concepts in the philosophy of mind,Cognitive psychology,Mental processes,Metaphysical theories,Metaphysics of mind,Theory of mind,Neuropsychological assessment,Concepts in metaphysics,Cognition,Cognitive science,Psychology,Interdisciplinary subfields]
---


Because they have no interest for me. Only those items which I notice shape my mind — without selective interest, experience is an utter chaos. […] When expectant attention is concentrated upon one of two sensations, that the other one is apt to be displaced from consciousness for a moment and to appear subsequent; although in reality the two may have been contemporaneous events. The act of paying attention and the way in which it is performed, James argues, is what sets geniuses apart from ordinary people:  Sustained attention is the easier, the richer in acquisitions and the fresher and more original the mind. In such minds, subjects bud and sprout and grow.

<hr>

[Visit Link](https://www.brainpickings.org/2016/03/25/william-james-attention/){:target="_blank" rel="noopener"}


