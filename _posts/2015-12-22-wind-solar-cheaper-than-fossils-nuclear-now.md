---
layout: post
title: "Wind & Solar Cheaper Than Fossils & Nuclear Now"
date: 2015-12-22
categories:
author: Guest Contributor, Carolyn Fortuna, Michael Barnard, Written By
tags: [Solar energy,Wind power,Rooftop solar power,Emissions trading,Climate change,Energy and the environment,Physical quantities,Sustainable development,Sustainable technologies,Renewable resources,Economy,Electric power,Natural resources,Power (physics),Climate change mitigation,Renewable energy,Sustainable energy,Energy,Nature]
---


By Henry Lindon  Wind energy and solar energy are notably beating out conventional generation modalities (coal, natural gas, nuclear, etc) with regard to production costs and abatement as well, according to a new study from the US investment bank Lazard. The report notes that, despite recent drops in the cost of natural gas in the US, solar and wind energy are still beating out conventional modalities in most situations — partly owing to the fact that solar and wind energy costs have dropped by 80% and 60% since 2009. Utility-scale solar photovoltaic (PV) costs have actually even fallen by 25% just over the last year, according to the new study. Therefore, the optimal solution for many regions of the world is to use complementary traditional and alternative energy resources in a diversified generation fleet.”  Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage? Or follow us on Google News Have a tip for CleanTechnica, want to advertise, or want to suggest a guest for our CleanTech Talk podcast?

<hr>

[Visit Link](http://cleantechnica.com/2015/12/03/wind-solar-cheaper-than-fossils-nuclear-now/){:target="_blank" rel="noopener"}


