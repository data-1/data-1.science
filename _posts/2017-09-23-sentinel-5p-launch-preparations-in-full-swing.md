---
layout: post
title: "Sentinel-5P launch preparations in full swing"
date: 2017-09-23
categories:
author: ""
tags: [Sentinel-5,Copernicus Programme,Satellite,Atmosphere,Outer space,Spaceflight,Space science,Flight,Space vehicles,Astronautics]
---


With liftoff set for 13 October, engineers at Russia’s Plesetsk launch site are steaming ahead with the task of getting Europe’s next Copernicus satellite ready for its journey into orbit. The Sentinel-5P satellite has been at Plesetsk in northern Russia for almost two weeks. So far, it has been taken out of its transport container, positioned for testing and engineers have started ticking off the jobs on the long ‘to do’ list. William Simpson, ESA’s Sentinel-5P satellite and launch manager, said, “It was great to see it arrive here in Plesetsk where the team was ready and waiting, keen to make sure the satellite was in good health after its voyage from the UK. This is always a tense moment, so there were smiles and cheers all round when the satellite clicked on, showing that all was well.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-5P/Sentinel-5P_launch_preparations_in_full_swing){:target="_blank" rel="noopener"}


