---
layout: post
title: "New lobster-like predator found in 508 million-year-old fossil-rich site"
date: 2015-07-21
categories:
author: University of Toronto 
tags: [Arthropod,Burgess Shale,Animal,Jean-Bernard Caron,Antenna (biology),Radiodonta,Fossil,Paleontology,Animals]
---


The fossil was identified by an international team led by palaeontologists at the University of Toronto (U of T) and the Royal Ontario Museum (ROM) in Toronto, as well as Pomona College in California. It is the first new species to be described from the Marble Canyon site, part of the renowned Canadian Burgess Shale fossil deposit. This creature is expanding our perspective on the anatomy and predatory habits of the first arthropods, the group to which spiders and lobsters belong, said Cedric Aria, a PhD candidate in U of T's Department of Ecology & Evolutionary Biology and lead author of the resulting study published this week in Palaeontology. This animal is therefore important for the study of Marble Canyon, and shows how the site increases the significance of the Burgess Shale in understanding the dawn of animals. Aria's doctoral research is supported by a Natural Sciences and Engineering Research Council of Canada discovery grant to Jean-Bernard Caron and University of Toronto fellowships.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/uot-nlp032415.php){:target="_blank" rel="noopener"}


