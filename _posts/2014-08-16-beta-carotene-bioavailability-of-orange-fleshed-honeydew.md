---
layout: post
title: "Beta-carotene bioavailability of orange-fleshed honeydew"
date: 2014-08-16
categories:
author: Rosalie Marion Bliss, Agricultural Research Service
tags: [-Carotene,Vitamin,Vitamin A,Nutrition]
---


But according to researchers, there are more than 100 million people worldwide who have vitamin A deficiency, and for some of them, consuming fruits and vegetables is the most available treatment. Select fruits and vegetables contain carotenoids such as beta-carotene, also known as provitamin A. Beta-carotene is the most potent precursor of vitamin A for humans (meaning the body breaks down beta-carotene into vitamin A). The team found that orange-fleshed honeydew had significantly higher beta-carotene concentrations than cantaloupe, but the two melon types had similar beta-carotene bioaccessibilities. In the laboratory, the researchers also tested the bioavailability of beta-carotene from orange-fleshed honeydew melon tissue. Matthew K. Fleshman, Gene E. Lester, Ken M. Riedl, Rachel E. Kopec, Sureshbabu Narayanasamy, Robert W. Curley, Jr., Steven J. Schwartz, and Earl H. Harrison.., 2011, 59 (9), pp 4448–4454.

<hr>

[Visit Link](http://phys.org/news323503040.html){:target="_blank" rel="noopener"}


