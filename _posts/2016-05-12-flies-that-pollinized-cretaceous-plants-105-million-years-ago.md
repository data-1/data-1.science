---
layout: post
title: "Flies that pollinized Cretaceous plants 105 million years ago"
date: 2016-05-12
categories:
author: Universidad de Barcelona
tags: [Pollination,Flower,Bennettitales,Flowering plant,Gymnosperm,Pollinator,Kingdoms (biology),Archaeplastida,Organisms,Botany,Plants]
---


However, in the Cretaceous -- about 105 million years ago -- bees and butterflies did not exist, and most terrestrial ecosystems were dominated by non-flowering plants (gymnosperms). An international research team has recently discovered some amber fly specimens in El Soplao cave (Cantabria, Spain). According to an article published in the scientific journal Current Biology, these specimens fed on nectar and pollinized gymnosperm plants 105 million years ago. Xavier Delclòs, professor in the Department ofStratigraphy, Paleontology and Marine Geosciences and researcher at the Biodiversity Research Institute (IRBio) of the University of Barcelona, is one of the authors of the study. When angiosperms began to dominate terrestrial ecosystems  There are few known cases of insects that fossilized when they were transporting pollen from one flower to another.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150710081531.htm){:target="_blank" rel="noopener"}


