---
layout: post
title: "30 years and counting, the X-ray laser lives on"
date: 2016-04-23
categories:
author: Lawrence Livermore National Laboratory
tags: [SLAC National Accelerator Laboratory,Laser,X-ray,X-ray laser,Photon,Physical chemistry,Applied and interdisciplinary physics,Optics,Physics,Chemistry,Science,Electrodynamics,Radiation,Electromagnetic radiation,Electromagnetism]
---


Researchers used two beams on Nova for X-ray laser experiments. X-ray lasers have evolved quite a bit since this circa 1985 image. I was working with them (Chapline and Wood) a bit because I was interested in this, said Dennis Matthews, a longtime LLNL laser experimentalist, now at UC Davis. London worked with Rosen as a postdoc at the time. Lawrence Livermore researchers later developed table-top X-ray lasers to initially explore plasmas (also known as a fourth state of matter), but eventually X-ray lasers moved into a variety of fields in the material science and biology worlds by supplying detailed information about the molecular-scale structure of new and existing materials.

<hr>

[Visit Link](http://phys.org/news348301570.html){:target="_blank" rel="noopener"}


