---
layout: post
title: "Holometer rules out first theory of space-time correlations"
date: 2015-12-22
categories:
author: Andre Salles, Fermi National Accelerator Laboratory
tags: [Holometer,LIGO,Physics,Science]
---


A member of the Holometer collaboration works on the sensitive space-time measuring device, located at Fermilab in Illinois. A scientist works on the laser interferometer at the heart of the Holometer experiment. We've developed a new way of studying space and time that we didn't have before. Hogan said the Holometer team will continue to take and analyze data, and will publish more general and more sensitive studies of holographic noise. It's new technology, and the Holometer is just the first example of a new way of studying exotic correlations, Hogan said.

<hr>

[Visit Link](http://phys.org/news/2015-12-holometer-theory-space-time.html){:target="_blank" rel="noopener"}


