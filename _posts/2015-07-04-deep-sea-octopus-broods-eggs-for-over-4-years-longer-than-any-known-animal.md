---
layout: post
title: "Deep-sea octopus broods eggs for over 4 years -- longer than any known animal"
date: 2015-07-04
categories:
author: Monterey Bay Aquarium Research Institute 
tags: [Monterey Bay Aquarium Research Institute,Octopus,Cephalopod,Animals,Organisms]
---


Every few months for the last 25 years, a team of MBARI researchers led by Bruce Robison has performed surveys of deep-sea animals at a research site in the depths of Monterey Canyon that they call Midwater 1. The last time the researchers saw the brooding octopus was in September 2011. After counting the remnants of the egg capsules, the researchers estimated that the female octopus had been brooding about 160 eggs. Such long brooding times present an evolutionary challenge, especially for animals such as octopus, which do not live very long. Although long-term observations of deep-sea animals are rare, the researchers propose that extended brooding periods may be common in the deep sea.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/mbar-dob072214.php){:target="_blank" rel="noopener"}


