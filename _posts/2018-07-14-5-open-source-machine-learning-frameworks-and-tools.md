---
layout: post
title: "5 Open-Source Machine Learning Frameworks and Tools"
date: 2018-07-14
categories:
author: "Nov."
tags: []
---


TensorFlow  Made public and open-sourced two years ago, TensorFlow is Google's own internal framework for deep learning (artificial neural networks). TensorFlow is general enough to be used for building any type of network — from text to image classifiers, to more advanced models like generative adversarial neural networks (GANs), and even allows other frameworks to be built on top of it (see Keras and Edward down the list). It then displays the results in a readable and interpretable way — for example, an explanation for a text classifier would look like this, highlighting the words that helped the model reach a decision, and their respective probabilities:  Image taken from project’s GitHub page  Lime supports — learn models out of the box, as it does for any classifier model that takes in raw text or an array, and outputs a probability for each class. It can also explain image classification models. Image taken from project’s GitHub page  Endnotes  We have listed here three major frameworks and two promising tools for machine learning researchers and engineers to consider when approaching a project, either by building it from scratch or trying to improve it.

<hr>

[Visit Link](https://dzone.com/articles/5-open-source-machine-learning-frameworks-and-tool?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


