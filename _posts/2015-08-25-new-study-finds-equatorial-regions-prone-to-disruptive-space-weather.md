---
layout: post
title: "New study finds equatorial regions prone to disruptive space weather"
date: 2015-08-25
categories:
author: Boston College 
tags: [Space weather,Weather,Earth,Tropics,Solar wind,Geomagnetic storm,Earths magnetic field,Geomagnetically induced current,Equator,Geophysics,Equatorial electrojet,Electrical grid,Applied and interdisciplinary physics,Space science,Nature,Electromagnetism,Earth sciences,Astronomy,Physical geography,Electrical engineering]
---


These equatorial electrical disruptions - fueled by geomagnetically induced currents - pose a threat to power grids in countries where shielding electricity infrastructure from space shocks has not been a recognized priority. In other words, electrical disruptions in the equatorial region do not require severe geomagnetic storms, similar in scale to events that have crashed power grids in the past, most notably in Quebec in 1989 and in Sweden in 2003. Analyzing 14 years of data collected in space and on Earth, the team found that geomagnetically induced currents are amplified by the equatorial electrojet, a naturally occurring flow of current approximately 100 kilometers above the surface of the Earth. In their report, Carter and his team, including researchers from RMIT and Dartmouth College, examine the effects of interplanetary shocks in the solar wind, which is the stream of charged particles that flows out of the Sun. The Earth's magnetic field does the job of shielding the Earth from the solar wind and when it gets hit by these shocks, you get a global magnetic signature at the ground, Carter said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/bc-nsf081415.php){:target="_blank" rel="noopener"}


