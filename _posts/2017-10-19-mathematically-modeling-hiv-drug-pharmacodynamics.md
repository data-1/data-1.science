---
layout: post
title: "Mathematically modeling HIV drug pharmacodynamics"
date: 2017-10-19
categories:
author: Society For Industrial, Applied Mathematics
tags: [Management of HIVAIDS,Virus latency,Virus,Infection,HIV,Antiviral drug,Pharmacokinetics,Pharmacodynamics,Medicine,Medical specialties,Clinical medicine,Health sciences,Biology,Life sciences,Health]
---


Viral reservoirs exist within resting CD4+ memory T-cells that maintain replication-competent HIV for extended time periods, allowing viral persistence even in the face of immune surveillance or antiretroviral therapy. The pharmacodynamic properties of drugs and their effects on success of treatment have so far received little attention. Our research uses mathematical modeling to gain deeper insights into the effects of antiretroviral therapy on HIV latent infections, and highlights that the pharmacodynamics of drugs—and thus, the choice of drugs—used in treatment regimens can be a determinant factor for successful therapy, Vaidya says. Credit: Eraxion/ Masterfile  Their model specifically focuses on the impact of antiretroviral therapy early in treatment to control latently infected cells. However, once the latent infection is established, the pharmacodynamic parameters have less effect on the latent reservoir and virus dynamics, Vaidya says.

<hr>

[Visit Link](https://phys.org/news/2017-10-mathematically-hiv-drug-pharmacodynamics.html){:target="_blank" rel="noopener"}


