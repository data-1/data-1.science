---
layout: post
title: "Video: Dark matter hunt with LUX-ZEPLIN"
date: 2017-09-22
categories:
author: "Slac National Accelerator Laboratory"
tags: [Weakly interacting massive particles,Matter,Dark matter,LZ experiment,Science]
---


SLAC is helping to build and test the LUX-ZEPLIN (LZ) detector, one of the biggest and most sensitive detectors ever designed to catch hypothetical dark matter particles known as weakly interacting massive particles (WIMPs). Credit: Greg Stewart/SLAC National Accelerator Laboratory  Researchers at the Department of Energy's SLAC National Accelerator Laboratory are on a quest to solve one of physics' biggest mysteries: What exactly is dark matter – the invisible substance that accounts for 85 percent of all the matter in the universe but can't be seen even with our most advanced scientific instruments? Leading candidates for dark matter particles are WIMPs, or weakly interacting massive particles. The following video explains how it works. Credit: SLAC National Accelerator Laboratory  Explore further New theory on the origin of dark matter

<hr>

[Visit Link](https://phys.org/news/2017-08-video-dark-lux-zeplin.html){:target="_blank" rel="noopener"}


