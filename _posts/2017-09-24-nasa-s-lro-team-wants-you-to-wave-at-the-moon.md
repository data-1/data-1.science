---
layout: post
title: "NASA's LRO team wants you to wave at the moon"
date: 2017-09-24
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Lunar Reconnaissance Orbiter,Solar eclipse,Goddard Space Flight Center,Moon,Earth,Planetary science,Space science,Outer space,Astronomy,Solar System,Bodies of the Solar System,Spaceflight,Astronomical objects known since antiquity,Astronautics]
---


NASA's Lunar Reconnaissance Orbiter (LRO) team invites the public to wave at the Moon on Aug. 21 as LRO turns its camera toward Earth. The LRO Camera, which has captured gorgeous views of the lunar landscape and documented geologic activity still occurring today, will turn toward Earth during the solar eclipse on Aug. 21 at approximately 2:25 p.m. EDT (11:25 a.m. PDT) to capture an image of the Moon's shadow on Earth. I'm really excited about this campaign because it is something so many people can be a part of, said Andrea Jones, LRO public engagement lead at NASA's Goddard Space Flight Center in Greenbelt, Maryland. While the LRO Camera won't be able to see people or buildings, it will be able to see the continents, clouds and large surface features. A note of caution: the only time it's safe to look at the Sun without eye protection is if you're in the 70-mile-wide path of totality and only during the minutes of totality.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-08/nsfc-nlt081617.php){:target="_blank" rel="noopener"}


