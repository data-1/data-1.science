---
layout: post
title: "Slow but steady: New study sheds light on the brain evolution of turtles"
date: 2018-08-02
categories:
author: "University of Birmingham"
tags: [Proganochelys,Turtle,Brain,Nature,Animals]
---


A new study led by the University of Birmingham shows that the brain of turtles has evolved slowly, but constantly over the last 210 million years, eventually reaching a variety in form and complexity, which rivals that of other animal groups. Turtles are one of the oldest vertebrate groups still alive today. The team's research, published today (1 February 2018) in the journal Frontiers in Ecology and Evolution, focussed on the fossils of the oldest turtle with a fully formed shell: Proganochelys quenstedti, found in the Triassic sediments (ca. Results of this study further showed that the turtle brain increased in size and complexity over the course of evolution to modern turtles. Gabriel Ferreira from the University of São Paulo, Brazil, who also co-authored the study, explained: 'By comparing the digital brain reconstruction of Proganochelys with those of modern turtles we can show that the first turtles with a fully formed shell were very likely living on land and not in the water or in an fossorial environment.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/uob-sbs013018.php){:target="_blank" rel="noopener"}


