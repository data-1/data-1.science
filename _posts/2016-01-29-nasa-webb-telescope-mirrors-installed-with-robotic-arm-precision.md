---
layout: post
title: "NASA Webb Telescope mirrors installed with robotic arm precision"
date: 2016-01-29
categories:
author: NASA/Goddard Space Flight Center
tags: [James Webb Space Telescope,Mirror,Telescope,Goddard Space Flight Center,NASA,Curiosity (rover),Optics,Science,Astronomy,Space science,Spaceflight,Electromagnetic radiation,Outer space,Technology]
---


The team uses a robotic arm called the Primary Mirror Alignment and Integration Fixture to lift and lower each of Webb's 18 primary flight mirror segments to their locations on the telescope structure. To precisely install the segments, the robotic arm can move in six directions to maneuver over the telescope structure. While one team of engineers maneuvers the robotic arm, another team of engineers simultaneously takes measurements with lasers to ensure each mirror segment is placed, bolted and glued perfectly before moving to the next. While the team is installing the mirrors there are references on the structure and the mirrors that allow the team to understand where the final mirror surface is located, said Harris Corporation's James Webb Space Telescope's Assembly Integration and Test Director Gary Matthews Greenbelt, Maryland. The James Webb Space Telescope is the scientific successor to NASA's Hubble Space Telescope.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/nsfc-nwt012716.php){:target="_blank" rel="noopener"}


