---
layout: post
title: "Immunity gene fusions uncovered in plants"
date: 2016-03-16
categories:
author: Earlham Institute
tags: [Plant disease resistance,Bioinformatics,Biotechnology and Biological Sciences Research Council,Biology,Biotechnology,Life sciences]
---


The fact that these fusions are common to the majority of surveyed flowering plants is an important discovery in combatting plant disease. Dr Ksenia Krasileva, Lead author of the study and Group Leader at TGAC/TSL, said: Our method for detecting variations in immune receptors across flowering plants revealed exciting new genes that might be important for plant health. About BBSRC  The Biotechnology and Biological Sciences Research Council (BBSRC) invests in world-class bioscience research and training on behalf of the UK public. Funded by Government, BBSRC invested over £509M in world-class bioscience in 2014-15 and is the leading funder of wheat research in the UK (over £100M investment on UK wheat research in the last 10 years). For more information about BBSRC, our science and our impact see: http://www.bbsrc.ac.uk  For more information about BBSRC strategically funded institutes see: http://www.bbsrc.ac.uk/institutes

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/tgac-igf021616.php){:target="_blank" rel="noopener"}


