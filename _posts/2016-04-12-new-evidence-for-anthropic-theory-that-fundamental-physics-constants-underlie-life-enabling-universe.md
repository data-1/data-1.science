---
layout: post
title: "New evidence for anthropic theory that fundamental physics constants underlie life-enabling universe"
date: 2016-04-12
categories:
author: Science China Press
tags: [Fine-tuned universe,Universe,Big Bang,Neutron,Anthropic principle,Physics,Chemical element,Atomic nucleus,Quark,Star,Big Bang nucleosynthesis,Physical sciences,Astronomy,Nature,Astrophysics,Nuclear physics,Particle physics,Science]
---


German scholar Ulf-G Meißner, chair in theoretical nuclear physics at the Helmholtz Institute, University of Bonn, adds to a series of discoveries that support this Anthropic Principle. On more microscopic scales, he adds, certain fundamental parameters of the Standard Model of light quark masses or the electromagnetic fine structure constant must take values that allow for the formation of neutrons, protons and atomic nuclei. In one series of experiments involving intricate computer simulations on JUQUUEN at Forschungszentrum Jülich, Professor Meißner and his colleagues altered the values of light quark masses from those found in Nature to determine how great a variation would prevent the formation of carbon or oxygen inside massive stars. Clearly, one can think of many universes, the multiverse, in which various fundamental parameters take different values leading to environments very different from ours, Professor Meißner states. See the article: Meißner, U. Anthropic considerations in nuclear physics Science Bulletin, 2015, 60(1) : 43-54  Link:  http://www.scibull.com:8080/EN/abstract/abstract509571.shtml  http://link.springer.com/article/10.1007%2Fs11434-014-0670-2  Science China Press is a leading publisher of scientific journals in China that operates under the auspices of the Chinese Academy of Sciences.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/scp-nef011415.php){:target="_blank" rel="noopener"}


