---
layout: post
title: "Leading the Way in the Ethical Use of AI"
date: 2018-07-18
categories:
author: "Apr."
tags: []
---


Ethical Use of AI  Despite this potential, the Committee set about exploring the ethical issues involved in the development of AI and how the UK can ensure the technology develops in the right way. The Committee has developed five principles around which they urge the development of AI to revolve:  Artificial intelligence should be developed for the common good and benefit of humanity. Artificial intelligence should not be used to diminish the data rights or privacy of individuals, families or communities. All citizens should have the right to be educated to enable them to flourish mentally, emotionally and economically alongside artificial intelligence. That the report follows the pattern of most of the other reports in being an intellectual exercise with no concrete steps to enact any of its recommendations suggest it will join the growing pile of well-meaning and largely sensible reports that result in nothing much changing.

<hr>

[Visit Link](https://dzone.com/articles/leading-the-way-in-the-ethical-use-of-ai?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


