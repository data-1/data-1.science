---
layout: post
title: "Global effort could cut ocean plastics by 77% by 2025"
date: 2018-08-18
categories:
author: "Hayley Jarvis, Brunel University"
tags: [Plastic pollution,Waste management,Recycling,Waste]
---


Seal caught in plastic rope. Credit: Alex Mustard, courtesy of Marine Conservation Society  Global funding to revolutionise waste management in the world's worst polluting countries could clean up ocean plastic by 77% by 2025. Launching the report at this week's Ocean Plastics Crisis Summit, Prof Kosior said: This dire situation calls for strong immediate action to slow the rate at which waste is produced. Speaking at London's Royal Geographical Society, Professor Kosior who runs recycling company, Nextek dubbed the UK's export of plastics recycling to China 'scandalous' because it stops developing recycling here. Developed countries need to invest in developing countries with global support from the UN, he said, to help poorer countries better manage their waste.

<hr>

[Visit Link](https://phys.org/news/2018-02-global-effort-ocean-plastics.html){:target="_blank" rel="noopener"}


