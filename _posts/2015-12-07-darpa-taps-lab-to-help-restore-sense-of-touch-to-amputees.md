---
layout: post
title: "DARPA taps lab to help restore sense of touch to amputees"
date: 2015-12-07
categories:
author: "$author" 
tags: [Prosthesis,Haptic technology,Amputation,Braincomputer interface,Somatosensory system,Nervous system,Neuroscience,Clinical medicine,Technology]
---


LIVERMORE, Calif. - The Defense Advanced Research Projects Agency (DARPA) recently selected Lawrence Livermore National Laboratory (LLNL) to join a collaborative research team that intends to build the world's first neural system to enable naturalistic feeling and movements in prosthetic hands. Lawrence Livermore's Neural Tech Group and their collaborators (Case Western Reserve University and the Louis Stokes Cleveland Veteran's Administration Medical Center) intend to develop neural interface systems that measure and decode motor signals recorded in peripheral nerves and muscles in the forearm by using tiny electrodes. The Revolutionizing Prosthetics and RE-NET programs, combined with the neural interface systems, intends to allow users to control prosthetic hand movements with their thoughts and have natural sensations. These packages would contain electronics that record and stimulate the peripheral nervous system to control movement and sensation in a patient's prosthetic hand. The packages have to be really small, so they don't put any weight or pressure on the nerves, said Pannu, adding that the smart packages need to bond with the electrodes to function.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/dlnl-dtl020915.php){:target="_blank" rel="noopener"}


