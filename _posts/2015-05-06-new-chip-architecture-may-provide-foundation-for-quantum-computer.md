---
layout: post
title: "New chip architecture may provide foundation for quantum computer"
date: 2015-05-06
categories:
author: American Institute Of Physics
tags: [Quantum computing,Integrated circuit,Computing,Computer,Quantum mechanics,Qubit,Ball grid array,Capacitor,Trapped ion quantum computer,Science,Technology]
---


Such simulations could revolutionize chemistry, biology and material science, but the development of quantum computers has been limited by the ability to increase the number of quantum bits, or qubits, that encode, store and access large amounts of data. The scalability of current trap architectures is limited since the connections for the electrodes needed to generate the trapping fields come at the edge of the chip, and their number are therefore limited by the chip perimeter. The researchers also freed up more chip space by replacing area-intensive surface or edge capacitors with trench capacitors and strategically moving wire connections. The BGA project demonstrated that it's possible to fit more and more electrodes on a surface trap chip while wiring them from the back of the chip in a compact and extensible way. But another reason that we work on such difficult problems is that it forces us to come up with solutions that may be useful elsewhere.

<hr>

[Visit Link](http://phys.org/news350040441.html){:target="_blank" rel="noopener"}


