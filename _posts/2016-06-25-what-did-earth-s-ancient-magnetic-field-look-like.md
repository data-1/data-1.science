---
layout: post
title: "What did Earth's ancient magnetic field look like?"
date: 2016-06-25
categories:
author: "Carnegie Institution for Science"
tags: [Earths magnetic field,Earth,Magnetism,Earths inner core,Dynamo theory,Geophysics,Planet,Physical sciences,Nature,Space science,Astronomy,Science,Physics,Planetary science,Applied and interdisciplinary physics]
---


This motion is driven by the loss of heat from the core and the solidification of the inner core. Could this exception be explained by a major event like the solidification of the planet's inner core? Then, shortly after the predicted timing of the core solidification event, Driscoll's dynamo simulations predict that Earth's magnetic field transitioned back to a strong, two-pole one. Overall, the findings have major implications for Earth's thermal and magnetic history, particularly when it comes to how magnetic measurements are used to reconstruct continental motions and ancient climates. ###  The Carnegie Institution for Science (carnegiescience.edu) is a private, nonprofit organization headquartered in Washington, D.C., with six research departments throughout the U.S.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/cifs-wde062416.php){:target="_blank" rel="noopener"}


