---
layout: post
title: "A lesson from wheat evolution: From the wild to our spaghetti dish"
date: 2016-04-06
categories:
author: Oxford University Press
tags: [Wheat,Domestication,Durum,Pasta,Evolution,Biology]
---


In a new study published in the early online edition of Molecular Biology and Evolution, scientists Romina Beleggia, Roberto Papa et al., examined the rich evolutionary history of pasta wheat by measuring the changes in metabolic content from three different populations and exploiting innovative population genomics methods. Credit: Roberto Papa, Università Politecnica delle Marche  While wheat has been much maligned recently for it's gluten content, and new suspicions casted about as to its nutritional value, scientists have been eager to trace the evolutionary history of wheat to better understand the pasta wheat currently available. Since the dawn of agriculture, humans have been selecting plants to maximize both the crop yield and food benefits. But which has left the larger genomic footprint, and are there evolutionary tradeoffs with domestication? In a new study published in the early online edition of Molecular Biology and Evolution, scientists Romina Beleggia, Roberto Papa et al., examined the rich evolutionary history of pasta wheat by measuring the changes in metabolic content from three different populations and exploiting innovative population genomics methods.

<hr>

[Visit Link](http://phys.org/news/2016-04-lesson-wheat-evolution-wild-spaghetti.html){:target="_blank" rel="noopener"}


