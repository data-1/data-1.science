---
layout: post
title: "Our favourite pictures of 2015 – Physics World"
date: 2015-12-22
categories:
author: "$author" 
tags: [Physics,Science,Technology]
---


Thanks to the latest work done by an international team of researchers, electrical discharges could be controlled and guided with lasers, along complex paths and even around obstacles. A distant galaxy has created four images of a supernova even further away, via gravitational lensing, that have been captured for the first time by an international team of astronomers using the Hubble Space Telescope (HST). Mechanical engineer Haneesh Kesari and colleagues at Brown University and Harvard University in the US have now unravelled the clever design of the sponge by developing a mathematical model of the internal structure of a spicule. However, when inflated to the pressure at which the balloons would burst spontaneously, the initial slit would suddenly bifurcate to create a “Y” shape. Developed by researchers at RMIT University in Melbourne, Australia, together with colleagues in India, these artificial organic flowers self-assemble in water, blooming just like an actual flower.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/dec/14/our-favourite-pictures-of-2015){:target="_blank" rel="noopener"}


