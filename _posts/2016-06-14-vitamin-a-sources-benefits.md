---
layout: post
title: "Vitamin A: Sources & Benefits"
date: 2016-06-14
categories:
author: "Maddy Biddulph"
tags: [Vitamin A,Vitamin,-Carotene,Retinol,Bone,Vitamin A deficiency,Acne,Health,Hypervitaminosis A,Antioxidant,Nutrition,Health sciences,Medical specialties,Clinical medicine]
---


This means it’s absorbed into the body with dietary fat, and then stored in body tissue for later use. You can also get vitamin A by consuming plant sources of beta-carotene – found in leafy veg and yellow fruit such as mango, papaya and apricots – as the body can convert this into retinol. This can result in inflammation, cancer and heart disease.”  Other benefits of vitamin A include:  Reduced risk of blindness  An age-related eye disease study by the National Eye Institute (opens in new tab) found that taking high levels of antioxidants, such as vitamin A, along with zinc, might reduce the risk of developing advanced age-related macular degeneration by as much as 25%. It’s also important for healthy bones. According to Denby, there are two forms of vitamin A we can obtain from our diet:  1) Retinol, which is preformed vitamin A  2) Beta-carotene, which is a precursor to vitamin A, or more specifically retinol (i.e. beta-carotene must be converted by the body into vitamin A).

<hr>

[Visit Link](http://www.livescience.com/51975-vitamin-a.html){:target="_blank" rel="noopener"}


