---
layout: post
title: "In Just 4 Hours, Google's AI Mastered All The Chess Knowledge in History"
date: 2018-07-14
categories:
author: ""
tags: [AlphaZero,Artificial intelligence,AlphaGo Zero,AlphaGo,Science,Applied mathematics,Technology,Branches of science,Cognitive science,Cognition,Cybernetics,Emerging technologies,Computing,Learning,Computer science,Computational neuroscience]
---


In a new paper, Google researchers detail how their latest AI evolution, AlphaZero, developed superhuman performance in chess, taking just four hours to learn the rules before obliterating the world champion chess program, Stockfish. In a series of 100 games against Stockfish, AlphaZero won 25 games while playing as white (with first mover advantage), and picked up three games playing as black. This algorithm could run cities, continents, universes. That victory streak culminated in a startling success in October, in which a new fully autonomous version of the AI – which only learns by playing itself, never facing humans – bested all its former incarnations. For now, Google and DeepMind's computer scientists aren't commenting publicly on the new research, which hasn't as yet been peer-reviewed.

<hr>

[Visit Link](https://futurism.com/4-hours-googles-ai-mastered-chess-knowledge-history/){:target="_blank" rel="noopener"}


