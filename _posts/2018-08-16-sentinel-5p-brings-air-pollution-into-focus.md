---
layout: post
title: "Sentinel-5P brings air pollution into focus"
date: 2018-08-16
categories:
author: ""
tags: [Sentinel-5,Satellite,Air pollution,Copernicus Programme,German Aerospace Center,Human impact on the environment,Environmental issues,Natural environment,Earth sciences,Nature,Societal collapse]
---


Applications Sentinel-5P brings air pollution into focus 01/12/2017 58775 views 373 likes  Launched on 13 October, the Sentinel-5P satellite has delivered its first images of air pollution. This new mission promises to image air pollutants in more detail than ever before. One of these first images shows nitrogen dioxide over Europe. The animation shows high levels of this air pollutant over parts of Asia, Africa and South America. Sentinel-5P captures Bali volcanic eruption “Data such as we see here will soon underpin the Copernicus Atmosphere Monitoring Service, and will be used to issue forecasts, and will ultimately be valuable for helping to put appropriate mitigation policies in place.” Sentinel-5P carries the most advanced sensor of its type to date: Tropomi.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-5P/Sentinel-5P_brings_air_pollution_into_focus){:target="_blank" rel="noopener"}


