---
layout: post
title: "OpenSSH security and hardening"
date: 2018-07-13
categories:
author: "Michael Boelen"
tags: [OpenSSH,Superuser,Password,Login,Software,Security engineering,Utility software,Secure communication,Information Age,Computer engineering,Cyberwarfare,Software engineering,Computer architecture,Information technology management,Computer security,Computer science,System software,Technology,Software development,Computing]
---


After reading this article, you will know:  Where the client settings and server settings are stored  How to see the active and default settings  How to test your configuration settings  Make an informed decision on how to secure SSH  Which tools can help audit SSH and apply best practices  SSH basics  SSH has two parts: the server daemon (sshd) that runs on a system and the client (ssh) used to connect to the server. PermitEmptyPasswords no  If you see this option enabled, then check which user accounts have no password set. Use HashKnownHosts  Each time the SSH client connects to a server, it will store a related signature (a key) of the server. Instead, we will have a look at the OpenSSH client tool. Instead of testing on the host itself, it can connect to an SSH server via the network.

<hr>

[Visit Link](https://linux-audit.com/audit-and-harden-your-ssh-configuration/){:target="_blank" rel="noopener"}


