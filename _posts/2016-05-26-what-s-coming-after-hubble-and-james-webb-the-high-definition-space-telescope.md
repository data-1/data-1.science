---
layout: post
title: "What's coming after Hubble and James Webb? The High-Definition Space Telescope"
date: 2016-05-26
categories:
author: Ramin Skibba
tags: [Hubble Space Telescope,Large Ultraviolet Optical Infrared Surveyor,James Webb Space Telescope,Exoplanet,Telescope,Science,Physical sciences,Outer space,Space science,Astronomy]
---


Credit: D. Ceverino, C. Moody, G. Snyder, and Z. Levay (STScI)  As the HDST's name suggests, its 12-meter wide segmented mirror would give it much higher resolution than any current or upcoming telescopes, allowing astronomers to focus on many Earth-like exoplanets orbiting stars outside our solar system up to 100 light-years away, resolve stars even in the Andromeda Galaxy, and image faraway galaxies dating back 10 billion years of cosmic time into our universe's past. The Hubble telescope required 20 years of planning, technological development, and budget allocations before it was launched in 1990. Planning for NASA's James Webb Space Telescope (JWST), which was also first proposed by AURA, began not long afterward. They also expect to leverage research on JWST's sunshield, which will be necessary to keep the proposed telescope at an extremely stable temperature, and on its detectors, when developing optimized gigapixel-class cameras. If the astronomical community comes on board and prioritizes this project for the next decade, then it likely would be designed and constructed in the 2020s and then launched in the 2030s.

<hr>

[Visit Link](http://phys.org/news/2015-08-hubble-james-webb-high-definition-space.html){:target="_blank" rel="noopener"}


