---
layout: post
title: "A step closer to bio-printing transplantable tissues and organs: Study"
date: 2014-06-30
categories:
author: University of Sydney 
tags: [3D bioprinting,Blood vessel,Circulatory system,Organ (biology),Clinical medicine,Medical specialties,Medicine,Biology,Anatomy,Life sciences]
---


The research challenge – networking cells with a blood supply. One of the greatest challenges to the engineering of large tissues and organs is growing a network of blood vessels and capillaries, says Dr Bertassoni. Cells die without an adequate blood supply because blood supplies oxygen that's necessary for cells to grow and perform a range of functions in the body. But this is what researchers have now achieved. What the researchers achieved  Using a high-tech 'bio-printer', the researchers fabricated a multitude of interconnected tiny fibres to serve as the mold for the artificial blood vessels.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/uos-asc062914.php){:target="_blank" rel="noopener"}


