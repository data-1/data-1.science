---
layout: post
title: "Studying the solar system with NASA's Webb Telescope"
date: 2016-03-15
categories:
author: NASA/Goddard Space Flight Center
tags: [James Webb Space Telescope,Planet,Solar System,Sun,Asteroid,Goddard Space Flight Center,Planetary science,Exoplanet,Comet,Telescope,Astronomy,Near-Earth object,Lagrange point,Star,Mars,Bodies of the Solar System,Outer space,Astronomical objects,Physical sciences,Planets,Science,Sky,Local Interstellar Cloud,Space science]
---


But the observatory also will investigate objects in Earth's own neighborhood - planets, moons, comets and asteroids in our solar system. These studies will help scientists understand more about the formation of the solar system and how Earth became capable of supporting life. The James Webb Space Telescope will be an innovative tool for studying objects in the solar system and can help take planetary science to a new level, said Stefanie Milam, the Webb telescope's deputy project scientist for planetary science at NASA's Goddard Space Flight Center in Greenbelt, Maryland. Scheduled for launch in 2018, the Webb telescope will carry four science instruments to take images of and collect information about the physical characteristics and compositions of astronomical objects. The Webb telescope will make it possible to observe many objects that are too small, too distant or too faint for ground-based instruments, said John Stansberry at the Space Telescope Science Institute in Baltimore.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/nsfc-sts020816.php){:target="_blank" rel="noopener"}


