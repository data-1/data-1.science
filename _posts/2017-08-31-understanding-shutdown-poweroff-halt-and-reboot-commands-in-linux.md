---
layout: post
title: "Understanding Shutdown, Poweroff, Halt and Reboot Commands in Linux"
date: 2017-08-31
categories:
author: "Aaron Kili"
tags: [Booting,Linux,Online and offline,Shutdown (computing),Server (computing),Information technology,Operating system technology,Software engineering,Computer science,Computing,Information technology management,Technology,Computer architecture,Information Age,System software,Computers,Computer engineering,Software,Digital media,Software development]
---


In this article, we will explain to you the difference between shutdown, poweroff, halt and reboot Linux commands. Examples of shutdown commands:  # shutdown # shutdown now # shutdown 13:20 # shutdown -p now #poweroff the machine # shutdown -H now #halt the machine # shutdown -r09:35 #reboot the machine at 09:35am  To cancel a pending shutdown, simply type the command below:  # shutdown -c  Halt Command  halt instructs the hardware to stop all CPU functions, but leaves it powered on. Below are examples of halt commands:  # halt #halt the machine # halt -p #poweroff the machine # halt --reboot #reboot the machine  Power off Command  poweroff sends an ACPI signal which instructs the system to power down. The following are examples of poweroff commands:  # poweroff #poweroff the machine # poweroff --halt #halt the machine # poweroff --reboot #reboot the machine  Reboot Command  reboot instructs the system to restart. # reboot #reboot the machine # reboot --halt #halt the machine # reboot -p #poweroff the machine  That’s all!

<hr>

[Visit Link](https://www.tecmint.com/shutdown-poweroff-halt-and-reboot-commands-in-linux/){:target="_blank" rel="noopener"}


