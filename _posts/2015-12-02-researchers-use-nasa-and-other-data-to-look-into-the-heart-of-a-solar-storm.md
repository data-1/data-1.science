---
layout: post
title: "Researchers use NASA and other data to look into the heart of a solar storm"
date: 2015-12-02
categories:
author: "$author" 
tags: [Coronal mass ejection,Space weather,Sun,Solar and Heliospheric Observatory,Magnetosphere,Cluster II (spacecraft),Space plasmas,Sky,Bodies of the Solar System,Spaceflight,Physical sciences,Astronomical objects,Astrophysics,Science,Plasma physics,Planetary science,Nature,Solar System,Outer space,Astronomy,Space science]
---


A space weather storm from the sun engulfed our planet on Jan. 21, 2005. With observations collected from ground-based networks and 20 different satellites, Kozyra and a group of colleagues, each an expert in different aspects of the data or models, found that the CME contained a rare piece of dense solar filament material. Solar filaments are ribbons of dense plasma supported in the sun's outer atmosphere – the corona -- by strong magnetic fields. What happened next was observed by a flotilla of Earth-orbiting scientific satellites, including NASA's IMAGE, FAST and TIMED missions, the joint European Space Agency, or ESA, and NASA's Cluster, the NASA and ESA's Geotail, the Chinese and ESA's Double Star-1; other spacecraft 1 million miles closer to the sun including SOHO and NASA's Advanced Composition Explorer, Wind various other spacecraft; as well as the National Science Foundation-supported ground-based SuperDARN radar network. The magnetic fields embedded in this CME generally pointed toward Earth's north pole, just as Earth's own magnetic fields do.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/nsfc-run082814.php){:target="_blank" rel="noopener"}


