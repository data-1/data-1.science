---
layout: post
title: "Scientists develop new theory of molecular evolution"
date: 2017-10-24
categories:
author: University of Colorado Anschutz Medical Campus
tags: [Evolution,Molecular evolution,Protein,Genetics,Mutation,Molecular biology,Biochemistry,Life sciences,Biotechnology,Nature,Biological evolution,Biology,Evolutionary biology]
---


The approach rests on understanding proteins as integrated systems, said Goldstein. It turns out this is really important for understanding why these molecules evolve the way they do. These patterns of molecular convergence were found to change regularly over evolutionary time in ways that indicated continually fluctuating constraints in different parts of proteins. But once the system was placed into a statistical mechanics framework, the magnitude of amino acid entrenchment was seen as central to understanding rates of evolutionary divergence. The researchers said that the strength of selection in protein evolution is balanced by the sequence entropy of folding, the number of sequences that provide a protein with a given degree of stability.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uoca-sdn101917.php){:target="_blank" rel="noopener"}


