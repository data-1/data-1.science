---
layout: post
title: "Brain-machine interface triggers recovery for paraplegic patients"
date: 2016-08-22
categories:
author: "Colorado State University"
tags: [Braincomputer interface,Spinal cord injury,Powered exoskeleton,Paraplegia,Virtual reality,Paralysis,Nervous system,Clinical medicine,Neuroscience]
---


This unprecedented scientific demonstration was the work of the Walk Again Project (WAP), a nonprofit, international research consortium that includes Alan Rudolph, vice president for research at Colorado State University, who is also an adjunct faculty member at Duke University's Center for Neuroengineering. This is the first study to report that long-term brain-machine interface use could lead to significant recovery of neurological function in patients suffering from severe spinal cord injuries. Until now, no clinical study employing brain-machine interfaces in patients suffering severe spinal cord injuries reported any neurological improvements. For the Scientific Reports study, the researchers trained eight paraplegic patients for a year on what they call the Walk Again Neurorehabilitation protocol. In this component, the patients used the same EEG cap to trigger the Lokomat movements while receiving tactile feedback.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/csu-bit081016.php){:target="_blank" rel="noopener"}


