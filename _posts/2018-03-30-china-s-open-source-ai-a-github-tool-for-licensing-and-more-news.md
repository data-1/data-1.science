---
layout: post
title: "China's open source AI, a GitHub tool for licensing, and more news"
date: 2018-03-30
categories:
author: "Scott Nesbitt
(Alumni)"
tags: [Java (programming language),Artificial intelligence,Jakarta EE,Open source,Eclipse (software),Communication,Technology,Computing,Information technology,Information Age,Software]
---


In this edition of our open source news roundup, we take a look China's open source AI technology, GitHub making open source licensing easier, the Eclipse Foundation taking over Java EE, and more. GitHub makes open source licensing easier  Do you find open source licensing to be confusing? GitHub is making choosing the right license easier by open sourcing a tool called Licensed. That's in spite of many influential people in the Java world, including Java's creator James Gosling, asking Oracle to let the Foundation use the name Java EE naming and packaging. Now, Ghostery is open source.

<hr>

[Visit Link](https://opensource.com/article/18/3/news-march-17){:target="_blank" rel="noopener"}


