---
layout: post
title: "NYSCF researchers develop novel bioengineering technique for personalized bone grafts"
date: 2018-07-18
categories:
author: "New York Stem Cell Foundation"
tags: [Tissue engineering,Regenerative medicine,Stem cell,Bone,Clinical medicine,Medicine,Medical specialties,Health sciences,Biotechnology,Life sciences,Biology,Bone grafting]
---


Bone grafts generated from patient stem cells overcome such limitations, but it is difficult to bioengineer these grafts in the exact size and shape needed to treat large defects. We wanted to see if we could instead engineer smaller segments of bone individually and then combine them to create a graft that overcomes the current limitations in the size and shape of a bone that can be grown in the lab. They first scanned the femur to assess the size and shape of the defect and generated a model of the graft. ###  About The New York Stem Cell Foundation Research Institute  The New York Stem Cell Foundation (NYSCF) Research Institute is an independent organization accelerating cures and better treatments for patients through stem cell research. The NYSCF Research Institute is an acknowledged world leader in stem cell research and in developing pioneering stem cell technologies, including the NYSCF Global Stem Cell ArrayTM and in manufacturing stem cells for scientists around the globe.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/nysc-nrd071718.php){:target="_blank" rel="noopener"}


