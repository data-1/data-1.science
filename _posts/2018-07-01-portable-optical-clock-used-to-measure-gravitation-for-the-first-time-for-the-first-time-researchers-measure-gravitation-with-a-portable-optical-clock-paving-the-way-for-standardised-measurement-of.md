---
layout: post
title: "Portable optical clock used to measure gravitation for the first time: For the first time, researchers measure gravitation with a portable optical clock, paving the way for standardised measurement of"
date: 2018-07-01
categories:
author: "Physikalisch-Technische Bundesanstalt (PTB)"
tags: [Atomic clock,News aggregator,Science,Metrology,Applied and interdisciplinary physics,Technology]
---


Until now, such delicate clocks have been restricted to laboratories at a few major research institutions, however, researchers at PTB have developed a transportable strontium optical lattice clock, opening up the possibility of performing measurements in the field. There, the team measured the gravity potential difference between the exact location of the clock inside the mountain and a second clock at INRIM -- located 90 km away in Torino, Italy, at a height difference of about 1,000 m.  The accurate comparison of the two clocks was made possible using a 150 km long optical fibre link, set up by INRIM, and a frequency comb from NPL, to connect the clock to the link. It will also lead to more consistent national height systems. One day such technology could help to monitor sea level changes resulting from climate change. Optical clocks could help to establish a unified world height reference system with significant impact on geodynamic and climate research.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/02/180212111735.htm){:target="_blank" rel="noopener"}


