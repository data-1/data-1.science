---
layout: post
title: "New in­form­a­tion on brain areas linked to tact­ile sense and meta­cog­nit­ive abil­ity"
date: 2018-08-22
categories:
author: "University of Helsinki"
tags: [Somatosensory system,Prefrontal cortex,Metacognition,Working memory,Sense,Neuropsychology,Concepts in metaphysics,Neuroscience,Cognitive science,Cognition,Mental processes,Psychological concepts,Behavioural sciences,Brain,Interdisciplinary subfields,Cognitive psychology,Psychology]
---


In addition to temporal and spatial discrimination of tactile stimuli, he studied the metacognitive abilities of his test subjects. The prefrontal cortex is involved in many complex cognitive abilities such as working memory, attention and introspection. The results of the second study showed that the transcranial magnetic stimulation of the middle frontal gyrus reduced the tactile discrimination ability when compared to the stimulation of the superior frontal gyrus, or sham stimulation. Gogulski also examined the metacognitive accuracy of tactile working memory tasks with transcranial magnetic stimulation of the prefrontal areas. Understanding the brain function of healthy test subjects could help in the development of new treatment methods for neuropsychiatric illnesses in the future, Gogulski says.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180820104215.htm){:target="_blank" rel="noopener"}


