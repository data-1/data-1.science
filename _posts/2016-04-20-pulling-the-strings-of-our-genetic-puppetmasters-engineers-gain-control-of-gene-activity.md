---
layout: post
title: "Pulling the strings of our genetic puppetmasters: Engineers gain control of gene activity"
date: 2016-04-20
categories:
author: Duke University
tags: [Gene,Genetics,Life sciences,Biological engineering,Molecular genetics,Branches of genetics,Biochemistry,Molecular biology,Biotechnology,Biology]
---


The researchers say having the ability to steer the epigenome will help them explore the roles that particular promoters and enhancers play in cell fate or the risk for genetic disease and it could provide a new avenue for gene therapies and guiding stem cell differentiation. The epigenome is everything associated with the genome other than the actual genetic sequence, and is just as important as our DNA in determining cell function in healthy and diseased conditions, said Charles Gersbach, assistant professor of biomedical engineering at Duke. The epigenome determines which genes each cell activates and to what degree. Next to every gene is a DNA sequence called a promoter that controls its activity, explained Gersbach. It's like we use CRISPR to find a genetic address so that we can alter the DNA's packaging at that specific site, said Reddy.

<hr>

[Visit Link](http://phys.org/news347535983.html){:target="_blank" rel="noopener"}


