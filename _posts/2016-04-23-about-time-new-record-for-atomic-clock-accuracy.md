---
layout: post
title: "About time: New record for atomic clock accuracy"
date: 2016-04-23
categories:
author: National Institute Of Standards
tags: [Atomic clock,Clock,Applied and interdisciplinary physics,Physical sciences,Physics,Metrology,Science]
---


JILA's strontium lattice atomic clock now performs betterthan ever because scientists literally take thetemperature of the atoms' environment. As described in Nature Communications, the experimental strontium lattice clock at JILA, a joint institute of NIST and the University of Colorado Boulder, is now more than three times as precise as it was last year, when it set the previous world record. Relativistic geodesy is the idea of using a network of clocks as gravity sensors to make 3D precision measurements of the shape of the Earth. The JILA group made the latest improvements with the help of researchers at NIST's Maryland headquarters and the Joint Quantum Institute (JQI). Explore further JILA strontium atomic clock sets new records in both precision and stability

<hr>

[Visit Link](http://phys.org/news348834022.html){:target="_blank" rel="noopener"}


