---
layout: post
title: "Scientists develop proteins that self-assemble into supramolecular complexes"
date: 2018-07-27
categories:
author: "Shinshu University"
tags: [Protein,Biology,Organism,Self-assembly,Biomolecule,Synthetic biology,News aggregator,Supramolecular chemistry,Life sciences,Biotechnology,Chemistry,Physical sciences,Biochemistry,Nature,Technology,Molecular biology]
---


The researchers created and developed the proteins with a specific function and their method reveals a possibility that certain protein functions can be created on demand. Arai and his team developed a simple and stable artificial protein, called WA20, in 2012. The researchers built on that success to develop extender PN-Blocks, which link WA20 proteins together to produce chain-like protein complexes and even more nanostructures. Getting these complexes together, the supramolecular nanostructure complexes were achieved by introducing a metal ion, which triggered the process through further self-assembly. These results demonstrate that the PN-Block strategy is a useful and systematic strategy for constructing novel nano-architectures, Arai said, noting that the ability to construct novel complexes is particularly important in biotechnology and synthetic biology.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180719094408.htm){:target="_blank" rel="noopener"}


