---
layout: post
title: "ESA astronaut Paolo Nespoli starts third mission on Space Station"
date: 2017-09-23
categories:
author: ""
tags: [International Space Station,Paolo Nespoli,NASA,Astronaut,Spacecraft,Spaceflight technology,Spaceflight,Space-based economy,Space industry,Crewed spacecraft,Space science,Astronautics,Space program of the United States,Aerospace,Space vehicles,Outer space,Human spaceflight programs,Space exploration,Life in space,Flight,Space programs,Human spaceflight,Space research]
---


Science & Exploration ESA astronaut Paolo Nespoli starts third mission on Space Station 29/07/2017 4514 views 62 likes  ESA astronaut Paolo Nespoli, NASA astronaut Randy Bresnik and Roscosmos commander Sergei Ryazansky were launched into space yesterday from the Baikonur cosmodrome in Kazakhstan at 15:41 GMT (17:41 CEST). Their Soyuz MS-05 spacecraft circled Earth four times to catch up with the International Space Station six hours later and the crew are now settling into their new home and place of work for five months. This is Paolo’s third spaceflight and third visit to the Space Station – he has already clocked 174 days in space with previous missions, one flight on a Space Shuttle in 2007 and a five-month mission in 2011. Liftoff of Soyuz MS-05 After docking to the Rassvet module, the hatch between the Soyuz spacecraft and the International Space Station was opened at 23:57 GMT (01:57 CEST) marking the beginning of the Vita mission. These changes need to be understood so that we can explore our Solar System and send humans farther into space, but they also offer interesting opportunities for researchers looking at the causes of similar ailments on Earth, such as osteoporosis.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/ESA_astronaut_Paolo_Nespoli_starts_third_mission_on_Space_Station){:target="_blank" rel="noopener"}


