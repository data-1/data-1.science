---
layout: post
title: "Parsing photons in the infrared, UCI-led astronomers uncover signs of earliest galaxies"
date: 2015-09-10
categories:
author: University of California - Irvine 
tags: [Galaxy,Hubble Space Telescope,University of California Irvine,Universe,Astronomy,Infrared,Telescope,Reionization,Big Bang,Star,Space science,Physical sciences,Science]
---


Irvine, Calif., Sept. 7, 2015 - Astronomers from the University of California, Irvine and Baltimore's Space Telescope Science Institute have generated the most accurate statistical description yet of faint, early galaxies as they existed in the universe 500 million years after the Big Bang. We sort of overlap with CIBER and then go into short optical wavelengths, and we see in addition to intrahalo light a new component - stars and galaxies that formed first in the universe. We do not see that signal in the optical [wavelengths], only in infrared. This is confirmation that the signal is from early times in the universe. Could there be X-ray emissions associated with this primordial stuff?

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/uoc--ppi090815.php){:target="_blank" rel="noopener"}


