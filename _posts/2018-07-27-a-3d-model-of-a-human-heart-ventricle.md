---
layout: post
title: "A 3D model of a human heart ventricle"
date: 2018-07-27
categories:
author: "Harvard John A. Paulson School of Engineering and Applied Sciences"
tags: [Tissue engineering,Heart,Cardiac muscle,Ventricle (heart),Cardiovascular system,Medicine,Clinical medicine]
---


Harvard University researchers have bioengineered a three-dimensional model of a human left heart ventricle that could be used to study diseases, test drugs and develop patient-specific treatments for heart conditions such as arrhythmia. The tissue is engineered with a nanofiber scaffold seeded with human heart cells. The scaffold acts like a 3D template, guiding the cells and their assembly into ventricle chambers that beat in vitro. Because the collector is spinning, all of the fibers align in the same direction. The fact that we can study this ventricle over long periods of time is really good news for studying the progression of diseases in patients as well as drug therapies that take a while to act, said MacQueen.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/hjap-a3m072318.php){:target="_blank" rel="noopener"}


