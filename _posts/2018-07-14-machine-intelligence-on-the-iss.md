---
layout: post
title: "Machine intelligence on the ISS"
date: 2018-07-14
categories:
author: "European Space Agency"
tags: [Artificial intelligence,European Space Agency,Technology,Computing,Cyberspace,Information technology]
---


Justin and Paolo. Credit: DLR , CC BY-SA 3.0 IGO  Artificial intelligence already helping astronauts on the International Space Station is also providing a promising approach for solving crimes. In an era of security concerns across Europe, the smart use of police data is critical for uncovering leads. The Space Applications Services company developed question-answering software for training astronauts on ESA's Columbus research laboratory, responding to queries such as What is this? The next ESA astronaut in space, Alexander Gerst, will take it to the next level this year, testing an intelligent mobile crew assistant.

<hr>

[Visit Link](https://phys.org/news/2018-01-machine-intelligence-iss.html){:target="_blank" rel="noopener"}


