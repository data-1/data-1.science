---
layout: post
title: "Pharmaceutical residues increasingly disrupt aquatic life: A hidden global change"
date: 2016-02-05
categories:
author: Netherlands Institute of Ecology (NIOO-KNAW)
tags: [Water,Sewage treatment,Aquatic plant,Environmental science,Natural environment,Earth sciences,Nature,Environmental technology,Systems ecology,Environmental issues,Ecology,Environmental engineering,Environment]
---


Residues of medicines in water can kill aquatic animals and play havoc with their food web and reproductive cycle. An international team of researchers led by the Netherlands Institute of Ecology (NIOO-KNAW) makes an urgent case for better wastewater treatment and biodegradable pharmaceuticals. These are some of the disruptive effects of pharmaceutical residues on the aquatic environment. Chemical substances from pharmaceuticals wreak havoc on underwater chemical communication, says the head of the NIOO's department of Aquatic Ecology, Ellen van Donk. Most of that chatter takes the form of infochemicals: chemical substances released by aquatic plants and animals that travel through the water.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/nioe-pri020216.php){:target="_blank" rel="noopener"}


