---
layout: post
title: "Earth’s magnetic heartbeat"
date: 2016-05-12
categories:
author:  
tags: [Earths magnetic field,Magnetosphere,Swarm (spacecraft),Earth,Planetary core,Bodies of the Solar System,Geophysics,Outer space,Physics,Space science,Astronomy,Physical sciences,Electromagnetism,Nature,Planetary science,Applied and interdisciplinary physics,Planets of the Solar System]
---


Applications Earth’s magnetic heartbeat 10/05/2016 48370 views 306 likes  With more than two years of measurements by ESA’s Swarm satellite trio, changes in the strength of Earth's magnetic field are being mapped in detail. The force that protects our planet Presented at this week’s Living Planet Symposium, new results from the constellation of Swarm satellites show where our protective field is weakening and strengthening, and importantly how fast these changes are taking place. As well as recent data from the Swarm constellation, information from the CHAMP and Ørsted satellites were also used to create the map. Swarm shows rate of change  For example, changes in the field have slowed near South Africa, but have changed faster over Asia. “Unexpectedly, we are finding rapid localised field changes that seem to be a result of accelerations of liquid metal flowing within the core.”  Swarm constellation Rune Floberghagen, ESA’s Swarm mission manager, added, “Two and a half years after the mission was launched it is great to see that Swarm is mapping the magnetic field and its variations with phenomenal precision.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Swarm/Earth_s_magnetic_heartbeat){:target="_blank" rel="noopener"}


