---
layout: post
title: "To stop cancer: Block its messages"
date: 2015-12-09
categories:
author: Weizmann Institute of Science
tags: [MAPKERK pathway,Cell (biology),Cell nucleus,Cell membrane,Cancer,Biochemical cascade,Cell biology,Biochemistry]
---


This whole membrane-to-nucleus communications system is known as a cellular signaling pathway, and there are about 15 different pathways for transferring the cell's main internal messaging. Seger has identified a number of the proteins involved in these pathways, especially in one particular pathway, called the MAPK/ERK cascade, which is involved in cancer. Seger realized that an effective nuclear spam filter on the ERK pathway would involve blocking just this step, thus preventing specific ERKs' messages from getting into the nucleus. The drugs currently used for melanoma, says Seger, usually work for a while and then the cancer becomes resistant to them. The method of designing small molecules that can get inside cells and stop certain messages before they become spam might be useful in treating other diseases, in addition to cancer.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/wios-tsc033015.php){:target="_blank" rel="noopener"}


