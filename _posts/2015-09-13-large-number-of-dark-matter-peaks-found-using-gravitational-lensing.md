---
layout: post
title: "Large number of dark matter peaks found using gravitational lensing"
date: 2015-09-13
categories:
author: Canada France Hawaii Telescope
tags: [Weak gravitational lensing,Dark matter,Physical cosmology,Universe,Gravitational lens,Sloan Digital Sky Survey,Dark energy,Mass,Astronomy,Cosmology,Celestial mechanics,Nature,Science,Astrophysics,Space science,Physics,Physical sciences]
---


The size of this map is about 4 square degrees corresponding to only 2.5% of the full CS82 survey footprint shown in the next figure. Indeed, following Einstein's theory of gravitation, we know that a mass concentration will deform locally the space-time and the observed shapes of distant galaxies seen through the such concentration will be deflected and distorted. Importantly, the number of mass peaks as a function of the mass peak significance encodes important information on the cosmological world model. To detect the weak lensing mass peaks, the research team used the Canada-France-Hawaii Telescope Stripe 82 Survey (CS82 in short), still one of the largest weak lensing survey yet. This result confirms that the dark matter distribution from weak lensing measurement can be used as a cosmological probe.

<hr>

[Visit Link](http://phys.org/news324660994.html){:target="_blank" rel="noopener"}


