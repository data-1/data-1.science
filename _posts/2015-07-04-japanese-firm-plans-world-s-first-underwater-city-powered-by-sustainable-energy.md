---
layout: post
title: "Japanese Firm Plans World’s First Underwater City Powered by Sustainable Energy"
date: 2015-07-04
categories:
author: Lucy Wang
tags: []
---


The spiral pathway is connected to a research facility, which would excavate the seabed for precious metals and create energy for the city above using temperature differences in ocean water. “This is just a blueprint by our company, but we are aiming to develop the technology that would enable us to build an underwater living space,” Shimizu spokesman Masataka Noguchi told the Wall Street Journal. The proposal also mentions harnessing the power of microorganisms to convert carbon dioxide to methane energy. Related: Crazy ‘Luna Ring’ Moon Solar Plant Could Beam Constant Green Energy to Earth  Although seemingly farfetched, Shimizu’s proposal underscores fears of sea level rise and loss of habitable environments. SIGN UP I agree to receive emails from the site.

<hr>

[Visit Link](http://inhabitat.com/japanese-firm-plans-worlds-first-underwater-city-powered-by-sustainable-energy/){:target="_blank" rel="noopener"}


