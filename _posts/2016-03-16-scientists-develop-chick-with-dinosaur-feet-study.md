---
layout: post
title: "Scientists develop chick with 'dinosaur' feet: study"
date: 2016-03-16
categories:
author:  
tags: [Dinosaur,Evolution,Theropoda,Biology]
---


Scientists in Chile have created a chicken embryo that developed dinosaur-like feet after genetic manipulation, highlighting the evolutionary link between theropod dinosaurs and birds  Scientists in Chile have created a chicken embryo that developed dinosaur-like feet after genetic manipulation, highlighting the evolutionary link between theropod dinosaurs and birds. The research—published last week in the journal Evolution—shows that by inhibiting early maturation of a leg of the chicken embryo, the leg reverts to the shape that dinosaurs' legs had, said Alexander Vargas, one of the six researchers at the University of Chile. The result is a chicken embryo with dinosaur legs, Vargas told AFP on Tuesday, explaining what amounts to reverse evolution. Theropods, a group of dinosaurs, started as carnivores but evolved to eat plants and insects. Birds evolved from small theropods in the Jurassic period more than 145 million years ago.

<hr>

[Visit Link](http://phys.org/news/2016-03-scientists-chick-dinosaur-feet.html){:target="_blank" rel="noopener"}


