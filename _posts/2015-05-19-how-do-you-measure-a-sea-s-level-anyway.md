---
layout: post
title: "How do you measure a sea's level, anyway?"
date: 2015-05-19
categories:
author: Gary Griggs, The Conversation
tags: [Sea level,Sea level rise,Ocean,Tide,Sea,Earth,Physical oceanography,Geophysics,Earth sciences,Physical geography,Applied and interdisciplinary physics,Oceanography,Hydrography,Hydrology,Geography]
---


Sea level change between 1993 and 2008. Credit: NASA/JPL  There are about 330 million cubic miles of water in the world oceans today, 97% of all the water on the planet. The level of the ocean around the Earth, and therefore the location of the shoreline, are directly related to the total amount of water in the oceans, and also closely tied to climate. Each of these official tide gauges keeps track of sea level at a particular coastal location. Each tide gauge keeps track of how sea level is changing relative to the land on which it is anchored. In parts of Alaska, the land is rising faster than sea level, so the tide gauge actually records a drop in sea level relative to the land.

<hr>

[Visit Link](http://phys.org/news351245167.html){:target="_blank" rel="noopener"}


