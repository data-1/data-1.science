---
layout: post
title: "What Microsoft's GitHub Deal Promises to Programmers"
date: 2018-06-28
categories:
author: ""
tags: []
---


“GitHub has never been open source. Not all code hosted on GitHub is open source at all,” Maffulli said. Rather, the company sees the GitHub marketplace as a place for it to promote its own software and services to developers. As cloud developers shift their focus toward taking advantage of Internet of Things data in real time at the “intelligent edge,” they need an on-ramp to the edge — a platform that allows the embedded systems of the world operate like the cloud, he told LinuxInsider. The developer workflows that Microsoft wants to see drive and influence business processes could spell the end of embedded computing as we know it, he cautioned, if we want to enable those 28 million developers to thrive at the edge.

<hr>

[Visit Link](http://www.linuxinsider.com/story/85382.html?rss=1){:target="_blank" rel="noopener"}


