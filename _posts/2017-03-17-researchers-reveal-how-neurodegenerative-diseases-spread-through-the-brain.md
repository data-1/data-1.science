---
layout: post
title: "Researchers reveal how neurodegenerative diseases spread through the brain"
date: 2017-03-17
categories:
author: "VIB (the Flanders Institute for Biotechnology)"
tags: [Neurodegenerative disease,Brain,Alzheimers disease,Patrik Verstreken,Medicine,Biochemistry,Biology,Medical specialties,Biotechnology,Life sciences,Cell biology,Neuroscience]
---


This allows neurodegenerative diseases such as Alzheimer's to spread through the brain. This the main conclusion of new research led by professor Patrik Verstreken (VIB-KU Leuven), in collaboration with Janssen Research & Development (Johnson & Johnson). During neurodegenerative disease, including Alzheimer's, toxic proteins are known to spread throughout the brain. They show that the toxic proteins cross from one brain cell to the next by being engulfed by 'vesicles', small bubbles in the receiving brain cell. BIN1 'improves' the transmission at synapses but in doing so, it enables the spread of toxic proteins.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/vfi-rrh110916.php){:target="_blank" rel="noopener"}


