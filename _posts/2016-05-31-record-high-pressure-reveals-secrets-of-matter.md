---
layout: post
title: "Record-high pressure reveals secrets of matter"
date: 2016-05-31
categories:
author: "Linköping University"
tags: [Electron,Pressure,Metal,Atom,Valence electron,Electrical resistivity and conductivity,Applied and interdisciplinary physics,Physical sciences,Chemistry,Materials,Nature,Physics,Physical chemistry,Phases of matter,Condensed matter physics,Materials science]
---


As pressure increases, the distance between the atoms decreases, and the outer electrons, the highly mobile valence electrons, interact with each other. Osmium is the metal with the highest density and is almost as incompressible as diamond. The high pressure didn't result in any significant change to the valence electrons, which surprised us. A. Abrikosov. The researchers in Bayreuth, with the help of nanotechnology, have developed a small synthetic diamond that is positioned halfway between two ordinary diamonds, on each side of the Osmium crystal.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/lu-rpr082515.php){:target="_blank" rel="noopener"}


