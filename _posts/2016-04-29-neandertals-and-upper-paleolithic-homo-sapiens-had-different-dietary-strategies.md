---
layout: post
title: "Neandertals and Upper Paleolithic Homo sapiens had different dietary strategies"
date: 2016-04-29
categories:
author: PLOS
tags: [Neanderthal,Paleolithic,Human,Homo,Upper Paleolithic,Pleistocene,Pleistocene life]
---


When fluctuating climates in the Ice Age altered habitats, modern humans may have adapted their diets in a different way than Neandertals, according to a study published April 27, 2016 in the open-access journal PLOS ONE by Sireen El Zaatari of the University of Tübingen, Germany, and colleagues. To be able to do this, they may have developed tools to extract dietary resources from their environment says Sireen El Zaatari. The researchers concluded that Upper Paleolithic modern humans' differing dietary strategies may have given them an advantage over the Neandertals. The Neandertals may have maintained their opportunistic approach of eating whatever was available in their changing habitats over hundreds of thousands of years. However, modern humans seem to have invested more effort in accessing food resources and significantly changed their dietary strategies over a much shorter period of time, in conjunction with their development of tools, which may have given them an advantage over Neanderthals.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/p-nau042216.php){:target="_blank" rel="noopener"}


