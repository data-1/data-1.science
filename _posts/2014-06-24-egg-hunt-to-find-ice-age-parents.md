---
layout: post
title: "Egg hunt to find Ice Age parents"
date: 2014-06-24
categories:
author: Flinders University
tags: [Genyornis,Emu,Bird,Egg]
---


International fossil expert Dr Gerald Grellet is working with Flinders Palaeontology Department to find the owner of a fossil egg from the Australian Ice Age. French born, US-based fossil expert Dr Gerald Grellet has flown in from the States to work with Dr Trevor Worthy and Associate Professor Gavin Prideaux in Flinders University's Palaeontology Department on a two-month mission to identify the true owner of a fossilised egg and remnant shells housed at the South Australian Museum. Dr Grellet, who works with research institutions globally on fossil and oological discoveries, says the Flinders team has serious doubts about the authenticity of the claim given the size of the so-called Genyornis egg is comparatively smaller than eggs produced by other large birds. During Dr Grellet's time at Flinders, the team will undertake oological assessments to determine factors including; the geology of the location where the eggs were found, the shape, dimension and thickness of the shell as well as the size and volume of the egg compared to the size and weight of the bird. If the eggs materials are found to belong to another bird, Dr Grellet said the history of Australia's Ice Age would need to be rewritten: The Genyornis egg has been one of the iconic fossils used as a tool to explain the controversial extinction of megafauna in Australia, and what caused it.

<hr>

[Visit Link](http://phys.org/news322813473.html){:target="_blank" rel="noopener"}


