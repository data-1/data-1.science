---
layout: post
title: "Getting more out of nature: Genetic toolkit finds new maximum for crop yields"
date: 2016-03-29
categories:
author: Cold Spring Harbor Laboratory
tags: [Cold Spring Harbor Laboratory,Plant breeding,Tomato,Flower,Genetics,Florigen,Mutation]
---


CSHL scientists have identified a set of genetic variants that can dramatically increase tomato production. The combination of genetic mutations on the far right produces twice as many tomatoes as the standard variety. Plant breeders will be able to combine different gene variants among the set to create an optimal plant architecture for particular varieties and growing conditions. In general scientific terms, Lippman explains, Plant architecture results from a delicate balance between vegetative growth – shoots and leaves – and flower production. With multiple genetic variants, breeders can combine different mutations to provide a new optimum for their particular variety (as shown above) and growing conditions.

<hr>

[Visit Link](http://phys.org/news334148050.html){:target="_blank" rel="noopener"}


