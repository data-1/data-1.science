---
layout: post
title: "The Linux Foundation Launches LF ENERGY, New Open Source Coalition"
date: 2018-07-13
categories:
author: "The Linux Foundation"
tags: [Linux Foundation,Linux,European Network of Transmission System Operators for Electricity,Smart grid,Electrical grid,Application software,Grid computing,Computing,Technology]
---


“We are thrilled to launch LF Energy and honored to work with RTE, European Network of Transmission System Operators, Vanderbilt University and The Electric Power Research Institute to harness open source technologies and advance the energy transition. “This initiative will allow us to share our research results with the open source community and facilitate technology transition to industry.”  ENTSO-E, the European Network of Transmission System Operators, represents 43 electricity transmission system operators (TSOs) from 36 countries across Europe. “This effort will continue to foster that spirit of collaboration, bringing multi-national perspectives to the table to inform globally-impactful work toward our respective and complimentary missions.”  RTE contributed three projects to The Linux Foundation to form LF Energy, and Vanderbilt University will transition its Resilient Information Architecture Platform for Smart Grid (RIAPS) applications technology. More about new LF Energy projects:  OperatorFabric: is a smart assistant for system operators for use in electricity, water, and other utility operations. About The Linux Foundation  The Linux Foundation is the organization of choice for the world’s top developers and companies to build ecosystems that accelerate open technology development and industry adoption.

<hr>

[Visit Link](https://www.linuxfoundation.org/press-release/the-linux-foundation-launches-lf-energy-new-open-source-coalition/){:target="_blank" rel="noopener"}


