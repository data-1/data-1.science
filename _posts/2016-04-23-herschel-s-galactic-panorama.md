---
layout: post
title: "Herschel’s Galactic panorama"
date: 2016-04-23
categories:
author:  
tags: [Milky Way,Star,Galaxy,Star formation,Cosmic dust,Interstellar medium,Nebula,Outer space,Concepts in astronomy,Stellar astronomy,Astrophysics,Sky,Physical sciences,Astronomical objects,Space science,Astronomy]
---


Science & Exploration Herschel’s Galactic panorama 22/04/2016 16011 views 181 likes  This new video from ESA’s Herschel space observatory reveals in stunning detail the intricate pattern of gas, dust and star-forming hubs along the plane of our Galaxy, the Milky Way. Against the diffuse background of the interstellar material, a wealth of bright spots, wispy filaments and bubbling nebulas emerge, marking the spots where stars are being born in the Galaxy. Herschel’s view of the Eagle Nebula Our disc-shaped Galaxy has a diameter of about 100 000 light-years and the Solar System is embedded in it about half way between the centre and periphery. Denser portions of the interstellar medium, the mixture of gas and dust that pervades the Galaxy, are visible in orange and red, popping up against the background in this false-colour view. Herschel obtained these unprecedented views by peering at the Milky Way in far infrared light to detect the glow of cosmic dust – a minor but crucial component of the interstellar mixture from which stars are born.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Herschel/Herschel_s_Galactic_panorama){:target="_blank" rel="noopener"}


