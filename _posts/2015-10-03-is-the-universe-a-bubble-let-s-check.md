---
layout: post
title: "Is the universe a bubble? Let's check"
date: 2015-10-03
categories:
author: Perimeter Institute For Theoretical Physics
tags: [Multiverse,Inflation (cosmology),Universe,Eternal inflation,Nature,Astrophysics,Physical cosmology,Cosmology,Physics,Science,Philosophy,Scientific method]
---


Never mind the big bang; in the beginning was the vacuum. Specifically, Johnson has been considering the rare cases in which our bubble universe might collide with another bubble universe. By producing testable predictions, the multiverse model has crossed the line between appealing story and real science. In fact, Johnson says, the program has reached the point where it can rule out certain models of the multiverse: We're now able to say that some models predict something that we should be able to see, and since we don't in fact see it, we can rule those models out. More information: — Check out some of Johnson et al.'s papers about simulating bubble collisions using numerical relativity on arXiv:  - Determining the outcome of cosmic bubble collisions in full General Relativity  - Simulating the universe(s): from cosmic bubble collisions to cosmological observables with numerical relativity  - Simulating the universe(s) II: phenomenology of cosmic bubble collisions in full General Relativity  — Read Johnson and company's work pertaining to observational tests of eternal inflation on arXiv:  - Towards observable signatures of other bubble universes  - First Observational Tests of Eternal Inflation  - Hierarchical Bayesian Detection Algorithm for Early-Universe Relics in the Cosmic Microwave Background

<hr>

[Visit Link](http://phys.org/news324821301.html){:target="_blank" rel="noopener"}


