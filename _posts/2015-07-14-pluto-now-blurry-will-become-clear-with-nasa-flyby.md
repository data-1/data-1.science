---
layout: post
title: "Pluto, now blurry, will become clear with NASA flyby"
date: 2015-07-14
categories:
author: Kerry Sheridan
tags: [New Horizons,Pluto,Charon (moon),Flyby (spaceflight),Spaceflight,Astronomy,Space science,Solar System,Outer space,Bodies of the Solar System,Astronomical objects,Planemos,Planets,Science,Planetary science,Space exploration,Local Interstellar Cloud,Astronautics]
---


Artist's concept, obtained courtesy of NASA/Johns Hopkins University Applied Physics Laboratory/Southwest Research Institute (JHUAPL/SwRI), of the New Horizons spacecraft as it approaches Pluto  The best picture we have of Pluto is a blurry, pixelated blob, but that is about to change when a NASA spacecraft makes the first-ever flyby of the dwarf planet. The US space agency's unmanned New Horizons spacecraft is scheduled to pass by Pluto on July 14, and will send back unprecedented high-resolution images, allowing people to glimpse the surface of the distant celestial body in rich detail. Rocky on the inside and icy on the outside, Pluto has five moons and resides in the Kuiper Belt, a zone of the solar system that is a relic of the era of planetary formation more than 4.5 billion years ago, and contains comets and the building blocks of small planets. Three months from today, NASA's New Horizons spacecraft will make the first exploration of the Pluto system, the Kuiper Belt and the farthest shore of exploration ever reached by humankind, Stern told reporters Tuesday. Size of a piano  New Horizons, about the size of a baby grand piano, is the fastest moving spacecraft ever launched, and is traveling about a million miles (1.6 million kilometers) a day on its way to this unexplored frontier.

<hr>

[Visit Link](http://phys.org/news348282327.html){:target="_blank" rel="noopener"}


