---
layout: post
title: "Europe’s billion-euro quantum project takes shape"
date: 2017-09-19
categories:
author: "Gibney, Elizabeth Gibney, You Can Also Search For This Author In"
tags: []
---


Scientists offer more detail on flagship programme to harness quantum effects in devices. Members of an advisory group steering the Quantum Technology Flagship, as the project is called, gave details of how it will work at a meeting on 7 April at the Russian Centre of Science and Culture in London. And he hopes that it will encourage member states to invest nationally to make stronger bids for funding. The country holds the most patents in the field and is already trialling both a quantum-communication satellite and a 2,000-kilometre secure ground-based link. The United Kingdom is one of the few nations to involve relevant companies in the research, Calarco points out, through its £350-million (US$450-million) UK National Quantum Technologies Programme.

<hr>

[Visit Link](http://www.nature.com/news/europe-s-billion-euro-quantum-project-takes-shape-1.21925?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


