---
layout: post
title: "NASA's flying observatory SOFIA to explore magnetic universe and beyond"
date: 2018-06-21
categories:
author: "Nicholas A. Veronico"
tags: [Stratospheric Observatory for Infrared Astronomy,Star formation,Star,Titan (moon),Cosmic dust,Science,Sky,Planetary science,Solar System,Outer space,Astronomical objects,Astronomy,Space science,Physical sciences]
---


One such program will use the instrument to understand the impact magnetic fields have on stars forming inside a dark cloud, a stellar nursery filled with dust and molecules, called L1448. Researchers will continue to search for methane on Mars. Observations planned while operating from there include:  Researchers will create a large-scale map of the biggest star-forming region in the Large Magellanic Cloud, 30 Doradus, (also known as the Tarantula Nebula.) The HAWC+ instrument will be onboard SOFIA for its first observations from the Southern Hemisphere, to study magnetic fields in star-forming regions and around black holes in the Large and Small Magellanic Clouds. Researchers will utilize SOFIA's mobility to study the atmosphere of Saturn's moon Titan by studying its shadow as it passes in front of a star during an eclipse-like event called an occultation.

<hr>

[Visit Link](https://phys.org/news/2018-01-nasa-observatory-sofia-explore-magnetic.html){:target="_blank" rel="noopener"}


