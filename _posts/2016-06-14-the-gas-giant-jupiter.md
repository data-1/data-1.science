---
layout: post
title: "The gas giant Jupiter"
date: 2016-06-14
categories:
author: "Universe Today"
tags: [Jupiter,Moons of Jupiter,Planetary core,Io (moon),Magnetosphere of Jupiter,Ganymede (moon),Space science,Astronomy,Planetary science,Planets,Solar System,Astronomical objects,Physical sciences,Outer space,Bodies of the Solar System,Planets of the Solar System]
---


Moons:  The Jovian system currently includes 67 known moons. One storm, the Great red spot, has been raging since at least the late 1600s. In 1610, Galileo Galilei was the first astronomer to use a telescope to observe the planets. In the course of his examinations of the outer solar system, he discovered the four largest moons of Jupiter (now known as the Galilean Moons). On December 7th, 1995, the Galileo orbiter became the first probe to establish orbit around Jupiter, where it would remain for seven years.

<hr>

[Visit Link](http://phys.org/news/2015-08-gas-giant-jupiter.html){:target="_blank" rel="noopener"}


