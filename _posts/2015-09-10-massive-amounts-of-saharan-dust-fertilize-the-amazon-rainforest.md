---
layout: post
title: "Massive amounts of Saharan dust fertilize the Amazon rainforest"
date: 2015-09-10
categories:
author: University of Maryland 
tags: [Amazon rainforest,Saharan dust,Sahara,Earth sciences,Physical geography,Natural environment,Nature]
---


This phosphorus accounts for just 0.08% of the 27.7 million tons of Saharan dust that settles in the Amazon every year. Dust will affect climate and, at the same time, climate change will affect dust, said lead author Hongbin Yu, an associate research scientist at the Earth System Science Interdisciplinary Center (ESSIC), a joint center of the University of Maryland and NASA's Goddard Space Flight Center. The team focused on Saharan dust transported across the Atlantic Ocean to South America and beyond, to the Caribbean Sea, because it is the largest transport of dust on the planet. The team estimated the phosphorus content of Saharan dust by studying samples from the Bodélé Depression and from ground stations on Barbados and in Miami. They then used this estimate to calculate how much phosphorus gets deposited in the Amazon basin.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/uom-mao022315.php){:target="_blank" rel="noopener"}


