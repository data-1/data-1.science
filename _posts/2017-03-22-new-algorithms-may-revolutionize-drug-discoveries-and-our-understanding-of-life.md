---
layout: post
title: "New algorithms may revolutionize drug discoveries -- and our understanding of life"
date: 2017-03-22
categories:
author: "University of Toronto"
tags: [Protein,Research,Algorithm,Innovation,Image resolution,Cryogenic electron microscopy,University of Toronto,Chemistry,Branches of science,Science,Technology]
---


A new set of machine learning algorithms developed by U of T researchers that can generate 3D structures of tiny protein molecules may revolutionize the development of drug therapies for a range of diseases, from Alzheimer's to cancer. Designing successful drugs is like solving a puzzle, says U of T PhD student Ali Punjani, who helped develop the algorithms. This new method is revolutionizing the way scientists can discover 3D protein structures, allowing the study of many proteins that simply could not be studied in the past. Our approach solves some of the major problems in terms of speed and number of structures you can determine, says Professor David Fleet, chair of the Computer and Mathematical Sciences Department at U of T Scarborough and Punjani's PhD supervisor. What's novel about their approach is that it eliminates the need for prior knowledge about the protein molecule being studied.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/uot-nam020317.php){:target="_blank" rel="noopener"}


