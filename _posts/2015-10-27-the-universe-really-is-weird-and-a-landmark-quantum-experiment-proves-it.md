---
layout: post
title: "The universe really is weird, and a landmark quantum experiment proves it"
date: 2015-10-27
categories:
author: Howard Wiseman, The Conversation
tags: [Bells theorem,Quantum entanglement,Quantum mechanics,Physical sciences,Epistemology of science,Branches of science,Academic discipline interactions,Applied and interdisciplinary physics,Scientific method,Science,Theoretical physics,Scientific theories,Physics]
---


Local causality is a very natural scientific assumption and it holds in all modern scientific theories, except quantum mechanics. Thus, because local causality is such a natural hypothesis about the world, there have been decades of experiments looking for, and finding, the very particular predictions of quantum mechanics that John Bell discovered in 1964. To close the first loophole, it is necessary that the laboratories be far enough apart (well separated). This has been a problem with experiments using photons (quantum particles of light) because often a photon will not be detected at all. Alice and Bob then each choose a setting and measure their electrons while Juanita performs a joint measurement on the two photons.

<hr>

[Visit Link](http://phys.org/news/2015-10-universe-weird-landmark-quantum.html){:target="_blank" rel="noopener"}


