---
layout: post
title: "Improving computer graphics with quantum mechanics"
date: 2016-07-29
categories:
author: "Robert Perkins, California Institute Of Technology"
tags: [Vorticity,Viscosity,Mechanics,Quantum mechanics,Applied and interdisciplinary physics,Science,Physics]
---


Caltech applied scientists have developed a new way to simulate large-scale motion numerically using the mathematics that govern the universe at the quantum level. The new technique, presented at the International Conference and Exhibition on Computer Graphics & Interactive Techniques (SIGGRAPH), held in Anaheim, California, from July 24-28, allows computers to more accurately simulate vorticity, the spinning motion of a flowing fluid. The Schrödinger equation, the basic description of quantum mechanical behavior, can be used to describe the motion of superfluids, which are fluids supercooled to temperatures near absolute zero that behave as though they are without viscosity. The Schrödinger equation, the basic description of quantum mechanical behavior, can be used to describe the motion of superfluids—fluids, supercooled to temperatures near absolute zero, that behave as though they are without viscosity. When asked why the Schrödinger equation, usually reserved for effects at the atomic level, does so well for fluids at the macroscopic level, Schröder says, The Schrödinger equation, as we use it, is a close relative of the non-linear Schrödinger equation which is used for the description of superfluids.

<hr>

[Visit Link](http://phys.org/news/2016-07-graphics-quantum-mechanics.html){:target="_blank" rel="noopener"}


