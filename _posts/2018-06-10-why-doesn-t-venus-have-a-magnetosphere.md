---
layout: post
title: "Why doesn't Venus have a magnetosphere?"
date: 2018-06-10
categories:
author: "Matt Williams"
tags: [Planetary core,Earths outer core,Earth,Planet,Giant-impact hypothesis,Venus,Structure of Earth,Dynamo theory,Magnetosphere,Bodies of the Solar System,Planets of the Solar System,Terrestrial planets,Nature,Planets,Astronomy,Physical sciences,Space science,Planetary science]
---


According to the most widely-accepted models of planet formation, terrestrial planets are not formed in a single stage, but from a series of accretion events characterized by collisions with planetesimals and planetary embryos – most of which have cores of their own. Such a stratified core would be incapable of convection, which is believed to be what allows for Earth's magnetic field. The Earth’s layers, showing the Inner and Outer Core, the Mantle, and Crust. Light elements like O, Si, and S increasingly partition into core forming liquids during core formation when pressures and temperatures are higher, so later core forming events incorporate more of these elements into the core because the Earth is bigger and pressures and temperatures are therefore higher. If a late, violent and giant impact is necessary for a planetary magnetic field then such an impact may be necessary for life.

<hr>

[Visit Link](https://phys.org/news/2017-12-doesnt-venus-magnetosphere.html){:target="_blank" rel="noopener"}


