---
layout: post
title: "Artificial leaf harnesses sunlight for efficient fuel production"
date: 2016-06-24
categories:
author: "California Institute of Technology"
tags: [Photoelectrochemical cell,Applied and interdisciplinary physics,Chemical elements,Sustainable energy,Manufacturing,Physical chemistry,Artificial materials,Chemical substances,Energy,Technology,Physical sciences,Chemistry,Materials,Nature]
---


Over the past five years, researchers at JCAP have made major advances toward this goal, and they now report the development of the first complete, efficient, safe, integrated solar-driven system for splitting water to create hydrogen fuels. A key part of the JCAP design is the plastic membrane, which keeps the oxygen and hydrogen gases separate. The new complete solar fuel generation system developed by Lewis and colleagues uses such a 62.5-nanometer-thick TiO2 layer to effectively prevent corrosion and improve the stability of a gallium arsenide-based photoelectrode. This catalyst is among the most active known catalysts for splitting water molecules into oxygen, protons, and electrons and is a key to the high efficiency displayed by the device. The photoanode was grown onto a photocathode, which also contains a highly active, inexpensive, nickel-molybdenum catalyst, to create a fully integrated single material that serves as a complete solar-driven water-splitting system.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150828142940.htm){:target="_blank" rel="noopener"}


