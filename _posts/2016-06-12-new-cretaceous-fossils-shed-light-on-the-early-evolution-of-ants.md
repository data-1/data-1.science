---
layout: post
title: "New Cretaceous fossils shed light on the early evolution of ants"
date: 2016-06-12
categories:
author: "Chinese Academy of Sciences Headquarters"
tags: [Ant,Predation,Biology]
---


Ants comprise one lineage of the triumvirate of eusocial insects and experienced their early diversification within the Cretaceous. The success of ants is generally attributed to their remarkable social behavior. Models of early ant evolution predict that the first ants were solitary specialist predators, but discoveries of Cretaceous fossils suggest group recruitment and socially advanced behavior among stem-group ants. Together with other Cretaceous haidomyrmecine ants, the new fossil suggests that at least some of the earliest Formicidae were solitary specialist predators. The exaggerated condition in the new fossil reveals a proficiency for carriage of large-bodied prey to the exclusion of smaller, presumably easier-to-subdue prey, and highlights a more complex and diversified suite of ecological traits for the earliest ants.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/caos-ncf053016.php){:target="_blank" rel="noopener"}


