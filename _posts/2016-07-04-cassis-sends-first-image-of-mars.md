---
layout: post
title: "CaSSIS sends first image of Mars"
date: 2016-07-04
categories:
author: "University of Bern"
tags: [Trace Gas Orbiter,Mars,ExoMars,Discovery and exploration of the Solar System,Terrestrial planets,Science,Outer space,Space science,Astronomy,Spaceflight,Solar System,Spacecraft,Astronomical objects known since antiquity,Bodies of the Solar System,Planets of the Solar System,Space exploration,Flight,Planetary science,Astronautics]
---


The pictures are a part of the mission's preparations for arriving at its destination in October. It was launched with the European Space Agency's ExoMars Trace Gas Orbiter (TGO) in March and has already travelled just under half of its nearly 500 million km journey. Camera is working well  The images have confirmed the sensitivity of the instrument and are sharp, says Antoine Pommerol, co-investigator of the Colour and Stereo Surface Imaging System (CaSSIS) of the Center for Space and Habitability (CSH) at the University of Bern. Observing dynamics on Mars  CaSSIS is a high resolution imaging system designed to complement the data acquired by the other payload on TGO and other Mars orbiters while also enhancing our knowledge of the surface of Mars. The instrument will obtain stereo images of the surface in colour at a resolution of better than 5 m.  It is now known that Mars is more dynamic than previously thought.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/06/160616072227.htm){:target="_blank" rel="noopener"}


