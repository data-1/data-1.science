---
layout: post
title: "Science in space"
date: 2015-09-03
categories:
author: "$author"   
tags: [Outer space,Spaceflight,Aerospace,Spaceflight technology,Discovery and exploration of the Solar System,Space probes,Uncrewed spacecraft,Scientific exploration,Human spaceflight,Space missions,Space traffic management,European Space Agency spacecraft,Astronomy,Space research,Pan-European scientific organizations,Space policy of the European Union,Astronautics,Space exploration,Spacecraft,Space agencies,Space programs,European space programmes,Flight,Space vehicles,European Space Agency,Space science]
---


ESA astronaut Alexander Gerst gives an overview of some of the science he has performed during his Blue Dot mission on the International Space Station so far. From robotic surgery to vaccines and accurate thermometers, research in space is bringing benefits for humans on Earth. Watch as Alexander takes you through some of the highlights of his mission. Read more about his Blue Dot mission here http://www.esa.int/Our_Activities/Human_Spaceflight/Blue_dot  Follow Alexander here: http://alexandergerst.esa.int/

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Blue_dot/Highlights/Science_in_space){:target="_blank" rel="noopener"}


