---
layout: post
title: "Study finds pollution is more deadly than war, natural disasters, and disease"
date: 2017-10-23
categories:
author: Amanda Froelich
tags: []
---


Every year, more people are killed by pollutants — from toxic air to contaminated water — than by all war and violence. Continue reading below Our Featured Videos  Epidemiologist Philip Landrigan, lead author and Dean of global health at the Icahn School of Medicine in New York, said, “There’s been a lot of study of pollution, but it’s never received the resources or level of attention as, say, AIDS or climate change.” Landrigan added that pollution is a “massive problem” few truly comprehend, as what they’re witnessing are “scattered bits of it.” This is the first study of its kind to take into account data on all diseases and death caused by pollution combined. In 2015, one out of four (2.5 million) premature deaths in India and one out of five (1.8 million) premature deaths in China were caused by pollution-related illness. People who are sick or dead cannot contribute to the economy. They need to be looked after.”  To determine the global impact of pollution, the study’s authors used methods outlined by the U.S. Environmental Protection Agency for assessing field data from soil tests, in addition to air and water pollution data from the Global Burden of Disease.

<hr>

[Visit Link](https://inhabitat.com/study-finds-pollution-is-more-deadly-than-war-natural-disasters-and-disease){:target="_blank" rel="noopener"}


