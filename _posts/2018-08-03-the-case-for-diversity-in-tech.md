---
layout: post
title: "The Case for Diversity in Tech"
date: 2018-08-03
categories:
author: ""
tags: [Problem solving]
---


I strongly believe that having a diverse workforce and leadership team is very important to the success of a company or team, but don’t take my word for it, let me instead share with you some of the research and statistics that helped convince me. Why does having a diverse team actually make everyone on that team more effective? [/su_pullquote]  Better Problem Solving  As I mentioned before, a diverse team brings a diverse set of perspectives which improves problem-solving, but it actually goes further than that. Boards are not quite the same as tech jobs, but there’s no reason not to assume that tech workers and teams would benefit from many of the same things that boards do when they are diverse. In the long term, focusing on diversity will also help us increase the size and quality of the talent pool from which we are all hiring.

<hr>

[Visit Link](http://www.women2.com/2018/07/11/the-case-for-diversity-in-tech/){:target="_blank" rel="noopener"}


