---
layout: post
title: "A common electronic language for magnetic resonance"
date: 2018-06-07
categories:
author: "Université de Genève"
tags: [Spectroscopy,Atom,Molecule,Nuclear magnetic resonance,Chemistry,Resonance,Physical chemistry,Atomic molecular and optical physics,Science,Physical sciences,Applied and interdisciplinary physics,Electromagnetism,Physics]
---


Scientists working in the field of organic chemistry are always looking for new molecules that are created and studied using magnetic resonance. The standards used to re-transcribed the collected data is however specific to each laboratory or publication, making it difficult to export the information electronically and thus to be used by the scientific community. This study, published in the journal Magnetic Resonance in Chemistry (Wiley), paves the way for creating an international, open-access database and specific tools, including artificial intelligence analysis. Researchers use magnetic resonance to verify these compositions that are made blind: every atom that makes up the molecule emits a signal, whose frequency is translated in the form of a spectrum that the chemists can then decode. Our new format, called NMReDATA, operates according to a system of labels that are assigned to each item of data extracted from the spectra in a defined order -- and which can be easily read by a computer, says Marion Pupier, a chemical engineer in the Department of Organic Chemistry at UNIGE.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180606120417.htm){:target="_blank" rel="noopener"}


