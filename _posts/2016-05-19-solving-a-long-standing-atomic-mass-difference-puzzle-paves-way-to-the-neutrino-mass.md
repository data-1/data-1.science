---
layout: post
title: "Solving a long-standing atomic mass difference puzzle paves way to the neutrino mass"
date: 2016-05-19
categories:
author: Universität Mainz
tags: [Radioactive decay,Neutrino,Electron capture,Electron,Isotope,Mass,Standard Model,Mass number,Nuclear physics,Nuclear chemistry,Science,Chemistry,Physics,Physical sciences,Nature]
---


To measure the mass of neutrinos, scientists study radioactive decays in which they are emitted. This decay energy must be known with highest precision. Neutrinos are everywhere. Within the ECHo Collaboration, a research group headed by Professor Christoph E. Düllmann of the Institute of Nuclear Chemistry together with colleagues at the TRIGA research reactor at Mainz University have been responsible for the production and preparation of the needed supply of 163Ho. Our successful production of 163Ho samples for these studies is an important step towards the preparation of samples suitable for a sensitive measurement of the neutrino mass, said Düllmann.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150811091918.htm){:target="_blank" rel="noopener"}


