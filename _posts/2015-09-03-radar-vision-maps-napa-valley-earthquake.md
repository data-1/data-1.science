---
layout: post
title: "Radar vision maps Napa Valley earthquake"
date: 2015-09-03
categories:
author: "$author"   
tags: [Sentinel-1A,Earthquake,Sentinel-1,Interferometric synthetic-aperture radar,Outer space,Spaceflight,Space science]
---


Radar images from this fledgling satellite have been used to map the rupture caused by the biggest earthquake that has shaken northern California in 25 years. Yngvar Larsen from Norway’s Northern Research Institute and Petar Marinkovic from PPO.labs in the Netherlands processed this new interferogram from two images: one that Sentinel-1A acquired on 7 August, the day the satellite reached its operational orbit, and another captured on 31 August. Napa Valley quake Importantly, the extent of the ground deformation in the interferogram shows that the fault slip continues further north than the extent of the rupture mapped at the surface. Sharp lines in the interferogram show minor movements on other faults, such as the part of the West Napa Fault system that crosses Napa airport. “COMET scientists are building a system that will provide these results routinely for all continental earthquakes, as well as mapping the slow warping of the ground surface that leads to earthquakes.” Prof. Andy Hooper, also at the University of Leeds, added, “This satellite represents a sea change in the way we will be able to monitor catastrophic events such as earthquakes and volcanic eruptions in the future, due to its systematic observation strategy.”  Radar vision Austin Elliott, a PhD student at the University of California Davis, one of the team mapping the earthquake rupture on the ground, said, “The data from satellites are invaluable for completely identifying the surface break of the earthquake – deformation maps from satellite imagery guide us to places where rupture has not yet been mapped.” Although Sentinel-1A is still being commissioned, ESA was able to respond specifically to the incident and provide data rapidly to the science team.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-1/Radar_vision_maps_Napa_Valley_earthquake){:target="_blank" rel="noopener"}


