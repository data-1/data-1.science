---
layout: post
title: "Getting started with Git"
date: 2016-07-11
categories:
author: "Seth Kenlon
(Team, Red Hat)"
tags: [Git,Computer file,GitHub,World Wide Web,Microsoft Windows,Computer science,Computing,Software,Technology,System software,Software development,Software engineering,Information technology management,Computer architecture,Computers,Computer engineering,Digital media,Information Age,Utility software,Intellectual works,Information technology]
---


To grab an entire repository with Git, use the git clone command with the URL of the Git repository. $ git clone https: // gitlab.com / trashy / trashy.git trashy.clone  Cloning into 'trashy.clone' ...  remote: Counting objects: 142 , done. The problem with using this method is that you might find that certain files don't actually exist in a raw Git repository, as a file might only exist in its complete form after a make command builds the file, which won't happen until you download the repository, read the README or INSTALL file, and run the command. Terminal basics  The first thing to understand is that all files have a path. If you want to clone a Git repository into your Documents directory, then you could open a terminal and run this command:  $ git clone https: // gitlab.com / foo / bar.git $HOME / Documents / bar.clone  Once that is complete, you can open a file manager window, navigate to your Documents folder, and you'll find the bar.clone directory waiting for you.

<hr>

[Visit Link](https://opensource.com/life/16/7/stumbling-git){:target="_blank" rel="noopener"}


