---
layout: post
title: "Finding a good compression utility"
date: 2015-09-16
categories:
author: Sven Vermeulen
tags: [Data compression,Information Age,Computer engineering,Algorithms,Computer programming,Computing,Computer data,Technology,Computer science,Utility software types]
---


No, what I want is to find the best compression for a given file, with best being either  most reduced size (which I call compression delta in my script)  best reduction obtained per time unit (which I call the efficiency)  For me personally, I think I would use it for the various raw image files that I have through the photography hobby. Although the script could be used to get the most compression (without resolving to an optimization algorithm for the compression commands) for each file, this is definitely not the use case. Another use case I'm currently looking into is how much improvement I can get when multiple files (all raw image files) are first grouped in a single archive ( .tar ). The best compression compression command that the script found was:  ~$ 7za -t7z -m0=PPMd:mem=28 a test.nef.7z test.nef  The resulting file ( test.nef.7z ) is 18 MByte, a reduction of 11,64%. The output shows - The package (in Gentoo) that the command belongs to - The command run - The duration (in seconds) - The size (in bytes) of the resulting file - The compression delta (percentage) showing how much is reduced (higher is better) - The efficiency ratio showing how much reduction (percentage) per second (higher is better) When the command supports multithreading, we use the number of available cores on the system (as told by /proc/cpuinfo).

<hr>

[Visit Link](http://blog.siphos.be/2015/08/finding-a-good-compression-utility/){:target="_blank" rel="noopener"}


