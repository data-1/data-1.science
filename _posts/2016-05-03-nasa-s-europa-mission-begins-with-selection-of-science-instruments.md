---
layout: post
title: "NASA's Europa mission begins with selection of science instruments"
date: 2016-05-03
categories:
author:  
tags: [Europa (moon),Moon,Science,Planets of the Solar System,Bodies of the Solar System,Space science,Solar System,Planetary science,Outer space,Astronomy]
---


Credits: NASA/JPL-Caltech  NASA has selected nine science instruments for a mission to Jupiter's moon Europa, to investigate whether the mysterious icy moon could harbor conditions suitable for life. Europa has tantalized us with its enigmatic icy surface and evidence of a vast ocean, following the amazing data from 11 flybys of the Galileo spacecraft over a decade ago and recent Hubble observations suggesting plumes of water shooting out from the moon, said John Grunsfeld, associate administrator for NASA's Science Mission Directorate in Washington. The payload of selected science instruments includes cameras and spectrometers to produce high-resolution images of Europa's surface and determine its composition. This dual-frequency ice penetrating radar instrument is designed to characterize and sound Europa's icy crust from the near-surface to the ocean, revealing the hidden structure of Europa's ice shell and potential water within. This instrument will determine the composition of the surface and subsurface ocean by measuring Europa's extremely tenuous atmosphere and any surface material ejected into space.

<hr>

[Visit Link](http://phys.org/news351879636.html){:target="_blank" rel="noopener"}


