---
layout: post
title: "New research finds ocean's most abundant organisms have clear daily cycles"
date: 2014-07-11
categories:
author: University of Hawaii at Manoa 
tags: [Hawaii Ocean Time-series,Monterey Bay Aquarium Research Institute,Marine microorganisms,Microorganism,Earth sciences,Nature,Science,Biology,Environmental science]
---


But in a new study published in the July 11 issue of the journal Science, researchers working at Station ALOHA, a deep ocean study site 100 km north of Oahu, observed different species of free-living, heterotrophic bacteria turning on diel cycling genes at slightly different times—suggesting a wave of transcriptional activity that passes through the microbial community each day. The coordinated timing of gene firing across different species of ocean microbes could have important implications for energy transformation in the sea. This study was funded in part by the National Science Foundation and by a grant from the Gordon and Betty Moore Foundation [Grant GBMF3777]. About the Environmental Sample Processor (ESP):  Development and applications of the MBARI ESP were made possible with funding from National Science Foundation Grant OCE-0314222 (to C.A.S. http://www.mbari.org/esp/  About the School of Ocean and Earth Science and Technology  The School of Ocean and Earth Science and Technology at the University of Hawaii at Manoa was established by the Board of Regents of the University of Hawaii in 1988 in recognition of the need to realign and further strengthen the excellent education and research resources available within the University.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/uoha-nrf070314.php){:target="_blank" rel="noopener"}


