---
layout: post
title: "Study shows novel pattern of electrical charge movement through DNA"
date: 2016-04-23
categories:
author: Arizona State University
tags: [DNA,Electron,DNA repair,Chemistry,Physical sciences,Biochemistry]
---


The property is known as charge transport. Oxidative damage is believed to play a role in the initiation and progression of cancer. The length of a DNA molecule and its sequence of 4 nucleotides A, T, C and G can be readily modified and studies have shown that both alterations have an effect on how electrical charge moves through the molecule. Charge transport in DNA (and other molecules) over longer distances involves the process of hopping. A further property of DNA is also of importance in considering charge transport.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/asu-ssn041415.php){:target="_blank" rel="noopener"}


