---
layout: post
title: "Found: Andromeda’s first spinning neutron star"
date: 2016-04-05
categories:
author:  
tags: [Neutron star,Andromeda Galaxy,Star,Milky Way,Pulsar,Galaxy,Astronomy,XMM-Newton,Nature,Stars,Outer space,Stellar astronomy,Astronomical objects,Physical sciences,Space science]
---


Science & Exploration Found: Andromeda’s first spinning neutron star 31/03/2016 18310 views 143 likes  Decades of searching in the Milky Way’s nearby ‘twin’ galaxy Andromeda have finally paid off, with the discovery of an elusive breed of stellar corpse, a neutron star, by ESA’s XMM-Newton space telescope. It spins every 1.2 seconds, and appears to be feeding on a neighbouring star that orbits it every 1.3 days. “It could be what we call a ‘peculiar low-mass X-ray binary pulsar’ – in which the companion star is less massive than our Sun – or alternatively an intermediate-mass binary system, with a companion of about two solar masses,” says Paolo Esposito of INAF-Istituto di Astrofisica Spaziale e Fisica Cosmica, Milan, Italy. “We need to acquire more observations of the pulsar and its companion to help determine which scenario is more likely.” “The well-known Andromeda galaxy has long been a source of exciting discoveries, and now an intriguing periodic signal has been detected by our flagship X-ray mission,” adds Norbert Schartel, ESA’s XMM-Newton project scientist. The source detected in the EXTraS data is identified as 3XMM J004301.4+413017.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Found_Andromeda_s_first_spinning_neutron_star){:target="_blank" rel="noopener"}


