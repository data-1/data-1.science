---
layout: post
title: "Astronomy: Dark matter mapped"
date: 2017-09-21
categories:
author: "Yale University"
tags: [Dark matter,Universe,Gravitational lens,Space science,Astronomy,Physical cosmology,Astrophysics,Physical sciences,Physics,Cosmology,Science,Nature,Celestial mechanics]
---


A Yale-led team has produced one of the highest-resolution maps of dark matter ever created, offering a detailed case for the existence of cold dark matter -- sluggish particles that comprise the bulk of matter in the universe. Yale astrophysicist Priyamvada Natarajan led an international team of researchers that analyzed the Hubble images. With the data of these three lensing clusters we have successfully mapped the granularity of dark matter within the clusters in exquisite detail, Natarajan said. Dark matter may explain the very nature of how galaxies form and how the universe is structured. Dark matter particles are thought to provide the unseen mass that is responsible for gravitational lensing, by bending light from distant galaxies.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/03/170301105603.htm){:target="_blank" rel="noopener"}


