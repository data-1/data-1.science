---
layout: post
title: "Mysterious gene functionally decoded"
date: 2014-06-24
categories:
author: Kiel University
tags: [Gene,RNA interference,Gene expression,Alternative splicing,Messenger RNA,Protein,Neurospora crassa,Genetics,Biotechnology,Life sciences,Molecular biology,Biochemistry,Cell biology,Molecular genetics,Biology,Biological processes,Cellular processes,Branches of genetics]
---


The protein BEM46 is found in all creatures having a nucleus. Mammals have several copies of this gene whereas fungus have only one copy. This bem46 mutant exhibited ascospore germination lower than wild type, but much higher than the previously characterized bem46-overexpressing and RNAi lines, says Professor Kempken, senior scientist of the current study. Reinvestigation of the RNAi transformants revealed two types of alternative spliced bem46 mRNA, with expression of either type leading to loss of ascospore germination. This interaction was confirmed in vivo by a split-YFP approach.

<hr>

[Visit Link](http://phys.org/news322731248.html){:target="_blank" rel="noopener"}


