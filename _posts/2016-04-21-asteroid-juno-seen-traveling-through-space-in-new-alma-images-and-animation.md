---
layout: post
title: "Asteroid Juno seen traveling through space in new ALMA images and animation"
date: 2016-04-21
categories:
author:  
tags: [Atacama Large Millimeter Array,Astronomical interferometer,Asteroid,European Southern Observatory,Planetary science,Solar System,Scientific observation,Outer space,Science,Astronomical objects,Physical sciences,Space science,Astronomy]
---


Images were taken when Juno was approximately 295 million kilometers from Earth. Credit: ALMA (NRAO/ESO/NAOJ)  A series of images made with the Atacama Large Millimeter/submillimeter Array (ALMA) provides an unprecedented view of the surface of Juno, one of the largest members of our solar system's main asteroid belt. For this observation, ALMA achieved a resolution of 40 milliarcseconds, meaning that each pixel in the images is about 60 kilometers across, covering approximately one fourth of the surface of Juno. Earlier models of Juno developed by studying its reflected light indicate that it has an oblong, or potato-like, shape with possibly minor indentations on its surface. Juno is one of five targets selected for study during the ALMA Long Baseline Campaign to test the telescope's high-resolution capabilities, achieved when the antennas are at their greatest separation: up to 15 kilometers apart.

<hr>

[Visit Link](http://phys.org/news347619490.html){:target="_blank" rel="noopener"}


