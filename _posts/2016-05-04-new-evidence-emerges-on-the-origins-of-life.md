---
layout: post
title: "New evidence emerges on the origins of life"
date: 2016-05-04
categories:
author: University Of North Carolina Health Care
tags: [Transfer RNA,Last universal common ancestor,Protein,Life,Genetic code,RNA,Organism,Abiogenesis,Genetics,Amino acid,Nature,Cell biology,Biomolecules,Biochemistry,Molecular biology,Biology,Biotechnology,Life sciences,Chemistry]
---


New research shows that the close linkage between the physical properties of amino acids, the genetic code, and protein folding was likely the key factor in the evolution from building blocks to organisms in Earth's primordial soup. Their findings, published in companion papers in the Proceedings of the National Academy of Sciences, fly in the face of the problematic RNA world theory, which posits that RNA - the molecule that today plays roles in coding, regulating, and expressing genes - elevated itself from the primordial soup of amino acids and cosmic chemicals to give rise first to short proteins called peptides and then to single-celled organisms. Those chemicals reacted to form amino acids, which remain the building blocks of proteins in our own cells today. We know a lot about LUCA and we are beginning to learn about the chemistry that produced building blocks like amino acids, but between the two there is a desert of knowledge, Carter said. The first PNAS paper, led by Wolfenden, shows that both the polarities of the twenty amino acids (how they distribute between water and oil) and their sizes help explain the complex process of protein folding - when a chain of connected amino acids arranges itself to form a particular 3-dimensional structure that has a specific biological function.

<hr>

[Visit Link](http://phys.org/news352371006.html){:target="_blank" rel="noopener"}


