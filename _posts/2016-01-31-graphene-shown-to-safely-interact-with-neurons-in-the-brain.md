---
layout: post
title: "Graphene shown to safely interact with neurons in the brain"
date: 2016-01-31
categories:
author: University of Cambridge
tags: [Neuron,Brain,Graphene,Action potential,Nerve,Electroencephalography,Electrode,Neuroscience]
---


The work may be used to build graphene-based electrodes that can safely be implanted in the brain, offering promise for the restoration of sensory functions for amputee or paralysed patients, or for individuals with motor disorders such as epilepsy or Parkinson's disease. By developing methods of working with untreated graphene, the researchers retained the material's electrical conductivity, making it a significantly better electrode. We then tested the ability of neurons to generate electrical signals known to represent brain activities, and found that the neurons retained their neuronal signalling properties unaltered. Our understanding of the brain has increased to such a degree that by interfacing directly between the brain and the outside world we can now harness and control some of its functions. However, the interface between neurons and electrodes has often been problematic: not only do the electrodes need to be highly sensitive to electrical impulses, but they need to be stable in the body without altering the tissue they measure.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/uoc-gst012916.php){:target="_blank" rel="noopener"}


