---
layout: post
title: "Juno spacecraft breaks solar power distance record"
date: 2016-07-04
categories:
author: "Dc Agle"
tags: [Juno (spacecraft),Jupiter,Solar panels on spacecraft,Astronomical objects known since antiquity,Outer space,Space science,Planetary science,Astronomical objects,Science,Sky,Spacecraft,Flight,Bodies of the Solar System,Astronautics,Solar System,Spaceflight,Astronomy]
---


Launching from Earth in 2011, the Juno spacecraft will arrive at Jupiter in 2016 to study the giant planet from an elliptical, polar orbit. Launched in 2011, Juno is the first solar-powered spacecraft designed to operate at such a great distance from the sun. While our massive solar arrays will be generating only 500 watts when we are at Jupiter, Juno is very efficiently designed, and it will be more than enough to get the job done. Juno's maximum distance from the sun during its 16-month science mission will be about 517 million miles (832 million kilometers), an almost five percent increase in the record for solar-powered space vehicles. Over the next year the spacecraft will orbit the Jovian world 33 times, skimming to within 3,100 miles (5,000 kilometers) above the planet's cloud tops every 14 days.

<hr>

[Visit Link](http://phys.org/news/2016-01-juno-spacecraft-solar-power-distance.html){:target="_blank" rel="noopener"}


