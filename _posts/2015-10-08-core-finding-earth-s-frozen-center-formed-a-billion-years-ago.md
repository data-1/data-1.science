---
layout: post
title: "Core Finding: Earth's Frozen Center Formed a Billion Years Ago"
date: 2015-10-08
categories:
author: Tia Ghose
tags: [Planetary core,Earth,Earths magnetic field,Earths inner core,Earths outer core,Mars,Heat transfer,Iron,Planet,Physical sciences,Bodies of the Solar System,Applied and interdisciplinary physics,Astronomy,Geology,Terrestrial planets,Earth sciences,Planets of the Solar System,Planetary science,Nature,Space science,Planets]
---


What's more, the new findings suggest that Earth's magnetic field, which is powered by the swirling flow of liquid iron surrounding the inner core, could continue going strong for quite a while, said study co-author Andy Biggin, a paleomagnetism researcher at the University of Liverpool in England. The theoretical model which best fits our data indicates that the core is losing heat more slowly than at any point in the last 4.5 billion years and that this flow of energy should keep the Earth's magnetic field going for another billion years or more, Biggin said in a statement. For much of those early years, Earth was a blob of molten rock, but over time, the surface cooled and formed a crust that floated on the Earth's liquid core. At some point in this process, the liquid iron churning at the heart of the planet froze. Follow Live Science @livescience, Facebook & Google+.

<hr>

[Visit Link](http://www.livescience.com/52414-earths-core-formed-long-ago.html){:target="_blank" rel="noopener"}


