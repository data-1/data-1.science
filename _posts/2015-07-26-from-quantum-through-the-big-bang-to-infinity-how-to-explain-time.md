---
layout: post
title: "From quantum, through the big bang, to infinity -- how to explain time?"
date: 2015-07-26
categories:
author: De Gruyter 
tags: [Time,Physics,Reality,American Association for the Advancement of Science,Metaphysics,Branches of science,Theoretical physics,Scientific theories,Academic discipline interactions,Philosophy,Concepts in metaphysics,Science]
---


As Augustine of Hippo, expressed in the 4th century: What, then, is time? If no one asks me, I know; if I wish to explain to him who asks, I know not. Explaining time can be an onerous task, and, albeit fascinating, it is elusive. In particular, any attempt to define it inherently proves futile as it invariably involves time itself, leading to circular definitions. H. Chris Ransford, the author of 'The Far Horizons of Time' just published by De Gruyter Open, takes up the challenge, trying to unmask this abiding conundrum - and explores time within the context of the apparent contradictions that seem to bedevil all attempts to come up with a coherent picture of time definition.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/dgo-fqt041015.php){:target="_blank" rel="noopener"}


