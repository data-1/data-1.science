---
layout: post
title: "Sequencing test enables precise identification of drug-resistant TB: Researchers advance SMOR testing, with an eye on precision medicine for tuberculosis"
date: 2017-10-11
categories:
author: "The Translational Genomics Research Institute"
tags: [Antimicrobial resistance,Tuberculosis,DNA sequencing,Infection,Multidrug-resistant tuberculosis,Health care,Infectious diseases,Life sciences,Medicine,Diseases and disorders,Clinical medicine,Medical specialties,Public health,Health sciences,Epidemiology,Microbiology,Health,Causes of death,Immunology]
---


Two studies led by the Translational Genomics Research Institute (TGen) and the University of California San Francisco (UCSF) document how a new advanced genetic sequencing approach can help thwart the growing worldwide threat posed by drug-resistant mutations of tuberculosis (TB). In both studies, the TGen-UCSF team used Single Molecule-Overlapping Reads (SMOR) technology -- invented at TGen -- which precisely identifies variants of TB, even in cases where an older technology, known as Sanger sequencing, found no drug-resistant mutations. A version of this SMOR test is being developed that could be easily deployed to under-developed areas where it may be most needed. Identifying rapidly mutating, drug-resistant strains of TB is the greatest challenge to eradicating this disease, which in 2015 infected 10.4 million and killed 1.8 million people across the globe, according to the U.S. Centers for Disease Control and the World Health Organization -- nearly 5,000 deaths per day worldwide. The second TGen-UCSF study -- Mycobacterium tuberculosis subculture results in loss of potentially clinically relevant heteroresistance -- published Sept. 11 in the journal Antimicrobial Agents and Chemotherapy, also used ultra-deep next-generation SMOR sequencing to examine potentially drug resistant variants of TB.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171010133927.htm){:target="_blank" rel="noopener"}


