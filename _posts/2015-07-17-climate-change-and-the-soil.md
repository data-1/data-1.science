---
layout: post
title: "Climate change and the soil"
date: 2015-07-17
categories:
author: Carnegie Institution for Science 
tags: [Soil,Climate change,Soil respiration,Carnegie Institution for Science,Natural environment,Earth sciences,Nature,Physical geography,Earth phenomena]
---


If true, this would release even more carbon dioxide into the atmosphere, where it would accelerate global warming. Surprisingly, long-term warming had little effect on the overall storage of carbon in the tropical forest soil or the rate at which that carbon is processed into carbon dioxide. This means the observed increase in the rate of soil respiration accompanying rising temperatures is due to carbon dioxide released by the an uptick in the amount of litter falling on the forest floor and an increase in carbon from underground sources. ###  This work was funded by the National Science Foundation, the College of Tropical Agriculture and Human Resources at the University of Hawaii at Manoa, the USDA Forest Service, and the Carnegie Institution for Science. The Carnegie Institution for Science is a private, nonprofit organization headquartered in Washington, DC, with six research departments throughout the U.S.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/ci-cca072314.php){:target="_blank" rel="noopener"}


