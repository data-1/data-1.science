---
layout: post
title: "Japan scientists find ageing cure - for flowers"
date: 2014-08-16
categories:
author: ""      
tags: [Flower]
---


File photo of a morning glory plant where the flowers bloomed for 24 hours at a National Agriculture and Food Research Organization laboratory in Tsukuba, suburban Tokyo  Japanese scientists say they have found a way to slow down the ageing process in flowers by up to a half, meaning bouquets could remain fresh for much longer. This means the plant has fresh purple flowers alongside the paler blooms from the previous day, he said. We have concluded that the gene is linked to petal ageing, Shibuya told AFP by telephone on Thursday. For some flowers, such as carnations, florists currently use chemicals to inhibit ethylene, a plant hormone which sometimes causes blooms to ripen. A gene similar to EPHEMERAL1 could be responsible for petal ageing in these plants, Shibuya said, meaning the ability to suppress it would extend their life.

<hr>

[Visit Link](http://phys.org/news323667355.html){:target="_blank" rel="noopener"}


