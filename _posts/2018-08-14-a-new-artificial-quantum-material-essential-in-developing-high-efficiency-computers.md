---
layout: post
title: "A new artificial quantum material essential in developing high-efficiency computers"
date: 2018-08-14
categories:
author: "Institute of Physics, Chinese Academy of Sciences"
tags: [Electron,Semiconductor,Doping (semiconductor),State of matter,Epitaxy,Matter,Physics,Molecule,Electricity,Weyl semimetal,Atom,Superconductivity,Topological quantum computer,Molecular-beam epitaxy,Electromagnetism,Chemical product engineering,Phases of matter,Chemistry,Condensed matter physics,Nature,Theoretical physics,Condensed matter,Quantum mechanics,Physical sciences,Materials,Physical chemistry,Materials science,Applied and interdisciplinary physics]
---


Scientists at Tsinghua University and Institute of Physics, Chinese Academy of Sciences in Beijing have demonstrated the ability to control the states of matter, thus controlling internal resistance, within multilayered magnetically doped semiconductors using the Quantum Anomalous Hall Effect. The Topological Quantum Computer would be a potential further evolution on this. We can indeed realise QAH multilayers, or a stack of multiple layers of crystal lattices that are experiencing the QAH effect, with several magnetically doped films spaced by insulating Cadmium Selenide layers. These types of structures are very interesting to study because they force some of the electrons into what's called an edge state that, until now, were quite difficult to fabricate. By tuning the thicknesses of the QAH layers and Cadmium Selenide insulating layers; we can drive the system into a magnetic Weyl semimetal ... a state of matter that so far has never been convincingly demonstrated in naturally occurring materials.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/iopc-ana081118.php){:target="_blank" rel="noopener"}


