---
layout: post
title: "How to Enable Syntax Highlighting in Vi/Vim Editor"
date: 2018-04-28
categories:
author: "Ravi Saive"
tags: [Vim (text editor),Syntax highlighting,Text editor,Software,Computer engineering,Information technology management,Digital media,System software,Computer science,Computers,Computer programming,Technology,Software engineering,Computing,Software development]
---


Read Also: 10 Reasons Why You Should Use Vi/Vim Text Editor in Linux  In this article, we will show how to turn on syntax highlighting temporarily or permanently in Vi/Vim text editor. # yum -y install vim-enhanced  How to Enable Syntax Highlighting in VI and VIM  To enable Syntax Highlighting feature in VI editor, open the file called /etc/profile. # vi /home/tecmint/.bashrc  Add the alias function. # source /etc/profile OR # source /home/tecmint/.bashrc  Test Syntax Highlighting in Vi Editor  Open any example code of file with vi editor. Example of Syntax Highlighting in VI  Turn On or Turn Off Syntax Highlighting in VI  You can Turn On or Turn Off syntax highlighting by pressing ESC button and use command as :syntax on and :syntax off in Vi editor.

<hr>

[Visit Link](https://www.tecmint.com/enable-syntax-highlighting-in-vi-editor/){:target="_blank" rel="noopener"}


