---
layout: post
title: "How Einstein's equivalence principle extends to the quantum world"
date: 2018-08-18
categories:
author: "University Of Queensland"
tags: [Quantum mechanics,Mass,Physics,Scientific theories,Theoretical physics,Science,Physical sciences,Applied and interdisciplinary physics,Theory of relativity,Scientific method]
---


Credit: CC0 Public Domain  How Einstein's equivalence principle extends to the quantum world has been puzzling physicists for decades, but a team including a University of Queensland researcher has found the key to this question. UQ physicist, Dr. Magdalena Zych from the ARC Centre of Excellence for Engineered Quantum Systems, and the University of Vienna's Professor Caslav Brukner have been working to discover if quantum objects interact with gravity only through curved space-time. Physicists have been debating whether the principle applies to quantum particles, so to translate it to the quantum world we needed to find out how quantum particles interact with gravity. In a state unique to quantum physics, energy and mass can exist in a 'quantum superposition' – as if they consisted of two different values 'at the same time'. We realised that we had to look how particles in such quantum states of the mass behave in order to understand how a quantum particle sees gravity in general, she said.

<hr>

[Visit Link](https://phys.org/news/2018-08-einstein-equivalence-principle-quantum-world.html){:target="_blank" rel="noopener"}


