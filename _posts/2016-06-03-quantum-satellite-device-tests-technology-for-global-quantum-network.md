---
layout: post
title: "Quantum satellite device tests technology for global quantum network"
date: 2016-06-03
categories:
author: "Centre for Quantum Technologies at the National University of Singapore"
tags: [Quantum mechanics,Quantum entanglement,Quantum network,Photon,Computer network,Small satellite,Science,Theoretical physics,Technology,Applied and interdisciplinary physics,Physics]
---


With a network that carries information in the quantum properties of single particles, you can create secure keys for secret messaging and potentially connect powerful quantum computers in the future. Team-leader Alexander Ling, an Assistant Professor at the Centre for Quantum Technologies (CQT) at NUS, said This is the first time anyone has tested this kind of quantum technology in space. It takes photons from a BluRay laser and splits them into two, then measures the pair's properties, all on board the satellite. With later satellites, the researchers will try sending entangled photons to Earth and to other satellites. Researcher contacts:  Alexander Ling  Principal Investigator and Assistant Professor  Centre for Quantum Technologies, National University of Singapore  cqtalej@nus.edu.sg  +65 6516 2985  Daniel Oi  Lecturer, SUPA Department of Physics  University of Strathclyde, Glasgow, UK  daniel.oi@strath.ac.uk  +44 (0)141 548 3112  Notes for editors:  Alexander Ling will be presenting this work at CLEO2016 in San Jose, United States, on 9 June.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/cfqt-qst060216.php){:target="_blank" rel="noopener"}


