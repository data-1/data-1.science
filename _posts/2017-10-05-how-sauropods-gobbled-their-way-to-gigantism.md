---
layout: post
title: "How sauropods gobbled their way to gigantism"
date: 2017-10-05
categories:
author: "Jon Tennant, Public Library Of Science"
tags: [Sauropoda,Plateosaurus,Dinosaur,Sauropodomorpha]
---


But we do have skulls, and by comparing dinosaurs with other modern reptiles such as crocodiles and birds, we can actually be pretty confident about what the reconstructed skull muscles of dinosaurs looked like. By looking at both the reconstructed skull muscles and bones, Button and colleagues were able to gain a much more accurate mechanical picture of how sauropodomorph skulls function, as opposed to just using the skulls alone like the vast majority of previous studies. Reconstructed muscles of Plateosaurus (left) and Camarasaurus (right). Credit: Button et al., 2016  This increase in bite force which they found on an evolutionary time scale is mirrored in the acquisition of additional key features that would have been critical in the ability of sauropods to bulk feed. The shift from this structurally weaker skull in Plateosaurus to one in Camarasaurus that can accommodate increasing food-related forces would undoubtedly have been significant in sauropods' increasing ability to bulk feed, and ultimately their enormous size as part of an 'evolutionary cascade'.

<hr>

[Visit Link](http://phys.org/news/2016-12-sauropods-gobbled-gigantism.html){:target="_blank" rel="noopener"}


