---
layout: post
title: "Free Software vs Open Source vs Freeware: What's the Difference?"
date: 2018-08-22
categories:
author: "May."
tags: []
---


Free software, open source, freeware, and shareware are some of the most commonly confused software terms in the industry. Here are three of the most popular type of licenses that define free software:  The MIT (Massachusetts Institute of Technology) License: This is a permissive license that places limited restrictions on software reuse. The GNU General Public License v2: This copyleft license gives users the freedom to run, study, and make improvements to the software. To minimize misunderstandings and avoid the terminology debate between free software and open source software, other terms such as FOSS (free and open source software) and FLOSS (free, libre, and open source software) may be used to describe the concepts. For example, if you want to release your created program freely to the open source community, ensure you do sufficient research to understand the limitations and responsibilities of the licensing you select.

<hr>

[Visit Link](https://dzone.com/articles/free-software-vs-open-source-vs-freeware-whats-the?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


