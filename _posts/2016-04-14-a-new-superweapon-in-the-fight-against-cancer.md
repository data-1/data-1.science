---
layout: post
title: "A new superweapon in the fight against cancer"
date: 2016-04-14
categories:
author: Paula Hammond
tags: [TED (conference),Diseases and disorders,Clinical medicine,Medicine,Health,Health sciences,Medical specialties]
---


Cancer is a very clever, adaptable disease. To defeat it, says medical researcher and educator Paula Hammond, we need a new and powerful mode of attack. With her colleagues at MIT, Hammond engineered a nanoparticle one-hundredth the size of a human hair that can treat the most aggressive, drug-resistant cancers. Learn more about this molecular superweapon and join Hammond's quest to fight a disease that affects us all.

<hr>

[Visit Link](http://www.ted.com/talks/paula_hammond_a_new_superweapon_in_the_fight_against_cancer){:target="_blank" rel="noopener"}


