---
layout: post
title: "Navigation system of brain cells decoded"
date: 2017-10-26
categories:
author: Karlsruher Institut für Technologie (KIT) 
tags: [Axon,Brain,Nerve,Neuron,Development of the nervous system,Neuroscience]
---


Information among them is transmitted via a complex network of nerve fibers. But how exactly do the nerve fibers find their target region during growth? Nearly one million nerve fibers reach the visual regions via the visual nerve. The solution: The axons indeed are desensitized for all types of signals guiding them, but they surprisingly preserve the ratio of signal strengths to each other, Weth says. Nothing in biology is more ordered than the hardwiring of our brain.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/kift-nso102517.php){:target="_blank" rel="noopener"}


