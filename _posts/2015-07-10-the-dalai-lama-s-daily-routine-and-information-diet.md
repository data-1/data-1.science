---
layout: post
title: "The Dalai Lama’s Daily Routine and Information Diet"
date: 2015-07-10
categories:
author: Maria Popova
tags: [Tibetan Buddhism,Dalai Lama,Buddhism,Meditation]
---


And yet the Dalai Lama approaches his information diet like he does his meditation — as a deliberate practice. At the heart of this message is a larger testament to the most essential characteristic of reality — something Alan Watts, who began popularizing Eastern philosophy in the West when the Dalai Lama was still a teenager, captured memorably when he wrote: “Life and Reality are not things you can have for yourself unless you accord them to all others.”  Indeed, contacting this interconnectedness of all beings and all lives is the very impetus for the Dalai Lama’s morning routine and his information diet — a beautiful assurance that beneath our obsession with routine and ritual lies a deeper, more expansive longing for meaning, for orienting ourselves in this vibrating universe of interconnectedness that we call reality. You have eyes, ears, legs, hands, and, if you are lucky, all of them are in good working order. You never, if you are sane, think of your finger as an independent entity (though you may occasionally say, “My toe seems to have a mind of its own”). Suddenly, the Dalai Lama’s morning routine and his information diet are revealed in a whole new light of meaning — they are a form of self-empowerment in the journey toward shedding self-centeredness.

<hr>

[Visit Link](http://www.brainpickings.org/2015/06/09/pico-iyer-the-open-road-dalai-lama/){:target="_blank" rel="noopener"}


