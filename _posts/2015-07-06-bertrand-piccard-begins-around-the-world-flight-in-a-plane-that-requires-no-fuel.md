---
layout: post
title: "Bertrand Piccard begins around-the-world flight in a plane that requires no fuel"
date: 2015-07-06
categories:
author: Kate Torgovnick May
tags: [TED (conference),Comet,Blackboard,Astronomy,Space science,Physical sciences,Outer space]
---


This morning, the Solar Impulse 2 — a plane powered only by the sun — took off from Abu Dhabi in the United Arab Emirates, sailing past the Sheikh Zayed Grand Mosque on the first leg of its journey around the world. This flight is the culmination of 12 years of dreaming by TED speaker Bertrand Piccard (watch his talk from 2009, “My solar-powered adventure“), who hopes that this fuel-less plane will spark imaginations about the potential of clean technologies. This around-the-world flight will have Piccard and fellow pilot André Borschberg flying for up to six days at a time in the single-seater before landing and switching off. “What cannot fly forever is the pilot. The pilot is the weak part of the chain.” Read more »

<hr>

[Visit Link](http://feedproxy.google.com/~r/TEDBlog/~3/5a24zPWL8Yw/){:target="_blank" rel="noopener"}


