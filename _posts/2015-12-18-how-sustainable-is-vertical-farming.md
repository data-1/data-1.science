---
layout: post
title: "How sustainable is vertical farming?"
date: 2015-12-18
categories:
author: Renee Cho, Earth Institute, Columbia University
tags: [Vertical farming,Agriculture,Primary sector of the economy,Economy,Natural environment]
---


The 44-member, New York City-based Association for Vertical Farming challenged a group of students in Columbia University's Master of Science in Sustainability Management program, who must work in teams with real clients for their final capstone projects, to come up with a certification system to assess the sustainability of vertical farms. After researching other certification systems to understand the types of systems and frameworks that exist, the students broke them down into various principles and criteria. Most of the farms are new. Phase 1 would collect data for 24 metrics, for example on energy use intensity, total growing area and growing medium. With the fast growth of these new technologies, Gordon-Smith is pondering a larger question: How do we set standards for emerging technologies to balance business and sustainability?

<hr>

[Visit Link](http://phys.org/news/2015-12-sustainable-vertical-farming.html){:target="_blank" rel="noopener"}


