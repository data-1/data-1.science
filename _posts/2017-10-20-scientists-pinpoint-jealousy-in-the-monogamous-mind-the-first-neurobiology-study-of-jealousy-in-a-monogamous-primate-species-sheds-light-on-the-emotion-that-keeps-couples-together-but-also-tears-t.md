---
layout: post
title: "Scientists pinpoint jealousy in the monogamous mind: The first neurobiology study of jealousy in a monogamous primate species sheds light on the emotion that keeps couples together -- but also tears t"
date: 2017-10-20
categories:
author: Frontiers
tags: [Pair bond,Jealousy,Aggression,Human bonding,Psychological concepts,Primate behavior,Cognitive science,Behavioural sciences,Psychology]
---


Jealousy leads to increased brain activity in areas associated with social pain and pair bonding in monogamous monkeys, finds a study published today in open-access journal Frontiers in Ecology and Evolution. Jealousy is especially interesting given its role in romantic relationships -- and also in domestic violence. The researchers found that in the jealousy condition, the monkeys' brains showed heightened activity in an area associated with social pain in humans, the cingulate cortex. Our research indicates that in titi monkeys, this region of the brain also plays a role in maintaining the pair bond. The locations of these areas differ between rodent and primate brains, but the underlying neurochemistry seems to involve the same hormones.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171019101010.htm){:target="_blank" rel="noopener"}


