---
layout: post
title: "What's the difference between a fork and clone?"
date: 2018-02-18
categories:
author: "VM (Vicky) Brasseur
(Alumni)"
tags: [Distributed version control,Fork (software development),Version control,Computer engineering,Systems engineering,Open content,Information technology management,Computer science,Intellectual works,Technology,Open-source movement,Free content,Free software,Software engineering,Software,Software development,Computing]
---


Those who fork a project rarely, if ever, contribute to the parent project again. Whatever the reason, a project fork is the copying of a project with the purpose of creating a new and separate community around it. When you fork a project on GitHub, you are actually just creating a clone of it—a copy on which you can perform your work. It is entirely possible that from here you may choose to fork the project in the original sense: create a separate project and associated community rather than simply sending pull requests back to the original project. This overloading of the word fork has caused more than a little bit of confusion in free and open source software communities, most recently creating the scare that Google might have forked (in the original sense) the Swift programming language (implying that it was creating a new and separate project), rather than what it actually did: clone the project in order to contribute back to it, as any good free and open source citizen would.

<hr>

[Visit Link](https://opensource.com/article/17/12/fork-clone-difference){:target="_blank" rel="noopener"}


