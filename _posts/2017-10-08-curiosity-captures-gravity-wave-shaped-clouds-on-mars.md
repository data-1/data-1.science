---
layout: post
title: "Curiosity captures gravity wave shaped clouds on Mars"
date: 2017-10-08
categories:
author: "Matt Williams"
tags: [Mars,Planetary science,Cloud,Curiosity (rover),Planet,Atmosphere,Water vapor,Earth,Sky,Planets,Nature,Planets of the Solar System,Astronomy,Space science,Outer space,Earth sciences,Physical sciences,Terrestrial planets]
---


As a team of researchers from the Center for Research in Earth and Space Sciences (CRESS) at York University, demonstrated, Curiosity obtained of some rather interesting images of Mars' weather patterns over the past few years. During aphelion, the point farthest from the Sun, the planet is dominated by two cloud systems. Hubble images show cloud formations (left) and the effects of a global dust storm on Mars. Credit: NASA/JPL-Caltec  But as Moore explained in an interview with Science Magazine, seeing an Earth-like phenomenon on Mars is consistent with what we've seen so far from Mars. At the same time, further research is necessary before scientists can say definitely that gravity waves were observed here.

<hr>

[Visit Link](https://phys.org/news/2017-03-curiosity-captures-gravity-clouds-mars.html){:target="_blank" rel="noopener"}


