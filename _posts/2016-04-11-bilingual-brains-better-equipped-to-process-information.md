---
layout: post
title: "Bilingual brains better equipped to process information"
date: 2016-04-11
categories:
author: Northwestern University
tags: [Functional magnetic resonance imaging,Multilingualism,Language,Cognitive psychology,Cognitive science,Neuroscience,Cognition,Interdisciplinary subfields,Linguistics,Mental processes,Behavioural sciences,Branches of science,Psychology]
---


When the brain is constantly exercised in this way, it doesn't have to work as hard to perform cognitive tasks, the researchers found. When you have to do that all the time, you get really good at inhibiting the words you don't need, she said. She found that when bilinguals heard words in one language, such as marker in English, they often made eye movements to objects whose names sounded similar in another language they knew, such as marka which means stamp in Russian. The bilingual speakers were better at filtering out the competing words because their brains are used to controlling two languages and inhibiting the irrelevant words, the researchers found. The fMRI scans showed that monolinguals had more activation in the inhibitory control regions than bilinguals; they had to work much harder to perform the task, Marian said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-11/nu-bbb111014.php){:target="_blank" rel="noopener"}


