---
layout: post
title: "Genetic road map may bring about better cotton crops"
date: 2016-04-23
categories:
author: University Of Texas At Austin
tags: [Cotton,Plant breeding,Genetics,DNA sequencing,Polyploidy,Agriculture,Biology]
---


A University of Texas at Austin scientist, working with an international research team, has developed the most precise sequence map yet of U.S. cotton and will soon create an even more detailed map for navigating the complex cotton genome. Z. Jeffrey Chen and his collaborators, Tianzhen Zhang and Wangzhen Guo at Nanjing Agricultural University (NAU) in China, describe the new draft genomic sequence in a paper published today in Nature Biotechnology. Only about 20-30 percent of the cells on the cotton seed surface actually produce fibers, and no one knows why, said Chen, the D.J. Knowing more about the sequence map could allow for more and better cotton to be produced on every seed or plant. The task is complicated for Upland cotton, a plant formed by two ancestral species 1.5 million years ago and that carries twice as many copies of genes as the presumed parent plants.

<hr>

[Visit Link](http://phys.org/news348739942.html){:target="_blank" rel="noopener"}


