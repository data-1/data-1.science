---
layout: post
title: "Record-breaking tiny Chinese land snails discovered"
date: 2015-09-30
categories:
author: Colm Gorey, Colm Gorey Was A Senior Journalist With Silicon Republic
tags: [Angustopila dominikae,Biology,Organisms,Animals]
---


Raising further questions on the entire genus of microsnails, several new samples of an undiscovered land snail species with a height of less than 1mm have been found. Publishing its findings on ZooKeys, the team also discovered slightly larger species such as the Angustopila subelevata measuring in at 0.87mm. Without the actual creature itself, the ability to pin the evolutionary changes over millions of years is practically impossible. “Extremes in body size of organisms not only attract attention from the public but also incite interest regarding their adaptation to their environment,” the team said in its research paper. “Investigating tiny-shelled land snails is important for assessing biodiversity and natural history as well as for establishing the foundation for studying the evolution of dwarfism in invertebrate animals.”

<hr>

[Visit Link](https://www.siliconrepublic.com/earth-science/2015/09/29/tiny-chinese-land-snails){:target="_blank" rel="noopener"}


