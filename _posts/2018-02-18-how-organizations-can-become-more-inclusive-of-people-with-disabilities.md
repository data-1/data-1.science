---
layout: post
title: "How organizations can become more inclusive of people with disabilities"
date: 2018-02-18
categories:
author: "Michael Schulz"
tags: [Disability,Visual impairment,Screen reader,Accessibility,American Sign Language,Closed captioning,Communication,Technology,Human communication]
---


One of these is people with disabilities. But the community must be willing to do so. Accessibility to events should consider both presenters and attendees. Finding the way to sessions can be a challenge for visually impaired individuals, and an open and welcoming community can address this. Preferred seating can be provided to ensure that attendees with limited vision are located as close as possible to the stage.

<hr>

[Visit Link](https://opensource.com/article/17/12/diversity-and-inclusion){:target="_blank" rel="noopener"}


