---
layout: post
title: "iriss mission liftoff"
date: 2015-09-03
categories:
author: "$author"   
tags: [European Space Agency,Spacecraft,Soyuz (spacecraft),Spaceport,Outer space,Scientific exploration,Flight,Human spaceflight,Space programs,Space vehicles,Life in space,Spaceflight technology,Space exploration,Human spaceflight programs,Rocketry,Aerospace,Space science,Space industry,Crewed spacecraft,Space-based economy,Space research,Spaceflight,Astronautics]
---


ESA astronaut Andreas Mogensen, commander Sergei Volkov and Aidyn Aimbetov were launched into space this morning 2 September at 04:37:43 GMT (06:37:43 CEST) from Baikonur cosmodrome, Kazakhstan. The launch marks the start of ESA’s 10-day ‘iriss’ mission that will focus on testing new technologies and ways of running complex space missions. The astronaut’s Soyuz TMA-18M spacecraft was pushed into Earth orbit as planned accelerating 50 km/h on every second for the first nine minutes of their launch. The spacecraft separated from the Soyuz launcher at 04:46 GMT (06:46 CEST)

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2015/09/iriss_mission_liftoff){:target="_blank" rel="noopener"}


