---
layout: post
title: "Researchers discover first sensor of Earth's magnetic field in an animal"
date: 2015-07-21
categories:
author: University of Texas at Austin 
tags: [Earths magnetic field,Brain,Magnetic field,Neuroscience]
---


A team of scientists and engineers at The University of Texas at Austin has identified the first sensor of the Earth's magnetic field in an animal, finding in the brain of a tiny worm a big clue to a long-held mystery about how animals' internal compasses work. Animals as diverse as migrating geese, sea turtles and wolves are known to navigate using the Earth's magnetic field. Chances are that the same molecules will be used by cuter animals like butterflies and birds, said Jon Pierce-Shimomura, assistant professor of neuroscience in the College of Natural Sciences and member of the research team. The neuron sporting a magnetic field sensor, called an AFD neuron, was already known to sense carbon dioxide levels and temperature. The researchers discovered the worms' magnetosensory abilities by altering the magnetic field around them with a special magnetic coil system and then observing changes in behavior.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/uota-rdf061715.php){:target="_blank" rel="noopener"}


