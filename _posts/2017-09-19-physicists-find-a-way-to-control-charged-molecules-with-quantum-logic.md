---
layout: post
title: "Physicists find a way to control charged molecules -- with quantum logic"
date: 2017-09-19
categories:
author: "National Institute of Standards and Technology (NIST)"
tags: [Molecule,Ion,Atom,Laser,Atomic physics,Physics,Energy level,Light,Atomic clock,Hydrogen,National Institute of Standards and Technology,Quantum,Energy,Atomic molecular and optical physics,Physical sciences,Nature,Chemistry,Theoretical physics,Science,Quantum mechanics,Physical chemistry,Applied and interdisciplinary physics]
---


The new technique achieves an elusive goal, controlling molecules as effectively as laser cooling and other techniques can control atoms. This type of control of molecular ions -- several atoms bound together and carrying an electrical charge -- could lead to more sophisticated architectures for quantum information processing, amplify signals in basic physics research such as measuring the roundness of the electron's shape, and boost control of chemical reactions. The NIST method finds the quantum state (electronic, vibrational, and rotational) of the molecular ion by transferring the information to a second ion, in this case an atomic ion, which can be laser cooled and controlled with previously known techniques. This is part of NIST's basic mission, to develop precision measurement tools that maybe other people can use in their work, Leibfried added. The atomic ion then started scattering light, signaling that the molecular ion's state had changed and it was in the desired target state.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/05/170510132027.htm){:target="_blank" rel="noopener"}


