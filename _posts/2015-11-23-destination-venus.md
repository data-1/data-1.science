---
layout: post
title: "Destination: Venus"
date: 2015-11-23
categories:
author: "$author"  
tags: [Atmosphere of Earth,Venus,Atmosphere,Cloud,Polar vortex,Wind,Vortex,Greenhouse effect,Planet,Earth,Earth sciences,Bodies of the Solar System,Outer space,Nature,Astronomical objects,Applied and interdisciplinary physics,Planets,Space science,Planetary science,Sky,Astronomy,Planets of the Solar System,Physical sciences,Atmospheric sciences,Terrestrial planets,Solar System,Physical geography]
---


Although winds on the planet’s surface move very slowly, at a few kilometres per hour, the atmospheric density at this altitude is so great that they exert greater force than much faster winds would on Earth. This causes some especially dynamic and fast-moving effects in the planet’s upper atmosphere, one of the most prominent being its ‘polar vortices’. The polar vortices arise because there is more sunlight at lower latitudes. In the centre of the polar vortex, sinking air pushes the clouds lower down by several kilometres, to altitudes where the atmospheric temperature is higher. The shape of this vortex core, which typically measures 2000–3000 km across, changes dramatically as it is buffeted by turbulent winds.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2015/11/Destination_Venus){:target="_blank" rel="noopener"}


