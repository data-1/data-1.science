---
layout: post
title: "Evidence of ancient life discovered in mantle rocks deep below the seafloor"
date: 2015-09-02
categories:
author: Woods Hole Oceanographic Institution
tags: [Hydrothermal vent,Life,Seabed,JOIDES Resolution,Woods Hole Oceanographic Institution,Seawater,Supercontinent,Lost City Hydrothermal Field,Ocean,Microorganism,Pangaea,Earth,Physical geography,Physical sciences,Geology,Nature,Earth sciences]
---


Scientist found mummified microbial life in rocks from a seafloor hydrothermal system that was active more than 100 million years ago during the Early Cretaceous when the supercontinent Pangaea was breaking apart and the Atlantic ocean was just about to open. Hydrothermal fluids rich in hydrogen and methane mixed with seawater about 65 meters below the seafloor. The fossilized microbes are likely the same as those found at the active Lost City hydrothermal field, providing potentially important clues about the conditions that support 'intraterrestrial' life in rocks below the seafloor. Drilling Deep  The rock samples analyzed in the study were originally drilled from the Iberian continental margin off the coast of Spain and Portugal in 1993. This prompted Klein to ask Schubotz, an expert in lipid biomarker analysis at the University of Bremen, if she could tease out further information about the lipids from these ancient rocks.

<hr>

[Visit Link](http://phys.org/news/2015-08-evidence-ancient-life-mantle-deep.html){:target="_blank" rel="noopener"}


