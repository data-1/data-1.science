---
layout: post
title: "A step closer to single-atom data storage"
date: 2018-07-12
categories:
author: "Ecole Polytechnique Federale De Lausanne"
tags: [Scanning tunneling microscope,Magnet,Atom,Computer data storage,Microscope,Technology,Electromagnetism,Chemistry,Applied and interdisciplinary physics,Electrical engineering]
---


One of these are single-atom magnets: storage devices consisting of individual atoms stuck (adsorbed) on a surface, each atom able to store a single bit of data that can be written and read using quantum mechanics. The tip reflection, seen at the top of the round silver crystal, is used to align the tip close to the sample surface. Credit: P. Forrester/EPFL  The scientists exposed the atom to extreme conditions that normally de-magnetize single-atom magnets, such as temperature and high magnetic fields, all of which would pose risks to future storage devices. We have demonstrated that the smallest bits can indeed be extremely stable, but next we need to learn how to write information to those bits more effectively to overcome the magnetic 'trilemma' of magnetic recording: stability, writability, and signal-to-noise ratio. Explore further Single-atom magnet breaks new ground for future data storage  More information: Thermal and magnetic field stability of holmium single atom magnets, Thermal and magnetic field stability of holmium single atom magnets, arxiv.org/abs/1712.07871  Fabian Donat Natterer et al, Thermal and Magnetic-Field Stability of Holmium Single-Atom Magnets, Physical Review Letters (2018).

<hr>

[Visit Link](https://phys.org/news/2018-07-closer-single-atom-storage.html){:target="_blank" rel="noopener"}


