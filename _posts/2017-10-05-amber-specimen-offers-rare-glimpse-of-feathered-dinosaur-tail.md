---
layout: post
title: "Amber specimen offers rare glimpse of feathered dinosaur tail"
date: 2017-10-05
categories:
author: "University of Bristol"
tags: [Feather,Dinosaur,Fossil,News aggregator,Amber,Paleontology,Taxa]
---


Researchers from China, Canada, and the University of Bristol have discovered a dinosaur tail complete with its feathers trapped in a piece of amber. The finding reported today in Current Biology helps to fill in details of the dinosaurs' feather structure and evolution, which can't be surmised from fossil evidence. The analysis shows that the soft tissue layer around the bones retained traces of ferrous iron, a relic left over from haemoglobin that was also trapped in the sample. The findings show the value of amber as a supplement to the fossil record. Ryan McKellar added: Amber pieces preserve tiny snapshots of ancient ecosystems, but they record microscopic details, three-dimensional arrangements, and labile tissues that are difficult to study in other settings.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/12/161208141637.htm){:target="_blank" rel="noopener"}


