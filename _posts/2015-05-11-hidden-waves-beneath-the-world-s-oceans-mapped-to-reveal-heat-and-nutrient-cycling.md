---
layout: post
title: "Hidden Waves Beneath the World's Oceans Mapped to Reveal Heat and Nutrient Cycling"
date: 2015-05-11
categories:
author: Science World Report
tags: [Wind wave,Ocean,Internal wave,Earth sciences,Physical geography,Applied and interdisciplinary physics,Oceanography,Nature,Hydrography,Environmental science,Hydrology]
---


Internal waves have long been recognized as essential components of the ocean's nutrient cycle, and key to how oceans will store and distribute additional heat brought on by global warming. They combined computer models constructed largely by Princeton University researchers with on-ship observations. This revealed the movement and energy of the waves from their origin on a double-ridge between Taiwan and the Philippines to when they fade off of the coast of China. Known as the Luzon Strait, this region is perfect for studying internal waves since there's nothing in the way. These waves are known to provide nutrients and actually pose a hazard to shipping; the Luzon Strait internal waves move west at speeds as fast as 18 feet per second and can be as much as 1,640 feet from trough to crest.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/25329/20150511/hidden-waves-beneath-worlds-oceans-mapped-reveal-heat-nutrient-cycling.htm){:target="_blank" rel="noopener"}


