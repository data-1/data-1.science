---
layout: post
title: "How spiders spin silk"
date: 2015-10-28
categories:
author: PLOS 
tags: [Spidroin,Carbonic anhydrase,Spider silk,Spider,Biology,PH,Physical sciences,Chemistry,Biochemistry,Chemical substances]
---


In new research publishing in the open access journal PLOS Biology on August 5, Anna Rising and Jan Johansson show how the silk formation process is regulated. These changes are triggered by an acidity (pH) gradient present between one end of the spider silk gland and the other. By using highly selective microelectrodes to measure the pH within the glands, the authors showed the pH falls from a neutral pH of 7.6 to an acidic pH of 5.7 between the beginning of the tail and half-way down the duct, and that the pH gradient was much steeper than previously thought. The authors also found that pH had opposite effects on the stability of the two regions at each end of the spidroin proteins, which was surprising given that these regions had been suggested to have similar roles in silk formation. Interestingly, the structure of the C-terminal domain is similar to those in the amyloid fibrils found in the brains of individuals with diseases such as Alzheimer's disease.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/p-hss080114.php){:target="_blank" rel="noopener"}


