---
layout: post
title: "Milestone: 100 Million Certificates Issued"
date: 2017-10-12
categories:
author: ""
tags: [Lets Encrypt,World Wide Web,HTTPS,Cyberwarfare,Software,Technology,Computer networking,Cyberspace,Internet,Computing,Application layer protocols,Computer security,Information Age,Telecommunications,Information technology,Secure communication,Internet protocols,Computer science,Cryptography,Digital rights,Wide area networks,Communication]
---


This number reflects at least a few things:  First, it illustrates the strong demand for our services. I’m incredibly proud of the work our engineering teams have done to make this volume of issuance possible. Third, it illustrates the power of automated certificate management. The total number of certificates we’ve issued is an interesting number, but it doesn’t reflect much about tangible progress towards our primary goal: a 100% HTTPS Web. To understand that progress we need to look at this graph:  When Let’s Encrypt’s service first became available, less than 40% of page loads on the Web used HTTPS.

<hr>

[Visit Link](https://letsencrypt.org//2017/06/28/hundred-million-certs.html){:target="_blank" rel="noopener"}


