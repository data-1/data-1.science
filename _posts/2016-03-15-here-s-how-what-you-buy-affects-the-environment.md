---
layout: post
title: "Here's how what you buy affects the environment"
date: 2016-03-15
categories:
author: Norwegian University Of Science
tags: [Global warming potential,Carbon footprint,Greenhouse gas emissions,Ecological footprint,Carbon dioxide,Natural environment,Nature]
---


Source: Ivanova et al. Environmental Impact Assessment of Household Consumption. Their analysis, recently published in the Journal of Industrial Ecology, showed that consumers are responsible for more than 60 per cent of the globe's greenhouse gas emissions, and up to 80 per cent of the world's water use. But between 60-80 per cent of the impacts on the planet come from household consumption. Producing beef requires lots of water because cows eat grains that need water to grow. Diana Ivanova et al. Environmental Impact Assessment of Household Consumption,(2015).

<hr>

[Visit Link](http://phys.org/news/2016-02-affects-environment.html){:target="_blank" rel="noopener"}


