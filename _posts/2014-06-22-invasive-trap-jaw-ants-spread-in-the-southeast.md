---
layout: post
title: "Invasive Trap-Jaw Ants Spread in the Southeast"
date: 2014-06-22
categories:
author: Science World Report
tags: [Ant,Apocrita,Animals]
---


Now, this species is spreading into new territory in the southeastern U.S., and may have actually started to evolve into a separate species. Trap-jaw ant species are in the genus Ondontomachus. And while O. Ondontomachus can be found in the United States, it's actually native to South America. Haemotodus is particularly interesting because it is larger and more aggressive than other trap-jaw ants in the United States, said Magdalena Sorger, one of the researchers, in a news release. The findings reveal a bit more about these under-studied ants, and show that they may be spreading into new territory.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15515/20140618/invasive-trap-jaw-ants-spread-southeast.htm){:target="_blank" rel="noopener"}


