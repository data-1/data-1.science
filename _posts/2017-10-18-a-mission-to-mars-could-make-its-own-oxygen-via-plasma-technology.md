---
layout: post
title: "A mission to Mars could make its own oxygen via plasma technology"
date: 2017-10-18
categories:
author: Institute Of Physics
tags: [Carbon dioxide,In situ resource utilization,Atmosphere,Human mission to Mars,Chemistry,Physical sciences,Nature]
---


Lead author Dr Vasco Guerra, from the University of Lisbon, said: Sending a manned mission to Mars is one of the next major steps in our exploration of space. Low temperature plasmas are one of the best media for CO2 decomposition – the split-up of the molecule into oxygen and carbon monoxide – both by direct electron impact, and by transferring electron energy into vibrational excitation. Mars has excellent conditions for In-Situ Resource Utilisation (ISRU) by plasma. Dr Guerra said: The low temperature plasma decomposition method offers a twofold solution for a manned mission to Mars. The case for in situ resource utilisation for oxygen production on Mars by non-equilibrium plasmas, Plasma Sources Science and Technology (2017).

<hr>

[Visit Link](https://phys.org/news/2017-10-mission-mars-oxygen-plasma-technology.html){:target="_blank" rel="noopener"}


