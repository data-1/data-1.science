---
layout: post
title: "Growing human brain cells in the lab: Scientists develop a cheaper, quicker, and more reliable stem cell-based technology to facilitate drug discovery"
date: 2017-10-11
categories:
author: "Gladstone Institutes"
tags: [Induced pluripotent stem cell,Brain,Neuron,Research,Experiment,News aggregator,Life sciences,Neuroscience]
---


I thought that if we could find a way to simplify and better control that approach, we might be able to improve the way we engineer human brain cells in the lab. They applied their technique to produce human neurons by using iPSCs. Then, they developed a drug discovery platform and screened 1,280 compounds. We showed that we can engineer large quantities of human brain cells that are all the same, while also significantly reducing the costs, said Wang, Gladstone postdoctoral scholar. A Powerful Tool for the Entire Scientific Community  We have developed a cost-effective technology to produce large quantities of human brain cells in two simple steps, summarized Gan.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171010133912.htm){:target="_blank" rel="noopener"}


