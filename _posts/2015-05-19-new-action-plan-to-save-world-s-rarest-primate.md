---
layout: post
title: "New action plan to save world's rarest primate"
date: 2015-05-19
categories:
author: Zoological Society Of London
tags: [Hainan black crested gibbon,Natural environment]
---


This species faces a high risk of extinction due to its isolation and tiny population size - it could potentially become the first ape species to be wiped out by human activity. Dr Samuel Turvey, Senior Research Fellow at ZSL, who co-chaired the major international conservation planning meeting in Hainan that produced the report, said: Ensuring a future for the Hainan gibbon is one of the most important global priorities in mammal conservation. I hope that the Hainan gibbon will be used in the future as an example of a conservation success story. The report identified over 40 key actions needed to boost gibbon numbers and ensure their long-term survival, including enhancing monitoring systems to keep track of remaining individuals, creating canopy bridges between forest fragments to expand their habitat range, and limiting disturbance by people in forested areas. There were more than 2,000 Hainan gibbons in the 1950s.

<hr>

[Visit Link](http://phys.org/news351247779.html){:target="_blank" rel="noopener"}


