---
layout: post
title: "'Residual echo' of ancient humans in scans may hold clues to mental disorders"
date: 2017-08-31
categories:
author: "NIH/National Institute of Mental Health"
tags: [National Institute of Mental Health,Neanderthal,Human,Interbreeding between archaic and modern humans,American Association for the Advancement of Science,Health,Evolution,Mental disorder,Brain,Disease,Neuroimaging]
---


The more a person's genome carries genetic vestiges of Neanderthals, the more certain parts of his or her brain and skull resemble those of humans' evolutionary cousins that went extinct 40,000 years ago, says NIMH's Karen Berman, M.D. In particular, the parts of our brains that enable us to use tools and visualize and locate objects owe some of their lineage to Neanderthal-derived gene variants that are part of our genomes and affect the shape of those structures -- to the extent that an individual harbors the ancient variants. For example, in 2012, Berman and colleagues reported on how genetic variation shapes the structure and function of a brain area called the Insula in the autism-related disorder Williams Syndrome. Neanderthal-derived genetic variation shapes modern human cranium and brain. About the National Institutes of Health (NIH): NIH, the nation's medical research agency, includes 27 Institutes and Centers and is a component of the U.S. Department of Health and Human Services.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-07/niom-eo072517.php){:target="_blank" rel="noopener"}


