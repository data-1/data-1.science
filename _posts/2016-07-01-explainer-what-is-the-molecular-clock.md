---
layout: post
title: "Explainer: What is the molecular clock?"
date: 2016-07-01
categories:
author: "Al Tanner, The Conversation"
tags: [Evolution,Molecular clock,Speciation,Species,Genetics,Biology,Science,Nature,Biological evolution,Evolutionary biology]
---


Dating the origin of species  Sometimes known as the evolutionary clock or gene clock, the molecular clock has foundations in the biological concept of heredity: all life inherits information in the form of genetic molecules (usually DNA) from the previous generation. She wants to know how long ago the history of each book diverged, and studies the differences between the two books. She finds that the texts differ by 100 changes per page, and from this she calculates that the books had a common origin around 500 years ago (100 changes, each one taking 10 years to appear, all divided by two since differences were accumulating in two different book lineages). Evolution can be convergent, where different species independently evolve to appear similar, making it easy to mistake them for being closely related. For some lineages, we know how long ago they shared a common ancestor because we have a fossil of that ancestor, and through techniques such as radiometric dating we can tell how long ago it lived.

<hr>

[Visit Link](http://phys.org/news/2015-09-molecular-clock.html){:target="_blank" rel="noopener"}


