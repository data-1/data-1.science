---
layout: post
title: "GOCE helps tap into sustainable energy resources"
date: 2015-09-03
categories:
author: "$author"   
tags: [Geothermal gradient,Gravity Field and Steady-State Ocean Circulation Explorer,Geothermal energy,Energy development,Map,Earth,Gravity anomaly,Geophysics,Energy,Geology,Nature,Earth sciences]
---


These energy sites exist underground, but often in remote areas, making them difficult, expensive and time-consuming to explore and measure. Free air gravity anomaly The tool’s maps show certain characteristics that may help in the search for geothermal reservoirs, including areas with thin crusts, subduction zones and young magmatic activity. “In doing so, the tool provides a shortcut for lengthy and costly explorations and unlocks the potential of geothermal energy as a reliable and clean contribution to the world’s energy mix.” After a potential site location has been selected using the online tool, ground surveys and seismic measurements are still needed to determine the exact points for energy extraction, but the new resource is a step towards developing a comprehensive geothermal prospecting technique. “This is the first time that global gravity data from GOCE have been used as a tool for geothermal energy site exploration,” said Volker Liebig, Director of ESA’s Earth Observation Programmes. “ESA will continue its collaboration with IRENA to further improve space-based gravity data as a resource for sustainable energy development.”

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/GOCE/GOCE_helps_tap_into_sustainable_energy_resources){:target="_blank" rel="noopener"}


