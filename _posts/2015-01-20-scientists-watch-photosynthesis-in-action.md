---
layout: post
title: "Scientists watch photosynthesis in action"
date: 2015-01-20
categories:
author: Deutsches Elektronen-Synchrotron DESY 
tags: [SLAC National Accelerator Laboratory,DESY,Photosystem II,Water splitting,Chemistry,Physical sciences,Nature,Applied and interdisciplinary physics,Science]
---


The observations show with molecular resolution that photosystem II significantly changes shape during this process. The water splitting process is known to be divided into four steps, explains Chapman, who is also a professor at the University of Hamburg and a member of the Hamburg Center for Ultrafast Imaging CUI. With the short and intense flashes of SLAC's X-ray laser LCLS (Linac Coherent Light Source) the scientists could monitor how the molecular structure of the photosystem II complex changed during the process. The splitting of water during photosynthesis is a catalytic process, meaning that photosystem II enables the reaction without being used up. Free-electron lasers are research light sources that use large particle accelerators.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/ded-swp070714.php){:target="_blank" rel="noopener"}


