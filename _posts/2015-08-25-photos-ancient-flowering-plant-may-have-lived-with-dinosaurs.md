---
layout: post
title: "Photos: Ancient Flowering Plant May Have Lived with Dinosaurs"
date: 2015-08-25
categories:
author: Live Science Staff
tags: [Flower,Science,News,Plant,Fossil]
---


The ancient plant, Montsechia vidalii, lived underwater and is raising new questions about the planet's first flowering plants. [Read full story about the ancient plant fossils]  Aquatic plant  Montsechia vidalii had long shoots and small leaves and likely bloomed underwater. (Credit: David Dilcher, Indiana University)  Ancient blooms  The fossilized remains of Montsechia vidalii show long- and short-leaved forms of the flowering plant. (Credit: David Dilcher, Indiana University)  Underwater pollination  Researchers said the plant appears to have relied on water currents alone to move its pollen about. (Credit: David Dilcher, Indiana University)  Ancient find  The ancient plant is about 125 million to 130 million years old, and lived in the early Cretaceous period.

<hr>

[Visit Link](http://www.livescience.com/51898-ancient-flowering-plant-photos.html){:target="_blank" rel="noopener"}


