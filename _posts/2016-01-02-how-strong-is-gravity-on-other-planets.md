---
layout: post
title: "How strong is gravity on other planets?"
date: 2016-01-02
categories:
author: Matt Williams
tags: [Jupiter,Earth,Mercury (planet),Mass,Solar System,Planet,Mars,Gas giant,Venus,Surface gravity,Saturn,Gravity,Space exploration,Free fall,Moon,Uranus,Gravity of Earth,Neptune,Astronomy,Science,Planemos,Planets,Celestial mechanics,Astronomical objects,Physics,Outer space,Physical sciences,Planetary science,Bodies of the Solar System,Planets of the Solar System,Space science]
---


However, thanks to its high density – a robust 5.427 g/cm3, which is just slightly lower than Earth's 5.514 g/cm3 – Mercury has a surface gravity of 3.7 m/s2, which is the equivalent of 0.38 g.  Gravity on Venus:  Venus is similar to Earth in many ways, which is why it is often referred to as Earth's twin. With a mean radius of 4.6023×108 km2, a mass of 4.8675×1024 kg, and a density of 5.243 g/cm3, Venus is equivalent in size to 0.9499 Earths, 0.815 times as massive, and roughly 0.95 times as dense. Calculations based on its mean radius (1737 km), mass (7.3477 x 1022 kg), and density (3.3464 g/cm3), and the missions conducted by the Apollo astronauts, the surface gravity on the Moon has been measured to be 1.62 m/s2 , or 0.1654 g.  Gravity on Mars:  Mars is also similar to Earth in many key respects. As a result, Jupiter's surface gravity (which is defined as the force of gravity at its cloud tops), is 24.79 m/s, or 2.528 g.  Gravity on Saturn:  Like Jupiter, Saturn is a huge gas giant that is significantly larger and more massive than Earth, but far less dense. Hence, why its surface gravity (measured from its cloud tops) is slightly weaker than Earth's – 8.69 m/s2, or 0.886 g.  Gravity on Neptune:  With a mean radius of 24,622 ± 19 km and a mass of 1.0243×1026 kg, Neptune is the fourth largest planet in the solar system.

<hr>

[Visit Link](http://phys.org/news/2016-01-strong-gravity-planets.html){:target="_blank" rel="noopener"}


