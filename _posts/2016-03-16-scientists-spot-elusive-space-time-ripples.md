---
layout: post
title: "Scientists spot elusive space-time ripples"
date: 2016-03-16
categories:
author:  
tags: []
---


After decades of search for these ripples in space-time, which Albert Einstein predicted exactly 100 years ago, scientists working with the gigantic optical instruments in the U.S. called LIGO [Laser Interferometer Gravitational-wave Observatory], have detected signals of gravitational waves emanating from two merging black holes 1.3 billion light years away arriving at their instruments on the Earth. www.phdcomics.com)  The announcement was beamed across all the laboratories of the world participating in the LIGO Science Collaboration (LSC). The announcement was received with thunderous applause here too because it was a proud moment for the Indian gravitational wave community as well. The total signal lasted for about 0.4 s with the “ringing down” that is characteristic of two orbiting black holes in-spiralling towards each other, shrinking of the orbit, merger of the two, coalescence and finally settling down as a single black hole, he said. The data is consistent with one black hole with 36 solar masses merging with another of 29 solar masses giving rise to a single black hole of 62 solar masses.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/scientists-spot-elusive-spacetime-ripples/article8224711.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


