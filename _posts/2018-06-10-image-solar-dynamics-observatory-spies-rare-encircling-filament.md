---
layout: post
title: "Image: Solar Dynamics Observatory spies rare encircling filament"
date: 2018-06-10
categories:
author: ""
tags: [Solar Dynamics Observatory,Sun,Science,Astronomy,Space science]
---


They are usually elongated and uneven strands. Only a handful of times before have we seen one shaped like a circle. The black area to the left of the brighter active region is a coronal hole, a magnetically open region of the sun. While it may have no major scientific value, it is noteworthy because of its rarity. The still was taken in a wavelength of extreme ultraviolet light.

<hr>

[Visit Link](https://phys.org/news/2017-11-image-solar-dynamics-observatory-spies.html){:target="_blank" rel="noopener"}


