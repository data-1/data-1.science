---
layout: post
title: "New state of matter holds promise for ultracompact data storage and processing"
date: 2016-01-29
categories:
author: Lynn Yarris, Lawrence Berkeley National Laboratory
tags: [Magnetism,Superlattice,Physical sciences,Physics,Electromagnetism,Science,Materials science,Chemistry,Applied and interdisciplinary physics]
---


A team of scientists with the U.S. Department of Energy (DOE)'s Lawrence Berkeley National Laboratory (Berkeley Lab) and the University of California (UC) Berkeley have recorded the first ever observations of rotating topologies of electrical polarization that are similar to the discrete swirls of magnetism known as skyrmions. It has long been thought that rotating topological structures are confined to magnetic systems and aren't possible in ferroelectric materials, but through the creation of artificial superlattices, we have controlled the various energies of a ferrolectric material to promote competition that lead to such new states of matter and polarization arrangements, says Ramamoorthy Ramesh, Berkeley Lab's Associate Laboratory Director for Energy Technologies and the co-principal investigator for this study. Ferroic materials display unique electrical or magnetic properties – or both in the case of multiferroics. (Photo by Roy Kaltschmidt)  We believe the polar vortices we observed in ferroelectrics, when fully explored, have the potential to be topological states of matter that are similar to magnetic skyrmions, Ramesh says. This is just the beginning for the study of polar vortices in ferroelectric materials, Martin says.

<hr>

[Visit Link](http://phys.org/news/2016-01-state-ultracompact-storage.html){:target="_blank" rel="noopener"}


