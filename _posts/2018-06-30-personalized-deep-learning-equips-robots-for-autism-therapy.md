---
layout: post
title: "Personalized “deep learning” equips robots for autism therapy"
date: 2018-06-30
categories:
author: "Becky Ham"
tags: [Autism,Deep learning,Psychotherapy,Robot,Artificial intelligence,Cognitive science,Cognition,Branches of science,Neuroscience,Behavioural sciences,Psychology,Interdisciplinary subfields]
---


Researchers at the MIT Media Lab have now developed a type of personalized machine learning that helps robots estimate the engagement and interest of each child during these interactions, using data that are unique to that child. The therapist then programs the robot to show these same emotions to the child, and observes the child as she or he engages with the robot. One 4-year-old girl hid behind her mother while participating in the session but became much more open to the robot and ended up laughing by the end of the therapy. The robots’ personalized deep learning networks were built from layers of these video, audio, and physiological data, information about the child’s autism diagnosis and abilities, their culture and their gender. Trained on these personalized data coded by the humans, and tested on data not used in training or tuning the models, the networks significantly improved the robot’s automatic estimation of the child’s behavior for most of the children in the study, beyond what would be estimated if the network combined all the children’s data in a “one-size-fits-all” approach, the researchers found.

<hr>

[Visit Link](http://news.mit.edu/2018/personalized-deep-learning-equips-robots-autism-therapy-0627){:target="_blank" rel="noopener"}


