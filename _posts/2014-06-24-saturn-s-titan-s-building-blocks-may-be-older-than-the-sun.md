---
layout: post
title: "Saturn's Titan's Building Blocks May be Older Than the Sun"
date: 2014-06-24
categories:
author: Science World Report
tags: [Titan (moon),Solar System,Atmosphere,Earth,Moon,Sun,Comet,Planet,Saturn,Atmosphere of Titan,Outer space,Planets,Bodies of the Solar System,Astronomical objects,Physical sciences,Astronomy,Planetary science,Planemos,Space science,Planets of the Solar System,Astronomical objects known since antiquity,Nature]
---


Scientists have found evidence that nitrogen in the moon's atmosphere originated in conditions similar to the cold birthplace of the most ancient comets from the Oort cloud, revealing a bit more about the origins of both Saturn and Titan. Nitrogen is the main ingredient in the atmospheres of both Earth and Titan, and Titan itself shares many similarities with Earth. The small amount of change in this isotope allowed the scientists to compare Titan's original building blocks to other solar system objects. Instead, Titan's building blocks formed early in the solar system's history within the cold disk of gas and dusk that formed the sun. The findings don't just have implications for Titan, though; it also has implications for Earth.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15624/20140624/saturns-titans-building-blocks-older-sun.htm){:target="_blank" rel="noopener"}


