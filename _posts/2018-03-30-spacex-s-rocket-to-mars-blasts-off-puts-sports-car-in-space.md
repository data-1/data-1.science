---
layout: post
title: "SpaceX's rocket to Mars blasts off, puts sports car in space"
date: 2018-03-30
categories:
author: ""
tags: []
---


View from space  This image from video provided by SpaceX shows the company's spacesuit in Elon Musk's red Tesla sports car which was launched into space during the first test flight of the Falcon Heavy rocket on Feb. 6, 2018. The Falcon Heavy is a combination of three Falcon 9s, the rocket that the company uses to ship supplies to the International Space Station and lift satellites. Even before the successful test flight, customers were signed up. In six months, a car in Mars  SpaceX founder Elon Musk during a press conference following the first launch of a SpaceX Falcon Heavy rocket. The car faces considerable speed bumps before settling into its intended orbit around the sun, an oval circle stretching from the orbit of Earth on one end to the orbit of Mars on the other.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/technology/spacexs-rocket-to-mars-blasts-off-puts-sports-car-in-space/article22673997.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


