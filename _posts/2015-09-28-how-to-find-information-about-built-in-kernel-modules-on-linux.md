---
layout: post
title: "How to find information about built-in kernel modules on Linux"
date: 2015-09-28
categories:
author: "$author"   
tags: []
---


Is there a way to get a list of all built-in kernel modules and device drivers, and find detailed information about them? The modern Linux kernel has been growing significantly over the years to support a wide variety of hardware devices, file systems and networking functions. Find a List of Built-in Kernel Modules  To get a list of all built-in modules, run the following command. $ cat /lib/modules/$(uname -r)/modules.builtin  You can also get a hint on what modules are built-in by running:  Find Parameters of Built-in Kernel Modules  Each kernel module, whether it's built-in or loadable, comes with a set of parameters. For loadable kernel modules, the modinfo command will show parameter information about them.

<hr>

[Visit Link](http://lxer.com/module/newswire/ext_link.php?rid=219427){:target="_blank" rel="noopener"}


