---
layout: post
title: "Does Marijuana Change the Brain?"
date: 2016-06-14
categories:
author: "Tia Ghose"
tags: [Cannabis (drug),Effects of cannabis,Tetrahydrocannabinol,Schizophrenia,Brain,Neuroscience,Health]
---


And several studies have found that people who smoke pot are more likely to develop schizophrenia than those who don't use the drug. The team found that teens who had smoked pot — even once — did have smaller brain volume in the amygdala, a region associated with emotion processing and reward seeking, compared with those who had not smoked pot. That suggests there might be common factors, genetic and environmental, that predispose us to using marijuana that also contribute to variations in our brain volumes, Agrawal told Live Science. They found that over the four years, those who had smoked pot and also had several genes that increased their risk of schizophrenia had thinning in the cortex — the outer, gray matter of the brain — compared with those who had the same genes but had not smoked pot. Still, that doesn't mean marijuana is safe for people with the right genes, Goldman added.

<hr>

[Visit Link](http://www.livescience.com/51981-does-marijuana-change-the-brain.html){:target="_blank" rel="noopener"}


