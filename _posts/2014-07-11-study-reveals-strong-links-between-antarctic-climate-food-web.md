---
layout: post
title: "Study reveals strong links between Antarctic climate, food web"
date: 2014-07-11
categories:
author: Virginia Institute of Marine Science 
tags: [Antarctica,Marine food web,Phytoplankton,Ocean,Krill,Ice,Antarctic Peninsula,Algal bloom,Sea ice,Oceanography,Hydrology,Systems ecology,Physical geography,Marine biology,Fisheries science,Biogeochemistry,Ecology,Earth phenomena,Applied and interdisciplinary physics,Earth sciences,Natural environment,Hydrography,Nature,Environmental science]
---


Steinberg, one of the Palmer program's lead scientists since 2008, says the current study provides one of the few instances where marine researchers have a dataset of sufficient length and detail to reveal how climate signals can reverberate through a polar food web. The West Antarctic Peninsula is one of the fastest warming regions on Earth, with annual winter temperatures increasing by 11°F during the last 50 years. The team's research shows that populations of photosynthetic algae—the tiny drifting plants that support the polar food web—peak every four to six years in the waters along the West Antarctic Peninsula. In winter during a negative phase of SAM, cold southerly winds blow across the Peninsula, increasing the extent of winter ice. If even one positive SAM episode lasted longer than the krill lifespan—4 or 6 years with decreased phytoplankton abundance and krill recruitment—it could be catastrophic to the krill population.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/viom-srs070714.php){:target="_blank" rel="noopener"}


