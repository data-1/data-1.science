---
layout: post
title: "New tool for gauging public opinion reveals skepticism of climate engineering: Researchers publish results of decision pathway survey on geoengineering"
date: 2016-02-02
categories:
author: University of California - Santa Barbara
tags: [Climate engineering,Climate change,Environmental impact,Climate variability and change,Natural environment]
---


Members of the public find the risks of climate engineering technology more likely than any of the benefits, according to an article published in the current edition of the Proceedings of the National Academy of Sciences. Authored by scholars from the NSF Center for Nanotechnology in Society at the University of California, Santa Barbara (CNS-UCSB) and the University of British Columbia, the article, titled Using Decision Pathway Surveys to Inform Climate Engineering Policy Choices, describes a new method for understanding public concerns regarding the ethics and governance of new technologies, and sheds light on public sentiment towards technologies that either capture greenhouse gases or reflect sunlight away from Earth's surface. Such research is crucial because even if the goal of reducing global temperatures by two degrees Celsius -- as set forth in the Paris Agreement signed onto by 195 nations in December -- is achieved, it will not halt the impacts of global climate change, including sea-level rise, shifts in rainfall, and extreme weather events . Given this context, a growing number of scientists are advocating for climate engineering technologies, also referred to as geoengineering. In order to assess public views of climate engineering, researchers affiliated with CNS-UCSB have devised a tool called a decision pathway survey.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2016/02/160201141915.htm){:target="_blank" rel="noopener"}


