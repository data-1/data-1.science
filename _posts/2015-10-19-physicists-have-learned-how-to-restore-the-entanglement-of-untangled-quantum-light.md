---
layout: post
title: "Physicists have learned how to restore the entanglement of 'untangled' quantum light"
date: 2015-10-19
categories:
author: Russian Quantum Center
tags: [Quantum entanglement,Quantum mechanics,Beam splitter,Photon,Optics,Quantum cryptography,Optical fiber,Light,Physics,Science,Applied and interdisciplinary physics,Electromagnetic radiation,Theoretical physics]
---


Therefore, the possibility of transmission of quantum information is very limited. Lvovsky's group in the Quantum Optics Laboratory at the Russian Quantum Center conducted a series of experiments in which they managed to restore the level of quantum correlation between pulses of light in two optical channels, which was almost completely destroyed after a 20x optical loss. As a result, entangled photon pairs were produced in the crystal and directed into two different optical channels. Then a special amplification procedure was applied, restoring the quantum properties of light in the channel to levels close to the levels measured before the loss. Now it turns out to be of great practical use—it allows one to restore entanglement of quantum light, says Lvovsky.

<hr>

[Visit Link](http://phys.org/news/2015-10-physicists-entanglement-untangled-quantum.html){:target="_blank" rel="noopener"}


