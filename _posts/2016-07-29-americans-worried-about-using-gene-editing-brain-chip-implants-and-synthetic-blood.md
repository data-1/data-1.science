---
layout: post
title: "Americans worried about using gene editing, brain chip implants and synthetic blood"
date: 2016-07-29
categories:
author: "Pew Research Center"
tags: [Brain implant,Brain,Technology,Surgery,Plastic surgery,Dental implant,Genome editing,Human enhancement,Playing God (ethics),Implant (medicine)]
---


US adults show more concern than enthusiasm for using these to 'enhance' human abilities  WASHINGTON, D.C. (July 26, 2016) - Many in the general public think scientific and technological innovations bring helpful change to society, but they are more concerned than excited when it comes to the potential use of emerging technologies to make people's minds sharper, their bodies stronger and healthier than ever before, according to a new Pew Research Center survey. A majority of Americans would be 'very' or 'somewhat' worried about gene editing (68%); brain chips (69%); and synthetic blood (63%), while no more than half say they would be enthusiastic about each of these developments. Americans are a bit more positive about the impact of gene editing to reduce disease; 36% think it will have more benefits than downsides, while 28% think it will have more downsides than benefits. For example, 51% of U.S. adults say that a brain chip implant would be less acceptable if the effects were permanent and could not be reversed. Fewer women than men say they would want gene editing for their baby (43% vs. 54%), brain chip implants (26% vs. 39%) or synthetic blood substitute for themselves (28% vs. 43%).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/prc-awa072016.php){:target="_blank" rel="noopener"}


