---
layout: post
title: "Beyond Pluto—New Horizons' mission is not over yet"
date: 2015-07-15
categories:
author: Jonti Horner And Jonathan P. Marshall, The Conversation
tags: [Pluto,Natural satellite,Charon (moon),New Horizons,Planet,Moon,Planetary science,Space science,Outer space,Bodies of the Solar System,Solar System,Astronomical objects,Planemos,Planets,Local Interstellar Cloud,Physical sciences,Planets of the Solar System,Astronomy]
---


Pluto: Once shattered, twice shy  Like our own Earth, Pluto has an oversized satellite, Charon. But how did this satellite system come to be? It also seems very unlikely they were captured – that just doesn't fit with our observations. Pluto and its moons will therefore be the second shattered satellite system we've seen up close, and the results from New Horizons will be key to interpreting their formation. Jupiter and its volcanic moon Io, taken by New Horizons as it tore past the giant planet en-route to Pluto.

<hr>

[Visit Link](http://phys.org/news/2015-07-plutonew-horizons-mission.html){:target="_blank" rel="noopener"}


