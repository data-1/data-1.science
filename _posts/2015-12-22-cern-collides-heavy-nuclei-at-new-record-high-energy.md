---
layout: post
title: "CERN collides heavy nuclei at new record high energy"
date: 2015-12-22
categories:
author: University of Copenhagen - Niels Bohr Institute
tags: [Quarkgluon plasma,ALICE experiment,Large Hadron Collider,Gluon,Strong interaction,Quark,Elementary particle,Nuclear physics,Physics,Particle physics,Quantum chromodynamics,Physical sciences,Nature,Theoretical physics,Quantum mechanics,Quantum field theory,Applied and interdisciplinary physics,Subatomic particles,Standard Model]
---


The LHC has been colliding protons at record high energy since the summer, but now the time has now come to collide large nuclei (nuclei of lead, Pb, consist of 208 neutrons and protons). Approximately one millionth of a second after the Big Bang, quarks and gluons became confined inside the protons and the neutrons, which are the present day constituents of the atomic nuclei. It is however, possible to recreate a state of matter consisting of quarks and gluons, and which behaves as a liquid, in close imitation of the state of matter prevailing in the very early universe. This is more than 40 times the energy density of a proton, says Jens Jørgen Gaardhøje. The extreme energy density will enable researchers to develop new and detailed models of the quark-gluon-plasma and of the strong interaction, which binds the quarks and nuclear matter together and thus understand the conditions prevailing in the early universe all the way back to a billionth of a second after the Big Bang.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-11/uoc--cch112515.php){:target="_blank" rel="noopener"}


