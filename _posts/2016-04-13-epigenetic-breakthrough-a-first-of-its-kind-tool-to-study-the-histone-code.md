---
layout: post
title: "Epigenetic breakthrough: A first of its kind tool to study the histone code"
date: 2016-04-13
categories:
author: University of North Carolina Health Care
tags: [Gene,Epigenetics,Regulation of gene expression,Histone,Genetics,Cellular differentiation,Biology,Cell (biology),Protein,Cancer,Drosophila melanogaster,Organism,Enzyme,Biochemistry,Life sciences,Molecular biology,Biotechnology,Cell biology]
---


Cancer is actually a disease of development in which the cells don't maintain their proper functions; they don't do what they're supposed to be doing. One aspect of gene regulation involves enzymes placing chemical tags or modifications on histone proteins - which control a cell's access to the DNA sequences that make up a gene. With this new research tool, scientists will be able to better study thousands of enzyme-histone interactions important for human health. Beyond Yeast  Before now, a lot of this epigenetic research had been done in yeast - single cell organisms that also use enzymes to lay chemical tags on histone proteins. With their new fruit fly research model, the UNC researchers altered the histone gene so that this particular enzyme could not modify its histone protein target.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/uonc-eba021015.php){:target="_blank" rel="noopener"}


