---
layout: post
title: "6 top continuous integration tools"
date: 2015-08-07
categories:
author: "Nitish Tiwari"
tags: [Jenkins (software),Application software,Software development,Computing,Software engineering,Software,Technology,Information technology management,Computer science,Computer engineering,Free software,Systems engineering,Technology development,Computer programming,Product development,Open-source movement,Intellectual works,Information technology,Computers]
---


In this post, let's take a look at six open source CI server tools that you can use in your agile setup. Buildbot installation has one or more masters and a collection of slaves. You need to provide a Python configuration script to the master for the Buildbot configuration. A new Travis CI build is triggered after a file is committed to GitHub. The configuration is done using the init.rb file.

<hr>

[Visit Link](http://opensource.com/business/15/7/six-continuous-integration-tools){:target="_blank" rel="noopener"}


