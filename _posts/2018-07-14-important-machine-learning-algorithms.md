---
layout: post
title: "Important Machine Learning Algorithms"
date: 2018-07-14
categories:
author: "Dec."
tags: []
---


Python code:  #Import Library from sklearn import svm #Assumed you have, X (predictor) and Y (target) for training data set and x_test(predictor) of test_dataset # Create SVM classification object model = svm.svc() # there is various option associated with it, this is simple for classification. Even if these features depend on each other or on the existence of the other features, a naive Bayes classifier would consider all of these properties to independently contribute to the probability that this fruit is an apple. Things to consider before selecting KNN:  KNN is computationally expensive. Find the closest distance for each data point from new centroids and get associated with new K clusters. To classify a new object based on attributes, each tree gives a classification and we say the tree “votes” for that class.

<hr>

[Visit Link](https://dzone.com/articles/important-machine-learning-algorithms?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


