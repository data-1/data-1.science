---
layout: post
title: "Mini NASA satellite begins environmental testing"
date: 2017-10-06
categories:
author: "Nasa'S Goddard Space Flight Center"
tags: [CubeSat,Satellite,Goddard Space Flight Center,Magnetosphere,Small satellite,Spaceflight,NASA,Flight,Physical sciences,Space science,Outer space,Astronomy,Astronautics,Science,Spacecraft,Sky,Nature]
---


Dellingr's exterior is lined with solar panels. Credit: NASA's Goddard Space Flight Center/Bill Hrybyk  Construction of NASA's Dellingr CubeSat - a miniature satellite that provides a low-cost platform for missions - is complete, and the satellite has just left the lab for environmental testing. This is a key step after any satellite has been built to make sure it can withstand intense vibrations, the extremes of hot and cold, and even the magnetic fields of space - all the rigorous conditions the CubeSat will encounter during launch and spaceflight. Named for the god of dawn in Norse mythology, Dellingr will study the ionosphere - the outer region of Earth's atmosphere populated by charged particles, ionized by incoming solar radiation and magnetospheric particle precipitation. Slightly larger than a cereal box, Dellingr carries three novel payloads to perform this science: a miniaturized mass spectrometer and two no-boom magnetometer systems.

<hr>

[Visit Link](http://phys.org/news/2016-08-mini-nasa-satellite-environmental.html){:target="_blank" rel="noopener"}


