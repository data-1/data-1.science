---
layout: post
title: "Brain's alertness circuitry conserved through evolution"
date: 2017-11-19
categories:
author: "NIH/National Institute of Mental Health"
tags: [National Institute of Mental Health,National Institute on Drug Abuse,Brain,Health,Neuroscience,Medicine,Health care,Health sciences]
---


Using a molecular method likely to become widely adopted by the field, researchers supported by the National Institutes of Health have discovered brain circuitry essential for alertness, or vigilance - and for brain states more generally. In this case, the researchers used the technique to screen activity of neurons visible through the transparent heads of genetically-engineered larval zebra fish. 2017, Cell 171, 1-13 December 14, 2017 https://doi.org/10.1016/j.cell.2017.10.021  For more information:  Novel technology pioneered by Stanford researchers ties brain circuits to alertness (Stanford news release) http://med.stanford.edu/news/all-news/2017/11/novel-technology-ties-brain-circuits-to-alertness.html  About the National Institute of Mental Health (NIMH): The mission of the NIMH is to transform the understanding and treatment of mental illnesses through basic and clinical research, paving the way for prevention, recovery and cure. About the National Institutes of Health (NIH): NIH, the nation's medical research agency, includes 27 Institutes and Centers and is a component of the U.S. Department of Health and Human Services. For more information about NIH and its programs, visit the NIH website, http://www.nih.gov.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/niom-bac110317.php){:target="_blank" rel="noopener"}


