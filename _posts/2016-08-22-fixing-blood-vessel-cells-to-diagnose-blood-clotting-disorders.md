---
layout: post
title: "'Fixing' blood vessel cells to diagnose blood clotting disorders"
date: 2016-08-22
categories:
author: "Wyss Institute for Biologically Inspired Engineering at Harvard"
tags: [Endothelium,Coagulation,Blood vessel,Hemostasis,Health sciences,Medical specialties,Clinical medicine,Medicine,Tissues (biology),Anatomy,Health,Diseases and disorders,Causes of death]
---


In normal 'hemostasis', the endothelium prevents deadly blood loss and clot formation. Now, a team led by Ingber at the Wyss Institute for Biologically Inspired Engineering at Harvard University has discovered that endothelial cells need not be 'living' in order to confer their effects on blood coagulation. A new device developed by the team, published in August in the journal Biomedical Microdevices, could monitor blood clot formation and diagnose effectiveness of anti-platelet therapy, by microengineering tiny hollow channels lined by chemically 'fixed' human endothelial cells that more closely mimic cellular and vascular flow conditions inside a patient's body than a bare surface. It's a bioinspired device that contains the endothelial function of a diseased patient without having actual living cells, and this greatly increases the robustness of the device, said the study's first author Abhishek Jain, Ph.D., a former Wyss Institute Postdoctoral Fellow who has recently been appointed Assistant Professor of Biomedical Engineering at Texas A&M University. Using chemically fixed tissue that is no longer alive offers a clear, low-risk path toward further testing and product development.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/wifb-bv080916.php){:target="_blank" rel="noopener"}


