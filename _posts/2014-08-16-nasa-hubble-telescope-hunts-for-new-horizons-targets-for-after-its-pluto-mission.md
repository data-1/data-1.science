---
layout: post
title: "NASA Hubble Telescope Hunts for New Horizons' Targets for After its Pluto Mission"
date: 2014-08-16
categories:
author: Science World Report
tags: [New Horizons,Kuiper belt,Hubble Space Telescope,Space missions,Spacecraft,Astronomical objects,Astronautics,Space exploration,Planetary science,Bodies of the Solar System,Spaceflight,Solar System,Outer space,Astronomy,Space science,Science,Flight,Local Interstellar Cloud,Space research,Uncrewed spacecraft,Discovery and exploration of the Solar System,Space program of the United States]
---


But what will happen to the equipment after that? Now, NASA's Hubble Space Telescope is conducting an intensive search to find a suitable outer solar system object for New Horizons to visit after its Pluto mission. More specifically, Hubble has set its sights on the Kuiper Belt when it comes to hunting for an object for study. In order to actually have New Horizons deviate from its course and study the Kuiper Belt, though, Hubble needs to find a minimum of two KBOs. That said, the scientists still need to establish whether or not these KBOs are suitable targets for New Horizons.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15787/20140702/nasa-hubble-telescope-hunts-new-horizons-targets-pluto-mission.htm){:target="_blank" rel="noopener"}


