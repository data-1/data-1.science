---
layout: post
title: "NASA's Fermi mission expands its search for dark matter"
date: 2017-09-21
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Dark matter,Fermi Gamma-ray Space Telescope,Gamma ray,Axion,Milky Way,Weakly interacting massive particles,Matter,Universe,Science,Physical cosmology,Physical sciences,Astronomy,Astronomical objects,Space science,Nature,Physics,Astrophysics]
---


With these results, Fermi has excluded more candidates, has shown that dark matter can contribute to only a small part of the gamma-ray background beyond our galaxy, the Milky Way, and has produced strong limits for dark matter particles in the second-largest galaxy orbiting it. Astronomers see its effects throughout the cosmos -- in the rotation of galaxies, in the distortion of light passing through galaxy clusters, and in simulations of the early universe, which require the presence of dark matter to form galaxies at all. Manuel Meyer at Stockholm University led a study to search for these effects in the gamma rays from NGC 1275, the central galaxy of the Perseus galaxy cluster, located about 240 million light-years away. Blazars constitute more than half of the total gamma-ray sources seen by Fermi, and they make up an even greater share in a new LAT catalog of the highest-energy gamma rays. For more information about NASA's Fermi Gamma-ray Space Telescope, visit:  http://www.nasa.gov/fermi

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/nsfc-nfm081216.php){:target="_blank" rel="noopener"}


