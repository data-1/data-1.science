---
layout: post
title: "Decades of discovery: NASA's exploration of Jupiter"
date: 2016-08-24
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Jupiter,Juno (spacecraft),Solar System,Planet,New Horizons,Sun,Io (moon),CassiniHuygens,Rings of Saturn,Hubble Space Telescope,Voyager program,Planets,Sky,Physical sciences,Local Interstellar Cloud,Planets of the Solar System,Astronautics,Astronomical objects known since antiquity,Astronomical objects,Bodies of the Solar System,Spaceflight,Planetary science,Space science,Astronomy,Outer space]
---


Plus, for the first time, scientists discovered the existence of active volcanoes elsewhere than Earth, on the planet's moon Io. Unlike previous missions, this set of spacecraft -- an atmospheric probe and an orbiter -- were designed to orbit the planet rather than collect data on flyby. Cassini's camera took 26,000 images of the planet and its moons and created the most detailed global color portrait of Jupiter ever produced at the time. Hubble's observations of the planet stretch over 26 years, and many of its observations are concurrent with other Jupiter missions, particularly Galileo. Juno will make observations about Jupiter's atmosphere and magnetic and gravitational fields, providing more information about the planet's structure so scientists can deepen their understanding of Jupiter's origin and evolution.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/nsfc-dod080516.php){:target="_blank" rel="noopener"}


