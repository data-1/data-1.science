---
layout: post
title: "Galileo launch – replay"
date: 2015-09-03
categories:
author: "$author"   
tags: [Galileo (satellite navigation),Guiana Space Centre,Astronautics,Space traffic management,Pan-European scientific organizations,Space agencies,Space policy of the European Union,European Space Agency,Spaceflight technology,Space exploration,Space science,European space programmes,Satellites,Space programs,Flight,Space vehicles,Spacecraft,Outer space,Spaceflight,Rocketry,Astronomy,Bodies of the Solar System,Technology,Aerospace,Space policy,Space organizations]
---


On 22 August, at 12:27 GMT/14:27 CEST, a Soyuz rocket launched Europe’s fifth and six Galileo satellites from Europe's Spaceport in Kourou, French Guiana. Rewatch the moment of launch here. These new satellites are intended to join four Galileo satellites already in orbit, launched in October 2011 and October 2012 respectively. This first quartet were ‘In-Orbit Validation’ satellites, serving to demonstrate the Galileo system would function as planned. These ‘Full Operational Capability’ satellites are significant as the first of the rest of the Galileo constellation.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2014/08/Galileo_launch_replay){:target="_blank" rel="noopener"}


