---
layout: post
title: "Fast access to CryoSat’s Arctic ice measurements now available"
date: 2015-09-03
categories:
author: "$author"   
tags: [Sea ice,CryoSat,Svalbard,Arctic,Arctic ice pack,Sea ice thickness,Sea,Satellite,Earth sciences,Physical geography,Nature,Earth phenomena]
---


ESA’s ice mission has become the first satellite to provide information on Arctic sea-ice thickness in near-real time to aid maritime activities in the polar region. With specialist data processing provided by the UK’s Centre for Polar Observation and Modelling (CPOM), these measurements can now be delivered within two days of acquisition through a website launched today. The rapid data processing is important for managing and planning activities affected by Arctic sea ice, such as shipping, tourism, Arctic exploration and search and rescue. “This new capability goes far beyond CryoSat’s original purpose, which was to collect measurements for scientific research,” said Professor Andy Shepherd, CPOM Director and the CryoSat’s principal scientific advisor. “The mission is now an essential tool for a wide range of services operating in areas of the planet where sea ice forms.”

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/CryoSat/Fast_access_to_CryoSat_s_Arctic_ice_measurements_now_available){:target="_blank" rel="noopener"}


