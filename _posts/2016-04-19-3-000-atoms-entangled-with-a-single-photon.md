---
layout: post
title: "3,000 atoms entangled with a single photon"
date: 2016-04-19
categories:
author: Massachusetts Institute Of Technology
tags: [Quantum entanglement,Atomic clock,Atom,Clock,Photon,Quantum mechanics,Spin (physics),Theoretical physics,Scientific theories,Physical sciences,Applied and interdisciplinary physics,Physics,Science]
---


Today's best atomic clocks are based on the natural oscillations within a cloud of trapped atoms. A laser beam within the clock, directed through the cloud of atoms, can detect the atoms' vibrations, which ultimately determine the length of a single second. The larger the number of entangled particles, then, the better an atomic clock's timekeeping. Picking up quantum noise  Scientists have so far been able to entangle large groups of atoms, although most attempts have only generated entanglement between pairs in a group. Vuletic reasoned that if a photon has passed through the atom cloud without event, its polarization, or direction of oscillation, would remain the same.

<hr>

[Visit Link](http://phys.org/news346488175.html){:target="_blank" rel="noopener"}


