---
layout: post
title: "Scientists unveil plan to create synthetic human genomes"
date: 2016-06-12
categories:
author: ""
tags: [Human Genome Project,Synthetic biology,Genetics,Genome,Genome project,DNA,Gene,Biotechnology,Biology,Life sciences]
---


The ambitious proposal could make it possible to grow human organs for transplant and speed up the development of vaccines, the project backers said in a paper published Thursday in the journal Science. Dubbed Human Genome Project-write or HGP-write—since synthesizing would amount to writing rather than reading our genetic code—the project aims to reduce the cost of engineering DNA segments in the lab. They did not provide an estimate for total costs, saying only that it would likely be less than the $3 billion for the Human Genome Project. Public debate  In response to ethical concerns, the project's promoters said they envisioned a broad public discourse with conversations well in advance of project implementation. First we already replace segments of human genes in cells growing in culture dishes.

<hr>

[Visit Link](http://phys.org/news/2016-06-scientists-unveil-synthetic-human-genomes.html){:target="_blank" rel="noopener"}


