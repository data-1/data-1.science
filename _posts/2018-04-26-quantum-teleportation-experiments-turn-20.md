---
layout: post
title: "Quantum-teleportation experiments turn 20"
date: 2018-04-26
categories:
author: "Gisin, Nicolas Gisin Is In The Group Of Applied Physics, University Of Geneva, Geneva, You Can Also Search For This Author In"
tags: []
---


NEWS AND VIEWS  06 December 2017 Quantum-teleportation experiments turn 20 In 1997, it was demonstrated that quantum states can be teleported from one location to a distant one. To get disembodied here and reconstituted at a distant location sounds both marvellous and impossible. Nature 552, 42-43 (2017)  doi: https://doi.org/10.1038/d41586-017-07689-5  References Boschi, D., Branca, S., De Martini, F., Hardy, L. & Popescu, S. Phys. Bouwmeester, D. et al. Gisin, N. Quantum Chance, Nonlocality, Teleportation and Other Quantum Marvels (Springer, 2014).

<hr>

[Visit Link](http://www.nature.com/articles/d41586-017-07689-5?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


