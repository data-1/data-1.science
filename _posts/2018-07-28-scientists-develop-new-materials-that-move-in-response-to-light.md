---
layout: post
title: "Scientists develop new materials that move in response to light"
date: 2018-07-28
categories:
author: "Tufts University"
tags: [Magnetism,Tufts University,Light,Ferromagnetism,Electromagnetism,Applied and interdisciplinary physics,Materials,Chemistry,Materials science,Physics,Electrical engineering,Technology,Physical sciences,Physical chemistry]
---


(July 23, 2018)--Researchers at Tufts University School of Engineering have developed magnetic elastomeric composites that move in different ways when exposed to light, raising the possibility that these materials could enable a wide range of products that perform simple to complex movements, from tiny engines and valves to solar arrays that bend toward the sunlight. By heating and cooling a magnetic material, one can turn its magnetism off and on. One of the advantages of these materials is that we can selectively activate portions of a structure and control them using localized or focused light, said Meng Li, the first author of the paper, And unlike other light actuated materials based on liquid crystals, these materials can be fashioned to move either toward, or away from the direction of the light. To demonstrate this versatility, the researchers constructed a simple Curie engine. Materials used to create the light actuated materials include polydimethylsoloxane (PDMS), which is a widely used transparent elastomer often shaped into flexible films, and silk fibroin, which is a versatile biocompatible material with excellent optical properties that can be shaped into a wide range of forms - from films to gels, threads, blocks and sponges.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/tu-sdn072318.php){:target="_blank" rel="noopener"}


