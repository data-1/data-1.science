---
layout: post
title: "Your IoT predictions for 2018 (Part 2)"
date: 2018-04-20
categories:
author: "Dec."
tags: []
---


We have severely underestimated the complexity of collecting data at the edge of the network and moving this data securely to nodes in the cloud where it can be analyzed and acted upon. — Dimitri Tcherevik, CTO, Progress  Deploying LPWAN Connectivity in the Industrial Space  Another trend that started building momentum this past year – and will continue into 2018 – is a greater use of low-power, wide-area networking (LPWAN). — Maciej Kranz, Vice President, Corporate Technology Group, Cisco  The Rise of Blockchain and Machine Learning in IoT  Two of the most interesting IoT developments to emerge in 2017, with the most potential for innovation, were blockchain and machine learning. — Mike Bell, EVP IoT and Devices at Canonical  The AI Buzz Stops Here  Artificial intelligence and machine learning are often misunderstood and misused terms. — Toufic Boubez, VP of Engineering on AI and machine learning, Splunk  Growing Investment Drives Real ROI  As IoT platforms mature, and proof of concepts provide the viability of connected strategies, we expect corporate investment to grow at a healthy pace.

<hr>

[Visit Link](https://dzone.com/articles/your-iot-predictions-for-2018-part-two?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


