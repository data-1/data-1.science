---
layout: post
title: "Why do mitochondria retain their own genome?"
date: 2016-05-15
categories:
author: Christopher Packham
tags: [Mitochondrion,Nuclear gene,Cell (biology),Protein,Mitochondrial DNA,Endoplasmic reticulum,Genome,Chloroplast,Molecular biology,Cell biology,Biochemistry,Biology,Biotechnology,Life sciences,Genetics]
---


Over millennia, the genomes of mitochondria evolved under selection for minimal gene content, but researchers have been unable to determine why some but not all mitochondrial genes have been transferred to the nuclear genome. If a transmembrane protein domain (TMD) scored hydrophobic and the length of its tail was longer than 120 amino acids, the researchers predicted it would be arrested by SRP and directed into the endoplasmic reticulum. We conclude that genes for hydrophobic membrane proteins of more than 120 amino acids are likely retained in distinct organelle genomes to ensure a correct localization of these proteins and avoid transport to the endoplasmic reticulum, the authors write. Thus, the researchers conclude, the selection against mistargeting hydrophobic proteins into the endoplasmic reticulum posed at least one major selective constraint on the retention of the mitochondrial genome. The results of the current study are consistent with these findings.

<hr>

[Visit Link](http://phys.org/news/2015-07-mitochondria-retain-genome.html){:target="_blank" rel="noopener"}


