---
layout: post
title: "First completely scalable quantum simulation of a molecule"
date: 2016-07-21
categories:
author: "Bob Yirka"
tags: [Quantum computing,Computer,Quantum algorithm,Computing,Simulation,Privacy,Information Age,Applied mathematics,Branches of science,Technology,Computer science]
---


In a paper uploaded to the open access journal Physical Review X, the team describes the variational quantum eigensolver (VQE) approach they used to create and solve one of the first real-world quantum computer applications. One such problem is solving the molecular electronic structure problem, which as Google Quantum Software Engineer Ryan Babbush notes in a blog post involves searching for the lowest electron energy configuration of a given molecule. Once they had built and programmed the system, they tested it by computing the energy of a hydrogen molecule. P. J. J. O'Malley et al. Scalable Quantum Simulation of Molecular Energies,(2016). We compare the experimental performance of these approaches to show clear evidence that the variational quantum eigensolver is robust to certain errors.

<hr>

[Visit Link](http://phys.org/news/2016-07-scalable-quantum-simulation-molecule.html){:target="_blank" rel="noopener"}


