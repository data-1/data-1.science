---
layout: post
title: "Physicists create new form of light: Newly observed optical state could enable quantum computing with photons"
date: 2018-07-03
categories:
author: "Massachusetts Institute of Technology"
tags: [Photon,Atom,Light,Physical sciences,Quantum mechanics,Applied and interdisciplinary physics,Theoretical physics,Science,Physical chemistry,Atomic molecular and optical physics,Chemistry,Nature,Physics]
---


It may seem like such optical behavior would require bending the rules of physics, but in fact, scientists at MIT, Harvard University, and elsewhere have now demonstrated that photons can indeed be made to interact -- an accomplishment that could open a path toward using photons in quantum computing, if not in light sabers. In controlled experiments, the researchers found that when they shone a very weak laser beam through a dense cloud of ultracold rubidium atoms, rather than exiting the cloud as single, randomly spaced photons, the photons bound together in pairs or triplets, suggesting some kind of interaction -- in this case, attraction -- taking place among them. The phase tells you how strongly they're interacting, and the larger the phase, the stronger they are bound together, Venkatramani explains. The team observed that as three-photon particles exited the atom cloud simultaneously, their phase was shifted compared to what it was when the photons didn't interact at all, and was three times larger than the phase shift of two-photon molecules. If another photon is simultaneously traveling through the cloud, it can also spend some time on a rubidium atom, forming a polariton -- a hybrid that is part photon, part atom.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/02/180215141713.htm){:target="_blank" rel="noopener"}


