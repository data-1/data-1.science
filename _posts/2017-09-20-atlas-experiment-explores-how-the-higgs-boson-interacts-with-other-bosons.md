---
layout: post
title: "ATLAS Experiment explores how the Higgs boson interacts with other bosons"
date: 2017-09-20
categories:
author: "Atlas Experiment"
tags: [ATLAS experiment,Higgs boson,Standard Model,Large Hadron Collider,W and Z bosons,Quantum field theory,Quantum mechanics,Subatomic particles,Nuclear physics,Physical sciences,Theoretical physics,Science,Particle physics,Physics]
---


Figures 1 and 2: Measurement of the Higgs boson production cross sections in its main production modes and normalised to the Standard Model predictions, as obtained by the H→ZZ*→4ℓ and H→γγ decay channels respectively. Now, having recorded more than 36,000 trillion collisions between 2015 and 2016, the ATLAS experiment can perform ever more precise measurements of the properties of the Higgs boson. Measuring how the Higgs boson is produced and how it decays is one of the major goals of the LHC experiments. The H→γγ decay channel is also used to measure several differential cross sections for observables sensitive to Higgs boson production and decay, where good agreement was found between the data and Standard Model predictions. More information: Measurement of the Higgs boson coupling properties in the H→ZZ*→4l decay channel at 13 TeV with the ATLAS detector (ATLAS-CONF-2017-043): Measurement of the Higgs boson coupling properties in the H→ZZ*→4l decay channel at 13 TeV with the ATLAS detector (ATLAS-CONF-2017-043): atlas.web.cern.ch/Atlas/GROUPS … ATLAS-CONF-2017-043/  Measurements of Higgs boson properties in the diphoton decay channel with 36.1 fb−1 pp collision data at the center-of-mass energy of 13 TeV with the ATLAS detector (ATLAS-CONF-2017-045): atlas.web.cern.ch/Atlas/GROUPS … ATLAS-CONF-2017-045/  Combined measurements of Higgs boson production and decay in the H→ZZ*→4ℓ and H→γγ channels using 13 TeV pp collision data collected with the ATLAS experiment (ATLAS-CONF-2017-047): atlas.web.cern.ch/Atlas/GROUPS … ATLAS-CONF-2017-047/

<hr>

[Visit Link](https://phys.org/news/2017-07-atlas-explores-higgs-boson-interacts.html){:target="_blank" rel="noopener"}


