---
layout: post
title: "Animals experience a full range of emotions, according to new research"
date: 2014-08-16
categories:
author: Colin Lecher, Jul
tags: [Websites,Computing,Technology,World Wide Web,Cyberspace,Internet,Communication,Web 20]
---


Cookie banner  We use cookies and other tracking technologies to improve your browsing experience on our site, show personalized content and targeted ads, analyze site traffic, and understand where our audiences come from. To learn more or opt-out, read our Cookie Policy. Please also read our Privacy Notice and Terms of Use, which became effective December 20, 2019. By choosing I Accept, you consent to our use of cookies and other tracking technologies.

<hr>

[Visit Link](http://www.theverge.com/2014/7/3/5869209/animals-depression-anxiety){:target="_blank" rel="noopener"}


