---
layout: post
title: "How to sail through space on sunbeams – solar satellite leads the way"
date: 2015-06-28
categories:
author: Matteo Ceriotti, The Conversation
tags: [Solar sail,Spacecraft propulsion,Rocket,Spaceflight technology,Flight,Outer space,Spaceflight,Space science,Astronautics,Astronomy,Spacecraft,Science,Technology,Space vehicles,Aerospace,Physical sciences]
---


Solar sailing is the space equivalent of boat sailing. The idea behind solar sailing is the same except, instead of wind, particles of light emitted by the Sun drive the craft forward. Vehicles on Earth accelerate by interacting with something around them. Even with large and lightweight sails, the acceleration can still be small. A solar sail produces thrust without propellant mass by simply reflecting incident photons from the sun.

<hr>

[Visit Link](http://phys.org/news353310334.html){:target="_blank" rel="noopener"}


