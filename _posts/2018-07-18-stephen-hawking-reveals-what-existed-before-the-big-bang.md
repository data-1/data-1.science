---
layout: post
title: "Stephen Hawking reveals what existed before the Big Bang"
date: 2018-07-18
categories:
author: "Greg Beach"
tags: []
---


In an interview with astrophysicist Neil deGrasse Tyson, iconic physicist Stephen Hawking recently revealed what he believes existed prior to the Big Bang. “One can regard imaginary and real time as beginning at the South Pole, which is a smooth point of space-time where the normal laws of physics hold. The stability of the solar system is of particular interest to residents of Earth. He predicts that the Earth will become a ball of fire within the next 600 years while also warning humanity that we have less than a century to leave Earth before it becomes uninhabitable. He also warned about the existential dangers of artificial intelligence.

<hr>

[Visit Link](https://inhabitat.com/stephen-hawking-reveals-what-existed-before-the-big-bang){:target="_blank" rel="noopener"}


