---
layout: post
title: "Small, but plentiful: How the faintest galaxies illuminated the early universe"
date: 2014-08-17
categories:
author: Jason Maderer, Royal Astronomical Society
tags: [Milky Way,Universe,Star,Chronology of the universe,Big Bang,Nature,Space science,Physical sciences,Astronomy,Physics,Astronomical objects,Astrophysics,Science]
---


This cool gas will eventually form the first stars in the universe but for millions of years, there are no stars. The light returned when newly forming stars and galaxies re-ionised the universe during the 'epoch of re-ionisation'. The video shows hot and ionised gas in blue, and cold and neutral gas in red. The galaxies were small, but so plentiful that they contributed a significant fraction of UV light in the re-ionisation process. A zoomed-in view of the most massive dwarf galaxy in the simulation, seen when the universe was only 700 million years old.

<hr>

[Visit Link](http://phys.org/news323935989.html){:target="_blank" rel="noopener"}


