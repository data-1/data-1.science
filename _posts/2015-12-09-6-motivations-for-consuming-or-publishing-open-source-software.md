---
layout: post
title: "6 motivations for consuming or publishing open source software"
date: 2015-12-09
categories:
author: "Ben Balter"
tags: [Open source,Proprietary software,Free software,Software,Open-source software,Red Hat,Software bug,Technology,Computing,Software engineering]
---


This frees developers up to work on yet-unsolved challenges, the types of challenges that are unique to and add value to your organization's mission. You're going to be paying for both regardless of if the software is open or closed source, the cost often being baked into the license in the case of commercial, off-the-shelf software (COTS). Cost Open source Proprietary Licensing No Yes Implementation Yes Yes Maintenance Yes Yes Support Yes Yes  Given enough eyeballs, all bugs are shallow (Linus's Law): Empirically, open source tends to produce better quality software than its proprietary or alternative counterparts. Open source is how modern organizations, and increasingly more traditional organizations build software. With open source, not only can leaner, more agile, non-profit-oriented organization move faster, since you have access to the source code, you can often apply fixes, both large and small, at your own convenience, not at the convenience of the publishing organization's release cycle.

<hr>

[Visit Link](https://opensource.com/life/15/12/why-open-source){:target="_blank" rel="noopener"}


