---
layout: post
title: "The ocean below: Scientific plan measures ocean’s carbon cycle, predicts its future conditions"
date: 2016-04-05
categories:
author: University of California - Santa Barbara
tags: [Biological pump,Carbon cycle,Ocean,Nature,Chemical oceanography,Environmental engineering,Ecology,Biogeochemistry,Environmental chemistry,Hydrology,Physical sciences,Systems ecology,Hydrography,Applied and interdisciplinary physics,Natural environment,Chemistry,Environmental science,Physical geography,Oceanography,Earth sciences]
---


It is a key player in the global carbon cycle, producing about half of the world's output of organic carbon. New work by UC Santa Barbara researchers aims to facilitate a new understanding of ocean carbon transport processes, which affect climates around the world. According to the researchers, quantifying this carbon flux is critical for predicting the atmosphere's response to changing climates. Understanding the biological pump is critical, said Siegel, a professor in UCSB's Department of Geography. Siegel noted that once scientists are able to more accurately estimate carbon flux within the ocean, they will seek to determine how those estimates could change in the future.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/03/160330174212.htm){:target="_blank" rel="noopener"}


