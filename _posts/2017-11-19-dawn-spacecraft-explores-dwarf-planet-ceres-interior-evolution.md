---
layout: post
title: "Dawn spacecraft explores dwarf planet Ceres' interior evolution"
date: 2017-11-19
categories:
author: "NASA/Jet Propulsion Laboratory"
tags: [Ceres (dwarf planet),Planet,Dawn (spacecraft),Dwarf planet,Impact event,Impact crater,NASA,Astronomy,Outer space,Solar System,Bodies of the Solar System,Astronomical objects,Science,Planets,Planemos,Physical sciences,Planetary science,Space science]
---


Specifically, the study explored linear features -- the chains of pits and small, secondary craters common on Ceres. Secondary crater chains, the most common of the linear features, are long strings of circular depressions created by fragments thrown out of large impact craters as they formed on Ceres. Scully said the study's greatest challenge was differentiating between secondary crater chains and pit chains. The material may have flowed upward from Ceres' interior because it is less dense than surrounding materials. The Dawn mission is managed by JPL for NASA's Science Mission Directorate in Washington.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/11/171109135703.htm){:target="_blank" rel="noopener"}


