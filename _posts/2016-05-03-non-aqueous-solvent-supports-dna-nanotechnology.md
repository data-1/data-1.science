---
layout: post
title: "Non-aqueous solvent supports DNA nanotechnology"
date: 2016-05-03
categories:
author: Georgia Institute Of Technology
tags: [DNA nanotechnology,Nanotechnology,DNA,Water,Life,Solvent,Solution (chemistry),Materials science,Physical chemistry,Technology,Applied and interdisciplinary physics,Physical sciences,Materials,Chemistry]
---


Researchers at the Georgia Institute of Technology have now shown that they can assemble DNA nanostructures in a solvent containing no water. With this work, we have shown that DNA nanostructures can be assembled in a water-free solvent, and that we can mix water with the same solvent to speed up the assembly. The solvent system developed at Georgia Tech adds a new level of control over DNA assembly. Not only did the glycholine assemble the DNA structure at a relatively low temperature, but it also avoided kinetic traps, intermediate structures that are stable, but not the desired structure, Gállego said. The research on water-free solvents grew out of Georgia Tech research into the origins of life.

<hr>

[Visit Link](http://phys.org/news351926390.html){:target="_blank" rel="noopener"}


