---
layout: post
title: "Quantum leap in lasers at Dartmouth brightens future for quantum computing"
date: 2015-05-22
categories:
author: Dartmouth College 
tags: [Superconductivity,Electron,Light,Atom,Quantum dot,Laser,Photon,Computing,Computer,Theoretical physics,Physics,Quantum mechanics,Electromagnetism,Physical sciences,Applied and interdisciplinary physics,Atomic molecular and optical physics,Science,Physical chemistry,Technology,Chemistry,Electrical engineering]
---


The fact that we use only superconducting pairs is what makes our work so significant, says Alex Rimberg, a professor of physics and astronomy at Dartmouth. Light from the laser is produced by applying electricity to the artificial atom. This causes electrons to hop across the atom and, in the process, produce photons that are trapped between two superconducting mirrors. With the new laser, electrical energy is converted to light that has the ability to transmit information to and from a quantum computer. A computer that does a calculation but has no way of getting the information anywhere else isn't particularly useful.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/dc-qli072214.php){:target="_blank" rel="noopener"}


