---
layout: post
title: "Bioengineered robotic hand with its own nervous system will sense touch"
date: 2017-11-19
categories:
author: "Florida Atlantic University"
tags: [Prosthesis,Nerve,Sense,Robotics,Somatosensory system,Neuroprosthetics,Nervous system,Neuron,Sensory neuron,Brain,Engineering,Florida Atlantic University,Branches of science,Neuroscience]
---


With expertise in robotics, bioengineering, behavioral science, nerve regeneration, electrophysiology, microfluidic devices, and orthopedic surgery, the research team is creating a living pathway from the robot's touch sensation to the user's brain to help amputees control the robotic hand. The research team will be able to stimulate the neurons with electrical impulses from the robot's hand to help regrowth after injury. Using an electroencephalogram (EEG) to detect electrical activity in the brain, Emmanuelle Tognoli, Ph.D., co-principal investigator, associate research professor in FAU's Center for Complex Systems and Brain Sciences in the Charles E. Schmidt College of Science, and an expert in electrophysiology and neural, behavioral, and cognitive sciences, will examine how the tactile information from the robotic sensors is passed onto the brain to distinguish scenarios with successful or unsuccessful functional restoration of the sense of touch. Once the nerve impulses from the robot's tactile sensors have gone through the microfluidic chamber, they are sent back to the human user manipulating the robotic hand. Researchers also are working in collaboration with I-SENSE and FAU's Brain Institute, two of the University's research pillars, focused on institutional strengths.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/fau-brh111317.php){:target="_blank" rel="noopener"}


