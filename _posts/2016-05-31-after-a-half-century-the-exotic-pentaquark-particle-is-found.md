---
layout: post
title: "After a half century, the exotic pentaquark particle is found"
date: 2016-05-31
categories:
author: "Rod Pyle, California Institute Of Technology"
tags: [Quark,Pentaquark,Hadron,Elementary particle,Baryon,LHCb experiment,Murray Gell-Mann,Meson,Standard Model,Particle physics,Physics,Subatomic particles,Quantum field theory,Quantum chromodynamics,Nuclear physics,Fermions,Theoretical physics,Science]
---


Combining a quark and an antiquark (a quark's antimatter equivalent) creates a type of hadron called a meson, while baryons are hadrons composed of three quarks. Protons, for example, have two up quarks and one down quark, while neutrons have one up and two down quarks. Gell-Mann's scheme also allowed for more exotic forms of composite particles, including tetraquarks, made of four quarks, and the pentaquark, consisting of four quarks and an antiquark. A small fraction of the protons collide, creating other particles in the process. After further analysis, the researchers concluded that the objects were pentaquarks composed of two up quarks, one down quark, one charm quark, and one anticharm quark.

<hr>

[Visit Link](http://phys.org/news/2015-08-century-exotic-pentaquark-particle.html){:target="_blank" rel="noopener"}


