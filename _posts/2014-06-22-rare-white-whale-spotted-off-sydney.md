---
layout: post
title: "Rare white whale spotted off Sydney"
date: 2014-06-22
categories:
author: ""         
tags: [Humpback whale,Cetaceans,Mammal infraorders,Cetology]
---


Tourists on the deck of a whale watching vessel off the coast near Sydney on June 8, 2010  An extremely rare white humpback whale has been spotted off the coast of Sydney in an event onlookers called a once-in-a-lifetime experience. There is an exclusion zone of 500 metres (yards) around him which we stuck to but with the nature of a competition pod it's sometimes hard to predict where they will come up next. So it is a very rare sight. In 2011, a baby white whale, believed to be just a few weeks old, was seen off Queensland's Whitsunday Islands, but it has not been spotted since. Explore further Rare white whale calf spotted off Australia  © 2014 AFP

<hr>

[Visit Link](http://phys.org/news322488025.html){:target="_blank" rel="noopener"}


