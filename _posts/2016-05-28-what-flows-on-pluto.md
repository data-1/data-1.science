---
layout: post
title: "What flows on Pluto?"
date: 2016-05-28
categories:
author: "Helen Maynard-Casely, The Conversation"
tags: [Pluto,Moons of Pluto,Hydrogen,Molecule,New Horizons,Ice,Physical sciences,Physical chemistry,Chemistry]
---


Contrasting that with the other major finding of 3000 m water-ice mountains on Pluto – does it leave you wondering why ice can form mountains on Pluto, but methane flows? Part of the reason that ice takes up that structures under the conditions of Earth (and indeed the surface of Pluto) is the way that the hydrogen atoms in each water (H 2 O) 'stick' to the oxygen atom in the next molecule. These mountains don't flow like on Earth because it's too cold. If your ball pool was square and, for a moment, didn't have a ton of kids playing in it the balls would arrange themselves in a structure we call 'close packing'. And that's the structure that solid methane forms.

<hr>

[Visit Link](http://phys.org/news/2015-08-pluto.html){:target="_blank" rel="noopener"}


