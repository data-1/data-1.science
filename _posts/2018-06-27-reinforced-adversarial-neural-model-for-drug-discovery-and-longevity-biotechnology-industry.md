---
layout: post
title: "Reinforced adversarial neural model for drug discovery and longevity biotechnology industry"
date: 2018-06-27
categories:
author: "InSilico Medicine"
tags: [Insilico Medicine,Deep learning,Artificial intelligence,Drug design,Generative adversarial network,Medicinal chemistry,Drug discovery,Research,In silico,Neural network,Medicine,Ageing,Chemistry,Branches of science,Technology,Life sciences,Science,Biology,Biotechnology]
---


Tuesday, June 26st, Rockville, MD - Today, Insilico Medicine, Inc., a Rockville-based next-generation artificial intelligence company specializing in the application of deep learning for target identification, drug discovery and aging research announces the publication of a new research paper Reinforced Adversarial Neural Computer for De Novo Molecular Design in The Journal of Chemical Information and Modeling. As a result, many of the generated structures meet the crucial criteria used in medicinal chemistry of today and are able to pass medical chemistry filters. For its work in the field of artificial intelligence for drug discovery and development, Insilico Medicine received the Frost & Sullivan 2018 North American Artificial Intelligence for Aging Research and Drug Development Technology Innovation Award. Insilico pioneered the applications of the generative adversarial networks (GANs) and reinforcement learning for generation of novel molecular structures for the diseases with a known target and with no known targets. Through a partnership with LifeExtension.com the company launched a range of nutraceutical products compounded using the advanced bioinformatics techniques and deep learning approaches.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/imi-ran062618.php){:target="_blank" rel="noopener"}


