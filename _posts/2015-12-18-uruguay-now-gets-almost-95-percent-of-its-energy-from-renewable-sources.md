---
layout: post
title: "Uruguay now gets almost 95 percent of its energy from renewable sources"
date: 2015-12-18
categories:
author: Cat Distasio
tags: []
---


As the world awaits news from Paris about the next international climate deal, some smaller nations are taking big steps toward a future filled with clean and renewable energy that doesn’t contribute to global warming. In less than 10 years, the developing country has reduced its environmental impact and energy costs simultaneously by implementing enough clean energy infrastructure to get 94.5 percent of its energy from renewable sources. Continue reading below Our Featured Videos  Uruguay’s national director of energy, Ramón Méndez, shared the good news in a statement issued as leaders from around the world meet to develop a global strategy for fighting climate change. Now, clean energy has brought costs down and helped make the energy supply more reliable, translating into greater resilience to droughts. SIGN UP I agree to receive emails from the site.

<hr>

[Visit Link](http://inhabitat.com/uruguay-now-gets-almost-95-percent-of-its-energy-from-renewable-sources/){:target="_blank" rel="noopener"}


