---
layout: post
title: "Meals ready to eat: Expedition 44 crew members sample leafy greens grown on space station"
date: 2016-05-17
categories:
author: NASA/Johnson Space Center
tags: [NASA,Vegetable,International Space Station,Astronautics,Outer space,Spaceflight]
---


Fresh food grown in the microgravity environment of space officially is on the menu for the first time for NASA astronauts on the International Space Station. Fresh food grown in the microgravity environment of space officially is on the menu for the first time for NASA astronauts on the International Space Station. Wheeler said Veggie will help NASA learn more about growing plants in controlled environment agriculture settings. The Veggie experiment is currently the only experiment we are supporting which involves evaluating the effects of plant life on humans in space, Whitmire said. The team at Kennedy and Johnson hope that Veggie and space gardening will become a valued feature of life aboard the space station and in the future on Mars.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/nsc-mrt080715.php){:target="_blank" rel="noopener"}


