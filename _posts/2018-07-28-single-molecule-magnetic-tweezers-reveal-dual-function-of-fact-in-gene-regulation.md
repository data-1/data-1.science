---
layout: post
title: "Single-molecule magnetic tweezers reveal dual function of FACT in gene regulation"
date: 2018-07-28
categories:
author: "Chinese Academy of Sciences Headquarters"
tags: [Nucleosome,Chromatin,Transcription (biology),DNA,Histone,Biological processes,Biomolecules,Gene expression,Nucleic acids,Molecular biology,Genetics,Biochemistry,Biotechnology,Biology,Life sciences,Molecular genetics,Cell biology,Branches of genetics,Cellular processes,Macromolecules]
---


FACT (Facilitates Chromatin Transcription) is the key factor facilitating the elongation of RNA polymerase on chromatin. By exerting tension on a chromatin, magnetic tweezers can be used to study the construction of a chromatin by deconstructing it, thus yielding force spectroscopic fingerprints characteristic of each chromatin. In collaboration with Professors LI Guohong and CHEN Ping from the Institute of Biophysics of the Chinese Academy of Sciences, they investigated the dynamics of nucleosomes and chromatin fibers in the presence of FACT and deciphered the role of FACT in remodeling nucleosomes and chromatin fibers at the single-molecular level. At the same time, nucleosomes cannot reassemble their intact structure in the absence of FACT, due to the dissociation of histones from DNA. This revelation regarding FACT suggests that it plays an essential role in restringing nucleosomes to preserve the histone on DNA throughout the DNA polymerase passage during DNA transcription.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/caos-smt071918.php){:target="_blank" rel="noopener"}


