---
layout: post
title: "At the origin of language structure"
date: 2016-06-14
categories:
author: "Sissa Medialab"
tags: [Subjectobjectverb word order,Language,Subjectverbobject word order,Object (grammar),News aggregator,Verb,Branches of science,Syntax,Communication,Grammar,Culture,Interdisciplinary subfields,Linguistics,Branches of linguistics,Cognition,Human communication,Cognitive science]
---


A team from SISSA's Language, Cognition and Development Lab (along with two Iranian institutions) studied the mechanism that controls the transition from the SOV form, considered the basic order by scientists, to the SVO order while the language is evolving, demonstrating that when the computational load on the brain is lightened, humans choose more efficient systems of communication which encourage the use of more complex grammatical structures. The study by Marno and other SISSA colleagues (Alan Langus and professor Marina Nespor), as well as colleagues from the Medical University of Tehran and the Institute for Research in Fundamental Sciences of Tehran has been published in the journal Frontiers of Psychology. However, there is an element that opposes this growth: the limit of computational load that our cognitive system is able to withstand, explains Marno. In the original experiments (performed by Alan Langus and Marina Nespor), two groups of subjects (one speaking Italian, an SVO language, and another speaking Turkish, an SOV language) had to communicate messages using gestural language they invented. A clear preference for the SOV form emerged, regardless of the language of origin.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150827083532.htm){:target="_blank" rel="noopener"}


