---
layout: post
title: "Why seashells' mineral forms differently in seawater"
date: 2016-04-15
categories:
author: David L. Chandler, Massachusetts Institute Of Technology
tags: [Calcite,Aragonite,Calcium,Calcium carbonate,Ocean acidification,Carbonate,Carbon,Mineral,Exoskeleton,Applied and interdisciplinary physics,Chemistry,Nature,Physical sciences,Materials]
---


Two different forms of calcium carbonate have identical chemical composition, but look different and have different properties such as solubility. Calcium carbonate can take the form of two different minerals: Calcite is the stable form, whereas aragonite is metastable: Over time, or when heated, it can ultimately transform into calcite. The MIT team's analysis shows that the ratio of calcium to magnesium in the water affects the surface energy of the nucleating crystals; when that ratio passes a specific value, it tips the balance from forming calcite to forming aragonite. The researchers' calculated results closely match the proportions of the two forms seen experimentally when the magnesium ratios are varied, Sun says, showing that the analysis provides a tool to predict how other compounds will form from a solution. So far, computational materials science has been very useful at predicting which materials might possess desirable technological properties, Sun says, This work enables us to predict how to reliably make them.

<hr>

[Visit Link](http://phys.org/news344586376.html){:target="_blank" rel="noopener"}


