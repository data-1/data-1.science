---
layout: post
title: "How the cone snail's deadly venom can help us build better medicines"
date: 2017-10-11
categories:
author: "National Institute of Standards and Technology (NIST)"
tags: [Venom,Cone snail,Nervous system,Drosophila melanogaster,Cell (biology),Stinger,Predation,Toxin,Brain,Biology,Animals]
---


Like all NIST scientists, Marí measures things. Cone snails are so unusual, Marí says. For a paper just published in Scientific Reports, Marí and his team used cone snail toxins as molecular probes to identify an important overlap between the immune and central nervous systems in humans. The pattern on a cone snail shell is very beautiful, Marí says. More information about NIST can be found at http://www.nist.gov.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/nios-htc101017.php){:target="_blank" rel="noopener"}


