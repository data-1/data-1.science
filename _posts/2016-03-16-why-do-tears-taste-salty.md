---
layout: post
title: "Why do tears taste salty?"
date: 2016-03-16
categories:
author:  
tags: []
---


Janani, Chennai  Body fluids like sweat and tears are salty to taste and this has physiological, immunological and evolutionary significance. These tears are classified into basal, reflex and psychic tears. Basal tears are responsible for keeping the cornea of eye moist. Reflex tears are produced during eye irritation. The salinity of tears is attributed to the presence of salts of sodium and potassium.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/why-do-tears-taste-salty/article7747001.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


