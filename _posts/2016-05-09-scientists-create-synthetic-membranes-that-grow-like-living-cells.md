---
layout: post
title: "Scientists create synthetic membranes that grow like living cells"
date: 2016-05-09
categories:
author: University of California - San Diego
tags: [Cell membrane,Lipid bilayer,Cell (biology),Artificial cell,Synthetic biology,Life,Phospholipid,Life sciences,Nature,Biology,Molecular biology,Biotechnology,Biochemistry,Chemistry,Cell biology]
---


Chemists and biologists at UC San Diego have succeeded in designing and synthesizing an artificial cell membrane capable of sustaining continual growth, just like a living cell. Their achievement, detailed in a paper published in this week's issue of the Proceedings of the National Academy of Sciences, will allow scientists to more accurately replicate the behavior of living cell membranes, which until now have been modeled only by synthetic cell membranes without the ability to add new phospholipids. 'The membranes we created, though completely synthetic, mimic several features of more complex living organisms, such as the ability to adapt their composition in response to environmental cues,' said Neal Devaraj, an assistant professor of chemistry and biochemistry at UC San Diego who headed the research team, which included scientists from the campus' BioCircuits Institute. 'We developed an artificial cell membrane that continually synthesizes all of the components needed to form additional catalytic membranes.' In this way, they added, 'our system continually transforms simpler, higher-energy building blocks into new artificial membranes.'

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150622154541.htm){:target="_blank" rel="noopener"}


