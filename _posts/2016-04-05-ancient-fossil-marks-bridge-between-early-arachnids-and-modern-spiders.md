---
layout: post
title: "Ancient fossil marks bridge between early arachnids and modern spiders"
date: 2016-04-05
categories:
author: Bob Yirka
tags: [Spider,Uraraneida,Fossil,Arachnid]
---


DOI: 10.1098/rspb.2016.0125  (Phys.org)—A team of researchers from the U.S., Germany and the U.K. has used modern technology to reveal the true nature of an ancient arachnid. In their paper published in Proceedings of the Royal Society B the team describes the ancient creature as almost a spider. The fossil, named Idmonarachne brasieri was dated to approximately 305 million years ago, putting it before the dinosaurs, and it resembles modern spiders in many ways, but is missing one critical part: an organ for spinning silk. Almost a spider: a 305-million-year-old fossil arachnid and spider origins,(2016). We describe a new fossil arachnid, Idmonarachne brasieri gen. et sp.

<hr>

[Visit Link](http://phys.org/news/2016-03-ancient-fossil-bridge-early-arachnids.html){:target="_blank" rel="noopener"}


