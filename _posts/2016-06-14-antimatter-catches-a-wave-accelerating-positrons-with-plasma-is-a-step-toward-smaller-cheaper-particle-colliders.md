---
layout: post
title: "Antimatter catches a wave: Accelerating positrons with plasma is a step toward smaller, cheaper particle colliders"
date: 2016-06-14
categories:
author: "Slac National Accelerator Laboratory"
tags: [Particle accelerator,SLAC National Accelerator Laboratory,Plasma acceleration,Particle physics,Electron,Positron,Large Hadron Collider,Collider,Antimatter,Plasma (physics),Physics,Applied and interdisciplinary physics,Nature,Physical sciences,Experimental physics]
---


The method may help boost the energy and shrink the size of future linear particle colliders - powerful accelerators that could be used to unravel the properties of nature's fundamental building blocks. Future particle colliders will require highly efficient acceleration methods for both electrons and positrons. Previous work showed that the method works efficiently for electrons: When one of FACET's tightly focused bundles of electrons enters an ionized gas, it creates a plasma wake that researchers use to accelerate a trailing second electron bunch. Left: For electrons, a drive bunch (on the right) generates a plasma wake (white area) on which a trailing electron bunch (on the left) gains energy. Credit: W. An/UCLA  In this stable state, about 1 billion positrons gained 5 billion electronvolts of energy over a short distance of only 1.3 meters, said former SLAC researcher Sébastien Corde, the study's first author, who is now at the Ecole Polytechnique in France.

<hr>

[Visit Link](http://phys.org/news/2015-08-antimatter-positrons-plasma-smaller-cheaper.html){:target="_blank" rel="noopener"}


