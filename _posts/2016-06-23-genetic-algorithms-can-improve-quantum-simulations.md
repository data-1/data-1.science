---
layout: post
title: "Genetic algorithms can improve quantum simulations"
date: 2016-06-23
categories:
author: "Lisa Zyga"
tags: [Genetic algorithm,Quantum computing,Algorithm,Simulation,Quantum mechanics,Mathematical optimization,Artificial intelligence,Quantum simulator,Problem solving,Branches of science,Technology,Theoretical computer science,Applied mathematics,Science,Cognitive science,Computing,Computer science]
---


In general, quantum simulations can provide a clearer picture of the dynamics of systems that are impossible to understand using conventional computers due to their high degree of complexity. Besides reducing errors due to decoherence, genetic algorithms can also reduce two other types of errors in digital quantum simulations. One type is the digital error created by the reduced number of steps used for approximating the algorithms. Their adaptability allows for a flexible and clever technique to solve different problems in different quantum technologies and platforms. These techniques could be used to solve problems that require resources unaffordable for present and future digital quantum simulations and gate-based quantum computing, by reducing and optimizing them, Solano said.

<hr>

[Visit Link](http://phys.org/news/2016-06-genetic-algorithms-quantum-simulations.html){:target="_blank" rel="noopener"}


