---
layout: post
title: "Singapore Will Stop Adding New Cars to Its Roads in 2018"
date: 2017-10-24
categories:
author:  
tags: [Car,Vehicle,Transport,Economy]
---


Limited Space in Singapore  The city-state of Singapore has announced a plan to reduce the vehicle growth rate from 0.25 percent per year to 0 percent for cars and motorcycles. The permits are auctioned off. The growth rate for commercial vehicles will remain at 0.25 percent. Also, halting the expansion of the roads system in the country will prevent further transportation development of natural spaces. The new restrictions will not impact the number of permits available.

<hr>

[Visit Link](https://futurism.com/singapore-will-stop-adding-new-cars-to-its-roads-in-2018/){:target="_blank" rel="noopener"}


