---
layout: post
title: "Antarctic Peninsula ice flow"
date: 2016-05-12
categories:
author:  
tags: [Ice sheet,Copernicus Programme,Ice shelf,Antarctica,Ice-sheet dynamics,Antarctic ice sheet,Glacier,Sentinel-1A,Ice,Earth sciences,Glaciology,Physical geography,Earth phenomena,Cryosphere,Geography,Hydrology,Hydrography]
---


Successive radar images captured by the Copernicus Sentinel-1A satellite during December 2014 – March 2016 were used to create this spectacular map showing how fast the ice flows on the Antarctic Peninsula. The map was constructed by tracking the movement of ice features in pairs of radar images taken 12 days apart. Living Planet Symposium, Abstract. ESA 2016. Wuite, J., Nagler, T., Hetzenecker, M., Blumthaler, U., Rott, H. (2016): Continuous monitoring of Greenland and Antarctic ice sheet velocities using Sentinel-1 SAR.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2016/05/Antarctic_Peninsula_ice_flow){:target="_blank" rel="noopener"}


