---
layout: post
title: "The moons of Saturn"
date: 2016-06-30
categories:
author: "Matt Williams"
tags: [Moons of Saturn,Natural satellite,Saturn,Titan (moon),Rhea (moon),Dione (moon),Rings of Saturn,Ancient astronomy,Planemos,Planets,Outer planets,Bodies of the Solar System,Moons,Astronomy,Planetary science,Astronomical objects known since antiquity,Gas giants,Astronomical objects,Outer space,Space science,Planets of the Solar System,Solar System,Tethys (moon)]
---


Saturn's Inner Large Moons, which orbit within the E Ring (see below), include the larger satellites Mimas, Enceladus, Tethys, and Dione. Large Outer Moons:  The Large Outer Moons, which orbit outside of the Saturn's E Ring, are similar in composition to the Inner Moons – i.e. composed primarily of water ice and rock. The surface of Hyperion is covered with numerous impact craters, most of which are 2 to 10 km in diameter. The moons of Saturn, from left to right: Mimas, Enceladus, Tethys, Dione, Rhea; Titan in the background; Iapetus (top) and Hyperion (bottom). All have prograde orbits that range from 11.1 to 17.9 million km, and from 7 to 40 km in diameter.

<hr>

[Visit Link](http://phys.org/news/2015-09-moons-saturn.html){:target="_blank" rel="noopener"}


