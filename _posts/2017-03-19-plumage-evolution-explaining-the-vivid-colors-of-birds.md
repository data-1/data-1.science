---
layout: post
title: "Plumage evolution: Explaining the vivid colors of birds"
date: 2017-03-19
categories:
author: "Okinawa Institute of Science and Technology Graduate University - OIST"
tags: [Bird,Species,Feather,News aggregator,Evolution,Biogeography,Nature,Biology]
---


During his notable trip to the Galápagos Islands, Charles Darwin collected several mockingbird specimens on different islands in the region. The popularity of these birds leads many to assume that tropical climates are home to a greater proportion of colorful birds than temperate climates. But do birds evolve to become more colorful when they move to the tropics? Next, Friedman used data from satellites to describe the geographical region each species lives in. The pattern is really clear Friedman reports, birds living in the desert tend to be more grey on their backs, while birds living in the forest have evolved to be more of a dark green -- we think they are evolving these colors to match their background.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/11/161104101848.htm){:target="_blank" rel="noopener"}


