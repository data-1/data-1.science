---
layout: post
title: "Unconventional superconductor may be used to create quantum computers of the future"
date: 2018-07-03
categories:
author: "Chalmers University of Technology"
tags: [Superconductivity,Quantum mechanics,Majorana fermion,Topological insulator,Quantum computing,Quantum decoherence,Condensed matter,Physics,Theoretical physics,Applied and interdisciplinary physics,Physical sciences,Science,Condensed matter physics,Scientific theories,Phases of matter,Electromagnetism]
---


With their insensitivity to decoherence what are known as Majorana particles could become stable building blocks of a quantum computer. One track within quantum computer research is therefore to make use of what are known as Majorana particles, which are also called Majorana fermions. Majorana fermions are highly original particles, quite unlike those that make up the materials around us. But when they cooled the component down again later, to routinely repeat some measurements, the situation suddenly changed - the characteristics of the superconducting pairs of electrons varied in different directions. ###  The results were recently published in the scientific journal Nature Communications: Induced unconventional superconductivity on the surface states of Bi2Te3 topological insulator  More about quantum computers and the Majorana particle  A large Quantum computer project in the Wallenberg Quantum Technology Centre is underway at Chalmers University of Technology.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/cuot-usm021618.php){:target="_blank" rel="noopener"}


