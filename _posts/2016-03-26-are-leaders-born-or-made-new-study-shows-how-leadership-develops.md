---
layout: post
title: "Are leaders born or made? New study shows how leadership develops"
date: 2016-03-26
categories:
author: University Of Illinois At Urbana-Champaign
tags: [Leadership,Self-efficacy,Cognition,Education,Cognitive science,Branches of science,Psychological concepts,Behavior modification,Psychology,Behavioural sciences]
---


The new study shows that science is involved in teaching leadership development, Rosch said. If students enter the course with low levels of self-efficacy—saying 'I don't really think of myself as a leader' or 'I'm not confident in my abilities'—they don't increase in being willing and able in 15 weeks, but they make big increases in readiness, he added. This shows us we need to work on readiness so students can make the most of advanced leadership courses. Students who come into the introductory class with leadership readiness saying, I've got this, I'm a leader have a different learning experience. So how do you influence people?

<hr>

[Visit Link](http://phys.org/news331834149.html){:target="_blank" rel="noopener"}


