---
layout: post
title: "Scientists reprogram plants for drought tolerance"
date: 2016-04-12
categories:
author: University of California - Riverside
tags: [Abscisic acid,University of California Riverside,Drought tolerance,Drought,Plant,Biology]
---


UC Riverside-led research in synthetic biology provides a strategy that has reprogrammed plants to consume less water after they are exposed to an agrochemical, opening new doors for crop improvement  RIVERSIDE, Calif. - Crops and other plants are constantly faced with adverse environmental conditions, such as rising temperatures (2014 was the warmest year on record) and lessening fresh water supplies, which lower yield and cost farmers billions of dollars annually. When plants encounter drought, they naturally produce abscisic acid (ABA), a stress hormone that inhibits plant growth and reduces water consumption. In the lab, they used synthetic biological methods to develop a new version of these plants' abscisic acid receptors, engineered to be activated by mandipropamid instead of ABA. The researchers showed that when the reprogrammed plants were sprayed with mandipropamid, the plants effectively survived drought conditions by turning on the abscisic acid pathway, which closed the stomata on their leaves to prevent water loss. The University of California, Riverside is a doctoral research university, a living laboratory for groundbreaking exploration of issues critical to Inland Southern California, the state and communities around the world.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/uoc--srp020315.php){:target="_blank" rel="noopener"}


