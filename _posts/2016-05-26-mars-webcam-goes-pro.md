---
layout: post
title: "Mars Webcam goes pro"
date: 2016-05-26
categories:
author:  
tags: [Mars Express,Mars,Planetary science,Terrestrial planets,Astronomical objects known since antiquity,Discovery and exploration of the Solar System,Space exploration,Spacecraft,Science,Outer space,Planets of the Solar System,Space science,Spaceflight,Astronomy,Astronautics,Solar System,Flight]
---


In 2007, ESA’s flight controllers switched it back on to see if it could still be used, possibly for education or science outreach, without interfering with routine operations or the mission’s prime science investigations. Enthusiasts have downloaded, shared and processed images to enhance certain features or boost clarity. Millions of views The page now features over 19 000 images that have been viewed over two million times. Mars Express Project Scientist Dmitri Titov is delighted that the camera is opening up a new range of investigations at Mars: “Cloud tracking and dust storm monitoring, for example, are significant topics in the planetary community, and it will allow us to extend Mars Express science ‘into the atmosphere’, filling a gap in the spacecraft’s science portfolio. “Thanks to the outreach, education and social media experience, we’ve long suspected that it has intrinsic science value, and now this research group in Spain is keen on joining us in exploiting these images on a professional level.” “This new research will support the main Mars Express investigations and significantly increase the value of our 13 year-old mission.” The camera’s images will continue to be made available to the public as they have been.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Operations/Mars_Webcam_goes_pro){:target="_blank" rel="noopener"}


