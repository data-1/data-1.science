---
layout: post
title: "Researchers attempt to uncover the origins of water's unusual properties"
date: 2016-01-29
categories:
author: Lisa Zyga
tags: [Water,Liquid,Entropy,Ice,Heat,Phase (matter),Spacetime,SLAC National Accelerator Laboratory,Heat capacity,Phase diagram,Molecule,Time,Temperature,Laser,Homogeneity and heterogeneity,Hydrogen bond,Physical sciences,Nature,Applied and interdisciplinary physics,Chemistry,Physical chemistry,Physics]
---


However, above the critical point in a funnel-like region, water is constantly fluctuating between these two structures, and these structural fluctuations are what give rise to many of water’s anomalous properties. Pettersson from Stockholm University have pulled together the results from dozens of papers published over the past several years that have investigated water's molecular structure, often with the help of cutting-edge experimental tools and simulations. The researchers propose that, in the pressure (P) and temperature (T) region where water exhibits its anomalous behaviors (the funnel-like region in the P-T phase diagram above), water coexists in two different types of structures: a highly ordered, low-density structure with strong hydrogen bonds, and a somewhat smashed, high-density structure with distorted hydrogen bonds. In these regions, water's structure is homogeneous, existing as only the low-density structure below the funnel-like region and only the high-density structure above the funnel-like region. It's extremely challenging to cool liquid water to these cold temperatures quickly enough so that it can be probed, even for a moment, before it turns to solid ice.

<hr>

[Visit Link](http://phys.org/news/2016-01-uncover-unusual-properties.html){:target="_blank" rel="noopener"}


