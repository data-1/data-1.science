---
layout: post
title: "Russian scientists make progress on secret of eternal life"
date: 2015-09-30
categories:
author: "$author"  
tags: [Bacteria,Immortality,Gene,Woolly mammoth,Sakha,Siberia,Permafrost,Kraken,Archaeology,Life]
---


This work was ongoing for several years and it finished at the end of last year. We need to use the fact that such bacteria were found in our permafrost. I believe that this bacteria could be very useful.' 'Now we have applied for a grant to conduct further research, especially on human blood cells, and we hope that we will get it, because the research is extremely promising.' 'We found our bacteria in deeper, more ancient layers of permafrost, significantly lower that the layers where mammoths were found,' said Dr Brouchkov.

<hr>

[Visit Link](http://www.ancient-origins.net/news-science-space/russian-scientists-make-progress-secret-eternal-life-003917){:target="_blank" rel="noopener"}


