---
layout: post
title: "Microformats turn 9 years old [LWN.net]"
date: 2014-06-25
categories:
author: Posted June
tags: [Microformat,RDFa,Software engineering,Hypertext,Web development,Software development,World Wide Web,Technology,Computing]
---


[Development] Posted Jun 20, 2014 22:44 UTC (Fri) by n8willis  At his blog, Tantek Çelik writes about the ninth birthday of the microformats effort, which seeks to express semantic information in web pages through the use of attribute names within HTML elements, in contrast to comparatively heavyweight schemes like RDFa and Microdata. Çelik notes that the community-driven process of microformats' development seems to have enabled its survival. Looking back nine years ago, none of the other alternatives promoted in the 2000s (even by big companies like Google and Yahoo) survive to this day in any meaningful way , he says. Large companies tend to promote more complex solutions, perhaps because they can afford the staff, time, and other resources to develop and support complex solutions. Such approaches fundamentally lack empathy for independent developers and designers, who don't have time to keep up with all the complexity.

<hr>

[Visit Link](http://lwn.net/Articles/603039/rss){:target="_blank" rel="noopener"}


