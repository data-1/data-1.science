---
layout: post
title: "Researchers use single molecule of DNA to create world's smallest diode"
date: 2016-04-10
categories:
author: University of Georgia
tags: [Molecule,DNA,Diode,Nanotechnology,Integrated circuit,Electronics,Silicon,Chemistry,Ben-Gurion University of the Negev,Symmetry,University of Georgia,Materials science,Applied and interdisciplinary physics,Electromagnetism,Electricity,Physical sciences,Materials,Electrical engineering,Technology]
---


The finding may eventually lead to smaller, more powerful and more advanced electronic devices, according to the study's lead author, Bingqian Xu. For 50 years, we have been able to place more and more computing power onto smaller and smaller chips, but we are now pushing the physical limits of silicon, said Xu, an associate professor in the UGA College of Engineering and an adjunct professor in chemistry and physics. He says DNA's predictability, diversity and programmability make it a leading candidate for the design of functional electronic devices using single molecules. This finding is quite counterintuitive because the molecular structure is still seemingly symmetrical after coralyne intercalation, Xu said. Our discovery can lead to progress in the design and construction of nanoscale electronic elements that are at least 1,000 times smaller than current components, Xu said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/uog-rus040416.php){:target="_blank" rel="noopener"}


