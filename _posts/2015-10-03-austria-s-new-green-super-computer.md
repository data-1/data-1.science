---
layout: post
title: "Austria's new green super computer"
date: 2015-10-03
categories:
author: University Of Vienna
tags: [Supercomputer,Computing,Vienna,Science,Research,Branches of science,Technology]
---


Phase VSC-3 (Vienna Scientific Cluster 3) offers not only impressive computing power, but also serious energy efficiency. A total of eight Austrian universities are participating in the project. Austria now has the very latest infrastructure in a high-tech area that will serve as the basis for new findings in both the natural sciences and technical fields, said Reinhold Mitterlehner, Federal Minister for Science, Research and Economy. For the universities, the VSC is a showcase for energy efficiency and sustainability; crucial topics for the University of Vienna in terms of its social and ecological responsibilities. This alliance gives us and the participating Austrian universities the computing power necessary for our researchers and opens doors into the relevant European networks, explains Sabine Schindler, Vice Rector for Research at the University of Innsbruck.

<hr>

[Visit Link](http://phys.org/news324635285.html){:target="_blank" rel="noopener"}


