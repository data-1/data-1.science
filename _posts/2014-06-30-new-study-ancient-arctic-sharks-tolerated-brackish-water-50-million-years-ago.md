---
layout: post
title: "New study: Ancient Arctic sharks tolerated brackish water 50 million years ago"
date: 2014-06-30
categories:
author: University Of Colorado At Boulder
tags: [Eocene,Ocean,Shark,Arctic Ocean,Greenhouse effect,Water,Arctic,Brackish water,Fossil,Salinity,Climate,Earth,Hydrology,Environmental science,Oceanography,Earth sciences,Physical geography,Natural environment,Nature]
---


Oxygen isotopes in the teeth indicated sharks living in the Eocene Arctic Ocean roughly 50 million years ago were tolerant of brackish water, unlike their shark relatives living today. The study indicates the Eocene Arctic sand tiger shark, a member of the lamniform group of sharks that includes today's great white, thresher and mako sharks, was thriving in the brackish water of the western Arctic Ocean back then. The ancient sand tiger sharks that lived in the Arctic during the Eocene were very different than sand tiger sharks living in the Atlantic Ocean today. The new findings on Arctic Ocean salinity conditions in the Eocene were calculated in part by comparing ratios of oxygen isotopes locked in ancient shark teeth found in sediments on Banks Island in the Arctic Circle and incorporating the data into a salinity model. Oxygen isotopes in ancient bones and teeth reflect the water animals are living in or drinking, said Kim, a former postdoctoral researcher at the University of Wyoming.

<hr>

[Visit Link](http://phys.org/news323359921.html){:target="_blank" rel="noopener"}


