---
layout: post
title: "Four-dimensional physics in two dimensions"
date: 2018-07-03
categories:
author: "Penn State"
tags: [Dimension,Quantum Hall effect,Photon,Space,Hall effect,Light,Physics,Four-dimensional space,Chemistry,Applied and interdisciplinary physics,Physical sciences,Quantum mechanics,Physical chemistry,Theoretical physics,Electromagnetism,Science]
---


An international team of researchers from Penn State, ETH Zurich in Switzerland, the University of Pittsburgh, and the Holon Institute of Technology in Israel have demonstrated that the behavior of particles of light can be made to match predictions about the four-dimensional version of the quantum Hall effect -- a phenomenon that has been at the root of three Nobel Prizes in physics -- in a two-dimensional array of waveguides. But, we have now shown that four-dimensional quantum Hall physics can be emulated using photons -- particles of light -- flowing through an intricately structured piece of glass -- a waveguide array. This quantization of conductance, first described in two-dimensions, cannot be observed in an ordinary three-dimensional material, but in 2000, it was shown theoretically that a similar quantization could be observed in four spatial dimensions. By encoding two extra synthetic dimensions into the complex geometric structure of the waveguides, the researchers were able to model the two-dimensional system as having a total of four spatial dimensions. The researchers then measured how light flowed through the device and found that it behaved precisely according to the predictions of the four-dimensional quantum Hall effect.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/ps-fpi122117.php){:target="_blank" rel="noopener"}


