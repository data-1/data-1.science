---
layout: post
title: "Rosetta fuels debate on origin of Earth’s oceans"
date: 2015-09-03
categories:
author: "$author"   
tags: [Comet,Solar System,Oort cloud,Asteroid,Rosetta (spacecraft),Sun,Formation and evolution of the Solar System,Local Interstellar Cloud,Bodies of the Solar System,Astronomical objects,Planets,Outer space,Space science,Physical sciences,Planetary science,Astronomy]
---


Science & Exploration Rosetta fuels debate on origin of Earth’s oceans 10/12/2014 158669 views 375 likes  ESA’s Rosetta spacecraft has found the water vapour from its target comet to be significantly different to that found on Earth. Of the 11 comets for which measurements have been made, it is only the Jupiter-family Comet 103P/Hartley 2 that was found to match the composition of Earth’s water, in observations made by ESA’s Herschel mission in 2011. By contrast, meteorites originally hailing from asteroids in the Asteroid Belt also match the composition of Earth’s water. “This surprising finding could indicate a diverse origin for the Jupiter-family comets – perhaps they formed over a wider range of distances in the young Solar System than we previously thought,” says Kathrin Altwegg, principal investigator for ROSINA and lead author of the paper reporting the results in the journal Science this week. “Our finding also rules out the idea that Jupiter-family comets contain solely Earth ocean-like water, and adds weight to models that place more emphasis on asteroids as the main delivery mechanism for Earth’s oceans.” “We knew that Rosetta’s in situ analysis of this comet was always going to throw up surprises for the bigger picture of Solar System science, and this outstanding observation certainly adds fuel to the debate about the origin of Earth’s water,” says Matt Taylor, ESA’s Rosetta project scientist.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Rosetta/Rosetta_fuels_debate_on_origin_of_Earth_s_oceans){:target="_blank" rel="noopener"}


