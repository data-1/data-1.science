---
layout: post
title: "In Praise of the Tamed Metaphysicist: Einstein on Reality, Rationality, and the Human Passion for Comprehension"
date: 2016-05-14
categories:
author: Maria Popova
tags: [Metaphysics,Reason,Thought,Science,Concepts in the philosophy of mind,Branches of science,Metaphysics of mind,Philosophy,Philosophical theories,Metaphilosophy,Epistemology,Theoretical philosophy,Epistemological theories,Philosophical movements,Justification (epistemology),Concepts in metaphysics,Cognition,Cognitive science,Internalism and externalism,Empiricism,Western philosophy,Academic discipline interactions]
---


For fifteen years, it has remained free and ad-free and alive thanks to patronage from readers. If this labor has made your own life more livable in the past year (or the past decade), please consider aiding its sustenance with a one-time or loyal donation. Your support makes all the difference. MONTHLY DONATION  ♥ $3 / month ♥ $5 / month ♥ $7 / month ♥ $10 / month ♥ $25 / month  ONE-TIME DONATION  You can also become a spontaneous supporter with a one-time donation in any amount:  BITCOIN DONATION  Partial to Bitcoin? You can beam some bit-love my way: 197usDS6AsL9wDKxtGM6xaWjmR5ejgqem7  Need to cancel a recurring donation?

<hr>

[Visit Link](https://www.brainpickings.org/2016/05/12/einstein-passion-for-comprehension/){:target="_blank" rel="noopener"}


