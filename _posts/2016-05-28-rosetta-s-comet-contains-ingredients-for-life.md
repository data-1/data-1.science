---
layout: post
title: "Rosetta’s comet contains ingredients for life"
date: 2016-05-28
categories:
author: ""
tags: [Comet,Rosetta (spacecraft),Abiogenesis,Life,Coma (cometary),Comet nucleus,Glycine,Cosmic dust,Nature,Astronomy,Space science,Physical sciences,Outer space,Solar System,Planetary science,Astronomical objects,Chemistry,Bodies of the Solar System]
---


They include the amino acid glycine, which is commonly found in proteins, and phosphorus, a key component of DNA and cell membranes. Hints of the simplest amino acid, glycine, were found in samples returned to Earth in 2006 from Comet Wild-2 by NASA’s Stardust mission. “This is the first unambiguous detection of glycine at a comet,” says Kathrin Altwegg, principal investigator of the ROSINA instrument that made the measurements, and lead author of the paper published in Science Advances today. The first detection was made in October 2014 while Rosetta was just 10 km from the comet. Another exciting detection made by Rosetta and described in the paper is of phosphorus, a key element in all known living organisms.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Rosetta/Rosetta_s_comet_contains_ingredients_for_life){:target="_blank" rel="noopener"}


