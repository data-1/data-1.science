---
layout: post
title: "How to debug a C/C++ program with GDB command-line debugger"
date: 2015-01-28
categories:
author: ""      
tags: [GNU Debugger,Breakpoint,Computer program,Debugger,Computer programming,Software,Computers,Technology,Software development,Computer engineering,Computer science,Information technology management,Computing,Systems engineering,Technology development,System software,Software engineering,Information Age]
---


While most people use the debugger included in their favorite IDE, Linux is famous for its powerful command line C/C++ debugger: GDB. Once our breakpoints are set, we can run the program with the run command, or simply:  r [command line arguments if your program takes some]  as most words can be abbreviated in just a letter with gdb . But here we can do:  set var a = 0  And just like any good debugger, we can step with:  step  to run the next line and potentially step into a function. And to finish testing, you can delete a breakpoint with:  delete [line number]  Keep running the program from the current breakpoint with:  continue  and exit GDB with:  quit  To conclude, with GDB, no more praying to compile, no more blood offerings to run, no more printf(test) . What do you think of GDB?

<hr>

[Visit Link](http://xmodulo.com/gdb-command-line-debugger.html){:target="_blank" rel="noopener"}


