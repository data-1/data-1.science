---
layout: post
title: "Revolutionary guitar string rocks the guitar world"
date: 2017-09-22
categories:
author: "University Of St Andrews"
tags: [Vibrato systems for guitar,String (music),Electric guitar,Necked lutes,Guitars,Musical instruments,Guitar family instruments,Irish musical instruments,Celtic musical instruments,String instruments,Music technology]
---


Credit: University of St Andrews  A revolutionary guitar string developed at the University of St Andrews has struck a chord with some of the greats of the music world. Dr Kemp said:  While string sets have been available before with balanced tensions, those strings have featured different sensitivities, with all strings bending through different pitch intervals when the player performs identical movements. With the new strings the properties are controlled to ensure that four of the strings (the plain G and the overwound D, A and low E strings) on a standard electric guitar bend through the same pitch intervals for identical player control changes, whether that be through conventional pitch bends (dragging the strings through a certain distance along a fret to increase tension) or through use of a tremolo/vibrato arm. The clearest demonstration of this is through listening to chords played on these strings during tremolo arm use. The new strings mean that chord bends can be achieved that have not been possible before on standard guitars, such as Fender Stratocasters with standard tremolo units or guitars with the Floyd Rose locking tremolo system.

<hr>

[Visit Link](https://phys.org/news/2017-09-revolutionary-guitar-world.html){:target="_blank" rel="noopener"}


