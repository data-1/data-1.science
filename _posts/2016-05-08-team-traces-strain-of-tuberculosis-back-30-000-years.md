---
layout: post
title: "Team traces strain of tuberculosis back 30,000 years"
date: 2016-05-08
categories:
author: Bob Yirka
tags: [Mycobacterium,Tuberculosis,Mycobacterium tuberculosis,ZiehlNeelsen stain,Medical specialties,Microbiology]
---


The team believes it is likely that the dominance of the Beijing family strain (it is currently responsible for approximately a quarter of all infections worldwide) came about due to the mycobacteria evolving into a hypervirulent strain as a result of the massive population densities that arose following the Han migration and that learning more about its history might lead to discovery of a weakness that might be exploited to help combat the disease. Explore further Research on early fur trade sheds new light on how tuberculosis persists in populations  More information: Southern East Asian origin and coexpansion of Mycobacterium tuberculosis Beijing family with Han Chinese, Tao Luo, PNAS, DOI: 10.1073/pnas.1424063112 Southern East Asian origin and coexpansion of Mycobacterium tuberculosis Beijing family with Han Chinese, Tao Luo,  Abstract  The Beijing family is the most successful genotype of Mycobacterium tuberculosis and responsible for more than a quarter of the global tuberculosis epidemic. We show that the Beijing strains endemic in East Asia are genetically diverse, whereas the globally emerging strains mostly belong to a more homogenous subtype known as modern Beijing. Phylogeographic and coalescent analyses indicate that the Beijing family most likely emerged around 30,000 y ago in southern East Asia, and accompanied the early colonization by modern humans in this area. By combining the genomic data and genotyping result of 1,793 strains from across China, we found the modern Beijing sublineage experienced massive expansions in northern China during the Neolithic era and subsequently spread to other regions following the migration of Han Chinese.

<hr>

[Visit Link](http://phys.org/news353664742.html){:target="_blank" rel="noopener"}


