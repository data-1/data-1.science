---
layout: post
title: "El Nino will be 'substantial' warn Australian scientists"
date: 2015-07-09
categories:
author: ""   
tags: [El Nio,20142016 El Nio event,Drought,El NioSouthern Oscillation,Weather,Meteorology,Extreme weather,Oceanography,Storms,Climate zones,Geographical zones,Tropical meteorology,Tropical cyclones,Natural events,Natural hazards,Atmospheric sciences,Natural disasters,Natural environment,Climate,Branches of meteorology,Tropics,Applied and interdisciplinary physics,Tropical cyclone seasons,Climate patterns,Nature,Physical geography,Earth sciences,Vortices,Earth phenomena,Weather events,Disasters]
---


El Nino weather phenomenon, which can spark deadly and costly climate extremes, is underway in the tropical Pacific for first time in five years, according to Australia's Bureau of Meteorology  Australian scientists Tuesday forecast a substantial El Nino weather phenomenon for 2015, potentially spelling deadly and costly climate extremes, after officially declaring its onset in the tropical Pacific. Certainly the models aren't predicting a weak event. Severe drought  He said this year's pattern could create drier conditions in Indonesia, Papua New Guinea and parts of Southeast Asia. In the past it has caused heavier-than-normal rainfall in the eastern Pacific and South America—raising the spectre of floods and landslides, while the southwest United States and southern Africa tend to be drier. Neil Plummer, the bureau's assistant director for climate information services, said it was often associated with below average rainfall across eastern Australia and warmer temperatures in the southern half over the hottest months.

<hr>

[Visit Link](http://phys.org/news350619127.html){:target="_blank" rel="noopener"}


