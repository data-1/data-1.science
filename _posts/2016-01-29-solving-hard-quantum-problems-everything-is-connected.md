---
layout: post
title: "Solving hard quantum problems: Everything is connected"
date: 2016-01-29
categories:
author: Vienna University of Technology
tags: [Quantum mechanics,Atom,Probability,Applied mathematics,Scientific method,Scientific theories,Physical sciences,Applied and interdisciplinary physics,Science,Theoretical physics,Physics]
---


Quantum Correlations  Quantum physics is a game of luck and randomness. The atoms are photographed, and this is what determines their position. Therefore, the result of every position measurement of any atom depends on the positions of all the other atoms in a mathematically complicated way. First we calculate the probability of the first particle being measured on a certain position. This kind of quantum entanglement makes the problem mathematically extremely challenging.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/vuot-shq012616.php){:target="_blank" rel="noopener"}


