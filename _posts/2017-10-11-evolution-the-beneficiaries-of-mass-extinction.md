---
layout: post
title: "Evolution: The beneficiaries of mass extinction"
date: 2017-10-11
categories:
author: "University of Birmingham"
tags: [Extinction event,Extinction,Biodiversity,PermianTriassic extinction event,Dinosaur,Nature,Biology,Evolutionary biology,Biological evolution,Natural environment,Biogeochemistry,Ecology,Earth sciences]
---


Mass extinctions were followed by periods of low diversity in which certain new species dominated wide regions of the supercontinent Pangaea, reports a new study. The researchers, from the University of Birmingham (UK), North Carolina State University (USA), University of Leeds (UK) and CONICET?Museo Argentino de Ciencias Naturales (Argentina) assessed long-term changes in biodiversity in the supercontinent Pangaea. This period witnessed two mass extinctions and the origins of dinosaurs and many modern vertebrate groups. The team compared the similarity of animal communities from different regions of the globe based both on which species they shared, and how closely related the species from one region were to those from other regions. David Button, a postdoctoral researcher at North Carolina State University and the resident Brimley Scholar at the NC museum of Natural Sciences said, These results show that, after both mass extinctions, biological communities not only lost a large number of species, but also became dominated by widespread, newly-evolving species, leading to low diversity across the globe.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uob-etb100917.php){:target="_blank" rel="noopener"}


