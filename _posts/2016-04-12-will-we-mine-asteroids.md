---
layout: post
title: "Will we mine asteroids?"
date: 2016-04-12
categories:
author: Fraser Cain
tags: [Asteroid mining,Asteroid,Astronomy,Space science,Outer space,Spaceflight,Planetary science,Astronautics,Physical sciences,Nature,Bodies of the Solar System,Solar System]
---


Credit: Periodictableru  It's been said that a single asteroid might be worth trillions of dollars in precious rare metals. Mining here on Earth is hard enough, but actually harvesting material from asteroids in the Solar System sounds almost impossible. If we had Robotic harvesters extract the gold, platinum and iridium off the surface of the space rock and they could send return capsules to Earth. Within a few decades, they should have identified some ideal candidate asteroids for mining, and we get on with the work of mining with Solar System to support our further exploration. Will we eventually mine asteroids to send material back to Earth and support the exploration of space?

<hr>

[Visit Link](http://phys.org/news340015726.html){:target="_blank" rel="noopener"}


