---
layout: post
title: "Caltech physicists uncover novel phase of matter"
date: 2015-10-27
categories:
author: California Institute of Technology 
tags: [Cuprate superconductor,Superconductivity,Materials science,Condensed matter physics,Condensed matter,Materials,Physical chemistry,Phases of matter,Quantum mechanics,Theoretical physics,Chemical product engineering,Science,Physical sciences,Chemistry,Electromagnetism,Applied and interdisciplinary physics,Physics]
---


The building block of this type of order, namely charge, is simply a scalar quantity--that is, it can be described by just a numerical value, or magnitude. Over the last several decades, physicists have developed sophisticated techniques to look for both of these types of phases. But what if the electrons in a material are not ordered in one of those ways? We found that light reflected at the second harmonic frequency revealed a set of symmetries completely different from those of the known crystal structure, whereas this effect was completely absent for light reflected at the fundamental frequency, says Hsieh. Recently, a pseudogap phase also has been observed in Sr2IrO4--and Hsieh's group has found that the multipolar order they have identified exists over a doping and temperature window where the pseudogap is present.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/ciot-cpu102615.php){:target="_blank" rel="noopener"}


