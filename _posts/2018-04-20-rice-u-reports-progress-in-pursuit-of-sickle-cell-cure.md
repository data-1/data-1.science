---
layout: post
title: "Rice U. reports progress in pursuit of sickle cell cure"
date: 2018-04-20
categories:
author: "Rice University"
tags: [CRISPR gene editing,Mutation,Cas9,Rice University,Sickle cell disease,Biology,Genetics,Life sciences,Clinical medicine,Biotechnology,Medical specialties,Health sciences,Medicine]
---


AUSTIN - (Feb. 16, 2018) - Scientists have successfully used gene editing to repair 20 to 40 percent of stem and progenitor cells taken from the peripheral blood of patients with sickle cell disease, according to Rice University bioengineer Gang Bao. Sickle cell disease is caused by a single mutation in the beta-globin gene (in the stem cell's DNA), he said. The idea is to correct that particular mutation, and then stem cells that have the correction would differentiate into normal blood cells, including red blood cells. Bao's lab collaborated with Vivien Sheehan, an assistant professor of pediatrics and hematology at Baylor and a member of the sickle cell program at Texas Children's, to collect stem and progenitor cells (CD34-positive cells) from patients with the disease. Bao pointed out that researchers still don't know whether repairing as much as 40 percent of the cells is enough to cure a patient.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/ru-rur020918.php){:target="_blank" rel="noopener"}


