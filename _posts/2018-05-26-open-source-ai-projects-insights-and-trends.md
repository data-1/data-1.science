---
layout: post
title: "Open Source AI: Projects, Insights, and Trends"
date: 2018-05-26
categories:
author: ""
tags: [Linux,Open source,Linux Foundation,Open-source movement,Software,Technology,Intellectual works,Computing]
---


The Linux Foundation surveyed 16 popular open source AI projects – looking in depth at their histories, codebases, and GitHub contributions. Our conclusion? Building the right community around your open source AI project is critical to its success. We identify several examples of projects that have benefited dramatically from joining an incubator. We discuss several examples of projects that have halted, or may halt, development as contributors flock to other competing projects.

<hr>

[Visit Link](https://www.linuxfoundation.org/publications/open-source-ai-projects-insights-and-trends/){:target="_blank" rel="noopener"}


