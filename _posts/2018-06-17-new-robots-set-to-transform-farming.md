---
layout: post
title: "New robots set to transform farming"
date: 2018-06-17
categories:
author: ""
tags: [Agriculture,Precision agriculture,Unmanned aerial vehicle,Herbicide,Pesticide,Robot,Unmanned ground vehicle,Computing,Technology]
---


Use of robots in precision farming has the potential not only to increase yield, but also to reduce the reliance on fertilisers, herbicides and pesticides through selectively spraying individual plants or through weed removal. Farming with drones and robots  Development of precision farming techniques is a very active area of research, so the goal of Flourish was to bridge the gap between the current and desired capabilities of agricultural robots. The project consortium developed an autonomous farming system where drones and robots work together to monitor the crop and precisely remove weeds. Project members are also trying to facilitate interoperability with other products on the market by utilising standard communication protocols such as the robot operating system. Technological advancements in farming such as those introduced by Flourish will enable farmers to minimise chemical use and produce healthier crops and higher yields.

<hr>

[Visit Link](https://phys.org/news/2018-05-robots-farming.html){:target="_blank" rel="noopener"}


