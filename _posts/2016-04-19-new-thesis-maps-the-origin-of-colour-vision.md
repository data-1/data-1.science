---
layout: post
title: "New thesis maps the origin of colour vision"
date: 2016-04-19
categories:
author: Uppsala University
tags: [Cone cell,Rod cell,Zebrafish,Gene duplication,Evolution,Eye,Photoreceptor cell,Model organism,Visual perception,Gene,Genome,Color,Biology]
---


Then, about 350 million years ago, another doubling of the genome occurred in the ancestor of ray-finned fishes. In his thesis, David Lagman shows that many of the genes that give rise to differences between rods and cones originate from the early doublings of the vertebrate genome. The research group has furthermore studied how these extra genetic copies have changed during evolution in zebrafish, which is commonly used as a model organism in both physiology and developmental biology. Explore further Zebrafish may hold the answer to repairing damaged retinas and returning eyesight to people  More information: Evolution of Vertebrate Vision by Means of Whole Genome Duplications : Zebrafish as a Model for Gene Specialisation. Evolution of Vertebrate Vision by Means of Whole Genome Duplications : Zebrafish as a Model for Gene Specialisation.

<hr>

[Visit Link](http://phys.org/news346576080.html){:target="_blank" rel="noopener"}


