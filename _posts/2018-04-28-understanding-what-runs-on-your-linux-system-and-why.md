---
layout: post
title: "Understanding what runs on your Linux system (and why)"
date: 2018-04-28
categories:
author: "Michael Boelen"
tags: [Daemon (computing),Shell (computing),Process (computing),Linux kernel,Command-line interface,Bash (Unix shell),Executable and Linkable Format,Operating system,Information Age,Computing,Technology,Information technology,Software,Computer architecture,System software,Computer science,Information technology management,Software development,Computer engineering,Operating system technology,Computers,Software engineering]
---


Linux processes and daemons  Each Linux system has a bunch of processes running. What is a process? For example, when you type in a command like ls, it sees this as a known command and executes it. How much memory has the system available? While there are whole books about memory management, you can see this field as the memory it needs to put the program into memory.

<hr>

[Visit Link](https://linux-audit.com/running-processes-and-daemons-on-linux-systems/){:target="_blank" rel="noopener"}


