---
layout: post
title: "Physicists enable one-dimensional atom chains to grow"
date: 2017-10-06
categories:
author: "University Of Erlangen-Nuremberg"
tags: [Magnetism,Antiferromagnetism,Ferromagnetism,Metal,Atom,Chemistry,Physical sciences,Materials,Materials science,Applied and interdisciplinary physics]
---


For the first time, with the help of oxygen, we have managed to produce atom chains that cover the entire iridium surface, are arranged with a regular distance of 0.8 nanometres between each atom and can be up to 500 atoms long, without a single structural fault. This gives the chains their one-dimensional character and their magnetic properties. The calculations made by the working group in Vienna showed that the magnetism of the metals changes in the one-dimensional structure: nickel becomes non-magnetic, cobalt remains ferromagnetic, and iron and manganese become antiferromagnetic, which means that the magnetisation direction changes with each atom. 'What is unique about our process is that, as well as allowing perfect chains of individual materials to grow, it enables chains of alternating metal atoms to form,' Alexander Schneider explains. Potential for new developments in basic research  The discovery of the self-assembling system of perfectly organised magnetic atom chains could lead to new developments in basic research on one-dimensional systems.

<hr>

[Visit Link](http://phys.org/news/2016-08-physicists-enable-one-dimensional-atom-chains.html){:target="_blank" rel="noopener"}


