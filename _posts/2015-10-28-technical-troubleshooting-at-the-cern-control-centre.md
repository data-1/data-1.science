---
layout: post
title: "Technical troubleshooting at the CERN Control Centre"
date: 2015-10-28
categories:
author: Stephanie Hills
tags: [Technology]
---


Credit: Stephanie Hills  If a technical alert goes off at CERN, it will almost certainly show up on one of Chris Wetton's 15 screens in the CERN Control Centre. Wetton is a member of the Technical Infrastructure Operation group, an eight-strong team that works 24/7 to monitor all the services for CERN's accelerator complex. Wetton says that level two alerts are the most common during a technical stop when people are working on the machines. We need to know who is working where, what they're doing, and what alerts they might trigger. When the accelerators are working it's reasonably calm, but at the moment, we're taking 200-280 phone calls every day.

<hr>

[Visit Link](http://phys.org/news326447443.html){:target="_blank" rel="noopener"}


