---
layout: post
title: "Gravitational wave mission selected, planet-hunting mission moves forward"
date: 2017-09-23
categories:
author: ""
tags: [Laser Interferometer Space Antenna,PLATO (spacecraft),Space science,Outer space,Physical sciences,Science,Astronomical objects,Stellar astronomy,Planetary science,Astronomy]
---


Science & Exploration Gravitational wave mission selected, planet-hunting mission moves forward 20/06/2017 16392 views 146 likes  The LISA trio of satellites to detect gravitational waves from space has been selected as the third large-class mission in ESA’s Science programme, while the Plato exoplanet hunter moves into development. LISA concept Furthermore, ESA’s LISA Pathfinder mission has also now demonstrated key technologies needed to detect gravitational waves from space. Planet-hunter adopted Searching for exoplanetary systems In the same meeting Plato – Planetary Transits and Oscillations of stars – has now been adopted in the Science Programme, following its selection in February 2014. Missions of opportunity Proba-3 The Science Programme Committee also agreed on participation in ESA’s Proba-3 technology mission, a pair of satellites that will fly in formation just 150 m apart, with one acting as a blocking disc in front of the Sun, allowing the other to observe the Sun’s faint outer atmosphere in more detail than ever before. ESA will also participate in Japan’s X-ray Astronomy Recovery Mission (XARM), designed to recover the science of the Hitomi satellite that was lost shortly after launch last year.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Gravitational_wave_mission_selected_planet-hunting_mission_moves_forward){:target="_blank" rel="noopener"}


