---
layout: post
title: "2017 was the warmest year on record for the global ocean"
date: 2018-08-18
categories:
author: "Institute of Atmospheric Physics, Chinese Academy of Sciences"
tags: [Ocean heat content,Climate change,Ocean,American Association for the Advancement of Science,Climatology,Climate,Natural environment,Applied and interdisciplinary physics,Atmosphere,Climate variability and change,Atmospheric sciences,Earth phenomena,Nature,Oceanography,Hydrography,Meteorology,Environmental impact,Global natural environment,Environmental issues with fossil fuels,Global environmental issues,Human impact on the environment,Change,Earth sciences,Physical geography]
---


2017 was the warmest year on record for the global ocean according to an updated ocean analysis from Institute of Atmospheric Physics/Chinese Academy of Science (IAP/CAS). The oceans in the upper 2000 m were 1.51 × 1022 J warmer than the second warmest year of 2015 and 19.19 × 1022 J above the 1981-2010 climatological reference period. Owing to its large heat capacity, the ocean accumulates the warming derived from human activities; indeed, more than 90% of Earth's residual heat related to global warming is absorbed by the ocean. The increase in ocean heat of 1.51 × 1022 J in 2017 resulted in a 1.7 mm sea global level rise. Other consequences include declining ocean oxygen, bleaching of coral reefs, and melting sea ice and ice shelves.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/ioap-2wt011718.php){:target="_blank" rel="noopener"}


