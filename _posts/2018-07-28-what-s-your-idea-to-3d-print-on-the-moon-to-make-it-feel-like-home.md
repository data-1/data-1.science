---
layout: post
title: "What’s your idea to 3D print on the Moon – to make it feel like home?"
date: 2018-07-28
categories:
author: ""
tags: [3D printing,Moonbase,Moon,Astronautics,Spaceflight,Space science,Flight,Technology,Outer space]
---


Enabling & Support What’s your idea to 3D print on the Moon – to make it feel like home? Global space agencies are focused on the concept of a lunar base as the next step in human space exploration – and 3D printing represents a key technology for making it happen. What would be the one item you would like to have 3D printed to keep with you in a lunar home from home? 3D printed polymers The winners will have their chosen item printed for real - if technically possible – or else a 3D printed space object based on consortium research. What’s your idea to 3D print on the Moon – to make it feel like home?

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/What_s_your_idea_to_3D_print_on_the_Moon_to_make_it_feel_like_home){:target="_blank" rel="noopener"}


