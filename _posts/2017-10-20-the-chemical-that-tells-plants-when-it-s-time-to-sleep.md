---
layout: post
title: "The chemical that tells plants when it's time to sleep"
date: 2017-10-20
categories:
author: Michelle Mendonca And Dr Daryl Holland, University Of Melbourne
tags: [Circadian rhythm,Ethylene as a plant hormone,Circadian clock,Biology,Physiology]
---


The day-night cycle is regulated by what are known as circadian clocks. So what are circadian clocks? However, we don't know much about how the clock affects ageing in plants, Dr Haydon says. Ethylene gas is a way for plants to communicate with each other, ensuring, for example, the fruit all ripen at the same time, says Dr Haydon. The other clocks  Circadian clocks regulate the 24-hour cycles of plants, but Dr Haydon says there are other 'clocks' that respond to longer time-scales, like seasonal cycles, and these long-term ageing processes, which are heavily influenced by ethylene, could be impacted by disruptions to circadian clocks.

<hr>

[Visit Link](https://phys.org/news/2017-10-chemical.html){:target="_blank" rel="noopener"}


