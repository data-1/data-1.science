---
layout: post
title: "Meet the skyrmions—exotic quasiparticles could revolutionise computing"
date: 2018-08-06
categories:
author: ""
tags: [Skyrmion,Physics,Computing,Energy,Computer data storage,Technology,Magnetism]
---


For most of us, any concerns about computing speed or data storage are usually to make it go faster while storing more. It's about ten years since it became bigger than the carbon footprint of air travel, says Christopher Marrows, professor of condensed matter physics at the University of Leeds, UK. Due to their unique properties, skyrmions are smaller, and more stable and mobile than current computing and magnetic storage devices, making them a better basis for building the next generation of IT devices—plus the energy needed to power them is fractions of what we use now, from ten times less to potentially much more. Skyrmions can pave the way not only to high-density storage, but also new kinds of devices with very little energy consumption. As more of this information becomes available the focus inevitably turns to applications.

<hr>

[Visit Link](https://phys.org/news/2018-06-skyrmionsexotic-quasiparticles-revolutionise.html){:target="_blank" rel="noopener"}


