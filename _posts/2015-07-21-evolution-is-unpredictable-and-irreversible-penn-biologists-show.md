---
layout: post
title: "Evolution is unpredictable and irreversible, Penn biologists show"
date: 2015-07-21
categories:
author: University of Pennsylvania 
tags: [Natural selection,Epistasis,Evolution,Mutation,Biology,Biological evolution,Evolutionary biology,Science,Nature]
---


Using simulations of an evolving protein, they show that the genetic mutations that are accepted by evolution are typically dependent on mutations that came before, and the mutations that are accepted become increasingly difficult to reverse as time goes on. Plotkin explained that this is because purifying selection favors mutations that have a small effect. Thus mutations that are dependent upon earlier mutations will be favored. The way these substitutions occur, since they're highly contingent on what happened before, makes predictions of long-term evolution extremely difficult, Plotkin said. Gould's famous tape of life would be very different if replayed, even more different than Gould might have imagined.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/uop-eiu060815.php){:target="_blank" rel="noopener"}


