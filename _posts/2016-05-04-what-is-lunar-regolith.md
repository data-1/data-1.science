---
layout: post
title: "What is lunar regolith?"
date: 2016-05-04
categories:
author: Matt Williams
tags: [Regolith,Lunar soil,Moon,Mars,Titan (moon),Martian soil,Asteroid,Geology of the Moon,Physical sciences,Astronomical objects known since antiquity,Bodies of the Solar System,Solar System,Planets of the Solar System,Earth sciences,Outer space,Astronomy,Space science,Planetary science]
---


Essentially, you would be seeing the components of what is known as regolith, which is a collection of particles of dust, soil, broken rock, and other materials found here on Earth. Definition:  The term regolith refers to any layer of material covering solid rock, which can come in the form of dust, soil or broken rock. and sand is the presence of organic materials. The moon:  The surface of the moon is covered with a fine powdery material that scientists refer to it as lunar regolith. Lunar dust is also used, but mainly to refer to even finer materials than lunar soil.

<hr>

[Visit Link](http://phys.org/news352110458.html){:target="_blank" rel="noopener"}


