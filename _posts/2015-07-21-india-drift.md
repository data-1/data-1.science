---
layout: post
title: "India drift"
date: 2015-07-21
categories:
author: Massachusetts Institute of Technology 
tags: [Plate tectonics,Subduction,Volcanic arc,Continental drift,Applied and interdisciplinary physics,Global natural environment,Terrestrial planets,Nature,Lithosphere,Geophysics,Planets of the Solar System,Structure of the Earth,Geology,Earth sciences,Physical sciences,Planetary science,Tectonics]
---


Now geologists at MIT have offered up an answer: India was pulled northward by the combination of two subduction zones -- regions in the Earth's mantle where the edge of one tectonic plate sinks under another plate. The group incorporated the measurements they obtained from the Himalayas into their new model, and found that a double subduction system may indeed have driven India to drift at high speed toward Eurasia some 80 million years ago. Squeezing honey  Instead, Royden and Jagoutz believe that India's fast drift may be explained by the subduction of two plates: the tectonic plate carrying India and a second plate in the middle of the Tethys Ocean. The team calculated that plates that are relatively narrow and far apart can squeeze more material out, resulting in faster drift. While others have calculated similar rates for India's drift, this is the first evidence that double subduction acted as the continent's driving force.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/miot-id043015.php){:target="_blank" rel="noopener"}


