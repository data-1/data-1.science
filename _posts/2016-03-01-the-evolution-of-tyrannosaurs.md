---
layout: post
title: "The evolution of tyrannosaurs"
date: 2016-03-01
categories:
author: Public Library Of Science
tags: [Tyrannosauroidea,Tyrannosaurus,Stephen L Brusatte,Dinosaur,Paleontology,Taxa]
---


But there were actually a lot of other dinosaurs similar to T. rex, together forming a group known as tyrannosauroids. Forming hypotheses of relationships like this forms the basis for assessing important evolutionary factors, such as the origins and evolution of particular anatomical features, rates of evolution, diversity, anatomical disparity, and biogeography. Credit: Brusatte and Carr, 2016  In addition to this, Brusatte and Carr decided to approach this with a dual method. Fortunately for Brusatte and Carr, the results of both analyses were quite similar overall, lending support to their conclusions. In the case of tyrannosauroids, there is a 20 million year gap in their fossil record from just before the time when the Western Interior Seaway covered much of North America.

<hr>

[Visit Link](http://phys.org/news/2016-02-evolution-tyrannosaurs.html){:target="_blank" rel="noopener"}


