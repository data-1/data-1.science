---
layout: post
title: "World’s first $9 Linux based computer is shipping now"
date: 2015-09-28
categories:
author: "$author"  
tags: []
---


Chip costs just $9 — although, of course, things like a monitor, an input device, and shipping are not included in that figure. The board is Open Hardware and and is capable of running on any Linux-based operating system. It has a 512MB of RAM and a storage capacity of 4GB. It is more powerful than the Raspberry Pi B+. What Chip offers :  1GHz R8 ARM processor  4GB of internal flash storage  512MB of DDR3 RAM  Bluetooth  Wi-Fi connection  A single full-sized USB port  Microphone input  Headphones output  A composite video output that supports older televisions  A micro USB that supports OTG  The standard version of the Chip is anticipated to start shipping early next year.

<hr>

[Visit Link](http://lxer.com/module/newswire/ext_link.php?rid=219825){:target="_blank" rel="noopener"}


