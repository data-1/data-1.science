---
layout: post
title: "Doctors rely on more than just data for medical decision making"
date: 2018-07-28
categories:
author: "Anne Trafton"
tags: [Physician,Computer science,Sentiment analysis,Research,Artificial intelligence,Medicine,Health,Massachusetts Institute of Technology,Science,Patient,Disease,Engineering,Intelligence,Intensive care medicine,Medical imaging,Branches of science,Health sciences,Cognition,Technology,Health care,Cognitive science]
---


“They’re tapping into something that the machine may not be seeing.”  A new study from MIT computer scientists suggests that human doctors provide a dimension that, as yet, artificial intelligence does not. By analyzing doctors’ written notes on intensive-care-unit patients, the researchers found that the doctors’ “gut feelings” about a particular patient’s condition played a significant role in determining how many tests they ordered for the patient. “That gut feeling is probably informed by a history of experience that doctors have,” Ghassemi says. That’s not because of something mystical, but because she had so much experience dealing with me when I had done something wrong that a simple glance had some data in it.”  To try to reveal whether this kind of intuition plays a role in doctors’ decisions, the researchers performed sentiment analysis of doctors’ written notes. If medical data alone was driving doctors’ decisions, then sentiment would not have any correlation with the number of tests ordered.

<hr>

[Visit Link](http://news.mit.edu/2018/doctors-rely-gut-feelings-decision-making-0720){:target="_blank" rel="noopener"}


