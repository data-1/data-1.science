---
layout: post
title: "Action spectrum of sun skin damage documented"
date: 2016-05-10
categories:
author: Newcastle University
tags: [Sunscreen,Ultraviolet,Human skin,Skin,Chemistry]
---


Scientists at Newcastle University have documented for the first time the DNA damage which can occur to skin across the full range of ultraviolet radiation from the sun providing an invaluable tool for sun-protection and the manufacturers of sunscreen. Testing on human skin cell lines, this study published in The Society for Investigative Dermatology, documents the action spectrum of ultraviolet damage in cells derived from both the upper layer (dermis) and lower layer (epidermis) of the skin. Too many reactive oxygen species can be harmful because they can damage the DNA within our cells. Over time, this can lead to the accumulation of DNA damage, particularly in mitochondria -- the batteries of the cells -- which speed up aging and destroy the skin's supportive fibres, collagen and elastin, leading to wrinkles. The Engineering and Physical Sciences Research Council funded Dr Jennifer Latimer as a CASE PhD Student at Newcastle University for the work alongside a funding award for the collaboration with Proctor and Gamble.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150626095523.htm){:target="_blank" rel="noopener"}


