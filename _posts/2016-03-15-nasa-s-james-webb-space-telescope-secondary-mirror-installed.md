---
layout: post
title: "NASA's James Webb Space Telescope secondary mirror installed"
date: 2016-03-15
categories:
author: NASA/Goddard Space Flight Center
tags: [James Webb Space Telescope,Mirror,Telescope,Astronomy,Telescopes,Astronomical imaging,Electromagnetic radiation,Science,Astronomical observatories,Spaceflight,Outer space,Optics,Space science]
---


The sole secondary mirror that will fly aboard NASA's James Webb Space Telescope was installed onto the telescope at NASA's Goddard Space Flight Center in Greenbelt, Maryland, on March 3, 2016. The secondary mirror is called the secondary mirror because it is the second surface the light from the cosmos hits on its route into the telescope. The James Webb Space Telescope is too large to fit into a rocket in its final shape so engineers have designed it to unfold like origami after its launch. The secondary mirror is supported by three struts that extend out from the large primary mirror. The Webb telescope has 21 mirrors, 18 of which are primary mirror segments working together as one large 21.3-foot (6.5-meter) primary mirror.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/nsfc-njw030716.php){:target="_blank" rel="noopener"}


