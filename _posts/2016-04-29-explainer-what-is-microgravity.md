---
layout: post
title: "Explainer: What is microgravity?"
date: 2016-04-29
categories:
author: Sabrina Gaertner, The Open University, The Conversation
tags: [Weightlessness,Micro-g environment,Free fall,Space science,Physical quantities,Dynamics (mechanics),Celestial mechanics,Motion (physics),Science,Statics,Gravity,Outer space,Mechanics,Classical mechanics,Astronomy]
---


Credit: NASA  It's easy to assume that astronauts float in space because they are far away from the Earth's gravitational force. Gravity is an attractive force, which is always present between two objects that have a mass. It's such a weedy force, however, that we need huge objects such as planets or moons to realise it's there at all. However, we can create environments in which we don't experience the effects of gravity. Credit: NASA  Microgravity research  But why do scientists need microgravity?

<hr>

[Visit Link](http://phys.org/news/2016-04-microgravity.html){:target="_blank" rel="noopener"}


