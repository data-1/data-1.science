---
layout: post
title: "What are extrasolar planets?"
date: 2016-05-03
categories:
author: Matt Williams
tags: [Exoplanet,Kepler space telescope,Methods of detecting exoplanets,Circumstellar habitable zone,Planetary habitability,Stellar astronomy,Outer space,Exoplanetology,Planetary science,Science,Astronomical objects,Astronomy,Physical sciences,Space science]
---


Over time, the methods for detecting these extrasolar planets have improved, and the list of those whose existence has been confirmed has grown accordingly (to almost 2000!) Detection methods  While some exoplanets have been observed directly with telescopes (a process known as Direct Imaging), the vast majority have been detected through indirect methods such as the transit method and the radial-velocity method. The is one means of detecting planets because, as planet's orbit a star, they exert a gravitational influence that causes the star itself to move in its own small orbit around the system's center of mass. This method is effective in determining the existence of multiple transiting planets in one system, but requires that the existence of at least one already be confirmed. Future missions  With the winding down of Kepler's mission, and so many discoveries made within a short period of time, NASA and other federal space agencies plan to continue in the hunt for extrasolar planets.

<hr>

[Visit Link](http://phys.org/news351762176.html){:target="_blank" rel="noopener"}


