---
layout: post
title: "Handheld surgical 'pen' prints human stem cells"
date: 2016-04-10
categories:
author: ARC Centre of Excellence for Electromaterials Science
tags: [Tissue engineering,Surgery,3D printing,Peter Choong,Technology,Clinical medicine,Biotechnology,Medical specialties,Medicine,Biology]
---


The device, developed out of collaboration between ARC Centre of Excellence for Electromaterials Science (ACES) researchers and orthopaedic surgeons at St Vincent's Hospital, Melbourne, is designed to allow surgeons to sculpt customised cartilage implants during surgery. Using a hydrogel bio-ink to carry and support living human stem cells, and a low powered light source to solidify the ink, the pen delivers a cell survival rate in excess of 97%. This makes it extremely difficult to pre-prepare an artificial cartilage implant. Professor Peter Choong, Director of Orthopaedics at St Vincent's Hospital Melbourne, developed the concept with ACES Director Professor Gordon Wallace. The team designed the BioPen with the practical constraints of surgery in mind and fabricated it using 3D printed medical grade plastic and titanium.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/acoe-hs033016.php){:target="_blank" rel="noopener"}


