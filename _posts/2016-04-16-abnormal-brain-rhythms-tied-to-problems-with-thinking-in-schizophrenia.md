---
layout: post
title: "Abnormal brain rhythms tied to problems with thinking in schizophrenia"
date: 2016-04-16
categories:
author: University of California - San Francisco
tags: [Schizophrenia,Mental disorder,Gamma wave,Health,Neuroscience,Clinical medicine,Mental disorders,Mental health,Health sciences,Cognitive science,Diseases and disorders]
---


Converging evidence from many previous studies has implicated a population of neurons in the brain's prefrontal cortex called fast-spiking (FS) interneurons in schizophrenia, but a causal relationship between malfunctioning FS interneurons and cognitive symptoms of the disorder has not yet been firmly established, Sohal said. Electroencephalography (brain wave) studies of normal individuals have revealed that gamma oscillations--neural activity with a regular rhythm between 30 and 120 cycles per second--increase in the front of the brain during cognitive tasks related to the WCST, but these gamma oscillations are blunted in individuals with schizophrenia, said Sohal, the Staglin Family-IMHRO Assistant Professor of Psychiatry at UCSF. Because gamma oscillations emerge from the activity of FS interneurons, and because postmortem studies of the brains of individuals with schizophrenia have shown biochemical abnormalities in FS interneurons, many researchers have concluded these neurons must play some role in the cognitive symptoms of the disorder. ###  In addition to Sohal, Cho, and Rubenstein, researchers participating in the study included former postdoctoral fellow Renee Hoch, PhD; Anthony Lee, a student in USCF's MD/PhD program; and laboratory specialist Tosha Patel. Now celebrating the 150th anniversary of its founding as a medical college, UCSF is dedicated to transforming health worldwide through advanced biomedical research, graduate-level education in the life sciences and health professions, and excellence in patient care.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/uoc--abr030415.php){:target="_blank" rel="noopener"}


