---
layout: post
title: "Writing and deleting magnets with lasers"
date: 2018-07-01
categories:
author: "Helmholtz Association Of German Research Centres"
tags: [Melting,Laser,Thin film,Atom,Chemistry,Materials science,Applied and interdisciplinary physics,Materials,Physical chemistry,Physical sciences,Physics]
---


A strong laser pulse disrupts the arrangement of atoms in an alloy and creates magnetic structures (left). The reversibility of the process opens up new possibilities in the fields of material processing, optical technology, and data storage. The alloy possesses a highly ordered structure, with layers of iron atoms that are separated by aluminum atomic layers. Shooting laser pulses at the same area again—this time at reduced laser intensity—was then used to delete the magnet. Within a nanosecond after melting, and as soon as the temperature drops below the melting point, the solid part of the film starts to regrow, and the atoms rapidly rearrange from the disordered liquid structure to the crystal lattice.

<hr>

[Visit Link](https://phys.org/news/2018-04-deleting-magnets-lasers.html){:target="_blank" rel="noopener"}


