---
layout: post
title: "What is Git?"
date: 2016-07-11
categories:
author: "Seth Kenlon
(Team, Red Hat)"
tags: [Git,Version control,Graphical user interface,Computer file,GitHub,Digital media,Computing,Software,Software development,Software engineering,Technology,Information technology management,Computer science,System software,Intellectual works,Computer engineering,Computers]
---


Why use Git at all? Now you have three versions; the merged copy that you both worked on, the version you changed, and the version your partner has changed. Who should use Git? When should I use Git, and what should I use it for? So you see, Git really is for everyone.

<hr>

[Visit Link](https://opensource.com/life/16/7/what-is-git){:target="_blank" rel="noopener"}


