---
layout: post
title: "X-ray emission from massive stars"
date: 2015-10-27
categories:
author: Harvard-Smithsonian Center For Astrophysics
tags: [Star,Emission spectrum,Star formation,X-ray astronomy,X-ray,Chandra X-ray Observatory,Astronomy,Space science,Physical sciences,Science,Stellar astronomy,Astronomical objects]
---


A composite image of the massive star forming region Cygnus OB2. The image shows X-ray emission from Chandra (blue), infrared from Spitzer (red), and optical data from the Isaac Newton Telescope (orange). Credit: X-ray: NASA/CXC/SAO/J.Drake et al, Infrared: NASA/JPL-Caltech, Optical: Univ. This relatively large sample enabled the scientist to test their models by examining, for example, whether or not there are clear correlations between a star's X-ray strength and its luminosity. The astronomers find for their massive stars that there is a well-defined correlation between the X-ray and total stellar luminosity, with the X-ray strength being about sixteen million times less; indeed, their relation is similar to one previously reported for another massive star-forming region, and favors the first (radiatively driven) kind of shocks.

<hr>

[Visit Link](http://phys.org/news/2015-10-x-ray-emission-massive-stars.html){:target="_blank" rel="noopener"}


