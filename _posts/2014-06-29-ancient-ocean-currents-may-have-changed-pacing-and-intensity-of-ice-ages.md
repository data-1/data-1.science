---
layout: post
title: "Ancient ocean currents may have changed pacing and intensity of ice ages"
date: 2014-06-29
categories:
author: Columbia Climate School 
tags: [Ice age,Ocean,Oceanography,Planetary science,Hydrology,Natural environment,Hydrography,Environmental science,Geology,Physical sciences,Applied and interdisciplinary physics,Earth sciences,Physical geography,Nature]
---


How vigorously those currents moved in the past can be inferred by how much North Atlantic water made it that far, as measured by isotope ratios of the element neodymium bearing the signature of North Atlantic seawater. But they also discovered that at about 950,000 years ago, ocean circulation weakened significantly and stayed weak for 100,000 years; during that period the planet skipped an interglacial—the warm interval between ice-ages--and when the system recovered it entered a new phase of longer, 100,000-year ice age cycles. After this turning point, the deep ocean currents remain weak during ice ages, and the ice ages themselves become colder, they find. Building on that idea, the researchers hypothesize that the advancing ice might have triggered the slowdown in deep ocean currents, leading the oceans to vent less carbon dioxide, which suppressed the interglacial that should have followed. In a 2000 study in Nature, Goldstein and colleagues used neodymium ratios in deep-sea sediment samples to show that ocean circulation slowed during past ice ages.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/teia-aoc062314.php){:target="_blank" rel="noopener"}


