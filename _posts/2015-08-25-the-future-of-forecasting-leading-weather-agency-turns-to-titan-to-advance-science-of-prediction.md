---
layout: post
title: "The future of forecasting: Leading weather agency turns to Titan to advance science of prediction"
date: 2015-08-25
categories:
author: Gregory Scott Jones And Jeremy Rumsey, Oak Ridge National Laboratory
tags: [European Centre for Medium-Range Weather Forecasts,Message Passing Interface,Weather forecasting,Weather,Exascale computing,Titan (supercomputer),Supercomputer,OpenMP,Computers,Technology,Computing,Computer science]
---


Researchers from the European Centre for Medium-Range Weather Forecasts (ECMWF) have used the Titan supercomputer, located at the US Department of Energy's (DOE's) Oak Ridge National Laboratory, to refine their highly lauded weather prediction model, the Integrated Forecasting System (IFS), in hopes of further understanding their future computational needs for more localized weather forecasts. Using Titan, the team was able to run the IFS model at the highest resolution ever (2.5-kilometer global resolution), using Coarray Fortran, an extension of Fortran, to optimize communication between nodes. The ECMWF team now has a roadmap to assist it in planning, particularly in terms of scalability on future architectures. Generally, this arrangement works well for many applications, but for high-resolution weather modeling using the spectral transform method, it can bog down the application, wasting precious runtime—it simply does not easily allow for any overlap of computation and communication. Mozdzynski points out that the use of unstructured meshes in the spherical model with new numerics, along with a number of vendor tools, are all being explored to move the science forward.

<hr>

[Visit Link](http://phys.org/news/2015-08-future-weather-agency-titan-advance.html){:target="_blank" rel="noopener"}


