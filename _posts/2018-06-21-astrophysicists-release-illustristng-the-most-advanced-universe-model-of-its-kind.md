---
layout: post
title: "Astrophysicists release IllustrisTNG, the most advanced universe model of its kind"
date: 2018-06-21
categories:
author: "Simons Foundation"
tags: [Milky Way,Star,Astronomy,Universe,Observable universe,Astrophysics,Galaxy formation and evolution,Physical cosmology,Dark matter,Black hole,Physical sciences,Space science,Physics,Astronomical objects,Nature,Science]
---


Credit: IllustrisTNG collaboration  Modeling a (more) realistic universe  IllustrisTNG is a successor model to the original Illustris simulation developed by the same research team, but it has been updated to include some of the physical processes that play crucial roles in the formation and evolution of galaxies. The interstellar magnetic field strength: blue/purple shows regions of low magnetic energy arranged along filaments of the cosmic web, while orange and white show regions with significant magnetic energy inside halos and galaxies. Credit: The TNG Collaboration  The cosmic web of gas and dark matter predicted by IllustrisTNG produces galaxies quite similar to real galaxies in shape and size. Credit: The TNG Collaboration  Supermassive black holes squelch star formation  In another study, Dylan Nelson, a researcher at MPA, was able to demonstrate the impact of black holes on galaxies. Credit: The TNG Collaboration  New findings for galaxy structure  IllustrisTNG also improves our understanding of the hierarchical structure of galaxy formation.

<hr>

[Visit Link](https://phys.org/news/2018-02-astrophysicists-illustristng-advanced-universe-kind.html){:target="_blank" rel="noopener"}


