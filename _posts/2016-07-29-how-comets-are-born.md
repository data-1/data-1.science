---
layout: post
title: "How comets are born"
date: 2016-07-29
categories:
author: ""
tags: [Comet,Formation and evolution of the Solar System,Solar System,67PChuryumovGerasimenko,Rosetta (spacecraft),Sun,Accretion (astrophysics),Trans-Neptunian object,Local Interstellar Cloud,Nature,Astronomy,Outer space,Bodies of the Solar System,Astronomical objects,Planetary science,Physical sciences,Space science]
---


Science & Exploration How comets are born 28/07/2016 14619 views 135 likes  Detailed analysis of data collected by Rosetta show that comets are the ancient leftovers of early Solar System formation, and not younger fragments resulting from subsequent collisions between other, larger bodies. “Either way, comets have been witness to important Solar System evolution events, and this is why we have made these detailed measurements with Rosetta – along with observations of other comets – to find out which scenario is more likely,” says Matt Taylor, ESA’s Rosetta project scientist. Structures and features on different size scales observed by Rosetta’s cameras provide further information on how this growth may have taken place. “While larger TNOs in the outer reaches of the Solar System appear to have been heated by short-lived radioactive substances, comets don’t seem to show similar signs of thermal processing. Around three million years into the Solar System’s history, gas had disappeared from the solar nebula, only leaving solid material behind.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Rosetta/How_comets_are_born){:target="_blank" rel="noopener"}


