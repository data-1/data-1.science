---
layout: post
title: "Water molecules favor negative charges"
date: 2015-10-03
categories:
author: Sarah Perrin, Ecole Polytechnique Federale De Lausanne
tags: [Ion,Molecule,Electric charge,Water,Chemical bond,Chemical substance,Hydrogen,Materials,Applied and interdisciplinary physics,Physical chemistry,Physical sciences,Chemistry,Atomic molecular and optical physics,Nature]
---


(Phys.org) —In the presence of charged substances, H 2 O molecules favor associating with elements with a negative electrical charge rather than a positive electric charge. EPFL researchers have published a study on the subject that could provide new insights on the processes of cell formation. Some say that in life, it's better to be positive… for charges immersed in water this may not be true. Their interactions with the electrically neutral water molecules were, however, very different depending on whether they were positively or negatively charged. Thus, they get much more hydrated and their effects, in particular on the orientation and alignment of water molecules at the interface between the two substances, were stronger and more stable.

<hr>

[Visit Link](http://phys.org/news324713915.html){:target="_blank" rel="noopener"}


