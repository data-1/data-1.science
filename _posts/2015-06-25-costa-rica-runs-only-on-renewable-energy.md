---
layout: post
title: "Costa Rica runs only on renewable energy"
date: 2015-06-25
categories:
author: Elizabeth Matsangou, Tuesday, March
tags: [Sustainable energy,Hydroelectricity,Renewable energy,Physical quantities,Sustainable technologies,Energy and the environment,Natural environment,Climate change mitigation,Economy and the environment,Economy,Environmental technology,Renewable resources,Natural resources,Environment,Energy development,Sustainable development,Economic development,Sustainability,Climate change,Nature,Energy]
---


In its latest triumph to protect the environment, Costa Rica has been powered completely by hydropower for over 75 consecutive days  Costa Rica is setting a high bar for environmental awareness, not only for the region, but also for the globe. Recently the state-run Costa Rican Electricity Institute (ICE) released a statement announcing that since the beginning of the year, the nation has relied solely on environmentally-friendly energy. As a result of heavy rainfall, Costa Rica has not needed to use fossil fuels to fulfil its electricity needs. According to the Green Energy Leaders report published by WWF, “In the Latin American region, [Costa Rica] is the cleanest country in terms of a range of factors: energy consumption per unit of GDP; carbon intensity in energy conversion processes; impacts over air and water pollution related to energy production; and amount of emissions for electricity produced.”  Costa Rica has been making strides in the field of renewable energy, with this milestone being the latest in a string of achievements for reducing its carbon impact. Enhancing geothermal energy is an important step in Costa Rica’s plan to use 100 percent renewable energy by 2021, as droughts could hinder this objective.

<hr>

[Visit Link](http://www.theneweconomy.com/home/costa-rica-runs-only-on-renewable-energy){:target="_blank" rel="noopener"}


