---
layout: post
title: "Science instruments of NASA's James Webb Space Telescope successfully installed"
date: 2016-05-26
categories:
author: NASA/Goddard Space Flight Center
tags: [James Webb Space Telescope,Goddard Space Flight Center,European Space Agency,Spacecraft,Spaceflight,Outer space,Astronomy,Space science,Science,Astronautics,Space exploration,Space programs,Flight,Space agencies,Space program of the United States,Space research,NASA,Space vehicles,Astronomy organizations]
---


With surgical precision, two dozen engineers and technicians successfully installed the package of science instruments of the James Webb Space Telescope into the telescope structure. The package is the collection of cameras and spectrographs that will record the light collected by Webb's giant golden mirror. Our personnel were navigating a very tight space with very valuable hardware, said Jamie Dunn, ISIM Manager (ISIM stands for 'Integrated Science Instrument Module'). This is a tremendous accomplishment for our worldwide team, said John Mather, James Webb Space Telescope Project Scientist and Nobel Laureate. The James Webb Space Telescope is the scientific successor to NASA's Hubble Space Telescope.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/nsfc-sio052416.php){:target="_blank" rel="noopener"}


