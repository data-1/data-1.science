---
layout: post
title: "Over 80% of future deforestation confined to just 11 places"
date: 2016-04-24
categories:
author:  
tags: [Deforestation,Ecological footprint,Forest,Agriculture,European Union,Sustainability,Environmental social science,Environment,Economy,Natural resources,Nature,Natural environment]
---


Up to 170 million hectares of forest could be lost between 2010 and 2030 in these deforestation fronts if current trends continue, according to findings in the latest chapter of WWF's Living Forests Report series. Credit: WWF  Living Forests Report: Saving Forests at Risk examines where most deforestation is likely in the near term, the main causes and solutions for reversing the projected trends. They require solutions that look at the whole landscape, says Taylor. A main contributor is the agriculture sector through its consumption of oil crops, such as soy and palm oil and their derived products as well as meat consumption. We believe the EU should take action now, to match these efforts with policies and regulatory measures, boosting the pledges made, holding companies accountable for implementing their commitments, and raisinge the environmental, social and human rights standards for all companies.

<hr>

[Visit Link](http://phys.org/news349426248.html){:target="_blank" rel="noopener"}


