---
layout: post
title: "Breakthrough method predicts resistance to antibiotics under development"
date: 2018-06-17
categories:
author: "Dora Bokor, Hungarian Academy Of Sciences"
tags: [Antimicrobial resistance,Antibiotic,Evolution,Directed evolution,Mutation,Synthetic biology,Genomics,Genetics,Biochemistry,Life sciences,Biotechnology,Biology]
---


Given the ever-increasing threat of antibiotic resistance, European researchers have developed a method to rapidly test new antibiotic candidates for their potential to be hit by emerging resistance. The high-throughput mutagenesis method, dubbed DIvERGE, reveals probable resistance within days, making it a fast and cost-effective tool to guide antibiotic development. Although the exact probability and the time-frame of resistance emergence require further analyses and should consider other factors as well, this ability to rapidly analyze potential antimicrobial resistance (AMR) processes could be a powerful strategy to guide drug development. As a proof of their concept, the research group from the Biological Research Centre of the Hungarian Academy of Sciences tested the assay in several bacterial species and against various antibiotics, including two widely used drugs, trimethoprim and ciprofloxacin, and a drug candidate that is under clinical development. Therefore, DIvERGE could allow pharmaceutical companies to expand the time-frame until potential loss of effectiveness and make antibiotic development more profitable.

<hr>

[Visit Link](https://phys.org/news/2018-06-breakthrough-method-resistance-antibiotics.html){:target="_blank" rel="noopener"}


