---
layout: post
title: "Fingerprinting the formation of giant planets"
date: 2015-10-03
categories:
author: Canada-France-Hawaii Telescope
tags: [Star,16 Cygni,Planetary core,Planet,Giant planet,Nebular hypothesis,Jupiter,Accretion (astrophysics),Physical sciences,Astronomical objects,Planetary science,Stellar astronomy,Outer space,Space science,Planets,Astronomy]
---


Difference in chemical composition between the stars 16 Cyg A and 16 Cyg B, versus the condensation temperature of the elements in the proto-planetary nebula. In other words, star 16 Cyg B, the host star of a giant planet, is deficient in all chemical elements, especially in the refractory elements (those with high condensation temperatures and that form dust grains more easily), suggesting evidence of a rocky core in the giant planet 16 Cyg Bb. For the first time, astronomers have detected evidence of this rocky core, the first step in the formation of a giant planet like our own Jupiter. By decomposing the light from the two stars into their basic components and looking at the difference between the two stars, the astronomers were able to detect signatures left from the planet formation process on 16 Cygni B. The second fingerprint is that on top of an overall deficiency of all analyzed elements in 16 Cygni B, this star has a systematic deficiency in the refractory elements such as iron, aluminum, nickel, magnesium, scandium, and silicon.

<hr>

[Visit Link](http://phys.org/news324884769.html){:target="_blank" rel="noopener"}


