---
layout: post
title: "Scientists engineer animals with ancient genes to test causes of evolution"
date: 2017-03-22
categories:
author: "University of Chicago Medical Center"
tags: [Evolution,Alcohol dehydrogenase,Adaptation,Drosophila melanogaster,Natural selection,Genetics,Genetically modified organism,Gene,Evolutionary biology,Biology,Ethanol,Ecology,Organism,Genetically modified animal,Biological evolution,Nature,Science,Life sciences]
---


Scientists at the University of Chicago have created the first genetically modified animals containing reconstructed ancient genes, which they used to test the evolutionary effects of genetic changes that happened in the deep past on the animals' biology and fitness. We realized we could overcome this problem by combining two recently developed methods--statistical reconstruction of ancient gene sequences and engineering of transgenic animals, he said. Siddiq and his advisor, Joe Thornton, PhD, professor of ecology and evolution and human genetics at the University of Chicago, wanted to directly test the effects of a gene's evolution on adaptation. So, the idea that the Adh enzyme was the cause of the fruit fly's adaptation to ethanol became the first accepted case of a specific gene that mediated adaptive evolution of a species. The Adh story was accepted because the ecology, physiology, and the statistical signature of selection all pointed in the same direction.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/uocm-sea011217.php){:target="_blank" rel="noopener"}


