---
layout: post
title: "What Happens When You Put An Intel Chip In LED Lights? Built-in Intelligence For Cities, Industries"
date: 2016-11-22
categories:
author: ""
tags: [General Electric,Technology,Light-emitting diode,Internet of things,Incandescent light bulb,Innovation]
---


Courtesy: iQ by Intel.) They’re bright, use up to 80 percent less energy and can last fifteen times longer than incandescent bulbs, according to GE Now engineers are making smart LED lights with sensors, computing and communication capabilities. Light bulbs are destined to become an essential fixture in the Internet of Things revolution, according to Tony Neal-Graves, vice president of Intel’s industry Internet of Things Group.“You already have a fixed location with power running through it where it’s easy to add sensors, and they’re already in every place you’d want to collect data,” he said in an interview with GE Reports In San Francisco last August , GE showed how intelligent street lamps can play a critical role in making cities, buildings and factories smarter and connected.“We partnered with Intel and put sensors in them to measure humidity and temperature, microphone for sound, camera that can see things,” said John Gordon, chief digital officer at GE’s Current energy division, during a keynote at the Intel Developer Forum.“We get metadata, like the count of people, their speed and direction. Cities could use this for pedestrian flow management and mitigating safety incidents, according to Gordon.He said the real-time data collected from smart lampposts could help people find parking, help with traffic control and keep close tabs on air quality.Months before that demo, the two companies created a smart lighting prototype to solve a challenge in a GE turbine factory in Greenville, South Carolina. Knowing precisely when the part was cool enough to initiate the next attachment sped the process and helped avoid defects, according to Marcus Kennedy, commercial director for industrial and energy solutions division, Intel’s Internet of Things Group.He said GE determined that replacing existing lights with smart LED lights that measure temperature could save the company hundreds of thousands of dollars.“We took the LED light and added an Intel chip on board that processes all of the information coming from the sensor,” said Kennedy.

<hr>

[Visit Link](http://www.gereports.com/happens-put-intel-chip-led-light-built-intelligence/){:target="_blank" rel="noopener"}


