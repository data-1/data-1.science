---
layout: post
title: "Applying machine learning to the universe's mysteries"
date: 2018-07-14
categories:
author: "DOE/Lawrence Berkeley National Laboratory"
tags: [Quarkgluon plasma,Nuclear physics,Particle physics,Matter,Physical sciences,Science,Physics]
---


The team fed thousands of images from simulated high-energy particle collisions to train computer networks to identify important features. Powerful machine learning algorithms allow these networks to improve in their analysis as they process more images. We are trying to learn about the most important properties of the quark-gluon plasma, said Xin-Nian Wang, a nuclear physicist in the Nuclear Science Division at Berkeley Lab who is a member of the team. They used computing resources at Berkeley Lab's National Energy Research Scientific Computing Center (NERSC) in their study, with most of the computing work focused at GPU clusters at GSI in Germany and Central China Normal University in China. The University of California manages Berkeley Lab for the U.S. Department of Energy's Office of Science.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/dbnl-aml012918.php){:target="_blank" rel="noopener"}


