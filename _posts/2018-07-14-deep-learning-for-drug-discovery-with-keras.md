---
layout: post
title: "Deep Learning for Drug Discovery With Keras"
date: 2018-07-14
categories:
author: "Dec."
tags: []
---


machine learning, and deep learning, in particular, have been highly successful in predicting the chemical reactions between candidate compounds and target molecules (see some examples here , here , and here ). goal: drug discovery using deep learning on the merck molecular activity dataset  upon completion of this post, an enterprising data scientist should have trained at least one deep neural network to predict molecular activity on the merck dataset. the convergence of training will also be verified by plotting the loss function of the best cross-validated model, as it is retrained on training data plus holdout data. ## # `cv_model` and `evaluator` both defined in example code to cross validate ## df_predictions = cv_model.transform(df_holdout) r2_score = evaluator.evaluate(df_predictions) print(r2_score) 0.915  last but not least, gpu utilization and decreasing loss function values may be viewed during training by inspecting the spark executor log file output. estimator = distkeras(trainers.adag, {'batch_size': 64, 'communication_window': 3, 'num_epoch': 5, 'num_workers': num_workers}, **param_grid[0]) evaluator = evaluation.regressionevaluator(metricname='r2') cv_estimator = tuning.crossvalidator(estimator=estimator, estimatorparammaps=param_grid, evaluator=evaluator, numfolds=4) cv_model = cv_estimator.fit(df_train)  example code to visualize prediction quality and verify decreasing training loss  the spark ml pipelines api enables us to compute r 2 scores , run a trained model, and obtain predictions and provides us with a reference to the best dist-keras model from our cross-validation above.

<hr>

[Visit Link](https://dzone.com/articles/deep-learning-for-drug-discovery-with-keras-1?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


