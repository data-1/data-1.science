---
layout: post
title: "Why you need to get involved in the geoengineering debate – now"
date: 2017-10-19
categories:
author: Rob Bellamy, The Conversation
tags: [Climate change,Solar geoengineering,Climate engineering,Greenhouse gas,Greenhouse effect,Climate forcing,Physical geography,Atmosphere,Climate,Natural environment,Change,Societal collapse,Earth sciences,Global environmental issues,Environmental issues with fossil fuels,Environmental impact,Climate variability and change]
---


There are many touted methods of engineering the climate. The first is greenhouse gas removal: those ideas that would seek to remove and store carbon dioxide and other greenhouse gases from the atmosphere. The second is solar radiation management: the ideas that would seek to reflect a level of sunlight away from the Earth. How to govern? The key to defining a responsible way to govern geoengineering experiments is accounting for public interests and concerns.

<hr>

[Visit Link](https://phys.org/news/2017-10-involved-geoengineering-debate.html){:target="_blank" rel="noopener"}


