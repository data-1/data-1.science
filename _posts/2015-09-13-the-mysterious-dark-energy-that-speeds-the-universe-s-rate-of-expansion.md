---
layout: post
title: "The mysterious dark energy that speeds the universe's rate of expansion"
date: 2015-09-13
categories:
author: Robert Scherrer, The Conversation
tags: [Dark energy,Universe,Expansion of the universe,Space science,Nature,Astrophysics,Astronomy,Physics,Physical cosmology,Science,Physical sciences,Cosmology]
---


Suppose that you tossed a ball into the air, and instead of being attracted back to the ground, the ball was repelled by the Earth and blasted faster and faster into the sky. Scientists have known for almost a century that the universe is expanding, with all of the galaxies flying apart from each other. So imagine scientists' surprise when two different teams of astronomers discovered, back in 1998, that neither of these behaviors was correct. Different theories for dark energy predict small differences in the way that the expansion of the universe changes with time, so our best hope of probing dark energy seems to come from ever more accurate measurements of the acceleration of the universe, building on that first discovery 17 years ago. Maybe there is no dark energy, and our measurements actually mean that Einstein's theory of gravity is wrong and needs to be fixed.

<hr>

[Visit Link](http://phys.org/news348998540.html){:target="_blank" rel="noopener"}


