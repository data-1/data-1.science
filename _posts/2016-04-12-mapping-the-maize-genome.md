---
layout: post
title: "Mapping the maize genome"
date: 2016-04-12
categories:
author: Botanical Society of America
tags: [Genetic screen,Gene mapping,Biotechnology,Branches of genetics,Biology,Genetics,Life sciences,Molecular biology]
---


In plants, the positional cloning method has been traditionally used in studies of model organisms such as rice and Arabidopsis, providing important insights into plant genetics. The detailed protocol is published in the January issue of Applications in Plant Sciences . With the complete sequence of the maize genome now available, positional cloning can be used to identify genes responsible for traits caused by mutations as well as by natural genetic variation. Applications in Plant Sciences 3(1): 1400092. doi:10.3732/apps.1400092. Applications in Plant Sciences (APPS) is a monthly, peer-reviewed, open access journal focusing on new tools, technologies, and protocols in all areas of the plant sciences.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/bsoa-mtm012015.php){:target="_blank" rel="noopener"}


