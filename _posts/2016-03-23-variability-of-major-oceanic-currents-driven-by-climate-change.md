---
layout: post
title: "Variability of major oceanic currents driven by climate change"
date: 2016-03-23
categories:
author: Christopher Packham
tags: [Atlantic meridional overturning circulation,Ocean current,Thermohaline circulation,Climate change,Ocean,Climate variability and change,Atmospheric circulation,Climate,Atmospheric sciences,Environmental science,Atmosphere,Physical oceanography,Earth phenomena,Natural environment,Hydrology,Earth sciences,Physical geography,Applied and interdisciplinary physics,Hydrography,Nature,Oceanography]
---


For example, one such oceanic conveyor is the Atlantic Meridional Overturning Circulation (AMOC-IV), which drives warm water poleward from the equator, cooling toward the North, and sinking as it becomes more dense at high latitudes. The authors conclude, Our study suggests that AMOC-IV may be significantly weakened in amplitude and shortened in period under future global warming, and that these responses could be caused by strengthened ocean stratification and, in turn, the speedup of baroclinic Rossby waves. More information: Reduced interdecadal variability of Atlantic Meridional Overturning Circulation under global warming. In this study, we analyze the responses of AMOC-IV under various scenarios of future global warming in multiple models and find that AMOC-IV becomes weaker and shorter with enhanced global warming. From the present climate condition to the strongest future warming scenario, on average, the major period of AMOC-IV is shortened from ∼50 y to ∼20 y, and the amplitude is reduced by ∼60%.

<hr>

[Visit Link](http://phys.org/news/2016-03-variability-major-oceanic-currents-driven.html){:target="_blank" rel="noopener"}


