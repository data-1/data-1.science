---
layout: post
title: "Open Cars Kick-Off Conference"
date: 2018-07-16
categories:
author: ""
tags: [Open source,Open Source Initiative,Open-source software,Self-driving car,Car,Open-source-software movement,Technology,Computing,Intellectual works,Open-source movement]
---


Open Cars will be the solution. Open Cars? Open Cars mean open standards built into new cars that will support an aftermarket for autonomous driving and other electronic accessories. However, Open Standards make the development of Open Source autonomous driving systems possible. Paper  The first paper on Open Cars has been published in the Berkeley Technology Law Journal.

<hr>

[Visit Link](https://opensource.org/node/941){:target="_blank" rel="noopener"}


