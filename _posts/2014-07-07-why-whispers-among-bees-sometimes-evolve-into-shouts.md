---
layout: post
title: "Why 'whispers' among bees sometimes evolve into 'shouts'"
date: 2014-07-07
categories:
author: University Of California - San Diego
tags: [Bee]
---


Researchers have in general thought about eavesdropping as a force that makes signals less conspicuous, leading to the evolution of 'whispers' to counter spying. Trigona hyalinata spies that detect food sources marked by Trigona spinipes foragers will often displace T. spinipes from desirable sites in the wild if they can recruit sufficient nestmates. But Lichtenberg found in a controlled field study that the eavesdropping species will avoid desirable sources of food that have been visited frequently by T. spinipes (communicated by the larger number of pheromone markings at the site) to avoid being attacked. Our study is one of the first to investigate what drives the behavior of eavesdroppers collecting information from competitors within the same trophic level, which use the same food resources as the eavesdropper, she adds. The study not only provides information about the evolution of different strategies of animal communication, but suggests how these strategies can affect the ecology of plant communities.

<hr>

[Visit Link](http://phys.org/news323946444.html){:target="_blank" rel="noopener"}


