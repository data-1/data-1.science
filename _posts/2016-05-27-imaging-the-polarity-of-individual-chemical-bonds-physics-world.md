---
layout: post
title: "Imaging the polarity of individual chemical bonds – Physics World"
date: 2016-05-27
categories:
author:  
tags: [Atomic force microscopy,Molecule,Chemistry,Physical sciences,Applied and interdisciplinary physics,Physical chemistry,Physics,Materials science,Atomic molecular and optical physics,Materials,Electromagnetism,Science,Condensed matter physics]
---


Subtle bonds: mapping the bonds for F 12 C 18 Hg 3 and H 12 C 18 Hg 3  A new atomic force microscopy (AFM) technique, which would allow users to precisely detect and map the charge distribution within molecules, has been developed by researchers in Europe. The team has used its method to demonstrate the difference in bond polarity between two structurally identical but chemically distinct molecules. The electrostatic interaction with the surface pushes and pulls on the tip, changing its oscillation frequency. The electrostatic interaction has a different mathematical dependence on distance, from that of the other forces. In all cases, the results were closer than traditional KPFS to theoretical predictions.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/aug/19/imaging-the-polarity-of-individual-chemical-bonds){:target="_blank" rel="noopener"}


