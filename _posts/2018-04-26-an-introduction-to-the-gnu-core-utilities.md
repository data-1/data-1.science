---
layout: post
title: "An introduction to the GNU Core Utilities"
date: 2018-04-26
categories:
author: "David Both
(Correspondent)"
tags: [Unix,GNU,Operating system,Linux,Computer engineering,Computing,Software,System software,Technology,Computer architecture,Computers,Software development,Computer science,Software engineering,Information technology management,Operating system families,Operating system technology]
---


Two sets of utilities—the GNU Core Utilities and util-linux —comprise many of the Linux system administrator's most basic and regularly used tools. This new operating system was much more limited than Multics, as only two users could log in at a time, so it was called Unics. It was becoming impossible to share software with other users and organizations. Those and many of the other commands that are not in the GNU coreutils can be found in the util-linux collection. Summary  These two collections of Linux utilities, the GNU Core Utilities and util-linux , together provide the basic utilities required to administer a Linux system.

<hr>

[Visit Link](https://opensource.com/article/18/4/gnu-core-utilities){:target="_blank" rel="noopener"}


