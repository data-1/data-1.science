---
layout: post
title: "Capturing cell growth in 3-D"
date: 2016-05-27
categories:
author: Rob Matheson, Massachusetts Institute Of Technology
tags: [Microfluidics,Cancer,Clinical medicine,Medicine,Technology,Biology,Life sciences]
---


AIM Biotech's microfluidics device (shown here) has and array of culturing sections, each with three chambers: a middle chamber for hydrogel and any cell type, and two side channels for culturing additional cell types. Designed originally for Kamm's lab, the new commercial device is a plastic chip with three chambers: a middle chamber for hydrogel and any cell type, such as cancer cells or endothelial cells (which line blood vessels), and two side channels for culturing additional cell types. Because of the distance these dishes must be kept from the microscope, Kamm says, it's difficult to capture high-resolution images. Soon, Kamm was using the device in his lab: In a 2011 study, researchers in his group discovered that breast cancer cells can break free from tumors and travel against flows normally present inside the tissue; in a 2012 study, they found that macrophages—a type of white blood cells—were key in helping tumor cells break through blood vessels. It became apparent that, if there's this much interest in these systems and that much need for them, we should set up a company to develop the technology and market it, Kamm says.

<hr>

[Visit Link](http://phys.org/news/2015-08-capturing-cell-growth-d.html){:target="_blank" rel="noopener"}


