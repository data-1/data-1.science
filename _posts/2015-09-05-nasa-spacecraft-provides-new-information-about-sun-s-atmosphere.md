---
layout: post
title: "NASA spacecraft provides new information about sun's atmosphere"
date: 2015-09-05
categories:
author: "$author" 
tags: [Stellar corona,Sun,Space science,Stellar astronomy,Astrophysics,Astronomical objects,Astronomical objects known since antiquity,Physical sciences,Science,Plasma physics,Physical phenomena,Nature,Spaceflight,Astronomy,Solar System,Bodies of the Solar System,Space plasmas,Outer space]
---


NASA's Interface Region Imaging Spectrograph (IRIS) has provided scientists with five new findings into how the sun's atmosphere, or corona, is heated far hotter than its surface, what causes the sun's constant outflow of particles called the solar wind, and what mechanisms accelerate particles that power solar flares. Identifying such sources of unexpected heat can offer deeper understanding of the heating mechanisms throughout the solar atmosphere. A surprise to researchers was the third finding of IRIS observations showing structures resembling mini-tornadoes occurring in solar active regions for the first time. Nanoflares are smaller versions that have long been thought to drive coronal heating. Lockheed Martin designed the IRIS observatory and manages the mission for NASA.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/nsfc-nsp101614.php){:target="_blank" rel="noopener"}


