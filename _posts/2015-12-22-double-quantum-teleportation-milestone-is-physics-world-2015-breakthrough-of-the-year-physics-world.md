---
layout: post
title: "Double quantum-teleportation milestone is Physics World 2015 Breakthrough of the Year – Physics World"
date: 2015-12-22
categories:
author: "$author" 
tags: [Photon,Quantum teleportation,Quantum mechanics,Spin (physics),Quantum entanglement,Physical sciences,Scientific theories,Applied and interdisciplinary physics,Science,Theoretical physics,Physics]
---


The Physics World 2015 Breakthrough of the Year goes to Jian-Wei Pan and Chaoyang Lu of the University of Science and Technology of China in Hefei, for being the first to achieve the simultaneous quantum teleportation of two inherent properties of a fundamental particle – the photon. The first experimental teleportation of the spin of a photon was achieved in 1997, and since then, everything from individual states of atomic spins, coherent light fields and other entities have been transferred. In this experiment, this is a “hyper-entangled” set, where the two particles are simultaneously entangled in both their spin and their OAM (see “Two quantum properties teleported together for first time”). The ability to teleport multiple states simultaneously is essential to fully describe a quantum particle, and is a tentative step towards teleporting anything larger than a quantum particle. This has been done by cooling fermionic atoms to ultracold temperatures, and then using light and magnetic fields to fine-tune the interactions between the atoms.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/dec/11/double-quantum-teleportation-milestone-is-physics-world-2015-breakthrough-of-the-year){:target="_blank" rel="noopener"}


