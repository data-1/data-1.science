---
layout: post
title: "Scientists predict the existence of a new boson: New Madala boson might assist in the understanding of dark matter"
date: 2017-09-21
categories:
author: "Wits University"
tags: [Physics,Standard Model,Large Hadron Collider,Particle physics,CERN,Higgs boson,Fundamental interaction,Universe,Nature,Science,Physical sciences,Theoretical physics]
---


A computer-generated image of the LHC particle accelerator at CERN in the tunnel originally built for the LEP accelerator that was closed in 2000. Credit: CERN  Scientists at the High Energy Physics Group (HEP) of the University of the Witwatersrand in Johannesburg predict the existence of a new boson that might aid in the understanding of Dark Matter in the Universe. Using data from a series of experiments that led to the discovery and first exploration of the Higgs boson at the European Organization for Nuclear Research (CERN) in 2012, the group established what they call the Madala hypothesis, in describing a new boson, named as the Madala boson. However, where the Higgs boson in the Standard Model of Physics only interacts with known matter, the Madala boson interacts with Dark Matter, which makes about 27% of the Universe. Credit: Claudia Marcelloni/CERN  The universe is made of mass and energy. These new particles can explain where the unknown matter in the Universe comes from.

<hr>

[Visit Link](http://phys.org/news/2016-09-scientists-boson-madala-dark.html){:target="_blank" rel="noopener"}


