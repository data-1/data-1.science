---
layout: post
title: "NASA's MAVEN mission observes ups and downs of water escape from Mars"
date: 2017-09-23
categories:
author: "NASA/Goddard Space Flight Center"
tags: [MAVEN,Mars,Atmosphere of Mars,Atmosphere,Atmosphere of Earth,Goddard Space Flight Center,Hydrogen,Physical sciences,Spaceflight,Planets of the Solar System,Astronomical objects known since antiquity,Nature,Astronomical objects,Bodies of the Solar System,Solar System,Planets,Planetary science,Outer space,Astronomy,Space science]
---


After investigating the upper atmosphere of the Red Planet for a full Martian year, NASA's MAVEN mission has determined that the escaping water does not always go gently into space. Hydrogen in Mars' upper atmosphere comes from water vapor in the lower atmosphere. In the most detailed observations of hydrogen loss to date, four of MAVEN's instruments detected the factor-of-10 change in the rate of escape. The university provided two science instruments and leads science operations, as well as education and public outreach, for the mission. The University of California at Berkeley's Space Sciences Laboratory also provided four science instruments for the mission.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/nsfc-nmm101916.php){:target="_blank" rel="noopener"}


