---
layout: post
title: "Insights into the venom composition and evolution of an endoparasitoid wasp by combining proteomic and transcriptomic analyses"
date: 2016-01-29
categories:
author: Yan, State Key Laboratory Of Rice Biology, Ministry Of Agriculture Key Laboratory Of Agricultural Entomology, Institute Of Insect Sciences, Zhejiang University, Hangzhou, Fang, Wang, Liu, Department Of Entomology
tags: [Parasitoid,Proteomics,Parasitism,DNA sequencing,Real-time polymerase chain reaction,Protein,UniGene,Parasitoid wasp,Transcriptomics technologies,Transcriptome,Gene expression,Complementary DNA,Western blot,Venom,SDS-PAGE,Biology,Molecular biology,Life sciences,Biochemistry,Biotechnology]
---


Among all unigenes, 43.73% (17,379) unigenes got matches in the nr database using blastx (e-value < 1e−5). Full size image  We next specifically compared P. puparum venom proteins to venoms reported in N. vitripennis and to our database of venoms from other endoparasitoids (OEP, see methods) using blastp (Supplementary Table S2). Twenty-five venom proteins in P. puparum and 22 in N. vitripennis were shared in P. puparum and N. vitripennis venom only and might be Pteromalinae venom specific. Some venom proteins which were previously reported as unique in N. vitripennis were also detected in P. puparum venom, including venom protein D, G, J, L, O, U and Z. Eighteen venoms were shared among all three data sets and might present a core of venom proteins in parasitoid wasps, including venom allergen, calreticulin, serine protease, acid phosphatase, glucose dehydrogenase, gamma-glutamyltranspeptidase and so on (Supplementary Table S2). Western blotting results also showed several venom proteins were not detected in N. vitripennis venom by the antibodies against P. puparum venom proteins (Fig.

<hr>

[Visit Link](http://www.nature.com/articles/srep19604){:target="_blank" rel="noopener"}


