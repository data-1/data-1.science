---
layout: post
title: "James Webb Space Telescope mirror halfway complete"
date: 2015-12-29
categories:
author: NASA/Goddard Space Flight Center
tags: [James Webb Space Telescope,Goddard Space Flight Center,Telescope,NASA,Astronomical observatories,Spaceflight,Astronomy,Space science,Space program of the United States,Telescopes,Outer space,Science,Astronomical imaging,Astronomical instruments,Scientific instruments,Astronomy organizations,Observational astronomy]
---


This marks the halfway completion point for the James Webb Space Telescope's segmented primary mirror. The James Webb Space Telescope team has been working tirelessly to install all 18 of Webb's mirror segments onto the telescope structure. The mirrors were built by Ball Aerospace & Technologies Corp., in Boulder, Colorado. The installation of the mirrors onto the telescope structure is performed by Harris Corporation of Rochester, New York. The James Webb Space Telescope is the scientific successor to NASA's Hubble Space Telescope.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/nsfc-jws122815.php){:target="_blank" rel="noopener"}


