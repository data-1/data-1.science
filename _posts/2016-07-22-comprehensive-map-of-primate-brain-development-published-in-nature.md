---
layout: post
title: "Comprehensive map of primate brain development published in Nature"
date: 2016-07-22
categories:
author: "Allen Institute"
tags: [Allen Institute for Brain Science,Brain,Cerebral cortex,Development of the nervous system,Neuroscience]
---


Researchers at the Allen Institute for Brain Science have published an in-depth analysis of a comprehensive molecular atlas of brain development in the non-human primate. This analysis uncovers features of the genetic code underlying brain development in our close evolutionary relative, while revealing distinct features of human brain development by comparison. While we know many of the details of gene expression in the adult brain, mapping gene expression across development has been one of the missing links for understanding the genetics of disorders like autism and schizophrenia, says Thomas R. Insel, Ph.D., former Director of the National Institute of Mental Health. ###  The data for the NIH Blueprint Non-Human Primate Atlas are publicly accessible through blueprintnhpatlas.org and with the suite of Allen Institute resources at brain-map.org. About the Allen Institute for Brain Science  The Allen Institute for Brain Science is a division of the Allen Institute (alleninstitute.org), an independent, 501(c)(3) nonprofit medical research organization, and is dedicated to accelerating the understanding of how the human brain works in health and disease.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/ai-cmo071416.php){:target="_blank" rel="noopener"}


