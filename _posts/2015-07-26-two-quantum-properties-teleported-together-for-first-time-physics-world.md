---
layout: post
title: "Two quantum properties teleported together for first time – Physics World"
date: 2015-07-26
categories:
author: Tushna Commissariat
tags: [Quantum teleportation,Quantum entanglement,Quantum mechanics,Photon,Spin (physics),Theoretical physics,Science,Scientific theories,Applied and interdisciplinary physics,Physical sciences,Applied mathematics,Physics]
---


Alice would then interact the unknown quantum state with her half of the entangled particle, measure the combined quantum state and send the result through a classical channel to Bob. The first experimentation teleportation of the spin (or polarization) of a photon took place in 1997. To transfer the two properties requires not only an extra entangled set of particles (the quantum channel), but a “hyper-entangled” set – where the two particles are simultaneously entangled in both their spin and their OAM. (Courtesy: Nature)  The image above represents Pan’s double-teleportation protocol – A is the single photon whose spin and OAM will eventually be teleported to C (one half of the hyper-entangled quantum channel). On the other hand, if the comparative measurement showed that A’s polarization as compared with B differed by 90° (i.e. A and B are orthogonally polarized), then we would rotate C’s field by 90° with respect to that of A to make a perfect transfer once more.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/JO-KA8145-0/two-quantum-properties-teleported-together-for-first-time){:target="_blank" rel="noopener"}


