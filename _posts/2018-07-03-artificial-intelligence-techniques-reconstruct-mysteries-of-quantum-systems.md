---
layout: post
title: "Artificial intelligence techniques reconstruct mysteries of quantum systems"
date: 2018-07-03
categories:
author: "Flatiron Institute"
tags: [Quantum mechanics,Quantum entanglement,Electron,System,Physics,Machine learning,Schrdingers cat,Spin (physics),Branches of science,Cognitive science,Science,Applied mathematics,Technology]
---


For the first time, physicists have demonstrated that machine learning can reconstruct a quantum system based on relatively few experimental measurements. That method works well for simple systems containing only a few particles. Conventional methods, therefore, just aren't feasible for complex quantum systems. For eight electrons, each with spin up or down, the software could accurately reconstruct the system with only around 100 measurements. Besides applications to fundamental research, Carleo says that the lessons the team learned as they blended machine learning with ideas from quantum physics could improve general-purpose applications of artificial intelligence as well.

<hr>

[Visit Link](https://phys.org/news/2018-02-artificial-intelligence-techniques-reconstruct-mysteries.html){:target="_blank" rel="noopener"}


