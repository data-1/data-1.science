---
layout: post
title: "What does a supernova sounds like?"
date: 2014-06-24
categories:
author: Fraser Cain
tags: [Voyager 1,Cosmic dust,Sun,Solar wind,Sky,Nature,Physical sciences,Outer space,Astronomy,Space science]
---


There are no sounds in space. There's not enough stuff in space to be considered a medium for sound to move through. Don't get me wrong there's stuff there. So, even if you did watch the Death Star explode, you couldn't hear it. There's no sound in space, so you can't hear what a supernova sounds like.

<hr>

[Visit Link](http://phys.org/news322816507.html){:target="_blank" rel="noopener"}


