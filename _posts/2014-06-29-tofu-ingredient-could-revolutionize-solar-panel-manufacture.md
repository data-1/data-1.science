---
layout: post
title: "Tofu ingredient could revolutionize solar panel manufacture"
date: 2014-06-29
categories:
author: University of Liverpool 
tags: [Solar cell,Cadmium,Solubility,Chloride,Magnesium chloride,Cadmium telluride photovoltaics,Energy,Physical sciences,Technology,Chemical substances,Nature,Chemistry,Sustainable technologies,Sustainable energy,Materials]
---


The chemical used to make tofu and bath salts could also replace a highly toxic and expensive substance used to make solar cells, a University study published in the journal Nature has revealed. Cadmium chloride is currently a key ingredient in solar cell technology used in millions of solar panels around the world. By applying cadmium chloride to them, this efficiency increases to over 15 percent. Liverpool research, however, has shown that magnesium chloride can achieve the same boost to efficiency. Cadmium chloride is toxic and expensive and we no longer need to use it.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/uol-tic062614.php){:target="_blank" rel="noopener"}


