---
layout: post
title: "Palaeontology: Evolution with teeth"
date: 2017-09-11
categories:
author: "Humphrey, A Researcher In Human Origins At The Natural History Museum, Louise Humphrey, You Can Also Search For This Author In, Author Information, Corresponding Author, Correspondence To"
tags: []
---


Evolution's Bite: A Story of Teeth, Diet, and Human Origins Peter S. Ungar Princeton University Press: 2017. Thus, the teeth of our hominin predecessors in the archaeological and fossil record are a prodigious store of evidence. The teeth of fossil hominins can reveal what our extinct relations were able to eat. To kick off his exploration of human evolution, Ungar analyses the interplay of food and tooth form. But as evidence accumulates that diverse hominin species coexisted from at least 3.5 million years ago until around 40,000 years ago, a future challenge will be to understand how different foraging strategies enabled them to share the landscape.

<hr>

[Visit Link](http://www.nature.com/nature/journal/v545/n7652/full/545026a.html?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


