---
layout: post
title: "CRISPR diversifies: Cut, paste, on, off, and now—evolve"
date: 2018-08-02
categories:
author: "Innovative Genomics Institute"
tags: [Evolution,Cas9,DNA,DNA repair,DNA sequencing,Directed evolution,Gene,Mutation,Nature,Genetics,Molecular genetics,Branches of genetics,Biochemistry,Molecular biology,Life sciences,Biotechnology,Biology]
---


Natural DNA variation is akin to this process – as time goes by, random changes pop up across the genomes of different individual organisms. Step-by-step, how EvolvR targets a specific sequence for diversification. The researchers are especially excited about combining EvolvR with another CRISPR toolbox favorite, high-throughput CRISPR screening. This potent pairing of technologies would let them diversify thousands of different genes in a single experiment, potentially creating brand new functions instead of just turning genes on and off. A single drop of bacteria diversified with EvolvR contains immense diversity.

<hr>

[Visit Link](https://phys.org/news/2018-08-crispr-diversifies-nowevolve.html){:target="_blank" rel="noopener"}


