---
layout: post
title: "De-icing agent remains stable at more than a million atmospheres of pressure"
date: 2016-08-22
categories:
author: "DOE/Lawrence Livermore National Laboratory"
tags: [Lawrence Livermore National Laboratory,Magnesium chloride,Science,Applied and interdisciplinary physics,Chemistry,Physical sciences,Physical chemistry]
---


Lawrence Livermore National Laboratory scientists have combined X-ray diffraction and vibrational spectroscopy measurements together with first-principle calculations to examine the high-pressure structural behavior of magnesium chloride. The team observed an extensive stability of MgCl2 under pressure that contradicts the well-established structural systematics. The immediate technical aim of the study was to provide equations of state (EOS) and structural phase diagrams to improve the confidence of semi-empirical thermochemical calculations predicting the products and performance of detonated chemical formulations. According to previous theoretical studies and the well-established phase diagram of high pressure compounds, MgCl2 should have transformed to a higher coordination number (more dense) and 3D connectivity structure well below 40 GPa through a first order phase transition, said lead author Elissaios (Elis) Stavrou, an LLNL physicist. ###  This research was funded by the Defense Threat Reduction Agency.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/dlnl-dar081216.php){:target="_blank" rel="noopener"}


