---
layout: post
title: "Nanoparticles that stick wounds together"
date: 2017-10-09
categories:
author: "Cornelia Zogg, Swiss Federal Laboratories For Materials Science"
tags: [Adhesive,Bioglass 45S5,Wound,Silicon dioxide,Technology]
---


Now there is an innovative tissue glue, the purpose of which is to help to close wounds optimally in areas where they are difficult to locate or access, and to avoid diffuse and often life-threatening haemorrhages. A combination of glue and bioglass makes the blood clot more quickly at the location of the wound. First study of potential surgical use is published  Doctors helped the Empa research team to develop the tissue glue. In their study, the Empa researchers aimed to fulfil the doctors' wish. He investigated its capacity to glue the intestines together in the Empa textile laboratory in St. Gallen, by using a machine that usually tests the tear strength of materials.

<hr>

[Visit Link](https://phys.org/news/2017-10-nanoparticles-wounds.html){:target="_blank" rel="noopener"}


