---
layout: post
title: "Near-extinct forest giraffe shows resilience in a war zone"
date: 2014-08-16
categories:
author: Cardiff University
tags: [Okapi,Endangered species,Conservation biology,Democratic Republic of the Congo,Genetics,Habitat fragmentation,Biodiversity,Species,Congo Basin,Environmental conservation,Natural environment]
---


Ongoing threat from armed conflict, habitat fragmentation, human encroachment and poaching have in the past year rendered the species endangered, according to a 2013 assessment carried out for the IUCN Red List of Threatened Species. Our research showed that Okapi are both genetically distinct and diverse – not what you might expect from an endangered animal at low numbers, said chief investigator of the study, Dr David Stanton from the School of Biosciences. The data show that Okapi have survived through historic changes in climate, and therefore indicate that the species may be more resilient to future changes. There is a concern however, that much of this genetic diversity will be lost in the near future, due to rapidly declining populations in the wild making efforts to conserve the species, facilitated by the IUCN Species Survival Commission's Giraffe and Okapi Specialist Group, critical. The latest research into the Okapi provides an important contribution to a range-wide Okapi conservation project run in conjunction with ICCN, the Congolese conservation agency, and provides a unique perspective to better understand the diversity of wildlife in the forests of Central Africa, including information on how these forests are likely to have changed throughout ancient history.

<hr>

[Visit Link](http://phys.org/news324537051.html){:target="_blank" rel="noopener"}


