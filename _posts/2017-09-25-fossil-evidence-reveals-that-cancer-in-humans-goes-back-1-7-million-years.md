---
layout: post
title: "Fossil evidence reveals that cancer in humans goes back 1.7 million years"
date: 2017-09-25
categories:
author: "Patrick Randolph-Quinney And Edward John Odes, The Conversation"
tags: [Neoplasm,Cancer,Malignancy,Dinosaur,Bone,Australopithecus sediba,Fossil,Tasmanian devil,Medical imaging,Clinical medicine]
---


These discoveries were made possible by state-of-the-art 3-D imaging techniques. Fossil evidence  Finding any cancer or neoplastic disease in the archaeological record has always been a contentious issue. One was an example of a benign tumour and the other a malignant cancer. The external bone mass might have suggested a benign tumour. Cancer is not simply a disease affecting humans.

<hr>

[Visit Link](http://phys.org/news/2016-09-fossil-evidence-reveals-cancer-humans.html){:target="_blank" rel="noopener"}


