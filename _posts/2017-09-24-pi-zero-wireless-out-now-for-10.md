---
layout: post
title: "Pi Zero Wireless out now for $10"
date: 2017-09-24
categories:
author: "Ben Nuttall
(Alumni)"
tags: [Raspberry Pi,Electronics industry,Equipment,Microcomputers,Classes of computers,Electronics,Information and communications technology,Computing,Computers,Technology,Computer engineering,Computer science,Office equipment,Computer hardware,Computer architecture,Manufactured goods]
---


Today, on the fifth anniversary of the release of the original Raspberry Pi, the Foundation has released Pi Zero W, a Pi Zero with built-in WiFi and Bluetooth, for $10. The latest addition to the Raspberry Pi family has the same specifications as the original Pi Zero:  1GHz single-core ARMv6 CPU (BCM2835)  VideoCore IV GPU, 512MB RAM  Mini HDMI and USB on-the-go ports  Micro USB power  HAT-compatible 40-pin header  Composite video and reset headers  CSI camera connector  Plus, it has an on-board radio chip that provides wireless connectivity for WiFi and Bluetooth. Alex Eames, raspi.tv  If you don't need WiFi and Bluetooth, you can still grab the original Pi Zero for just $5! If you want to connect USB peripherals, you'll need a micro USB adapter or hub. The good thing about this is that you get to choose between a male header, female header, or a right-angled header:  Adafruit, CC BY  The Pi Zero retailers usually sell kits with the extra bits you might need (HDMI adapter, USB adapter and a set of GPIO headers).

<hr>

[Visit Link](https://opensource.com/article/17/2/pi-zero-wireless){:target="_blank" rel="noopener"}


