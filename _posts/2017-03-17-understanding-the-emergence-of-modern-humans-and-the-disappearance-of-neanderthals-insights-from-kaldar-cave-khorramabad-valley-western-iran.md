---
layout: post
title: "Understanding the emergence of modern humans and the disappearance of Neanderthals: Insights from Kaldar Cave (Khorramabad Valley, Western Iran)"
date: 2017-03-17
categories:
author: "Bazgir, Institut Català De Paleoecologia Humana I Evolució Social, Iphes, Zona Educacional, Campus Sescelades Urv, Edif., Àrea De Prehistòria, Universitat Rovira I Virgili. Fac. De Lletres, Avinguda Catalunya, Ollé"
tags: []
---


Full size table  Table 2 Radiocarbon results for charcoal samples from Kaldar Cave. Table 3 Distribution of the faunal taxa identified in Kaldar Cave, Layers 4 and 5. Evidence of anthropogenic activity appears in three ways: cut marks, bone fracturing, and cremations (Supplementary Fig. Full size image  In the Upper Palaeolithic lithic assemblages of Layer 4 (sub-layers 5 & 5II), bladelets dominate (13%), followed by blades (12.5%), retouched tools (5.1%), cortical pieces (4.4%), by-products (3.5%), bladelet cores (1.6%), undetermined cores (1.4%; including a centripetal core), pointed flakes, blanks, and other types of tools (a borer and point; all less than 1%), a blade core (0.2%) and finally a considerable amount of debris (56.4%). Interestingly, our comparative analysis shows that significant differences are present among all the elements from the Middle and Upper Palaeolithic industries of Kaldar Cave.

<hr>

[Visit Link](http://www.nature.com/srep/2017/170302/srep43460/full/srep43460.html?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


