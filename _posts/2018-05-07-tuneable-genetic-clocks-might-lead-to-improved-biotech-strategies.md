---
layout: post
title: "Tuneable genetic 'clocks' might lead to improved biotech strategies"
date: 2018-05-07
categories:
author: "Caroline Brogan, Imperial College London"
tags: [Genetics,Cell (biology),Synthetic biology,Gene,Biotechnology,Life sciences,Biology,Technology]
---


In genetics, oscillators are biological 'clocks' that express certain key genes at regular time intervals. Scientists are increasingly giving genetic oscillators to engineered cells, to help them control the release of drugs, biofuels, and other chemicals. Lead author Dr. Guy-Bart Stan, from Imperial's Department of Bioengineering, said: We have shown that it's possible to independently tune the amplitude and frequency of genetic clocks, giving us greater control over their output. The researchers used computer modelling to unravel the links between amplitude and frequency in current genetic oscillators, and, based on this, proposed new design principles to control amplitude and frequency independently. DOI: 10.1016/j.cels.2018.04.001  Marios Tomazou et al. Computational Re-design of Synthetic Genetic Oscillators for Independent Amplitude and Frequency Modulation, Cell Systems (2018).

<hr>

[Visit Link](https://phys.org/news/2018-05-tuneable-genetic-clocks-biotech-strategies.html){:target="_blank" rel="noopener"}


