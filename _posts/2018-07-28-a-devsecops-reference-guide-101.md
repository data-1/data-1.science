---
layout: post
title: "A DevSecOps Reference Guide 101"
date: 2018-07-28
categories:
author: "Jul."
tags: []
---


DevSecOps is enabling integration of security testing earlier in the software development lifecycle — learn the terms you need to know to understand DevSecOps. Join the DZone community and get the full member experience. A significant shift in the application development process is towards security testing and DevOps. This leads to the conjuring of terms, new and existing, which are often confused and used interchangeably. This article enumerates a few of these terms that are imperative for engineering teams to be aware of from an application security and DevSecOps standpoint.

<hr>

[Visit Link](https://dzone.com/articles/a-devsecops-reference-guide-101?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


