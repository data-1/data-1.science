---
layout: post
title: "Orange Takes Leading Role in 5G with Help of LFN"
date: 2018-05-26
categories:
author: "The Linux Foundation"
tags: [Network function virtualization,Linux Foundation,Computer networking,Computer engineering,Computer science,Technology,Computing,Service industries,Communication,Information and communications technology,Telecommunications]
---


As a leading global telecommunications company, Orange understands the importance of 5G in today’s networks. The company announced 5G trials in France for 2018 and aims to deliver an operational 5G network by 2020. This is no small feat when looking at Orange’s 273M customers worldwide, including 152,000 employees. As open source is a key element of its solution, Orange joined LFN at the Platinum level and is leveraging three key projects to drive its On-Demand Network transformation efforts — ONAP, OPNFV and ODL. Read more about Orange’s participation in LFN and use of open source here.

<hr>

[Visit Link](https://www.linuxfoundation.org/networking-orchestration/orange-takes-leading-role-in-5g-with-help-of-lfn/){:target="_blank" rel="noopener"}


