---
layout: post
title: "Researchers edit plant DNA using mechanism evolved in bacteria"
date: 2016-05-07
categories:
author: University Of Georgia
tags: [Genome editing,CRISPR,Gene,Populus,Biology,Genetics,Biotechnology,Life sciences]
---


Credit: University of Georgia  Researchers at the University of Georgia have used a gene editing tool known as CRISPR/Cas to modify the genome of a tree species for the first time. This is a mechanism that evolved naturally, but we can borrow the bacteria's gene-cutting abilities and use it to edit very specific genes in all kinds of organisms, including plants and animals, said Tsai, who is also director of UGA's Plant Center. Tsai credits her collaborator Thomas Jacobs, a former doctoral student in UGA's Institute of Plant Breeding, Genetics and Genomics, who adapted the CRISPR system for plant genome editing. We thought we knew what genes control lignin and condensed tannin production, and we did target the right genes, but the work showed us that there are other genes with overlapping roles, Tsai said. Explore further Highly efficient CRISPR knock-in in mouse

<hr>

[Visit Link](http://phys.org/news352667561.html){:target="_blank" rel="noopener"}


