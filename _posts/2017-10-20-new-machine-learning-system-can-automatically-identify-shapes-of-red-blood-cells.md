---
layout: post
title: "New machine learning system can automatically identify shapes of red blood cells"
date: 2017-10-20
categories:
author: Public Library Of Science
tags: [Red blood cell,Blood cell,Sickle cell disease,Convolutional neural network,Machine learning,Blood,Deep learning,Internet privacy,Technology]
---


To automate the process of identifying red blood cell shape, Mengjia Xu of Northeastern University, China, and colleagues developed a computational framework that employs a machine-learning tool known as a deep convolutional neural network (CNN). The new framework uses three steps to classify the shapes of red blood cells in microscopic images of blood. We have developed the first deep learning tool that can automatically identify and classify red blood cell alteration, hence providing direct quantitative evidence of the severity of the disease, says study co-author George Karniadakis. The research team plans to further improve their deep CNN tool and test it in other blood diseases that alter the shape and size of red blood cells, such as diabetes and HIV. Explore further Computer models provide new understanding of sickle cell disease

<hr>

[Visit Link](https://phys.org/news/2017-10-machine-automatically-red-blood-cells.html){:target="_blank" rel="noopener"}


