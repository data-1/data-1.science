---
layout: post
title: "Light receptors determine the behavior of flashlight fish"
date: 2018-07-13
categories:
author: "Ruhr-University Bochum"
tags: [Light,Bioluminescence,Photoreceptor cell,Opsin,Splitfin flashlightfish,Fish,Retina,Biologist,News aggregator,Eye,Flashlight]
---


The photoreceptors known as opsins allow the fish to detect light with a specific wavelength. As published on the 11th July 2018 in PLOS ONE the scientists found new opsin variants, which are specialized to detect low intensity blue light in the wavelength range of bioluminescent light emitted by the fish. Two opsin variants to detect blue light  Besides the fact that bioluminescence is a widespread phenomenon in marine environments it is currently not known, how bioluminescence is processed and which physiological and behavioural consequences bioluminescence is evoking in most species. Fish are conditioned during feeding  Next, the research team analysed if Anomalops katoptron uses blue light for behavioural responses. They used a much lower light intensity in comparison to the red flashlight and found that the fish now only reacted to low intensity blue but not red light.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180712100450.htm){:target="_blank" rel="noopener"}


