---
layout: post
title: "Programming human cells to follow sets of logical instructions"
date: 2017-09-10
categories:
author: "Bob Yirka"
tags: [DNA,Protein,Privacy,Cell (biology),Lookup table,Biochemistry,Technology,Molecular biology,Genetics,Life sciences,Biotechnology,Biology]
---


Cutting DNA and sewing it back together allows for programming cells because DNA controls which proteins a cell makes. By cutting and sewing in a certain way, the researchers were able to induce a human kidney cell to produce a fluorescent protein which caused the cell to light up under desired conditions. Another possibility is using the technique to program stem cells to grow into desired tissue. Large-scale design of robust genetic circuits with multiple inputs and outputs for mammalian cells, Nature Biotechnology (2017). We present a robust, general, scalable system, called 'Boolean logic and arithmetic through DNA excision' (BLADE), to engineer genetic circuits with multiple inputs and outputs in mammalian cells with minimal optimization.

<hr>

[Visit Link](https://phys.org/news/2017-03-human-cells-logical.html){:target="_blank" rel="noopener"}


