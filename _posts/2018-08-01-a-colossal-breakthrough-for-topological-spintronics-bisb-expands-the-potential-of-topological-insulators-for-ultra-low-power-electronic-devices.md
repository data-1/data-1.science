---
layout: post
title: "A colossal breakthrough for topological spintronics: BiSb expands the potential of topological insulators for ultra-low-power electronic devices"
date: 2018-08-01
categories:
author: "Tokyo Institute of Technology"
tags: [Bismuth antimonide,Topological insulator,Magnetoresistive RAM,Spintronics,Insulator (electricity),Hall effect,Spin (physics),Electrical resistivity and conductivity,Thin film,Spin Hall effect,Spinorbit interaction,Band gap,Manufacturing,Physical quantities,Electronics,Building engineering,Physical sciences,Phases of matter,Chemistry,Condensed matter,Physics,Chemical product engineering,Technology,Applied and interdisciplinary physics,Condensed matter physics,Electricity,Materials science,Materials,Electromagnetism,Electrical engineering]
---


The achievement represents a big step forward in the development of spin-orbit torque magnetoresistive random-access memory (SOT-MRAM)[3] devices with the potential to replace existing memory technologies. The BiSb thin films achieve a colossal spin Hall angle of approximately 52, conductivity of 2.5 x 105 and spin Hall conductivity of 1.3×107 at room temperature. [2] Topological insulators: Materials with highly electrically conductive surfaces, but which act as insulators on the inside. [4] Spin Hall effect: A Hall effect for spins originating from the coupling of charge and spin. Pure spin currents can be generated via the spin Hall effect.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180731092023.htm){:target="_blank" rel="noopener"}


