---
layout: post
title: "NASA's Fermi discovers the most extreme blazars yet"
date: 2017-09-21
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Blazar,Fermi Gamma-ray Space Telescope,Black hole,Gamma ray,Astrophysics,Space science,Astronomy,Physical phenomena,Nature,Science,Physical sciences,Astronomical objects,Physics]
---


NASA's Fermi Gamma-ray Space Telescope has identified the farthest gamma-ray blazars, a type of galaxy whose intense emissions are powered by supersized black holes. Light from the most distant object began its journey to us when the universe was 1.4 billion years old, or nearly 10 percent of its present age. Astronomers think their high-energy emissions are powered by matter heated and torn apart as it falls from a storage, or accretion, disk toward a supermassive black hole with a million or more times the sun's mass. Blazars appear bright in all forms of light, including gamma rays, the highest-energy light, when one of the jets happens to point almost directly toward us. Previously, the most distant blazars detected by Fermi emitted their light when the universe was about 2.1 billion years old.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/nsfc-nfd013017.php){:target="_blank" rel="noopener"}


