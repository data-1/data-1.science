---
layout: post
title: "Jurassic period saw fastest mammal evolution"
date: 2015-07-20
categories:
author: ""   
tags: []
---


Mammals underwent a rapid ‘burst’ of evolutionary change that reached its peak around the middle of the Jurassic period around 145-200 million years ago, a new Oxford-led study has found. By calculating evolutionary rates across the entire Mesozoic, they show that mammals underwent a rapid ‘burst’ of evolutionary change that reached its peak around the middle of the Jurassic (200-145 million years ago). The team recorded the number of significant changes to body plans or teeth that occurred in mammal lineages every million years. During the mid-Jurassic the frequency of such changes increased to up to 8 changes per million years per lineage, almost ten times that seen at the end of the period. “Once high ecological diversity had evolved, the pace of innovation slowed,” Close added.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/jurassic-period-saw-fastest-mammal-evolution/article7443906.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


