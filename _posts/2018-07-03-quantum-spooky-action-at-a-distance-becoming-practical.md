---
layout: post
title: "Quantum 'spooky action at a distance' becoming practical"
date: 2018-07-03
categories:
author: "Griffith University"
tags: [Quantum entanglement,Photon,Quantum nonlocality,Physics,Quantum mechanics,Science,Theoretical physics,Applied and interdisciplinary physics,Optics,Physical sciences]
---


They demonstrated that the effect, also known as quantum nonlocality, can still be verified even when many of the photons are lost by absorption or scattering as they travel from source to destination through an optical fiber channel. This is a problem for existing quantum nonlocality verification techniques with photons. Every photon lost makes it easier for the eavesdropper to break the security by mimicking entanglement. The team used a different approach - quantum teleportation - to overcome the problem of lost photons. The team aims to integrate their method into quantum networks that are being developed by the Australian Research Council Centre of Excellence for Quantum Computation and Communication Technology, and test it in real-life conditions.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/gu-qa010418.php){:target="_blank" rel="noopener"}


