---
layout: post
title: "Unique feeding mechanism among marine reptiles from the age of dinosaurs"
date: 2015-10-27
categories:
author: Society Of Vertebrate Paleontology
tags: [Cretaceous,Mesozoic,Elasmosaurus,Dinosaur,Reptile,Morturneria]
---


Among the many groups of marine reptiles from the Age of Dinosaurs, elasmosaurs are famous for their necks, which can have up to 76 vertebrae and make up more than half the total length of the animal. These sea dragons attained worldwide distribution and vanished only during the mass extinction at the end of the Cretaceous 66 million years ago. Fossils of the elasmosaur Aristonectes were first reported from the Late Cretaceous of Patagonia in 1941. Recent discoveries in Chile and on Seymour Island (Antarctica) have provided much new information on this elasmosaur and the closely related Morturneria, respectively. F. Robin O'Keefe (Marshall University, Huntington, WV), and his colleagues reported at the 75th Annual Meeting of the Society of Vertebrate Paleontology (Dallas, October 14-17, 2015) that these reptiles employed a unique mode of feeding.

<hr>

[Visit Link](http://phys.org/news/2015-10-unique-mechanism-marine-reptiles-age.html){:target="_blank" rel="noopener"}


