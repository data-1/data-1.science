---
layout: post
title: "The big ethical questions for artificial intelligence in healthcare"
date: 2018-07-18
categories:
author: "Nuffield Council on Bioethics"
tags: [Artificial intelligence,Artificial intelligence in healthcare,Health care,Bioethics,Applied ethics,Health,Medicine,Health sciences]
---


The Nuffield Council on Bioethics examines the current and potential applications of AI in healthcare, and the ethical issues arising from its use, in a new briefing note, Artificial Intelligence (AI) in healthcare and research, published today. The briefing note outlines the ethical issues raised by the use of AI in healthcare, such as:  the potential for AI to make erroneous decisions;  who is responsible when AI is used to support decision-making;  difficulties in validating the outputs of AI systems;  the risk of inherent bias in the data used to train AI systems;  ensuring the security and privacy of potentially sensitive data;  securing public trust in the development and use of AI technology;  effects on people's sense of dignity and social isolation in care situations;  effects on the roles and skill-requirements of healthcare professionals; and  the potential for AI to be used for malicious purposes. ###  Notes to editors  The Nuffield Council on Bioethics is an independent body that has been advising policy makers on ethical issues in bioscience and medicine for more than 25 years. This is the third in a new series of bioethics briefing notes published by the Council. When the Institute becomes fully established, it will examine the ethical and social issues arising from the use of data, algorithms, and artificial intelligence, and ensure they are harnessed for social well-being.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/ncob-tbe051418.php){:target="_blank" rel="noopener"}


