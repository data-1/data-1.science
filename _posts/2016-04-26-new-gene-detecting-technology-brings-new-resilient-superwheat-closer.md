---
layout: post
title: "New gene-detecting technology brings new, resilient superwheat closer"
date: 2016-04-26
categories:
author: John Innes Centre
tags: [Plant disease resistance,Wheat,Biotechnology,Mutation,Research,Gene,Health,Stem rust,Agriculture,Sustainability,John Innes Centre,Science,Biotechnology and Biological Sciences Research Council,Rust (fungus)]
---


Working with fellow scientists at TSL, Dr Brande Wulff from the JIC developed the new technology called 'MutRenSeq' which accurately pinpoints the location of disease resistance genes in large plant genomes and which has reduced the time it takes to clone these genes in wheat from 5 to 10 years down to just two. Using this technology, scientists can very quickly locate resistance genes from crops, clone them and stack multiple resistance genes into one elite variety. Dr Wulff said:  With MutRenSeq we can find the needle in the haystack: we can reduce the complexity of finding resistance genes by zeroing in from 124,000 genes, to just a single candidate gene. The knowledge, resources and trained researchers we generate help global societies address important challenges including providing sufficient and affordable food, making new products for human health and industrial applications, and developing sustainable bio-based manufacturing. The John Innes Centre is strategically funded by the Biotechnology and Biological Sciences Research Council (BBSRC).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/jic-ngt042116.php){:target="_blank" rel="noopener"}


