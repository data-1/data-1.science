---
layout: post
title: "Astronauts board space station for one-year mission (Update)"
date: 2016-04-19
categories:
author: Bydmitry Lovetsky And Jim Heintz
tags: [Scott Kelly (astronaut),Mikhail Kornienko,Gennady Padalka,Soyuz TMA-16M,International Space Station,Astronaut,Baikonur Cosmodrome,Spaceflight technology,Spacecraft,Space industry,Space exploration,Space vehicles,Space program of Russia,Human spaceflight programs,Crewed spacecraft,Flight,Aerospace,Space programs,Life in space,Outer space,Human spaceflight,Astronautics,Spaceflight]
---


Russia's Gennady Padalka is beginning a six-month stay. (AP Photo/Dmitry Lovetsky)  NASA has never flown anyone longer than seven consecutive months. (AP Photo/Dmitry Lovetsky)  Seen through part of the rocket transporter, the Soyuz-FG rocket booster with Soyuz TMA-16M space ship carrying a new crew to the International Space Station, ISS, blasts off at the Russian leased Baikonur cosmodrome, Kazakhstan, Saturday, March 28, 2015. (AP Photo/Dmitry Lovetsky)  British singer Sarah Brightman attends a farewell ceremony of the Expedition 43 crew U.S. Astronaut Scott Kelly, Russian Cosmonauts Gennady Padalka, and Mikhail Kornienko, from the Russian Cosmonaut Hotel at the Russian leased Baikonur cosmodrome, Kazakhstan, Friday, March 27, 2015. (AP Photo/Dmitry Lovetsky, Pool)  British singer Sarah Brightman attends a farewell ceremony of the Expedition 43 crew U.S. Astronaut Scott Kelly, Russian Cosmonauts Gennady Padalka, and Mikhail Kornienko, from the Russian Cosmonaut Hotel at the Russian leased Baikonur cosmodrome, Kazakhstan, Friday, March 27, 2015.

<hr>

[Visit Link](http://phys.org/news346697135.html){:target="_blank" rel="noopener"}


