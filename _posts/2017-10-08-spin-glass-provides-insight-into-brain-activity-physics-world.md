---
layout: post
title: "Spin glass provides insight into brain activity – Physics World"
date: 2017-10-08
categories:
author: ""
tags: [Synapse,Neurotransmitter,Memory,Nervous system,Brain,Spin glass,Neurotransmission,Neuron,Neuroscience,Cognitive science]
---


Neurons are also linked by synapses in a way that is similar to how magnetic spins interact with each other. Synapse strength  When we create a memory, it is stored in our brain as a pattern of neural activity encoded by the strength of the synapses. Mathematical models of the recall of learned memories are based around simulations of binary neurons linked by connections of varying strengths. If you compute the same measure for a spin glass you will have zero.”  Frustrated states  They also found that these frustrated states are associated with relatively high or low brain activity – unlike memory recall – that correlate with well-known “up” and “down” states that have been described in neural models and observed in the brain activity of mammals. “We have proven both theoretically and through simulation that the up and down states observed in the activity of mammal brains would be but a mere manifestation of these spin-glass states,” says Torres.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/mar/22/spin-glass-provides-insight-into-brain-activity){:target="_blank" rel="noopener"}


