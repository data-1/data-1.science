---
layout: post
title: "What is the surface of Neptune like?"
date: 2016-04-23
categories:
author: Matt Williams
tags: [Neptune,Atmosphere,Uranus,Great Dark Spot,Planet,Jupiter,Sun,Nature,Sky,Planemos,Physical sciences,Solar System,Bodies of the Solar System,Outer space,Planets of the Solar System,Astronomical objects,Space science,Astronomy,Planets,Planetary science]
---


Credit: NASA/JPL  As a gas giant (or ice giant), Neptune has no solid surface. Credit: NASA  Also like Uranus, Neptune's internal structure is differentiated between a rocky core consisting of silicates and metals; a mantle consisting of water, ammonia and methane ices; and an atmosphere consisting of hydrogen, helium and methane gas. Deeper clouds of water ice should be also found in the lower regions of the troposphere, where pressures of about 50 bars (5.0 MPa) and temperature of 273 K (0 °C) are common. This differential rotation is the most pronounced of any planet in the Solar System, and results in strong latitudinal wind shear and violent storms. This nickname first arose during the months leading up to the Voyager 2 encounter in 1989, when the cloud group was observed moving at speeds faster than the Great Dark Spot.

<hr>

[Visit Link](http://phys.org/news/2016-04-surface-neptune.html){:target="_blank" rel="noopener"}


