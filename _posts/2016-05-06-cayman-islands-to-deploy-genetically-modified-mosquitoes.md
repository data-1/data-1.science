---
layout: post
title: "Cayman Islands to deploy genetically modified mosquitoes"
date: 2016-05-06
categories:
author: Jennifer Kay And Ben Fox
tags: [Oxitec,Aedes aegypti,Zika fever]
---


The company has deployed its mosquitoes to fight Zika in Brazil following initial trials there and previously conducted tests in the Cayman Islands and Panama. Oxitec and officials in the Florida Keys have proposed testing there as well and are awaiting U.S. regulatory approval. The mosquitoes will not be used on the two smaller islands of the British territory, Little Cayman and Cayman Brac, which do not have the Aedes aegypti. The company has started outreach on Grand Cayman to address any public concerns. Phil Goodman, chairman of the five-member Florida Keys Mosquito Control District board of commissioners, said the decision to use Oxitec's mosquitoes on Grand Cayman was good news for the prospects of a similar trial in Florida if it is approved by the U.S. Food and Drug Administration.

<hr>

[Visit Link](http://phys.org/news/2016-05-cayman-islands-deploy-genetically-mosquitoes_1.html){:target="_blank" rel="noopener"}


