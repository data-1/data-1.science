---
layout: post
title: "The path to artificial photosynthesis"
date: 2015-07-20
categories:
author: Helmholtz-Zentrum Berlin für Materialien und Energie 
tags: [Catalysis,Physical sciences,Chemistry,Materials,Nature,Applied and interdisciplinary physics,Physical chemistry,Chemical substances]
---


Through their work, Professor Emad Aziz, head of the HZB Institute Methods for Material Development, Professor Leone Spiccia from Monash University and their teams have taken an important leap forward in understanding photosynthesis - the method green plants use to obtain energy - in artificial systems. At his institute, Emad Aziz is doing research on artificial water splitting catalysts with the goal of getting them to perform at the level of the oxygen evolution center of photosynthesis. He says: Under a bias, our manganese complexes produce nanoparticles of manganese oxides within nafion matrix. When exposed to light and biased simultaneously, these oxides promote water oxidation, a key and challenging reaction associated with the splitting water into oxygen and hydrogen. Our goal is to provide synthetic chemists with a full picture of the catalytic process under real test conditions in order to enhance their work on the function of these materials, says Emad Aziz, and figure out if and under what conditions it might be used for technological application in converting light to chemical energy.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/hbfm-tpt012115.php){:target="_blank" rel="noopener"}


