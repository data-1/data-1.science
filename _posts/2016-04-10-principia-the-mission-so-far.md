---
layout: post
title: "Principia: the mission so far"
date: 2016-04-10
categories:
author:  
tags: [Tim Peake,Astronauts,Aerospace,Science,Pan-European scientific organizations,Flight,European Space Agency,Space-based economy,People in the space industry,Space programs,Space-flown life,Space industry,Aeronauts,Outer space,Spaceflight,Human spaceflight,Astronautics,Life in space,People associated with spaceflight,Space policy of the European Union,Space agencies,European space programmes,European astronauts,Space exploration,People associated with vehicles]
---


ESA astronaut Tim Peake shares his views of Earth and his six-month Principia mission while on the International Space Station. Narrated by Tim himself taken from interviews while in space, this video shows the best views, experiments and shares the experience of Tim's life in space. Music:Roob Sebastiaan - Gravityhttps://soundcloud.com/r00b/sets/ambient-occlusionFollow Tim Peake via http://timpeake.esa.int  More about Principia: http://www.esa.int/principia

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2016/04/Principia_the_mission_so_far){:target="_blank" rel="noopener"}


