---
layout: post
title: "All creatures great and small: London Zoo weighs 17,000 animals"
date: 2016-06-14
categories:
author: ""
tags: [Zoo,Zookeeper,Zoological Society of London,London Zoo]
---


A humboldt penguin is weighed at London Zoo on August 26, 2015 during the zoo's annual weigh-in  London Zoo on Wednesday carried out its annual weigh-in as it sought to keep track of more than 17,000 animals in its care. Ten-week-old penguin chicks and 80-year-old tortoises were among a huge variety of animals that had their vital statistics recorded at the zoo in London's Regents Park. Each measurement is recorded in the Zoological Information Management System (ZIMS), a database shared with zoos all around the world, helping zookeepers to compare important information on thousands of endangered species. Methods used include getting penguins to walk over scales as they line up for their morning feed and training tortoises to clamber onto scales hidden in their grassy paddock. Keepers at London Zoo use ingenious tactics to entice the animals in their care to stand up and be measured  Founded in 1826, ZSL is an international charity aiming to promote worldwide conservation of animals and their habitats.

<hr>

[Visit Link](http://phys.org/news/2015-08-creatures-great-small-london-zoo.html){:target="_blank" rel="noopener"}


