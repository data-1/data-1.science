---
layout: post
title: "Superfast light source made from artificial atom"
date: 2016-04-29
categories:
author: University Of Copenhagen
tags: [Electron,Quantum dot,Light,Atom,Niels Bohr,Photon,Electron hole,Matter,Quantum,Physical chemistry,Chemistry,Electromagnetism,Science,Atomic molecular and optical physics,Applied and interdisciplinary physics,Physics,Quantum mechanics,Theoretical physics,Physical sciences]
---


The attraction between the electron and hole creates a new quantum state with a very strong light-matter interaction and a corresponding quick release of light. New research results from the Niels Bohr Institute show that light sources can be made much faster by using a principle that was predicted theoretically in 1954. It is an outstanding single-photon source, says Søren Stobbe, who is an associate professor in the Quantum Photonic research group at the Niels Bohr Institute at the University of Copenhagen and led the project. Petru Tighineanu, a postdoc in the Quantum Photonics research group at the Niels Bohr Institute, has carried out the experiments and he explains the effect as such, that the atoms are very small and light is very 'big' because of its long wavelength, so the light almost cannot 'see' the atoms – like a lorry that is driving on a road and does not notice a small pebble. In the same way, light interacts much more strongly with the quantum dot if the quantum dot contains the special superradiant quantum state, which makes it look much bigger.

<hr>

[Visit Link](http://phys.org/news/2016-04-superfast-source-artificial-atom.html){:target="_blank" rel="noopener"}


