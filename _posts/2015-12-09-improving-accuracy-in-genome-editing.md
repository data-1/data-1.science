---
layout: post
title: "Improving accuracy in genome editing"
date: 2015-12-09
categories:
author: Harvard University
tags: [Genome editing,Cas9,Biotechnology,Biology,Genetics,Life sciences,Biochemistry,Molecular biology,Chemistry,Biological engineering,Health sciences,Branches of genetics]
---


We engineered a form of Cas9 that is only active in the presence of a harmless, drug-like small molecule which we provide to the cell, Liu explained. We showed it's possible to achieve efficient genome editing using this system, but because you can stop the production of active Cas9 by withholding the small molecule once it's had enough time to modify the target genes, you limit the opportunity for off-target genome modification. The idea to use a small molecule to control the activation of Cas9 grew out of the realization by Liu and colleagues, that genome editing - unlike virtually every other drug therapy or tool used in the lab - is not steady-state chemistry. To achieve that short-burst of activity in Cas9, Liu and colleagues engineered a form of the protein that was only active in the presence of a small, drug-like molecule. It's all about making sure the benefits of the technology outweigh the risks, he continued.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/hu-iai042315.php){:target="_blank" rel="noopener"}


