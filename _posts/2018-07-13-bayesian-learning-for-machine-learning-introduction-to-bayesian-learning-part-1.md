---
layout: post
title: "Bayesian Learning for Machine Learning: Introduction to Bayesian Learning (Part 1)"
date: 2018-07-13
categories:
author: "May."
tags: []
---


If case 1 is observed, you are now more certain that the coin is a fair coin, and you will decide that the probability of observing heads is 0.5 with more confidence. Bayesian learning uses Bayes' theorem to determine the conditional probability of a hypotheses given some evidence or observations. However, for now, let us assume that P(θ) = p. P(X|θ) — Likelihood is the conditional probability of the evidence given a hypothesis. Binomial Likelihood  The likelihood for the coin flip experiment is given by the probability of observing heads out of all the coin flips given the fairness of the coin. As the Bernoulli probability distribution is the simplification of Binomial probability distribution for a single trail, we can represent the likelihood of a coin flip experiment that we observe k number of heads out of N number of trials as a Binomial probability distribution as shown below:  Beta Prior Distribution  The prior distribution is used to represent our belief about the hypothesis based on our past experiences.

<hr>

[Visit Link](https://dzone.com/articles/bayesian-learning-for-machine-learning-part-i-intr?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


