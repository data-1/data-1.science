---
layout: post
title: "WIRED - The Latest in Technology, Science, Culture and Business"
date: 2014-07-13
categories:
author: Condé Nast, Mike Mariani, Grace Browne, Francesca Tripodi, Jaipreet Virdi, Jeremy White, Will Knight, Boone Ashworth, Katrina Miller, Adrienne So
tags: [Computer-mediated communication,Information technology,Online services,Software,Digital media,Computer networks,Computing,Mass media,Websites,Web 20,Technology,Communication,Internet,Cyberspace,World Wide Web]
---


Gear  How Stewart Butterfield Created Slack From a Failed Videogame  At first, the software was hard to explain—nobody had ever used anything like it. It was based on a system his team developed for Glitch. He called it Slack.

<hr>

[Visit Link](http://feeds.wired.com/c/35185/f/661467/s/3c659312/sc/5/l/0L0Swired0N0C20A140C0A70Chow0Eto0Eteach0Ehumans0Eto0Eremember0Ereally0Ecomplex0Epasswords0C/story01.htm){:target="_blank" rel="noopener"}


