---
layout: post
title: "Tara Oceans expedition yields treasure trove of plankton data"
date: 2015-07-12
categories:
author: American Association for the Advancement of Science (AAAS) 
tags: [American Association for the Advancement of Science,Ocean,Plankton,Earth sciences,Oceanography,Science,Nature]
---


This news release is available in Japanese. In five related reports in this issue of the journal Science, a multinational team of researchers who spent three and a half years sampling the ocean's sunlit upper layers aboard the schooner Tara unveil the first officially reported global analyses of the Tara Oceans consortium. Planktonic life in the ocean is far more diverse than scientists knew, these reports show. They provide new resources for cataloguing the ocean's numerous planktonic organisms, which -- though critical to life on Earth, providing half the oxygen generated annually through photosynthesis, for example -- have largely been uncharacterized. Gipsi Lima-Mendez and colleagues used all of these data sets to create a map of plankton species interactions, which have not previously been understood.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/aaft-toe051815.php){:target="_blank" rel="noopener"}


