---
layout: post
title: "Gravitational waves: a three-minute guide"
date: 2016-03-15
categories:
author:  
tags: []
---


Thank you for visiting nature.com. You are using a browser version with limited support for CSS. To obtain the best experience, we recommend you use a more up to date browser (or turn off compatibility mode in Internet Explorer). In the meantime, to ensure continued support, we are displaying the site without styles and JavaScript.

<hr>

[Visit Link](http://www.nature.com/news/gravitational-waves-a-three-minute-guide-1.19366){:target="_blank" rel="noopener"}


