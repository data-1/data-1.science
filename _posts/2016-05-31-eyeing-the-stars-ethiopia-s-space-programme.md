---
layout: post
title: "Eyeing the stars: Ethiopia's space programme"
date: 2016-05-31
categories:
author: ""
tags: [Ethiopia,Observatory,Astronomy,Telescope,Science,Space science,Outer space]
---


Reflective one-meter telescopes are pictured at the grounds of The Entoto Observatory and Research Center, on the outskirts of Addis Ababa, Ethiopia  High above the crowded streets of Addis Ababa, among fields where farmers lead oxen dragging wooden ploughs, sits Ethiopia's space programme. Science is part of any development cycle—without science and technology nothing can be achieved, said Abinet Ezra, communications director for the Ethiopian Space Science Society (ESSS). But its supporters have had a tough ride to set it up. Being poor is not a boundary to start this programme, Solomon said, adding that by boosting support for science, it would help develop the country. A chalkboard with mathematic equations as well as writing in Amharic, the Ethiopian national language, is seen inside the Entoto Observatory and Research Center, on the outskirts of Addis Ababa  But Ethiopia has plans, including to build a far more powerful observatory in the northern mountains around Lalibela, far from city lights.

<hr>

[Visit Link](http://phys.org/news/2015-08-eyeing-stars-ethiopia-space-programme.html){:target="_blank" rel="noopener"}


