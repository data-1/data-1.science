---
layout: post
title: "Uruguay Builds Clean Electricity Footprint"
date: 2015-12-18
categories:
author: Glenn Meyers, Steve Hanley, Sponsored Content, Charles W. Thurston, James Ayre, Written By
tags: [Sustainable energy,Renewable energy,Natural resources,Energy,Economy,Nature,Natural environment,Sustainable development,Economy and the environment,Energy and the environment]
---


In less than 10 years, the country has shrunk its carbon footprint while lowering electricity costs, without calling on the government for renewable energy subsidies. Uruguay’s electricity mix is as follows:  Hydropower  Thermal power  Wind power  Diesel generators  Developing solar & biomass  With a population of 3.4 million, this country has been cheered for efforts to ‘decarbonize’ its economy. The list of those applauding includes the World Bank and the Economic Commission for Latin America and the Caribbean. Additionally, the WWF named Uruguay among its “Green Energy Leaders,” proclaiming: “The country is defining global trends in renewable energy investment.”  “What we’ve learned is that renewables is just a financial business,” Méndez said. Importantly, the majority of this energy portfolio happens to be clean and renewable.

<hr>

[Visit Link](http://cleantechnica.com/2015/12/10/uruguay-builds-clean-electricity-footprint/){:target="_blank" rel="noopener"}


