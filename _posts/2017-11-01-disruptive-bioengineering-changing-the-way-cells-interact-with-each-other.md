---
layout: post
title: "Disruptive bioengineering – changing the way cells interact with each other"
date: 2017-11-01
categories:
author: Oxford Science Blog, University Of Oxford
tags: [Cas9,Cell signaling,CRISPR gene editing,Cancer,Cell (biology),Genetics,Molecular biology,Biochemistry,Biotechnology,Life sciences,Biology,Clinical medicine]
---


CRISPR/Cas9 frequently makes the headlines as it allows medical researchers to accurately manipulate the human genome – opening up new possibilities for treating diseases. Using this approach, the researchers altered the Lego bricks to build a new class of synthetic receptors, and programmed them to initiate specific cascades of events in response to a variety of distinct natural signals. The system also has potential applications for other systemic diseases, like diabetes. To demonstrate this potential, the team engineered another receptor complex that can sense the amount of glucose in the surroundings and prompt insulin production – the hormone that takes glucose up from the blood stream. With this new technique developed in Oxford, the team hopes that genome engineering does not have to be limited to correcting DNA faults but altering the way that cells work – regardless of the root cause of disease.

<hr>

[Visit Link](https://phys.org/news/2017-10-disruptive-bioengineering-cells-interact.html){:target="_blank" rel="noopener"}


