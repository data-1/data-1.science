---
layout: post
title: "Autism and human evolutionary success"
date: 2017-03-19
categories:
author: "University of York"
tags: [Autism,Autism spectrum,Evolution,Biodiversity,Psychology,Behavioural sciences,Cognitive science,Branches of science,Cognition]
---


A subtle change occurred in our evolutionary history 100,000 years ago which allowed people who thought and behaved differently - such as individuals with autism - to be integrated into society, academics from the University of York have concluded. But rather than being left behind, or at best tolerated, the research team conclude that many would have played an important role in their social group because of their unique skills and talents. We are arguing that it is the rise of collaborative morality that led to the possibility for widening the diversity of the human personality. Dr Spikins said The archaeological record doesn't give us a skeletal record for autism, but what it does do is give us a record for other people who have various differences and how they have been integrated. It was also roughly at that time that we see collaborative morality emerging.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/uoy-aah111516.php){:target="_blank" rel="noopener"}


