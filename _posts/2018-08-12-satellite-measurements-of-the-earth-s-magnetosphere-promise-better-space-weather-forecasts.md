---
layout: post
title: "Satellite measurements of the Earth's magnetosphere promise better space weather forecasts"
date: 2018-08-12
categories:
author: "Kanazawa University"
tags: [Van Allen radiation belt,Earth,Magnetosphere,Applied and interdisciplinary physics,Nature,Science,Physics,Physical sciences,Outer space,Electromagnetism,Astronomy,Space science]
---


Kanazawa University-led researchers take measurements of the invisible electric and magnetic fields closely related to charged particles whirling through Earth's inner magnetosphere to help protect satellites from those particles  Kanazawa, Japan - Earth is constantly being hammered by charged particles emitted by the Sun that have enough power to make life on Earth almost impossible. We get to enjoy the sight of those particles when the bands they move in (the Van Allen radiation belts) dip into our atmosphere near the poles creating the Northern (and Southern) lights. So far, NASA have launched twin satellites to study the Van Allen belts--however, their orbits only allow them to explore the equatorial regions. Another strength of our project is that we can also compare the satellite data with data collected simultaneously on the ground. Understanding how electrons and other particles are hurled out of the magnetosphere onto our planet could be key to predicting such bursts and protecting against them.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/ku-smo080818.php){:target="_blank" rel="noopener"}


