---
layout: post
title: "UTA bioengineer to collaborate with UTSW to improve neonatal brain monitoring"
date: 2017-10-11
categories:
author: "University of Texas at Arlington"
tags: [University of Texas Southwestern Medical Center,Neuroimaging,Infant,Biomedical engineering,Medical imaging,Health,Medical specialties,Health sciences,Medicine,Clinical medicine,Health care]
---


A University of Texas at Arlington bioengineering professor and her team will integrate a portable brain imaging system with an advanced signal-processing technique for newborns that will better measure the babies' neurophysiology in real time, providing physicians the analysis needed to treat encephalopathy or brain swelling more quickly. Hanli Liu, a UTA bioengineer, will use $500,000 of a five-year, $1.73 million grant from the National Institutes of Health to Dr. Lina Chalak, a UT Southwestern neonatal intensivist whose research is focused on improving outcomes of the neonatal brain. Such regional brain oxygenation parameters, when being combined with other vital recordings, will give physicians real-time information needed to proceed with life-saving treatment for babies experiencing encephalopathy. This is why Dr. Chalak from UTSW relies on Liu and her team for quantification of coherence factors between several neurophysiological and vital parameters, such as neurovascular wavelet analysis. So, the combination of a portable brain imaging system with advances of WCA Liu is developing will help clinicians determine when the treatment should be initiated.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uota-ubt100917.php){:target="_blank" rel="noopener"}


