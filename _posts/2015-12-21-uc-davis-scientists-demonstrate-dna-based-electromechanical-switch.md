---
layout: post
title: "UC Davis scientists demonstrate DNA-based electromechanical switch"
date: 2015-12-21
categories:
author: University of California - Davis
tags: [Nanotechnology,Electronics,Computing,DNA origami,Nucleic acid double helix,DNA,Computer,Electromechanics,Engineering,Materials science,Materials,Applied and interdisciplinary physics,Branches of science,Technology,Electromagnetism,Electrical engineering,Chemistry,Electricity]
---


A team of researchers from the University of California, Davis and the University of Washington have demonstrated that the conductance of DNA can be modulated by controlling its structure, thus opening up the possibility of DNA's future use as an electromechanical switch for nanoscale computing. The electrical properties, however, have generally been difficult to control, said Hihath. Possible Paradigms for Computing  In addition to potential advantages in fabrication at the nanoscale level, such DNA-based devices may also improve the energy efficiency of electronic circuits. To develop DNA into a reversible switch, the scientists focused on switching between two stable conformations of DNA, known as the A-form and the B-form. Eventually, the environmental gating aspect of this work will have to be replaced with a mechanical or electrical signal in order to locally address a single device, noted Hihath.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/uoc--uds121115.php){:target="_blank" rel="noopener"}


