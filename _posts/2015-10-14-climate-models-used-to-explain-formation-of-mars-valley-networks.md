---
layout: post
title: "Climate models used to explain formation of Mars valley networks"
date: 2015-10-14
categories:
author: Penn State 
tags: [Mars,Water,Valley network (Mars),Atmosphere,Climate change,Astronomical objects known since antiquity,Solar System,Astronomical objects,Bodies of the Solar System,Nature,Planetary science,Space science,Planets,Astronomy,Planets of the Solar System,Earth sciences,Terrestrial planets,Physical sciences,Outer space]
---


Now, a team of Penn State and NASA researchers is using climate models to predict how greenhouse warming could be the source of the water. Our work involves using models to best estimate what was happening on the surface of Mars 3.8 billion years ago. Previous studies analyzing craters support the idea of flowing water on early Mars but still unknown is how surface warming occurred to melt that water and how much water was really there. In a recent issue of Icarus, they report that using a photochemical model, they determined the possibility that there was a high percentage of hydrogen in the atmosphere. According to Batalha, further evidence showing water stayed on Mars for tens of millions of years would support a warm early Mars while putting the impact hypothesis to rest.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/ps-cmu101215.php){:target="_blank" rel="noopener"}


