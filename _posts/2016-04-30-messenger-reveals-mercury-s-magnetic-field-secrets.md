---
layout: post
title: "MESSENGER reveals Mercury's magnetic field secrets"
date: 2016-04-30
categories:
author: University Of British Columbia
tags: [Mercury (planet),MESSENGER,Science,Solar System,Planetary science,Outer space,Planets of the Solar System,Astronomy,Space science,Bodies of the Solar System,Astronomical objects known since antiquity,Physical sciences,Planets,Astronomical objects,Terrestrial planets,Spaceflight]
---


Credit: NASA/Johns Hopkins University Applied Physics Laboratory/Carnegie Institution of Washington  New data from MESSENGER, the spacecraft that orbited Mercury for four years before crashing into the planet a week ago, reveals Mercury's magnetic field is almost four billion years old. When MESSENGER flew close to the planet, its magnetometer collected data on the magnetism of rocks in Mercury's surface. The planet itself formed around the same time as Earth, just over 4.5 billion years ago. If we didn't have these recent observations, we would never have known how Mercury's magnetic field evolved over time, said Johnson, also a scientist at the Planetary Science Institute. The engineers also designed large elliptical orbits around Mercury that allowed the spacecraft to spend time far from the planet in each orbit and cool off.

<hr>

[Visit Link](http://phys.org/news350226044.html){:target="_blank" rel="noopener"}


