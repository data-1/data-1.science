---
layout: post
title: "Why don't men live as long as women?"
date: 2016-05-10
categories:
author: University Of Southern California
tags: [Cardiovascular disease,Ageing,Mortality rate,Health,Death,Eileen M Crimmins,Disease,Branches of science,Health sciences]
---


In the wake of this massive but uneven decrease in mortality, a review of global data points to heart disease as the culprit behind most of the excess deaths documented in adult men, said USC University Professor and AARP Professor of Gerontology Eileen Crimmins. Focusing on mortality in adults over the age of 40, the team found that in individuals born after 1880, female death rates decreased 70 percent faster than those of males. Even when the researchers controlled for smoking-related illnesses, cardiovascular disease appeared to still be the cause of the vast majority of excess deaths in adult men over 40 for the same time period. Surprisingly, smoking accounted for only 30 percent of the difference in mortality between the sexes after 1890, Crimmins said. The study, Twentieth century surge of excess adult male mortality, appears in the Proceedings of the National Academy of Sciences and was supported by the National Institute on Aging.

<hr>

[Visit Link](http://phys.org/news355408796.html){:target="_blank" rel="noopener"}


