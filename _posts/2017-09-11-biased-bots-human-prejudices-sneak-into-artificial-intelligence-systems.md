---
layout: post
title: "Biased bots: Human prejudices sneak into artificial intelligence systems"
date: 2017-09-11
categories:
author: "Princeton University, Engineering School"
tags: [Machine learning,Bias,Implicit-association test,Artificial intelligence,Algorithmic bias,Research,Cognition,Branches of science,Cognitive science,Science]
---


Common machine learning programs, when trained with ordinary human language available online, can acquire cultural biases embedded in the patterns of wording, the researchers found. Questions about fairness and bias in machine learning are tremendously important for our society, said researcher Arvind Narayanan, an assistant professor of computer science and an affiliated faculty member at the Center for Information Technology Policy (CITP) at Princeton University, as well as an affiliate scholar at Stanford Law School's Center for Internet and Society. As it turned out, the Princeton machine learning experiment managed to replicate the broad substantiations of bias found in select Implicit Association Test studies over the years that have relied on live, human subjects. In an apparent corroboration of this bias, the new Princeton study demonstrated that a set of African American names had more unpleasantness associations than a European American set. Computer programmers might hope to prevent cultural stereotype perpetuation through the development of explicit, mathematics-based instructions for the machine learning programs underlying AI systems.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/04/170413141055.htm){:target="_blank" rel="noopener"}


