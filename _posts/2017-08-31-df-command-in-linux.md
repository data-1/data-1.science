---
layout: post
title: "DF Command in Linux"
date: 2017-08-31
categories:
author: ""
tags: [File system,Computer file,Filename,Hard disk drive,Computers,Information science,Computer science,Data management,Computer hardware,Software,Storage software,System software,Information retrieval,Data,Utility software,Information technology management,Computer data storage,Computer engineering,Technology,Computer data,Computing,Computer architecture,Operating system technology]
---


Df Command in Linux  The Linux df command, also known as disk free, is used to display free and used disk space for each file system on Linux and Unix-like systems. By default, the Linux df command displays the free space information in 1 K blocks, which means 1024 bytes multiplied by the displayed number. Syntax  The basic syntax of the df command is:  df [options] [file_path]  Df Command Options  The df command in Linux provides various options to display space information of currently mounted file systems and in different size formats. The following table lists and describes the most common df command supported options:  Option Description -a To include dummy files in the output, which include zero block sizes -B To display block size in the specified unit, such as BM for MB and BG for GB -h To display block size in human-readable form -i Displays inode information instead of the block usage -l Displays disk space information of only the local file system -T Displays file system type, such as ext4 -t Limit to showing a specific filesystem type -x Excludes the specified file system type from the output  How to Use Df Command  The df command in Linux can be used with or without options to display different types of disk space information. Check Disk Usage Information For All the File Systems  To display disk usage information for all currently mounted file systems, use the df command without any option:  df  Check Disk Usage Information of a Particular File System  To display disk space usage information of a specific file system, specify the file system name with the df command:  df /boot  Display Disk Usage Information in Different Block Formats  By default, the disk space usage information is displayed in 1K block format.

<hr>

[Visit Link](http://linoxide.com/linux-command/linux-df-command/){:target="_blank" rel="noopener"}


