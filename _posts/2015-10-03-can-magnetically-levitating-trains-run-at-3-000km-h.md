---
layout: post
title: "Can magnetically levitating trains run at 3,000km/h?"
date: 2015-10-03
categories:
author: Roger Goodall, The Conversation
tags: [Maglev,Train,Technology,Transport,Transportation engineering]
---


Credit: Alex Needham, CC BY  Trains that use magnets to levitate above the tracks might sound like something from Back to the Future, but the concept of magnetic levitation has been around for many years. This is fine at speeds of up to 400km/h (the speed of proposed Britain's HS2 line), but aerodynamics makes going much faster very difficult. Maglev's 'magic'  All Maglev technologies use some form of magnet – this could be a permanent magnet, an electro-magnet or a magnet using a superconducting coil. To achieve high-speed operation, coils are fitted to the track and these are used to create a travelling magnetic field which essentially drags the vehicles along by their magnets. This is where it becomes really difficult, and very costly.

<hr>

[Visit Link](http://phys.org/news324202716.html){:target="_blank" rel="noopener"}


