---
layout: post
title: "Thomas Pesquet returns to Earth"
date: 2017-09-23
categories:
author: ""
tags: [Astronaut,Space vehicles,Spaceflight,Astronautics,Outer space,Flight,Human spaceflight,Life in space,Space programs,Spacecraft,Space exploration,Spaceflight technology,Aerospace,Space science,Space program of the United States,Human spaceflight programs,NASA,Space industry,Space-based economy,Space research]
---


Science & Exploration Thomas Pesquet returns to Earth 02/06/2017 12425 views 137 likes  ESA astronaut Thomas Pesquet landed on the steppe of Kazakhstan today with Russian commander Oleg Novitsky in their Soyuz MS-03 spacecraft after six months in space. Touchdown was at 14:10 GMT after a four-hour flight from the International Space Station. Thomas in Soyuz spacecraft The return was routine – or as routine as you can get for a ride that requires braking from 28 800 km/h to zero. The next voyage will be more sedate as Thomas flies directly to ESA’s astronaut centre in Cologne, Germany, for debriefing and tests. First call “The next ESA astronaut to be launched will be Paolo Nespoli next month, ensuring science and technology demonstrations in space continue for the benefit of humankind.” With Thomas and Oleg back on Earth, three astronauts remain on the orbiting outpost: NASA astronauts Peggy Whitson and Jack Fischer, and Roscosmos cosmonaut Fyodor Yurchikhin.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Proxima/Thomas_Pesquet_returns_to_Earth){:target="_blank" rel="noopener"}


