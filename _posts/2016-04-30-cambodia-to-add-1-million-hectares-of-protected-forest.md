---
layout: post
title: "Cambodia to add 1 million hectares of protected forest"
date: 2016-04-30
categories:
author:  
tags: [Cambodia,Forestry,Forest,Natural environment,Natural resource management,Natural resources,Environmental protection,Environmental conservation]
---


Illegal logging has eviscerated Cambodia's forests - destroying around a third of the total in the country in the past 30 years  Cambodia's prime minister has ordered a million hectares of forest be included in protected zones as the country faces one of the world's fastest deforestation rates. The move, which covers five new areas of forest, will bump Cambodia's conservation zones up by a fifth, bringing more than a quarter of the country's land under protection. The Ministry of Environment must...list the five forests as protected areas, said the order signed by Prime Minister Hun Sen, which was seen by AFP on Saturday. The new conservation areas will include parts of Prey Lang—a forest where activists have long been risking their lives to expose the illegal logging that has eviscerated Cambodia's forest cover. His government has been criticised for allowing firms to clear hundreds of thousands of hectares of forest land—including in protected zones—for everything from rubber and sugar cane plantations to hydropower dams.

<hr>

[Visit Link](http://phys.org/news/2016-04-cambodia-million-hectares-forest.html){:target="_blank" rel="noopener"}


