---
layout: post
title: "3 Factors That Can Accelerate Wind & Solar Adoption"
date: 2015-12-18
categories:
author: Written By
tags: [Renewable energy,Solar power,Merit order,Photovoltaics,Electrical grid,Price,Supply (economics),Market (economics),Distributed generation,Concentrated solar power,Duck curve,Nature,Energy,Economy]
---


Understanding the “Problems”  There has been increasing concern that variable renewables such as wind and solar may face an upper limit to adoption in the U.S. grid. 2) Demand is increasingly flexible, not fixed  Analysts arguing that renewables’ variability will limit their growth often assume perfectly efficient wholesale markets, but unchanged retail markets and fixed demand profiles. In other words, as renewables reduce energy prices during certain times of day, demand flexibility allows customers to shift demand to those times, which will both reduce energy prices at other (peak) times and raise the price paid to renewables during times when they produce the most. But instead, we can tap the latent power of supply diversity, demand flexibility, storage, and market design to level the playing field for all resources, rather than clinging to the premises of the 20th century grid. Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2015/12/04/3-factors-that-can-accelerate-wind-solar-adoption/){:target="_blank" rel="noopener"}


