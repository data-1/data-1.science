---
layout: post
title: "New software helps detect adaptive genetic mutations"
date: 2018-07-18
categories:
author: "Brown University"
tags: [Mutation,Evolution,Heredity,Genetics,Thrifty gene hypothesis,Gene,Statistics,Evolutionary biology,Biological evolution,Biology]
---


That framework can then be used to scan genomic data from multiple individuals and compute the probabilities that individual mutations or regions of a genome are adaptive. SWIF(r) looks for the statistical signatures of selective sweeps in genomic datasets. While most techniques identify only regions of the genome likely to contain adaptive mutations, SWIF(r) can also identify the particular mutations themselves. To show that the technique works, the researchers validated it on a simulated dataset in which known adaptive mutations were included, as well as on canonical adaptive mutations that have been identified in human genomes through multiple molecular experiments. The ‡Khomani San have the largest genetic diversity of any living population, Alpert Sugden said, which is interesting from our perspective because there's a lot of opportunity for adaptive mutations to arise.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/bu-nsh021918.php){:target="_blank" rel="noopener"}


