---
layout: post
title: "Mercury's MESSENGER mission comes to a crashing climax"
date: 2016-04-26
categories:
author: Donna Burton, The Conversation
tags: [MESSENGER,Mercury (planet),BepiColombo,Mariner program,Spaceflight,Space science,Astronomy,Outer space,Solar System,Bodies of the Solar System,Planetary science,Astronomical objects,Physical sciences,Astronomical objects known since antiquity,Planets of the Solar System,Sky,Planets,Local Interstellar Cloud,Astronautics]
---


The probe carries a number of science instruments to map the surface of the planet and its magnetic field, detect atmospheric gases and various elements of Mercury's crust, and much more. After its launch, the spacecraft flew by Earth once (in 2005), Venus twice (in 2006 and 2007) and Mercury three times (twice in 2008 and once in 2009). Credit: NASA  After travelling 7.9 billion km and orbiting the sun 15 times MESSENGER entered Mercury's orbit on March 18, 2011. And so after a mission lasting almost 11 years the tiny robotic probe MESSENGER will end its mission on 30 April by crashing into the surface of the planet. On its final orbit the probe will be only around 250 - 500 m above the surface at around 14,500km/hr.

<hr>

[Visit Link](http://phys.org/news349510972.html){:target="_blank" rel="noopener"}


