---
layout: post
title: "Scientists smash solar cell efficiency record by converting 40.4% of sunlight to energy"
date: 2015-07-04
categories:
author: Charley Cameron
tags: []
---


A team from the University of New South Wales, Australia, just set a new world record for solar energy efficiency by successfully converting 40.4% of available sunlight into electricity. These cells, as Motherboard explains, “are basically a sandwich of differently tuned semiconductors with each one able to capture a different wavelength of sunlight.”  And so while “traditional methods use one solar cell, which limits the conversion of sunlight to electricity to about 33 percent, the newer technology splits the sunlight into four different cells, which boosts the conversion levels,” UNSW Professor Martin Green explained to the AFP. SIGN UP I agree to receive emails from the site. Check our Privacy Policy. SIGN UP  We’re unlikely to see this particular design popping up in rooftop arrays anytime soon, however.

<hr>

[Visit Link](http://inhabitat.com/scientists-smash-solar-cell-efficiency-record-by-converting-40-4-of-sunlight-to-energy/){:target="_blank" rel="noopener"}


