---
layout: post
title: "Endangered dragonflies, raised in captivity, being released"
date: 2016-05-14
categories:
author: Byregina Garcia Cano
tags: [Hines emerald,Fly,Dragonfly,Natural environment]
---


In this 2015 photo provided by Daniel Soluk, an adult Hine's emerald dragonfly hangs from the skin of the last larval stage from which it just emerged at the Mud Lake Wildlife Area in Baileys Harbor, Wis. A number of the federally endangered insects that were raised at a South Dakota laboratory over the past several years are being released at a forest preserve in Illinois in July 2015. (Daniel Soluk via AP)  Federally endangered dragonflies that have been raised in a laboratory over the past several years are being released at a forest preserve this week in Illinois, where scientists believe they'll be a good match with the small population still there. In the wild, he said, not very many dragonfly eggs survive to become adults—perhaps 10 of 1,000. The Hine's emerald dragonfly was discovered in Ohio, but by the mid-1900s, scientists believed the insect was extinct. Between 80 and 320 Hine's emerald larvae emerge as flying adults in Illinois every year, said Kristopher Lah, an endangered species coordinator at the U.S.

<hr>

[Visit Link](http://phys.org/news/2015-07-endangered-dragonflies-captivity.html){:target="_blank" rel="noopener"}


