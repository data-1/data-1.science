---
layout: post
title: "Fine, You Reached Pluto. So What's Next?"
date: 2014-06-26
categories:
author: Michael D. Lemonick
tags: [New Horizons,Kuiper belt,Pluto,Hubble Space Telescope,Spaceflight,Science,Planemos,Planets,Physical sciences,Spacecraft,Space exploration,Space science,Astronomy,Outer space,Solar System,Planetary science,Bodies of the Solar System,Local Interstellar Cloud,Flight,Astronautics,Astronomical objects]
---


It will be a very big deal when NASA’s New Horizons spacecraft finally arrives at Pluto next July, after a nine-year, four-billion-mile-plus journey. Once considered to be a weird little world orbiting alone at the edges of the Solar System, it’s now recognized as just the biggest member (more or less) of a huge swarm of frozen objects known collectively as the Kuiper Belt. But once you’ve spent $700 million on a space probe, it’s a shame to let it go idle after zipping past its prime target. The problem: while planetary scientists are sure there are billions of KBO’s out there, they’ve only identified 1,500 or so over the past 22 years—and none of them is on New Horizons’ projected trajectory. “We get 40 orbits right off the bat, but we have to find at least two new objects in that time or they don’t give us the other 160.”  They have to do it soon, too.

<hr>

[Visit Link](http://time.com/2921174/pluto-spacecraft-whats-next/){:target="_blank" rel="noopener"}


