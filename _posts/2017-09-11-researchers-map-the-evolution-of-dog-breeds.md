---
layout: post
title: "Researchers map the evolution of dog breeds"
date: 2017-09-11
categories:
author: "Cell Press"
tags: [Dog,Herding dog,Dog breed,Biology]
---


However, in a study published April 25 in Cell Reports, researchers have used gene sequences from 161 modern breeds to assemble an evolutionary tree of dogs. The map of dog breeds, which is the largest to date, unearths new evidence that dogs traveled with humans across the Bering land bridge, and will likely help researchers identify disease-causing genes in both dogs and humans. Wouldn't you like it to represent your breed in the dog genome sequence database?' Every time there's a disease gene found in dogs it turns out to be important in people, too. Cell Reports, Parker et al.: Genomic Analyses Reveal the Influence of Geographic Origin, Migration, and Hybridization on Modern Dog Breed Development http://www.cell.com/cell-reports/fulltext/S2211-1247(17)30456-4  Cell Reports (@CellReports), published by Cell Press, is a weekly open-access journal that publishes high-quality papers across the entire life sciences spectrum.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/cp-rmt042017.php){:target="_blank" rel="noopener"}


