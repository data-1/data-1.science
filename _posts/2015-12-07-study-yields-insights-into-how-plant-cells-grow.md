---
layout: post
title: "Study yields insights into how plant cells grow"
date: 2015-12-07
categories:
author: Purdue University 
tags: [Cell (biology),Actin,Cytoskeleton,American Association for the Advancement of Science,Protein,Microtubule,Plant,Trichome,Cell biology,Biology]
---


Previous research had shown that two intracellular fiber systems control plant cell shape: the microtubule cytoskeleton and the actin cytoskeleton. A protein complex known as ARP2/3 controls the production and distribution of actin in the cell and directs traffic along these roadways from its position at the tip of the trichome. How this protein complex influences the actin network of the cell was a major discovery, Szymanski said. The model produced a number of predictions about cell wall properties that Szymanski then verified in live trichomes. Understanding cell shape could also be used to model organ growth and development, he said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/pu-syi031015.php){:target="_blank" rel="noopener"}


