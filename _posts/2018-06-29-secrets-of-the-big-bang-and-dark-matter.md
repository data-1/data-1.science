---
layout: post
title: "Secrets of the Big Bang and dark matter"
date: 2018-06-29
categories:
author: "Karlsruhe Institute Of Technology"
tags: [Particle physics,Belle experiment,Physics,Matter,CP violation,Particle accelerator,Quark,Physical sciences,Science,Nature]
---


The new particle accelerator experiment Belle II searches for the origin of the universe. Credit: Felix Metzner, KIT  At the Japanese Research Center for Particle Physics KEK, the new particle accelerator experiment Belle II started operation after eight years of construction. In this particle accelerator, electrons with opposed anti-particles collide and produce heavy quarks and leptons, particles that no longer exist in today's universe. Credit: KEK  Several institutes of KIT have made important contributions to the Belle II experiment: The Institute for Theoretical Particle Physics was largely involved in the development of the planned physics program. Explore further Belle II measures first particle collisions

<hr>

[Visit Link](https://phys.org/news/2018-06-secrets-big-dark.html){:target="_blank" rel="noopener"}


