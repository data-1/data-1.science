---
layout: post
title: "Making organic molecules in hydrothermal vents in the absence of life"
date: 2015-07-05
categories:
author: Woods Hole Oceanographic Institution 
tags: [Hydrothermal vent,Methane,Carbon dioxide,Abiogenesis,Volcano,Von Damm Vent Field,Chemical reaction,Hydrogen,Oceanography,Earth,Life,Ocean,Earth sciences,Applied and interdisciplinary physics,Chemistry,Nature,Physical sciences,Physical geography]
---


New research by geochemists at Woods Hole Oceanographic Institution, published June 8 in the Proceedings of the National Academy of Sciences, is the first to show that methane formation does not occur during the relatively quick fluid circulation process, despite extraordinarily high hydrogen contents in the waters. Their research further reveals that another organic abiotic compound is formed during the vent circulation process at adjacent lower temperature, higher pH vents, but reaction rates are too slow to completely reduce the carbon all the way to methane. We also have a much better sense of how quickly these reactions are occurring in natural systems - some take thousands of years, while others only take hours to days. Finding out how methane and other organic species are formed in deep-sea hydrothermal systems is compelling because these compounds support modern day life, providing energy for microbial communities in the deep biosphere, and because of the potential role of abiotically-formed organic compounds in the origin of life. Instead CO2 and hydrogen combined to create an intermediate compound called formate - an important pre-biotic organic compound.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/whoi-mom060815.php){:target="_blank" rel="noopener"}


