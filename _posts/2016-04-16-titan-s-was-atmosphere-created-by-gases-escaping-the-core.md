---
layout: post
title: "Titan's was atmosphere created by gases escaping the core"
date: 2016-04-16
categories:
author: Elizabeth Howell, Astrobiology Magazine
tags: [Titan (moon),Huygens (spacecraft),Atmosphere of Titan,Atmosphere,Ganymede (moon),CassiniHuygens,Callisto (moon),Planetary core,Moon,Jupiter,Planetary science,Space science,Astronomy,Physical sciences,Solar System,Bodies of the Solar System,Planets of the Solar System,Outer space,Astronomical objects,Planets,Astronomical objects known since antiquity]
---


A decade later, we are only now starting to understand how the atmosphere of Titan formed, mostly based on what Huygens observed. Seeking noble gases  The Huygens probe found an isotope of argon—a noble gas also found in Earth's atmosphere—that appeared to be made within Titan's presumed rocky core. This would be similar in structure to what is hypothesized on Ganymede, the largest moon in the Solar System, and unlike that of Callisto, another large moon that is mostly undifferentiated, Glein said. Credit: NASA  According to Glein, some noble gases behave very similarly (in terms of how easily they form gases) to methane and nitrogen, which are the gases that give Titan an atmosphere. These noble gas analogies allowed Glein to calculate how much methane and nitrogen can go from the rocky core to the atmosphere, a distance of at least 500 km.

<hr>

[Visit Link](http://phys.org/news344763020.html){:target="_blank" rel="noopener"}


