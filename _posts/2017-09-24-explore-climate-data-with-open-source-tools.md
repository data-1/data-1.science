---
layout: post
title: "Explore climate data with open source tools"
date: 2017-09-24
categories:
author: "Don Watkins
(Correspondent)"
tags: [Red Hat,Open source,Software development,Software,Technology,Computing]
---


A quick search of the Internet led me to the Open Climate Workbench, a project that is part of the Apache Software Foundation. The Open Climate Workbench (OCW) develops software that performs climate model evaluation on data that comes from the Earth System Grid Federation, Coordinated Regional Climate Downscaling Environment, the U.S. You can also install the OCW in a virtual machine using Vagrant and a provider like VirtualBox. I found the easiest way to see how the OCW works is to download a virtual machine image from the Regional Climate Model Evaluation System (RCMES). The RCMES provides detailed instructions for downloading, importing, and running the virtual machine.

<hr>

[Visit Link](https://opensource.com/article/17/1/apache-open-climate-workbench){:target="_blank" rel="noopener"}


