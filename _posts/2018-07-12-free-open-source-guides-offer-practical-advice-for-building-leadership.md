---
layout: post
title: "Free Open Source Guides Offer Practical Advice for Building Leadership"
date: 2018-07-12
categories:
author: "Sam Dean"
tags: [Open source,Leadership,Linux,Linux Foundation,Management,Business]
---


The new Building Leadership in an Open Source Community guide provides practical advice that can help organizations build leadership and influence within open source projects. The path toward leadership is not always straightforward, however, so the latest Open Source Guide for the Enterprise from The TODO Group provides practical advice for building leadership in open source projects and communities.”  Indeed, the role of leadership in open source is often misunderstood, precisely because open source projects and communities are often structured to encourage highly distributed contribution models. Measuring Your Open Source Program’s Success. Recruiting Open Source Developers. “They come to understand that the only way to gain leadership is to earn the role within the community.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/free-open-source-guides-offer-practical-advice-for-building-leadership/){:target="_blank" rel="noopener"}


