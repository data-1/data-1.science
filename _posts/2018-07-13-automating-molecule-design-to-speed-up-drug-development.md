---
layout: post
title: "Automating molecule design to speed up drug development"
date: 2018-07-13
categories:
author: "Rob Matheson"
tags: [Simplified molecular-input line-entry system,Medicinal chemistry,Machine learning,Molecule,MIT Computer Science and Artificial Intelligence Laboratory,Chemistry,Applied mathematics,Branches of science,Computing,Technology]
---


The model basically takes as input molecular structure data and directly creates molecular graphs — detailed representations of a molecular structure, with nodes representing atoms and edges representing bonds. Both scaffold tree structure and molecular graph structure are encoded into their own vectors, where molecules are group together by similarity. For lead optimization, the model can then modify lead molecules based on a desired property. Given a desired property, the model optimizes a lead molecule by using the prediction algorithm to modify its vector — and, therefore, structure — by editing the molecule’s functional groups to achieve a higher potency score. They tested the model on tasks to generate valid molecules, find the best lead molecules, and design novel molecules with increase potencies.

<hr>

[Visit Link](http://news.mit.edu/2018/automating-molecule-design-speed-drug-development-0706){:target="_blank" rel="noopener"}


