---
layout: post
title: "Rare form: Novel structures built from DNA emerge"
date: 2016-05-15
categories:
author: Arizona State University
tags: [Tessellation,DNA nanotechnology,Technology,Chemistry]
---


F) fishnet pattern G) flower and bird design The completed nanostructures are seen in the accompanying atomic force microscopy images. In the current study, complex nano-forms displaying arbitrary wireframe architectures have been created, using a new set of design rules. The technique of DNA origami capitalizes on the simple base-pairing properties of DNA, a molecule built from the four nucleotides Adenine (A), Thymine (T) Cytosine (C) and (Guanine). Three such patterns were made, including a star shape, a 5-fold Penrose tile and an 8-fold quasicrystalline pattern. The new design rules were next tested with the assembly of increasingly complex nanostructures, involving vertices ranging from 2 to 10 arms, with many different angles and curvatures involved, including a complex pattern of birds and flowers.

<hr>

[Visit Link](http://phys.org/news/2015-07-rare-built-dna-emerge.html){:target="_blank" rel="noopener"}


