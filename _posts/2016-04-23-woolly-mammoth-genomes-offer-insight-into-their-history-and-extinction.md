---
layout: post
title: "Woolly mammoth genomes offer insight into their history and extinction"
date: 2016-04-23
categories:
author: Cell Press
tags: [Woolly mammoth,Mammoth,Biology,Genetics]
---


Credit: Love Dalén  Before the world's last woolly mammoth took its final breath, the iconic animals had already suffered from a considerable loss of genetic diversity. One of those mammoths, representing the last population on Russia's Wrangel Island, is estimated to have lived about 4,300 years ago. We found that the genome from one of the world's last mammoths displayed low genetic variation and a signature consistent with inbreeding, likely due to the small number of mammoths that managed to survive on Wrangel Island during the last 5,000 years of the species' existence, says Love Dalén of the Swedish Museum of Natural History. Sequencing ancient genomes is no easy feat, explains Dalén and Eleftheria Palkopoulou. A mammoth tusk by a river on the Taimyr Peninsula in Siberia.

<hr>

[Visit Link](http://phys.org/news349007907.html){:target="_blank" rel="noopener"}


