---
layout: post
title: "Scientists synthesize molecules that absorb any color of sunlight, from the oranges through the near-infrared"
date: 2015-04-03
categories:
author: Diana Lutz, Washington University In St. Louis
tags: [Chlorophyll,Chlorophyll a,Molecular orbital,Absorption spectroscopy,Color,Chemical bond,Absorption band,Chemistry,Physical sciences,Physical chemistry,Atomic molecular and optical physics,Applied and interdisciplinary physics]
---


Green plants rely on two pigments, chlorophylls a and b, to absorb the energy in sunlight. But the formyl group linked to the third carbon in chlorophyll d and at the second carbon in chlorophyll f shifts absorption to the red, outside the chlorophyll a bands. Bocian's calculations showed that the formyl on the seventh carbon of chlorophyll b draws electrons away from the core of the molecule along the molecular x axis. Credit: Holten et al.  Next they tried to push the absorption wavelengths of the bacteriochlorins as far into the near infrared as they could. If you come to us and say we'd like to have a molecule of this type that is going to absorb a narrow band of light centered on some particular red or near-infrared wavelength, say 750 nanometers, we could tell you how to synthesize one that will give the spectrum you want.

<hr>

[Visit Link](http://phys.org/news347210443.html){:target="_blank" rel="noopener"}


