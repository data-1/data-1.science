---
layout: post
title: "World's most sensitive dark matter detector completes search"
date: 2016-07-22
categories:
author: "Brown University"
tags: [Large Underground Xenon experiment,Sanford Underground Research Facility,Weakly interacting massive particles,XENON,Dark matter,Science,Astronomy,Nature,Physical sciences,Astrophysics,Physics,Particle physics]
---


That enables scientists to confidently eliminate many potential models for dark matter particles, offering critical guidance for the next generation of dark matter experiments. With this final result from the 2014 to 2016 search, the scientists of the LUX Collaboration have pushed the sensitivity of the instrument to a final performance level that is four times better than the original project goals. The LUX experiment was designed to look for weakly interacting massive particles, or WIMPs, the leading theoretical candidate for a dark matter particle. Among those next generation experiments will be the LUX-ZEPLIN (LZ) experiment, which will replace LUX at the Sanford Underground Research Facility. We expect LZ to achieve 70 times the sensitivity of LUX.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/bu-wms071916.php){:target="_blank" rel="noopener"}


