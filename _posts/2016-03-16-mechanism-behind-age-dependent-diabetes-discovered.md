---
layout: post
title: "Mechanism behind age-dependent diabetes discovered"
date: 2016-03-16
categories:
author: Karolinska Institutet
tags: [Beta cell,Type 2 diabetes,Mitochondrion,Insulin,Ageing,Diseases and disorders,Clinical medicine,Biochemistry,Cell biology,Biology,Biotechnology,Causes of death]
---


Ageing of insulin-secreting cells is coupled to a progressive decline in signal transduction and insulin release, according to a recent study by researchers at Karolinska Institutet in Sweden. As the investigators behind the recent study searched for a link between ageing, beta cell dysfunction and diabetes, they took a closer look at calcium ions. The second type of mouse represents naturally mature ageing, whereas the third is more resistant to age-induced deterioration. When comparing the mice, the investigators found that the function of the mitochondria is reduced with age. The defective metabolism-induced deterioration in calcium ion dynamics reflects an important age-dependent phenotype that may have a critical role in the development of type 2 diabetes, says Principal Investigator Per-Olof Berggren at The Rolf Luft Research Center for Diabetes and Endocrinology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/ki-mba091714.php){:target="_blank" rel="noopener"}


