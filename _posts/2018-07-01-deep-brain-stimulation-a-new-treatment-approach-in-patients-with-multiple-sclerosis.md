---
layout: post
title: "Deep brain stimulation: A new treatment approach in patients with multiple sclerosis"
date: 2018-07-01
categories:
author: "Charité - Universitätsmedizin Berlin"
tags: [Transcranial magnetic stimulation,Mental disorders,Clinical medicine,Health,Medicine,Diseases and disorders,Health sciences,Health care,Medical specialties,Causes of death,Human diseases and disorders,Neuroscience]
---


A pilot study has shown that treatment with deep transcranial magnetic stimulation (dTMS) significantly reduces symptoms of fatigue in patients with multiple sclerosis (MS). The results from this research suggest that TMS is a safe option for the treatment of patients with MS.  Up to 90 percent of people living with MS report experiencing severe fatigue. A team of researchers, led by Prof. Dr. Friedemann Paul of the NeuroCure Clinical Research Center (NCRC), has shown that dTMS, using the proprietary H-coil, a technology that allows brain stimulation three times deeper than that of standard TMS, is capable of producing significant improvements in fatigue symptoms. 33 study participants with fatigue received thrice-weekly sessions of dTMS for a duration of six weeks; this involves a stimulation H-coil being placed above the patient's head, which generates a magnetic field that influences nerve activity and neural circuits in the brain. We are excited about the collaboration between Prof. Dr. Friedemann Paul and his team at Charité's NeuroCure and Brainsway for studying the use of our patented Deep TMS technology for the benefit of MS patients, a new neurological field for us.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/02/180207120619.htm){:target="_blank" rel="noopener"}


