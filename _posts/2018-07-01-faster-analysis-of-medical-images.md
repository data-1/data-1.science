---
layout: post
title: "Faster analysis of medical images"
date: 2018-07-01
categories:
author: "Rob Matheson"
tags: [Image registration,Convolutional neural network,Neuroimaging,Branches of science,Computer science,Computing,Cognitive science,Technology,Science,Artificial intelligence,Applied mathematics,Computer vision]
---


In a pair of upcoming conference papers, MIT researchers describe a machine-learning algorithm that can register brain scans and other 3-D images more than 1,000 times more quickly using novel learning techniques. The algorithm works by “learning” while registering thousands of pairs of images. “After 100 registrations, you should have learned something from the alignment. During training, brain scans were fed into the algorithm in pairs. Some registration algorithms incorporate CNN models but require a “ground truth,” meaning another traditional algorithm is first run to compute accurate registrations.

<hr>

[Visit Link](http://news.mit.edu/2018/faster-analysis-of-medical-images-0618){:target="_blank" rel="noopener"}


