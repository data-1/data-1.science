---
layout: post
title: "Ariane 5’s second launch of 2015"
date: 2015-09-03
categories:
author: "$author"   
tags: [Ariane 5,DirecTV satellite fleet,Satellites,European Space Agency,European space programmes,Rockets and missiles,Rocket families,Space access,Space launch vehicles,Spacecraft,Space vehicles,Outer space,Spaceflight,Rocketry,Spaceflight technology,Astronautics,Flight,Space programs]
---


Directv-15, with a mass of about 6200 kg and mounted in the upper position atop Ariane’s Sylda dual-payload carrier, was the first to be released about 28 minutes into the mission. Sky Mexico-1 was released into its own transfer orbit about 10 minutes after the first satellite. The satellite has a design life of about 15 years. Sky Mexico-1, owned and operated by Sky Mexico, will be positioned at 78.8°W in geostationary orbit to provide high-definition direct TV broadcast services for Mexico, Central America and the Caribbean. The payload mass for this launch was 10130 kg.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Launchers/Ariane_5_s_second_launch_of_2015){:target="_blank" rel="noopener"}


