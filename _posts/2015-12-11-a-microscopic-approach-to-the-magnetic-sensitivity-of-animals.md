---
layout: post
title: "A microscopic approach to the magnetic sensitivity of animals"
date: 2015-12-11
categories:
author: University of Tokyo
tags: [Cryptochrome,Magnetoreception,Flavin adenine dinucleotide,Physical sciences,Chemistry]
---


Researchers at the University of Tokyo have succeeded in developing a new microscope capable of observing the magnetic sensitivity of photochemical reactions believed to be responsible for the ability of some animals to navigate in the Earth's magnetic field, on a scale small enough to follow these reactions taking place inside sub-cellular structures. However, to date there has been no way to measure the effect of magnetic fields on radical pairs in living cells. The research group of Associate Professor Jonathan Woodward at the Graduate School of Arts and Sciences are specialists in radical pair chemistry and investigating the magnetic sensitivity of biological systems. In this latest research, PhD student Lewis Antill made measurements using a special microscope to detect radical pairs formed from FAD, and the influence of very weak magnetic fields on their reactivity, in volumes less than 4 millionths of a billionth of a liter (4 femtoliters). Early View: 2015/6/3, doi: 10.1002/anie.201502591  Links  Graduate School of Arts and Sciences, The University of Tokyo  Organization for Programs on Environmental Sciences (OPES), Graduate School of Arts and Sciences, The University of Tokyo  Woodward Laboratory, Organization for Programs on Environmental Sciences (OPES), Graduate School of Arts and Sciences, The University of Tokyo  Associate Professor Jonathan R. Woodward  Graduate School of Arts and Sciences  The University of Tokyo  3-8-1 Komaba, Meguro-ku, Tokyo, 153-8902, Japan  About the University of Tokyo  The University of Tokyo is Japan's leading university and one of the world's top research universities.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/uot-ama060415.php){:target="_blank" rel="noopener"}


