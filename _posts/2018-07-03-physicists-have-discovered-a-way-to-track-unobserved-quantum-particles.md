---
layout: post
title: "Physicists Have Discovered a Way to Track Unobserved Quantum Particles"
date: 2018-07-03
categories:
author: ""
tags: [Quantum mechanics,Physics,Particle,Mechanics,Wave function,Wave,Concepts in metaphysics,Science,Theoretical physics,Scientific theories,Physical sciences,Applied and interdisciplinary physics,Scientific method]
---


Image Credit: Robert Couse-Baker  Within this new study, published in the journal Physical Review A, researchers from the University of Cambridge demonstrated that, by examining the way a quantum object interacts with its environment instead of measuring the object itself, you can track unobserved quantum particles. Also, in following these tags, the researchers found that they could decode the information from the particles at the end of an experiment when the particles were observed. The Forbidden Domain  This new way to track unobserved quantum particles could allow scientists to test old predictions in quantum mechanics. But the researchers in this study found that the information encoded into each quantum particle after each tagging interaction is directly related to the wave function. This research could help to support continuing efforts to understand the movement and behavior of quantum particles and wave particles.

<hr>

[Visit Link](https://futurism.com/tracking-unobserved-quantum-particles/){:target="_blank" rel="noopener"}


