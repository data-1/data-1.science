---
layout: post
title: "Special issue: Forest health"
date: 2015-09-07
categories:
author: American Association for the Advancement of Science (AAAS) 
tags: [Forest,Biodiversity,Drought,Taiga,Human impact on the environment,Tree,Sustainability,Hemlock woolly adelgid,Wildfire,Physical geography,Environmental science,Earth phenomena,Ecology,Systems ecology,Global environmental issues,Environmental social science,Environmental conservation,Nature,Earth sciences,Biogeochemistry,Environment,Natural environment]
---


The special issue is complemented by a package from Science's news department. In a second Review, Sylvie Gauthier et al. discuss the traditionally resilient boreal forest, which now faces unprecedented rates of change and new threats - warmer temperatures could expand the range of pests and a drier climate could lead to extensive forest cover thinning, or loss. While trees have withstood periods of drought for millennia, the authors note that recent increase in the occurrence of hotter droughts is testing trees' ability to survive new extremes. Forest fires, and megafires in particular, are a major concern, and could cause severe transformation of the temperate forests of today. Together, this collection on forest health highlights the importance and susceptibility of one of land's most crucial plants in the face of a changing climate.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/aaft-sif081715.php){:target="_blank" rel="noopener"}


