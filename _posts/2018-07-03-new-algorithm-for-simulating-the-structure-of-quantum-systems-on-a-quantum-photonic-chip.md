---
layout: post
title: "New algorithm for simulating the structure of quantum systems on a quantum photonic chip"
date: 2018-07-03
categories:
author: "University Of Bristol"
tags: [Quantum mechanics,Quantum computing,Physics,Photon,Energy level,Particle physics,Quantum state,Energy,Scientific method,Branches of science,Technology,Applied and interdisciplinary physics,Physical sciences,Theoretical physics,Science]
---


Credit: University of Bristol  An international collaboration of quantum physicists from the University of Bristol, Microsoft, Google, Imperial College, Max Planck Institute, and the Sun Yat-sen University have introduced a new algorithm to solve the energy structure of quantum systems on quantum computers. In particular, this new algorithm is capable of finding the excited-states in a way that seems to have no direct analogue on a classical computer, providing a new way of studying physics and chemistry at the microscopic level. Fundamental chemical and physical properties of systems can be characterised by finding a particular set of quantized states called eigenstates which contains the ground state of the system (the state with minimal energy) and excited states (stationary states with higher energies). It is expected that large quantum computers will be able to simulate complex chemical systems, a task impossible for classical computers, increasing our knowledge of physics and chemistry. Lead author Dr Raffaele Santagati said: In this work we provide a new tool for studying the properties of quantum systems with quantum computers.

<hr>

[Visit Link](https://phys.org/news/2018-01-algorithm-simulating-quantum-photonic-chip.html){:target="_blank" rel="noopener"}


