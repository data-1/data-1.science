---
layout: post
title: "A better grasp of primate grip"
date: 2015-12-09
categories:
author: Yale University 
tags: [Primate,Hand,Thumb,Human evolution,Human,Australopithecus,Science,Branches of science]
---


In a new study, a research team led by Yale University found that even the oldest known human ancestors may have had precision grip capabilities comparable to modern humans. This includes Australopithecus afarensis, which appears in the fossil record a million years before the first evidence of stone tools. Using measurements of the digits' segments, the team created a kinematic model of the thumb and index finger of the skeletons of living primates and fossil remains of human ancestors. It is the first such model of digit movement during precision grasping and manipulation in a broad sample of humans, non-human primates, and fossil hominins. Yet there remains debate about the gripping capabilities of early fossil hominins, especially regarding the use of tools.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/yu-abg042015.php){:target="_blank" rel="noopener"}


