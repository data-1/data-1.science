---
layout: post
title: "Extremely rare footage of a tuatara hatching captured on film (w/ Video)"
date: 2016-03-29
categories:
author: Victoria University Of Wellington
tags: [Tuatara,Computing,Technology,Cyberspace,Communication,Internet]
---


Last to hatch, the egg was one of 23 being incubated in captivity this year as part of a joint initiative that has helped to save a threatened population of tuatara from extinction. Using a low-cost microcomputer and infrared camera, Warren Butcher from Victoria's Image Services team filmed seven hours of footage and then compressed it into a short video clip. Since the early 1990s, Victoria University, the Department of Conservation and local Mana Whenua Ngati Manuhiri have run an intensive conservation recovery plan for tuatara on Hauturu ō Toi/Little Barrier Island, partly-funded by the Hauturu Supporters Trust and Auckland Zoo. Hauturu ō Toi is a nature reserve located 80 kilometres north-east of Auckland, now home to around 300 tuatara, 255 of which have been incubated at Victoria University. The star hatchling will be released there in a few years' time.

<hr>

[Visit Link](http://phys.org/news333264244.html){:target="_blank" rel="noopener"}


