---
layout: post
title: "Climate Change Is Acidifying Our Lakes and Rivers the Same Way It Does With Oceans"
date: 2018-08-18
categories:
author: ""
tags: [Ocean acidification,Carbon dioxide,Water,Environment,Environmental engineering,Environmental chemistry,Environmental technology,Ecology,Earth phenomena,Environmental issues with fossil fuels,Earth sciences,Natural environment,Physical geography,Environmental science,Human impact on the environment,Climate change,Climate variability and change,Environmental issues,Applied and interdisciplinary physics,Environmental impact,Global environmental issues,Hydrology,Nature,Systems ecology,Oceanography,Societal collapse]
---


Acidifying Water  Earth's oceans absorb about 40% of all the carbon dioxide (CO2) that humans emit into the atmosphere. Wildlife Danger  It is likely, according to this study, that freshwater systems absorb CO2 in different ways than oceans do. To explore this further, the team documented the effects of this acidification in freshwater crustaceans —two species of Daphnia, which are known as water fleas. In ocean studies, acidification affects animals' abilities to form shells, but these freshwater species had before not been studied. This body of research still needs further exploration, but it is clear that climate change is contributing to acidification in freshwater bodies of water as well as the oceans.

<hr>

[Visit Link](https://futurism.com/climate-change-acidifying-lakes-rivers-same-way-oceans/){:target="_blank" rel="noopener"}


