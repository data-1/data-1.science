---
layout: post
title: "Why do strawberries taste so good?"
date: 2016-05-10
categories:
author: Simon Cotton, The Conversation
tags: [Ripening,Strawberry,Odor,Taste]
---


Some 500 years ago, the wood strawberry, Fragaria vesca, was around in Europe and the musk strawberry, Fragaria moschata, was starting to be cultivated. The balance of sweetness and acidity is very important to the taste of a strawberry. But how do scientists know which molecules are responsible for taste and smell? They can make up 90% of the aroma molecules from a strawberry. In their quest for better tasting fruit, scientists are starting to investigate the genes responsible for making particular flavour molecules.

<hr>

[Visit Link](http://phys.org/news355475951.html){:target="_blank" rel="noopener"}


