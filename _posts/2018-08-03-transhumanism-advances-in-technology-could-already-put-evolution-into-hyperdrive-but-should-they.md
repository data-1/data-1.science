---
layout: post
title: "Transhumanism—advances in technology could already put evolution into hyperdrive – but should they?"
date: 2018-08-03
categories:
author: "David Trippett, The Conversation"
tags: [Transhumanism,Hearing,Technology,Sound,Cognitive science]
---


Its advocates believe in fundamentally enhancing the human condition through applied reason and a corporeal embrace of new technologies. Take the hypothetical augmentation of human hearing, something I am researching within a broader project on sound and materialism. But human hearing is already being augmented. This raises heady questions about the very definition of sound. Must it be perceived by a human ear to constitute sound?

<hr>

[Visit Link](https://phys.org/news/2018-03-transhumanismadvances-technology-evolution-hyperdrive.html){:target="_blank" rel="noopener"}


