---
layout: post
title: "Yale scientists use gene editing to correct mutation in cystic fibrosis"
date: 2016-04-24
categories:
author: Yale University
tags: [Gene,Peptide nucleic acid,Mutation,Genome editing,Cystic fibrosis,DNA,Genetics,Cell (biology),Genetic disorder,Medicine,Clinical medicine,Health sciences,Biotechnology,Molecular biology,Biology,Life sciences,Biochemistry,Branches of genetics,Medical specialties]
---


New Haven, Conn. -- Yale researchers successfully corrected the most common mutation in the gene that causes cystic fibrosis, a lethal genetic disorder. The study was published April 27 in Nature Communications. To correct the mutation, a multidisciplinary team of Yale researchers developed a novel approach. In both human airway cells and mouse nasal cells, the researchers observed corrections in the targeted genes. The technology could be used as a way to fix the basic genetic defect in cystic fibrosis.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/yu-ysu042715.php){:target="_blank" rel="noopener"}


