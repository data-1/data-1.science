---
layout: post
title: "Chage Command in Linux"
date: 2018-04-28
categories:
author: ""
tags: [Sudo,User (computing),Password,Command-line interface,Information technology management,Software,Software engineering,Utility software,Computing,Information Age,Software development,Technology,Digital media,Computer science,System software]
---


Linux provides the chage, also known as change age, a command-line utility to check, set, and update the password expiration date. Chage Command  The chage command displays and updates the user password expiry information. To set or change the account expiration date use -E option. Set Account Inactivity Period  Use the chage -I command to specify the total number of days in which the account password will be inactive if the expired is not changed. sudo chage -I 10 tom  Use value -1 to set password inactive days to never.

<hr>

[Visit Link](https://linoxide.com/linux-how-to/linux-reset-passwords-expire-number-days/){:target="_blank" rel="noopener"}


