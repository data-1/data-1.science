---
layout: post
title: "Genetic 'fossils' reveal long-term viral partnerships in grass: Within host cells, partner viruses may exchange genetic material and evolve together over time"
date: 2017-10-06
categories:
author: "PLOS"
tags: [Virus,Life,Evolution,Genome,Baltimore classification,Genetics,DNA,Host (biology),News aggregator,Microbiology,Biological evolution,Life sciences,Biology]
---


Specifically, they were interested in fossils of viruses known as pararetroviruses (PRVs), which don't integrate into host DNA as part of their normal life cycle, but may incidentally leave viral fossil in host genomes while retaining the ability to complete their life cycle. The researchers sequenced and analyzed PRV fossils integrated into modern grass genomes and identified several different PRV species, three of which were found to be defective. They found that one defective PRV species may have evolved to form a commensal relationship with a non-defective species, allowing it to use the non-defective protein machinery to complete its life cycle. Evidence suggests that the other two defective PRVs may have developed a mutualistic partnership, with each species providing complementary protein machinery lacked by the other species. This concerted evolution has resulted in the partnered species sharing highly similar NRSs, and it appears to have enabled development and maintenance of the partnerships over time.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/06/170629143004.htm){:target="_blank" rel="noopener"}


