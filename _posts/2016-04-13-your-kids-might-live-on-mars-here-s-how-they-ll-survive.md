---
layout: post
title: "Your kids might live on Mars. Here's how they'll survive"
date: 2016-04-13
categories:
author: Stephen Petranek
tags: [TED (conference)]
---


It sounds like science fiction, but journalist Stephen Petranek considers it fact: within 20 years, humans will live on Mars. In this provocative talk, Petranek makes the case that humans will become a spacefaring species and describes in fascinating detail how we'll make Mars our next home. Humans will survive no matter what happens on Earth, Petranek says. We will never be the last of our kind.

<hr>

[Visit Link](http://www.ted.com/talks/stephen_petranek_your_kids_might_live_on_mars_here_s_how_they_ll_survive){:target="_blank" rel="noopener"}


