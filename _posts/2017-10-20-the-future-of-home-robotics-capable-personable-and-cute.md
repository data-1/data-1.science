---
layout: post
title: "The Future Of Home Robotics: Capable, Personable And Cute?"
date: 2017-10-20
categories:
author:  
tags: [General Electric,Technology,Robot,Innovation,Robotics,Economy]
---


With parts becoming cheaper, useful home assistant robots that can navigate around your home and be aware of their surroundings can finally be made at a price point that is attractive to a wider consumer market. At this point in time, robot capabilities such as mapping and navigation are still more difficult to implement than voice recognition capabilities. We’re seeing that the capabilities of a robot are important, but even more important are the emotional interactions that take place between robots and humans. As the technology improves, cost lowers, and consumer expectation increases, home robots will ultimately become a form of art and entertainment. The home robot will be not just a capable assistant, but something with personality—life-like, a companion in the home that you actually like having around.

<hr>

[Visit Link](http://www.gereports.com/future-home-robotics-capable-personable-cute/){:target="_blank" rel="noopener"}


