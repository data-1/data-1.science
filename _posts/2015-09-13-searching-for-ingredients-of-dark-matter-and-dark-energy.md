---
layout: post
title: "Searching for ingredients of dark matter and dark energy"
date: 2015-09-13
categories:
author: American Association for the Advancement of Science (AAAS) 
tags: [Weakly interacting massive particles,XENON,Science,Cosmology,Astronomy,Physical cosmology,Astrophysics,Theoretical physics,Nature,Physics,Particle physics,Physical sciences]
---


This news release is available in Japanese. Two new reports advance efforts to identify components of dark matter and energy, which together comprise about 95% of the universe yet leave much to scientists' imaginations. Both experiments illustrate how basic questions about the universe's development can be addressed by laboratory-scale experiments. While gravitational processes have been thought to involve standard model particles like neutrinos and photons, more recent studies of the physical processes forming our universe suggest new particle types - like WIMPs (or weakly interacting massive particles) - are involved. Paul Hamilton et al. searched for a hypothetical force called a chameleon field, one of the most prominent candidates for dark energy - a force thought to have propelled the expansion of the universe.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/aaft-sfi081715.php){:target="_blank" rel="noopener"}


