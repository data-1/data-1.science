---
layout: post
title: "Discovery provides insights on how plants respond to elevated CO2 levels"
date: 2014-08-17
categories:
author: University Of California - San Diego
tags: [Stoma,Carbon dioxide,Protein,Epidermis (botany),Biology,Nature]
---


Because elevated CO 2 reduces the density of stomatal pores in leaves, this is, at first sight beneficial for plants as they would lose less water. Our research is aimed at understanding the fundamental mechanisms and genes by which CO 2 represses stomatal pore development, says Schroeder. We identified CRSP, a secreted protein, which is responsive to atmospheric CO 2 levels, says Engineer. You can imagine that such a 'sensing and response' mechanism involving CRSP and EPF2 could be used to engineer crop varieties which are better able to perform in the current and future high CO 2 global climate where fresh water availability for agriculture is dwindling. The discoveries of these proteins and genes have the potential to address a wide range of critical agricultural problems in the future, including the limited availability of water for crops, the need to increase water use efficiency in lawns as well as crops and concerns among farmers about the impact heat stress will have in their crops as global temperatures and CO 2 levels continue to rise.

<hr>

[Visit Link](http://phys.org/news323856466.html){:target="_blank" rel="noopener"}


