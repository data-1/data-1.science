---
layout: post
title: "Extreme makeover: Humankind's unprecedented transformation of Earth"
date: 2016-05-10
categories:
author: University of Leicester
tags: [Anthropocene,Human,News aggregator,Science,Natural environment,Nature,Earth sciences]
---


Professor Jan Zalasiewicz from the University of Leicester's Department of Geology who was involved in the study explained the research: We are used to seeing headlines daily about environmental crises: global warming, ocean acidification, pollution of all kinds, looming extinctions. These changes are advancing so rapidly, that the concept that we are living in a new geological period of time, the Anthropocene Epoch -- proposed by the Nobel Prize-winning atmospheric chemist Paul Crutzen -- is now in wide currency, with new and distinctive rock strata being formed that will persist far into the future. Episodes of global warming, ocean acidification and mass extinction have all happened before, well before humans arrived on the planet. The team examined what makes the Anthropocene special and different from previous crises in Earth's history. They identified four key changes:  The homogenization of species around the world through mass, human-instigated species invasions -- nothing on this global scale has happened before  One species, Homo sapiens, is now in effect the top predator on land and in the sea, and has commandeered for its use over a quarter of global biological productivity.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150629080158.htm){:target="_blank" rel="noopener"}


