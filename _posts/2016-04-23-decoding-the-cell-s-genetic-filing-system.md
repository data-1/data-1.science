---
layout: post
title: "Decoding the cell's genetic filing system"
date: 2016-04-23
categories:
author: Princeton  University
tags: [Histone,Chromatin,Epigenetics,DNA,Nucleosome,DNA sequencing,Protein splicing,Protein,Genetics,Gene,Transcription (biology),Chromatin immunoprecipitation,Biomolecules,Biochemistry,Molecular biology,Biotechnology,Cellular processes,Branches of genetics,Structural biology,Biology,Proteins,Life sciences,Cell biology,Macromolecules,Chemistry,Molecular genetics]
---


Published on April 6 in the journal Nature Chemistry, this work is the latest chemical contribution from the Muir lab towards understanding nature's remarkable information indexing system. There have been incredible advances in genetic sequencing over the last 10 years that have made this work possible, said Manuel Müller, a postdoctoral researcher in the Muir lab and co-author on the Nature Methods article. Within minutes of being introduced into the cell, the labeled intein fragment bound to the histone intein fragment. It's really a beautiful way to engineer proteins in a cell, David said. ; Muir, T.W.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/pu-dtc042215.php){:target="_blank" rel="noopener"}


