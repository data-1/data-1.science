---
layout: post
title: "Experiment resolves mystery about wind flows on Jupiter"
date: 2017-09-23
categories:
author: "University of California - Los Angeles"
tags: [Juno (spacecraft),Jupiter,Planet,Astronomical objects,Solar System,Planets,Physical sciences,Planetary science,Science,Outer space,Astronomy,Space science]
---


The challenge to re-creating swirling winds in the lab was building a model of a planet with three key attributes believed to be necessary for jets to form: rapid rotation, turbulence and a curvature effect that mimics the spherical shape of a planet. Previous attempts to create jets in a lab often failed because researchers couldn't spin their models fast enough or create enough turbulence, Aurnou said. The Juno data from the very first flyby of Jupiter showed that structures of ammonia gas extended over 60 miles into Jupiter's interior, which was a big shock to the Juno science team, Aurnou said. This year, Aurnou and his team will use supercomputers at Argonne National Laboratory in Argonne, Illinois, to simulate the dynamics of Jupiter's interior and atmosphere. They'll also continue their work at the laboratory in Marseilles to make the spinning table simulation more complex and more realistic.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/uoc--erm012017.php){:target="_blank" rel="noopener"}


