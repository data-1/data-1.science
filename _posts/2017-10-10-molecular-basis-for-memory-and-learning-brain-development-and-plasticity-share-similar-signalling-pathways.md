---
layout: post
title: "Molecular basis for memory and learning: Brain development and plasticity share similar signalling pathways"
date: 2017-10-10
categories:
author: "Goethe-Universität Frankfurt am Main"
tags: [Low-density lipoprotein receptor-related protein 8,Synaptic plasticity,Cell signaling,Memory,Brain,Neuroscience,Cell biology,Biochemistry]
---


Learning and memory are two important functions of the brain that are based on the brain's plasticity. The Frankfurt scientists now discovered that three key molecules are involved in this regulation: GRIP1, ephrinB2 and ApoER2, the latter being a receptor for the signalling molecule Reelin. These results are fascinating since it has been known for years that ephrinB2 as well as Reelin are essential for the development of the brain  explains Amparo Acker-Palmer. Both, ApoER2 and ephrinB2 molecules have been linked to the development of Alzheimer's, although the mechanisms of action are not clear yet, says Amparo Acker-Palmer. With our research we not only discovered new interactions of key molecules for the regulation of learning and memory but also shed light on potential new therapeutic targets for the treatment of Alzheimer's disease.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171009093207.htm){:target="_blank" rel="noopener"}


