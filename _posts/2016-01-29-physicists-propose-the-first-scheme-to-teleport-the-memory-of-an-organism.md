---
layout: post
title: "Physicists propose the first scheme to teleport the memory of an organism"
date: 2016-01-29
categories:
author: Science China Press
tags: [Quantum mechanics,Quantum teleportation,Electron,Physics,Scientific theories,Science,Physical sciences,Applied and interdisciplinary physics,Theoretical physics]
---


The internal state (an electron spin) or the center-of-mass motion state of a microorganism on an electromechanical oscillator can be teleported to a remote microorganism on another electromechanical oscillator assisted with superconducting circuits. In a recent study, Prof. Tongcang Li at Purdue University and Dr. Zhang-qi Yin at Tsinghua University proposed the first scheme to use electromechanical oscillators and superconducting circuits to teleport the internal quantum state (memory) and center-of-mass motion state of a microorganism. Besides photons, quantum teleportation with atoms, ions, and superconducting circuits have been demonstrated. In a recent study, Tongcang Li and Zhang-qi Yin propose to put a bacterium on top of an electromechanical membrane oscillator integrated with a superconducting circuit to prepare the quantum superposition state of a microorganism and teleport its quantum state. Since internal states of an organism contain information, this proposal provides a scheme for teleporting information or memories between two remote organisms.

<hr>

[Visit Link](http://phys.org/news/2016-01-physicists-scheme-teleport-memory.html){:target="_blank" rel="noopener"}


