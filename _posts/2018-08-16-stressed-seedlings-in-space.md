---
layout: post
title: "Stressed seedlings in space"
date: 2018-08-16
categories:
author: ""
tags: [Plant,Plants in space,Plants,Agriculture,Botany]
---


Science & Exploration Stressed seedlings in space 07/11/2017 5958 views 112 likes  Life on Earth has a myriad of problems, but gravity isn’t one of them – staying grounded means organisms can soak up the light and heat that enables growth. Like humans, plants have proven their robustness in space. Now, thanks to the International Space Station, we know more on how they cope with weightlessness. Germinated in prepacked cassettes monitored by ground control, the seedlings were harvested after six days, frozen or preserved and returned to Earth for inspection. Obviously, seedlings in microgravity grew random roots but they still managed to grow.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Research/Stressed_seedlings_in_space){:target="_blank" rel="noopener"}


