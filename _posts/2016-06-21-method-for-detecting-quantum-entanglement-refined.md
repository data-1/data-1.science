---
layout: post
title: "Method for detecting quantum entanglement refined"
date: 2016-06-21
categories:
author: "RMIT University"
tags: [Quantum entanglement,Quantum computing,Computing,News aggregator,Theoretical physics,Theoretical computer science,Quantum information science,Technology,Applied mathematics,Science,Branches of science,Physics,Quantum mechanics]
---


RMIT quantum computing researchers have developed and demonstrated a method capable of efficiently detecting high-dimensional entanglement. Full-scale quantum computing relies heavily on entanglement between the individual particles used to store information, the quantum bits, or qubits. Quantum computing promises to exponentially speed up certain tasks because entanglement allows a vastly increased amount of information to be stored and processed with the same number of qubits. A higher dimensional state, however, is a particle that contains a message that can be 0, 1, 2 or more, so much more information can be stored and transmitted. In the future when quantum computers become available, our method can potentially serve as a tool in certifying whether the system has enough entanglement between the qubits.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/06/160617082309.htm){:target="_blank" rel="noopener"}


