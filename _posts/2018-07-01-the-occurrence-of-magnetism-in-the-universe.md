---
layout: post
title: "The occurrence of magnetism in the universe"
date: 2018-07-01
categories:
author: "Helmholtz Association Of German Research Centres"
tags: [Dynamo theory,Precession,Earth,Fluid dynamics,Physics,Science,Physical sciences]
---


A precession driven flow is supposed to power magnetic field self-excitation in a planned liquid metal dynamo experiment at HZDR. Credit: HZDR  Flows of molten metal can generate magnetic fields. To our surprise, we observed a symmetrical double role structure in a specific range of the precession rate, which should provide a dynamo effect at a magnetic Reynolds number of 430, says the physicist. The Earth's rotational axis is tilted by 23.5 degrees from its orbital plane. For example, we know at which rotational rates the dynamo effect occurs and which magnetic field structures we can expect, says Giesecke.

<hr>

[Visit Link](https://phys.org/news/2018-03-occurrence-magnetism-universe.html){:target="_blank" rel="noopener"}


