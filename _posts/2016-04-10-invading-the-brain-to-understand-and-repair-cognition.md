---
layout: post
title: "Invading the brain to understand and repair cognition"
date: 2016-04-10
categories:
author: Cognitive Neuroscience Society
tags: [Brain,Memory,Epilepsy,Neuroscience,Neuromodulation (medicine),Cognitive science]
---


These new technologies, being presented today at the Cognitive Neuroscience Society (CNS) annual conference in New York City, are mapping new understandings of cognition and advancing efforts to improve memory and learning in patients with cognitive deficits. That data, combined with new data mining and processing techniques, has led to an explosion in studies involving humans. Short of that, scientists may be able to decouple IEDs from electrochemical signals associated with memory consolidation. In work previously only possible in animals, scientists are now recording these signals directly from the brain and in some cases trying to stimulate the brain with those signals to help patients with cognitive deficits. The team is using these already-implanted electrodes to record and stimulate the activity of single neurons and small neuronal populations during the patients' 2-3-week-long hospitalization.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/cns-itb040416.php){:target="_blank" rel="noopener"}


