---
layout: post
title: "Google's AI program: Building better algorithms for detecting eye disease"
date: 2018-07-18
categories:
author: "American Academy of Ophthalmology"
tags: [Diabetic retinopathy,Ophthalmology,Artificial intelligence,Branches of science,Medical specialties,Clinical medicine,Health care,Health sciences,Medicine,Health]
---


More than 29 million Americans have diabetes, and are at risk for diabetic retinopathy, a potentially blinding eye disease. In earlier research, Dr. Peng and her team used neural networks--complex mathematical systems for identifying patterns in data--to recognize diabetic retinopathy. Dr. Peng showed the software worked roughly as well as human experts. To tease out how this could be done, Dr. Peng compared the performance of the original algorithm with manual image grading by either a majority decision of three general ophthalmologists, or a consensus grading by three retinal specialists. At the end of the process, the retina specialists indicated that the precision used in the decision process was above that typically used in everyday clinical practice.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-03/aaoo-gap031218.php){:target="_blank" rel="noopener"}


