---
layout: post
title: "European Space Agency’s Philae Probe Sends Astounding First Images from Comet Landing"
date: 2015-07-17
categories:
author: Beverley Mitchell
tags: []
---


Continue reading below Our Featured Videos  Of the successful landing, ESA’s Director General Jean-Jacques Dordain said in a statement: “ESA and its Rosetta mission partners achieved something extraordinary today. Our ambitious Rosetta mission has secured another place in the history books: not only is it the first to rendezvous with and orbit a comet, but it is now also the first to deliver a probe to a comet’s surface.” Stephan Ulamec, Philae Lander Manager at the DLR German Aerospace Center, added: “We are extremely relieved to be safely on the surface of the comet, especially given the extra challenge of the comet’s unusual shape and unexpectedly hazardous surface.”  The probe’s orbiter craft, Rosetta, was launched on 2 March, 2004, and it journeyed 6.4 billion kilometers (3.914 billion miles) before reaching the comet on 6 August, 2014. Jets of gas and dust stream from the surface. Related: European Space Agency Launches MetOp-B Satellite to Track Climate Change, Deliver Highly Accurate Weather Reports  Join Our Newsletter Receive the latest in global news and designs building a better future. Via Gigaom and The New York Times  Lead image: Rosetta’s lander Philae is safely on the surface of Comet 67P/Churyumov-Gerasimenko, as these first two CIVA images confirm.

<hr>

[Visit Link](http://inhabitat.com/european-space-agencys-philae-probe-sends-astounding-first-images-from-comet-landing/){:target="_blank" rel="noopener"}


