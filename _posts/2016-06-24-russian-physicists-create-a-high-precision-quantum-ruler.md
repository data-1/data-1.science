---
layout: post
title: "Russian physicists create a high-precision 'quantum ruler'"
date: 2016-06-24
categories:
author: "Moscow Institute of Physics and Technology"
tags: [Interferometry,Quantum mechanics,Optics,Quantum entanglement,Atomic molecular and optical physics,Physical chemistry,Physical sciences,Theoretical physics,Waves,Applied and interdisciplinary physics,Science,Natural philosophy,Electromagnetism,Electromagnetic radiation,Physics]
---


This technique will enable us to use quantum effects to increase the accuracy of measuring the distance between observers that are separated from one another by a medium with losses. These states could be important for metrology, or, more precisely, they could significantly improve the capabilities of optical interferometers, such as those used to detect gravitational waves in the LIGO project. In our experiment conducted at the RQC laboratory, Alice and Bob create two entangled states. The send one of the parts to a medium with losses, which in our experiment is simulated by darkened glass. This results in entanglement swapping: the remaining parts of Alice and Bob's states are in the N00N state.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/miop-rpc062316.php){:target="_blank" rel="noopener"}


