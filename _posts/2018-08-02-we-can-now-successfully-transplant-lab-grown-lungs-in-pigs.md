---
layout: post
title: "We Can Now Successfully Transplant Lab-Grown Lungs in Pigs"
date: 2018-08-02
categories:
author: ""
tags: [Organ transplantation,Organ donation,Tissue engineering,Lung,Biology,Anatomy,Organs (anatomy),Medicine,Medical specialties,Clinical medicine]
---


In it, they detail their latest milestone along the path to creating lab-grown lungs for humans: they can now successfully transplant these bioengineered lungs into pigs. They then added cells from recipient pigs' own lungs to each of the scaffolds and let the lungs grow for 30 days. Bioengineered organs are something of a holy grail in transplantation research. Because they are grown from the recipient's own cells, the body is less likely to reject the organ, and we could grow them in the lab as needed — no more organ shortages. READ MORE: Production and Transplantation of Bioengineered Lung Into a Large-Animal Model [Science Translational Medicine]  More on bioengineering: Researchers Can Now Bioengineer Lungs With the Original Blood Vessels Intact

<hr>

[Visit Link](https://futurism.com/lab-grown-lungs-transplant/){:target="_blank" rel="noopener"}


