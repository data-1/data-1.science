---
layout: post
title: "Meteorite that doomed dinosaurs remade forests"
date: 2015-09-14
categories:
author: University of Arizona 
tags: [Leaf,Plant,Hell Creek Formation,Deciduous,Flowering plant,Nature,Earth sciences,Natural environment]
---


The impact decimated slow-growing evergreens and made way for fast-growing, deciduous plants, according to a study applying biomechanical analyses to fossilized leaves  The meteorite impact that spelled doom for the dinosaurs 66 million years ago decimated the evergreens among the flowering plants to a much greater extent than their deciduous peers, according to a study led by UA researchers. Our study provides evidence of a dramatic shift from slow-growing plants to fast-growing species, he said. By analyzing leaves, which convert carbon dioxide from the atmosphere and water into nutrients for the plant, the study followed a new approach that enabled the researchers to predict how plant species used carbon and water, shedding light on the ecological strategies of plant communities long gone, hidden under sediments for many millions of years. In other words, how much carbon the plant had invested in the leaf. The analyses revealed that while slow-growing evergreens dominated the plant assemblages before the extinction event, fast-growing flowering species had taken their places afterward.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/uoa-mtd091114.php){:target="_blank" rel="noopener"}


