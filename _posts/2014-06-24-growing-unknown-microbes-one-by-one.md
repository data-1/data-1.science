---
layout: post
title: "Growing unknown microbes one by one"
date: 2014-06-24
categories:
author: Jessica Stoller-Conrad, California Institute Of Technology
tags: [DNA sequencing,Bacteria,Microorganism,Human microbiome,Human Microbiome Project,Microbiome,Privacy,Organism,Genetics,Organisms,Life sciences,Biology]
---


To do this, Liang Ma, a postdoctoral scholar in Ismagilov's lab, developed a way to isolate and cultivate individual bacterial species of interest. To grow these elusive microbes, the Caltech researchers turned to SlipChip, a microfluidic device previously developed in Ismagilov's lab. Since bacteria often depend on nutrients and signals from the extracellular environment to support growth, the substances from this fluid were used to recreate this environment within the tiny SlipChip compartment—a key to successfully growing the difficult organism in the lab. Although a genomic sequence of the new organism is a useful tool, further studies are needed to learn how this species of microbe is involved in human health, Ismagilov says. The technique, says Ismagilov, allows researchers to target specific microbes in a way that was not previously possible.

<hr>

[Visit Link](http://phys.org/news322813197.html){:target="_blank" rel="noopener"}


