---
layout: post
title: "Challenging existing models of black holes"
date: 2018-06-21
categories:
author: "University of Texas at San Antonio"
tags: [Star,News aggregator,Black hole,Astronomy,Natural sciences,Science,Astronomical objects,Space science,Physics,Physical sciences]
---


Chris Packham, associate professor of physics and astronomy at The University of Texas at San Antonio (UTSA), has collaborated on a new study that expands the scientific community's understanding of black holes in our galaxy and the magnetic fields that surround them. Packham and astronomers lead from the University of Florida observed the magnetic field of a black hole within our own galaxy from multiple wavelengths for the first time. A black hole has a magnetic field as it was created from the remnant of a star after the explosion. As matter is broken down around a black hole, jets of electrons are launched by the magnetic field from either pole of the black hole at almost the speed of light. Our results are surprising and one that we're still trying to puzzle out.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/01/180119085942.htm){:target="_blank" rel="noopener"}


