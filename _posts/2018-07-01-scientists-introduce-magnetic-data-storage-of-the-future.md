---
layout: post
title: "Scientists introduce magnetic data storage of the future"
date: 2018-07-01
categories:
author: ""
tags: [Computer data storage,Single-molecule magnet,Dysprosium,Physical sciences,Chemistry,Technology,Materials]
---


Their findings were published recently in the Angewandte Chemie journal. Summarising the project objectives on CORDIS, she added: The potential is huge for SMM systems that would demonstrate magnetic field and light-driven changes in both their optical and magnetic properties because they could reproduce on a single molecule the same type of magneto-optical effects which are used for some current data storage technologies. For instance, single atoms and SMMs could be cooled with liquid helium at a temperature of -269 °C. This is important for their potential use in the magnetic storage of information, according to the team. The PhotoSMM (Single Molecule Magnets light-switching with photochromic ligands) project will demonstrate that a light input can induce a modification of the magnetic and optical properties of monometallic or bimetallic SMMs.

<hr>

[Visit Link](https://phys.org/news/2018-04-scientists-magnetic-storage-future.html){:target="_blank" rel="noopener"}


