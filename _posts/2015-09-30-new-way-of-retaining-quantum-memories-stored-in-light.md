---
layout: post
title: "New way of retaining quantum memories stored in light"
date: 2015-09-30
categories:
author: Springer 
tags: [Light,Photon,Quantum memory,Waves,Technology,Applied and interdisciplinary physics,Electromagnetism,Electromagnetic radiation,Science,Theoretical physics,Quantum mechanics,Physics]
---


A team of Chinese physicists has now developed a way to confine light. This is significant because the approach allows quantum memories stored within photons to be retained. These findings stem from a study by Nan Sun from Nanjing University of Posts & Telecommunications, China, and colleagues, which has just been published in EPJ D. The results may herald the advent of a multitude of hybrid, optoelectronic devices relying on the use of quantum information stored in photons for processing information that can be used in communication networks or quantum computing. Indeed, stopping and storing light for a duration ranging from a few seconds to a few minutes is key for quantum information processing. This new study focuses on understanding the propagation properties of the electromagnetic wave associated with light to learn how best to stop it.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/s-nwo093015.php){:target="_blank" rel="noopener"}


