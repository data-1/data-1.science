---
layout: post
title: "Fossil specimen reveals a new species of ancient river dolphin"
date: 2015-09-02
categories:
author: Smithsonian 
tags: [River dolphin,Fossil,Dolphin,National Museum of Natural History,Marine mammal,Evolution,Extinction,News aggregator,Species,Marine life,Animals]
---


The team named it Isthminia panamensis. Today there are only four species of river dolphins -- all living in freshwater or coastal ecosystems and all endangered, including the Chinese river dolphin, which is likely now extinct. We discovered this new fossil in marine rocks, and many of the features of its skull and jaws point to it having been a marine inhabitant, like modern oceanic dolphins, said the study's lead author Nicholas D. Pyenson, curator of fossil marine mammals at the Smithsonian's National Museum of Natural History. Other fossilized animals found at the same site as I. panamensis were marine species, indicating that unlike river dolphins living today, I. panamensis lived in the salty waters of a food-rich Caribbean Sea, before the full closure of the Panama Isthmus. The name of the new genus, Isthminia, recognizes both the Panama Isthmus and the fossil specimen's living relative, the Amazon river dolphin, Inia geoffrensis.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150901095511.htm){:target="_blank" rel="noopener"}


