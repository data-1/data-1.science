---
layout: post
title: "Watching individual neurons respond to magnetic therapy"
date: 2014-06-30
categories:
author: Duke University 
tags: [Transcranial magnetic stimulation,Neuroscience,Nervous system,Brain,Neuron,Cognitive science,Clinical medicine,Medicine,Psychology,Branches of science]
---


The advance will help researchers understand the underlying physiological effects of TMS -- a procedure used to treat psychiatric disorders -- and optimize its use as a therapeutic treatment. While studies have demonstrated the efficacy of TMS, the technique's physiological mechanisms have long been lost in a black box. We set out to try to understand what's happening inside that black box by recording activity from single neurons during the delivery of TMS in a non-human primate. First, Grill and his colleagues in the Duke Institute for Brain Sciences (DIBS) engineered new hardware that could separate the TMS current from the neural response, which is thousands of times smaller. The TMS magnetic field was creating an electric current through the electrode measuring the neuron, raising the possibility that this current, instead of the TMS, was causing the neural response.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/du-win062414.php){:target="_blank" rel="noopener"}


