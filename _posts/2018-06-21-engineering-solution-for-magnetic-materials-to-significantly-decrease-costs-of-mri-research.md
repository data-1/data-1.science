---
layout: post
title: "Engineering solution for magnetic materials to significantly decrease costs of MRI research"
date: 2018-06-21
categories:
author: "The National University Of Science, Technology Misis"
tags: [Magnetic resonance imaging,Medical imaging,Rare-earth element,Technology]
---


Credit: The National University of Science and Technology MISIS  Researchers from the NUST MISIS Engineering Center for Industrial Technologies have developed an innovative technology for the production of magnetic materials and permanent magnets at a reduced cost. The highest quality pictures are taken by using superconducting magnetic systems generating very strong magnetic fields. NUST MISIS scientists have developed a prototype of an economically and environmentally friendly low-field magnetic resonance imaging prototype based on magnetic materials and components produced in Russia. We have developed an innovative technology for the production of low cost hard-magnetic materials and permanent magnets manufactured from alloys of rare, domestic earth metals and their compounds, including the ones obtained in the processing of industrial waste magnetic production. All this allowed us to design and reduce the weight of permanent magnets used in the design of magnetic systems by almost 30 percent, and thus to reduce the cost of the devices, said Evgeny Gorelikov, the project head.

<hr>

[Visit Link](https://phys.org/news/2018-01-solution-magnetic-materials-significantly-decrease.html){:target="_blank" rel="noopener"}


