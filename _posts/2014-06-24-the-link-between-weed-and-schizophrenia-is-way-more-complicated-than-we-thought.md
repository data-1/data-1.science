---
layout: post
title: "The link between weed and schizophrenia is way more complicated than we thought"
date: 2014-06-24
categories:
author: Arielle Duhaime-Ross, Jun
tags: [Schizophrenia,Cannabis (drug),Psychosis,Mental disorder,Psychiatry,Mental disorders,Behavioural sciences,Clinical medicine,Health sciences,Psychology,Abnormal psychology,Mental health,Health,Diseases and disorders]
---


In the 1960s and 1970s, scientists thought that smoking weed could trigger psychosis in just about anyone. The resulting studies suggest that the neurobiology underlying schizophrenia might also put people affected by the disorder at increased risk for smoking pot. But this study indicates that people who are at risk for schizophrenia are more likely to use cannabis, and in greater quantities, he says — which means the causal relationship might actually go both ways. Weed affects people with schizophrenia differently  When The Verge asked about the risks associated with cannabis use, Hill said that there are detailed studies which have demonstrated that cannabis can have very different effects on the brain of someone who is at risk for schizophrenia than someone who isn't. No single gene has been associated with either drug addiction or schizophrenia, so these illnesses are the result of many genes working in combination, Evins said, each with a small contribution to the overall risk of developing the disorders.

<hr>

[Visit Link](http://www.theverge.com/2014/6/24/5836762/the-link-between-weed-and-schizophrenia-is-way-more-complicated-than){:target="_blank" rel="noopener"}


