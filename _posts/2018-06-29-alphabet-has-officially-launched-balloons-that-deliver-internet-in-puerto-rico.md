---
layout: post
title: "Alphabet Has Officially Launched Balloons that Deliver Internet In Puerto Rico"
date: 2018-06-29
categories:
author: ""
tags: [Loon LLC,Tesla Powerwall,Technology,Telecommunications,Information and communications technology,Service industries,Computing]
---


Web Balloons  Alphabet's Project Loon has officially launched in Puerto Rico in an effort to bring basic internet connectivity to the island after its infrastructure was ravaged by Hurricane Maria. Speaking to Engadget, the head of Project Loon Alastair Westgarth stated, In times of crisis, being able to communicate with loved ones, emergency services, and critical information is key. The balloons set off from Winnemucca, Nevada and the team used machine learning algorithms to fly them to Puerto Rican airspace. Alphabet isn't the only company that is looking to use their technologies to help rebuild Puerto Rico. The devices helped to deliver cell phone service, including LTE wireless, to up to 8,000 people in San Juan, the territory's capital.

<hr>

[Visit Link](https://futurism.com/internet-delivering-balloons-puerto-rico/){:target="_blank" rel="noopener"}


