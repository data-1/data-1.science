---
layout: post
title: "What set the Earth's plates in motion?"
date: 2015-07-04
categories:
author: University of Sydney 
tags: [Plate tectonics,Earth science,Earth sciences,Global natural environment,Applied and interdisciplinary physics,Tectonics,Geology,Planetary science,Structure of the Earth,Nature,Geophysics,Planets of the Solar System,Terrestrial planets,Lithosphere,Physical sciences]
---


Earth is the only planet in our solar system where the process of plate tectonics occurs, said Professor Patrice Rey, from the University of Sydney's School of Geosciences. There are eight major tectonic plates that move above the earth's mantle at rates up to 150 millimetres every year. At mid-oceanic ridges, rocks are hot and their density is low, making them buoyant or more able to float. But three to four billion years ago, the earth's interior was hotter, volcanic activity was more prominent and tectonic plates did not become cold and dense enough to spontaneously sank. Our modelling shows that these early continents could have placed major stress on the surrounding plates.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/uos-wst091614.php){:target="_blank" rel="noopener"}


