---
layout: post
title: "Black holes from an exacomputer"
date: 2018-06-02
categories:
author: "Goethe University Frankfurt"
tags: [Supercomputer,Exascale computing,Black hole,Gravitational wave,Simulation,Numerical analysis,Luciano Rezzolla,Physics,Goethe University Frankfurt,Computer,Science]
---


What happens when two black holes merge, or when stars collide with a black hole? This has now been simulated by researchers from Goethe University Frankfurt and the Frankfurt Institute for Advanced Studies (FIAS) using a novel numerical method. While they are waiting for the first exascale computers to be built, the ExaHyPE scientists are already testing their software at the largest supercomputing centres available in Germany. +49 (0) 69 798-47871, rezzolla@fias.uni-frankfurt.de. Current news about science, teaching, and society in GOETHE-UNI online  Goethe University is a research-oriented university in the European financial centre Frankfurt The university was founded in 1914 through private funding, primarily from Jewish sponsors, and has since produced pioneering achievements in the areas of social sciences, sociology and economics, medicine, quantum physics, brain research, and labour law.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/guf-bhf052818.php){:target="_blank" rel="noopener"}


