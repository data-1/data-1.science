---
layout: post
title: "A new synthesis method enables the production of wafer-thin carbon layers"
date: 2014-06-25
categories:
author: Max Planck Society
tags: [Molecule,Nanoelectronics,Chemical bond,Carbon,Chemical reaction,Chemical compound,Solubility,Chemical substance,Infrared spectroscopy,Ultraviolet,Atom,Functional group,Water,Nanotechnology,Nanomaterials,Max Planck Institute of Colloids and Interfaces,Synchrotron light source,Physical sciences,Technology,Applied and interdisciplinary physics,Chemistry,Physical chemistry,Materials,Materials science]
---


An international team headed by scientists from the Swiss Federal Institute of Technology Lausanne (EPFL) and the Max Planck Institute of Colloids and Interfaces in Potsdam-Golm has developed an elegant method for producing self-organised carbon nanolayers and equipping them chemically with a range of functions. Because almost all of the bristles were thereby eventually bonded with their neighbouring bristles, a consistent layer of carbon atoms arose – a carbon nanolayer. Between these two ends, the scientists placed the reactive triple bonds. Special methods came into play here, which are part of the repertoire of Gerald Brezesinski and his Research Group at the Max Planck Institute of Colloids and Interfaces in Potsdam. Gerald Brezesinski from the Max Planck Institute of Colloids and Interfaces in Potsdam was also pleased with this success: This means that it is actually possible to design surfactant-like molecules in a way that enables their use in the synthesis of a carbon layer on a water surface.

<hr>

[Visit Link](http://phys.org/news322897618.html){:target="_blank" rel="noopener"}


