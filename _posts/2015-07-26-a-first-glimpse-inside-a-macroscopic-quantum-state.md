---
layout: post
title: "A first glimpse inside a macroscopic quantum state"
date: 2015-07-26
categories:
author: ICFO-The Institute of Photonic Sciences 
tags: [Quantum entanglement,Light,Photon,Quantum mechanics,Squeezed states of light,Physical sciences,Applied and interdisciplinary physics,Optics,Atomic molecular and optical physics,Electromagnetic radiation,Electromagnetism,Scientific theories,Physics,Science,Theoretical physics]
---


Quantum entanglement is always related to the microscopic world, but it also has striking macroscopic effects, such as the squeezing of light or superconductivity, a physical phenomenon that allows high-speed trains to levitate. Compared with normal light, laser light, composed of independent photons, has an extremely small but nonzero polarization uncertainty. This uncertainty or quantum noise is directly linked to the existence of photons, the smallest energy quanta of light. When the theoretical predictions came out, saying that there should be a sea of entangled particles inside a squeezed state, I was floored. By changing the density of the beam, they also observed effects of entanglement monogamy, where particles can be strongly entangled only if they have few entanglement partners.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/iiop-afg032315.php){:target="_blank" rel="noopener"}


