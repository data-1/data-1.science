---
layout: post
title: "Volcanologists discover how bubbles accumulate in magma"
date: 2016-04-14
categories:
author: Eth Zurich
tags: [Volcano,Magma,Mount Tambora,Types of volcanic eruptions,Magma chamber,Nature,Earth sciences]
---


Tambora on the Indonesian island of Sumbawa: the explosive eruption of this volcano 200 years ago cooled the climate and lead to a year without a summer. Together with other scientists from ETH Zurich and Georgia Institute of Technology (Georgia Tech), the researchers studied the behaviour of bubbles with a computer model. In many volcanic systems, the magma reservoir consists mainly of two zones: an upper layer consisting of viscous melt with almost no crystals, and a lower layer rich in crystals, but still containing pore space. Parmigiani explains this as follows: when the proportion of bubbles in the pore space of the crystal-rich layers increases, small individual bubbles coalesce into finger-like channels, displacing the existing highly viscous melt. Through this mechanism, a large number of gas bubbles can accumulate in the crystal-poor melt under the roof of the magma reservoir.

<hr>

[Visit Link](http://phys.org/news/2016-04-volcanologists-accumulate-magma.html){:target="_blank" rel="noopener"}


