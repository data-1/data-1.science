---
layout: post
title: "Dental plaque DNA shows Neanderthals used 'aspirin'"
date: 2017-03-17
categories:
author: "University of Adelaide"
tags: [Neanderthal,Bacteria,Sidrn Cave,Antibiotic,Human microbiome,Clinical medicine,Health sciences,Medical specialties]
---


Ancient DNA found in the dental plaque of Neandertals -- our nearest extinct relative -- has provided remarkable new insights into their behaviour, diet and evolutionary history, including their use of plant-based medicine to treat pain and illness. Published today in the journal Nature, an international team led by the University of Adelaide's Australian Centre for Ancient DNA (ACAD) and Dental School, with the University of Liverpool in the UK, revealed the complexity of Neandertal behaviour, including dietary differences between Neandertal groups and knowledge of medication. The international team analysed and compared dental plaque samples from four Neandertals found at the cave sites of Spy in Belgium and El Sidrón in Spain. Neandertals, ancient and modern humans also shared several disease-causing microbes, including the bacteria that cause dental caries and gum disease. The Neandertal plaque allowed reconstruction of the oldest microbial genome yet sequenced -- Methanobrevibacter oralis, a commensal that can be associated with gum disease.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/03/170308131218.htm){:target="_blank" rel="noopener"}


