---
layout: post
title: "Amazing Pop-Up Solar Power Station Delivers Energy Anywhere it’s Needed"
date: 2014-06-24
categories:
author: Beverley Mitchell
tags: []
---


Ecosphere Technologies’ latest product combines several of our very favorite things in one easy-to-transport package: shipping containers, off-the-grid solar power, and clean drinking water generation. Continue reading below Our Featured Videos  According to the Ecosphere Technologies’ website, the “Ecos PowerCube® is the world’s largest, mobile, solar-powered generator. Energy is stored in onboard batteries.” The unit is designed to fit inside shipping containers for easy transportation, and it’s available in 10-foot, 20-foot and 40-foot ISO shipping container footprints. Related: Community Solar Gardens Bring Affordable Green Energy to the Masses  There are also many other onboard features built into the unit – including communication systems and water treatment and distribution systems. The patented design is anticipated to have many applications, including humanitarian aid and disaster relief, military applications and power generation in remote locations.

<hr>

[Visit Link](http://inhabitat.com/amazing-pop-up-solar-power-station-delivers-energy-anywhere-its-needed/){:target="_blank" rel="noopener"}


