---
layout: post
title: "Why genetic research must be more diverse"
date: 2016-06-23
categories:
author: "Keolu Fox"
tags: [TED (conference),Genome,Research,Life sciences,Biotechnology,Genetics]
---


Ninety-six percent of genome studies are based on people of European descent. The rest of the world is virtually unrepresented -- and this is dangerous, says geneticist and TED Fellow Keolu Fox, because we react to drugs differently based on our genetic makeup. Fox is working to democratize genome sequencing, specifically by advocating for indigenous populations to get involved in research, with the goal of eliminating health disparities. The research community needs to immerse itself in indigenous culture, he says, or die trying.

<hr>

[Visit Link](http://www.ted.com/talks/keolu_fox_why_genetic_research_must_be_more_diverse){:target="_blank" rel="noopener"}


