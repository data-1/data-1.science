---
layout: post
title: "Gene editing can now change an entire species -- forever"
date: 2016-05-10
categories:
author: Jennifer Kahn
tags: [Gene drive,Genetics,Gene,Branches of genetics,Molecular biology,Life sciences,Biotechnology,Biology,Biochemistry,Molecular genetics,Modification of genetic information,Biological engineering,Genomics]
---


CRISPR gene drives allow scientists to change sequences of DNA and guarantee that the resulting edited genetic trait is inherited by future generations, opening up the possibility of altering entire species forever. More than anything, the technology has led to questions: How will this new power affect humanity? What are we going to use it to change? Are we gods now? Join journalist Jennifer Kahn as she ponders these questions and shares a potentially powerful application of gene drives: the development of disease-resistant mosquitoes that could knock out malaria and Zika.

<hr>

[Visit Link](http://www.ted.com/talks/jennifer_kahn_gene_editing_can_now_change_an_entire_species_forever){:target="_blank" rel="noopener"}


