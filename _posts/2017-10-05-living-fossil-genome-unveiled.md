---
layout: post
title: "Living fossil genome unveiled"
date: 2017-10-05
categories:
author: "GigaScience"
tags: [Evolution,Genome,Plant,Ginkgo,GigaScience,Species,Biology,Genetics]
---


Given its longevity as a species and unique position in the evolutionary tree of life, the ginkgo genome will provide an extensive resource for studies concerning plant defenses against insects and pathogens, and research investigating early events in tree evolution and in evolution overall. The great interest in the history and biology of gingko, however, made the work of sequencing and assembling the genome a challenge the researchers from China felt worth taking, and one they succeeded in accomplishing. Professor Yunpeng Zhao, one of the authors from Zhejiang University, explains how this evolutionary placement is of great interest to researchers: Ginkgo represents one of the five living groups of seed plants, and has no living relatives. An initial analysis of the tree's more than 40,000 predicted genes showed extensive expansion of gene families that provide for a variety of defensive mechanisms. ###  In keeping with the journal's goals of making the data underlying the analyses used in published research fully and freely available, all data from this project are available under a CC0 waiver in the GigaScience database, GigaDB, in a citable format (http://dx.doi.org/10.5524/100209), and, as a standard, the sequence data is available in the NCBI public repository under accession number PRJNA307642.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/g-lfg111716.php){:target="_blank" rel="noopener"}


