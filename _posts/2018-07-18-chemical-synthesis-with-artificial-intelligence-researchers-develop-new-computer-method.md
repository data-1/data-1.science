---
layout: post
title: "Chemical synthesis with artificial intelligence: Researchers develop new computer method"
date: 2018-07-18
categories:
author: "University of Münster"
tags: [Chemical substance,Neural network,Deep learning,Chemistry,Monte Carlo method,Chemical reaction,Chemical synthesis,Chemist,Computer,Organic chemistry,Monte Carlo tree search,Chess,Artificial intelligence,Branches of science,Cognitive science,Interdisciplinary subfields,Cognition,Science,Technology]
---


The recipe for the success of this computer programme is made possible through a combination of the so-called Monte Carlo Tree Search and deep neural networks based on machine learning and artificial intelligence. This is where the new method comes into play, linking up the deep neural networks with the Monte Carlo Tree Search - a constellation which is so promising that currently a large number of researchers from a variety of disciplines are working on it. In a similar way, the computer now looks for the best possible moves for the chemical synthesis. We need the help of an 'intelligent' computer. ###  The authors of the study:  Marwin Segler and Prof. Mark Waller, both chemists, carried out the study together with Dr. Mike Preuss, a business information specialist, at the University of Muenster.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-03/uom-csw032918.php){:target="_blank" rel="noopener"}


