---
layout: post
title: "Niels Bohr: Biography & Atomic Theory"
date: 2017-09-20
categories:
author: "Elizabeth Palermo"
tags: [Niels Bohr,Bohr model,Electron,Atom,Quantum mechanics,Physics,Ernest Rutherford,Atomic nucleus,Atomic physics,Theoretical physics,Chemistry,Science,Applied and interdisciplinary physics,Physical sciences]
---


Right: Antonio Abrignani  Niels Bohr was one of the foremost scientists of modern physics, best known for his substantial contributions to quantum theory and his Nobel Prize-winning research on the structure of atoms. He studied the subject throughout his undergraduate and graduate years and earned a doctorate in physics in 1911 from Copenhagen University. He went back to Copenhagen University in 1916 to become a professor of theoretical physics. Combining Rutherford's description of the nucleus and Planck's theory about quanta, Bohr explained what happens inside an atom and developed a picture of atomic structure. Despite his contributions to the U.S. Atomic Energy Project during World War II, Bohr was an outspoken advocate for the peaceful application of atomic physics.

<hr>

[Visit Link](https://www.livescience.com/32016-niels-bohr-atomic-theory.html){:target="_blank" rel="noopener"}


