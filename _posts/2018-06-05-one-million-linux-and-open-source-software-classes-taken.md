---
layout: post
title: "​One million Linux and open-source software classes taken"
date: 2018-06-05
categories:
author: "Steven Vaughan-Nichols, Senior Contributing Editor, Feb."
tags: []
---


Then, you need to know Linux and open-source software. While we're humbled at having reached one million folks, we remain aware of just how big the need is, said Clyde Seepersad, general manager of Linux Foundation training, in a statement. Many of The Linux Foundation's most popular classes are free, entry-level introductions to hot topics. The Foundation offers three sets of courses: Enterprise IT and Linux System Administration, Linux Programming and Development, and Open Source Compliance. Both can be a great help in landing a Linux admin job.

<hr>

[Visit Link](http://www.zdnet.com/article/one-million-linux-and-open-source-software-classes-taken/#ftag=RSSbaffb68){:target="_blank" rel="noopener"}


