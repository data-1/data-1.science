---
layout: post
title: "Sentinel-1 sees through hurricanes"
date: 2018-08-16
categories:
author: ""
tags: [Tropical cyclone,Sea,Sentinel-1,Copernicus Programme,Radar,Weather,Earth sciences,Vortices,Tropical meteorology,Storms,Tropical cyclones in,Tropical cyclone seasons,Disasters,Physical geography,Natural events,Atlantic hurricane seasons,Natural hazards,Natural disasters,Atlantic hurricanes,Tropical cyclones,Branches of meteorology,Weather events,Natural environment]
---


Imaging the top of hurricanes from space is nothing new, but the Sentinel-1 satellites can see right through these towering spinning weather systems, measuring the sea surface below to help predict the storm’s path. The 2017 hurricane season isn’t even over yet, but 10 Atlantic storms in a row have already reached hurricane strength – the first time this has happened in more than a century. Since understanding and predicting these powerful weather systems is essential to saving lives and property, scientists have been looking into how the Copernicus Sentinel-1 radar mission can help. Radar vision Unlike satellites that carry optical instruments, from which we get the familiar images of the top of hurricanes, radar can penetrate clouds to image the sea underneath these powerful and destructive weather systems. This is especially important because in situ measurements of wind and sea state cannot be gained from buoys or dropped probes in such extreme weather or over such a wide area.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-1/Sentinel-1_sees_through_hurricanes){:target="_blank" rel="noopener"}


