---
layout: post
title: "Graphene is both transparent and opaque to radiation"
date: 2016-04-10
categories:
author: Ecole Polytechnique Fédérale de Lausanne
tags: [Electromagnetic radiation,Wireless,Optical filter,Telecommunications,Polarization (waves),Hertz,Graphene,Terahertz radiation,Radiation,Electrical engineering,Electronics,Technology,Information and communications technology,Electromagnetism,Optics,Telecommunications engineering,Waves,Electrodynamics,Electricity]
---


Our graphene based microchip is an essential building block for faster wireless telecommunications in frequency bands that current mobile devices cannot access, says EPFL scientist Michele Tamagnone. Like polarized glasses, their graphene-based microchip makes sure that radiation that only vibrates a certain way gets through. The EPFL scientists and their colleagues from Geneva used this property to create a device known as an optical isolator. Faster Uploads in the Terahertz Bandwidth  Moreover, their microchip works in a frequency band that is currently empty, called the Terahertz gap. Wireless devices work today by transmitting data in the Gigahertz range or at optical frequencies.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/epfd-gib040516.php){:target="_blank" rel="noopener"}


