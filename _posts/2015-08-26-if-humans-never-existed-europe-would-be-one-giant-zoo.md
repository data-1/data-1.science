---
layout: post
title: "If humans never existed, Europe would be one giant zoo"
date: 2015-08-26
categories:
author: Colm Gorey, Colm Gorey Was A Senior Journalist With Silicon Republic
tags: [Biodiversity,Human,Mammal,Organisms,Earth sciences,Animals,Ecology,Natural environment]
---


A new study is attempting to answer one of mankind’s oldest questions: what would the Earth be like if humans never existed? Perhaps unsurprisingly, the Earth would be a lot better without us. The study, undertaken by Danish researchers from Aarhus University, asked what Earth would be like if humans never existed, not from a climate perspective, but simply one of biodiversity. However, one of the researchers on the study, Prof Jens-Christian Svenning, has stated that greater biodiversity would not have been limited to Europe, but across the world  While Africa is considered the ‘last refuge’ for the greatest biodiversity among mammals in the world today, the study shows that, in a world without humans, the Americas – both North and South – would have far, far greater biodiversity than it does today. Instead, it reflects that it’s one of the only places where human activities have not yet wiped out most of the large animals.”  Likewise, the existence of greater mammal diversity in mountainous regions across the world is not due to natural patterns, but rather that we humans are less likely to go up there.

<hr>

[Visit Link](https://www.siliconrepublic.com/earth-science/2015/08/24/if-humans-never-existed-europe){:target="_blank" rel="noopener"}


