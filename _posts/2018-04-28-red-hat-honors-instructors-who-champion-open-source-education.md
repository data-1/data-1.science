---
layout: post
title: "Red Hat Honors Instructors Who Champion Open Source Education"
date: 2018-04-28
categories:
author: ""
tags: [Red Hat,Open source,Forward-looking statement,Education,Risk,Technology,Cloud computing,Intellectual property,Computing]
---


Red Hat is proud to recognize educators who embrace the open source way and are committed to teaching the next generation about the potential of a career in open source. These educators are helping to pave the way for their students’ success and for other instructors who can benefit from the resources they create.” Gina Likins university outreach, Open Source and Standards, Red Hat  For the second year, Red Hat is honoring the work of higher education instructors who are committed to teaching the open source development process to their students. Corporations such as Red Hat and Google provide support through Teaching Open Source and their participation in POSSE workshops, which are co-taught by members of the academic and open source communities. This year’s list of instructors leading the way in teaching open source are:  Aria Chernik, lecturing fellow, Social Science Research Institute, and director, Open Source Pedagogy, Research and Innovation (OSPRI), Duke University  Joshua Dehlinger, assistant/associate professor, Department of Computer and Information Sciences, Towson University  Robert Duvall, lecturer, Department of Computer Science, Duke University  Joshua Pearce, professor, Department of Materials Science and Engineering; professor, Department of Electrical and Computer Engineering; and advisor, Open Source Hardware Enterprise, Michigan Technological University  Alan Rea, professor, business information systems, Haworth College of Business, Western Michigan University  Wes Turner, senior lecturer, computer science, School of Science, Rensselaer Polytechnic Institute  Stewart Weiss, associate professor, Department of Computer Science, Hunter College of the City University of New York  Sabine Wojcieszak, lecturer, University of Applied Science Kiel  The efforts of these instructors both to integrate open source into their classrooms and to grow a community of like-minded educators are making a difference in the lives of students and making the world a more open and collaborative place. Supporting Quote  Gina Likins, university outreach, Open Source and Standards, Red Hat  “As more organizations use and develop open source technology, the need for graduates skilled in open source grows.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/red-hat-honors-instructors-who-champion-open-source-education){:target="_blank" rel="noopener"}


