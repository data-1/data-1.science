---
layout: post
title: "Tracking the genetic arms race between humans and mosquitoes"
date: 2016-05-10
categories:
author: University of Southern California
tags: [Evolution,Mosquito,Natural selection,Gene,Nature,Life sciences,Evolutionary biology,Genetics,Biological evolution,Biology]
---


Scientists studying mosquitoes in various types of environments in the United States and in Russia found that between 5 and 20 percent of a mosquito population's genome is subject to evolutionary pressures at any given time -- creating a strong signature of local adaptation to environment and humans. That is -- a suburban mosquito in the States has more in common with an urban mosquito in the States than it does with a suburban mosquito in Russia. In addition to the insights into the contemporary evolution of mosquitoes, the methods we used in this study can be applied to compare genes under natural selection across populations of any species, including humans, said Hosseinali Asgharian, lead author of the study and Ph.D. student at USC Dornsife. The scientists hope that the knowledge will help inform strategies to control mosquito populations. Of the 2,205 West Nile Virus cases reported in the U.S. in 2014, 801 were in California, according to the Centers for Disease Control and Prevention.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/uosc-ttg062515.php){:target="_blank" rel="noopener"}


