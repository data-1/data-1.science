---
layout: post
title: "'Topological' graphene nanoribbons trap electrons for new quantum materials"
date: 2018-08-12
categories:
author: "University Of California - Berkeley"
tags: [Graphene nanoribbon,Graphene,Electron,Topological insulator,Quantum mechanics,Materials,Technology,Condensed matter physics,Materials science,Physics,Applied and interdisciplinary physics,Physical sciences,Chemistry]
---


This gives us a new way to control the electronic and magnetic properties of graphene nanoribbons, said Crommie, a UC Berkeley professor of physics. The synthesis of the hybrid nanoribbons was a difficult feat, said Fischer, a UC Berkeley professor of chemistry. His recipe required taking so-called topologically trivial nanoribbons and pairing them with topologically non-trivial nanoribbons, where Louie explained how to tell the difference between the two by looking at the shape of the quantum mechanical states that are adopted by electrons in the ribbons. The hybrid nanoribbons they made had between 50 and 100 junctions, each occupied by an individual electron able to quantum mechanically interact with its neighbors. Fischer said that the length of each segment of nanoribbon can be varied to change the distance between trapped electrons, thus changing how they interact quantum mechanically.

<hr>

[Visit Link](https://phys.org/news/2018-08-topological-graphene-nanoribbons-electrons-quantum.html){:target="_blank" rel="noopener"}


