---
layout: post
title: "Time Crystals to Tetraquarks: Quantum Physics in 2017"
date: 2018-07-03
categories:
author: "Rafi Letzter"
tags: [Subatomic particle,Neutrino,Flavour (particle physics),Quark,Atom,Baryon,Quantum Experiments at Space Scale,Nature,Science,Applied and interdisciplinary physics,Subatomic particles,Quantum mechanics,Theoretical physics,Nuclear physics,Physical sciences,Particle physics,Physics]
---


From time crystals to the elusive tetraquark, here are 15 of the most amazing quantum discoveries of the past 12 months. As long as the conditions they need to exist remain, time crystals become locked into a single moment in time; no matter how much energy gets pumped into them, they don't heat up or increase in entropy — effectively suspending the laws of thermodynamics as they're typically understood. Explosive quantum secret  (Image credit: Shutterstock)  When Rosner and Karliner's method turned out to work, they were able to apply it in other contexts. Elusive tetraquark  (Image credit: Giroscience/Shutterstock)  Rosner and Karliner's technique for determining the mass of theoretical particles also allowed them to predict that the holy grail hadron, a tetraquark — a particle made up of four quarks, not the three found in baryons — could really exist in its purest form. Micius sent a pair of entangled — or quantum-linked — protons to base stations on Earth separated from one another by a distance of 747.5 miles (1,203 km), the first step toward building secure encryption for a quantum internet.

<hr>

[Visit Link](https://www.livescience.com/61260-quantum-physics-in-2017.html){:target="_blank" rel="noopener"}


