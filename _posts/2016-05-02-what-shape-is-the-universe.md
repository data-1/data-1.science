---
layout: post
title: "What shape is the universe?"
date: 2016-05-02
categories:
author: Vanessa Janek
tags: [Universe,Shape of the universe,Cosmic microwave background,Physical sciences,Nature,Geometry,Astronomy,Astrophysics,Science,Physical cosmology,Cosmology,Physics]
---


In fact, two distant particles travelling in two straight lines would actually intersect before ending up back where they started. This type of universe, conveniently easy to imagine in three dimensions, would only arise if the cosmos contained a certain, large amount of energy. To be positively curved, or closed, the universe would first have to stop expanding – something that would only happen if the cosmos housed enough energy to give gravity the leading edge. Open, without boundaries in space or time. There are other options too – like a soccer ball, a doughnut, or a trumpet.

<hr>

[Visit Link](http://phys.org/news350637956.html){:target="_blank" rel="noopener"}


