---
layout: post
title: "Molecular snapshots of oxygen formation in photosynthesis"
date: 2015-10-03
categories:
author: Umea University
tags: [Photosystem II,Artificial photosynthesis,Chemical reaction,Photosynthesis,Chemistry,Nature,Applied and interdisciplinary physics,Physical chemistry,Physical sciences]
---


Credit: Umea University  Researchers from Umeå University, Sweden, have explored two different ways that allow unprecedented experimental insights into the reaction sequence leading to the formation of oxygen molecules in photosynthesis. Thus, it is not surprising that it has been so difficult to prove experimentally how precisely a catalyst consisting of four manganese ions and one calcium ion (Mn4Ca cluster) performs this reaction sequence in photosystem II. The data show that no large scale structural changes (> 0.5·10−10 m) occur in the Mn4Ca cluster and the rest of the photosystem II complex during oxygen formation. The simultaneously collected X-ray emission data confirm that the arresting of the two bound water molecules, as observed in the mass spectrometric experiments, is not due to a change in the charge (oxidation state) of the manganese ions of the Mn4Ca cluster, nor to the formation of a first bond between the oxygen atoms of the two water molecules. The first study was performed in collaboration with two French researchers.

<hr>

[Visit Link](http://phys.org/news324292820.html){:target="_blank" rel="noopener"}


