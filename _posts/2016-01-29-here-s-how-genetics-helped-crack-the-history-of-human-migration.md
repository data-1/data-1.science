---
layout: post
title: "Here's how genetics helped crack the history of human migration"
date: 2016-01-29
categories:
author: George Busby, The Conversation
tags: [Recent African origin of modern humans,Homo,Europe,Interbreeding between archaic and modern humans,Human,Genetics,Human populations]
---


A family migrating to western US in 1886. Credit: Marion Doss/Flickr, CC BY-SA  Over the past 25 years, scientists have supported the view that modern humans left Africa around 50,000 years ago, spreading to different parts of the world by replacing resident human species like the Neanderthals. The technology  Our ability to sequence DNA has increased dramatically since the human genome was first sequenced 15 years ago. Out of Africa … and back  In addition to their use in medical genetics, these data are providing us with an increasingly sophisticated view of human history. Ancient DNA also allows us to directly view the genomes of past populations. Explore further Ancient genome from Africa sequenced for the first time  This article was originally published on The Conversation.

<hr>

[Visit Link](http://phys.org/news/2016-01-genetics-history-human-migration.html){:target="_blank" rel="noopener"}


