---
layout: post
title: "Researchers pursue low-cost, efficient technologies for hydrogen generation"
date: 2017-10-12
categories:
author: "Julie Chao, Us Department Of Energy"
tags: [Electrolysis,Hydrogen production,Fuel cell,Water splitting,Hydrogen,Technology,Nature,Sustainable technologies,Energy technology,Energy development,Sustainable energy,Materials,Chemistry,Energy]
---


Credit: US Department of Energy  While hydrogen is often talked about as a pollution-free fuel of the future, especially for use in fuel cell electric vehicles, hydrogen can be used for much more than zero-emission cars. These include electrolysis, which uses electricity to split water into hydrogen and oxygen, and photoelectrochemical (PEC) cells, which use sunlight to do the same thing. If we can utilize that electricity in an intermittent electrolysis unit, we can start producing some very inexpensive hydrogen. We have extensive experience in researching fuel cells, and we are leveraging that for electrolysis as well, said Danilovic. While there are still science problems to overcome Weber and Danilovic, both in Berkeley Lab's Energy Technologies Area, see great promise for hydrogen to play a role in industries from transportation to heating to food production.

<hr>

[Visit Link](https://phys.org/news/2017-10-pursue-low-cost-efficient-technologies-hydrogen.html){:target="_blank" rel="noopener"}


