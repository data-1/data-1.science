---
layout: post
title: "Embattled GMO Golden Rice Is Now Allowed in US Food Supply"
date: 2018-06-02
categories:
author: ""
tags: [Genetically modified organism,Golden rice,Rice,Genetic engineering,Food and drink]
---


In the years since, that crop, named golden rice, hasn't saved any lives. But now, that might change — the U.S. Food and Drug Administration (FDA) has approved the crop and deemed it safe to eat. Golden rice is one of the oldest genetically modified organisms (GMOs) on the books. It's different from other types of rice because its DNA has been altered so that the plant produces Vitamin A, which would help children who subsist on rice get the required dose of the vitamin to the 250,000 to 500,000 children who go blind each year when they don't get enough of it. MASIPAG, an advocacy group for poor farmers, stated, in 2013, that Golden Rice will only make biotech companies a lot of money.

<hr>

[Visit Link](https://futurism.com/gmo-golden-rice-allowed-us/){:target="_blank" rel="noopener"}


