---
layout: post
title: "GDPR Compliance: Technical Requirements for Linux Systems"
date: 2018-06-05
categories:
author: "Michael Boelen"
tags: [General Data Protection Regulation,Password,Personal data,Audit,Linux,Patch (computing),Vulnerability (computing),Lynis,Security engineering,Computing,Information technology management,Technology,Information technology,Information Age,Computer science,Computer security,Cyberwarfare,Software,Secure communication,Security technology,Systems engineering]
---


When speaking about stored data, it includes the handling of data at any given time, from the initial creation of the data, until the final deletion of it. The GDPR applies to all companies that store personal data from EU citizens. Auditing and Events  One of the important topics in GDPR is dealing with breaches. You can only know how good your backup is by doing regular restores. Best practices for software patching  Using staging for testing software  Deploy software on a regular basis  Apply security patches as quick as possible with automation  General GDPR principles and tips  The “data as cold coffee” principle  Most people don’t like a pot of old (and cold) coffee.

<hr>

[Visit Link](https://linux-audit.com/gdpr-compliance-technical-requirements-for-linux-systems/){:target="_blank" rel="noopener"}


