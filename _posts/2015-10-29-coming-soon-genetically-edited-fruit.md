---
layout: post
title: "Coming soon: Genetically edited fruit?"
date: 2015-10-29
categories:
author: Cell Press
tags: [Genetically modified organism,Genetic engineering,Biotechnology,Genome editing,Branches of genetics,Biological engineering,Life sciences,Modification of genetic information,Biology,Technology,Genetics]
---


Recent advances that allow the precise editing of genomes now raise the possibility that fruit and other crops might be genetically improved without the need to introduce foreign genes, according to researchers writing in the Cell Press publication Trends in Biotechnology on August 13th. With awareness of what makes these biotechnologies new and different, genetically edited fruits might be met with greater acceptance by society at large than genetically modified organisms (GMOs) so far have been, especially in Europe, they say. This could mean that genetically edited versions of GMOs such as super bananas that produce more vitamin A and apples that don't brown when cut, among other novelties, could be making an appearance on grocery shelves. Most transgenic fruit crop plants have been developed using a plant bacterium to introduce foreign genes, and only papaya has been commercialized in part because of stringent regulation in the European Union (EU). Transfer of foreign genes was the first step to improve our crops, but GEOs will surge as a natural strategy to use biotechnology for a sustainable agricultural future.

<hr>

[Visit Link](http://phys.org/news327152055.html){:target="_blank" rel="noopener"}


