---
layout: post
title: "Calculating evolution: Program predicts the development of influenza viruses"
date: 2016-04-11
categories:
author: Max Planck Society
tags: [Virus,Evolution,Influenza A virus subtype H3N2,Organism,Influenza,Fitness (biology),Prediction,Research,Genetics,Privacy,Severe acute respiratory syndrome coronavirus 2,Mutation,Genome,Science,Biology]
---


Together with researchers from Cambridge and Santa Barbara scientists at the Max Planck Institute for Developmental Biology in Tübingen have developed an algorithm that can predict the evolution of asexual organisms such as viruses or cancer cells. Furthermore, the method developed by the researchers is not restricted to influenza viruses – it can even be applied to predict the development of HIV and noroviruses, as well as cancer cells. The algorithm developed by the international team is based on a simple idea: using the branches of a genealogical tree as reference, it infers an organism's capability of surviving – i.e. its biological fitness. Such predictions could also help scientists produce vaccines against rapidly developing pathogens such as influenza viruses. This algorithm uses long time series of genetic data of influenza viruses to predict which virus type will be dominant in the upcoming year, and is designed specifically for influenza.

<hr>

[Visit Link](http://phys.org/news335003925.html){:target="_blank" rel="noopener"}


