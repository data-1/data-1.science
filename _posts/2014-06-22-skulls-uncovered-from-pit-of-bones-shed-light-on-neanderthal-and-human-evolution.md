---
layout: post
title: "Skulls Uncovered from 'Pit of Bones' Shed Light on Neanderthal and Human Evolution"
date: 2014-06-22
categories:
author: Science World Report
tags: [Neanderthal,Human,Geological epochs,Human populations,Pliocene,Evolution of primates,Biological anthropology,Pleistocene,Human evolution]
---


Researchers are learning a bit more about the evolution of Neanderthals. The findings shed a bit more light on the history of Neanderthal evolution. The Middle Pleistocene, which occurred about 400 to 500 thousand years ago, was a time when archaic humans split off from other groups in Africa and East Asia and ultimately settled in Eurasia. The Middle Pleistocene was a long period of about half a million years during which hominin evolution didn't proceed through a slow process of change with just one kind of hominin quietly evolving toward the class Neanderthal, said Juan-Luis Arsuaga, one of the researchers, in a news release. In fact, other Homo sapiens during this time period did not exhibit the suite of Neanderthal-derived features seen in this fossil group, which means that more than one evolutionary lineage coexisted at this time.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15559/20140620/skulls-uncovered-pit-bones-shed-light-neanderthal-human-evolution.htm){:target="_blank" rel="noopener"}


