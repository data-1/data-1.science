---
layout: post
title: "First 'image' of a dark matter web that connects galaxies"
date: 2017-09-21
categories:
author: "Royal Astronomical Society (RAS)"
tags: [Dark matter,Galaxy,News aggregator,Astrophysics,Concepts in astronomy,Astronomical objects,Astronomy,Space science,Physical sciences,Physical cosmology,Science,Cosmology,Celestial mechanics,Physics,Natural sciences]
---


Researchers at the University of Waterloo have been able to capture the first composite image of a dark matter bridge that connects galaxies together. The scientists publish their work in a new paper in Monthly Notices of the Royal Astronomical Society. The composite image, which combines a number of individual images, confirms predictions that galaxies across the universe are tied together through a cosmic web connected by dark matter that has until now remained unobservable. This image moves us beyond predictions to something we can see and measure. By using this technique, we're not only able to see that these dark matter filaments in the universe exist, we're able to see the extent to which these filaments connect galaxies together, said Epps.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/04/170412091230.htm){:target="_blank" rel="noopener"}


