---
layout: post
title: "Quantum physics problem proved unsolvable"
date: 2015-12-21
categories:
author: University College London
tags: [Physics,Undecidable problem,Mathematics,Particle physics,Quantum mechanics,Macroscopic scale,Theoretical physics,Computer science,Standard Model,Matter,Physical sciences,Applied and interdisciplinary physics,Scientific theories,Branches of science,Science]
---


A mathematical problem underlying fundamental questions in particle and quantum physics is provably unsolvable, according to scientists at UCL, Universidad Complutense de Madrid - ICMAT and Technical University of Munich. The findings are important because they show that even a perfect and complete description of the microscopic properties of a material is not enough to predict its macroscopic behaviour. Using sophisticated mathematics, the authors proved that, even with a complete microscopic description of a quantum material, determining whether it has a spectral gap is, in fact, an undecidable question. What we've shown is that the spectral gap is one of these undecidable problems. The most famous problem concerning spectral gaps is whether the theory governing the fundamental particles of matter itself - the standard model of particle physics - has a spectral gap (the `Yang-Mills mass gap' conjecture).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/ucl-qpp120815.php){:target="_blank" rel="noopener"}


