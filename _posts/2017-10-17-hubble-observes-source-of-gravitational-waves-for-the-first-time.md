---
layout: post
title: "Hubble observes source of gravitational waves for the first time"
date: 2017-10-17
categories:
author: "ESA/Hubble Information Centre"
tags: [Gravitational wave,GW170817,Kilonova,Neutron star,Star,Astronomy,NGC 4993,Physical sciences,Nature,Space science,Physics,Astronomical objects,Astrophysics,Science,Physical phenomena,Stellar astronomy]
---


The NASA/ESA Hubble Space Telescope has observed for the first time the source of a gravitational wave, created by the merger of two neutron stars. This merger created a kilonova -- an object predicted by theory decades ago -- that ejects heavy elements such as gold and platinum into space. About two seconds after the detection of the gravitational wave, ESA's INTEGRAL telescope and NASA's Fermi Gamma-ray Space Telescope observed a short gamma-ray burst in the same direction. Once I saw that there had been a trigger from LIGO and Virgo at the same time as a gamma-ray burst I was blown away, recalls Andrew Levan of the University of Warwick, who led the Hubble team that obtained the first observations. [3] A neutron star forms when the core of a massive star (above eight times the mass of the Sun) collapses.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/eic-hos101617.php){:target="_blank" rel="noopener"}


