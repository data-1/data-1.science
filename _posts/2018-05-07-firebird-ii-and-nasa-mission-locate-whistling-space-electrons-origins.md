---
layout: post
title: "FIREBIRD II and NASA mission locate whistling space electrons' origins"
date: 2018-05-07
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Van Allen radiation belt,Electron,Van Allen Probes,Space science,Outer space,Astronomy,Spaceflight,Physical sciences,Nature,Astronautics,Sky,Science,Electromagnetism,Spacecraft]
---


New research using data from NASA's Van Allen Probes mission and FIREBIRD II CubeSat has shown that a common plasma wave in space is likely responsible for the impulsive loss of high-energy electrons into Earth's atmosphere. It's draped with twisted magnetic field lines and swooping electrons and ions. Dictating the movements of these particles, Earth's magnetic environment traps electrons and ions in concentric belts encircling the planet. These belts, called the Van Allen Radiation Belts, keep most of the high-energy particles at bay. Late on Jan. 20, 2016, the Van Allen Probes observed chorus waves from its lofty vantage point and immediately after, FIREBIRD II saw microbursts.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/nsfc-fia111517.php){:target="_blank" rel="noopener"}


