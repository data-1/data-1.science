---
layout: post
title: "Cassini prepares for last up-close look at Hyperion"
date: 2016-05-04
categories:
author: Preston Dyches, Jet Propulsion Laboratory
tags: [Hyperion (moon),CassiniHuygens,Spaceflight,Astronomical objects,Gas giants,Outer planets,Moons,Saturn,Flight,Astronautics,Astronomical objects known since antiquity,Outer space,Solar System,Space science,Astronomy,Bodies of the Solar System,Planetary science,Planets of the Solar System]
---


The view was obtained during Cassini's close flyby on Sept. 26, 2005. Credit: NASA/JPL/Space Science Institute  NASA's Cassini spacecraft will make its final close approach to Saturn's large, irregularly shaped moon Hyperion on Sunday, May 31. Hyperion (168 miles, 270 kilometers across) rotates chaotically, essentially tumbling unpredictably through space as it orbits Saturn. Because of this, it's challenging to target a specific region of the moon's surface, and most of Cassini's previous close approaches have encountered more or less the same familiar side of the craggy moon. That flyby will represent the mission's penultimate close approach to that moon. In October, Cassini will make two close flybys of the active moon Enceladus, with its jets of icy spray, coming as close as 30 miles (48 kilometers) in the final pass.

<hr>

[Visit Link](http://phys.org/news352096163.html){:target="_blank" rel="noopener"}


