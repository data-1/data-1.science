---
layout: post
title: "New fossil skulls reveal insights about penguin brain evolution"
date: 2015-09-02
categories:
author: Annegret Kalus, Society Of Vertebrate Paleontology
tags: [Penguin,Evolution,Paleontology,Fossil]
---


This Eocene Antarctic fossil penguin skull was discovered at La Meseta Formation at Seymour Island. Penguins are unique among modern birds in that they 'fly' through the water. The Antarctic fossils reveal that the neuroanatomy of penguins was still evolving roughly 30 million years after the loss of aerial flight, with trends such as the expansion of the Wulst and reduction of the olfactory bulbs still in progress, said co-author Daniel Ksepka. In addition to the increase in visual complexity, and reduction in olfaction, findings in the ear region shed light on the head position and equilibrium-maintaining abilities of the fossil penguins. All together, the findings show that these early penguins had many of the adaptations of living forms, while having a few unique traits not seen in the modern ones.

<hr>

[Visit Link](http://phys.org/news/2015-08-fossil-skulls-reveal-insights-penguin.html){:target="_blank" rel="noopener"}


