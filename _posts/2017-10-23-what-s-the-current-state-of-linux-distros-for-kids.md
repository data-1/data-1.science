---
layout: post
title: "What's the current state of Linux distros for kids?"
date: 2017-10-23
categories:
author: "Jimmy Sjölund"
tags: [Sugar (software),Linux distribution,Ubuntu,Software,Computing,Technology,Computer science,System software,Software development,Computers,Computer engineering,Computer architecture,Software engineering,Digital media]
---


What I found, however, was that the development of Linux for kids has declined—and several of the distributions I found nearly three years ago have been discontinued or stalled. Activities, as the name implies, are Apps that involve active engagement from the learner. Not a good sign, but here's what the website says:  DoudouLinux is specially designed for children to make computer use as easy and pleasant as possible for them (and for their parents too!). If you want to support any of these specialized distributions, reach out to the project and ask how you can help. They suggest using your usual Linux flavor, adding your child as a user, customizing the look and feel, installing applications and games, and letting them explore!

<hr>

[Visit Link](https://opensource.com/article/17/9/linux-kids){:target="_blank" rel="noopener"}


