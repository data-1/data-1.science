---
layout: post
title: "Virginia Tech researcher shines light on origin of bioluminescence"
date: 2015-07-21
categories:
author: Virginia Tech 
tags: [Motyxia,Bioluminescence,Biology]
---


But bioluminescence at least in one millipede may have evolved as a way to survive in a hot, dry environment, not as a means to ward off predators, according to scientists publishing this week (Monday, May 4) in the Proceedings of the National Academy of Sciences. The discovery, based on a millipede that hadn't been seen in 50 years, shows that even the seemingly most complex and intricate of traits are actually small evolutionary steps leading to an intricate feature we see today. This discovery clarifies the evolutionary origins of many complex traits, not just bioluminescence. After we sequenced them we were able to place the millipede on an evolutionary tree with other bioluminescent species in Motyxia, Marek said. Marek mapped the brightness of light in M. bistipita, and its close relatives that live at higher elevation and glow more brightly, and demonstrated a gradation of brightness from faint to bright.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/vt-vtr050115.php){:target="_blank" rel="noopener"}


