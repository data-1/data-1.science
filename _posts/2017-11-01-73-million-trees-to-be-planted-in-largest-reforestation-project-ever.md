---
layout: post
title: "73 million trees to be planted in largest reforestation project ever"
date: 2017-11-01
categories:
author: Greg Beach
tags: []
---


Conservation International aims to plant 73 million trees in the Brazilian Amazon in the largest reforestation project ever. In the short-term, the project aims to restore 70,000 acres of tropical forest. A 2014 study from the Food and Agriculture Organization and Biodiversity International found that more than 90 percent of native tree species planted using the muvuca method germinate and are well suited to survive drought conditions for up to six months. “With plant-by-plant reforestation techniques, you get a typical density of about 160 plants per hectare,” said Rodrigo Medeiros, Conservation International’s vice president of the Brazil program and project lead, according to Fast Company. “With muvuca, the initial outcome is 2,500 species per hectare.

<hr>

[Visit Link](https://inhabitat.com/73-million-trees-to-be-planted-in-largest-reforestation-project-ever-undertaken){:target="_blank" rel="noopener"}


