---
layout: post
title: "Boosting your body for lift off: NASA's One-Year Mission investigates the metabolism"
date: 2016-05-31
categories:
author: "Amy Blanchett"
tags: [Atherosclerosis,Health,NASA,Spaceflight,Exercise,Stress (biology),Astronaut,Health sciences,Medical specialties]
---


The test results establish a profile of the body's response to spaceflight, helping scientists understand how the body reacts in microgravity in different groups of people. On Earth, people who proactively seek to reduce elevated levels of oxidative and inflammatory stress may gain a greater understanding of how environmental factors can impact their cardiovascular health. The Integrated Immune investigation assesses risks to the immune system by collecting health surveys, blood, urine and saliva samples before, during and after spaceflight. Validating a monitoring strategy enables development of countermeasures to reduce in-flight immune dysfunction. Researchers will use the data to monitor the effectiveness of countermeasures such as exercise, medication, immune modulators, etc.

<hr>

[Visit Link](http://phys.org/news/2015-08-boosting-body-nasa-one-year-mission.html){:target="_blank" rel="noopener"}


