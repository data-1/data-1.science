---
layout: post
title: "New way to save fish—and fishers"
date: 2015-05-19
categories:
author: ""    
tags: [Fishing,Poaching,Coral reef,Overfishing,Natural environment]
---


Credit: Katrina Davis  An end to poaching will benefit ocean conservation and fishing communities worldwide, an Australian-led scientific study shows. Protecting both the world's ocean life and the livelihoods of fishers creates a win-win situation for both fishing communities and conservation, says lead author Ms Katrina Davis of CEED and The University of Western Australia (UWA). Marine species in Chile, such as the Chilean abalone, are managed through a program called Territorial User Rights for Fisheries (TURF), Ms Davis says. The study shows that fishers earned more in enforced-TURF zones than they did in open-access areas, with every (US) dollar spent on enforcement yielding an increase of between US$4-9 in fishing revenue, Ms Davis says. In TURF areas, fishers are only allowed to take 30 per cent of the available fish stock, leaving enough fish in the ocean to reproduce.

<hr>

[Visit Link](http://phys.org/news351231369.html){:target="_blank" rel="noopener"}


