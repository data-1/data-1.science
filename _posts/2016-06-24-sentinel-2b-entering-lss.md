---
layout: post
title: "Sentinel-2B entering LSS"
date: 2016-06-24
categories:
author: ""
tags: [Satellite,European Space Agency,European Space Research and Technology Centre,Astronautics,Spaceflight,Outer space,Spacecraft,Flight,Space vehicles,Space science,Space programs,Technology,Spaceflight technology]
---


ESA’s Sentinel-2B Earth-observing satellite being lowered into Europe’s largest vacuum chamber, at the start of a test campaign to ensure it is ready to serve in space. Meanwhile, liquid nitrogen circulates through the black walls to mimic the cold of sunless space. Sentinel-2B arrived by lorry on the night of 15 June, making the first time any of the Sentinel family of satellites serving the Copernicus initiative for global environmental monitoring has visited ESA’s technical heart, in Noordwijk, the Netherlands. Sentinel travelled from Airbus Defence and Space’s Friedrichshafen facility in Germany, where it was first assembled and then all of its main elements underwent detailed testing. The largest centre of its kind in Europe, ESA’s test facilities simulate every aspect of the space environment.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2016/06/Sentinel-2B_entering_LSS){:target="_blank" rel="noopener"}


