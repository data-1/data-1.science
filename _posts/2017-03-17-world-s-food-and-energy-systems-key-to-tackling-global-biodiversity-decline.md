---
layout: post
title: "World's food and energy systems key to tackling global biodiversity decline"
date: 2017-03-17
categories:
author: ""
tags: [Living Planet Index,Sustainability,Planetary boundaries,Environmental science,Natural environment,Nature,Ecology,Earth sciences,Environmental social science,Environmental conservation,Environment,Sustainable development]
---


Credit: naturepl.com / Lisa Hoffner / WWF  Global wildlife could plunge to a 67 per cent level of decline in just the fifty-year period ending this decade as a result of human activities, according to WWF's Living Planet Report 2016. The Living Planet Report 2016 outlines solutions to reform the way we produce and consume food to help ensure that the world is well-fed in a sustainable way. The report also focuses on the fundamental changes required in the global energy and finance systems to meet the sustainability needs of future generations. The report also features research from the Global Footprint Network that shows that while we only have one Earth, humanity is currently using the resources of 1.6 planets to provide the goods and services we use each year. Explore further Humanity decimating planetary wildlife  More information: Living Planet Report 2016: Risk and resilience in a new era: Living Planet Report 2016: Risk and resilience in a new era: d2ouvy59p0dg6k.cloudfront.net/ … 16__full_report_.pdf

<hr>

[Visit Link](http://phys.org/news/2016-10-world-food-energy-key-tackling.html){:target="_blank" rel="noopener"}


