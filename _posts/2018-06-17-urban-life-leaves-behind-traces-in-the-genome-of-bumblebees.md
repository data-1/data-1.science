---
layout: post
title: "Urban life leaves behind traces in the genome of bumblebees"
date: 2018-06-17
categories:
author: "Martin-Luther-Universität Halle-Wittenberg"
tags: [Genetics,Bumblebee,Martin Luther University of Halle-Wittenberg,Biology]
---


Although genetic differences are not major, they nevertheless may influence how well the insects adapt to their habitat. For example, urban bumblebees are probably better able to react to environmental challenges that come with city life, such as higher temperatures. In order to test this, the researchers collected bumblebees from nine large German cities and paired neighbouring rural regions and analysed the bees' genetic material with the aid of so-called next generation sequencing. Overall differences in the genetic material of urban and rural bees are subtle, says Paxton. The biologists from Halle are unable to say for sure which of the many, different living conditions in cities cause these changes in the genetic material.

<hr>

[Visit Link](https://phys.org/news/2018-04-urban-life-genome-bumblebees.html){:target="_blank" rel="noopener"}


