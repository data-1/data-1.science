---
layout: post
title: "Photon hunting in the twilight zone: Visual features of bioluminescent sharks"
date: 2015-10-28
categories:
author: Public Library Of Science
tags: [Eye,Bioluminescence,Deep-sea fish,Mesopelagic zone,Shark,Visual system,Retina]
---


The eye of a velvet belly lanternshark. Credit: Dr. J. Mallefet (FNRS/UCL)  The eyes of deep-sea bioluminescent sharks have a higher rod density when compared to non-bioluminescent sharks, according to a study published August 6, 2014 in the open-access journal PLOS ONE by Julien M. Claes, postdoctoral researcher from the FNRS at Université catholique de Louvain (Belgium), and colleagues. This adaptation is one of many these sharks use to produce and perceive bioluminescent light in order to communicate, find prey, and camouflage themselves against predators. The mesopelagic twilight zone, or about 200-1000 meters deep in the sea, is a vast, dim habitat, where, with increasing depth, sunlight is progressively replaced by point-like bioluminescent emissions. To better understand strategies used by bioluminescent predators inhabiting this region that help optimize photon capture, the authors of this study analyzed the eye shape, structure, and retinal cell mapping in the visual systems of five deep-sea bioluminescent sharks, including four Lanternsharks (Etmopteridae) and one kitefin shark (Dalatiidae).

<hr>

[Visit Link](http://phys.org/news326534919.html){:target="_blank" rel="noopener"}


