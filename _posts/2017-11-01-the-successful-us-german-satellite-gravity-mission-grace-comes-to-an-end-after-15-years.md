---
layout: post
title: "The successful US/German satellite gravity mission GRACE comes to an end after 15 years"
date: 2017-11-01
categories:
author: "GFZ GeoForschungsZentrum Potsdam, Helmholtz Centre"
tags: [GRACE and GRACE-FO,Atmospheric entry,Spaceflight technology,Technology,Space exploration,Aerospace,Sky,Science,Space vehicles,Astronomy,Spacecraft,Space science,Flight,Outer space,Spaceflight,Astronautics]
---


The mission team made plans for one final science collection beginning in mid-October, when the satellites would be in full view of the Sun. All mission activities involving GRACE-1 will be completed by the end of the year, followed by decommissioning of GRACE-1 and atmospheric reentry in early 2018. NASA and the German Space Operations Center will jointly monitor both GRACE satellites as they deorbit and reenter the atmosphere. It is of course a great pity that the monthly observation of mass transport in system Earth has ended after 15 and a half years. JPL manages GRACE for NASA's Science Mission Directorate in Washington.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/ggph-tsu102717.php){:target="_blank" rel="noopener"}


