---
layout: post
title: "Here's What a World Powered by Quantum Computers Will Look Like"
date: 2017-11-01
categories:
author: ""
tags: [Quantum mechanics,Quantum entanglement,Computing,Quantum computing,Physics,Science,Branches of science,Theoretical physics,Technology,Computer science]
---


While a classical computer works with bits as information placeholders, a quantum computer works with quantum bits (qubits). Meanwhile, entanglement allows particles to be manipulated despite the distance between them — anything that happens to one particle will instantly be reflected in the other. Hurley elaborated on this in an email to Futurism:  It's my personal belief that quantum computing will help us make sense of the deluge of data we find ourselves creating to solve some very interesting problems. Still, I am incredibly excited about the potential positive outcomes of quantum computing. Researchers in China and elsewhere in the world have begun exploring the potential of building quantum networks using the same principles behind quantum computing.

<hr>

[Visit Link](https://futurism.com/future-quantum-computer/){:target="_blank" rel="noopener"}


