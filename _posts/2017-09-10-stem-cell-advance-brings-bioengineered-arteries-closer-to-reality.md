---
layout: post
title: "Stem cell advance brings bioengineered arteries closer to reality"
date: 2017-09-10
categories:
author: "Morgridge Institute for Research"
tags: [CRISPR gene editing,Cellular differentiation,Artery,Stem cell,Biotechnology,Biology,Clinical medicine,Life sciences,Medical specialties,Medicine,Health sciences,Cell biology]
---


MADISON -- Stem cell biologists have tried unsuccessfully for years to produce cells that will give rise to functional arteries and give physicians new options to combat cardiovascular disease, the world's leading cause of death. Further, these cells contributed both to new artery formation and improved survival rate of mice used in a model for myocardial infarction. The key finding here is a way to make arterial endothelial cells more functional and clinically useful. The research team applied two pioneering technologies to the project. They also identified some very common growth factors used in stem cell science, such as insulin, that surprisingly inhibit arterial endothelial cell differentiation.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-07/mifr-sca070617.php){:target="_blank" rel="noopener"}


