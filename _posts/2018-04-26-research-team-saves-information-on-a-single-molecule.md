---
layout: post
title: "Research team saves information on a single molecule"
date: 2018-04-26
categories:
author: "Kiel University"
tags: [Computer data storage,Hard disk drive,Scanning tunneling microscope,Bit,Information,Technology,Science,Chemistry,Computing]
---


Similar to normal hard drives, these special molecules can save information via their magnetic state. One bit on a hard drive now only requires a space of around 10 by 10 nanometres. The technology that is currently being used to store data on hard drives now reaches the fundamental limits of quantum mechanics due to the size of the bit. Credit: Manuel Gruber  The molecule that the research team used can assume two different magnetic states, and when attached to a special surface, it can also change its connection to the surface. We needed to find a way to firmly attach the molecule to the surface without affecting its switching ability, explained Gruber.

<hr>

[Visit Link](https://phys.org/news/2017-12-team-molecule.html){:target="_blank" rel="noopener"}


