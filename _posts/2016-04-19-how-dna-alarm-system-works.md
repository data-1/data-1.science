---
layout: post
title: "How DNA alarm-system works"
date: 2016-04-19
categories:
author: Lomonosov Moscow State University
tags: [DNA repair,DNA,ATM serinethreonine kinase,Ataxiatelangiectasia,Mutation,Macromolecules,Genetics,Biochemistry,Molecular biology,Biotechnology,Biology,Life sciences,Branches of genetics,Cell biology,Biological processes,Diseases and disorders,Molecular genetics,Nucleic acids,Causes of death,Clinical medicine,Biomolecules,Cellular processes]
---


DNA repair consists of enzymes which find the damaged DNA and repair it before the replication. Some of them recognize the damaged bases and give signals to the other enzymes, which repair the DNA. Scientists used to think that ATM exclusively recognizes DNA double-strand breaks (DSBs). The concept of the cellular function lies in the prevention of DNA double-strand break's formation, -- Svetlana V. Khoronenkova said, -- We now understand that ATM recognizes and is activated in response to DNA single-strand breaks (SSBs). This leads to a delay in the DNA replication, giving a cell more time to repair.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/lmsu-hda033015.php){:target="_blank" rel="noopener"}


