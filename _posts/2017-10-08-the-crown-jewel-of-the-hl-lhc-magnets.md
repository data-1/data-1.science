---
layout: post
title: "The crown jewel of the HL-LHC magnets"
date: 2017-10-08
categories:
author: "Stefania Pandolfi"
tags: [High Luminosity Large Hadron Collider,Large Hadron Collider,Physics laboratories,Particle physics facilities,Applied and interdisciplinary physics,Science,Particle physics,Physics]
---


They will squeeze the beams before collisions, replacing the quadrupoles in the LHC's triplets. Two types of these new quadrupole magnets of two different lengths (4.5 metres in US and 7.5 metres at CERN) are being developed. Last year, a 1.5 metre-long short model quadrupole, made of two coils from the LARP (LHC Accelerator Research Program) consortium and two from CERN, was tested in the United States, reaching a peak magnetic field of 13 T. Another short model, with three coils made at CERN and one in the US, was also tested at CERN later in the year, to verify the performance reproducibility. Another iteration of the assembly will be done in the second part of the year. In January 2017, a full-length 4.5 metre-long coil – a world-record-breaking length, for a Nb3Sn magnet in an accelerator – has been tested at the US Brookhaven National Laboratory and reached the nominal field value of 13.4 T.  Meanwhile at CERN, the winding of 7.15-metre-long coils already started in the Large Magnet Facility building.

<hr>

[Visit Link](https://phys.org/news/2017-06-crown-jewel-hl-lhc-magnets.html){:target="_blank" rel="noopener"}


