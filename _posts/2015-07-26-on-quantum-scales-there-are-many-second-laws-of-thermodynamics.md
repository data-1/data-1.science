---
layout: post
title: "On quantum scales, there are many second laws of thermodynamics"
date: 2015-07-26
categories:
author: University College London
tags: [Entropy,Heat,Physical chemistry,Branches of science,Systems theory,Physics,Theoretical physics,Applied and interdisciplinary physics,Science,Physical sciences,Applied mathematics,Thermodynamics,Mathematical physics]
---


The traditional second law of thermodynamics is sometimes thought of as a statistical law that only holds when there is a vast numbers of particles that make up a system, said Professor Jonathan Oppenheim (UCL Physics & Astronomy), one of the authors of the study. More recently, physicists have begun wondering whether the second law holds not just on average, for very large systems, but whether it holds for small individual systems, such as those with only a small number of particles. Surprisingly, the researchers found that not only does the second law hold at such small scales, but there are actually many other second laws at work. But there are additional second laws which constrain the way in which disorder can increase. These additional second laws, can be thought of as saying that there are many different kinds of disorder at small scales, and they all tend to increase as time goes on, said co-author Professor Michal Horodecki (Gdansk).

<hr>

[Visit Link](http://phys.org/news342703682.html){:target="_blank" rel="noopener"}


