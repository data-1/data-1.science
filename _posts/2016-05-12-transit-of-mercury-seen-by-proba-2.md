---
layout: post
title: "Transit of Mercury seen by Proba-2"
date: 2016-05-12
categories:
author:  
tags: [Astronomical transit,Mercury (planet),Sun,Solar System,Astronomy,Sky,Space science,Outer space,Bodies of the Solar System,Planetary science,Astronomical objects,Physical sciences,Local Interstellar Cloud,Planets,Celestial mechanics,Planets of the Solar System,Astronomical objects known since antiquity,Science]
---


As the smallest planet in the Solar System crossed the face of the Sun on Monday 9 May, one of ESA’s smallest satellites was watching. Proba-2, smaller than a cubic metre, monitors the Sun from Earth orbit with an extreme-ultraviolet telescope. It was able to spot Mercury’s transit of the Sun as a small black disc roughly four pixels in diameter. The Mercury transit was visible from Earth starting at 11:13 GMT and ending at 18:42 GMT. The total transit time was about 7 hours and 31 minutes.

<hr>

[Visit Link](http://www.esa.int/spaceinvideos/Videos/2016/05/Transit_of_Mercury_seen_by_Proba-2){:target="_blank" rel="noopener"}


