---
layout: post
title: "Inspiring the Next Generation of Open Source"
date: 2018-02-18
categories:
author: "The Linux Foundation"
tags: [Linux,Open source,Linus Torvalds,Linux Foundation,Software development,Open-source movement,Software,Technology,Intellectual works,Computing]
---


In the past couple of months, we’ve invited 13-year-old algorithmist and cognitive developer Tanmay Bakshi, 11-year-old hacker and cybersecurity ambassador Reuben Paul, and 15-year-old programmer Keila Banks to speak at Linux Foundation conferences. In 2014 when he was 12, Zachary Dupont wrote a letter to his hero Linus Torvalds. We arranged for Zach to meet Linus–a visit that helped clinch his love for Linux. We encourage everyone to find ways to bring more people of all ages into open source. The more people we bring into the community, the stronger we will be in the years ahead.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/inspiring-next-generation-open-source/){:target="_blank" rel="noopener"}


