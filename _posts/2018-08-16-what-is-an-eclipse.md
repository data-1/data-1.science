---
layout: post
title: "What is an eclipse?"
date: 2018-08-16
categories:
author: ""
tags: [Solar eclipse,European Space Agency,Eclipse,NASA,Hinode (satellite),International Space Station,Astronomical objects known since antiquity,Space exploration,Physical sciences,Spacecraft,Astronomy,Bodies of the Solar System,Flight,Astronautics,Space science,Spaceflight,Outer space,Space programs,Solar System]
---


What is a lunar eclipse? What is a solar eclipse? This short video explains the difference between these regularly occurring events that can be observed from Earth. Remember: never look directly at the Sun, even when partially eclipsed, without proper eye protection such as special solar eclipse glasses, or you risk permanent eye damage. Credits: ESA, ESA/CESAR (graphics, ground-based observations), NASA's Scientific Visualization Studio (partial lunar eclipse sequence) ESA/NASA (ISS footage), ESA/Royal Observatory of Belgium (Proba-2 footage), NASA/Hinode/XRT (Hinode image).

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2018/07/What_is_an_eclipse){:target="_blank" rel="noopener"}


