---
layout: post
title: "Sea turtle given a 3D-printed jaw implant after boat accident"
date: 2015-05-18
categories:
author: Dante D'Orazio, May
tags: [The Verge]
---


The turtle, which is marked as an endangered species, was struck by a boat propeller while swimming in its natural habitat. The accident shattered the animal's upper and lower jaw, leaving the turtle unable to eat on its own. That's when a group at Pamukkale University in Turkey decided to explore its options for healing the animal and bringing it back into the wild. The team partnered with BTech, a Turkish biotechnology company specializing in 3D-printed prosthetics, and after two months of research and development, a medical-grade titanium jaw was born. And just a few weeks ago, veterinarians and surgeons installed the jaw during a two-and-a-half hour surgery that's said to be the very first of its kind.

<hr>

[Visit Link](http://www.theverge.com/2015/5/17/8617339/sea-turtle-receives-3d-printed-jaw-implant-after-boat-accident){:target="_blank" rel="noopener"}


