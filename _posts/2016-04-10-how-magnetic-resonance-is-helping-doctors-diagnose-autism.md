---
layout: post
title: "How Magnetic Resonance Is Helping Doctors Diagnose Autism"
date: 2016-04-10
categories:
author:  
tags: [Autism,Autism spectrum,Magnetic resonance imaging,General Electric,Brain,Technology]
---


“MRI is helping us to understand the underlying biology of the condition,” Williams says. “If we can diagnose earlier and determine the differences in brain biology, then we can begin to develop potential treatments.”The researchers are using several different MRI protocols to look at the brain, including structural, functional and something called “diffusion tensor MRI tractography” (see top image). Its researchers have used MRI to show changes in thickness of the outer layer of the brain, called the cortex, between people with ASD and a control group. The latest research is studying the impact of neurotransmitters on ASD.Williams has been working with GE MRI scanners for several years. The data was acquired and processed on a GE MRI scanner(MR750) with a 3 Tesla magnet, using diffusion spectrum imaging accelerated with compressed sensing, a technique developed at GE Global Research.

<hr>

[Visit Link](http://www.gereports.com/how-magnetic-resonance-is-helping-doctors-diagnose-autism/){:target="_blank" rel="noopener"}


