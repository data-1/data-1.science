---
layout: post
title: "The mystery of quantum computers"
date: 2017-09-20
categories:
author: ""
tags: [Quantum computing,Computer,Technology,Branches of science,Computing,Computer science,Information Age,Applied mathematics,Science]
---


That's why scientists are working on computers using quantum physics, or quantum computers, which promise to be faster and more powerful than conventional computers. What is a quantum computer? An ordinary computer works with bits with a single binary value, either zero or one. That increases enormously the capacities of calculations. This algorithm, if running on a quantum computer, factorises integer numbers into prime factors faster than any known classical algorithm.

<hr>

[Visit Link](https://phys.org/news/2017-05-mystery-quantum.html){:target="_blank" rel="noopener"}


