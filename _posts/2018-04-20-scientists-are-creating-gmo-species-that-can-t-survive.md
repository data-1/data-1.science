---
layout: post
title: "Scientists Are Creating GMO Species That Can't Survive"
date: 2018-04-20
categories:
author: ""
tags: [Genetically modified organism,Genetic engineering,Life sciences,Biology,Genetics,Biotechnology,Technology,Nature]
---


However, as we genetically modify organisms, we run the risk of them mating with their non-modified counterparts, which could irreparably change wild species and disrupt whole populations and ecosystems. “This is a problem that has been recognized for a while, said Maciej Maselko, a postdoctoral researcher at the BioTechnology Institute and the University of Minnesota's (U of M) College of Biological Sciences (CBS), in a U of M news release. While previous attempts to prevent this breeding have centered on quarantining the modified organisms, Maselko has developed a tool that could prove to be more successful as it makes it impossible for modified species to effectively breed with their wild versions. He then modifies the organism so that it produces a protein that looks for the unmodified version of that specific gene, which it would only find if the modified organism tried to breed with its unmodified counterpart. Designer Fish  Maselko has already considered several potential applications for his synthetic incompatibility tool.

<hr>

[Visit Link](https://futurism.com/scientists-creating-gmo-species-cant-survive/){:target="_blank" rel="noopener"}


