---
layout: post
title: "LIGO and Virgo observatories detect gravitational wave signals from black hole collision"
date: 2017-10-08
categories:
author: "National Science Foundation"
tags: [LIGO,Virgo interferometer,Gravitational wave,Gravitational-wave observatory,First observation of gravitational waves,Gravitational-wave astronomy,Physical sciences,Astronomy,Astrophysics,Science,Gravity,General relativity,Space science,Physical cosmology,Physics,Celestial mechanics,Theory of relativity,Theories of gravitation]
---


Coordinated Universal Time (UTC) using the two National Science Foundation (NSF)-funded Laser Interferometer Gravitational-Wave Observatory (LIGO) detectors located in Livingston, Louisiana, and Hanford, Washington, and the Virgo detector, funded by CNRS and INFN and located near Pisa, Italy. The detection by the LIGO Scientific Collaboration (LSC) and the Virgo collaboration is the first confirmed gravitational wave signal recorded by the Virgo detector. Little more than a year and a half ago, NSF announced that its Laser Interferometer Gravitational Wave Observatory had made the first-ever detection of gravitational waves, which resulted from the collision of two black holes in a galaxy a billion light-years away, said NSF Director France Córdova. With the next observing run planned for fall 2018, we can expect such detections weekly or even more often. Beginning operations in September 2015, Advanced LIGO has conducted two observing runs.

<hr>

[Visit Link](https://phys.org/news/2017-09-ligo-virgo-observatories-black-hole.html){:target="_blank" rel="noopener"}


