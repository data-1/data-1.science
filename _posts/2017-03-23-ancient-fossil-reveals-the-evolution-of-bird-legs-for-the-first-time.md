---
layout: post
title: "Ancient fossil reveals the evolution of bird legs for the first time"
date: 2017-03-23
categories:
author: "University Of Manchester"
tags: [Fossil,Bird]
---


Credit: University of Manchester  Researchers from the UK and China have found that living birds have a more crouched leg posture than their ancestors, who are generally thought to have moved with straighter limbs similar to those of humans. The study, published in Nature Communications, highlights how birds shifted towards this more crouched posture. These soft tissues were not just preserved as an ashen replacement of the former tissue, as sometimes happens - rather, the structure of the tissues was preserved at a microscopic level, said Professor Baoyu Jiang, a co-author of the study from Nanjing University. In particular, the team found evidence of fragments of the collagen proteins that made up the leg ligaments, which matched the preservation at the microscopic tissue level of detail. The new information we gained about the anatomy of the cartilages and tendons show that this early bird had an ankle whose form fit an intermediate function between that of early dinosaurs and modern birds, said Professor John R. Hutchinson from the Royal Veterinary College, who led the study.

<hr>

[Visit Link](https://phys.org/news/2017-03-ancient-fossil-reveals-evolution-bird.html){:target="_blank" rel="noopener"}


