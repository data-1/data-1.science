---
layout: post
title: "The physical computing capabilities of the Raspberry Pi"
date: 2017-09-24
categories:
author: "Ben Nuttall
(Alumni)"
tags: [Raspberry Pi,General-purpose inputoutput,Computing,Computer engineering,Technology,Computers,Computer science,Computer hardware,Manufactured goods,Office equipment,Electronics industry,Information Age,Computer architecture,Electronics,Electrical engineering]
---


Read more about the function of the pins at pinout.xyz  Add-on boards / HATs  You can connect simple components directly to the pins using jumper wires, or you can use a breadboard to hold everything in place and allow components to share use of some pins. CC BY-SA Raspberry Pi Foundation  Alternatively, you can use add-on boards that provide embedded components on a PCB (printed circuit board), which sits on top of the Pi's GPIO pins. The GPIO Zero library provides a simple interface to GPIO devices and includes support for a range of components and add-on boards. off ( )  sleep ( 1 )  See my article on GPIO Zero and Raspberry Pi programming starter projects, and see the GPIO Zero documentation for more information. start_preview ( )  sleep ( 10 )  camera.

<hr>

[Visit Link](https://opensource.com/article/17/3/physical-computing-raspberry-pi){:target="_blank" rel="noopener"}


