---
layout: post
title: "Oldest ancient-human DNA details dawn of Neanderthals"
date: 2016-03-15
categories:
author: Callaway, Ewen Callaway, You Can Also Search For This Author In
tags: []
---


Meyer’s feat of recovery has revealed that the remains, from a cavern in northern Spain, represent early Neanderthals — and has pushed back estimates of the time at which the ancient predecessors of humans must have split from those of Neanderthals (M. Meyer et al. It suggested that at least one individual identified from the remains was more closely related to a group called Denisovans — known from remains found thousands of kilometres away in Siberia — than it was to European Neanderthals (M. Meyer et al. The nuclear DNA, Meyer’s team reports in Nature on 14 March, shows that the Sima hominins are in fact early Neanderthals. And its age suggests that the early predecessors of humans diverged from those of Neanderthals between 550,000 and 765,000 years ago — too far back for the common ancestors of both to have been Homo heidelbergensis, as some had posited. Credit: Javier Trueba, MADRID SCIENTIFIC FILMS “It’s fascinating and keeps us all on our toes trying to make sense of it all,” says Chris Stringer, a palaeoanthropologist at the Natural History Museum in London.

<hr>

[Visit Link](http://www.nature.com/news/oldest-ancient-human-dna-details-dawn-of-neanderthals-1.19557){:target="_blank" rel="noopener"}


