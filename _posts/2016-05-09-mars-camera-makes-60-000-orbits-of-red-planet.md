---
layout: post
title: "Mars camera makes 60,000 orbits of Red Planet"
date: 2016-05-09
categories:
author: Robert Burnham
tags: [Thermal Emission Imaging System,Mars,Curiosity (rover),2001 Mars Odyssey,Gale (crater),Planetary science,Planets of the Solar System,Outer space,Terrestrial planets,Astronomical objects known since antiquity,Solar System,Bodies of the Solar System,Spaceflight,Astronomy,Space science]
---


Seen shortly after local sunrise, clouds gather in the summit pit, or caldera, of Pavonis Mons, a giant volcano on Mars. These observations have allowed scientists to map the properties of the surface materials over nearly all of Mars. Mars Odyssey began orbiting the Red Planet on Oct. 23, 2001. In its orbit, the spacecraft always flies near each pole. The goal of the orbit change is to let THEMIS systematically observe the Martian atmosphere and surface shortly after local sunrise.

<hr>

[Visit Link](http://phys.org/news354126889.html){:target="_blank" rel="noopener"}


