---
layout: post
title: "New research introduces 'pause button' for boiling"
date: 2016-03-16
categories:
author: Syracuse University
tags: [Boiling,Heat transfer,American Association for the Advancement of Science,Water,Heat,Steam,Technology,Applied and interdisciplinary physics,Chemistry,Physical chemistry,Thermodynamics,Continuum mechanics,Mechanical engineering]
---


Gather your patience and put the old a watched pot never boils saying to the test. The experience might rival watching paint dry, but of course the water will eventually begin to boil. When it does, you'll see a flurry of bubbles form and quickly rise to the surface of the water. This method gives researchers the time necessary to microscopically study vapor bubbles and determine ways to optimize the boiling process--maximizing the amount of heat removal with a minimal rise in surface temperature. The new understanding is going to help researchers design surface structures to achieve desired heat transfer, accurately predict as well as enhance boiling in outer space where lack of gravity causes bubbles to stay stationary on a heated surface, and create next-generation technology for thermal management in electronics.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/su-nri022316.php){:target="_blank" rel="noopener"}


