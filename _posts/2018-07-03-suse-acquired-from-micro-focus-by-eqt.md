---
layout: post
title: "​SUSE acquired from Micro Focus by EQT"
date: 2018-07-03
categories:
author: "Steven Vaughan-Nichols, Senior Contributing Editor, July"
tags: []
---


Now, SUSE has announced EQT will buy it from Micro Focus for $2.5 billion. With this new ownership, SUSE expects to further its Linux offerings and its emerging open-source cloud and container product groups. Together with EQT we will benefit both from further investment opportunities and having the continuity of a leadership team focused on securing long-term profitable growth combined with a sharp focus on customer and partner success. The current leadership team has managed SUSE through a period of significant growth, and now, with continued investment in technology innovation and go to market capability, will further develop SUSE's momentum going forward. With more funds for growth and owners willing to give SUSE its head in Linux and further open-source software development, SUSE should flourish.

<hr>

[Visit Link](https://www.zdnet.com/article/suse-acquired-from-micro-focus-by-eqt/#ftag=RSSbaffb68){:target="_blank" rel="noopener"}


