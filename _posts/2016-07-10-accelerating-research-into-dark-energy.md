---
layout: post
title: "Accelerating research into dark energy"
date: 2016-07-10
categories:
author: "University College London"
tags: [Universe,Void (astronomy),Dark energy,Computer simulation,Galaxy,Accelerating expansion of the universe,Physical sciences,Science,Astronomy,Physical cosmology,Physics,Cosmology,Nature,Astrophysics,Space science]
---


A quick method for making accurate, virtual universes to help understand the effects of dark matter and dark energy has been developed by UCL and CEFCA scientists. We end up needing to take an average over hundreds of simulations to get a 'gold standard' prediction. The scientists say their method will speed up research into the unseen forces in the universe by allowing many model universes to be rapidly developed to test alternate versions of dark energy and dark matter. When they compared the output of the paired universes to that of the gold standard method - which averages 300 virtual universes to remove uncertainties - they found the results to be very similar. Because we can get a more accurate prediction in a single shot, we don't need to spend so much computer time on existing ideas and can instead investigate a much wider range of possibilities for what this weird dark energy might really be made from, said Dr Pontzen.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/ucl-ari070516.php){:target="_blank" rel="noopener"}


