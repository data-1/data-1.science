---
layout: post
title: "Forest-mapping instrument for space station passes major milestone"
date: 2016-07-02
categories:
author: "Elizabeth Zubritsky, Nasa'S Goddard Space Flight Center"
tags: [Global Ecosystem Dynamics Investigation,Goddard Space Flight Center,Forest,Earth sciences,Natural environment,Nature]
---


Credit: NASA/JPL-Caltech  A laser-based instrument for mapping the 3-D structure of Earth's forests has passed a major milestone toward deployment on the International Space Station (ISS). The Global Ecosystem Dynamics Investigation (GEDI), led by the University of Maryland, College Park, and built by NASA's Goddard Space Flight Center in Greenbelt, Maryland, successfully transitioned to Phase B, moving from requirements development and mission definition to preliminary design. GEDI will provide the first comprehensive, high-resolution measurements of the vertical canopy structure of Earth's temperate and tropical forests. NASA selected the GEDI proposal in July 2014 to join a growing suite of technologies deployed on the ISS providing key observations about Earth's environment. By making detailed maps of forest vertical structure, the GEDI science team members, working together with forest managers and those who make environmental policy, will help protect ecosystems and the vital services they provide.

<hr>

[Visit Link](http://phys.org/news/2015-09-forest-mapping-instrument-space-station-major.html){:target="_blank" rel="noopener"}


