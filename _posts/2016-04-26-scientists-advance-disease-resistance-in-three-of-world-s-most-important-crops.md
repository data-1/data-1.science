---
layout: post
title: "Scientists advance disease resistance in three of world's most important crops"
date: 2016-04-26
categories:
author: Two Blades Foundation
tags: [Plant disease resistance,Plant pathology,Wheat,Potato,Biotechnology,Phytophthora infestans,2Blades,Soybean,Rust (fungus),Agriculture]
---


They report the isolation of novel disease resistance genes and the successful transfer of resistance into wheat, soybean, and potato. The 2Blades Foundation supported the development of these efforts as part of the organization's mission to discover, advance, and deliver genetic improvements in crop disease resistance. The Nature Biotechnology reports focus on wheat stem rust, Asian soybean rust, and potato late blight, diseases that are difficult to control, and each capable of causing yield losses over 80%. One of the reports, by researchers at the John Innes Centre (Norwich, UK), working with researchers at CSIRO (Canberra, AU), details a key innovation in resistance gene identification called MutRenSeq (Mutational Resistance Gene Enrichment Sequencing) that they used to isolate two resistance genes, Sr22 and Sr45 . DOI: 10.1038/nbt.3543  Cintia G Kawashima et al. A pigeonpea gene confers resistance to Asian soybean rust in soybean, Nature Biotechnology (2016).

<hr>

[Visit Link](http://phys.org/news/2016-04-scientists-advance-disease-resistance-world.html){:target="_blank" rel="noopener"}


