---
layout: post
title: "Mapping winds and dune evolution on the Red Planet"
date: 2014-06-22
categories:
author: Thomas Deane, Trinity College Dublin
tags: []
---


This is a subset of NASA HiRISE image ESP_011909 taken of a dune in Proctor Crater on Mars, showing various directions of ripples on the surface of a Martian dune (image is approximately 480 m across. The resolution of the HiRISE data is so fine that the team was able to view individual ripples in the sand on Mars 225 million kilometres away from Earth. Modelling the wind flow in 3D enabled the scientists, for the first time, to see how large dunes on Mars modify localised wind flow. Dr Bourke said: I have always wondered which way the winds blow on Mars. Credit: H Viles  The scientists' results showed that established thinking using large-scale global air circulation models is too crude for understanding how winds move sand on the more complex surfaces of Mars (e.g. over sand dunes).

<hr>

[Visit Link](http://feeds.sciencedaily.com/~r/sciencedaily/~3/7oPCZYqVMQ8/140619145930.htm){:target="_blank" rel="noopener"}


