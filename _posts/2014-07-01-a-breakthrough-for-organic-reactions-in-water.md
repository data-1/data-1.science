---
layout: post
title: "A breakthrough for organic reactions in water"
date: 2014-07-01
categories:
author: McGill University 
tags: [Chemical reaction,Catalysis,Green chemistry,Chemical reactions,Organic chemistry,Materials,Chemical substances,Physical sciences,Chemistry]
---


Researchers discover way to use water as solvent in a reaction widely used to make chemical products  Green-chemistry researchers at McGill University have discovered a way to use water as a solvent in one of the reactions most widely used to synthesize chemical products and pharmaceuticals. Chao-Jun Li and Feng Zhou of McGill's Department of Chemistry report that they have discovered a catalytic system which for the first time allows direct metal-mediated reactions between aryl halides and carbonyl compounds in water. For the past two decades, researchers have been exploring ways to do away with chemists' traditional reliance on non-renewable petrochemical feedstocks and toxic solvents. One important method has involved replacing the toxic solvents used in metal-mediated reactions with water – something that was previously considered impossible. The Barbier-Grignard-Type Carbonyl Arylation Using Unactivated Aryl Halides in Water, Feng Zhou and Chao-Jun Li, Nature Communications, published June 26, 2014.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/mu-abf062514.php){:target="_blank" rel="noopener"}


