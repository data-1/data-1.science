---
layout: post
title: "Virus evolution differs by species of mosquito carrier"
date: 2016-04-05
categories:
author: Cell Press
tags: [Virus,West Nile virus,Infection,Mosquito,Microorganism,Pathogen transmission,Viral evolution,Mutation,Cell (biology),Species,Culex quinquefasciatus,Microbiology,Medical specialties,Biology,Biological evolution,Organisms,Infectious diseases]
---


The study, published March 31, 2016 in Cell Host & Microbe, could lead to new strategies for predicting and preparing for future viral outbreaks. They found that the genetic diversity of the virus population depended on the mosquito species, with the greatest accumulation of mutations occurring in the southern house mosquito, Culex quinquefasciatus. These mutations reduced the ability of West Nile virus populations to survive in infected chicken skin cells when competing against viruses that did not undergo mosquito transmission. quinquefasciatus are found in the south, and also seem to be more likely to generate new West Nile virus genotypes compared to more northern species. Cell Host & Microbe, Grubaugh et al.: Genetic Drift during Systemic Arbovirus Infection of Mosquito Vectors Leads to Decreased Relative Fitness during Host Switching http://dx.doi.org/10.1016/j.chom.2016.03.002  Cell Host & Microbe (@cellhostmicrobe), published by Cell Press, is a monthly journal that publishes novel findings and translational studies related to microbes (which include bacteria, fungi, parasites, and viruses).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/cp-ved032416.php){:target="_blank" rel="noopener"}


