---
layout: post
title: "Research challenges popular theory on origin of languages"
date: 2016-04-15
categories:
author: David Ellis, University Of Adelaide
tags: [Language,Europe,David Reich (geneticist)]
---


New research published today in the journal Nature, led by University of Adelaide ancient DNA researchers and the Harvard Medical School, shows that at least some of the Indo-European languages spoken in Europe were likely the result of a massive migration from eastern Russia. This new study is the biggest of its kind so far and has helped to improve our understanding of the linguistic impact of Stone Age migration, says co-first author Dr Wolfgang Haak, from the University of Adelaide's Australian Centre for Ancient DNA (ACAD). The first was the arrival of Europe's first farmers, who had spread from the Near East (modern-day Turkey). Co-first author of the study Dr Iosif Lazaridis, a postdoctoral fellow at Harvard Medical School, says remarkably, the hunter-gatherers that lived in Europe did not disappear after the first farmers moved in. Surprisingly, a third ancestry component, with its origins in the east, was found to be present in every Central European sample after 4500 years ago, but not before that time, marking the second population turnover.

<hr>

[Visit Link](http://phys.org/news344584488.html){:target="_blank" rel="noopener"}


