---
layout: post
title: "Mercury's core dynamo present early in planet's history"
date: 2015-09-05
categories:
author: American Association for the Advancement of Science (AAAS) 
tags: [Mercury (planet),Topography,MESSENGER,Bodies of the Solar System,Solar System,Astronomical objects known since antiquity,Planets of the Solar System,Outer space,Science,Astronomy,Space science,Planetary science]
---


As Mercury also has a magnetic field in operation today, the evidence for an ancient field suggests that Mercury's dynamo has persisted for billions of years. On April 30, 2015, the Messenger spacecraft - the first to orbit Mercury - crashed into the enigmatic planet. During the four years it orbited Mercury, Messenger provided vast amounts of data to scientists. Now, using low-altitude satellite observations from the spacecraft, Catherine Johnson et al. report a weak magnetization signal that appears to emanate from an ancient region of Mercury's crust. Neumann at NASA Goddard Space Flight Center in Greenbelt, MD; B.J.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/aaft-mcd051315.php){:target="_blank" rel="noopener"}


