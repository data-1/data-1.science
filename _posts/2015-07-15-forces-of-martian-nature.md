---
layout: post
title: "Forces of martian nature"
date: 2015-07-15
categories:
author: ""   
tags: [Noachis quadrangle,Impact crater,Hellas Planitia,Outer space,Space science,Astronomy,Mars,Planetary science,Planets of the Solar System,Astronomical objects known since antiquity,Earth sciences,Terrestrial planets,Planets,Bodies of the Solar System,Surface features of planets,Surface features of bodies of the Solar System,Surface features of Mars,Solar System]
---


Science & Exploration Forces of martian nature 10/07/2014 10647 views 124 likes  The surface of Mars is pocked and scarred with giant impact craters and rocky ridges, as shown in this new image from ESA’s Mars Express that borders the giant Hellas basin in the planet’s southern hemisphere. Hellespontus Montes in context The Hellas basin, some 2300 km across, is the largest visible impact structure in the Solar System, covering the equivalent of just under half the land area of Brazil. This view highlights the Hellespontus Montes, a rough chain of mountain-like terrain that runs around the rim of the basin, seen here as an uneven ridge curving across the top of the main colour, topography and 3D images, and extending to the right in the perspective view. Once inside the crater, the snow became trapped and soon covered by surface dust, before compacting to form ice. The number of concentric lines indicates many cycles of this process and it is possible that craters like these may still be rich in ice hidden beneath just tens of metres of surface debris.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Mars_Express/Forces_of_martian_nature){:target="_blank" rel="noopener"}


