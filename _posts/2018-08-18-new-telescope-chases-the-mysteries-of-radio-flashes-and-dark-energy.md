---
layout: post
title: "New telescope chases the mysteries of radio flashes and dark energy"
date: 2018-08-18
categories:
author: "Kavilan Moodley And Carolyn Crichton, The Conversation"
tags: [Hydrogen Intensity and Real-time Analysis eXperiment,Square Kilometre Array,MeerKAT,Astronomy,Science,Physical sciences,Space science,Technology,Nature]
---


Now a new telescope is being unveiled that will be built at the SKA South Africa site in the Karoo. What will HIRAX do, and how? HIRAX has two main science goals: to study the evolution of dark energy by tracking neutral hydrogen gas in galaxies, and to detect and localise mysterious radio flashes called fast radio bursts. HIRAX's large field of view will allow it to observe large portions of sky daily – so when the flashes happen, the instrument will be more likely to see them. So it's independent from the SKA and its precursor, the MeerKAT – but will benefit greatly from the South African investment in the SKA project, which gives it access to excellent infrastructure hosted by the South African Radio Astronomy Observatory.

<hr>

[Visit Link](https://phys.org/news/2018-08-telescope-mysteries-radio-dark-energy.html){:target="_blank" rel="noopener"}


