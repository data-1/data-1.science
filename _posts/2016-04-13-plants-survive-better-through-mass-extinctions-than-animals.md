---
layout: post
title: "Plants survive better through mass extinctions than animals"
date: 2016-04-13
categories:
author: University of Gothenburg
tags: [CretaceousPaleogene extinction event,Biodiversity,Extinction,Earth sciences,Biological evolution,Biogeochemistry,Organisms,Nature,Natural environment,Ecology]
---


But a new study led by researchers at the University of Gothenburg shows that plants have been very resilient to those events. During this long history, many smaller and a few major periods of extinction severely affected Earth's ecosystems and its biodiversity. Their findings show that mass extinction events had very different impacts among plant groups. This event had a great impact on the configuration of terrestrial habitats and led to the extinction of all dinosaurs except birds, but surprisingly it had only limited effects on plant diversity. University of Gothenburg, Sweden & University of Lausanne, Switzerland  Dr. Alexandre Antonelli  Phone +46 703 989570  E-mail: alexandre.antonelli@bioenv.gu.se.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/uog-psb021715.php){:target="_blank" rel="noopener"}


