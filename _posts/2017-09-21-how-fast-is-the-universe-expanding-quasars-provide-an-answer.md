---
layout: post
title: "How fast is the universe expanding? Quasars provide an answer"
date: 2017-09-21
categories:
author: "Ecole Polytechnique Fédérale de Lausanne"
tags: [Quasar,Hubbles law,Physical cosmology,Lambda-CDM model,Universe,Hubble Space Telescope,Expansion of the universe,Galaxy,Big Bang,Star,Gravitational lens,Natural sciences,Astronomical objects,Concepts in astronomy,Nature,Science,Space science,Cosmology,Astrophysics,Physics,Astronomy,Physical sciences]
---


The H0LiCOW collaboration, a cosmology project led by EPFL and Max Planck Institute and regrouping several research organizations in the world has made a new measurement of the Hubble constant, which indicates how fast the universe is expanding. Hubble's observations uncovered a constant that quantified this expansion, and which was later going to be named the Hubble constant. Quasars: a new measurement for the Hubble constant  The H0LiCOW collaboration has now independently measured the Hubble constant, exploiting a cosmic phenomenon called gravitational lensing, whereby the enormous mass of galaxies bends spacetime. But the distance that the quasar light travels in each image depends of the expansion of the Universe, set by the Hubble constant. As a consequence, measuring the time-delay between the lensed images of quasars provides a way to determine the Hubble constant.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/epfd-hfi012517.php){:target="_blank" rel="noopener"}


