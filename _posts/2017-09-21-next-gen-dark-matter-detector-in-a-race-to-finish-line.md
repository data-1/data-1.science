---
layout: post
title: "Next-gen dark matter detector in a race to finish line"
date: 2017-09-21
categories:
author: "Glenn Roberts Jr., Lawrence Berkeley National Laboratory"
tags: [XENON,Weakly interacting massive particles,Dark matter,Physical sciences,Particle physics,Physics,Nature,Science]
---


Light-amplifying devices known as photomultiplier tubes (PMTs), developed for use in the LUX-ZEPLIN (LZ) dark matter-hunting experiment, are prepared for a test at Brown University. An array of detectors, known as photomultiplier tubes, at the top and bottom of the liquid xenon tank are designed to pick up particle signals. LZ will be at least 50 times more sensitive to finding signals from dark matter particles than its predecessor, the Large Underground Xenon experiment (LUX), which was removed from SURF last year to make way for LZ. SLAC and Berkeley Lab collaborators are also developing and testing hand-woven wire grids that draw out electrical signals produced by particle interactions in the liquid xenon tank. To ensure unwanted particles are not misread as dark matter signals, LZ's liquid xenon chamber will be surrounded by another liquid-filled tank and a separate array of photomultiplier tubes that can measure other particles and largely veto false signals.

<hr>

[Visit Link](https://phys.org/news/2017-02-next-gen-dark-detector-finish-line.html){:target="_blank" rel="noopener"}


