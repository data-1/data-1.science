---
layout: post
title: "Increased atmospheric carbon dioxide makes trees use water more efficiently"
date: 2016-04-30
categories:
author: University Of Exeter
tags: [Stoma,Carbon dioxide,Climate change,Carbon dioxide in Earths atmosphere,Environmental science,Nature,Earth sciences,Physical geography,Natural environment,Climate variability and change]
---


When the CO 2 concentration in the air increases, the size of the stomatal opening reduces to regulate the amount of carbon acquired which minimises the water lost. As a result the so-called water use efficiency increases. In this study the researchers used measurements of carbon from tree-rings and computer models to quantify tree and forest responses to both climate variation and increased atmospheric CO 2 concentrations. The study showed that reduced stomatal opening increased water use efficiency by 14% in broadleaf species and by 22% in needleleaf species. Despite the CO 2 induced stomatal closure, the models showed that the consequences of a warming climate - lengthened growing seasons, increased leaf area and increased evaporation - resulted in a 5% increase in forest transpiration - the cycle of water through trees.

<hr>

[Visit Link](http://phys.org/news350559676.html){:target="_blank" rel="noopener"}


