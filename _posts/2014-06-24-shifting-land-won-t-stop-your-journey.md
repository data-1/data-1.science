---
layout: post
title: "Shifting land won't stop your journey"
date: 2014-06-24
categories:
author: European Space Agency
tags: [Landslide,Satellite,Technology]
---


Now, ESA has looked at using satellites to watch for hazards across broad areas that could affect road and rail networks. Similarly, the Matist project, led by Gamma Remote Sensing and Consulting in Switzerland, combined satellite and terrestrial radar information and satnav to follow ground movements in the mountainous regions of Switzerland and Austria – notably covering the dense rail networks and Austria's main road network. A visualisation of the motion data obtained by radar satellite scans mapped onto the geological susceptibility map for the Falkirk area in Scotland. The thick lines indicate the geological susceptibility for subsidence (A: low potential hazards; E: high potential hazard). Credit: Live Land  These studies used existing Earth observation data but Europe's Sentinel satellites will become a valuable resource once they come online.

<hr>

[Visit Link](http://phys.org/news322806984.html){:target="_blank" rel="noopener"}


