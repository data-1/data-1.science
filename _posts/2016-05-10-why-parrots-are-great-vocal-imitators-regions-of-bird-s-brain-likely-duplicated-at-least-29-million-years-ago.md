---
layout: post
title: "Why parrots are great vocal imitators: Regions of bird's brain likely duplicated at least 29 million years ago"
date: 2016-05-10
categories:
author: Duke University
tags: [Vocal learning,Parrot,Brain,Neuroscience,Cognitive science,Cognition,Animals]
---


An international team of scientists led by Duke University researchers has uncovered key structural differences in the brains of parrots that may explain the birds' unparalleled ability to imitate sounds and human speech. By examining gene expression patterns, the new study found that parrot brains are structured differently than the brains of songbirds and hummingbirds, which also exhibit vocal learning. The shells are relatively bigger in species of parrots that are well known for their ability to imitate human speech, the group found. advertisement  Before now, some scientists had assumed that the regions surrounding the cores had nothing to do with vocal learning. Most of the bird's vocal learning brain regions are tucked into areas that also control movement.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150624143154.htm){:target="_blank" rel="noopener"}


