---
layout: post
title: "World's smallest neutrino detector finds big physics fingerprint"
date: 2017-09-22
categories:
author: "DOE/Oak Ridge National Laboratory"
tags: [Neutrino,Neutron,Matter,Dark matter,Particle physics,Weakly interacting massive particles,Oak Ridge National Laboratory,Atomic nucleus,Standard Model,Physics,Nuclear physics,Physical sciences,Nature,Science]
---


The research, performed at ORNL's Spallation Neutron Source (SNS) and published in the journal Science, provides compelling evidence for a neutrino interaction process predicted by theorists 43 years ago, but never seen. The one-of-a-kind particle physics experiment at Oak Ridge National Laboratory was the first to measure coherent scattering of low-energy neutrinos off nuclei, said ORNL physicist Jason Newby, technical coordinator and one of 11 ORNL participants in COHERENT, a collaboration of 80 researchers from 19 institutions and 4 nations. Alikhanov of National Research Centre Kurchatov Institute; National Research Nuclear University Moscow Engineering Physics Institute), USA (Indiana University, Triangle Universities Nuclear Laboratory, Duke University, University of Tennessee-Knoxville, North Carolina Central University, Sandia National Laboratories at Livermore, University of Chicago, Lawrence Berkeley National Laboratory, New Mexico State University, Los Alamos National Laboratory, University of Washington, Oak Ridge National Laboratory, North Carolina State University, Pacific Northwest National Laboratory, University of Florida), Canada (Laurentian University), Republic of Korea (Korea Advanced Institute of Science and Technology and Institute for Basic Science). of Energy; photographer Jason Richards  CAPTION/CREDIT, #4 SLIDESHOW 4_Duke_ScholbergRichBarbeau.jpg  From left, Professor Kate Scholberg of Duke University is spokesperson for COHERENT, an experiment that detected interactions of high-intensity neutrinos produced by the SNS at ORNL. Image credit: COHERENT Collaboration; photographer Juan Collar  CAPTION/CREDIT, #8: SLIDESHOW 8_2017-P04678.jpg  From left, Jason Newby of ORNL and Yuri Efremenko of the University of Tennessee-Knoxville/ORNL check equipment for the COHERENT experiment at the SNS.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-08/drnl-wsn072817.php){:target="_blank" rel="noopener"}


