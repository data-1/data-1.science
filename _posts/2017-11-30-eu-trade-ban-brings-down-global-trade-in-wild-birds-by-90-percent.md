---
layout: post
title: "EU trade ban brings down global trade in wild birds by 90 percent"
date: 2017-11-30
categories:
author: "University of Copenhagen - Faculty of Science"
tags: [Biodiversity,Parrot,Bird,Natural environment,Ecology]
---


Trade of wild birds has dropped about 90% globally since the EU banned bird imports in 2005. A study published today in the recognized scientific journal Science Advances demonstrates how the EU's ban decreased the number of birds traded annually from about 1.3 million to 130.000. International trade of wild birds is a root cause of exotic birds spreading worldwide. Historically, Europe has been the main importer of wild birds globally. Likewise, our study shows that international bird trade is a main cause of exotic birds spreading around the world. Today, passerine birds constitute less than 20% while parrots have increased to almost 80% of all traded birds.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/fos--etb112017.php){:target="_blank" rel="noopener"}


