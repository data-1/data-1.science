---
layout: post
title: "Destination: Moon"
date: 2015-09-03
categories:
author: "$author"   
tags: [Closed captioning,Moon,Spaceflight,Outer space]
---


This 8-minute film gives an overview of the past, present, and future of Moon Exploration, from the Lunar cataclysm to ESA’s vision of what Lunar exploration could be. Why is the Moon important for science? What resources does the Moon have? Is there water? Why should we go back and how will we do it?

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2015/01/Destination_Moon){:target="_blank" rel="noopener"}


