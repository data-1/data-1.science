---
layout: post
title: "Calcium-based MRI sensor enables more sensitive brain imaging"
date: 2018-07-01
categories:
author: "Anne Trafton, Massachusetts Institute Of Technology"
tags: [Brain,Nervous system,Magnetic resonance imaging,Neuron,Synaptotagmin,Neurotransmission,Sensor,Medical imaging,Electroencephalography,Neuroimaging,Neuroscience]
---


Credit: Massachusetts Institute of Technology  MIT neuroscientists have developed a new magnetic resonance imaging (MRI) sensor that allows them to monitor neural activity deep within the brain by tracking calcium ions. Because calcium ions are directly linked to neuronal firing—unlike the changes in blood flow detected by other types of MRI, which provide an indirect signal—this new type of sensing could allow researchers to link specific brain functions to their pattern of neuron activity, and to determine how distant brain regions communicate with each other during particular tasks. Concentrations of calcium ions are closely correlated with signaling events in the nervous system, says Alan Jasanoff, an MIT professor of biological engineering, brain and cognitive sciences, and nuclear science and engineering, an associate member of MIT's McGovern Institute for Brain Research, and the senior author of the study. In tests in rats, the researchers showed that their calcium sensor can accurately detect changes in neural activity induced by chemical or electrical stimulation, deep within a part of the brain called the striatum. To do that, they designed a new sensor that can detect subtle changes in calcium concentrations outside of cells and respond in a way that can be detected with MRI.

<hr>

[Visit Link](https://phys.org/news/2018-04-calcium-based-mri-sensor-enables-sensitive.html){:target="_blank" rel="noopener"}


