---
layout: post
title: "Researchers create quantum dots with single-atom precision"
date: 2014-06-30
categories:
author: Naval Research Laboratory
tags: [Quantum mechanics,Atom,Quantum dot,Scanning tunneling microscope,Electron,Adatom,Molecule,Spectroscopy,Quantum tunnelling,Symmetry,Degenerate energy levels,Applied and interdisciplinary physics,Theoretical physics,Physical sciences,Chemistry,Science,Atomic molecular and optical physics,Scientific theories,Physical chemistry,Materials science,Physics]
---


The team assembled the dots atom-by-atom, using a scanning tunneling microscope (STM), and relied on an atomically precise surface template to define a lattice of allowed atom positions. The team assembled quantum dots consisting of linear chains of N = 6 to 25 indium atoms; the example shown here is a chain of 22 atoms. Stefan Fölsch, a physicist at the PDI who led the team, explained that the ionized indium adatoms form a quantum dot by creating an electrostatic well that confines electrons normally associated with a surface state of the InAs crystal. Credit: Stefan Fölsch/PDI  Because the indium atoms are strictly confined to the regular lattice of vacancy sites, every quantum dot with N atoms is essentially identical, with no intrinsic variation in size, shape, or position. By combining the invariance of quantum dot molecules with the intrinsic symmetry of the InAs vacancy lattice, the team created degenerate states that are surprisingly resistant to environmental perturbations by defects.

<hr>

[Visit Link](http://phys.org/news323328458.html){:target="_blank" rel="noopener"}


