---
layout: post
title: "Rub al Khali"
date: 2016-05-21
categories:
author:  
tags: [Rub al Khali,Dune,Space science,Earth sciences,Outer space]
---


Rolling sand dunes in the expansive Rub’ al Khali desert on the southern Arabian Peninsula are pictured in this image from the Sentinel-2A satellite. Precipitation rarely exceeds 35 mm a year and regular high temperatures are around 50°C. Looking closer at the dunes in the lower right, many have three or more ‘arms’ shaped by changing wind directions and are known as ‘star dunes’. The dunes are interspersed with hardened flat plains – remnants of shallow lakes that existed thousands of years ago, formed by monsoon-like rains and runoff. The multispectral instrument on Sentinel-2 uses parts of the infrared spectrum to detect subtle changes in vegetation cover, but can also see changes in mineral composition where vegetation is sparse.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2016/05/Rub_al_Khali){:target="_blank" rel="noopener"}


