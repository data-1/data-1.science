---
layout: post
title: "Weyl points, first predicted in 1929, observed for the first time"
date: 2016-05-14
categories:
author: Massachusetts Institute of Technology
tags: [Subatomic particle,Photonic crystal,Physics,News aggregator,Gyroid,Laser,Weyl semimetal,Photonics,Physicist,Science,Physical sciences,Applied and interdisciplinary physics,Theoretical physics]
---


Part of a 1929 prediction by physicist Hermann Weyl -- of a kind of massless particle that features a singular point in its energy spectrum called the Weyl point -- has finally been confirmed by direct observation for the first time, says an international team of physicists led by researchers at MIT. Ling Lu, a research scientist at MIT and lead author of that team's paper, says the elusive points can be thought of as equivalent to theoretical entities known as magnetic monopoles. In this case, Lu was able to calculate precise measurements for the construction of a photonic crystal predicted to produce the manifestation of Weyl points -- with dimensions and precise angles between arrays of holes drilled through the material, a configuration known as a gyroid structure. For example, photonic crystals based on this design could be used to make large-volume single-mode laser devices. Professor Soljačić's group has a track record of rapidly converting new science into creative devices with industry applications, and I am looking forward to seeing how Weyl photonics crystals evolve.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150716160601.htm){:target="_blank" rel="noopener"}


