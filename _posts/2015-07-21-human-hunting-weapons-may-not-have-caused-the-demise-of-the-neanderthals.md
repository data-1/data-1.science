---
layout: post
title: "Human hunting weapons may not have caused the demise of the Neanderthals"
date: 2015-07-21
categories:
author: Elsevier 
tags: [Early modern human,Human,Neanderthal,Human evolution,Recent African origin of modern humans,Elsevier,Branches of science,Science]
---


Technological innovation may not have led to the colonization of Europe by anatomically modern humans, suggests new study  Amsterdam, April 28, 2015 - The demise of Neanderthals may have nothing to do with innovative hunting weapons carried by humans from west Asia, according to a new study published in the Journal of Human Evolution. Previous models assumed that anatomically modern humans - our direct ancestors - were special in the way they behaved and thought. These models considered technological and cultural innovation as the reason humans survived and Neanderthals did not. The researchers studied stone tools that were used by people in the Early Ahmarian culture and the Protoaurignacian culture, living in south and west Europe and west Asia around 40,000 years ago. The article is published open access and is available on ScienceDirect:  http://www.sciencedirect.com/science/article/pii/S0047248415000500  Author contact details  Dr. Seiji Kadowaki  Nagoya University Museum, Nagoya University, Furo-cho, Chikusa-ku, Nagoya 464-8601, Japan  kadowaki@num.nagoya-u.ac.jp  +81 52 747 6711  About Journal of Human Evolution  The Journal of Human Evolution concentrates on publishing the highest quality papers covering all aspects of human evolution.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/e-hhw042815.php){:target="_blank" rel="noopener"}


