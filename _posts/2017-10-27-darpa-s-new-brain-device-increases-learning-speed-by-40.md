---
layout: post
title: "DARPA's New Brain Device Increases Learning Speed by 40%"
date: 2017-10-27
categories:
author:  
tags: [Transcranial direct-current stimulation,Brain,Memory,Executive functions,Learning,Prefrontal cortex,Cognitive psychology,Interdisciplinary subfields,Mental processes,Psychology,Cognition,Cognitive science,Neuropsychology,Neuroscience,Behavioural sciences,Psychological concepts,Cognitive neuroscience]
---


In their study published, which has been published in the journal Current Biology, the HRL team describes how they used non-invasive transcranial direct current stimulation (tDCS) to stimulate the prefrontal cortex in the macaques. The macaques that wore the tDCS brain device significantly outperformed the control group. The former needed only 12 trials to learn how to immediately get the reward, while the latter needed 21 trials, with the tDCS device accounting for the 40 percent increase in learning speed, according to the researchers. All in the Mind  The ability to increase one's brain function almost instantaneously is no doubt appealing. However, the DARPA researchers claim their device is cheap, which may make it more appealing than other technologies like it.

<hr>

[Visit Link](https://futurism.com/darpas-new-brain-device-increases-learning-speed-by-40/){:target="_blank" rel="noopener"}


