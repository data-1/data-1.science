---
layout: post
title: "Improved ape genome assemblies provide new insights into human evolution: Better understanding of genetic influences on primate and human brain differences was also gained through comparative organoid"
date: 2018-08-06
categories:
author: "University of Washington Health Sciences/UW Medicine"
tags: [Gene,Gene duplication,Human,Human genome,Genome,Ape,Mutation,Evolution,Retrovirus,Genetics,Chimpanzee,Brain,Hominidae,Human evolution,Life sciences,Biology,Biotechnology]
---


The highly contiguous, newly assembled genomes present a better resource for novel gene discovery and high-resolution comparative genomics amongst the great apes. advertisement  The researchers examined the possible influence of some of the genetic variants and gene function regulators on such areas as human and ape dietary differences, anatomy, and brain formation. All the genomes were sequenced and assembled using the same process. These genes are more likely to have gained additional copies in the human species, compared to other apes, through a process of gene duplication. The high quality sequence helped identify the source PtERV1, common to chimpanzees and gorillas.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180607141036.htm){:target="_blank" rel="noopener"}


