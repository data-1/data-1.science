---
layout: post
title: "For bees and flowers, tongue size matters"
date: 2015-07-17
categories:
author: Ecological Society of America 
tags: [Bumblebee,Bee,Flower,Petal,Bombus terrestris,Bombus ruderatus,Kingdoms (biology),Plants,Insects,Bees]
---


The research group came up with an equation to predict tongue length from a combination of body size and taxonomic relationships. Bartomeus will explain the equation and the usefulness of tongue length data for ecology at the 99th Annual Meeting of the Ecological Society of America in Sacramento, Cal., this August during the Pollination I oral session on Thursday afternoon, August 14. A bee collects pollen on its body as it laps sugar-rich nectar from within the cupped interior of the flower's petals, and carries the flower's genetic heritage away with it to fertilize the next flower of the same species that it visits. Long-tongued bees are often specialists, favoring a few deep-throated flower species. ###  Presentation  Contributed talk 122-4 - The allometry of bee tongue length and its uses in ecology  Thursday, August 14, 2014: 2:30 PM  Room 315, Sacramento Convention Center  Speaker  Ignasi Bartomeus, Estación Biológica de Doñana (EBD-CSIC), Sevilla, Spain  nacho.bartomeus@gmail.com  Session:  Contributed talks 102: Pollination I. Thursday, August 14, 2014: 1:30 PM-5:00 PM.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/esoa-fb071514.php){:target="_blank" rel="noopener"}


