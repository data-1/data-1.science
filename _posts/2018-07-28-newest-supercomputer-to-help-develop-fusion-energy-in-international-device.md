---
layout: post
title: "Newest supercomputer to help develop fusion energy in international device"
date: 2018-07-28
categories:
author: "DOE/Princeton Plasma Physics Laboratory"
tags: [Nuclear fusion,ITER,Princeton Plasma Physics Laboratory,National Energy Research Scientific Computing Center,Simulation,Supercomputer,Fusion power,Energy,Physics,Plasma (physics),Science,Technology]
---


Scientists led by Stephen Jardin, principal research physicist and head of the Computational Plasma Physics Group at the U.S. Department of Energy's (DOE) Princeton Plasma Physics Laboratory (PPPL), have won 40 million core hours of supercomputer time to simulate plasma disruptions that can halt fusion reactions and damage fusion facilities, so that scientists can learn how to stop them. Receipt of the highly competitive 2018 ASCR Leadership Computing Challenge (ALCC) award entitles the physicists to simulate the disruption on Cori, the newest and most powerful supercomputer at the National Energy Research Scientific Computing Center (NERSC) at Lawrence Berkeley National Laboratory. On Cori the team will run the M3D-C1 code primarily developed by Jardin and PPPL physicist Nate Ferraro. The simulations will also cover strategies for the mitigation of ITER disruptions, which could develop from start to finish within roughly a tenth of a second. PPPL, on Princeton University's Forrestal Campus in Plainsboro, N.J., is devoted to creating new knowledge about the physics of plasmas -- ultra-hot, charged gases -- and to developing practical solutions for the creation of fusion energy.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/dppl-nst072518.php){:target="_blank" rel="noopener"}


