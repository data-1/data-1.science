---
layout: post
title: "Researchers engineer bacteria to create fertilizer out of thin air"
date: 2018-07-17
categories:
author: "Washington University in St. Louis"
tags: [Cyanothece,Nitrogen fixation,Photosynthesis,Fertilizer,Cyanobacteria,Plant,Nature,Natural environment,Earth sciences]
---


Next step could be 'nitrogen-fixing' plants that can do the same, reducing the need for fertilizer  In the future, plants will be able to create their own fertilizer. The research was rooted in the fact that, although there are no plants that can fix nitrogen from the air, there is a subset of cyanobacteria (bacteria that photosynthesize like plants) that is able to do so. Cyanobacteria are the only bacteria that have a circadian rhythm, Pakrasi said. Researchers found Synechocystis was able to fix nitrogen at 2 percent of Cyanothece. Things got really interesting, however, when Liu, a postdoctoral researcher who has been the mainstay of the project, began to remove some of those genes; with just 24 of the Cyanothece genes, Synechocystis was able to fix nitrogen at a rate of more than 30 percent of Cyanothece.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/wuis-reb071618.php){:target="_blank" rel="noopener"}


