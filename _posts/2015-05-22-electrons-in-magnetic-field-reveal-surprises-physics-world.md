---
layout: post
title: "Electrons in magnetic field reveal surprises – Physics World"
date: 2015-05-22
categories:
author: ""    
tags: [Electron,Magnetism,Physics,Magnetic field,Quantum mechanics,Landau quantization,Cyclotron,Theoretical physics,Electromagnetism,Applied and interdisciplinary physics,Science,Physical sciences]
---


Beam out: elongated Landau states  The best glimpse yet of electrons moving in a magnetic field has revealed that the particles’ behaviour differs strongly from what is predicted by classical physics but is consistent with quantum-mechanical theory. Instead of rotating uniformly at a particular frequency, an international team of researchers has found that electrons in a magnetic field are capable of rotating at three different frequencies, depending on their quantum properties. Free-electron Landau states are a form of quantized state adopted by electrons moving through a magnetic field. This interaction causes electrons in a magnetic field to move in a corkscrew pattern. Vortex beams  The team did not observe the electrons’ Landau states directly.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/UB-JBHzwJaM/electrons-in-magnetic-field-reveal-surprises){:target="_blank" rel="noopener"}


