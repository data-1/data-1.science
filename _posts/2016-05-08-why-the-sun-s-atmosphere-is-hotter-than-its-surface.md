---
layout: post
title: "Why the Sun's atmosphere is hotter than its surface"
date: 2016-05-08
categories:
author: CNRS
tags: [Stellar corona,Sun,Atmosphere,Transparent materials,Phases of matter,Physics,Electromagnetism,Physical phenomena,Physical sciences,Astronomy,Applied and interdisciplinary physics,Space science,Nature]
---


A layer beneath the Sun's surface, acting as a pan of boiling water, is thought to generate a small-scale magnetic field as an energy reserve which, once it emerges from the star, heats the successive layers of the solar atmosphere via networks of mangrove-like magnetic roots and branches[1]. So what source of energy can heat the atmosphere and maintain it at such high temperatures? The scientists also discovered that a structure resembling a mangrove forest appears around the solar mesospots: tangled 'chromospheric roots' dive into the spaces between the granules, surrounding 'magnetic tree trunks' that rise up towards the corona and are associated with the larger-scale magnetic field. Thin plasma jets near the tree trunks are also produced and are associated with recently discovered spicules[3]. The researchers found that the energy fluxes of their mechanisms match those required by all studies to maintain the temperature of the plasma in the solar atmosphere, namely 4,500 W/m2 in the chromosphere and 300 W/m2 in the corona.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150617091757.htm){:target="_blank" rel="noopener"}


