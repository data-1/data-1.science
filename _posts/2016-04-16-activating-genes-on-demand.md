---
layout: post
title: "Activating genes on demand"
date: 2016-04-16
categories:
author: Harvard University
tags: [Gene,Cas9,Genome editing,Genetics,Gene expression,Stem cell,Cell (biology),Genome,DNA,Biotechnology,Biochemistry,Molecular biology,Biological engineering,Biology,Life sciences]
---


A new approach developed by Harvard geneticist George Church, Ph.D., can help uncover how tandem gene circuits dictate life processes, such as the healthy development of tissue or the triggering of a particular disease, and can also be used for directing precision stem cell differentiation for regenerative medicine and growing organ transplants. In terms of genetic engineering, the more knobs you can twist to exert control over the expression of genetic traits, the better, said Church, a Wyss Core Faculty member who is also Professor of Genetics at Harvard Medical School and Professor of Health Sciences and Technology at Harvard and MIT. Jonathan Schieman, Ph.D, of the Wyss Institute and Harvard Medical School, and Suhani Vora, of the Wyss Institute, Massachusetts Institute of Technology, and Harvard Medical School, are also lead co-authors on the study. But now, that DNA dark matter could be accessed using Cas9, allowing scientists to document which non-translated genes can be activated in tandem to influence gene expression. This is the first time that Cas9 has been leveraged to efficiently differentiate stem cells into brain cells.

<hr>

[Visit Link](http://phys.org/news344709538.html){:target="_blank" rel="noopener"}


