---
layout: post
title: "Topography shapes mountain biodiversity"
date: 2016-02-02
categories:
author: Ecole Polytechnique Fédérale de Lausanne
tags: [Biodiversity,Topography,Species,Habitat,Landscape,American Association for the Advancement of Science,Climate,Climate change,Physical geography,Nature,Natural environment,Earth sciences,Science,Ecology,Environmental science,Branches of science]
---


One implication of their findings, presented in the Proceedings of the National Academy of Sciences, is that moving to higher elevations to adapt to a warming climate could drive species into habitats with a whole different set of spatial properties. And their biodiversity can be increased further if many similar habitats are connected. In mountainous terrain, other factors come into play, such as temperature, biological productivity, and exposition. By transposing the findings from flat land to mountainous terrain, a team of researchers from across Switzerland has found a new way to explain the observation that biodiversity in mountainous terrain tends to peak at mid-altitudes. To test their intuition that the very structure a landscape can shape biodiversity patterns, Bertuzzo and his coauthors let loose a large number of virtual species on a mountainous terrain in a computer simulation.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/epfd-tsm020116.php){:target="_blank" rel="noopener"}


