---
layout: post
title: "XSEDE supercomputer allocations on Stampede1 and Comet help sample protein folding in bone regeneration study"
date: 2018-07-14
categories:
author: "University Of Texas At Austin"
tags: [Osteoblast,Biochemical cascade,Integrin,Bone,MAPKERK pathway,RUNX2,Protein,Biochemistry,Biotechnology,Cell biology,Biology,Molecular biology,Life sciences]
---


XSEDE supercomputers Stampede at TACC and Comet at SDSC helped study authors simulate the head piece domain of the cell membrane protein receptor integrin in solution, based on molecular dynamics modeling. The supercomputers helped scientists model how the cell membrane protein receptor called integrin folds and activates the intracellular pathways that lead to bone formation. She researches the development of new biomaterials based on silk. We are doing a basic research here with our silk-silica systems, Martín-Moldes explained. The final goal is to develop these models that help design the biomaterials to optimize the bone regeneration process, when the bone is required to regenerate or to minimize it when we need to reduce the bone formation.

<hr>

[Visit Link](https://phys.org/news/2017-12-xsede-supercomputer-allocations-stampede1-comet.html){:target="_blank" rel="noopener"}


