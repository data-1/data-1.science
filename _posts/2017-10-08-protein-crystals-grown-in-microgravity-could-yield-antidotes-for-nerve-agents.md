---
layout: post
title: "Protein crystals grown in microgravity could yield antidotes for nerve agents"
date: 2017-10-08
categories:
author: "University Of Utah"
tags: [X-ray crystallography,Crystallography,Acetylcholinesterase,Chemistry,Physical sciences]
---


The most commonly used FDA-approved antidote—Pralidoxime (2-PAM)—disengages the organophosphate molecule from the AChE protein, which can return to normal functions. Over the next four months, the cocktail will dehydrate the protein solution, allowing the AChE protein to crystalize and grow in the microgravity of the space station. Credit: DOE  Deep within the gorge lies the active enzyme site, where the neurotransmitter, acetylcholine, is broken down and antidotes function. Send the crystals to space. In space, those blocks are brought together in a more regular pattern to form a better crystal.

<hr>

[Visit Link](https://phys.org/news/2017-07-protein-crystals-grown-microgravity-yield.html){:target="_blank" rel="noopener"}


