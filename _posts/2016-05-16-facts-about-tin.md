---
layout: post
title: "Facts About Tin"
date: 2016-05-16
categories:
author: Stephanie Pappas
tags: [Tin,Metal,Topological insulator,Bronze,Alloy,Physical sciences,Chemical elements,Chemistry,Nature,Materials,Atoms,Artificial materials,Crystals,Manufacturing,Metals,Sets of chemical elements,Chemical substances]
---


Uses of tin  Perhaps the most important use of tin, historically, has been to make bronze — an alloy of copper and tin or other metals — that changed civilization by ushering in the Bronze Age. But tin still has its uses. Copper and other metals are mixed with tin to make pewter, which was once a common metal for tableware. Stanene is special because it is the first material able to conduct electricity with 100 percent efficiency at room temperature. But Xu and his colleagues found that when tin atoms are arranged in a single, honeycomb layer, the elements' properties change.

<hr>

[Visit Link](http://www.livescience.com/37355-tin.html){:target="_blank" rel="noopener"}


