---
layout: post
title: "Test site for ESA-backed airbreathing engine"
date: 2017-09-23
categories:
author: ""
tags: [SABRE (rocket engine),Rocket engine,Flight,Spaceflight,Aerospace,Technology,Vehicles,Rocketry,Space vehicles,Outer space,Propulsion,Transport,Astronautics,Spacecraft,Space programs,Engines,Space access]
---


Enabling & Support Test site for ESA-backed airbreathing engine 04/05/2017 8530 views 142 likes  Work began today on building the UK’s latest rocket engine test facility, designed for firing the engine core of the ESA-backed SABRE propulsion system within three years. The Synergistic Air-Breathing Rocket Engine is uniquely designed to scoop up atmospheric air during the early part of its flight to orbit. This slashes the need for the vehicle to carry bulky onboard oxygen for this part of the ascent, before switching to rocket mode drawing on internal propellants for its final climb to space. Such engines have the potential to revolutionise space launches, powering vehicles that can take off and land like aircraft. “The opening of this new test facility marks an historic moment for the European aerospace industry and for the UK research and development in rocket propulsion,” remarked Franco Ongaro, ESA Director of Technology, Engineering and Quality.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/Test_site_for_ESA-backed_airbreathing_engine){:target="_blank" rel="noopener"}


