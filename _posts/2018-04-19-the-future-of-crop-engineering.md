---
layout: post
title: "The future of crop engineering"
date: 2018-04-19
categories:
author: "Max Planck Society"
tags: [RuBisCO,Photosynthesis,Carbon dioxide,Biological carbon fixation,Protein,Protein folding,Enzyme,Biochemistry,Chemistry,Biology,Biotechnology,Physical sciences,Life sciences,Nature]
---


Scientists aim to boost photosynthesis to meet the increasing global demand for food by engineering its key enzyme Rubisco. To meet the global demand for food, scientists aim to increase the efficiency of photosynthesis and therefore crop productivity. Engineering of plant Rubisco, and photosynthesis, would be enhanced by functional expression of the enzyme in alternative hosts. The researchers generated functional plant Rubisco in a bacterial host by simultaneously expressing plant chaperones and Rubisco in the same cells. Superior Rubisco variants  Genetic engineering facilitates efforts to generate Rubisco variants with improved functional properties.

<hr>

[Visit Link](https://phys.org/news/2017-12-future-crop.html){:target="_blank" rel="noopener"}


