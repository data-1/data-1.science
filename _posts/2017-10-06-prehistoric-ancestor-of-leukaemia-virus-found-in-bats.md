---
layout: post
title: "Prehistoric ancestor of leukaemia virus found in bats"
date: 2017-10-06
categories:
author: "University Of Glasgow"
tags: [Virus,Retrovirus,Adult T-cell leukemialymphoma,Biology,Medical specialties]
---


It has long been thought that deltaretroviruses have infected humans since prehistoric times. Dr Robert Gifford from the MRC - University of Glasgow Centre for Virus Research, said: The discovery of this viral sequence fills the last major gap in the fossil record of retroviruses. Understanding the history of these viruses will help scientists to better understand how they affect people and animals now and in the future. A team working under researcher Dr Daniel Elleder at the Czech Academy of Sciences identified the remnants of a deltaretrovirus in the genome of ' bent-winged bats'. Over recent years, studies of these sequences have revealed the unexpectedly ancient origins of various retrovirus groups, and in doing so, have helped scientists understand the long-term 'evolutionary arms-race' between retroviruses and mammals.

<hr>

[Visit Link](https://phys.org/news/2017-03-prehistoric-ancestor-leukaemia-virus.html){:target="_blank" rel="noopener"}


