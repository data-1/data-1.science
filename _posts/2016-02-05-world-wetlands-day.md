---
layout: post
title: "World Wetlands Day"
date: 2016-02-05
categories:
author:  
tags: [Wetland,Natural environment,Earth sciences]
---


Wetlands are land areas that are permanently or seasonally saturated with water. They play an important role in the local environment and associated plant and animal life. On 2 February 1971, the ‘Ramsar Convention on Wetlands’ was adopted in the Iranian city of Ramsar to provide the framework for national action and international cooperation for the conservation and wise use of wetlands. Forty-five years later, the anniversary of that signing is being celebrated under the theme ‘Wetlands for our Future: Sustainable Livelihoods’. From their vantage point of 800 km high, Earth-observing satellites provide data and imagery on wetlands.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Highlights/World_Wetlands_Day){:target="_blank" rel="noopener"}


