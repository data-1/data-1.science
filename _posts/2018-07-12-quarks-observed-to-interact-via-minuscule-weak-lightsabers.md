---
layout: post
title: "Quarks observed to interact via minuscule 'weak lightsabers'"
date: 2018-07-12
categories:
author: "Atlas Experiment"
tags: [ATLAS experiment,W and Z bosons,Elementary particle,Large Hadron Collider,Quark,Standard Model,Quantum mechanics,Quantum field theory,Science,Nuclear physics,Physics,Physical sciences,Particle physics,Theoretical physics]
---


As light beams of photons from torches or lasers unaffectedly penetrate each other, electromagnetic lightsabers will forever stay science fiction. One quark in each of two colliding protons has to radiate a W or a Z boson. This continues the experiment's long journey to scrutinise the VBS process: using 8 TeV data from 2012, ATLAS had obtained the first evidence for the W±W± → W±W± process with 18 candidate events. Such a yield would occur with a probability of less than 1:3000 as a pure statistical fluctuation. Besides the decay products of the scattered W or Z bosons, the signature of the process are two high-energetic particle jets originating from the two quarks that initially radiated the W or Z.  ATLAS has also combined 2015 and 2016 data to establish the scattering of W±Z → W±Z with a statistical significance of 5.6 σ above background.

<hr>

[Visit Link](https://phys.org/news/2018-07-quarks-interact-minuscule-weak-lightsabers.html){:target="_blank" rel="noopener"}


