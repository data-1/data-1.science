---
layout: post
title: "Old data, new tricks: Fresh results from NASA's Galileo spacecraft 20 years on"
date: 2018-07-01
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Ganymede (moon),Magnetosphere,Aurora,Galileo (spacecraft),Jupiter,Magnetosphere of Jupiter,Solar wind,Moon,Solar System,Space science,Astronomy,Outer space,Planetary science,Bodies of the Solar System,Planets of the Solar System,Astronomical objects,Physical sciences,Planets,Spaceflight,Astronomical objects known since antiquity,Science,Sky]
---


The mission ended in 2003, but newly resurrected data from Galileo's first flyby of Ganymede is yielding new insights about the moon's environment -- which is unlike any other in the solar system. While most planets in our solar system, including Earth, have magnetic environments -- known as magnetospheres -- no one expected a moon to have one. Nestled there, it's protected from the solar wind, making its shape different from other magnetospheres in the solar system. However, unlike our planet, the particles causing Ganymede's auroras come from the plasma surrounding Jupiter, not the solar wind. When analyzing the data, the scientists noticed that during its first Ganymede flyby, Galileo fortuitously crossed right over Ganymede's auroral regions, as evidenced by the ions it observed raining down onto the surface of the moon's polar cap.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180430131826.htm){:target="_blank" rel="noopener"}


