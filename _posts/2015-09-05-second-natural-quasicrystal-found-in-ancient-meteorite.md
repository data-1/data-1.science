---
layout: post
title: "Second natural quasicrystal found in ancient meteorite"
date: 2015-09-05
categories:
author: Princeton  University 
tags: [Quasicrystal,Paul Steinhardt,Materials,Nature,Science,Physical sciences]
---


A team from Princeton University and the University of Florence in Italy has discovered a quasicrystal -- so named because of its unorthodox arrangement of atoms -- in a 4.5-billion-year-old meteorite from a remote region of northeastern Russia, bringing to two the number of natural quasicrystals ever discovered. The finding of a second naturally occurring quasicrystal confirms that these materials can form in nature and are stable over cosmic time scales, said Paul Steinhardt, Princeton's Albert Einstein Professor of Science and a professor of physics, who led the study with Luca Bindi of the University of Florence. But 5-sided pentagons or 10-sided decagons laid next to each will result in gaps between tiles. To confirm that this quasicrystal, which has the five-fold symmetry of a soccer ball, was indeed of natural origins, Steinhardt and a team of scientists including geologists from the Russian Academy of Sciences traveled to the region in 2011 and returned with additional samples which they analyzed at the University of Florence; the Smithsonian Museum in Washington, DC; the California Institute of Technology; and the Princeton Institute for the Science and Technology of Materials (PRISM) Imaging and Analysis Center. The researchers confirmed that the quasicrystal originated in an extraterrestrial body that formed about 4.57 billion years ago, which is around the time our solar system formed.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/pu-snq031615.php){:target="_blank" rel="noopener"}


