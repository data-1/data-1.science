---
layout: post
title: "The case of the relativistic particles solved with NASA missions"
date: 2018-06-02
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Van Allen radiation belt,Electromagnetic radiation,Van Allen Probes,Electron,General relativity,Wave,Magnetic field,Science,Physical phenomena,Astronomy,Applied and interdisciplinary physics,Physics,Physical sciences,Space science,Electromagnetism,Nature]
---


Scientists had previously identified certain factors that might cause particles in the belts to become highly energized, but they had not known which cause dominates. This process wasn't a widely accepted theory before the Van Allen Probes mission. We've had studies in the past that look at individual events, so we knew local acceleration was going to be important for some of the events, but I think it was a surprise just how important local acceleration was, said Alex Boyd, lead author and researcher at New Mexico Consortium, Los Alamos, New Mexico. There are two main causes of particle energization in the Van Allen belts: radial diffusion and local acceleration. However, early on in its mission, the Van Allen Probes showed that local acceleration, which is caused by particles interacting with waves of fluctuating electric and magnetic fields can also provide energy to the particles.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/nsfc-tco052918.php){:target="_blank" rel="noopener"}


