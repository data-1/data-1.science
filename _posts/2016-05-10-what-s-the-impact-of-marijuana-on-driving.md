---
layout: post
title: "What's the impact of marijuana on driving?"
date: 2016-05-10
categories:
author: University of Iowa
tags: [Cannabis (drug),Driving under the influence,Alcohol (drug),Medical cannabis,Tetrahydrocannabinol,Health,Social aspects of psychoactive drugs,Psychoactive drugs,Individual psychoactive drugs,Health sciences]
---


A new study conducted at the University of Iowa's National Advanced Driving Simulator has found drivers who use alcohol and marijuana together weave more on a virtual roadway than drivers who use either substance independently. The UI was selected for the research because of the NADS' authenticity to real driving and the university's expertise in medicine, pharmacy, and engineering. Brown said plenty of research has been done on the effects of drinking alcohol and driving, but little has been done to measure the effects of using marijuana and driving. Is cannabis an issue?' Drivers with only alcohol in their systems showed impairment in all three areas while those strictly under the influence of vaporized cannabis only demonstrated problems weaving within the lane.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150623180408.htm){:target="_blank" rel="noopener"}


