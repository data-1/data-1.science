---
layout: post
title: "Harnessing rich satellite data to estimate crop yield"
date: 2017-10-08
categories:
author: "University Of Illinois At Urbana-Champaign"
tags: [Infrared,Electromagnetic spectrum,Electromagnetic radiation,Microwave,Technology,Science]
---


Guan says this work is the first time that so many spectral bands, including visible, infrared, thermal, and passive and active microwave, and canopy fluorescence measurements have been brought together to look at crops. When we pull the shared information out from each data set, what's left is the unique information relevant to vegetation conditions and crop yield. Credit: University of Illinois at Urbana-Champaign  The study uncovers that the many satellite data sets share common information related to crop biomass grown aboveground. Visible or near-infrared bands typically used for crop monitoring are mainly sensitive to the upper canopy, but provide little information about deeper vegetation and soil conditions affecting crop water status and yield, says John Kimball from University of Montana, a long-term collaborator with Guan and a coauthor of the paper. Our study suggests that the microwave radar data at the Ku-band contains uniquely useful information on crop growth, Guan says.

<hr>

[Visit Link](https://phys.org/news/2017-08-harnessing-rich-satellite-crop-yield.html){:target="_blank" rel="noopener"}


