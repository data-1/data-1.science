---
layout: post
title: "Extracting and displaying data with awk"
date: 2018-08-01
categories:
author: "Jim Hall
(Correspondent)"
tags: [AWK,Information technology management,Computer science,Software development,Software engineering,Technology,Software,Computing,Intellectual works,Digital media]
---


If you need to extract data from files, or simply display data in a new way, you need a script. AWK (usually written as just awk) originates from the 1970s and has become a standard feature on any Unix system. Named after its inventors—Aho, Weinberger, and Kernighan—awk can tackle most jobs that you throw at it. GNU awk (gawk) is the free software version of awk. It's pretty easy to write scripts using gawk to grab data from text files and transform that data for use elsewhere.

<hr>

[Visit Link](https://opensource.com/article/18/7/cheat-sheet-awk){:target="_blank" rel="noopener"}


