---
layout: post
title: "The ALICE experiment at CERN makes precise comparison of light nuclei and antinuclei"
date: 2016-05-27
categories:
author: CERN
tags: [ALICE experiment,Large Hadron Collider,Proton,Particle physics,Antiproton,Deuterium,CERN,Neutron,Atomic nucleus,High-energy nuclear physics,Antiparticle,Theoretical physics,Nature,Science,Physical sciences,Nuclear physics,Physics]
---


The ALICE experiment at the Large Hadron Collider (LHC) at CERN(1) has made a precise measurement of the difference between ratios of the mass and electric charge of light nuclei and antinuclei. The measurements are based on the ALICE experiment's abilities to track and identify particles produced in high-energy heavy-ion collisions at the LHC. Measurements at CERN, most recently by the BASE experiment, have already compared the same properties of protons and antiprotons to high precision. The measurement by ALICE comparing the mass-to-charge ratios in deuterons/antideuterons and in helium-3/antihelium-3 conﬁrms the fundamental symmetry known as CPT in these light nuclei. The experiment makes precise measurements of the curvature of particle tracks in the detector's magnetic field and of the particles' time of flight, and uses this information to determine the mass-to-charge ratios for the nuclei and antinuclei.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150817132556.htm){:target="_blank" rel="noopener"}


