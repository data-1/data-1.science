---
layout: post
title: "Millions of liters of juice from 1 grapefruit"
date: 2016-04-23
categories:
author: Austrian Research Centre of Industrial Biotechnology (ACIB)
tags: [Nootkatone,Yeast,Life sciences,Biotechnology,Biology]
---


The Austrian Centre of Industrial Biotechnology (acib) uses the positive aspects of synthetic biology for the ecofriendly production of a natural compound. The substance is expensive (more than 4000 USD per kilo) and can be found only in minute quantities in grapefruits. Because the natural sources cannot meet the demands, the acib method replaces chemical synthesis - an energy-consuming and anything but environmentally friendly process. Pichler: With our method, the important and expensive terpenoid Nootkatone can be produced industrially in an environmentally friendly, economical and resource-saving way in useful quantities. ###  The acib research results were published in the journal Metabolic Engineering: http://goo.gl/xu2s0h  About acib  The Austrian Centre of Industrial Biotechnology (acib) is an international Research Centre for Industrial Biotechnology with locations in Vienna, Graz, Innsbruck, Tulln (A), Hamburg, Bielefeld (D), Pavia (I) and Barcelona (E).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/arco-mol041415.php){:target="_blank" rel="noopener"}


