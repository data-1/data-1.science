---
layout: post
title: "Watch live: Sentinel-2B launch"
date: 2017-09-23
categories:
author: ""
tags: [Sentinel-2A,Spaceflight technology,Space programs,Space policy of the European Union,Astronautics,Flight,Earth observation satellites of the European Space Agency,Pan-European scientific organizations,European Space Agency,Satellite groups,Bodies of the Solar System,Remote sensing programs,Geographical technology,Space science,Space technology,Rocketry,Imaging,Earth observation projects,Earth observation platforms,European Space Agency satellites,Applications of computer vision,Space traffic management,European Space Agency programmes,Technology,Vega (rocket),Space launch vehicles,Earth orbits,Outer space,Spaceflight,Satellites,Earth observation satellites,Satellites orbiting Earth,Spacecraft,Space vehicles,European space programmes,Copernicus Programme,Space agencies,Remote sensing]
---


The second in the two-satellite Sentinel-2 mission is set for launch from French Guiana on 7 March at 01:49 GMT (02:49 CET). Sentinel-2 is the second mission for Europe’s Copernicus environment monitoring programme. It carries a wide-swath high-resolution multispectral imager working in 13 spectral bands for a new perspective of our land and vegetation. The live webstream will begin at 01:30 GMT (02:30 CET) and end at 03:15 GMT (04:15 CET). Read more about Sentinel-2.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-2/Watch_live_Sentinel-2B_launch){:target="_blank" rel="noopener"}


