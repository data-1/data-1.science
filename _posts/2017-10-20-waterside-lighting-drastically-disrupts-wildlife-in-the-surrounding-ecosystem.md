---
layout: post
title: "Waterside lighting drastically disrupts wildlife in the surrounding ecosystem"
date: 2017-10-20
categories:
author: Frontiers
tags: [Ecology,Biodiversity,Ecosystem,Street light,Lighting,Insect,Predation,Earth sciences,Biogeochemistry,Systems ecology,Environmental science,Nature,Environmental conservation,Environmental social science,Earth phenomena,Physical geography,Biogeography,Nature conservation,Natural environment]
---


The potential ecological impacts of new lighting concepts on ecosystems should be considered by landscape- and urban planners, lighting engineers and ecologists  Streetlights near waterways attract flying insects from the water and change the predator community living in the grass beneath the lights. The findings, published today in Frontiers in Environmental Science, show that night-time artificial lighting could disrupt the surrounding ecosystem and biodiversity. The researchers turned the streetlamps on at one of the ditches every night, but left the lamps permanently off at the other ditch. Some of the traps caught insects as they emerged from the water in the ditches. As expected, the team also found far more flying insects near the illuminated lamps themselves, compared with the lamps that were never on.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/f-wld101317.php){:target="_blank" rel="noopener"}


