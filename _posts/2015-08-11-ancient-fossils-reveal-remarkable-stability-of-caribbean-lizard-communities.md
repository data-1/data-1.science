---
layout: post
title: "Ancient fossils reveal remarkable stability of Caribbean lizard communities"
date: 2015-08-11
categories:
author: ""   
tags: [Anolis,Fossil]
---


Not only do we see the community structure of these lizards has remained stable for 20 million years, it's also difficult to tell some of these fossil lizards apart from those alive today, says Kevin de Queiroz, a herpetologist at the Smithsonian's National Museum of Natural History and co-author of a study which appeared today in the Proceedings of the National Academy of Sciences. After first appearing on each of the four Greater Antillean Islands some 50 million years ago, Anolis lizards spread out on each island to occupy various niches in island trees. Now, amber fossils reveal it has been an incredibly long time: some 20 million years or greater. For example, lizards from the trunk-crown area of the tree are normally large- or medium-sized and green, and they resemble one another from island to island. Micro-CT scanning of the exceptionally well-preserved amber fossils was used to reconstruct the fully articulated skulls shown here.

<hr>

[Visit Link](http://phys.org/news/2015-07-ancient-fossils-reveal-remarkable-stability.html){:target="_blank" rel="noopener"}


