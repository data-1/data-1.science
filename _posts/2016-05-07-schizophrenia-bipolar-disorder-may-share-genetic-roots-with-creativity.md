---
layout: post
title: "Schizophrenia, bipolar disorder may share genetic roots with creativity"
date: 2016-05-07
categories:
author: King's College London
tags: [Mental disorder,Schizophrenia,Bipolar disorder,Creativity,Institute of Psychiatry Psychology and Neuroscience,Psychiatry,Behavioural sciences,Mental disorders,Mental health,Psychology,Health,Abnormal psychology,Clinical medicine,Diseases and disorders,Human diseases and disorders,Cognitive science,Causes of death,Psychological concepts]
---


Genes linked to creativity could increase the risk of developing schizophrenia and bipolar disorder, according to new research carried out by researchers at the Institute of Psychiatry, Psychology & Neuroscience (IoPPN) at King's College London. Published in Nature Neuroscience, this new study lends support to the direct influence on creativity of genes found in people with schizophrenia and bipolar disorder. These findings lend support to the direct influence of genetic factors on creativity, as opposed to the effect of sharing an environment with individuals who have schizophrenia or bipolar disorder. By knowing which healthy behaviours, such as creativity, share their biology with psychiatric illnesses we gain a better understanding of the thought processes that lead a person to become ill and how the brain might be going wrong.' 'Our findings suggest that creative people may have a genetic predisposition towards thinking differently which, when combined with other harmful biological or environmental factors, could lead to mental illness.'

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150608120145.htm){:target="_blank" rel="noopener"}


