---
layout: post
title: "LightSail team prepares for tests of mylar space wonder"
date: 2015-06-28
categories:
author: Nancy Owano
tags: [Solar sail,LightSail,Astronautics,Spaceflight technology,Aerospace,Technology,Sky,Spacecraft,Astronomy,Space science,Flight,Spaceflight,Outer space,Space vehicles]
---


The LightSail flight unit, about the size of a loaf of bread, has inside 32 meters square of mylar, and when it goes into space it will deploy the very large sail. according to the Society. Solar sail spacecraft capture light momentum with large, lightweight mirrored surfaces—sails. In this month's test, said ExtremeTech, the team will place it just high enough above Earth that it can deploy the sails and perform a system check before falling back down. If the sail deploys correctly, they can move on to funding a full mission.

<hr>

[Visit Link](http://phys.org/news350584917.html){:target="_blank" rel="noopener"}


