---
layout: post
title: "NASA's WFIRST spacecraft offers a huge step forward in our understanding of dark matter"
date: 2016-05-02
categories:
author: Tomasz Nowakowski, Astrowatch.Net
tags: [Nancy Grace Roman Space Telescope,Exoplanet,Coronagraph,Gravitational microlensing,Dark energy,Supernova,Milky Way,Dark matter,Science,Outer space,Astrophysics,Nature,Physics,Physical sciences,Space science,Astronomy]
---


WFIRST will survey large areas of the sky measuring the effects of dark matter on the distribution of galaxies in the universe. It will provide a huge step forward in our understanding of dark matter and dark energy, Brooke Hsu of NASA's Goddard Space Flight Center in Greenbelt, Md. All told, more than a billion galaxies will be observed by WFIRST, Hsu revealed. More than 2,000 supernovae will be observed, Hsu said. WFI will provide the wide-field imaging and slitless spectroscopic capabilities required to perform the dark energy, exoplanet microlensing, and near-infrared (NIR) surveys while the coronagraph instrument is being designed for the exoplanet high contrast imaging and spectroscopic science.

<hr>

[Visit Link](http://phys.org/news/2016-05-nasa-wfirst-spacecraft-huge-dark.html){:target="_blank" rel="noopener"}


