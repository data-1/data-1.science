---
layout: post
title: "New Horizons probes the mystery of Charon's red pole"
date: 2016-06-30
categories:
author: ""
tags: [Charon (moon),Tholin,New Horizons,Atmosphere of Pluto,Pluto,Bodies of the Solar System,Space science,Planetary science,Physical sciences,Astronomy,Solar System,Nature,Outer space]
---


Charon's polar regions are very cold, and I mean VERY cold! These temperatures (especially with Charon's extremely thin atmosphere) are too cold to support surface liquid: gases are deposited straight to solids, and solids sublimate directly to gases. So—unlike at Charon's warmer equator—any gases that arrive on the winter pole would freeze solid instead of escaping, a process scientists refer to as cold trapping. Carly Howett Credit: JHUAPL/SwRI  This new substance is called a tholin, and has been made in similar conditions in laboratories here on Earth. haron likely has gradually built up a polar deposit over millions of years as Pluto's atmosphere slowly escapes, during which time the surface is being irradiated by the sun.

<hr>

[Visit Link](http://phys.org/news/2015-09-horizons-probes-mystery-charon-red.html){:target="_blank" rel="noopener"}


