---
layout: post
title: "Why big dinosaurs steered clear of the tropics"
date: 2015-09-10
categories:
author: University of Utah 
tags: [Dinosaur,Fossil,Triassic,Plant,Wildfire,Earth sciences,Nature]
---


In the tropics, the only dinosaurs present were small carnivores. In the new study, the authors focused on Chinle Formation rocks, which were deposited by rivers and streams between 205 and 215 million years ago at Ghost Ranch (better known to many outside of paleontology as the place where artist Georgia O'Keeffe lived and painted for much of her career). Fossilized organic matter from decaying plants provided another window on climate shifts. Changes in the ratio of stable isotopes of carbon in the organic matter bookmarked times when plant productivity declined during extended droughts. Drought and fire  Wildfire burn temperatures varied drastically, the researchers found, consistent with a fluctuating environment in which the amount of combustible plant matter rose and fell over time.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/uou-wbd061015.php){:target="_blank" rel="noopener"}


