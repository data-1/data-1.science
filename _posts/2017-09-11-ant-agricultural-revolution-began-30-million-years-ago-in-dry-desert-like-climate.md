---
layout: post
title: "Ant agricultural revolution began 30 million years ago in dry, desert-like climate"
date: 2017-09-11
categories:
author: "Smithsonian"
tags: [Agriculture,Antfungus mutualism,Fungus,Biology]
---


This transition allowed the ants to achieve higher levels of complexity in farming, rivaling the agricultural practices of humans: the domestication of crops that became permanently isolated from their wild habitats and thereby grew dependent on their farmers for their evolution and survival. In the lower, primitive forms of ant agriculture -- which largely occur in wet rain forests--fungal crops occasionally escape from their ant colonies and return to the wild. In this study, the team was interested in learning when ants began practicing higher agriculture -- that is, when some fungal crops came to be dependent on the ant-fungus relationship for survival. According to the evolutionary tree they constructed, the first ants to transition to higher agriculture likely lived in a dry or seasonally dry climate. But if your ant farmer evolves to be better at living in a dry habitat, and it brings you along and it sees to all your needs, then you're going to be doing okay, Schultz said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/s-aar040617.php){:target="_blank" rel="noopener"}


