---
layout: post
title: "NASA mini-balloon mission maps migratory magnetic boundary"
date: 2016-05-21
categories:
author: NASA/Goddard Space Flight Center
tags: [Van Allen radiation belt,Earths magnetic field,Earth,Sun,Magnetic field,Magnetism,Solar wind,Coronal mass ejection,Applied and interdisciplinary physics,Outer space,Electromagnetism,Physics,Physical sciences,Nature,Astronomy,Space science]
---


During this solar storm, three BARREL balloons were flying through parts of Earth's magnetic field that directly connect a region of Antarctica to Earth's north magnetic pole - these parts of the magnetic field are called closed field lines, because both ends are rooted on Earth. Due to various causes - such as incoming clouds of solar material - the closed magnetic field lines can realign into open field lines and vice versa, changing the location of the boundary between open and closed magnetic field lines. The six BARREL balloons flying during the January 2014 solar storm were able to map these changes, and they found something surprising - the open-closed boundary moves relatively quickly, changing location within minutes. However, solar energetic electrons happen to be in the same energy range as those radiation belt electrons, meaning that BARREL can see both. However, the particle counts measured by the two balloons on the open-closed boundary matched up to those observed by the other BARREL balloons - hovering on closed or open field lines only - strengthening the case that BARREL's balloons were actually crossing the boundary between solar and terrestrial magnetic field.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/nsfc-nmm051916.php){:target="_blank" rel="noopener"}


