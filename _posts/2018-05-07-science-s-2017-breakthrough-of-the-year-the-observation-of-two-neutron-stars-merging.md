---
layout: post
title: "Science's 2017 Breakthrough of the Year: The observation of two neutron stars merging"
date: 2018-05-07
categories:
author: "American Association for the Advancement of Science (AAAS)"
tags: [American Association for the Advancement of Science,Supraorganizations,Learned societies,Professional associations,Scientific supraorganizations,Learned societies of the United States,Scientific societies,Scientific organizations,Science and technology,Science,Clubs and societies,Non-profit organizations,Observation,Washington DC,Economy of the United States]
---


Science has chosen as its 2017 Breakthrough of the Year the first observations of a neutron-star merger, a violent celestial event that transfixed physicists and astronomers. As the two neutron stars spiraled together 130 million light years away, they generated tiny ripples in the fabric of spacetime called gravitational waves, sensed by enormous gravitational wave detectors on Earth. This merger also triggered an explosion studied by hundreds of astronomers around the world. Gravitational waves are the gift that keeps on giving, explains News Editor Tim Appenzeller. Observers not only detected gravitational waves from a collision of two neutron stars; they also saw the event at all wavelengths of light, from gamma rays all the way to radio.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/aaft-s2b121817.php){:target="_blank" rel="noopener"}


