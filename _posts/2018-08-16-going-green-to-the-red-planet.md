---
layout: post
title: "Going green to the Red Planet"
date: 2018-08-16
categories:
author: ""
tags: [European Space Agency,Solar power,Rosetta (spacecraft),Outer space,Space exploration,Bodies of the Solar System,Astronautics,Solar System,Spaceflight,Astronomy,Space science]
---


Now, it’s using sunlight to generate electricity, significantly reducing energy costs. Whether orbiting a planet, following a comet or peering deep into our Universe, spacecraft have long used solar cells to generate electricity from sunlight to power their mission. Now, ESA’s deep-space ground station at New Norcia, Western Australia, is also being powered in part by sunlight, thanks to a new solar power ‘farm’ completed in August. New Norcia solar panels “In the coming summer months, given some sunny, clear skies, we even expect to be able to deliver electricity back to the local grid.” The installation began in 2015 and is expected to provide a full return on investment within about 15 years. “We expect to save 340 tonnes of carbon dioxide each year.”  Communicating deep into space Opened in 2003, New Norcia was the first of ESA’s trio of deep-space antennas, which provide full communication coverage for spacecraft voyaging into our Solar System.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Operations/Estrack/Going_green_to_the_Red_Planet){:target="_blank" rel="noopener"}


