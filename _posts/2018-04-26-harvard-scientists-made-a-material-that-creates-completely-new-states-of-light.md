---
layout: post
title: "Harvard Scientists Made a Material That Creates Completely New States of Light"
date: 2018-04-26
categories:
author: ""
tags: [Optics,Polarization (waves),Light,Physical chemistry,Mechanics,Radiation,Theoretical physics,Quantum mechanics,Waves,Science,Chemistry,Electrodynamics,Motion (physics),Natural philosophy,Physics,Atomic molecular and optical physics,Electromagnetic radiation,Applied and interdisciplinary physics,Physical phenomena,Physical sciences,Electromagnetism]
---


The tool uses polarisation to generate structures such as swirling vortices, spirals, and corkscrews that not only help explore light's properties, but also have potential practical applications, such as high-powered imaging. The new tool - a type of metasurface - uses this along with second type of angular momentum called spin angular momentum (also known as circular polarisation). It's previously been established that a single beam of light can exhibit both types of angular momentum, and that connecting them and using polarisation to control the OAM can result in beams with new and complex shapes, such as the aforementioned corkscrew. Only certain polarisations could connect to certain OAMs. Other previously proposed applications include the manipulation of microscopic objects, and imaging systems.

<hr>

[Visit Link](https://futurism.com/harvard-scientists-made-material-creates-completely-new-states-light/){:target="_blank" rel="noopener"}


