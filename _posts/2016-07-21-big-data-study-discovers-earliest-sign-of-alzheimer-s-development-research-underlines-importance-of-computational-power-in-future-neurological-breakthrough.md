---
layout: post
title: "'Big Data' study discovers earliest sign of Alzheimer's development: Research underlines importance of computational power in future neurological breakthrough"
date: 2016-07-21
categories:
author: "McGill University"
tags: [Alzheimers disease,Alzheimers Disease Neuroimaging Initiative,Positron emission tomography,Medical imaging,Brain,Neurology,Neuroscience,Clinical medicine,Medical specialties,Medicine]
---


The researchers found that, contrary to previous understanding, the first physiological sign of Alzheimer's disease is a decrease in blood flow in the brain. LOAD is the most common cause of human dementia and an understanding of the interactions between its various mechanisms is important to develop treatments. Previous research on the many mechanisms that make up LOAD has been limited in scope and did not provide a complete picture of this complex disease. That by itself is justification for ADNI and data sharing, he says. While this study is one of the most thorough ever published on the subject of Alzheimer's disease progression, Evans says he would like to go further, to not only record but determine the causes of each mechanism, which could be the key to unlocking better treatments.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/07/160712130229.htm){:target="_blank" rel="noopener"}


