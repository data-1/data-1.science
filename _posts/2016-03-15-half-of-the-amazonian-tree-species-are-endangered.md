---
layout: post
title: "Half of the Amazonian tree species are endangered"
date: 2016-03-15
categories:
author: Institut de Recherche pour le Développement (IRD)
tags: [Amazon rainforest,Deforestation,News aggregator,Biogeochemistry,Natural environment,Ecology,Environmental conservation,Nature conservation,Environmental protection,Nature,Natural resource management,Conservation biology,Organisms,Systems ecology,Environmental social science,Global environmental issues]
---


The Amazon Tree Diversity Network international consortium, which includes the IRD, has just revealed in the Science Advances journal that, according to the predicted deforestation scenarios, 36% to 57% of Amazonian species are at risk of disappearing, i.e. up to 8,700 species out of the 15,000 estimated during the first inventory of the Amazonian Basin, published two years ago. According to these results, when considered at the scale of the planet, it is feared that 40,000 tropical tree species may be exposed to a risk of extinction and that the proportion of endangered plants on the planet has increased to one fifth. One in two tree species could disappear due to deforestation in the Amazon. Some may even disappear before they can be observed and described… Should these results be confirmed, the amount of endangered plants on the planet would increase to 22%. Through statistical calculation, the total number of species had then been estimated at 15,000 -- compared to the 12,000 identified to date on the planet.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2016/02/160210111733.htm){:target="_blank" rel="noopener"}


