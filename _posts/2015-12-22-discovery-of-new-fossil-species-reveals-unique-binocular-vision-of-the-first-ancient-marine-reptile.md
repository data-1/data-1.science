---
layout: post
title: "Discovery of new fossil species reveals unique binocular vision of the first ancient marine reptile"
date: 2015-12-22
categories:
author: Dawn Fuller, University Of Cincinnati
tags: [Mosasaur,Eye,Phosphorosaurus,Predation,Owl,Fossil,Animal,Taxa,Animals]
---


The discovery reveals that the eye structure of these smaller mosasaurs was different from their larger cousins, whose eyes were on either side of their large heads, such as the eye structure of a horse. The eyes and heads of the larger mosasaurs were shaped to enhance streamlined swimming after prey that included fish, turtles and even small mosasaurs. As a result, Konishi says it's believed these smaller marine reptiles hunted at night, much like the owl does compared with the daytime birds of prey such as eagles. Credit: Takuya Konishi/University of Cincinnati  Painstaking Preservation  The fossil, enclosed in a rock matrix, was first discovered in 2009, in a small creek in northern Japan. Revealing what was inside the matrix while protecting the fossil was a painstaking process that took place at the Hobetsu Museum in Mukawa.

<hr>

[Visit Link](http://phys.org/news/2015-12-discovery-fossil-species-reveals-unique.html){:target="_blank" rel="noopener"}


