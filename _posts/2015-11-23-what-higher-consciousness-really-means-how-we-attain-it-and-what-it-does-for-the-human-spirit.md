---
layout: post
title: "What Higher Consciousness Really Means, How We Attain It, and What It Does for the Human Spirit"
date: 2015-11-23
categories:
author: Maria Popova
tags: [Mind,Consciousness,Concepts in metaphysics,Psychological concepts,Cognition,Philosophy,Psychology,Metaphysics of mind,Cognitive science]
---


“[Leonardo da Vinci’s] unique brain wiring … allowed him the opportunity to experience the world from the vantage point of a higher dimension,” Leonard Shlain wrote in his stimulating inquiry into the source of Leonardo’s genius. But what is “higher consciousness,” really, and can it be unmoored from the baggage of spiritualism and superstition to enrich our secular understanding of what it means to be human? We start to think of other people in a more imaginative way. Rather than criticize and attack, we are free to imagine that their behavior is driven by pressures derived from their own more primitive minds, which they are generally in no position to tell us about. […]  States of higher consciousness are, of course, desperately short lived.

<hr>

[Visit Link](https://www.brainpickings.org/2015/11/16/school-of-life-higher-consciousness/){:target="_blank" rel="noopener"}


