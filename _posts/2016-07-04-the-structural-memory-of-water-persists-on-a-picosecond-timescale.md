---
layout: post
title: "The structural memory of water persists on a picosecond timescale"
date: 2016-07-04
categories:
author: "Max Planck Institute for Polymer Research"
tags: [Molecule,Water,Infrared spectroscopy,Liquid,News aggregator,Materials,Atomic molecular and optical physics,Materials science,Chemistry,Physical chemistry,Physical sciences,Applied and interdisciplinary physics,Molecular physics]
---


A team of scientists from the Max Planck Institute for Polymer Research (MPI-P) in Mainz, Germany and FOM Institute AMOLF in the Netherlands has characterized the local structural dynamics of liquid water, i.e. how quickly water molecules change their binding state. Using innovative ultrafast vibrational spectroscopies, the researchers show why liquid water is unique when compared to most other molecular liquids. This study has recently been published in the scientific journal Nature Communications. With the help of a novel combination of ultrafast laser experiments, the scientists found that local structures persist in water for longer than a picosecond, a picosecond (ps) being one thousandth of one billionth of a second (10-12 s). For this purpose the team of scientists used ultrafast infrared spectroscopy, particularly focusing on water molecules that are weakly (or strongly) hydrogen-bonded to their neighboring water molecules.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150918083121.htm){:target="_blank" rel="noopener"}


