---
layout: post
title: "On the way to plastic-free oceans"
date: 2018-08-18
categories:
author: "Planetearth Online"
tags: [Microplastics]
---


Most microplastics come from bigger items - many of them only used once. She said: You might think that out here in the middle of the ocean we would see pristine waters but the Atlantic currents act like a huge whirlpool, drawing everything in from the coasts. At the moment we don't know how much microplastic pollution there is, we don't know how it behaves and we don't know how it affects sea life. From 2018, the UK government has banned one source of this pollution—called microbeads—from lots of everyday products. Since scientists began to understand more about microplastics pollution in the oceans, they've been working with government and businesses to stop more plastics from getting into the sea.

<hr>

[Visit Link](https://phys.org/news/2018-01-plastic-free-oceans.html){:target="_blank" rel="noopener"}


