---
layout: post
title: "Could we terraform Jupiter?"
date: 2015-08-07
categories:
author: Fraser Cain
tags: [Planet,Jupiter,Solar System,Sun,Terraforming,Planetary core,Astronomy,Space science,Planetary science,Physical sciences,Outer space,Astronomical objects,Planets,Nature]
---


And in the comments on that video, ABitOfTheUniverse threw down, he wants to know what it would take to terraform Jupiter. The mass of the core is still a mystery, but recent computer simulations put it at somewhere between 7 and 45 times the mass of the Earth, complete with plenty of water ices and other chemicals you might require on an Earthlike planet. What good solar system-spanning civilization hasn't worked out hydrogen fusion? You take a long space station, and light up fusion thrusters on both ends. Instead of trying to terraform Jupiter, we could just push the planet closer to the Sun, where its icy moons warm up and become habitable themselves.

<hr>

[Visit Link](http://phys.org/news/2015-08-terraform-jupiter.html){:target="_blank" rel="noopener"}


