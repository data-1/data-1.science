---
layout: post
title: "Physicists create first direct images of the square of the wave function of a hydrogen molecule"
date: 2018-07-03
categories:
author: "Lisa Zyga"
tags: [Electron,Wave function,Quantum mechanics,Physics,Molecule,Spectroscopy,Wave,Atomic molecular and optical physics,Physical sciences,Applied and interdisciplinary physics,Science,Chemistry,Physical chemistry,Scientific method,Atomic physics,Electromagnetism,Theoretical physics]
---


Published in Nature Communications  For the first time, physicists have developed a method to visually image the entanglement between electrons. As these correlations play a prominent role in determining a molecule's wave function—which describes the molecule's quantum state—the researchers then used the new method to produce the first images of the square of the two-electron wave function of a hydrogen (H 2 ) molecule. The wave function is not an observable in quantum physics, so it cannot be observed, Martín said. The researchers expect that the new approach can be used to image molecules with more than two electrons as well, by detecting the reaction fragments of multiple electrons. The method could also lead to the ability to image correlations between the wave functions of multiple molecules.

<hr>

[Visit Link](https://phys.org/news/2018-01-physicists-images-square-function-hydrogen.html){:target="_blank" rel="noopener"}


