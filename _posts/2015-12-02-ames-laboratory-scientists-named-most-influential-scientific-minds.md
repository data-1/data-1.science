---
layout: post
title: "Ames Laboratory scientists named 'Most Influential Scientific Minds'"
date: 2015-12-02
categories:
author: "$author" 
tags: [Ames Laboratory,Science,American Association for the Advancement of Science,Chemistry,Technology,Materials science,Physics,Applied and interdisciplinary physics]
---


Three Ames Laboratory physicists, Paul Canfield, Sergey Bud'ko, and Costas Soukoulis, were recently named to Thomson Reuters' World's Most Influential Scientific Minds 2014. The three were named highly cited researchers in a list compiled by Thomson Reuters of more than 3,000 scientists in 21 fields of research. Canfield and Bud'ko study the design, discovery, growth and characterization of novel electronic and magnetic compounds and their interesting physical properties. Canfield and Soukoulis are both distinguished professors in the Department of Physics and Astronomy at Iowa State University. ###  Ames Laboratory is a U.S. Department of Energy Office of Science national laboratory operated by Iowa State University.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/dl-als082814.php){:target="_blank" rel="noopener"}


