---
layout: post
title: "Ancient worlds could be kept 'alive' by gravitational nudges"
date: 2014-07-01
categories:
author: Adam Hadhazy, Astrobio.Net
tags: [Planetary habitability,Exoplanet,Planet,Solar System,Circumstellar habitable zone,Star,Earth,Io (moon),Orbit,Jupiter,Red dwarf,Orbital eccentricity,Geophysics,Tide,Terrestrial planet,Sun,Atmosphere,Planetary science,Space science,Astronomy,Planets,Stellar astronomy,Science,Astronomical objects,Nature,Outer space,Physical sciences]
---


In another few billion years, that heat will have largely dissipated and Earth's geophysical activity will cease. It stands to reason that among the first habitable zone planets we will get to study will be those around ancient, low-mass stars. If another planet's gravitational effect keeps the habitable-zone planet on a slightly eccentric orbit, that would promote internal tidal heating, said Greenberg. For a habitable planet to keep geophysically active, that first phase has to be short enough such that the planet does not volcanically load its atmosphere with carbon dioxide and end up lethally hot and desiccated. When our planet and even solar system become no longer inhabitable, an interstellar human race might find itself a new, permanent home around a red dwarf star.

<hr>

[Visit Link](http://phys.org/news322982758.html){:target="_blank" rel="noopener"}


