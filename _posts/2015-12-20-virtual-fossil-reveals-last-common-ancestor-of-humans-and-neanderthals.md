---
layout: post
title: "'Virtual fossil' reveals last common ancestor of humans and Neanderthals"
date: 2015-12-20
categories:
author: University Of Cambridge
tags: [Homo,Neanderthal,Human evolution,Early modern human,Human,Fossil,Pliocene,Evolution of primates,Pleistocene,Biological anthropology]
---


Now, researchers have applied digital 'morphometrics' and statistical algorithms to cranial fossils from across the evolutionary story of both species, and recreated in 3D the skull of the last common ancestor of Homo sapiens and Neanderthals for the first time. This allowed researchers to work out how the morphology of both species may have converged in the last common ancestor's skull during the Middle Pleistocene—an era dating from approximately 800 to 100 thousand years ago. However, results from the 'virtual fossil' show the ancestral skull morphology closest to fossil fragments from the Middle Pleistocene suggests a lineage split of around 700,000 years ago, and that—while this ancestral population was also present across Eurasia—the last common ancestor most likely originated in Africa. This allowed us to predict mathematically and then recreate virtually skull fossils of the last common ancestor of modern humans and Neanderthals, using a simple and consensual 'tree of life' for the genus Homo, he said. The 'virtual fossil' of last common ancestor of humans and Neanderthals as hypothesized in the new study.

<hr>

[Visit Link](http://phys.org/news/2015-12-virtual-fossil-reveals-common-ancestor.html){:target="_blank" rel="noopener"}


