---
layout: post
title: "The Maintainer’s Paradox: Balancing Project and Community"
date: 2018-05-07
categories:
author: "Sam Dean"
tags: [Open source,Linux,Open-source-software movement,Technology,Computing,Software engineering,Software development]
---


What are some of the challenges open source project maintainers face? One common issue is “The Maintainer’s Paradox,” which refers to the fact that open source maintainers are presented with more ideas along with more challenges as their communities grow. “Raymond said that with enough eyeballs, all bugs are shallow,” Bird noted, adding that the reference applies to large open source communities. Diversity of thought  “When I do training at Sony, I use a light bulb metaphor for this,” he said. You want to review patches carefully and give appropriate feedback, but being a maintainer is sometimes overwhelming.”  Bird displayed a large photo of a puppy as he said: “Every time you get a patch that implies a new feature branch, that is something that has to be cared for indefinitely.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/the-maintainers-paradox-balancing-project-and-community/){:target="_blank" rel="noopener"}


