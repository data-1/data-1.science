---
layout: post
title: "NASA's Hubble finds universe is expanding faster than expected"
date: 2016-06-03
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Big Bang,Universe,Hubble Space Telescope,Hubbles law,Dark energy,Galaxy,Space Telescope Science Institute,Astrophysics,Physical cosmology,Science,Cosmology,Nature,Physics,Physical sciences,Space science,Astronomy]
---


Astronomers using NASA's Hubble Space Telescope have discovered that the universe is expanding 5 percent to 9 percent faster than expected. This surprising finding may be an important clue to understanding those mysterious parts of the universe that make up 95 percent of everything and don't emit light, such as dark energy, dark matter, and dark radiation, said study leader and Nobel Laureate Adam Riess of the Space Telescope Science Institute and The Johns Hopkins University, both in Baltimore, Maryland. If we know the initial amounts of stuff in the universe, such as dark energy and dark matter, and we have the physics correct, then you can go from a measurement at the time shortly after the big bang and use that understanding to predict how fast the universe should be expanding today, said Riess. Another idea is that the cosmos contained a new subatomic particle in its early history that traveled close to the speed of light. The Hubble observations were made with Hubble's sharp-eyed Wide Field Camera 3 (WFC3), and were conducted by the Supernova H0 for the Equation of State (SH0ES) team, which works to refine the accuracy of the Hubble constant to a precision that allows for a better understanding of the universe's behavior.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/nsfc-nhf060216.php){:target="_blank" rel="noopener"}


