---
layout: post
title: "The science of carbon dioxide and climate"
date: 2017-10-06
categories:
author: "Earth Institute, Columbia University"
tags: [Climate change,Greenhouse effect,Greenhouse gas,Earth,Fuel,Fossil fuel,Carbon dioxide,Natural environment,Nature,Climate variability and change,Applied and interdisciplinary physics,Climate,Atmosphere,Physical geography,Earth sciences,Physical sciences]
---


The heat trapped by carbon dioxide warms our oceans and atmosphere. The concentration of greenhouse gases in the atmosphere – measured in parts per million of carbon dioxide – has drastically increased since the start of the Industrial Revolution, in the 18th Century. The changes to our climate largely match the effects expected from the increase in emission of greenhouse gases. Ice cores tell us that the atmosphere's carbon dioxide levels have stayed between 170 and 300 parts per million for the last 800,000 years, and any shifts took millennia to happen. The rise in carbon dioxide matches well with the curve of known human emissions.

<hr>

[Visit Link](https://phys.org/news/2017-03-science-carbon-dioxide-climate.html){:target="_blank" rel="noopener"}


