---
layout: post
title: "Watch the launch of Galileos 19–22"
date: 2018-08-16
categories:
author: ""
tags: [Satellite,Galileo (satellite navigation),Multistage rocket,Guiana Space Centre,Space programs,Rocketry,Flight,Spacecraft,Astronautics,Space vehicles,Outer space,Spaceflight,Spaceflight technology,Space science,Space access,Satellites,Aerospace,Rockets and missiles,Space launch vehicles,Astronomy,Space exploration]
---


Europe’s Galileo satellite navigation system will come a giant leap nearer completion on Tuesday 12 December, as four more Galileo satellites are launched into orbit by Ariane 5. Liftoff from Europe’s Spaceport in Kourou, French Guiana took place at 18:36 UTC (19:36 CET, 15:36 local time), carrying Galileo satellites 19–22. Separation of the upper stage occurred about nine minutes after liftoff, followed by the first firing of the upper stage. The upper stage – carrying four 715-kg Galileo satellites – is currently flying in ballistic configuration for three hours and eight minutes, after which a second upper stage firing will place it into circular separation orbit. Once stabilised at 3h 35 min after liftoff, the Galileo dispenser will release the first two satellites, followed by the second pair 20 minutes later.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Navigation/Watch_the_launch_of_Galileos_19_22){:target="_blank" rel="noopener"}


