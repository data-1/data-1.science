---
layout: post
title: "Researchers have developed a new class of artificial proteins"
date: 2016-08-22
categories:
author: "University of Southern Denmark"
tags: [Protein,Locked nucleic acid,Cancer,American Association for the Advancement of Science,Denmark,Biomolecules,Macromolecules,Cell biology,Molecular biophysics,Biochemistry,Biotechnology,Chemistry,Molecular biology,Biology,Life sciences,Structural biology,Physical sciences]
---


Nature has created a host of proteins, which come in many forms, and which have many functions in our body. In spite of this natural diversity, in the past 20 years or so there has been great scientific interest in creating artificial proteins, in part stimulated by drug development opportunities. The peptides coiled around one another effectively, creating an artificial protein. We forced three building blocks together and managed to make them form a protein mimic, says Professor Jesper Wengel of SDU. It paves the way for testing countless new combinations, which could create new artificial proteins with functions, which nature itself has not created, but which we need.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/uosd-rhd080916.php){:target="_blank" rel="noopener"}


