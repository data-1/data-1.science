---
layout: post
title: "Columbia Engineering-led team advances single molecule electronic DNA sequencing"
date: 2016-04-22
categories:
author: Columbia University School of Engineering and Applied Science
tags: [DNA sequencing,Nanopore,DNA,Protein biosynthesis,Nanopore sequencing,Life sciences,Chemistry,Physical sciences,Nucleic acids,Technology,Macromolecules,Genetics,Biochemistry,Biotechnology,Biology,Molecular biology]
---


Previously, researchers from the laboratories of Ju at Columbia and Kasianowicz at NIST reported the general principle of nanopore sequencing by synthesis (SBS), the feasibility of design and synthesis of polymer-tagged nucleotides as substrates for DNA polymerase, the detection and the differentiation of the polymer tags by nanopore at the single molecule level [Scientific Reports 2, 684 (2012) doi: 10.1038/srep00684; http://www.nature.com/articles/srep00684]. The current PNAS paper describes the construction of the complete nanopore SBS system to produce single molecule electronic sequencing data with single-base resolution. The tags on the polymer-labeled nucleotides, which were verified to be active substrates for DNA polymerase, produce different electrical current blockade levels. By blocking the channel's ionic current to different levels, the distinct tags provide a readout of the template sequence in real time with single-base resolution. The PNAS paper is titled: Real-Time Single Molecule Electronic DNA Sequencing by Synthesis Using Polymer Tagged Nucleotides on a Nanopore Array.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/cuso-cet042116.php){:target="_blank" rel="noopener"}


