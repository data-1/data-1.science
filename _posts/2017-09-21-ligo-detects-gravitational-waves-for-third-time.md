---
layout: post
title: "LIGO detects gravitational waves for third time"
date: 2017-09-21
categories:
author: "Massachusetts Institute of Technology"
tags: [LIGO,Virgo interferometer,Black hole,Binary black hole,Astrophysics,Physical phenomena,Astronomical objects,Theory of relativity,Gravity,Celestial mechanics,General relativity,Science,Physics,Physical sciences,Space science,Astronomy,Physical cosmology]
---


The entire LIGO and Virgo scientific collaborations worked to put all these pieces together. LIGO made the first-ever direct observation of gravitational waves in September 2015 during its first observing run since undergoing major upgrades in a program called Advanced LIGO. There are two primary models to explain how binary pairs of black holes can be formed. The first model proposes that the black holes are born together: they form when each star in a pair of stars explodes, and then, because the original stars were spinning in alignment, the black holes likely remain aligned. Because LIGO sees some evidence that the GW170104 black holes are non-aligned, the data slightly favor this dense stellar cluster theory.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-06/miot-ldg052617.php){:target="_blank" rel="noopener"}


