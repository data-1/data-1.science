---
layout: post
title: "CERN Researchers Are Planning to Trap and Truck Antimatter From One Facility to Another"
date: 2018-07-01
categories:
author: ""
tags: [Antimatter,Matter,Antiproton,Physics,Particle physics,Nature,Science,Physical sciences,Nuclear physics]
---


However, antimatter — the less-understood counterpart to the abundant matter that makes up you, me, and the universe — comes with a unique host of qualities, somewhat different from a baby's: like its willingness to explode in response to any contact whatsoever with matter. You can't simply put antimatter in a container and throw it in the back of a truck; in fact, any of those steps would spell disaster. Preparing the bottle that will transport antimatter. Image Credit: Julien Marius Ordan/CERN  At CERN, scientists operate the largest particle physics laboratory in the world. This is why CERN has designated a special project for this exact task — the antiProton Unstable Matter Annihilation (PUMA) project.

<hr>

[Visit Link](https://futurism.com/cern-transport-antimatter/){:target="_blank" rel="noopener"}


