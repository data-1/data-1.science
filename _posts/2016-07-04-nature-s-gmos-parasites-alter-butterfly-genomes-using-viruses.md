---
layout: post
title: "Nature's GMOs: Parasites Alter Butterfly Genomes Using Viruses"
date: 2016-07-04
categories:
author: "Charles Q. Choi"
tags: [Virus,Bracovirus,Gene,Caterpillar,Wasp,Braconidae,Lepidoptera,Parasitism,Genetic engineering,Insect,Biology]
---


Genetically modified organisms may usually be thought of as human creations, but scientists now find that monarch butterflies, silkworms, and many other butterflies and moths naturally possess genes from parasitic wasps. Butterflies and moths may have kept these wasp genes because they protect against other viruses, the researchers added. Parasitic insects known as braconid wasps lay their eggs inside the caterpillars of butterflies and moths. Now, scientists find that bracoviruses at times brought wasp genes with them. We must be aware of the type of genes that we add, since they could be also transferred to other insects, Herrero told Live Science.

<hr>

[Visit Link](http://www.livescience.com/52225-parasites-viruses-genetically-modify-butterflies.html){:target="_blank" rel="noopener"}


