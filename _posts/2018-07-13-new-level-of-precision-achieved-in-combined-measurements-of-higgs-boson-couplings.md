---
layout: post
title: "New level of precision achieved in combined measurements of Higgs boson couplings"
date: 2018-07-13
categories:
author: "Atlas Experiment"
tags: [Top quark,Standard Model,Higgs boson,ATLAS experiment,Particle physics,Physics beyond the Standard Model,W and Z bosons,Quark,Elementary particles,Nuclear physics,Subatomic particles,Science,Physical sciences,Quantum mechanics,Theoretical physics,Quantum field theory,Physics]
---


The overall production rate of the Higgs boson was measured to be in agreement with Standard Model predictions, with an uncertainty of 8%. The measurements are broken down into production modes (assuming Standard Model decay branching ratios), as shown in Figure 1. Together with the observation of production in association with a weak boson and of the H→bb decay in a separate measurement, these results paint a complete picture of Higgs boson production and decay. Figure 2: Ratios of coupling strengths to each particle. Therefore, possible new physics contributions can be tested by comparing the gluon coupling with the direct measurement of the top quark coupling in Higgs boson production in association with top quarks, as shown in Figure 2.

<hr>

[Visit Link](https://phys.org/news/2018-07-precision-combined-higgs-boson-couplings.html){:target="_blank" rel="noopener"}


