---
layout: post
title: "Breakthrough in industrial CO2 usage"
date: 2018-07-27
categories:
author: "Technical University of Munich (TUM)"
tags: [Enzyme,Methionine,Protein,Chemical reaction,Amino acid,Product (chemistry),Biocatalysis,Carbon dioxide,Biochemistry,Chemical substances,Chemistry,Physical sciences,Nature]
---


Professor Arne Skerra of the Technical University of Munich (TUM) has succeeded for the first time in using gaseous CO2 as a basic material for the production of a chemical mass product in a biotechnical reaction. Based on the idea that methionine in microorganisms is degraded by enzymes to methional with the release of CO2, we tried to reverse this process, explains Professor Arne Skerra from the Department of Biological Chemistry at TUM, because every chemical reaction is in principle reversible, while often only with the extensive use of energy and pressure. Supported by postdoctoral researcher Lukas Eisoldt, Skerra began to determine the parameters for the manufacturing process and for producing the necessary biocatalysts (enzymes). In the future, the basic principle of this novel biocatalytic reaction can serve as a model for the industrial production of other valuable amino acids or precursors for pharmaceuticals. Publication:  Julia Martin, Lukas Eisoldt and Arne Skerra: Fixation of gaseous CO2 by reversing a decarboxylase for the biocatalytic synthesis of the essential amino acid L-methionine, Nature Catalysis 07/2018.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/tuom-bii072518.php){:target="_blank" rel="noopener"}


