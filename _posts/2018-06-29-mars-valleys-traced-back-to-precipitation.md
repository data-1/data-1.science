---
layout: post
title: "Mars valleys traced back to precipitation"
date: 2018-06-29
categories:
author: "ETH Zurich"
tags: [Valley network (Mars),Mars,Rain,Nature,Earth sciences,Physical geography,Planetary science,Planets of the Solar System,Terrestrial planets,Hydrology,Planets,Astronomical objects known since antiquity,Water]
---


Scientists therefore assume that there must have been once enough water on the red planet to feed water streams that incised their path into the soil. A new study now suggests that the branching structure of the former river networks on Mars has striking similarities with terrestrial arid landscapes. This has been demonstrated in a recent paper published in Science Advances by physicist Hansjörg Seybold from the group of James Kirchner, ETH professor at the Institute for Terrestrial Ecosystems, and planetary specialist Edwin Kite from the University of Chicago. The distribution of the branching angles of the valleys on Mars is very similar to those found in arid landscapes on Earth. Evaporation made it rain  One hypothesis suggests that the northern third of Mars was covered by an ocean at that time.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/ez-mvt062818.php){:target="_blank" rel="noopener"}


