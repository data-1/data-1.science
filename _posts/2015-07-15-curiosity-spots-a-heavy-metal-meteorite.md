---
layout: post
title: "Curiosity spots a heavy metal meteorite"
date: 2015-07-15
categories:
author: Jason Major
tags: [Curiosity (rover),Chemistry and Camera complex,Opportunity (rover),Spirit (rover),Space science,Outer space,Spaceflight,Mars]
---


2-meter wide iron meteorite dubbed “Lebanon,” as imaged by Curiosity’s ChemCam and Mastcam on May 25, 2014. Credit: NASA/JPL-Caltech/LANL/CNES/IRAP/LPGNantes/CNRS/IAS/MSSS  Talk about heavy metal! This shiny, lumpy rock spotted by NASA's Curiosity rover is likely made mostly of iron—and came from outer space! It's an iron meteorite, similar to ones found in years past by Curiosity's forerunners Spirit and Opportunity, but is considerably larger than any of the ones the MER rovers came across… in fact, at 2 meters (6.5 feet) wide this may very well be the biggest meteorite ever discovered on Mars! The images were taken on mission Sol 640 (May 25, 2014) and have been adjusted to simulate more Earth-like illumination. Dubbed Lebanon, the large meteorite has a smaller fragment lying alongside it, named Lebanon B.

<hr>

[Visit Link](http://phys.org/news324718363.html){:target="_blank" rel="noopener"}


