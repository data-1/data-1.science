---
layout: post
title: "The Future of Automated Testing"
date: 2018-07-18
categories:
author: "Jun."
tags: []
---


2) Systems that can leverage ML since there are more things to keep up with than a human can see . Developers will be working on architecture, user interface designs and interactions, and writing code that works. Developers will be working on architecture, user interface designs and interactions, and writing code that works. AI/ML are part of the solution as teams generate more data. We will be able to leverage AI/ML to self-heal automated tests that used to work without an engineer having to come in and make changes.

<hr>

[Visit Link](https://dzone.com/articles/the-future-of-automated-testing?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


