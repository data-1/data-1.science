---
layout: post
title: "Linux Commands - Complete Guide"
date: 2018-08-28
categories:
author: "Nishali Wijesinghe"
tags: [Package manager,User identifier,Yum (software),BIOS,Symbolic link,APT (software),Secure copy protocol,Domain Name System,File system,Group identifier,Passwd,Command-line interface,Redirection (computing),Operating system technology,Computer hardware,Unix,Software development,Software engineering,Utility software,Computer data,Information technology management,Computer science,Computers,Computer engineering,Software,Technology,System software,Computer architecture,Computing]
---


The following uname command with a option displays all information about the operating system. Last command displays a list of all user logged in (and out) from '/var/log/wtmp' since the file was created. 27. who  Who command is a tool print information about users who are currently logged in. File Related Linux Commands  These commands are used to handle files and directories. By default, running mkdir without any option, it will create a directory under the current directory.

<hr>

[Visit Link](https://linoxide.com/linux-how-to/linux-commands-brief-outline-examples/){:target="_blank" rel="noopener"}


