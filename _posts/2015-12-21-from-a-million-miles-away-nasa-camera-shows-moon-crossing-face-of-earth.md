---
layout: post
title: "From a million miles away, NASA camera shows moon crossing face of Earth"
date: 2015-12-21
categories:
author: NASA/Goddard Space Flight Center
tags: [Deep Space Climate Observatory,Moon,Far side of the Moon,Astronomical objects,Physical sciences,Astronomical objects known since antiquity,Astronautics,Planets of the Solar System,Space science,Astronomy,Outer space,Planetary science,Bodies of the Solar System,Sky,Solar System,Spaceflight]
---


The series of test images shows the fully illuminated dark side of the moon that is never visible from Earth. About twice a year the camera will capture the moon and Earth together as the orbit of DSCOVR crosses the orbital plane of the moon. The North Pole is in the upper left corner of the image, reflecting the orbital tilt of Earth from the vantage point of the spacecraft. This natural lunar movement also produces a slight red and blue offset on the left side of the moon in these unaltered images. These images, showing different views of the planet as it rotates through the day, will be available 12 to 36 hours after they are acquired.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/nsfc-fam080515.php){:target="_blank" rel="noopener"}


