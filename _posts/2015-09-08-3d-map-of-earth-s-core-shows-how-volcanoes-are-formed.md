---
layout: post
title: "3D map of Earth’s core shows how volcanoes are formed"
date: 2015-09-08
categories:
author: Gordon Hunt, Gordon Hunt Was A Journalist With Silicon Republic
tags: [Mantle plume,Volcano,Hotspot (geology),Lithosphere,Applied and interdisciplinary physics,Planetary science,Structure of the Earth,Nature,Geophysics,Planets of the Solar System,Earth sciences,Physical sciences,Terrestrial planets,Geology]
---


Seismologists have created the first 3D visualisation of the Earth’s interior, showing the role that plumes of hot rock spewing out of the planet’s core have in the formation of volcanic islands. Researchers at UC Berkeley managed to generate the map with the help of a US department of energy supercomputer, showing how the hot molten rock doesn’t rise up in a straight line. Rather, the plumes – which are 400-degrees hotter than surrounding rock, and are far wider below the surface than geophysicists had presumed – rise haphazardly and then widen as they get to the surface. The latter aspect is what shapes volcanic islands, like Hawaii, with the lower rock often not directly connected to what we consider islands, thanks to a meandering rise. “So while the tops of the plumes are associated with hotspot volcanoes, they are not always vertically under them.”  This research, published in Nature, means that those childhood schoolbooks that showed volvanoes, with a red line of lava going straight down the middle, directly into the Earth’s core, are wrong.

<hr>

[Visit Link](https://www.siliconrepublic.com/earth-science/2015/09/07/3d-map-of-earths-core-shows-how-volcanoes-are-formed){:target="_blank" rel="noopener"}


