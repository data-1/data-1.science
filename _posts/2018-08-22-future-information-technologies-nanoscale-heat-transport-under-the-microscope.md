---
layout: post
title: "Future information technologies: Nanoscale heat transport under the microscope"
date: 2018-08-22
categories:
author: "Helmholtz-Zentrum Berlin für Materialien und Energie"
tags: [Electron,News aggregator,X-ray crystallography,Laser,Materials,Technology,Physics,Physical sciences,Materials science,Physical chemistry,Electromagnetism,Applied and interdisciplinary physics,Chemistry]
---


Measurements taken with extremely short X-ray pulses have now shown that the heat is distributed a hundred times slower than expected in the model system. Using an ultra-short laser pulse (50 femtoseconds), the physicists introduced heat locally into the model system, then with extremely short X-ray pulses (200 femtoseconds), determined how the heat was distributed in the two nanolayers over time. Because the electron system in nickel is much more strongly coupled to the nickel crystal lattice vibrations than in the case of gold, the nickel crystal lattice absorbs the heat from the nickel electrons faster and the nickel electrons initially cool. However, since the heat conduction through the now warmer but poorly conducting nickel crystal lattice directly to the cooler gold crystal lattice is very low, the thermal energy finds another pathway from the warmer nickel lattice to the cooler gold lattice. In order to reach thermal equilibrium, thermal energy flows back from the nickel lattice via the nickel electrons to the gold electrons that in turn excite the gold lattice vibrations.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180821112018.htm){:target="_blank" rel="noopener"}


