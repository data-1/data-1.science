---
layout: post
title: "Verlinde's new theory of gravity passes first test"
date: 2017-10-08
categories:
author: "Netherlands Research School For Astronomy"
tags: [Dark matter,Erik Verlinde,Galaxy,Physical cosmology,Physical sciences,Astronomy,Science,Physics,Astrophysics,Cosmology]
---


This bending of light allows astronomers to measure the distribution of gravity around galaxies, even up to distances a hundred times larger than the galaxy itself. Brouwer and her team measured the distribution of gravity around more than 33,000 galaxies to put Verlinde's prediction to the test. Verlinde's new theory predicts how much gravity there must be, based only on the mass of the visible matter. She compared this prediction to the distribution of gravity measured by gravitational lensing, in order to test Verlinde's theory. However, the mass of the dark matter is a free parameter, which must be adjusted to the observation.

<hr>

[Visit Link](http://phys.org/news/2016-12-verlinde-theory-gravity.html){:target="_blank" rel="noopener"}


