---
layout: post
title: "You can now explore all 19 of South Africa’s National Parks on Google Maps"
date: 2017-11-16
categories:
author: "Amanda Froelich"
tags: []
---


Have you ever wanted to walk in the footsteps of Nelson Mandela, track cheetahs on foot, or stroll with elephants — and other exotic creatures — in South Africa? SIGN UP  Said Magdalena Filak, Program Manager for Google, “The hundreds of volunteers who helped along the way proved to be truly passionate about showing the best of South Africa through their participation in the loan program.” The Google Street View Camera Loan program encourages anyone to borrow the 360-degree camera technology to help the planet. The on-board technology plots the camera’s exact location on the trail. Related: Thousands of plastic bottles transformed into an inspiring tower of hope in South Africa  In addition to mapping over two hundred points of interest, volunteers mapped eight UNESCO World Heritage Sites. Dennis Wood of Ezemvelo KZN Wildlife said, “As the proud conservation authority for KwaZulu-Natal, Ezemvelo KZN Wildlife are excited to be partnered with Google’ new initiative in exposing our trails on this global platform that we believe will engage our prospective guests to “Take time to Discover” our province’s rich natural beauty and conservation wildlife heritage.”  + Google Street View Loan Program  Images via Google Maps

<hr>

[Visit Link](https://inhabitat.com/you-can-now-explore-all-19-of-south-africas-national-parks-on-google-maps){:target="_blank" rel="noopener"}


