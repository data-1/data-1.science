---
layout: post
title: "Image: CubeSat micro-pulsed plasma thruster"
date: 2018-07-01
categories:
author: "European Space Agency"
tags: [Plasma propulsion engine,CubeSat,Rocket engine,Outer space,Spaceflight,Flight,Technology]
---


Credit: ESA/Mars Space  This micro-pulsed plasma thruster has been designed for propulsion of miniature CubeSats; its first firing is seen here. The thruster works by pulsing a lightning-like electric arc between two electrodes. This vaporizes the thruster propellant into charged plasma, which is then accelerated in the electromagnetic field set up between the electrodes. Developed for ESA by Mars Space Ltd and Clyde Space of the UK with Southampton University, this 2 Watt, 42 Newton-second impulse plasma thruster has been qualified for space, with more than a million firing pulses demonstrated during testing. It has been designed for a range of uses, including drag compensation in low orbits, orbit maintenance, formation flying and small orbit transfers.

<hr>

[Visit Link](https://phys.org/news/2018-06-image-cubesat-micro-pulsed-plasma-thruster.html){:target="_blank" rel="noopener"}


