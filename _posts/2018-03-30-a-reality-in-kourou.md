---
layout: post
title: "a reality in Kourou"
date: 2018-03-30
categories:
author: ""
tags: [Guiana Space Centre,European integration,Rockets and missiles,Rocket families,Space access,Commercial launch service providers,Spacecraft,Outer space,Spaceflight technology,Government research,Astronautics,Commercial spaceflight,Space industry,Aerospace companies,Spaceflight,Space-based economy,Flight,Aerospace companies of France,Unmanned aerial vehicles,Airbus subsidiaries and divisions,Space exploration,Projectile weapons,Policies of the European Union,Spacecraft propulsion,Space research,Aerospace organizations,Airbus joint ventures,Space program of France,Service companies,Scientific exploration,European Space Agency,European space programmes,Pan-European scientific organizations,Space policy of the European Union,Space agencies,Space programs,Space traffic management,Space organizations,Space launch vehicles of Europe,Space policy,Arianespace,Ariane (rocket family),Transport authorities,Space launch vehicles,Space vehicles,Rocketry]
---


At Europe's Spaceport in French Guiana, Ariane 6 is now a reality with the launch zone taking shape. Indeed there is no time to lose for the future European launcher since its first launch is planned for July 2020. But the independent access to space for Europe is at stake along with its leading role on the launcher market. This A&B Roll shows the status of Ariane 6 launch zone in Kourou with latest drone images and an interview with Daniel Neuenschwander, Director of Space Transportation at ESA in English, German and French

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2017/11/Ariane_6_-_a_reality_in_Kourou){:target="_blank" rel="noopener"}


