---
layout: post
title: "Oxytocin and breastfeeding: Elucidation of a molecular mechanism"
date: 2017-10-11
categories:
author: "Kanazawa University"
tags: [Oxytocin,Breastfeeding,Gastrointestinal tract,Clinical medicine,Anatomy,Medical specialties,Biochemistry]
---


On the other hand, the oxytocin level in the blood of babies drinking mother's milk has been found to be elevated, suggesting oxytocin could somehow be transported even in the presence of such a barrier. Thus, the importance of breast feeding is now well recognized; however, information about oxytocin, which is necessary for development of the social brain for communication with others, has been fragmentary. It was thought that the uptake of oxytocin from mother's milk through the digestive tract should take place although the underlying mechanisms remained unknown. If one takes into account the increase in the quantity of blood associated with body weight increase, the increment of oxytocin concentration in the blood is considered to attenuate from postnatal days 5-7. This suggests that on postnatal days 1-3, gut closure is not completed and that oxytocin should be freely permeable from the gut in both the wild type and RAGE knockout mice.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171010114611.htm){:target="_blank" rel="noopener"}


