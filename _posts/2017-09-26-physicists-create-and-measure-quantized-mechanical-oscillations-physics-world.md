---
layout: post
title: "Physicists create and measure quantized mechanical oscillations – Physics World"
date: 2017-09-26
categories:
author: ""
tags: [Photon,Phonon,Quantum mechanics,Resonance,Optical cavity,Laser,Resonator,Physics,Optics,Energy,Quantum state,Theoretical physics,Applied and interdisciplinary physics,Physical sciences,Physical chemistry,Electromagnetism,Science,Atomic molecular and optical physics,Electromagnetic radiation]
---


They performed several operations on one of the quantum states, such as measuring its decay rate by exciting a single phonon mode, waiting a variable amount of time and extracting the energy again to measure the probability that the phonon had been lost. “In principle, we can, for example, put two phonons into our mechanical resonator,” says Chu, “That just involves exciting the qubit, putting in one phonon, exciting the qubit again and putting in another phonon: it’s just a matter of how many quantum operations we can do before the quantum state is lost.” They also hope to demonstrate “Schrödinger’s cat” states, in which the resonator vibrates in two independent ways at once. The other experiment was done by researchers at the University of Vienna in Austria and Delft University of Technology in the Netherlands. Preserved states  By looking at the statistical properties of the photons emitted on resonance with the cavity, the researchers can show that these photons are in the same quantum state as the photons originally used to excite the mechanical resonator. The quantum state of the input photon had therefore been preserved in the single phonon they created and was then transferred back onto the output photon.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/sep/25/physicists-create-and-measure-quantized-mechanical-oscillations){:target="_blank" rel="noopener"}


