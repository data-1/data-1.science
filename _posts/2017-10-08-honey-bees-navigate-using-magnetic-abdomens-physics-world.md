---
layout: post
title: "Honey bees navigate using magnetic abdomens – Physics World"
date: 2017-10-08
categories:
author: "Hamish Johnston"
tags: [Magnetoreception,SQUID,Magnetism,Magnetic field,Magnetization,Physics,Electricity,Electrical engineering,Electromagnetism]
---


Disruptive effect  What Veronika Lambinet, Michael Hayden, Gerhard Gries and colleagues at Simon Fraser University in Vancouver have now done is to show that a ferromagnetic material consistent with magnetite exists in the abdomen of honey bees. They found that the material can be magnetized using a strong permanent magnet and that magnetizing the abdomen of a live honey bee disrupts its ability to navigate using local magnetic fields. The team then used a strong permanent magnet to expose live honey bees to a magnetic field of 2.2 kOe – several thousand times stronger than the Earth’s magnetic field – for about 5 s. Further measurements with the SQUID revealed that pellets made from the abdomen of these bees were more strongly magnetized than pellets made from bees that had not been exposed to a magnetic field. Food source  To see how this magnetization affected the ability of live bees to navigate to a food source, the team first trained a group of bees to locate a sugar reward in an environment where electrical coils create a magnetic field. “Indeed, bees could become the model organism for studying magnetoreception,” Hayden adds.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/mar/27/honey-bees-navigate-using-magnetic-abdomens){:target="_blank" rel="noopener"}


