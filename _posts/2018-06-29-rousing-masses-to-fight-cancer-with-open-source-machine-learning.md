---
layout: post
title: "Rousing masses to fight cancer with open source machine learning"
date: 2018-06-29
categories:
author: "Georgia Institute of Technology"
tags: [Cancer,Prediction,Machine learning,Data,Breast cancer,Technology,Branches of science]
---


It goes out to cancer fighters and tempts them with a new program that predicts cancer drug effectiveness via machine learning and raw genetic data. Not only the labor but also the fruits will remain openly accessible to benefit the treatment of patients as best possible. We don't want to hold the code or data for ourselves or make profits with this, said John McDonald, the director of Georgia Tech's Integrated Cancer Research Center. Nine drugs are in the published study, but we've actually run about 120 drugs through the program all total, said Vannberg, an assistant professor in Georgia Tech's School of Biological Sciences. With our project, we're advertising that sharing should be what everybody does, Vannberg said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/giot-rmt103017.php){:target="_blank" rel="noopener"}


