---
layout: post
title: "Researchers test speed of light with greater precision than before"
date: 2016-06-30
categories:
author: "University Of Western Australia"
tags: [Speed of light,Light,Accuracy and precision,Privacy,Physics,Science]
---


Researchers from The University of Western Australia and Humboldt University of Berlin have completed testing that has effectively measured the spatial consistency of the speed of light with a precision ten times greater than ever before. UWA Researcher Stephen Parker from the Frequency and Quantum Metrology Research Group at the School of Physics said the experiment placed the microwave oscillators perpendicular to each other and rotated them on a turntable once every 100 seconds for a year. The frequency of the microwave signals directly linked to the speed of light, Dr Parker said. If this were to change depending on the direction it was facing it would indicate that Lorentz symmetry had been violated. But the frequencies didn't even change down to the 18th digit (the smallest part of the measurement of frequency), which is remarkable that this symmetry of nature still holds true at such tiny levels.

<hr>

[Visit Link](http://phys.org/news/2015-09-greater-precision.html){:target="_blank" rel="noopener"}


