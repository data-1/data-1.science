---
layout: post
title: "AIM: Asteroid touchdown"
date: 2016-05-31
categories:
author: ""
tags: [Double Asteroid Redirection Test,AIDA (international space cooperation),Bodies of the Solar System,Uncrewed spacecraft,Space vehicles,Planetary science,Space probes,Space exploration,Discovery and exploration of the Solar System,Space missions,Flight,Astronomy,Outer space,Spaceflight,Space science,Spacecraft,Astronautics,Solar System,Spaceflight technology,Planetary defense,Space research,Scientific exploration]
---


The 15 kg Mobile Asteroid Surface Scout-2 (Mascot-2) is building on the heritage of DLR’s Mascot-1 already flying on Japan’s Hayabusa-2. Launched in 2014, the latter will land on asteroid Ryugu in 2018. Light-emitting diodes (LEDs) would help AIM to pinpoint its microlander’s resting place from orbit. As well as a solar array, AIM would also deploy its low frequency radar LFR instrument, while cameras perform visible and thermal surface imaging. AIM and DART together are known as the Asteroid Impact & Deflection Assessment mission.

<hr>

[Visit Link](http://www.esa.int/spaceinvideos/Videos/2016/05/AIM_Asteroid_touchdown){:target="_blank" rel="noopener"}


