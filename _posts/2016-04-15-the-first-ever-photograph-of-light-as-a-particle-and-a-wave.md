---
layout: post
title: "The first ever photograph of light as a particle and a wave"
date: 2016-04-15
categories:
author: Ecole Polytechnique Fédérale de Lausanne
tags: [Light,Electron,Wave,Waveparticle duality,Radiation,Theoretical physics,Physical sciences,Waves,Physics,Atomic molecular and optical physics,Physical chemistry,Quantum mechanics,Chemistry,Physical phenomena,Electromagnetism,Science,Optics,Applied and interdisciplinary physics,Electromagnetic radiation]
---


The researchers have captured, for the first time ever, a single snapshot of light behaving simultaneously as both a wave and a stream of particles particle. The experiment is set up like this: A pulse of laser light is fired at a tiny metallic nanowire. This is where the experiment's trick comes in: The scientists shot a stream of electrons close to the nanowire, using them to image the standing wave of light. While this phenomenon shows the wave-like nature of light, it simultaneously demonstrated its particle aspect as well. As the electrons pass close to the standing wave of light, they hit the light's particles, the photons.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/epfd-tfe030115.php){:target="_blank" rel="noopener"}


