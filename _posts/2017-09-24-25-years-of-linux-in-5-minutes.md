---
layout: post
title: "25 years of Linux in 5 minutes"
date: 2017-09-24
categories:
author: "Opensource.com
(Red Hat)"
tags: [Linux kernel,Linus Torvalds,Red Hat,Git,Computer science,Open content,Free system software,Intellectual works,Computer architecture,Unix,Computers,Computing,Software development,Unix variants,Linux,Free content,Operating system families,System software,Software engineering,Technology,Open-source movement,Free software,Software]
---


Jeremy Garcia of LinuxQuestions.org and Bad Voltage (a podcast) delivers 25 years of Linux in five minutes: starting with Linux's first steps as just a hobby for creator Linus Torvalds, to its staggering popularity today with 135,000 developers from more than 1,300 companies and 22 million lines of code . 1992: First GPL release of Linux; it was initially licensed under a custom license that had some commercial restrictions. 2005: Linux has some licensing issues, so Linus writes Git and moves kernel development to it (very popular today). 2011: 3.0 release of Linux: Linus Torvalds says there's no change. 2016: Over 135,000 developers from more than 1,300 companies have contributed to the Linux kernel since the adoption of Git; 22 million lines of code.

<hr>

[Visit Link](https://opensource.com/article/16/11/25-years-linux-5-minutes){:target="_blank" rel="noopener"}


