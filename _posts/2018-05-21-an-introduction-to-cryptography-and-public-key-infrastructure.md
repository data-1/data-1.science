---
layout: post
title: "An introduction to cryptography and public key infrastructure"
date: 2018-05-21
categories:
author: "Alex Wood
(Red Hat)"
tags: [Encryption,Public-key cryptography,Key (cryptography),Cryptographic hash function,Block cipher mode of operation,Cryptography,Random number generation,Stream cipher,Block cipher,Message authentication code,Entropy (computing),Ciphertext,Military communications,Applied mathematics,Secure communication,Cyberwarfare,Computer security,Secrecy,Security engineering,Information Age,Security technology,Computer science,Espionage techniques,Cybercrime,Technology,Telecommunications,Cyberspace,Crime prevention,Digital rights,Cryptographic algorithms,Telecommunications engineering]
---


We call any cipher that uses the same key to both encrypt and decrypt a symmetric cipher. Symmetric ciphers are divided into stream ciphers and block ciphers. Our XOR cipher is a stream cipher, for example. These ciphers use two keys: a public key and a private key. He decrypts the ciphertext with his private key and uses the symmetric key Alice sent him to communicate with her confidentially using HMACs with each message to ensure integrity.

<hr>

[Visit Link](https://opensource.com/article/18/5/cryptography-pki){:target="_blank" rel="noopener"}


