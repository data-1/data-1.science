---
layout: post
title: "World-first quantum computer simulation of chemical bonds using trapped ions"
date: 2018-07-28
categories:
author: "University of Sydney"
tags: [Quantum computing,Chemistry,Quantum mechanics,Quantum chemistry,Simulation,Quantum information,Technology,Physical sciences,Branches of science,Physics,Science]
---


The research, led by University of Sydney physicist Dr Cornelius Hempel, explores a promising pathway for developing effective ways to model chemical bonds and reactions using quantum computers. They will provide us with a new tool to solve problems in materials science, medicine and industrial chemistry using simulations. Quantum chemistry is the science of understanding the complicated bonds and reactions of molecules using quantum mechanics. By modelling and understanding these processes using quantum computers, scientists expect to unlock lower-energy pathways for chemical reactions, allowing the design of new catalysts. Dr Hempel, who did the experiments while at the University of Innsbruck, now hopes to leverage Sydney's expertise to improve what can be accomplished with these kinds of simulations.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/uos-wqc072318.php){:target="_blank" rel="noopener"}


