---
layout: post
title: "Atomic oxygen detected in Martian atmosphere"
date: 2016-06-27
categories:
author: ""
tags: []
---


Scientists have detected atomic oxygen in the atmosphere of Mars for the first time since the last observation 40 years ago. These atoms were found in the upper layers of the Martian atmosphere known as the mesosphere. An instrument onboard the Stratospheric Observatory for Infrared Astronomy (SOFIA) detected only about half the amount of oxygen expected, which may be due to variations in the Martian atmosphere. Scientists will continue to use SOFIA to study these variations to help better understand the atmosphere of the Red Planet. “Atomic oxygen in the Martian atmosphere is notoriously difficult to measure,” said Pamela Marcum, SOFIA project scientist.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/scientists-detect-atomic-oxygen-in-martian-atmosphere/article8569808.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


