---
layout: post
title: "Both hemispheres of the brain process numbers"
date: 2016-01-29
categories:
author: Friedrich Schiller University Jena
tags: [Brain,Human brain,Cerebral hemisphere,Magnetic resonance imaging,News aggregator,Medical imaging,Neuroscience,Cognition,Cognitive science]
---


Researchers of the Jena University (Germany) and of the Jena University Hospital located an important region for the visual processing of numbers in the human brain and showed that it is active in both hemispheres. Neuroscientists of the Friedrich Schiller University Jena and of the Jena University Hospital discovered that the visual processing of numbers takes place in a so-called 'visual number form area' (NFA) -- in fact in both hemispheres alike. The Jena scientists were the first to publish high resolution magnetic resonance recordings showing the activity in this region of the brain of healthy test persons. In their study Dr. Mareike Grotheer and Prof. Dr. Gyula Kovács from the Institute for Psychology of Jena University as well as Dr. Karl-Heinz Herrmann from the Department of Radiology (IDIR) of the Jena University Hospital presented subjects with numbers, letters and pictures of everyday objects. They recorded three-dimensional images of the brain of the test subjects at an unusually high spatial resolution and hence with only very few artefacts.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2016/01/160127083844.htm){:target="_blank" rel="noopener"}


