---
layout: post
title: "Applying machine learning tools to earthquake data offers new insights: Algorithms pick out hidden signals that could boost geothermal energy production"
date: 2018-07-18
categories:
author: "Lamont-Doherty Earth Observatory, Columbia University"
tags: [Earthquake,Seismology,News aggregator,Machine learning,Branches of science,Technology]
---


In a new study in Science Advances, researchers at Columbia University show that machine learning algorithms could pick out different types of earthquakes from three years of earthquake recordings at The Geysers in California, one of the world's oldest and largest geothermal reservoirs. It's a totally new way of studying earthquakes, said study coauthor Benjamin Holtzman, a geophysicist at Columbia's Lamont-Doherty Earth Observatory. The machine-learning assist helped researchers make the link to the fluctuating amounts of water injected belowground during the energy-extraction process, giving the researchers a possible explanation for why the computer clustered the signals as it did. With sound designer Jason Candler, Holtzman had converted the seismic waves of recordings of notable earthquakes into sounds, and then speeded them up to make them intelligible to the human ear. It was a typical clustering problem, says Paisley.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/05/180523150010.htm){:target="_blank" rel="noopener"}


