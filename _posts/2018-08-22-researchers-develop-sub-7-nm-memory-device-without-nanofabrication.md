---
layout: post
title: "Researchers develop sub-7-nm memory device without nanofabrication"
date: 2018-08-22
categories:
author: "Lisa Zyga"
tags: [Nanolithography,Magnetoresistive RAM,Technology,Applied and interdisciplinary physics,Materials science,Materials]
---


Previous research has already demonstrated several different varieties of single-digit-nanometer structures. The most significant part of this research is that we showed sub-5-nm memory cell with good thermal stability, Hong told Phys.org. We used a self-assembly method to make 5-nm nanomaget grains for information storage without the necessity of nanofabrication. The two magnetization directions correspond to two states (parallel and anti-parallel) of a magnetic tunnel junction, which forms the basic building block of a nonvolatile memory cell. Using a state-of-the-art highly focused spin probe, the researchers demonstrated that an applied current switches the magnetization of individual nanomagnets due to spin transfer torque.

<hr>

[Visit Link](https://phys.org/news/2018-08-sub-nm-memory-device-nanofabrication.html){:target="_blank" rel="noopener"}


