---
layout: post
title: "Raptor tracker"
date: 2015-07-21
categories:
author: University of Alberta 
tags: [Velociraptor,Paleontology]
---


EDMONTON, Canada, June 11 -- In this summer's much anticipated blockbuster Jurassic World, actor Chris Pratt joins forces with a pack of swift and lethal velociraptors. Their big toes each bore an enlarged and wickedly hooked talon, which makes raptors well suited for Hollywood fight scenes.' Persons and University of Alberta alumnus Lida Xing are part of the research team that has just documented a rich fossil footprint site in central China, which contains the tracks of several kinds of dinosaurs, including raptors. To keep it sharp, raptors normally held it in a raised position,' Persons explains. Otherwise, it would have become dulled as it dug into the ground when the dinosaur walked.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/uoa-rt061115.php){:target="_blank" rel="noopener"}


