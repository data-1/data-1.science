---
layout: post
title: "ESA's next science mission to focus on nature of exoplanets"
date: 2018-08-16
categories:
author: ""
tags: [ARIEL,Exoplanet,Planet,Planets,Science,Nature,Astronomical objects,Planetary science,Physical sciences,Outer space,Space science,Astronomy]
---


Science & Exploration ESA's next science mission to focus on nature of exoplanets 20/03/2018 12707 views 122 likes  The nature of planets orbiting stars in other systems will be the focus for ESA’s fourth medium-class science mission, to be launched in mid 2028. Ariel, the Atmospheric Remote‐sensing Infrared Exoplanet Large‐survey mission, was selected by ESA today as part of its Cosmic Vision plan. In particular, there is a gap in our knowledge of how the planet’s chemistry is linked to the environment where it formed, or whether the type of host star drives the physics and chemistry of the planet’s evolution. “Ariel is a logical next step in exoplanet science, allowing us to progress on key science questions regarding their formation and evolution, while also helping us to understand Earth’s place in the Universe,” says Günther Hasinger, ESA Director of Science. As well as detecting signs of well-known ingredients such as water vapour, carbon dioxide and methane, it will also be able to measure more exotic metallic compounds, putting the planet in context of the chemical environment of the host star.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/ESA_s_next_science_mission_to_focus_on_nature_of_exoplanets){:target="_blank" rel="noopener"}


