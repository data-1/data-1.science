---
layout: post
title: "Chemists harness artificial intelligence to predict the future (of chemical reactions)"
date: 2018-07-18
categories:
author: "Princeton University"
tags: [Machine learning,Random forest,Chemist,Yield (chemistry),Prediction,Research,Chemical reaction,Chemical substance,Statistics,Cognitive science,Branches of science,Technology,Science]
---


Another challenge has been calculating quantitative descriptors for each chemical, to use as inputs for the model. These descriptors have typically been calculated one by one, which would have been impractical for the large number of chemical combinations they wanted to use. They then explored multiple common machine learning models and found that one called random forest delivered startlingly accurate yield predictions. Another breakthrough came when the researchers discovered that with random forests, reaction yields can be accurately predicted using the results of 'only' hundreds of reactions (instead of thousands), a number that chemists without robots can perform themselves, Ahneman said. The outcome is the yields of the reactions.

<hr>

[Visit Link](https://phys.org/news/2018-02-chemists-harness-artificial-intelligence-future.html){:target="_blank" rel="noopener"}


