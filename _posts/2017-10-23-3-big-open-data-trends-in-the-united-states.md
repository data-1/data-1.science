---
layout: post
title: "3 big open data trends in the United States"
date: 2017-10-23
categories:
author: "Sam McClenney"
tags: [Open data]
---


Cities are pushing open data not just to be transparent, but also to build a data governance model that can create a network of information that can span both states and regions. At the same time, no two cities need their data to be available in the same way. I expect the trend of data standardization to continue as the number of open cities grow. Practical ramifications of open data sharing  The idea of using open data to share information across departments is one that usually gets a mixed reaction. City leadership, data standards, and data sharing are just some of the trends we are seeing in open data.

<hr>

[Visit Link](https://opensource.com/article/17/5/open-data-trends){:target="_blank" rel="noopener"}


