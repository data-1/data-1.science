---
layout: post
title: "The quantum fridge"
date: 2016-02-02
categories:
author: Vienna University Of Technology
tags: [Gas,Temperature,Energy,Quantum mechanics,Atom,Physical chemistry,Nature,Chemistry,Theoretical physics,Applied and interdisciplinary physics,Physics,Physical sciences]
---


Bernhard Rauer in the lab at TU Wien  When cold milk is poured into a hot cup of tea, a temperature equilibrium is reached very quickly. Kicking out hot particles  The particles that make up a liquid or a gas have different energies, says Professor Jörg Schmiedmayer (TU Wien). Therefore, a simple trick can be used to cool down cold gases: with the help of electromagnetic fields, the particles with the highest energy are removed from the gas. There will never be a thermal distribution of many different energies, says Bernhard Rauer. These quantum waves store the energy of the system, and the more particles that are removed from the gas, the less intense these waves become.

<hr>

[Visit Link](http://phys.org/news/2016-02-quantum-fridge.html){:target="_blank" rel="noopener"}


