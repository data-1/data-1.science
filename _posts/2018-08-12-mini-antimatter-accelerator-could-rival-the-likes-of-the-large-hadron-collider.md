---
layout: post
title: "Mini antimatter accelerator could rival the likes of the Large Hadron Collider"
date: 2018-08-12
categories:
author: "Hayley Dunning, Imperial College London"
tags: [Particle physics,Particle accelerator,SLAC National Accelerator Laboratory,Large Hadron Collider,Physics,Electron,Positron,Higgs boson,Physical sciences,Science,Applied and interdisciplinary physics]
---


The method has been modelled using the properties of existing lasers, with experiments planned soon. Particle accelerators in facilities such as the Large Hadron Collider (LHC) in CERN and the Linac Coherent Light Source (LCLS) at Stanford University in the United States, speed up elementary particles like protons and electrons. Now a researcher at Imperial has invented a method of accelerating the antimatter version of electrons—called positrons—in a system that would be just centimetres long. This centimetre-scale accelerator could use existing lasers to accelerate positron beams with tens of millions of particles to the same energy as reached over two kilometres at the Stanford accelerator. Dr. Sahai added: It is particularly gratifying to do this work at Imperial, where our lab's namesake—Professor Patrick Blackett—won a Nobel Prize for his invention of methods to track exotic particles like antimatter.

<hr>

[Visit Link](https://phys.org/news/2018-08-mini-antimatter-rival-large-hadron.html){:target="_blank" rel="noopener"}


