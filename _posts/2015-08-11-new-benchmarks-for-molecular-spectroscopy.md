---
layout: post
title: "New benchmarks for molecular spectroscopy"
date: 2015-08-11
categories:
author: American Institute Of Physics
tags: [Energy level,Molecule,Vibronic spectroscopy,Spectroscopy,Ion,Atom,Photoelectric effect,Applied and interdisciplinary physics,Chemistry,Physical sciences,Physical chemistry,Atomic molecular and optical physics,Atomic physics,Physics,Molecular physics,Quantum mechanics,Nature,Chemical physics,Theoretical physics,Electromagnetism]
---


Credit: YMO/Tsinghua  Researchers at Tsinghua University in Beijing have recently used a technique called zero-kinetic energy photoelectron spectroscopy to obtain a list in unprecedented detail of the quantum energy levels of the cyanoacetylene cation, a linear, five-atom molecule that exhibits nuclear and electronic coupling effects and is found in interstellar clouds and in the atmosphere of Saturn's largest moon Titan. However, the approximation isn't sufficient to describe the changes in energetic states for a number of photo-induced processes involving cyanoacetylene. It has given the researchers a full, high-resolution readout of the energy levels for cations from their vibrational ground state to excited states several thousand wavenumbers, or magnitudes of frequency, higher, furthering their understanding of the coupled vibrations in the Renner-Teller effect. The researchers found the ions' energy levels to be in good agreement with the theoretical vibronic energy levels of the cyanoacetylene cation they obtained from a diabatic model, which are used to describe the electron and nuclear coupling effects in a quantum system. They also obtained a list of spin-vibronic energy levels for fluoromethane, chloromethane, and monochloroacetylene cations, which are highly symmetric or linear molecules that also have strong electron and nuclear coupling effects.

<hr>

[Visit Link](http://phys.org/news/2015-08-benchmarks-molecular-spectroscopy.html){:target="_blank" rel="noopener"}


