---
layout: post
title: "Quantum effects observed in photosynthesis"
date: 2018-05-26
categories:
author: "University of Groningen"
tags: [Quantum mechanics,Photosynthesis,Molecule,Schrdingers cat,Physical sciences,Applied and interdisciplinary physics,Chemistry,Science,Physics,Theoretical physics,Scientific theories,Physical chemistry]
---


If the cap of the vial is locked with a quantum system, it may simultaneously be open or closed, so the cat is in a mixture of the states dead and alive, until we open the box and observe the system. Vibrations  In earlier research, scientists had already found signals suggesting that light-harvesting molecules in bacteria may be excited into two states simultaneously. In itself this proved the involvement of quantum mechanical effects, however in those experiments, that excited state supposedly lasted more than 1 picosecond (0.000 000 000 001 second). Jansen concludes that biological systems exhibit the same quantum effects as non-biological systems. Moreover, the results may play a role in the development of new systems, such as the storage of solar energy or the development of quantum computers.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/uog-qeo051818.php){:target="_blank" rel="noopener"}


