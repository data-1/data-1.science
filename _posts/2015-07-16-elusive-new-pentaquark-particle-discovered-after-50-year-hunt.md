---
layout: post
title: "Elusive New Pentaquark Particle Discovered After 50-Year Hunt"
date: 2015-07-16
categories:
author: Tia Ghose
tags: [Subatomic particle,Quark,Hadron,Pentaquark,Large Hadron Collider,Matter,Baryon,Lambda baryon,Standard Model,Quantum chromodynamics,Subatomic particles,Theoretical physics,Quantum mechanics,Quantum field theory,Fermions,Physics,Hadrons,Nuclear physics,Particle physics]
---


Scientists at the Large Hadron Collider, the world's largest atom smasher, have found proof of the existence of the pentaquark, an elusive subatomic particle that was first proposed to exist more than 50 years ago. In 1964, physicist Murray Gell-Mann proposed that a group of particles known as baryons, which include protons and neutrons, are actually made up of three even tinier charged subatomic particles known as quarks. In the current study, Wilkinson and his colleagues examined the decay of particles after collisions in the Large Hadron Collider (LHC), a 17-mile-long (27 kilometers) underground ring beneath Geneva, Switzerland. The new evidence for pentaquarks is much more robust than past hints because the LHC experiment uses a detector that identifies all the final states of the particles after a collision, study co-author Sheldon Stone, a physicist at Syracuse University, told Live Science in an email. Based on the LHC data, the team concluded that these intermediate particles were pentaquarks made up of two up quarks, one down quark, one charm quark and one anti-charm quark.

<hr>

[Visit Link](http://www.livescience.com/51557-new-pentaquark-particle-discovered.html){:target="_blank" rel="noopener"}


