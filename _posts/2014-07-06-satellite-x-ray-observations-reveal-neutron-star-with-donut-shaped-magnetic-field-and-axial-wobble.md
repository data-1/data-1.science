---
layout: post
title: "Satellite X-ray observations reveal neutron star with donut-shaped magnetic field and axial wobble"
date: 2014-07-06
categories:
author: European Science Foundation 
tags: []
---


Some neutron stars, known as magnetars, possess powerful magnetic fields, which are stronger than any other known magnetism in the Universe. These intense magnetic fields somehow produce high-energy x-ray pulses, but this process is not well understood. We observed 4U 0142+61 using the Suzaku x-ray astronomy satellite to find out whether the magnetar's emissions change over time. The magnetar had previously been measured to spin at a rate of one revolution in about 8 seconds and to produce x-ray pulses of the same period, but Makishima and his co-workers noticed slow fluctuations in the arrival times of the x-ray pulses. The findings suggest that the magnetar is deformed from a perfect sphere due to an extremely strong, tightly wound toroidal magnetic field buried deep in the star's core.

<hr>

[Visit Link](http://feedproxy.google.com/~r/DiscoveryNews-Top-Stories/~3/WNu_p42p9bU/the-world-in-2025-10-scientific-breakthroughs-140705.htm){:target="_blank" rel="noopener"}


