---
layout: post
title: "Animals and fungi enhance the performance of forests"
date: 2018-08-02
categories:
author: "German Centre for Integrative Biodiversity Research (iDiv) Halle-Jena-Leipzig"
tags: [Biodiversity,Forest,Tree,Biodiversity loss,Pest (organism),American Association for the Advancement of Science,Species,Organisms,Earth phenomena,Ecology,Environmental social science,Physical geography,Environmental conservation,Environmental science,Biogeography,Nature,Natural environment,Biogeochemistry,Systems ecology,Earth sciences]
---


A new study shows that, in addition to the diversity of tree species, the variety of animal and fungus species also has a decisive influence on the performance of forests. A team of researchers led by the German Centre for Integrative Biodiversity Research (iDiv) and the Martin-Luther-University Halle-Wittenberg has published the results in the new issue of Nature Communications. Our analyses show that the diversity of animal and fungal species affects numerous important processes - such as the availability of nutrients for tree growth, said Dr Andreas Schuldt, first author of the study, from the German Centre for Integrative Biodiversity Research (iDiv) and the Martin-Luther-University Halle-Wittenberg. To understand why and how a loss of biodiversity affects these forests, it is not enough to concentrate solely on the trees and their species diversity. Furthermore, besides animals and fungi, the researchers found that the multifunctionality of forest stands is influenced not so much by the number of tree species as by their functional properties and the resulting composition of different types of tree species.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/gcfi-aaf080118.php){:target="_blank" rel="noopener"}


