---
layout: post
title: "Game makers eager to pick your brain with neurogaming"
date: 2014-06-22
categories:
author: fdsafdsdsa 
tags: [NeuroSky,Transcranial direct-current stimulation,Neuroscience,Cognition,Cognitive science]
---


Instead, its players also don a headset that enables them to hurl trucks or other virtual objects simply by thinking. Advocates of so-called neurogaming say the concept in a few years will incorporate a wide array of physiological factors, from a player's heart rate and hand gestures to pupil dilation and emotions. But just developing neurogames that are entertaining would be welcomed by many people, including 32-year-old Adam Garcia of Pittsburg, Calif., who works in construction and enjoys spending a few hours a week playing video games. NEUROGAMING  -What is it? That includes using transcranial direct current stimulation, which administers mild electrical currents to the person's brain.

<hr>

[Visit Link](http://phys.org/news322475185.html){:target="_blank" rel="noopener"}


