---
layout: post
title: "Training Schrodinger's cat: Controlling the quantum properties of light"
date: 2015-07-12
categories:
author: Stuart Mason Dambrot
tags: [Quantum mechanics,Cat state,Schrdingers cat,Quantum Zeno effect,Quantum error correction,Quantum information,Photoelectric effect,Schrdinger equation,Photon,Quantum decoherence,Circuit quantum electrodynamics,Physics,Theoretical physics,Applied and interdisciplinary physics,Scientific theories,Science,Applied mathematics,Physical sciences]
---


The cat comes from the similarity of such a state with a Schrödinger cat state of light: a superposition between two classical states of light. Recently, scientists at Ecole Normale Supérieure in Paris demonstrated a novel method for controlling the quantum properties of light by probing a superconducting circuit in a cavity with microwave photons to control the energy levels that photon quanta can occupy. As a result, this new technique could apply to the development of quantum computers by protecting qubits from decoherence as well as enhancing quantum error correction and quantum systems measurement. The primary difficulty in developing our method for manipulating electromagnetic modes by effectively controlling their phase space was to find a proper way of preventing any access to one or few energy levels, Huard tells Phys.org. Our method is a new technique that can produce exotic quantum states of light similar to Schrödinger cat states or vacuum squeezed states that are well-suited for quantum information or metrology purposes by increasing the precision of field or position measurements.

<hr>

[Visit Link](http://phys.org/news355553075.html){:target="_blank" rel="noopener"}


