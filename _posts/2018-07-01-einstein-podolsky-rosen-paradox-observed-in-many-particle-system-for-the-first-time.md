---
layout: post
title: "Einstein-Podolsky-Rosen paradox observed in many-particle system for the first time"
date: 2018-07-01
categories:
author: "University of Basel"
tags: [EPR paradox,Quantum mechanics,Physics,Theoretical physics,Science,Applied and interdisciplinary physics,Physical sciences,Scientific theories,Scientific method]
---


How precisely can we predict the results of measurements on a physical system? The paradox is that an observer can use measurements on system A to make more precise statements about system B than an observer who has direct access to system B (but not to A). Thanks to high-resolution imaging, they were able to measure the spin correlations between the separate regions directly and, at the same time, to localize the atoms in precisely defined positions. The results of the measurements in the two regions were so strongly correlated that they allowed us to demonstrate the EPR paradox, says PhD student Matteo Fadel, lead author of the study. It's fascinating to observe such a fundamental phenomenon of quantum physics in ever larger systems.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-04/uob-epo042618.php){:target="_blank" rel="noopener"}


