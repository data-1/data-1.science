---
layout: post
title: "Mammals defend against viruses differently than invertebrates"
date: 2014-06-24
categories:
author: The Mount Sinai Hospital
tags: [Virus,RNA interference,Interferon,Clinical medicine,Biology,Life sciences,Biotechnology,Medical specialties]
---


Two back-to-back studies in the journal Science last year said the answer is yes, but a study just published in Cell Reports by researchers at the Icahn School of Medicine at Mount Sinai found the opposite. In the Mount Sinai study, the results found that the defense system used by invertebrates—RNA interferences or RNAi—is not used by mammals as some had argued. Drug designers interested in using RNAi to treat disease have worried that if RNAi is part of the mammalian response to viral infections, RNAi-based agents could compromise a human's immune response, producing unintended consequences. The finding that mammals do not use RNAi to fight viruses suggests that RNAi-based drugs could augment the existing interferon response in mammals, Dr. tenOever says. If mammals used interferon and RNAi to fight the virus, we would have seen the RNAi-blocking virus flourish in at least this setting—but we did not, Dr. tenOever says.

<hr>

[Visit Link](http://phys.org/news322766843.html){:target="_blank" rel="noopener"}


