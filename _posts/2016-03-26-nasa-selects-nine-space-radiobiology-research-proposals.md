---
layout: post
title: "NASA selects nine space radiobiology research proposals"
date: 2016-03-26
categories:
author:  
tags: [Health threat from cosmic rays,NASA,Outer space,Spaceflight,Space science,Astronautics,Science,Flight,Astronomy]
---


NASA’s space radiobiology research aims to mitigate the harmful effects of the space radiation environment on astronaut health outside of the relative protection of the Van Allen belts (pictured in blue). Credit: NASA/SOHO  NASA's Human Research Program will fund nine proposals for ground-based research that will help enable extended and safer human exploration of space by quantifying and, ultimately, reducing the risks posed by space radiation. Using beams of high-energy, heavy ions that simulate space radiation, the research will employ new experimental approaches to understand better the role of space radiation in the development of cancer, heart and circulatory disease, and long-term cognitive function problems. These studies will be conducted at NASA's Space Radiation Laboratory at the Brookhaven National Laboratory in Upton, New York. The proposals were in response to the research announcement, Ground-Based Studies in Space Radiobiology.

<hr>

[Visit Link](http://phys.org/news331881976.html){:target="_blank" rel="noopener"}


