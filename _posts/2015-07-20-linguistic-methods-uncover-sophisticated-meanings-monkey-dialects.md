---
layout: post
title: "Linguistic methods uncover sophisticated meanings, monkey dialects"
date: 2015-07-20
categories:
author: New York University 
tags: [Alarm signal,Linguistics,Cognitive science,Science,Cognition,Language,Branches of science,Human communication,Culture,Interdisciplinary subfields]
---


The same species of monkeys located in separate geographic regions use their alarm calls differently to warn of approaching predators, a linguistic analysis by a team of scientists reveals. The combined team of linguists and primatologists analyzed alarm calls of Campbell's monkeys on two sites: the Tai forest in Ivory Coast and Tiwai Island in Sierra Leone. Notably, monkey predators on the two sites differ: the primates are threatened by eagles on Tiwai Island and by eagles and leopards in the Tai Forest. For instance, krak usually functions as a leopard alarm call in Tai, but as a general alarm call - to warn of all sorts of disturbances, including eagles - on Tiwai. The authors propose that krak always has a meaning of general alarm, but that in Tai it comes to be enriched by competition with hok (meaning: aerial threat) and krak-oo (meaning: weak threat) - with the result that it is enriched with a 'not hok ' component (hence: the threat is a non-aerial threat) and a 'not krak-oo ' component (hence: the threat is not weak).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-12/nyu-lmu121514.php){:target="_blank" rel="noopener"}


