---
layout: post
title: "USC scientists create new battery that's cheap, clean, rechargeable... and organic"
date: 2014-06-29
categories:
author: University of Southern California 
tags: [Electric battery,Energy storage,Redox,Electrochemistry,Rechargeable battery,Wind power,Energy,Sustainable technologies,Physical quantities,Electricity,Physical sciences,Technology,Energy technology,Nature,Chemistry]
---


Narayan collaborated with Surya Prakash, Prakash, professor of chemistry and director of the USC Loker Hydrocarbon Research Institute, as well as USC's Bo Yang, Lena Hoober-Burkhardt, and Fang Wang. 'Mega-scale' energy storage is a critical problem in the future of the renewable energy, requiring inexpensive and eco-friendly solutions, Narayan said. The tanks of electroactive materials can be made as large as needed – increasing total amount of energy the system can store – or the central cell can be tweaked to release that energy faster or slower, altering the amount of power (energy released over time) that the system can generate. Through a combination of molecule design and trial-and-error, they found that certain naturally occurring quinones – oxidized organic compounds – fit the bill. Currently, the quinones needed for the batteries are manufactured from naturally occurring hydrocarbons.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/uosc-usc062514.php){:target="_blank" rel="noopener"}


