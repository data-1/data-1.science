---
layout: post
title: "Evolution of marine crocodilians constrained by ocean temperatures"
date: 2015-07-17
categories:
author: University of Bristol 
tags: [Crocodilia,Fossil,Cretaceous,Extinction,Ocean,CretaceousPaleogene extinction event,Nature,Earth sciences]
---


The ancestors of today's crocodiles colonised the seas during warm phases and became extinct during cold phases, according to a new Anglo-French study which establishes a link between marine crocodilian diversity and the evolution of sea temperature over a period of more than 140 million years. On four occasions in the past 200 million years, major crocodile groups entered the seas, and then became extinct. Dr Martin, with a team of paleontologists and geochemists from the Université de Lyon and the University of Bristol, compared the evolution of the number of marine crocodilian fossil species to the sea temperature curve during the past 200 million years. The seawater temperatures derived from the composition of fish skeleton thus corresponds to the temperature of water in which the marine crocodiles also lived. Then, another crocodilian lineage appeared and colonised the marine environment during another period of global warming.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/uob-eom081914.php){:target="_blank" rel="noopener"}


