---
layout: post
title: "Artificial intelligence helps researchers predict drug combinations' side effects -- ScienceDaily"
date: 2018-07-13
categories:
author: "Stanford University"
tags: [Health care,Health,Health sciences,Branches of science,Medicine]
---


And if that isn't surprising enough, try this one: in many cases, doctors have no idea what side effects might arise from adding another drug to a patient's personal pharmacy. But computer science may be able to help. Zitnik and colleagues Monica Agrawal, a master's student, and Jure Leskovec, an associate professor of computer science, lay out an artificial intelligence system for predicting, not simply tracking, potential side effects from drug combinations. There are about 1000 different known side effects and 5,000 drugs on the market, making for nearly 125 billion possible side effects between all possible pairs of drugs. They also hope to create a more user-friendly tool to give doctors guidance on whether it's a good idea to prescribe a particular drug to a particular patient and to help researchers developing drug regimens for complex diseases with fewer side effects.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180710072013.htm){:target="_blank" rel="noopener"}


