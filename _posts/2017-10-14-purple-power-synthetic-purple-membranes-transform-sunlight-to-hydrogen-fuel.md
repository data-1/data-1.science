---
layout: post
title: "Purple power: Synthetic 'purple membranes' transform sunlight to hydrogen fuel"
date: 2017-10-14
categories:
author: "Argonne National Laboratory"
tags: [Synthetic biology,Protein,Biology,Cell membrane,Ultraviolet,Bacteriorhodopsin,Solar fuel,Energy,Cell (biology),Nanotechnology,Nanoparticle,Chemistry,Nature,Applied and interdisciplinary physics,Materials,Technology,Physical sciences]
---


Researchers at the U.S. Department of Energy's Argonne National Laboratory have found a new way to produce solar fuels by developing completely synthetic bionano machinery to harvest light without the need for a living cell. Central to the artificial template created by the Argonne researchers is synthetically produced bacteriorhodopsin. In bacteria, the protein uses energy from visible light to pump protons across the cell membrane, creating an electrical gradient the organism uses to generate and store chemical energy. advertisement  This synthetic system gives us the ability to reconfigure an ancient biological process for a new and useful application for energy, Rozhkova added. In a natural purple membrane, bacteria use bacteriorhodopsin to harvest energy from light.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171013091955.htm){:target="_blank" rel="noopener"}


