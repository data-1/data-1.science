---
layout: post
title: "Scientists part the clouds on how droplets form: Researchers find new mechanism to explain the birth of cloud droplets, could influence climate models"
date: 2016-03-25
categories:
author: DOE/Lawrence Berkeley National Laboratory
tags: [Particulates,Aerosol,Cloud,Water,Physical geography,Applied and interdisciplinary physics,Nature,Physical chemistry,Chemistry,Physical sciences,Earth sciences]
---


Conventional wisdom says that the water solubility of the aerosol is the key factor in the formation of cloud droplets, said study senior author Kevin Wilson, the deputy director of science at Berkeley Lab's Chemical Sciences Division. Reflecting more light has the effect of cooling Earth's surface. Certain inorganic particles, like sea salt, dissolve easily in water, but the atmosphere is typically a complex mixture of organic and inorganic aerosols. They measured the size of the droplets formed when the particles were exposed to water vapor under typical cloud-forming conditions. By factoring in the effects of surface tension depression, the researchers were able to correctly predict the size of the droplets formed.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/03/160324145422.htm){:target="_blank" rel="noopener"}


