---
layout: post
title: "Hunt for dark matter takes physicists deep below earth's surface, where WIMPS can't hide"
date: 2015-09-13
categories:
author: Margaret Allen, Southern Methodist University
tags: [Cryogenic Dark Matter Search,Weakly interacting massive particles,Dark matter,Nature,Science,Physics,Physical sciences,Particle physics,Dark concepts in astrophysics,Astrophysics,Astroparticle physics]
---


SuperCDMS has unprecedented sensitivity to the light mass, sometimes called low mass, WIMPS. CDMS and SuperCDMS Soudan also focus on lower mass dark matter. Deep below the ground, to block out distractions  SuperCDMS SNOLAB will be constructed 6,800 feet underground—much deeper than CDMS or SuperCDMS Soudan, which are 2,341 feet below the earth in an abandoned underground iron mine near Soudan, Minn. The shielding's purpose is to shield the detectors from background particles—the interaction signatures of neutrons—that can mimic the behavior of WIMPS. SMU's dark matter research is funded through a $1 million Early Career Development Award that Cooley was awarded in 2012 from the National Science Foundation.

<hr>

[Visit Link](http://phys.org/news326612704.html){:target="_blank" rel="noopener"}


