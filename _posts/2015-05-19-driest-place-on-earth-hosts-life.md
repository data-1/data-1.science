---
layout: post
title: "Driest place on Earth hosts life"
date: 2015-05-19
categories:
author: Andrew Williams, Astrobiology Magazine
tags: [Astrobiology,Atacama Desert,Mars,Mars Science Laboratory,Life on Mars,Space science,Science,Planetary science,Astronomy,Outer space,Physical sciences]
---


María Elena South: Mars on Earth in the Atacama Desert, Chile. The results are presented in the paper, Discovery and microbial content of the driest site of the hyperarid Atacama Desert, Chile, published in March in the journal Environmental Microbiology Reports. Implications for Astrobiology  For Azua-Bustos, the fact that the conditions at MES site, in terms of dryness, are the closest to Mars as it is possible to get means that it is one of the best analogue models on Earth to understand and investigate the potential existence, and type of, microbial life in the Martian subsurface. For Azua-Bustos, the fact that we already know that there is life in the soil at María Elena South means that it would also be interesting to test if the sample analysis at Mars (SAM) instrument (a suite of three instruments, including a mass spectrometer, gas chromatograph, and tuneable laser spectrometer carried onboard the MSL rover), as well as similar detection instruments scheduled to be sent to Mars, are also able to detect life at a similarly dry terrestrial site. Mars-like soils in the Atacama Desert, Chile, and the dry limit of microbial life.. 2003 Nov 7;302(5647):1018-21. www.ncbi.nlm.nih.gov/pubmed/14605363  Discovery and microbial content of the driest site of the hyperarid Atacama Desert, Chile.

<hr>

[Visit Link](http://phys.org/news351240437.html){:target="_blank" rel="noopener"}


