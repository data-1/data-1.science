---
layout: post
title: "Gradual changes in the environment delay evolutionary adaptations"
date: 2016-01-29
categories:
author: Wageningen University
tags: [Evolution,Saccharomyces cerevisiae,Yeast,Organism,Adaptation,Natural environment,Nature]
---


More gradual environmental change slows down evolution, but does not lead to a higher degree of adaptation of organisms to their environment, conclude Wageningen University scientists in their study published in The American Naturalist. One major concern associated with these issues is the impact they may have on the Earth's biodiversity: are organisms able to adapt, or will they become extinct? If we manage to slow down pollution and climate change, will this be helpful to the organisms that need to adapt? Baker's yeast  New research at Wageningen University using laboratory populations of the baker's yeast Saccharomyces cerevisiae shows that if the environment changes more gradually, evolutionary adaptation is delayed. However, for the final level of adaptation the rate of change does not matter: in a long-term laboratory evolution experiment, yeast populations that adapted to gradual versus abrupt increases in metal concentration were found to do equally well.

<hr>

[Visit Link](http://phys.org/news/2016-01-gradual-environment-evolutionary.html){:target="_blank" rel="noopener"}


