---
layout: post
title: "Study: Farmers and scientists divided over climate change"
date: 2016-04-11
categories:
author: Purdue University
tags: [Climate change,Agriculture,Climate,Human impact on the environment,Climatology,Greenhouse gas,Attribution of recent climate change,Weather,Climate variability and change,Environmental impact,Physical geography,Global environmental issues,Nature,Earth sciences,Natural environment]
---


More than 90 percent of the scientists and climatologists surveyed said they believed climate change was occurring, with more than 50 percent attributing climate change primarily to human activities. A quarter of producers said they believed climate change was caused mostly by natural shifts in the environment, and 31 percent said there was not enough evidence to determine whether climate change was happening or not. Our research suggests that this disparity in beliefs may cause agricultural stakeholders to respond to climate information very differently. Warmer temperatures could extend the growing season in northern latitudes, and an increase in atmospheric carbon dioxide could improve the water use efficiency of some crops. But increases in weather variability and extreme weather events could lower crop yields.

<hr>

[Visit Link](http://phys.org/news334933314.html){:target="_blank" rel="noopener"}


