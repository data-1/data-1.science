---
layout: post
title: "Clinical trials in a dish: A perspective on the coming revolution in drug development"
date: 2018-08-06
categories:
author: "SLAS (Society for Laboratory Automation and Screening)"
tags: [Drug development,Clinical trial,Health sciences,Life sciences,Medicine,Technology,Medical research]
---


A new SLAS Discovery article available now for free ahead-of-print, offers perspective from researchers at Coyne Scientific (Atlanta, GA) about Clinical Trials in a Dish (CTiD), a novel strategy that bridges preclinical testing and clinical trials. This new CTiD platform allows pharmaceutical companies to test, at the population level, novel drugs on patient cells before moving into actual clinical trials. Although still requiring improvements and enhancements, CTiD offers to refine the selection of drugs to move into clinical development, leading to reduced attrition and enabling safe drugs that address unmet medical needs to reach patients more quickly. About our Society and Journals  SLAS (Society for Laboratory Automation and Screening) is an international community of nearly 20,000 professionals and students dedicated to life sciences discovery and technology. SLAS Technology (Translating Life Sciences Innovation) was previously published (1996-2016) as the Journal of Laboratory Automation (JALA).

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/sfl-cti052618.php){:target="_blank" rel="noopener"}


