---
layout: post
title: "Does the European public understand the impacts of climate change on the ocean?"
date: 2018-08-18
categories:
author: "Frontiers"
tags: [Climate change,Effects of climate change,Ocean,Sea,Sea level rise,Environmental impact,Change,Earth phenomena,Oceanography,Environmental science,Applied and interdisciplinary physics,Climate variability and change,Environment,Climate,Global natural environment,Nature,Societal collapse,Environmental issues,Environmental issues with fossil fuels,Global environmental issues,Physical geography,Earth sciences,Natural environment,Human impact on the environment]
---


Opinion poll of European citizens reveals that most are relatively well informed but many believe scenarios that may happen by 2100 have already occurred, calling for an improvement in the way climate science is communicated to the public  The oceans are our lifeline and the lungs of our planet producing 70% of the oxygen we breathe. However, the ocean - and therefore the future of humanity - is threatened by climate change. An opinion poll of 10,000 citizens published in Frontiers in Marine Science is the first in-depth study looking at public engagement with marine climate change issues across 10 European countries. Awareness and levels of concern are generally higher for people living closest to the sea, women, Southern Europeans and increases with age. The survey calls for a major shift in the way we communicate climate change with far less emphasis on what changes may occur in the future, as these can be misinterpreted by the public, and we need a greater involvement of university scientists.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-07/f-dte070317.php){:target="_blank" rel="noopener"}


