---
layout: post
title: "Could gravitational waves reveal how fast our universe is expanding? Signals from rare black hole-neutron star pairs could pinpoint rate at which universe is growing, researchers say"
date: 2018-07-13
categories:
author: "Massachusetts Institute of Technology"
tags: [Gravitational wave,Neutron star,Star,LIGO,Hubbles law,Universe,Binary star,Black hole,Expansion of the universe,Gravitational-wave observatory,Hubble Space Telescope,Celestial mechanics,Astronomy,Space science,Physical sciences,Physics,Astrophysics,Physical cosmology,Science,Astronomical objects,Cosmology,Nature]
---


Now scientists from MIT and Harvard University have proposed a more accurate and independent way to measure the Hubble constant, using gravitational waves emitted by a relatively rare system: a black hole-neutron star binary, a hugely energetic pairing of a spiraling black hole and a neutron star. A new wave  In 2014, before LIGO made the first detection of gravitational waves, Vitale and his colleagues observed that a binary system composed of a black hole and a neutron star could give a more accurate distance measurement, compared with neutron star binaries. The researchers simulated a variety of systems with black holes, including black hole-neutron star binaries and neutron star binaries. Vitale says. So far, people have focused on binary neutron stars as a way of measuring the Hubble constant with gravitational waves, Vitale says.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180712114434.htm){:target="_blank" rel="noopener"}


