---
layout: post
title: "ESA's Jupiter mission moves off the drawing board"
date: 2017-10-08
categories:
author: "European Space Agency"
tags: [Jupiter Icy Moons Explorer,Ganymede (moon),Jupiter,Gravity assist,Europa (moon),Astronautics,Astronomical objects,Astronomical objects known since antiquity,Planets of the Solar System,Flight,Planets,Sky,Space science,Astronomy,Planetary science,Solar System,Spaceflight,Outer space,Bodies of the Solar System]
---


Europa is almost the same size as Earth's moon, while Ganymede, the largest moon in the Solar System, is larger than planet Mercury. Credit: NASA/JPL/DLR  Demanding electric, magnetic and power requirements, harsh radiation, and strict planetary protection rules are some of the critical issues that had to be tackled in order to move ESA's Jupiter Icy Moons Explorer – Juice – from the drawing board and into construction. The animation ends at the Jupiter orbit insertion point, but the planned 3.5 year mission will see Juice not only orbit Jupiter, but also make dedicated flybys of the moons Europa, Callisto and Ganymede, before orbiting the largest moon, Ganymede. Credit: ESA  After launch, Juice will make five gravity-assist flybys in total: one each at Mars and Venus, and three at Earth, to set it on course for Jupiter. The spacecraft's main engine will be used to enter orbit around the giant planet, and later around Jupiter's largest moon, Ganymede.

<hr>

[Visit Link](https://phys.org/news/2017-03-esa-jupiter-mission-board.html){:target="_blank" rel="noopener"}


