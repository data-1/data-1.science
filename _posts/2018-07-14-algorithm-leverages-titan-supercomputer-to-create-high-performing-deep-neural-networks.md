---
layout: post
title: "Algorithm leverages Titan supercomputer to create high-performing deep neural networks"
date: 2018-07-14
categories:
author: "Oak Ridge National Laboratory"
tags: [Deep learning,Hyperparameter optimization,Artificial neural network,Graphics processing unit,Cognitive science,Branches of science,Technology,Computer science,Science,Computing]
---


The research team's algorithm, called MENNDL (Multinode Evolutionary Neural Networks for Deep Learning), is designed to evaluate, evolve, and optimize neural networks for unique datasets. Designing the algorithm to really work at that scale was one of the challenges, Young said. The results matched or exceeded the performance of networks designed by experts. Having recently been awarded another allocation under the Advanced Scientific Computing Research Leadership Computing Challenge program, Perdue's team is building off its deep learning success by applying MENDDL to additional high-energy physics datasets to generate optimized algorithms. I think we'll learn really interesting things about how deep learning works, and we'll also have better networks to do our physics.

<hr>

[Visit Link](https://phys.org/news/2017-11-algorithm-leverages-titan-supercomputer-high-performing.html){:target="_blank" rel="noopener"}


