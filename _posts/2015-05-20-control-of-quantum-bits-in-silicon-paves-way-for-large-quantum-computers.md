---
layout: post
title: "Control of quantum bits in silicon paves way for large quantum computers"
date: 2015-05-20
categories:
author: ""    
tags: [Quantum computing,Qubit,Quantum information,Atom,Electron,Silicon,Computer,Bit,Quantum,Spin (physics),Technology,Electronics,Electromagnetism,Physics,Electricity,Electrical engineering]
---


Lead researcher, UNSW Associate Professor Andrea Morello from the School of Electrical Engineering and Telecommunications, said his team had successfully realised a new control method for future quantum computers. The team has already improved the control of these qubits to an accuracy of above 99% and established the world record for how long quantum information can be stored in the solid state, as published in Nature Nanotechnology in 2014. We demonstrated that a highly coherent qubit, like the spin of a single phosphorus atom in isotopically enriched silicon, can be controlled using electric fields, instead of using pulses of oscillating magnetic fields, explained UNSW's Dr Arne Laucht, post-doctoral researcher and lead author of the study. Associate Professor Morello said the method works by distorting the shape of the electron cloud attached to the atom, using a very localized electric field. Key to the success of this electrical control method is the placement of the qubits inside a thin layer of specially purified silicon, containing only the silicon-28 isotope.

<hr>

[Visit Link](http://www.spacedaily.com/reports/Electrical_control_of_quantum_bits_in_silicon_paves_the_way_to_large_quantum_computers_999.html){:target="_blank" rel="noopener"}


