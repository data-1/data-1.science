---
layout: post
title: "Japanese firm to mature whisky in space"
date: 2016-05-16
categories:
author:  
tags: [Whisky,Suntory,Single malt whisky]
---


Japanese whisky will be sent into space next month to test how time in a zero-gravity environment affects its flavour, one of the country's biggest drinks makers said Friday. Samples of whisky produced by Suntory will be stored in the Japanese laboratory facility of the International Space Station for at least a year, with some flasks staying longer. Researchers for the company believe that storing the beverage in an environment with only slight temperature changes and limited liquid movement could lead to a mellower flavour. Suntory will send whisky aged for 10, 18 and 21 years as well as a number of other alcoholic substances. Once they are returned to Earth, blenders will assess their flavours while researchers subject the liquids to scientific analysis, the company said.

<hr>

[Visit Link](http://phys.org/news/2015-07-japanese-firm-mature-whisky-space.html){:target="_blank" rel="noopener"}


