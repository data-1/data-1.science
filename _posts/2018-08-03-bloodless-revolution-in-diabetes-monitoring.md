---
layout: post
title: "Bloodless revolution in diabetes monitoring"
date: 2018-08-03
categories:
author: "University of Bath"
tags: [Blood sugar level,University of Bath,Diabetes,Research,Sensor,Health,Teaching Excellence Framework,Health sciences,Technology,Medicine]
---


Scientists have created a non-invasive, adhesive patch, which promises the measurement of glucose levels through the skin without a finger-prick blood test  Scientists have created a non-invasive, adhesive patch, which promises the measurement of glucose levels through the skin without a finger-prick blood test, potentially removing the need for millions of diabetics to frequently carry out the painful and unpopular tests. Crucially, because of the design of the array of sensors and reservoirs, the patch does not require calibration with a blood sample -- meaning that finger prick blood tests are unnecessary. An effective, non-invasive way of monitoring blood glucose could both help diabetics, as well as those at risk of developing diabetes, make the right choices to either manage the disease well or reduce their risk of developing the condition. Find out more: http://www.bath.ac.uk/collections/animal-research/  University of Bath  The University of Bath is one of the UK's leading universities both in terms of research and our reputation for excellence in teaching, learning and graduate prospects. In the Research Excellence Framework (REF) 2014 research assessment 87 per cent of our research was defined as 'world-leading' or 'internationally excellent'.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-04/uob-bri040618.php){:target="_blank" rel="noopener"}


