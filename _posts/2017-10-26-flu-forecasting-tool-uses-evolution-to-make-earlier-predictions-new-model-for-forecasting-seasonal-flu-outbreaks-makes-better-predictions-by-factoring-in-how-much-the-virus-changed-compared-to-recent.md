---
layout: post
title: "Flu forecasting tool uses evolution to make earlier predictions: New model for forecasting seasonal flu outbreaks makes better predictions by factoring in how much the virus changed compared to recent"
date: 2017-10-26
categories:
author: University of Chicago Medical Center
tags: [Influenza A virus subtype H3N2,Influenza,Health,Medical specialties,Health sciences,Epidemiology]
---


Using historical data as a test, the new model accurately predicted the total number of cases for each season in the U.S. from 2002 to 2016, and produced an accurate, real-time prediction for the 2016-17 season before it started last year. You could imagine using our model to make an early prediction about overall severity of the season, and then use other methods to forecast the timing of the outbreak once it begins. Each year, four influenza strains circulate in the human population: H3N2, H1N1, and two B variants. But most flu forecasting models don't factor in this change. Adding this crucial piece of information to the new model generates an early estimate of the overall severity of the coming flu season, because they can make a projection as soon as current year's variation of the virus starts to emerge in the spring and summer.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171025140509.htm){:target="_blank" rel="noopener"}


