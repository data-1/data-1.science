---
layout: post
title: "Neanderthals at El Sidrón ate a diet of wild mushrooms, pine nuts and moss"
date: 2017-03-17
categories:
author: "Spanish National Research Council (CSIC)"
tags: [Sidrn Cave,Neanderthal]
---


The studies of Neanderthal fossil remains found at dig sites across Europe continue to provide information about their lifestyles. The latest study, published in Nature magazine, and in which scientific investigators from the Spanish National Research Council (CSIC) collaborated, provides information about the diet of the Neanderthals who inhabited the El Sidrón site in Asturias, northern Spain. Analysis of genetic material preserved in the calcified dental plaque from these Neanderthals shows that their diet included wild mushrooms, pine nuts and moss, yet no evidence has been found to show they ate meat. Both health issues must have caused him intense pain, Rosas points out. Discovered in 1994, around 2,500 skeletal remains from at least 13 individuals of both sexes and of varying ages who lived there around 49,000 years ago have been recovered.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/snrc-nae030617.php){:target="_blank" rel="noopener"}


