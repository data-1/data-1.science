---
layout: post
title: "Monitoring Linux Systems for Rootkits"
date: 2015-09-30
categories:
author: Michael Boelen
tags: [Loadable kernel module,Linux,Kernel (operating system),Lynis,Device driver,Intrusion detection system,Information Age,Systems engineering,Information technology management,Computer architecture,Technology,Computer engineering,Software development,System software,Software engineering,Computer science,Software,Computing,Digital media,Cyberwarfare,Computer security,Operating system families,Information technology,Operating system technology,Computers]
---


System Protection  Kernel  The kernel is the brain of the software system and decides what should be executed by the central processing unit. Intrusion Detection  While prevention is a good thing, detection is usually even more valuable. Disable kernel modules  Objective: Disable loading of kernel modules, if system does not require it. Additionally, disable loading all kernel modules for full protection. So we should disable this driver from /etc/modules:  After adding a hash to the line starting with “lp”, we reboot the system another time.

<hr>

[Visit Link](http://linux-audit.com/monitoring-linux-systems-for-rootkits/){:target="_blank" rel="noopener"}


