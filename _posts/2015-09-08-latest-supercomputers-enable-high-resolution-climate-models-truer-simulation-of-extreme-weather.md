---
layout: post
title: "Latest supercomputers enable high-resolution climate models, truer simulation of extreme weather"
date: 2015-09-08
categories:
author: "$author" 
tags: [Office of Science,Lawrence Berkeley National Laboratory,Simulation,Laboratory,Science,Numerical weather prediction,Climate,Tropical cyclone,National Energy Research Scientific Computing Center,Precipitation,Climate change,General circulation model,Intergovernmental Panel on Climate Change,American Association for the Advancement of Science,Earth sciences,Nature,Physical geography]
---


What he found was that not only were the simulations much closer to actual observations, but the high-resolution models were far better at reproducing intense storms, such as hurricanes and cyclones. Using version 5.1 of the Community Atmospheric Model, developed by the Department of Energy (DOE) and the National Science Foundation (NSF) for use by the scientific community, Wehner and his co-authors conducted an analysis for the period 1979 to 2005 at three spatial resolutions: 25 km, 100 km, and 200 km. The computing was performed at Berkeley Lab's National Energy Research Scientific Computing Center (NERSC), a DOE Office of Science User Facility. Wehner says the high-resolution models will help scientists to better understand how climate change will affect extreme storms. Further down the line, Wehner says scientists will be running climate models with 1 km resolution.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-11/dbnl-lse111214.php){:target="_blank" rel="noopener"}


