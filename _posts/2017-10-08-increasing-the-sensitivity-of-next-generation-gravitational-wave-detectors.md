---
layout: post
title: "Increasing the sensitivity of next-generation gravitational wave detectors"
date: 2017-10-08
categories:
author: "Optica"
tags: [LIGO,Optics,Gravitational wave,Gravitational-wave observatory,Laser,Gravitational-wave astronomy,Natural philosophy,Physical sciences,Electrodynamics,Physical phenomena,Electrical engineering,Waves,Electromagnetism,Atomic molecular and optical physics,Science,Physics,Electromagnetic radiation]
---


With the multiphotodetector arrays in the 10-meter prototype interferometer, the researchers demonstrated a laser power stability that was a factor of five better than what has been achieved by other groups. Laser beam shaping to reduce noise  Willke's research group also demonstrated that with a few minor modifications, gravitational wave detectors could be made more sensitive by using a laser in what is called a Laguerre-Gauss mode. Researchers have proposed using Laguerre-Gauss mode lasers in design concepts for a third-generation gravitational wave detector, known as the Einstein Telescope, which is expected to be 10 times more sensitive than today's instruments. The researchers found that Laguerre-Gauss lasers are not compatible with devices known as pre-mode cleaners that Advanced LIGO uses to improve the stability of the laser beam. Optics Letters covers the latest research in optical science, including optical measurements, optical components and devices, atmospheric optics, biomedical optics, Fourier optics, integrated optics, optical processing, optoelectronics, lasers, nonlinear optics, optical storage and holography, optical coherence, polarization, quantum electronics, ultrafast optical phenomena, photonic crystals and fiber optics.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/tos-its020917.php){:target="_blank" rel="noopener"}


