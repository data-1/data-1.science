---
layout: post
title: "Neurons constantly rewrite their DNA"
date: 2016-04-24
categories:
author: Johns Hopkins Medicine
tags: [Synapse,Chemical synapse,Tet methylcytosine dioxygenase 3,Neuron,Neurotransmitter,DNA,Neuroscience,Biology,Biotechnology,Life sciences,Biochemistry]
---


We used to think that once a cell reaches full maturation, its DNA is totally stable, including the molecular tags attached to it to control its genes and maintain the cell's identity, says Hongjun Song, Ph.D., a professor of neurology and neuroscience in the Johns Hopkins University School of Medicine's Institute for Cell Engineering. Neurons can toggle the volume of this communication by adjusting the activity level of their genes to change the number of their messengers or receptors on the surface of the neuron. Surprisingly, when Tet3 levels were up, synaptic activity was down; when Tet3 levels were down, synaptic activity was up. If synaptic activity increases, Tet3 activity and base excision of tagged cytosines increases. This causes the levels of GluR1 at synapses to decrease, in turn, which decreases their overall strength, bringing the synapses back to their previous activity level.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/jhm-ncr042315.php){:target="_blank" rel="noopener"}


