---
layout: post
title: "Tapping evolution to improve biotech products"
date: 2017-03-17
categories:
author: "Emory University"
tags: [Factor VIII,Protein,Gene therapy,Evolution,Cancer,Clinical medicine,Biotechnology,Biology,Life sciences]
---


Credit: Emory University  Scientists can improve protein-based drugs by reaching into the evolutionary past, a paper published in Nature Biotechnology proposes. As a proof of concept for this approach, the research team from Emory, Children's Healthcare of Atlanta and Georgia Tech showed how ancestral sequence reconstruction or ASR can guide engineering of the blood clotting protein known as factor VIII, which is deficient in the inherited disorder hemophilia A. The authors say that ASR-based engineering could be applied to other recombinant proteins produced outside the human body, as well as gene therapy. Doering's lab teamed up with, Trent Spencer, PhD, director of cell and gene therapy for the Aflac Cancer and Blood Disorders Center, and Eric Gaucher, PhD, associate professor of biological sciences at Georgia Tech, who specializes in ASR. Using this information, scientists reconstruct a plausible ancestral sequence for a protein in early mammals.

<hr>

[Visit Link](http://phys.org/news/2016-09-evolution-biotech-products.html){:target="_blank" rel="noopener"}


