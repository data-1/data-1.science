---
layout: post
title: "Earth’s atmosphere: new results from the International Space Station"
date: 2018-08-16
categories:
author: ""
tags: [Sun,Atmosphere,Atmosphere of Earth,Solar System,Bodies of the Solar System,Sky,Astronomy,Outer space,Space science,Nature,Planetary science,Planets,Physical sciences,Astronomical objects,Spaceflight]
---


Science & Exploration Earth’s atmosphere: new results from the International Space Station 20/03/2018 6801 views 119 likes  With ESA’s help, the latest atmosphere monitor on the International Space Station is delivering results on our planet’s ozone, aerosol and nitrogen trioxide levels. Installed last year on the orbital outpost, NASA’s sensor tracks the Sun and Moon to probe the constituents of our atmosphere. SAGE and Hexapod The Station takes only 90 minutes for a complete circuit of our planet, experiencing 16 sunrises, 16 sunsets, and sometimes moonrises or moonsets, every day. “The Hexapod and SAGE collaboration is an exemplary transatlantic cooperation and we are very happy to see the first results coming in.”  International Space Station ESA has a history of tracking the Sun from the Space Station: working for more than nine years, its SOLAR facility created the most precise reference on the Sun’s energy output ever. The next ESA sensor to be launched to the Station is the Atmospheric Space Interactions Monitor, which will point straight down at Earth to investigate high-altitude electrical storms.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Research/Earth_s_atmosphere_new_results_from_the_International_Space_Station){:target="_blank" rel="noopener"}


