---
layout: post
title: "How seashells get their strength"
date: 2016-01-29
categories:
author: DOE/Pacific Northwest National Laboratory
tags: [Crystal,Exoskeleton,Biomineralization,Calcium,Pacific Northwest National Laboratory,Calcium carbonate,Atomic force microscopy,Chemistry,Physical sciences,Materials,Applied and interdisciplinary physics,Nature]
---


Though all three are made of calcium carbonate crystals, the hard materials include clumps of soft biological matter that make them much stronger. This work helps us to sort out how rather weak crystals can form composite materials with remarkable mechanical properties, said materials scientist Jim De Yoreo of the Department of Energy's Pacific Northwest National Laboratory. Proteins trapped in calcium carbonate crystals create a compressive force -- or strain -- within the crystal structure. The step edge has chemistry that the terrace doesn't, said De Yoreo. The steps capture the micelles for a chemical reason, not a mechanical one, and the resulting compression of the micelles by the steps then leads to forces that explain where the strength comes from, said De Yoreo.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/dnnl-hsg010816.php){:target="_blank" rel="noopener"}


