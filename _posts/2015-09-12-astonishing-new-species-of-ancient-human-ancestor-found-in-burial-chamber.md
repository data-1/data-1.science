---
layout: post
title: "Astonishing new species of ancient human ancestor found in burial chamber"
date: 2015-09-12
categories:
author: "$author"   
tags: [Homo naledi,Rising Star Cave,Homo,Human evolution]
---


The bones of new species of human relative have reportedly been found in a burial chamber in South Africa. The so-called “Homo naledi” fossilized bones recovered from the Rising Star cave in South Africa. The as-yet undated bones are being claimed by the project to reveal a new species of ancient human relative, named Homo Naledi (meaning ‘star’ in a South African language). What astonishes researchers the most is the realization that this species seems to have purposefully buried their dead in the hidden chamber below the ground. Not all experts on human origins are in agreeance with the new claims, and do not rule out that the bones might well be those of early Homo erectus , a species closely connected to modern humans who lived 1.5 million years ago in southern Africa.

<hr>

[Visit Link](http://www.ancient-origins.net/news-evolution-human-origins/astonishing-new-species-ancient-human-ancestor-found-burial-chamber-020523){:target="_blank" rel="noopener"}


