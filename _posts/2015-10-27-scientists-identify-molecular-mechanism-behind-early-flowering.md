---
layout: post
title: "Scientists identify molecular mechanism behind early flowering"
date: 2015-10-27
categories:
author: Technical University Munich
tags: [Arabidopsis thaliana,Transposable element,Gene,Plant,Flower,Plant breeding,DNA,Biology,Genetics,Biotechnology]
---


Although the mechanisms that cause flowering before and after winter are largely known by now, relatively little is known about how plants delay their flowering time during a cold spring. Climate change will bring about a change in the flowering behavior of many plants. The temperatures in a cool or warm spring also affect flowering behavior; however, very little is known about this. Given that small changes of just a few degrees Celsius can have a negative impact on agricultural production, it is important to understand these processes. The findings of the research team from the TUM Chair of Plant Systems Biology could help with the prediction and even modification of plant flowering time in the future.

<hr>

[Visit Link](http://phys.org/news/2015-10-scientists-molecular-mechanism-early.html){:target="_blank" rel="noopener"}


