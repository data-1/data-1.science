---
layout: post
title: "The bizarre world of topological materials"
date: 2018-07-02
categories:
author: "American Chemical Society"
tags: [Topological insulator,Materials,Physical sciences,Chemistry,Technology]
---


In 2016, three physicists received the Nobel Prize for using the mathematical concept of topology to explain the strange behavior of certain materials—for example, those that are insulators in their bulk but conductors on their surface. Now, researchers are investigating applications for these exotic materials in electronics, catalysis and quantum computing, according to an article in Chemical & Engineering News (C&EN), the weekly news magazine of the American Chemical Society. Topological materials are unusual for the robustness of their electrical properties, even when the temperature shifts dramatically or their physical structure is deformed, writes Contributing Editor Neil Savage. This ruggedness results from certain stable electronic states within the materials, which typically contain heavy metals. When electrons in a current hit a defect in the material, they simply flow around it, instead of being scattered or experiencing resistance as in traditional conductors.

<hr>

[Visit Link](https://phys.org/news/2018-06-bizarre-world-topological-materials.html){:target="_blank" rel="noopener"}


