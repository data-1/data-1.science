---
layout: post
title: "What types of video games improve brain function?"
date: 2015-12-21
categories:
author: SAGE
tags: [Cognition,American Association for the Advancement of Science,Cognitive science,Video game,Attention,Brain,Research,Psychology,Interdisciplinary subfields,Psychological concepts,Science]
---


This article is published today in the new issue of Policy Insights from the Behavioral and Brain Sciences, a Federation of Associations in Behavioral & Brain Sciences (FABBS) journal published by SAGE. C. Shawn Green and Aaron R. Seitz wrote that action video games- games that feature quickly moving targets that come in and out of view, include large amounts of clutter, and that require the user to make rapid, accurate decisions - have particularly positive cognitive impacts, even when compared to brain games, which are created specifically to improve cognitive function. Furthermore, video games are known to impact not only cognitive function, but many other aspects of behavior - including social functions - and this impact can be either positive or negative depending on the content of the games. ###  Find out more by reading the full article, The Impacts of Video Games on Cognition (and How the Government Can Guide the Industry) in the new issue of Policy Insights from the Behavioral and Brain Sciences. http://www.sagepub.com  Policy Insights from the Behavioral and Brain Sciences is an annual publication of the Federation of Associations in Behavioral and Brain Sciences (FABBS) that presents original research and scientific reviews relevant to public policy.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/sp-wto092915.php){:target="_blank" rel="noopener"}


