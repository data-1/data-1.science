---
layout: post
title: "Images of 2017 in physics – Physics World"
date: 2018-06-10
categories:
author: "Hamish Johnston"
tags: [X-ray crystallography,Cavitation,Jupiter,Glass,Sudbury Neutrino Observatory,Juno (spacecraft),Milky Way,Liquid,3D printing,Bubble (physics),Nature,Chemistry,Physical sciences]
---


Magnetic anomaly: a unique view of the Central African Republic  Magnetic map of Earth’s crust  This high-resolution map shows the magnetic field of the Earth’s crust in a completely new light. Transparent kingdom: 3D microstructures  can be printed using liquid glass  Tiny glass castle made by 3D printer  This is a microscope image of a glass castle measuring less than 2 mm across. It was made using a new 3D printing technique developed by researchers in Germany. Tiny bubbles: cavitation occurring in a liquid crystal  Bubble cavitation spotted in liquid crystals  This time series of images shows the formation of a cavitation bubble in a flowing liquid crystal, which has been seen for the first time by physicists in Germany. The technique was developed by researchers in the US, who were able to collect enough data within 14 min at room temperature to determine the structure of the virus down to 0.23 nm.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/dec/15/images-of-2017-in-physics){:target="_blank" rel="noopener"}


