---
layout: post
title: "Einstein's Cross under the gravitational microlens"
date: 2015-12-22
categories:
author: Asociación RUVID
tags: [Quasar,Black hole,Gravitational microlensing,Supermassive black hole,Space science,Physical sciences,Physics,Astrophysics,Astronomy,Astronomical objects,Physical cosmology,Celestial mechanics,Concepts in astronomy,Science,Cosmology,General relativity,Physical phenomena]
---


The Spanish interuniversity research group has obtained measurements for the innermost region of a disc of matter in orbital motion around a supermassive black hole tucked inside the quasar known as Einstein's Cross (Q2237-0305). Microlensing can be used to detect objects that either emit little light or are too far away to be measured the usual way. At the edge of a black hole  Quasars are very small, very distant objects that emit vast amounts of light. By studying the variation in brightness of the four different images of the disc provided by OGLE and GLITP, researchers have been able to obtain precise measurements of the structure of its innermost region, right at the edge of the black hole (also known as the event horizon). His colleague at the University of Granada, Jorge Jiménez Vicente, adds that the big breakthrough in this work is that we have been able to detect a structure on the innermost edge of such a small disc, so far away.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/12/151221095531.htm){:target="_blank" rel="noopener"}


