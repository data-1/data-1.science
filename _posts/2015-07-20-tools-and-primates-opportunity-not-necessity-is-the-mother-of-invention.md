---
layout: post
title: "Tools and primates: Opportunity, not necessity, is the mother of invention"
date: 2015-07-20
categories:
author: University of Cambridge 
tags: [Tool use by animals,Orangutan,Chimpanzee,Branches of science,Ethology,Zoology,Cognitive science,Animals,Behavioural sciences]
---


However, a review of current research into the use of tools by non-human primates suggests that ecological opportunity, rather than necessity, is the main driver behind primates such as chimpanzees picking up a stone to crack open nuts. She and her colleagues argue that research into tool use by primates should look at the opportunities for tool use provided by the local environment. Koops and colleagues reviewed studies on tool use among the three habitual tool-using primates - chimpanzees, orangutans and bearded capuchins. We showed that these ecological opportunities influence the occurrence of tool use. Our study suggests that published research on primate cultures, which depend on the 'method of exclusion', may well underestimate the cultural repertoires of primates in the wild, perhaps by a wide margin.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-11/uoc-tap111014.php){:target="_blank" rel="noopener"}


