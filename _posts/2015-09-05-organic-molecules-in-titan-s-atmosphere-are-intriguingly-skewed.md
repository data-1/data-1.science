---
layout: post
title: "Organic molecules in Titan's atmosphere are intriguingly skewed"
date: 2015-09-05
categories:
author: National Radio Astronomy Observatory 
tags: [Atacama Large Millimeter Array,Titan (moon),Associated Universities Inc,Astronomy,Physical sciences,Science,Space science,Nature,Planetary science,Outer space,Astronomical objects]
---


While studying the atmosphere on Saturn's moon Titan, scientists discovered intriguing zones of organic molecules unexpectedly shifted away from its north and south poles. We would expect the molecules to be quickly mixed around the globe by Titan's winds. Further observations are expected to improve our understanding of the atmosphere and ongoing processes on Titan and other objects throughout our Solar System. ###  The National Radio Astronomy Observatory is a facility of the National Science Foundation, operated under cooperative agreement by Associated Universities, Inc.  NASA's Astrobiology Program supported this work through a grant to the Goddard Center for Astrobiology, a part of the NASA Astrobiology Institute. ALMA construction and operations are led on behalf of Europe by ESO, on behalf of North America by the National Radio Astronomy Observatory (NRAO), which is managed by Associated Universities, Inc. (AUI) and on behalf of East Asia by the National Astronomical Observatory of Japan (NAOJ).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/nrao-omi102214.php){:target="_blank" rel="noopener"}


