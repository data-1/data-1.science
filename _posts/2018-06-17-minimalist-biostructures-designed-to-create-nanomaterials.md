---
layout: post
title: "Minimalist biostructures designed to create nanomaterials"
date: 2018-06-17
categories:
author: "Autonomous University Of Barcelona"
tags: [Nanotechnology,Protein,Prion,Amyloid,Protein domain,Amino acid,Biology,Macromolecules,Materials science,Materials,Biotechnology,Structural biology,Applied and interdisciplinary physics,Molecular biology,Biochemistry,Physical sciences,Chemistry]
---


Credit: IBB-UAB  Researchers of the Institute of Biotechnology and Biomedicine (IBB-UAB) have generated four peptides, molecules smaller than proteins, capable of self-assembling in a controlled manner to form nanomaterials. The new molecules are formed by a chain of seven amino acids, each of which are made up of only two amino acids, thus significantly speeding up and reducing the cost of creating functional synthetic amyloid structures with which to generate nanomaterials for biomedicine and nanotechnology. The four peptides we have fabricated are the shortest structures of this type created until now, and are capable of forming stable fibril assemblies, says Salvador Ventura, researcher at the IBB and the UAB Department of Biochemistry and Molecular Biology. The new molecules have numerous applications, but the researchers aim to focus on the generation of electrical nanoconductors and make use of the knowledge of the amyloid structure to generate synthetic fibres capable of being catalysts for new chemical reactions. In fact, each of the heptapeptides (mini-PrDs) designed only contains two different types of amino acids, says Salvador Ventura.

<hr>

[Visit Link](https://phys.org/news/2018-06-minimalist-biostructures-nanomaterials.html){:target="_blank" rel="noopener"}


