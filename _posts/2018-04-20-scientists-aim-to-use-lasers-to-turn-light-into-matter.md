---
layout: post
title: "Scientists aim to use lasers to turn light into matter"
date: 2018-04-20
categories:
author: "Greg Beach"
tags: []
---


Scientists at Imperial College London are attempting to use powerful lasers turn light into matter, potentially proving the 84-year-old theory known as the Breit-Wheeler process. According to this theory, it is technically possible to turn light into matter by smashing two photons to create a positron and an electron. While previous efforts to achieve this feat have required added high-energy particles, the Imperial scientists believe they have discovered a method that does not need additional energy to function. “This would be a pure demonstration of Einstein’s famous equation that relates energy and mass: E=mc2, which tells us how much energy is produced when matter is turned to energy,” explained Imperial Professor Steven Rose. “What we are doing is the same but backwards: turning photon energy into mass, i.e. m=E/c2.”  Continue reading below Our Featured Videos  The Imperial team’s system centers around two lasers, which create two different kinds of photons to be smashed.

<hr>

[Visit Link](https://inhabitat.com/scientists-aim-to-use-lasers-to-turn-light-into-matter){:target="_blank" rel="noopener"}


