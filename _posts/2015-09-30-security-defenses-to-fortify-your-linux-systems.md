---
layout: post
title: "Security Defenses to Fortify your Linux Systems"
date: 2015-09-30
categories:
author: Michael Boelen
tags: [Linux,Information security,Lynis,Risk,Patch (computing),Fortification,Risk management,Information Age,Computer science,Software,Technology,System software,Computer engineering,Software development,Information technology management,Software engineering,Information technology,Systems engineering,Computing]
---


How to Fortify your Linux Systems  Create a Linux security fortress; implementing security defenses using towers, bridges, and guards. Risk Management  Security boils down to understanding risk. When installing Linux systems, go for system hardening at day 1. Guards:  Review log files  Check software configurations  Have an external auditor or colleague do an analysis  Bridges:  Implement continuous auditing and monitoring tools (scripts, Lynis)  Implement system hardening  Centralized syslog server  Conclusion  Linux systems can be fortified to reduce the most common attacks. Your Linux system is not very different from the fortress of the medieval times.

<hr>

[Visit Link](http://linux-audit.com/security-defenses-to-fortify-your-linux-systems/){:target="_blank" rel="noopener"}


