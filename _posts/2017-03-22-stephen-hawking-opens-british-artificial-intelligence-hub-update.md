---
layout: post
title: "Stephen Hawking opens British artificial intelligence hub (Update)"
date: 2017-03-22
categories:
author: ""
tags: [Artificial intelligence,Robot,Stephen Hawking,Technology,Branches of science,Cognitive science,Science,Computing]
---


British scientist Stephen Hawking arrives to attend the launch of The Leverhulme Centre for the Future of Intelligence (CFI), at the University of Cambridge, in Cambridge, eastern England  Professor Stephen Hawking on Wednesday opened a new artificial intelligence research centre at Britain's Cambridge University. Opening the new centre, Hawking said it was not possible to predict what might be achieved with AI. The technology has led to major advances in the sciences of mind and life, she said, but, misused, also presents grave dangers. Hawking warned technological developments also posed a risk to our civilisation. Alongside the benefits, AI will also bring dangers, like powerful autonomous weapons, or new ways for the few to oppress the many.

<hr>

[Visit Link](http://phys.org/news/2016-10-british-artificial-intelligence-hub.html){:target="_blank" rel="noopener"}


