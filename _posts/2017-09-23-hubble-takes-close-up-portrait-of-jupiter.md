---
layout: post
title: "Hubble takes close-up portrait of Jupiter"
date: 2017-09-23
categories:
author: "ESA/Hubble Information Centre"
tags: [Jupiter,Great Red Spot,Hubble Space Telescope,Atmosphere of Jupiter,Planet,Earth,Sun,Solar System,Sky,Physical sciences,Bodies of the Solar System,Planets of the Solar System,Planemos,Planets,Outer space,Astronomical objects,Planetary science,Space science,Astronomy]
---


This image adds to many others made in the past, and together they allow astronomers to study changes in the atmosphere of the gas giant. The final image shows a sharp view of Jupiter and reveals a wealth of features in its dense atmosphere. These bands are created by differences in the opacity of the clouds which have varying quantities of frozen ammonia in them; the lighter bands have higher concentrations than the darker bands. However, as with the last images of Jupiter taken by Hubble and telescopes on the ground, this new image confirms that the huge storm which has raged on Jupiter's surface for at least 150 years continues to shrink. The observations of Jupiter form part of the Outer Planet Atmospheres Legacy (OPAL) programme, which allows Hubble to dedicate time each year to observing the outer planets.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/eic-htc040617.php){:target="_blank" rel="noopener"}


