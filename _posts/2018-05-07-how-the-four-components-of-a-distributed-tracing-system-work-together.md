---
layout: post
title: "How the four components of a distributed tracing system work together"
date: 2018-05-07
categories:
author: "Ted Young"
tags: [Library (computing),API,Microservices,Communication protocol,Database,System,Application software,Software,Information Age,Computer programming,Systems engineering,Computing,Computers,Computer science,Information technology,Computer engineering,Software development,Information technology management,Software engineering,Technology]
---


Your code. However, a shared library cannot not know which tracing protocol is being used by each application. So, an abstract API which (a) has no dependencies, (b) is wire protocol agnostic, and (c) works with popular vendors and analysis systems should be a requirement for instrumenting shared library code. The four big solutions  So, an abstracted tracing API would help libraries emit data and inject/extract Span Context. Tracing API: The OpenTracing project  As shown above, in order to instrument application code, a tracing API is required.

<hr>

[Visit Link](https://opensource.com/article/18/5/distributed-tracing){:target="_blank" rel="noopener"}


