---
layout: post
title: "What's important in open source today"
date: 2017-09-24
categories:
author: "Nicole C. Baratta
(Alumni, Red Hat)"
tags: [Open source,Red Hat,Technology]
---


Opensource.com community moderator Jono Bacon kicked off keynotes at All Things Open this year to talk about open source communities  Building communities  Jono mentioned that it's his goal in life to understand every nuance of how we build predicable, productive, and accessible communities. You need to feed your community and nurture your community to have a healthy open source community. Jim encouraged us to take a look at Open Source Stories to see how people are collaborating and using open for good. that is open source. Make it personal  The people who have been most successful at open source are the people who are scratching their own itch.

<hr>

[Visit Link](https://opensource.com/16/11/all-things-open-keynotes-day-1-summary){:target="_blank" rel="noopener"}


