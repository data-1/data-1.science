---
layout: post
title: "Paleo-engineering: Complexity of triceratops' teeth revealed"
date: 2015-09-14
categories:
author: Florida State University 
tags: [Dinosaur,Tooth,Reptile,Triceratops,News aggregator,Mammal,Animal,Wear,Taxa,Animals]
---


The teeth of most herbivorous mammals self wear with use to create complex file surfaces for mincing plants. It's just been assumed that dinosaurs didn't do things like mammals, but in some ways, they're actually more complex, Erickson said. advertisement  Erickson, who has been studying the evolution of dinosaurs for years, became interested in looking at dinosaurs' teeth several years ago and suspected that they had some unique properties. Krick is an assistant professor of mechanical engineering at Lehigh University and specializes in a relatively new area of materials science called tribology. Each of those tissues does something, Erickson said.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150605181935.htm){:target="_blank" rel="noopener"}


