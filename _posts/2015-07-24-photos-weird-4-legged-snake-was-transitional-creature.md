---
layout: post
title: "Photos: Weird 4-Legged Snake Was Transitional Creature"
date: 2015-07-24
categories:
author: Laura Geggel
tags: [Tetrapodophis,Snake,Paleontology]
---


Like other snake fossils from the Cretaceous, this one is from Gondwana, suggesting that snakes originated on the southern supercontinent. (Image credit: Dave Martill | University of Portsmouth.) (Image credit: Dave Martill | University of Portsmouth.) (Image credit: Dave Martill | University of Portsmouth.) (Image credit: Dave Martill | University of Portsmouth.)

<hr>

[Visit Link](http://www.livescience.com/51643-photos-four-legged-snake.html){:target="_blank" rel="noopener"}


