---
layout: post
title: "LHC achieves record luminosity"
date: 2018-04-26
categories:
author: "Corinne Pralavorio"
tags: [Large Hadron Collider,ATLAS experiment,Particle physics,Experimental particle physics,Accelerator physics,Physics laboratories,Particle physics facilities,Experimental physics,Applied and interdisciplinary physics,Science,Physics]
---


It has provided its two major experiments, ATLAS and CMS, with 50 inverse femtobarns of data, i.e. 5 billion million million collisions. The inverse femtobarn (fb-1) is the unit used to measure integrated luminosity, or the cumulative number of potential collisions over a given period. At the same time, over the course of the year, the operators have optimised the operating parameters. The LHC will continue to operate for another two weeks for two special runs including a week for operation studies. Over a week, they will perform operating tests to improve the accelerator's performance still further (it can never be too good) and begin to prepare the High-Luminosity LHC, which will take over from the LHC after 2025.

<hr>

[Visit Link](https://phys.org/news/2017-11-lhc-luminosity.html){:target="_blank" rel="noopener"}


