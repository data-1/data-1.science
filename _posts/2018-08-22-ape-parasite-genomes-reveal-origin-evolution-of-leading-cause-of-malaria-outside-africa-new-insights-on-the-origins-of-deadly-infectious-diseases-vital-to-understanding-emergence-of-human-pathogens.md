---
layout: post
title: "Ape parasite genomes reveal origin, evolution of leading cause of malaria outside Africa: New insights on the origins of deadly infectious diseases vital to understanding emergence of human pathogens"
date: 2018-08-22
categories:
author: "University of Pennsylvania School of Medicine"
tags: [Plasmodium vivax,Malaria,Infection,Parasitism,Human,Genetics,Microbiology,Biology,Medical specialties]
---


In this study, researchers compared almost-full-length genomes of ape parasites with sequences of P. vivax strains infecting humans worldwide. After generating sequences of parasites infecting chimpanzees and gorillas, the team found that ape P. vivax show very different patterns of genetic diversity, compared to their human counterparts. This once-small parasite population subsequently underwent a rapid population expansion as it dispersed over much of the globe, while the spread of the malaria-protective mutation eliminated P. vivax from people in most of sub-Saharan Africa. Since parasites very similar to human P. vivax infect a large number of wild-living apes, we need to be aware that there could be spillover into humans, said co-senior author Paul Sharp, PhD, an evolutionary biologist at the University of Edinburgh. The team cautions that malaria monitoring programs should keep a watch to see if the genetic diversity of the ape parasites is infiltrating human-infecting strains.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180820155109.htm){:target="_blank" rel="noopener"}


