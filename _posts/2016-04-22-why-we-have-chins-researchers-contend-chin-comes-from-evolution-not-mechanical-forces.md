---
layout: post
title: "Why we have chins: Researchers contend chin comes from evolution, not mechanical forces"
date: 2016-04-22
categories:
author: Richard C. Lewis, University Of Iowa
tags: [Chin,Human]
---


The reason: Only modern humans have chins. Rather, they write in a paper published online in the Journal of Anatomy, it appears the chin's emergence in modern humans arose from simple geometry: As our faces became smaller in our evolution from archaic humans to today—in fact, our faces are roughly 15 percent shorter than Neanderthals'—the chin became a bony prominence, the adapted, pointy emblem at the bottom of our face. Credit: Nathan Holton lab, University of Iowa  In short, we do not find any evidence that chins are tied to mechanical function and in some cases we find that chins are worse at resisting mechanical forces as we grow, says Holton, assistant professor and anthropologist in the Department of Orthodontics at the UI College of Dentistry. Thus, arose the theory that mechanical forces, such as chewing, led to our chins. But in examinations from periodic measurements of participants' heads from 3 years of age to more than 20 years old, the UI researchers found no evidence that these imperceptible mechanical forces led to new bone in the chin region.

<hr>

[Visit Link](http://phys.org/news348163865.html){:target="_blank" rel="noopener"}


