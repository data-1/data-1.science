---
layout: post
title: "Climate engineering, once started, would have severe impacts if stopped: Researchers co-author first study on biological impacts of abruptly ending efforts to cool Earth's climate"
date: 2018-04-19
categories:
author: "Rutgers University"
tags: [Climate engineering,Climate change,Atmosphere of Earth,Earth phenomena,Natural environment,Earth sciences,Nature,Physical geography,Climate variability and change,Climate,Applied and interdisciplinary physics,Global environmental issues,Environmental impact,Global natural environment,Atmosphere,Environmental issues with fossil fuels]
---


Facing a climate crisis, we may someday spray sulfur dioxide into the upper atmosphere to form a cloud that cools the Earth, but suddenly stopping the spraying would have a severe global impact on animals and plants, according to the first study on the potential biological impacts of geoengineering, or climate intervention. Rapid warming after stopping geoengineering would be a huge threat to the natural environment and biodiversity, Robock said. While scientists have studied the climate impacts of geoengineering in detail, they know almost nothing about its potential impacts on biodiversity and ecosystems, the study notes. The geoengineering idea that's attracted the most attention is to create a sulfuric acid cloud in the upper atmosphere as large volcanic eruptions do, Robock said. But if rapid warming forced them to move, and even if they could move fast enough, they may not be able find places with enough food to survive, he said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/01/180122150758.htm){:target="_blank" rel="noopener"}


