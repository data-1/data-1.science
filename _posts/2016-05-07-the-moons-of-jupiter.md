---
layout: post
title: "The moons of Jupiter"
date: 2016-05-07
categories:
author: Matt Williams
tags: [Moons of Jupiter,Galilean moons,Jupiter,Natural satellite,Ganymede (moon),Amalthea (moon),Adrastea (moon),Europa (moon),Callisto (moon),Thebe (moon),Io (moon),Irregular moon,Planet,Solar System,Ancient astronomy,Planetary science,Astronomical objects,Space science,Astronomy,Astronomical objects known since antiquity,Outer planets,Planets,Bodies of the Solar System,Moons,Gas giants,Planets of the Solar System,Outer space]
---


First, there are the largest moons known as the Galileans, or Main Group. They contain almost 99.999% of the total mass in orbit around Jupiter, and orbit between 400,000 and 2,000,000 km from the planet. At 5262.4 kilometers in diameter, Ganymede is the largest moon in the Solar System. Ganymede is composed primarily of silicate rock and water ice, and a salt-water ocean is believed to exist nearly 200 km below Ganymede's surface – though Europa remains the most likely candidate for this. This groups includes the moons of Metis, Adrastea, Amalthea, and Thebe.

<hr>

[Visit Link](http://phys.org/news353145957.html){:target="_blank" rel="noopener"}


