---
layout: post
title: "Ancient rocks record first evidence for photosynthesis that made oxygen"
date: 2015-10-08
categories:
author: David Tenenbaum, University Of Wisconsin-Madison
tags: [Uranium,Oxygen,Photosynthesis,Iron,Ocean,Water,Great Oxidation Event,Physical sciences,Earth sciences,Chemistry,Nature]
---


Aaron Satkoski, a scientist in the UW-Madison Geoscience Department, holds a sample sawn from a 3.23-billion-year-old rock core sample found in South Africa. Credit: David Tenenbaum/University of Wisconsin-Madison  A new study shows that iron-bearing rocks that formed at the ocean floor 3.2 billion years ago carry unmistakable evidence of oxygen. Iron oxides contained in the fine-grained, deep sediment that formed below the level of wave disturbance formed in the water with very little oxygen, says first author Aaron Satkoski, an assistant scientist in the Geoscience Department. Measurements of lead formed from the radioactive decay of uranium showed that the uranium entered the rock sample 3.2 billion years ago. Cyanobacteria could live in shallow water, doing photosynthesis, generating oxygen, but oxygen was not necessarily in the atmosphere or the deep ocean.

<hr>

[Visit Link](http://phys.org/news/2015-10-ancient-evidence-photosynthesis-oxygen.html){:target="_blank" rel="noopener"}


