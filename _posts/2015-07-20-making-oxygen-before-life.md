---
layout: post
title: "Making oxygen before life"
date: 2015-07-20
categories:
author: Genetics Society of America 
tags: [Venom,Rattlesnake,Snake,Predation,American Association for the Advancement of Science,Species,Antivenom,Snake venom,Coral,Animals,Biology]
---


But before the first photosynthesizing organisms appeared about 2.4 billion years ago, the atmosphere likely contained mostly carbon dioxide, as is the case today on Mars and Venus. Now UC Davis graduate student Zhou Lu, working with professors in the Departments of Chemistry and of Earth and Planetary Sciences, has shown that oxygen can be formed in one step by using a high energy vacuum ultraviolet laser to excite carbon dioxide. Previously, people believed that the abiotic (no green plants involved) source of molecular oxygen is by CO 2 + solar light — > CO + O, then O + O + M — > O 2 + M (where M represents a third body carrying off the energy released in forming the oxygen bond), Zhou said in an email. Zhou used a vacuum ultraviolet laser to irradiate CO 2 in the laboratory. Such one-step oxygen formation could be happening now as carbon dioxide increases in the region of the upper atmosphere, where high energy vacuum ultraviolet light from the Sun hits Earth or other planets.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/gsoa-msv010815.php){:target="_blank" rel="noopener"}


