---
layout: post
title: "Artificial Intelligence uncovers anti-aging plant extracts"
date: 2018-06-29
categories:
author: "InSilico Medicine"
tags: [Insilico Medicine,Life extension,Anti-aging movement,Artificial intelligence,Ageing,Dietary supplement,Deep learning,Health,Longevity,Branches of science,Health care,Life sciences,Technology,Medicine,Health sciences]
---


- Life Extension, in collaboration with Insilico Medicine, have employed artificial intelligence to develop a novel dietary supplement of plant extracts that can activate the body's own anti-aging path ways. Insilico, a next-generation artificial intelligence company specializing in the application of deep learning proprietary technology for biomarker development and aging research, used pioneering high-performance computer simulations to probe of the effect of extracts on anti-aging pathways. Suggested retail price for 30 softgels of GEROPROTECTTM Longevity A.I is $56. The company utilizes advances in genomics, big-data analysis, and deep learning for in silico drug discovery and drug repurposing for aging and age-related diseases. http://www.insilico.com  About Life Extension  For over 37 years, Life Extension has been a pioneer in funding and reporting the latest anti-aging research and integrative health regimens while offering superior-quality dietary supplements.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/imi-aiu103017.php){:target="_blank" rel="noopener"}


