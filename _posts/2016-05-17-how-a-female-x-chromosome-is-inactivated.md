---
layout: post
title: "How a female X chromosome is inactivated"
date: 2016-05-17
categories:
author: ETH Zurich
tags: [XIST,Gene,Genetics,X-inactivation,Cell (biology),Stem cell,X chromosome,RNA,News aggregator,Chromosome,Biochemistry,Biology,Branches of genetics,Life sciences,Biotechnology,Molecular biology,Cell biology]
---


A team of researchers lead by Anton Wutz, Professor of Genetics at ETH Zurich, have now discovered several of these inactivation molecules. Virus insertions that destroyed a gene, which was required for Xist RNA to inactivate the X chromosome, the X chromosome was not inactivated, and the corresponding cells survived. In other experiments, ETH researchers were able to show that if a mouse cell lacks the Spen gene, the proteins responsible for altering chromosome structure are not able to accumulate as efficiently at the X chromosome. ETH Professor Wutz explains that further research is required to understand exactly how this mechanism works and what role the other recently discovered genes play in it. A few years ago, a team of French researchers postulated that, in addition to Xist, humans also have another system which ensures that the single X chromosome in men and one of the two X chromosomes in women remain active.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150810110747.htm){:target="_blank" rel="noopener"}


