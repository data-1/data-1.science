---
layout: post
title: "What Philae did in its 60 hours on Comet 67P"
date: 2016-04-11
categories:
author: Mark Lorch, The Conversation
tags: [Comet,Philae (spacecraft),67PChuryumovGerasimenko,Neutron,Isotope,Proton,Solar System,Chemistry,Physical sciences,Bodies of the Solar System,Nature,Planetary science,Space science,Astronomy]
---


Right- or left-handed life  Four billion years ago the solar system was an unsettled place. These compounds may well have triggered the chemistry that led to life on our planet. Structurally, they are the same except for the fact that you can't superimpose one on the other. Philae's COSAC instrument is designed to sniff away at the comet's organic contents and figure out whether they look like the building blocks of life and, importantly, whether the comet contains the same preference for lefty chemistry as Earth-bound life. And since the material in the solar system came from more or less the same place, the isotopic carbon ratios for the Sun, the Earth and asteroids are pretty much the same.

<hr>

[Visit Link](http://phys.org/news335520277.html){:target="_blank" rel="noopener"}


