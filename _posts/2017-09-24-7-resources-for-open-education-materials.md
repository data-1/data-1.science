---
layout: post
title: "7 resources for open education materials"
date: 2017-09-24
categories:
author: "Don Watkins
(Correspondent)"
tags: [Open educational resources,Massive open online course,Education,Communication,Learning,Teaching,Behavior modification,Technology]
---


Shrinking school budgets and growing interest in open content has created an increased demand for open educational resources. According to the FCC, The U.S. spends more than $7 billion per year on K-12 textbooks, but too many students are still using books that are 7-10 years old, with outdated material. There is an alternative: openly licensed courseware. But where do you find this content and how can you share your own teaching and learning materials? This month I've rounded up a list of seven open educational resources for K-12 and higher education:  What additions do you have for the list?

<hr>

[Visit Link](https://opensource.com/education/16/8/7-resources-open-education-materials){:target="_blank" rel="noopener"}


