---
layout: post
title: "Exploring the human side of climate change adaptation"
date: 2016-04-20
categories:
author: Rob Margetta, National Science Foundation
tags: [Climate change adaptation,Psychological resilience,Climate change,Effects of climate change,Research,Privacy,Community]
---


Lemos' research is geared toward developing a conceptual model of adaptive capacity that could help civic leaders in communities in Brazil, the United States, or other areas, make strategic decisions about how they allocate resources. But Maria Carmen Lemos' research indicates that they need also to look at the human side: behavioral, social and economic factors including income, education, health and understanding of technology to build adaptive capacity to respond to these events. We found in a lot of assessments that people don't call it 'adaptation,' Lemos said. The region had built up measures for dealing with winters, but was less prepared for increased flooding and cities warming. Her lab is still working to find ways to categorize the different social, behavioral and economic stressors that will influence how people respond to climate change, and to see how they relate to climate models that measure the manifestations of climate change.

<hr>

[Visit Link](http://phys.org/news347263399.html){:target="_blank" rel="noopener"}


