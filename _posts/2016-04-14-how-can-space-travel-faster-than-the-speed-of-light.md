---
layout: post
title: "How can space travel faster than the speed of light?"
date: 2016-04-14
categories:
author: Vanessa Janek
tags: [Universe,Redshift,Physical cosmology,Expansion of the universe,Cosmological horizon,Observable universe,Hubble volume,Event horizon,General relativity,Hubbles law,Dark energy,Speed of light,Astronomy,Cosmology,Physics,Astrophysics,Physical sciences,Nature,Physical quantities,Theory of relativity,Science,Space science]
---


The light from these distant objects has been traveling for so long that, when we finally see it, we are seeing the objects as they were billions of years ago. The Hubble Volume  Redshifted light allows us to see objects like galaxies as they existed in the distant past; but we cannot see all events that occurred in our Universe during its history. Because our cosmos is expanding, the light from some objects is simply too far away for us ever to see. We can see objects that have accelerated beyond our current Hubble volume because the light we see today was emitted when they were within it. The particle horizon marks the distance to the farthest light that we can possibly see at this moment in time – photons that have had enough time to either remain within, or catch up to, our gently expanding Hubble sphere.

<hr>

[Visit Link](http://phys.org/news343902532.html){:target="_blank" rel="noopener"}


