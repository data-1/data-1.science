---
layout: post
title: "How do plants make oxygen? Ask cyanobacteria"
date: 2017-08-31
categories:
author: "California Institute of Technology"
tags: [Cyanobacteria,Algae,Photosynthesis,Plant,Evolution,Biology,Nature,Organisms,Life sciences]
---


The ability to generate oxygen through photosynthesis--that helpful service performed by plants and algae, making life possible for humans and animals on Earth--evolved just once, roughly 2.3 billion years ago, in certain types of cyanobacteria. The 41 species are all types of cyanobacteria but none carry genes for photosynthesis, and therefore they don't produce organic matter, like algae and plants do. Fischer and his colleagues found that a single branch of cyanobacteria--dubbed Oxyphobacteria--were likely the first and only group to evolve oxygenic photosynthesis. Their closest relatives, Melainabacteria, live in the guts of animals (including humans) among other environments, and do not produce oxygen. And while one might suggest that Melainabacteria simply lost the ability to produce oxygen over time, the next most closely related cyanobacteria after those, described in the paper as Sericytochromatia, also do not engage in oxygenic photosynthesis.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/ciot-hdp033017.php){:target="_blank" rel="noopener"}


