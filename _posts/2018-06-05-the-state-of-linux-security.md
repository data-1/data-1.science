---
layout: post
title: "The state of Linux security"
date: 2018-06-05
categories:
author: "Michael Boelen"
tags: [Linux kernel,Buffer overflow protection,Transport Layer Security,Ksplice,System software,Information technology management,Security engineering,Information technology,Cybercrime,Cyberwarfare,Computer engineering,Computers,Computer architecture,Computer security,Information Age,Software engineering,Software development,Computer science,Technology,Software,Computing]
---


25 years of Linux  This year included the celebration of the Linux project. When a new security vulnerability hits the kernel, the distribution can create a related patch. Linux vulnerabilities  Like previous years, this year had a fair number of serious vulnerabilities. Those who discovered the issue explain how it can result in root privileges, in their great write-up. Early 2000’s we saw rootkits, backdoored binaries, and an arsenal of tools to crash well-known software.

<hr>

[Visit Link](https://linux-audit.com/the-state-of-linux-security/){:target="_blank" rel="noopener"}


