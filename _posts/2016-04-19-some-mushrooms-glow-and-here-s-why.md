---
layout: post
title: "Some mushrooms glow, and here's why"
date: 2016-04-19
categories:
author: Cell Press
tags: [Fungus,Bioluminescence,Mushroom,Insect,Jay Dunlap,Organisms]
---


Only 71 of more than 100,000 described fungal species produce green light in a biochemical process that requires oxygen and energy. Researchers had believed in most cases that fungi produce light around the clock, suggesting that perhaps it was a simple, if expensive, metabolic byproduct. To find out what that green glow might do for the mushrooms, the researchers made sticky, fake mushrooms out of acrylic resin and lit some from the inside with green LED lights. Dunlap says they are interested in identifying the genes responsible for the mushrooms' bioluminescence and exploring their interaction with the circadian clock that controls them. As a result, Stevani says, it is very important to know how basidiomycetes grow and consequently how they spread their spores.

<hr>

[Visit Link](http://phys.org/news345979210.html){:target="_blank" rel="noopener"}


