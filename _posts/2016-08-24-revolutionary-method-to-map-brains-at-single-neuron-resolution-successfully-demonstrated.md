---
layout: post
title: "Revolutionary method to map brains at single-neuron resolution successfully demonstrated"
date: 2016-08-24
categories:
author: "Cold Spring Harbor Laboratory"
tags: [Cold Spring Harbor Laboratory,Brain,Neuron,DNA sequencing,Axon,Connectome,Neuroscience,Life sciences,Biology,Biotechnology]
---


The new method, called MAPseq (Multiplexed Analysis of Projections by Sequencing), makes it possible in a single experiment to trace the long-range projections of large numbers of individual neurons from a specific region or regions to wherever they lead in the brain - in experiments that are many times less expensive, labor-intensive and time-consuming than current mapping technologies allow. Such markers are good at determining all of the regions where neurons in the source region project to, but they cannot tell scientists that any two neurons in the source region project to the same region, to different regions, or to some of the same regions, and some different ones. If you go to the international terminal, you see a long line of ticket counters, Zador explains. Zador and his team, including Justus Kebschull, a graduate student in his lab who is first author on the Neuron paper introducing the new method, have spent several years working out a technology that enables them to assign unique barcode-like identifiers to large numbers of individual neurons via a single injection in any brain region of interest. In their demonstration experiment, only RNA that ended up in the cortex or olfactory bulb was sequenced, along with that of the source region in the LC where the barcodes were originally injected.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/cshl-rmt081816.php){:target="_blank" rel="noopener"}


