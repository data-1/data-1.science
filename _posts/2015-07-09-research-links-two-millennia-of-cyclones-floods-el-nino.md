---
layout: post
title: "Research links two millennia of cyclones, floods, El Nino"
date: 2015-07-09
categories:
author: Cornell College
tags: [Tropical cyclone,El NioSouthern Oscillation,El Nio,Rain,Weather,Tropics,Atmospheric sciences,Branches of meteorology,Meteorology,Tropical meteorology,Climate,Natural environment,Earth phenomena,Natural hazards,Earth sciences,Physical geography]
---


The article, Extreme rainfall activity in the Australian tropics reflects changes in the El Niño/Southern Oscillation over the last two millennia, presents a precisely dated stalagmite record of cave flooding events that are tied to tropical cyclones, which include storms such as hurricanes and typhoons. There was no doubt that the mud layers came from the cave having flooded. Once the ages of the stalagmites were known, the mud layers were measured. During El Niño events, for example, Australia and the Atlantic generally experience fewer tropical cyclones, while during La Niña events, the climatological opposite of El Niño, the regions see more tropical cyclones. Our work, and that of several other researchers, reveals that the frequency of storms has changed over the past hundreds and thousands of years, Denniston said.

<hr>

[Visit Link](http://phys.org/news347003951.html){:target="_blank" rel="noopener"}


