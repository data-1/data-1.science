---
layout: post
title: "How fungi grow: A movie from inside the cell"
date: 2018-04-20
categories:
author: "Karlsruher Institut für Technologie (KIT)"
tags: [Fungus,Hypha,Microscopy,Mold,Cell (biology),Protein,Biology]
---


Researchers of Karlsruhe Institute of Technology (KIT) have made a big step forwards: Using high-performance light microscopy, they watched mold fungi as they grew in the cell. Like most fungi, mold fungi are hyphal fungi. An important objective of biological fundamental research is to understand this growth on the molecular level, as hyphal growth plays an important role in both health-damaging effects and beneficial applications of fungi. They reveal how building materials are packed into smallest vesicles and transported along the fiber structures of the cell skeleton to the cell tip by transport vehicles, the motor proteins. advances.sciencemag.org/content/4/1/e1701798  Press contact:  Regina Link  Editor  +49 721 608-21158  regina.link@kit.edu  Being The Research University in the Helmholtz Association, KIT creates and imparts knowledge for the society and the environment.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-03/kift-hfg031518.php){:target="_blank" rel="noopener"}


