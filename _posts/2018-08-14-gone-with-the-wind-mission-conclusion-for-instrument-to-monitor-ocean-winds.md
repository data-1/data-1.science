---
layout: post
title: "Gone with the wind: Mission conclusion for instrument to monitor ocean winds"
date: 2018-08-14
categories:
author: "NASA/Johnson Space Center"
tags: [QuikSCAT,NASA,Spacecraft,Earth sciences,Spaceflight,Outer space,Science,Space science,Astronautics,Sky,Flight]
---


On Sept. 21, 2014, NASA scientists and engineers launched RapidScat toward the orbiting International Space Station, 250 miles above the Earth's surface, with a few objectives in mind: improve weather forecasting on Earth, provide cross-calibration for all international satellites that monitor ocean winds, and improve estimates of how ocean winds change throughout the day, around the globe. Using the existing data and power services of the station and hardware initially built as a spare for QuikScat, RapidScat not only recreated its predecessors work, but greatly improved the timeliness of data transmissions and the potential for cross-calibration of other sensors designed to measure sea-surface winds. RapidScat observations played an essential role in National Oceanic and Atmospheric Administration (NOAA) weather forecasting by aiding in their ability to measure temporal changes of wind fields and allowing them to study changes within one hour in high latitudes, compared to six hours with previous instruments. Wind speed is not only important in the prediction of bad weather and investigation of global wind circulation patterns, but also helps organizations like NASA to plan launches, flights and landings of space- and aircrafts. This paved the way to measure and compare data from various scatterometers, together providing wind measurements multiple times each day, giving meteorologists and other scientists a better look into how storms developed.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/nsc-gwt112916.php){:target="_blank" rel="noopener"}


