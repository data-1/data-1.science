---
layout: post
title: "Learning during development is regulated by an unexpected brain region"
date: 2017-10-18
categories:
author: Netherlands Institute for Neuroscience - KNAW
tags: [Critical period,Thalamus,Visual perception,Brain,Cerebral cortex,Neuroplasticity,News aggregator,Cognitive science,Interdisciplinary subfields,Behavioural sciences,Nervous system,Mental processes,Psychological concepts,Cognition,Neuroscience]
---


Half a century of research on how the brain learns to integrate visual inputs from the two eyes has provided important insights in critical period regulation, leading to the conclusion that it occurs within the cortex. Scientists have now made the surprising discovery that a brain region that passes on input from the eyes to the cortex also plays a crucial role in opening the critical period of binocular vision. Half a century of research on how the brain learns to integrate visual inputs from the two eyes has provided important insights in critical period regulation, leading to the conclusion that it occurs within the cortex. Neuroscientist Christiaan Levelt and his team now made the surprising discovery that a brain region that passes on input from the eyes to the cortex also plays a crucial role in opening the critical period of binocular vision. Levelt: To improve developmental problems resulting in learning problems during critical periods, reinstating flexibility in the visual cortex may not be sufficient.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171016122153.htm){:target="_blank" rel="noopener"}


