---
layout: post
title: "Fermilab experiment sees neutrinos change over 500 miles"
date: 2016-05-16
categories:
author: Fermi National Accelerator Laboratory
tags: [NOvA,Neutrino,Fermilab,T2K experiment,Neutrino oscillation,Muon,MINOS,Particle physics,Physical sciences,Astrophysics,Neutrinos,Science,Leptons,Physics]
---


The longer of the two tracks in each view is identified as a high-energy electron, telling us that this is likely an electron neutrino interaction. Researchers have collected data aggressively since February 2014, recording neutrino interactions in the 14,000-ton far detector in Ash River, Minnesota, while construction was still under way. This video shows the precision of the far detector, spotting neutrino interactions amidst the cosmic ray noise. NOvA, which will take data for at least six years, is seeing nearly equivalent results in a shorter time frame, something that bodes well for the experiment's ambitious goal of measuring neutrino properties that have eluded other experiments so far. Go behind the scenes of the creation of the NOvA far detector in this time-lapse video showing how one of the biggest experiments ever designed was built.

<hr>

[Visit Link](http://phys.org/news/2015-08-fermilab-neutrinos-miles.html){:target="_blank" rel="noopener"}


