---
layout: post
title: "Unique high-resolution map on bat diversity in Africa"
date: 2016-01-29
categories:
author: University Of Twente
tags: [Biodiversity,Research,Endemism,Ecology,Species,Geographic information system,Map,Information,Branches of science,Earth sciences,Science]
---


The researchers have created state-of-the-art species distribution models (SDMs) for a large taxonomic group and demonstrated that by stacking these, a plausible model of fine-grained continental species diversity and endemism patterns can be obtained despite often scarce and biased occurrence data (the so-called 'Wallacean shortfall'). Centers of endemism (hotspots of summed range size rarity) are mostly found in or near areas characterized by substantial elevational ranges – on tropical mountains often at higher elevations than hotspots of species richness. Further deployment of the approach  The approach in general, and the presented model in particular, should prove valuable for a range of applications because the maps of African bat diversity and endemism presented constitute one of the few published datasets featuring high spatial resolution, large geographic extent, and broad taxonomic scope. Explore further Protected area design secrets revealed in new study  More information: K. Matthias B. Herkt et al. A high-resolution model of bat diversity and endemism for continental Africa, Ecological Modelling (2016). K. Matthias B. Herkt et al. A high-resolution model of bat diversity and endemism for continental Africa,(2016).

<hr>

[Visit Link](http://phys.org/news/2016-01-unique-high-resolution-diversity-africa.html){:target="_blank" rel="noopener"}


