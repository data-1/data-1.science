---
layout: post
title: "On the road to creating an electrodeless spacecraft propulsion engine"
date: 2017-10-08
categories:
author: "Tohoku University"
tags: [Spacecraft propulsion,Plasma propulsion engine,Magnetic nozzle,Rocket engine,Plasma (physics),Spacecraft electric propulsion,Spaceflight,Electromagnetism,Astronomy,Flight,Outer space,Space science]
---


Magnetic fields stretched by plasma flows result in an increase in the field component along the plasma flow. Researchers from Tohoku University have been trying to find out how the plasma flow is influenced by its environment via laboratory experiments, and have made headway on research toward an electrodeless plasma thruster to propel spacecraft. In the same way, the plasma in the propulsive MN essentially diverges the magnetic field. To overcome this problem, researchers propose a scenario in which the magnetic field lines are stretched to infinity by the plasma flow. This result might imply that the plasma flow can direct the magnetic field into space while maintaining the thrust generation by the MN.

<hr>

[Visit Link](https://phys.org/news/2017-06-road-electrodeless-spacecraft-propulsion.html){:target="_blank" rel="noopener"}


