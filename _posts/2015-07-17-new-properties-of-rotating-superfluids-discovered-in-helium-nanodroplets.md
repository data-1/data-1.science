---
layout: post
title: "New properties of rotating superfluids discovered in helium nanodroplets"
date: 2015-07-17
categories:
author: University of Southern California 
tags: [Helium,Liquid,Superfluidity,Quantum vortex,Universe,Vortex,Quantum mechanics,Applied and interdisciplinary physics,Physical sciences,Physics,Nature,Physical chemistry,Chemistry,Science,Phases of matter]
---


Scientists use giant laser to examine frigid droplets of liquid helium that stretch the imagination and defy intuition  Liquid helium, when cooled down nearly to absolute zero, exhibits unusual properties that scientists have struggled to understand: it creeps up walls and flows freely through impossibly small channels, completely lacking viscosity. Now, a large, international team of researchers led by scientists at USC, Stanford and Berkeley has used X-rays from a free-electron laser to peer inside individual droplets of liquid helium, exploring whether this liquid helium retains its superfluid characteristics even at microscopic scales – such as in tiny droplets of helium mist. One of the ways that superfluidity manifests is through the formation of quantum vortices, but they have never been experimentally observed in droplets, said Andrey Vilesov, professor of chemistry and physics at the USC Dornsife Collge of Letters, Arts and Sciences and co-corresponding author of the study, which appears in Science on August 22. Now we are able to study this form of matter in a new way that allows us to see how quantum mechanics manifests itself in the motion of an isolated superfluid, Gessner said. Spinning superfluid helium similarly distorts its shape, but also forms a honeycomb consisting of countless quantum vortices, in contrast to the single vortex formed in a normal liquid.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/uosc-npo081914.php){:target="_blank" rel="noopener"}


