---
layout: post
title: "Early human diet explains our eating habits"
date: 2016-06-24
categories:
author: "Norman Owen-Smith, The Conversation"
tags: [Food,Meat,Human,Plant,Paleolithic diet,Neanderthal,Food and drink,Nutrition]
---


The researchers contend that it was digestible starches that provided extra energy needed to fuel the energy needs of bigger brains, rather than extra protein from meat to grow these brains. So are our energy, or protein, needs much different from other mammals of similar size? The extra nutrition that we need for brain work may be counterbalanced, at least partially, by a lesser need for:  a long gut to process poor quality foods, or  a large liver to handle nasty chemicals in these plant parts. The role of carbs among early humans  Meat has long been part of human diets, along with carbohydrates provided by fruits, tubers and grains. The food journey within evolution  Coping with the intensifying dry season in the expanding African savanna was a critical issue for human ancestors during the evolutionary transition from ape-men to the first humans between three and two million years ago.

<hr>

[Visit Link](http://phys.org/news/2015-08-early-human-diet-habits.html){:target="_blank" rel="noopener"}


