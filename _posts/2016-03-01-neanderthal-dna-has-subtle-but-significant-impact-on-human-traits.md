---
layout: post
title: "Neanderthal DNA has subtle but significant impact on human traits"
date: 2016-03-01
categories:
author: Vanderbilt University
tags: [Neanderthal,Human,Early modern human]
---


Our main finding is that Neanderthal DNA does influence clinical traits in modern humans: We discovered associations between Neanderthal DNA and a wide range of traits, including immunological, dermatological, neurological, psychiatric and reproductive diseases, said John Capra, senior author of the paper The phenotypic legacy of admixture between modern humans and Neanderthals published in the Feb. 12 issue of the journal Science. For example, they found that a specific bit of Neanderthal DNA significantly increases risk for nicotine addiction. They also found a number of variants that influence the risk for depression: some positively and some negatively. The data came from eMERGE - the Electronic Medical Records and Genomics Network funded by the National Human Genome Research Institute - which links digitized records from Vanderbilt University Medical Center's BioVU databank and eight other hospitals around the country. By comparing the two sets of data, they could test whether each bit of Neanderthal DNA individually and in aggregate influences risk for the traits derived from the medical records.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/vu-ndh020216.php){:target="_blank" rel="noopener"}


