---
layout: post
title: "Self-healing landscape: Landslides after earthquakes"
date: 2016-05-31
categories:
author: "Helmholtz Association Of German Research Centres"
tags: [Earthquake,Landslide,Earth sciences,Geology]
---


Large ground cracks on a small ridge and landslide in the background after the Nepal quake of April 2015, upper Bhote Koshi river valley Credit: O. Marc, GFZ  In mountainous regions earthquakes often cause strong landslides, which can be exacerbated by heavy rain. These new findings are presented by a German-Franco-Japanese team of geoscientists in the current issue of the journal Geology, under the lead of the GFZ German Research Centre for Geosciences. Even after strong earthquake the activity of landslides returns back over the course of one to four years to the background level before the earthquake. The interactions over time between earthquakes and processing shaping the landscape are still not well understood. We analytically separated the effect of the rain from the seismic activity and so were able to determine that the decrease of landslides through time is based on an internal healing process of the landscape, said Marc Odin.

<hr>

[Visit Link](http://phys.org/news/2015-08-self-healing-landscape-landslides-earthquakes.html){:target="_blank" rel="noopener"}


