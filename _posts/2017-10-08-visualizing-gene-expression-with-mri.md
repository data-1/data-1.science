---
layout: post
title: "Visualizing gene expression with MRI"
date: 2017-10-08
categories:
author: "California Institute of Technology"
tags: [Magnetic resonance imaging,Medical imaging,Clinical medicine,Biotechnology,Medical specialties,Chemistry,Biology,Medicine,Life sciences]
---


Researchers in the laboratory of Mikhail Shapiro, assistant professor of chemical engineering and Heritage Medical Research Institute Investigator, have invented a new method to link magnetic resonance imaging (MRI) signals to gene expression in cells--including tumor cells--in living tissues. We thought that if we could link signals from water molecules to the expression of genes of interest, we could change the way the cell looks under MRI, says Arnab Mukherjee, a postdoctoral scholar in chemical engineering at Caltech and co-lead author on the paper. Shapiro's team realized that increasing the number of aquaporins on a given cell made it stand out in MRI images acquired using a common clinical technique called diffusion-weighted imaging, which is sensitive to the movement of water molecules. This means that when a gene of interest is turned on, the cell will overexpress aquaporin, making the cell look darker under diffusion-weighted MRI. The researchers showed that this technique was successful in monitoring gene expression in a brain tumor in mice.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-12/ciot-vge122216.php){:target="_blank" rel="noopener"}


