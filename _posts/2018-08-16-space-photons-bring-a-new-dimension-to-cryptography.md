---
layout: post
title: "Space photons bring a new dimension to cryptography"
date: 2018-08-16
categories:
author: ""
tags: [Quantum key distribution,Encryption,Computing,Cyberwarfare,Espionage techniques,Applied mathematics,Computer security,Information and communications technology,Electronics,Branches of science,Communication,Cryptography,Computer science,Secure communication,Information Age,Telecommunications,Technology]
---


Applications Space photons bring a new dimension to cryptography 03/05/2018 10860 views 153 likes  ESA and its partners will investigate how satellites can distribute photon ‘keys’ to help secure encryption. Yesterday, ESA signed a contract with SES Techcom S.A. (LU) to develop QUARTZ (Quantum Cryptography Telecommunication System): a platform for quantum key distribution – a next-generation form of cryptography – and administer it from space. However, cutting-edge developments like quantum computers and new code-breaking techniques could leave certain aspects of today’s systems vulnerable to attack, which is why ESA is investigating novel encryption technologies like quantum key distribution (QKD). Studies and experiments by ESA have shown that photons can be collected and distributed in free space, from satellite to satellite, satellite to ground and vice versa. It is part of ESA’s ARTES ScyLight (SeCure and Laser communication Technology) programme, which fosters and supports the development and deployment of innovative optical technologies for satellite communication, and assists industry in developing new market opportunities for optical communication technologies.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Telecommunications_Integrated_Applications/Space_photons_bring_a_new_dimension_to_cryptography){:target="_blank" rel="noopener"}


