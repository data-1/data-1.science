---
layout: post
title: "How lizards regenerate their tails: Researchers discover genetic 'recipe'"
date: 2016-02-05
categories:
author: Arizona State University
tags: [Regeneration (biology),Biology,Life sciences]
---


Researchers have discovered the genetic 'recipe' as to how this happens. Other animals, such as salamanders, frog tadpoles and fish, can also regenerate their tails, with growth mostly at the tip. Arizona State University researchers discovered that green anole lizards turn on at least 326 genes in specific regions of the regenerating tail, including genes involved in embryonic development, response to hormonal signals and wound healing. Just like in mice and humans, lizards have satellite cells that can grow and develop into skeletal muscle and other tissues. By following the genetic recipe for regeneration that is found in lizards, and then harnessing those same genes in human cells, it may be possible to regrow new cartilage, muscle or even spinal cord in the future.

<hr>

[Visit Link](http://phys.org/news327756189.html){:target="_blank" rel="noopener"}


