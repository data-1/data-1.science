---
layout: post
title: "17-million-year-old whale fossil provides first exact date for East Africa's puzzling uplift"
date: 2015-09-10
categories:
author: Southern Methodist University 
tags: [Beaked whale,Earth sciences]
---


Uplift and aridification of East Africa causing changes in vegetation has been considered a driver of human evolution  Uplift associated with the Great Rift Valley of East Africa and the environmental changes it produced have puzzled scientists for decades because the timing and starting elevation have been poorly constrained. It was discovered 740 kilometers inland at an elevation of 620 meters in modern Kenya's harsh desert region, said vertebrate paleontologist Louis L. Jacobs, Southern Methodist University, Dallas. The whale was stranded up river at a time when east Africa was at sea level and was covered with forest and jungle, Jacobs said. Beaked whale fossil surfaced after going missing for more than 30 years  The beaked whale fossil was discovered in 1964 by J.G. From other institutions, authors are Henry Wichura and Manfred R. Strecker, University of Potsdam, and Fredrick K. Manthi, National Museums of Kenya.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/smu-1mw031715.php){:target="_blank" rel="noopener"}


