---
layout: post
title: "New quantum method generates really random numbers"
date: 2018-04-19
categories:
author: "National Institute of Standards and Technology (NIST)"
tags: [Randomness,Quantum mechanics,Random number generation,Quantum entanglement,Photon,Theoretical physics,Branches of science,Scientific method,Scientific theories,Applied mathematics,Physics,Science]
---


The new NIST method generates digital bits (1s and 0s) with photons, or particles of light, using data generated in an improved version of a landmark 2015 NIST physics experiment. In the new work, researchers process the spooky output to certify and quantify the randomness available in the data and generate a string of much more random bits. It's hard to guarantee that a given classical source is really unpredictable, NIST mathematician Peter Bierhorst said. We're very sure we're seeing quantum randomness because only a quantum system could produce these statistical correlations between our measurement choices and outcomes. First, the spooky action experiment generates a long string of bits through a Bell test, in which researchers measure correlations between the properties of the pairs of photons.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180411131625.htm){:target="_blank" rel="noopener"}


