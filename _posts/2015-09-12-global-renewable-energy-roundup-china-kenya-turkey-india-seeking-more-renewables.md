---
layout: post
title: "Global Renewable Energy Roundup: China, Kenya, Turkey, India Seeking More Renewables"
date: 2015-09-12
categories:
author: Bloomberg News Editors, Copyright Bloomberg, Root, --Ppa-Color-Scheme, --Ppa-Color-Scheme-Active
tags: [Solar power,Renewable energy,Hydroelectricity,Wind power,Geothermal energy,Watt,Sustainable technologies,Energy technology,Technology,Natural resources,Climate change mitigation,Economy,Electrical engineering,Sustainable development,Energy and the environment,Electricity,Nature,Physical quantities,Energy,Power (physics),Electric power,Sustainable energy,Renewable resources]
---


China is being encouraged by three industry groups to double the nation’s solar-power goal for 2020 to make up for shortfalls from nuclear and hydropower projects, while India’s installed capacity of solar power could reach 75 GW by 2020. For 2015, China plans to add about 18 gigawatts of solar power capacity, almost equal to all the generating capacity available in the U.S. at the end of 2014. Energy Minister Taner Yildiz said in November that Turkey wanted to increase Turkey’s wind capacity to at least 20,000 megawatts and total installed electricity capacity to 110,000 megawatts by 2023. Solar Power Capacity in India To Reach 75GW by 2022, Report Says  Anindya Upadhyay, Bloomberg, explains that solar will help meet more than a fifth of India’s increase in electricity demand by 2022 as installed capacity reaches 75 gigawatts, according to a research report. China’s solar capacity surged from less than 1 gigawatt in 2010 to almost 33 gigawatts at the end of last year, according to data from Bloomberg New Energy Finance.

<hr>

[Visit Link](http://www.renewableenergyworld.com/articles/2015/08/global-renewable-energy-roundup-china-kenya-turkey-india-seeking-more-renewables.html){:target="_blank" rel="noopener"}


