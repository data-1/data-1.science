---
layout: post
title: "Spacecraft fly through explosive magnetic phenomenon to understand space weather"
date: 2016-05-14
categories:
author: Imperial College London
tags: [Magnetosphere,Space weather,Earth,Geomagnetic storm,Earths magnetic field,Satellite,Solar System,Nature,Spaceflight,Outer space,Astronomy,Space science,Astronautics,Science,Physical sciences]
---


For the first time, spacecraft have flown through the heart of a magnetic process that controls Earth's space weather and geomagnetic storms. When these lines come up against field lines in different orientations - for example from the Sun - a process called magnetic reconnection occurs. On October 16, 2015, the spacecraft travelled straight through a magnetic reconnection event at the boundary where Earth's field lines bump up against the Sun's field lines. MMS is made up of four satellites flown in a precise formation using GPS that keeps them only around 10 kilometres apart. Reconnection events here are expected to be 'more explosive', according to Dr Eastwood, as they brighten the aurora and create magnetic storms.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/icl-sft051116.php){:target="_blank" rel="noopener"}


