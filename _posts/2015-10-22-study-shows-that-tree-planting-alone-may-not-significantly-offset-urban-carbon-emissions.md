---
layout: post
title: "Study shows that tree planting alone may not significantly offset urban carbon emissions"
date: 2015-10-22
categories:
author: University Of Iowa
tags: [Greenhouse gas emissions,Carbon offset,Greenhouse gas,Tree planting,Carbon neutrality,Carbon dioxide,Lidar,Climate change,Environmental impact,Nature,Climate forcing,Environmental issues with fossil fuels,Global environmental issues,Natural environment,Climate variability and change,Societal collapse]
---


Researchers at the University of Iowa examined the amount of carbon generated in two counties in the Twin Cities, Minnesota and then calculated the amount of carbon absorbed by all trees there. Many cities have outlined goals to reduce carbon-dioxide emissions, and obviously tree-planting is one way to achieve that goal, said Chang Zhao, a graduate student in the Geographical and Sustainability Sciences department at the UI and corresponding author on the paper, published in the journal PLOS One, but our study shows it plays a minor role and that we need to focus on reducing carbon emissions over removing them. The researchers calculated carbon emissions per census block—land areas with populations of at least 2,500 people that are used by the U.S. Census. With that information, the researchers were able to detail the amount of carbon generated—which they termed demand for carbon sequestration—in each census block and the amount of carbon stored in the trees—the supply—that would be emitted if the trees were removed. I would say [planting trees] is one strategy that would help with carbon offsets, but we find it's not going to do it on its own, says Heather Sander, assistant professor in geographical and sustainability sciences at the UI and the paper's co-author.

<hr>

[Visit Link](http://phys.org/news/2015-10-tree-significantly-offset-urban-carbon.html){:target="_blank" rel="noopener"}


