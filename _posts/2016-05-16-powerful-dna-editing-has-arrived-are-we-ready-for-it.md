---
layout: post
title: "Powerful DNA 'Editing' Has Arrived, Are We Ready for It?"
date: 2016-05-16
categories:
author: Jeff Bessen
tags: [Genome editing,Cas9,Designer baby,Gene,DNA,CRISPR,Genetics,Biotechnology,Branches of genetics,Biochemistry,Molecular biology,Biology,Life sciences]
---


Editing DNA to cure disease  While most human diseases (opens in new tab) are caused, at least partially, by mutations in our DNA, current therapies treat the symptoms of these mutations but not the genetic root cause. Further, there is a wide gap between our understanding of disease and the genes that might cause them. In March of 2015, a group of researchers and lawyers called for a voluntary pause on further using CRISPR technology in germ line cells until ethical guidelines could be decided. The study also revealed off-target DNA cutting and incomplete editing among all the cells of a single embryo. In 1975, a group that came to be known as the Asilomar Conference called for caution with an emerging technology called recombinant DNA until its safety could be evaluated and ethical guidelines could be published.

<hr>

[Visit Link](http://www.livescience.com/51776-powerful-dna-editing-has-arrived-are-we-ready-for-it.html){:target="_blank" rel="noopener"}


