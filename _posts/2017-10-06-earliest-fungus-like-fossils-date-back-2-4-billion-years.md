---
layout: post
title: "Earliest Fungus-Like Fossils Date Back 2.4 Billion Years"
date: 2017-10-06
categories:
author: "Jerry Redfern"
tags: [Fungus,Fossil,Nature,Organisms,Biology]
---


An international group of scientists says it has discovered 2.4 billion-year-old fungus-like fossils — approximately 2 billion years older than any previous fungal specimen and a billion or more years earlier than scientists currently think fungi first evolved. The structures were found in tiny bubbles and voids within the lava that generally fill with other minerals within 10 million years of forming, Bengtson said, meaning the fossils would be approximately the same age as the rock. It is possible, according to Bengtson, that an organism other than fungi formed the structures. This is the second major announcement in ancient evolutionary research from Bengtson and the Swedish Museum of Natural History in two months. Originally published on Seeker.

<hr>

[Visit Link](http://www.livescience.com/58808-earliest-fungus-like-fossils-found.html){:target="_blank" rel="noopener"}


