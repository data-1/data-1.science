---
layout: post
title: "Researchers identify key components of the cellular engine in living cells"
date: 2015-10-29
categories:
author: National University Of Singapore
tags: [Vinculin,Talin (protein),Focal adhesion,Cell adhesion,Actin,Cytoskeleton,Cell migration,Cell (biology),Nutrients,Biochemistry,Proteins,Macromolecules,Molecular biology,Cell biology]
---


The researchers showed that force-dependent vinculin binding to talin plays a critical role in mechanically connecting the actin cytoskeleton to the extracellular substrate to contribute towards cell migration. This understanding of the fundamental machinery that drives living cells movement is crucial in paving the way for physiological and pathological processes in modern medicine, ranging from normal tissue development to treating cardiovascular disorders and cancer metastasis. Inside the cell, focal adhesions are physically linked to a network of filaments, composed of a protein called actin, that form near the protruding cell membrane. For cells to move forward, cells have to transmit force from dynamic actin filaments to the extracellular substrate through focal adhesions. Although the actin filaments no longer move inwards, they are still growing near the cell membrane.

<hr>

[Visit Link](http://phys.org/news327221001.html){:target="_blank" rel="noopener"}


