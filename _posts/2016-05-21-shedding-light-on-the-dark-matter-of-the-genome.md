---
layout: post
title: "Shedding light on the 'dark matter' of the genome"
date: 2016-05-21
categories:
author: University of Toronto
tags: [Non-coding RNA,RNA,Transcription (biology),Non-coding DNA,Gene,Small nucleolar RNA,Messenger RNA,Genome,Genetics,Molecular biophysics,Chemistry,Nucleosides,Cell biology,Biomolecules,Molecular biology,Biochemistry,Biotechnology,Biology,Branches of genetics,Structural biology,Nucleotides,Life sciences,Molecular genetics,Nucleic acids]
---


Now, Professor Benjamin Blencowe's team at the University of Toronto's Donnelly Centre, including lead authors Eesha Sharma and Tim Sterne-Weiler, have developed a method, described in May 19, 2016 issue of Molecular Cell, that enables scientists to explore in depth what ncRNAs do in human cells. The genes are copied, or transcribed, into messenger RNA (mRNA) molecules, which provide templates for building proteins that do most of the work in the cell. However, it is emerging that many ncRNAs have important roles in gene regulation. When two RNA molecules have matching sequences - strings of letters copied from the DNA blueprint - they will stick together like Velcro. We would like to understand how ncRNAs function during development.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/uot-slo051916.php){:target="_blank" rel="noopener"}


