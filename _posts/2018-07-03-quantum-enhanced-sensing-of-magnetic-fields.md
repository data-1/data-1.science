---
layout: post
title: "Quantum-enhanced sensing of magnetic fields"
date: 2018-07-03
categories:
author: "ETH Zurich Department of Physics"
tags: [Magnetometer,Quantum computing,Magnetic field,News aggregator,Quantum mechanics,Magnetism,Physics,Science,Theoretical physics,Branches of science,Technology,Applied mathematics]
---


Pursuing a different route, a team including ETH physicists Andrey Lebedev and Gianni Blatter, together with colleagues in Finland and Russia, highlight another branch of technology where quantum devices promise unique benefits, and that with considerably more modest hardware resources. The so-called transmon qubit is currently one of the leading candidates for a building block of large-scale quantum computers, not least as it offers numerous freedoms for engineering the circuits in ways that suit the problem at hand. In addition to providing a strong coupling to a magnetic field, the transmon qubit has a defining property of a quantum system on offer: coherent superpositions of quantum states. Similarly, the design of the hardware used in these experiments draws on experience in building qubits for quantum computers. This combination of harnessing quantum hardware and quantum algorithms in the context of quantum sensing provides an appealing route towards novel devices that, ultimately, promise to push the sensitivity of single- or few-qubit magnetometers towards and beyond the limits of current magnetic-field sensors.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180702120442.htm){:target="_blank" rel="noopener"}


