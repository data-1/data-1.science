---
layout: post
title: "6 differences between agile and traditional planning"
date: 2018-03-30
categories:
author: "Ranjith Varakantam
(Red Hat)"
tags: [Leadership,Team,Risk,Goal,Advertising,Thought,Organization,Well-being,Innovation,Mindset,Employment,Branches of science,Business,Behavioural sciences]
---


Six differences between traditional and agile planning  1. The agile approach allows the team to find their own way towards achieving specific goals. The collaborative leadership style encourages the team to fail fast and fail forward to learn and lay out a dynamic path to reach its key objectives and goals. Investing in the team's overall wellbeing and growth  The focus of traditional project planning is to deliver a project based on scope, quality, and schedule. Individual team members' growth and wellbeing is not directly relevant to traditional project managers; this responsibility is traditionally allocated to others in the organization.

<hr>

[Visit Link](https://opensource.com/article/18/3/traditional-vs-agile-planning){:target="_blank" rel="noopener"}


