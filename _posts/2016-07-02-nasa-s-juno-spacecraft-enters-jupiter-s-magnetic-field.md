---
layout: post
title: "NASA's Juno spacecraft enters Jupiter's magnetic field"
date: 2016-07-02
categories:
author: "Jet Propulsion Laboratory"
tags: [Juno (spacecraft),Magnetosphere,Magnetosphere of Jupiter,Jupiter,Solar wind,Bow shock,JEDI,Astronomy,Astronomical objects,Planets of the Solar System,Spaceflight,Physical sciences,Planetary science,Astronomical objects known since antiquity,Science,Bodies of the Solar System,Solar System,Outer space,Space science]
---


This chart presents data the Waves investigation on NASA's Juno spacecraft recorded as the spacecraft crossed the bow shock just outside of Jupiter's magnetosphere on July 24, 2016. Credit: NASA/JPL-Caltech/SwRI/Univ. of Iowa  NASA's Jupiter-bound Juno spacecraft has entered the planet's magnetosphere, where the movement of particles in space is controlled by what's going on inside Jupiter. Data from Juno's Waves investigation, presented as audio stream and color animation, indicate the spacecraft's crossing of the bow shock just outside the magnetosphere on June 24 and the transit into the lower density of the Jovian magnetosphere on June 25. This chart presents data that the Waves investigation on NASA's Juno spacecraft recorded as the spacecraft entered Jupiter's magnetosphere on July 25, 2016, while approaching Jupiter. Credit: NASA/JPL-Caltech/SwRI/MSSS  While this transition from the solar wind into the magnetosphere was predicted to occur at some point in time, the structure of the boundary between those two regions proved to be unexpectedly complex, with different instruments reporting unusual signatures both before and after the nominal crossing.

<hr>

[Visit Link](http://phys.org/news/2016-07-nasa-juno-spacecraft-jupiter-magnetic.html){:target="_blank" rel="noopener"}


