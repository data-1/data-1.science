---
layout: post
title: "Scientists produce strongest evidence yet of schizophrenia's causes"
date: 2016-05-07
categories:
author: Cardiff University
tags: [Mental disorder,Genetics,Schizophrenia,Mutation,Copy number variation,Health,Brain,Risk factors of schizophrenia,Human diseases and disorders,Mental disorders,Diseases and disorders,Clinical medicine,Health sciences,Mental health,Neuroscience,Medicine]
---


An international team of scientists led by Cardiff University researchers has provided the strongest evidence yet of what causes schizophrenia -- a condition that affects around 1% of the global population. Published in the journal Neuron, their work presents strong evidence that disruption of a delicate chemical balance in the brain is heavily implicated in the disorder. Researchers studying psychiatric disorders have previously suspected that disruption of this balance contributes to schizophrenia. The first evidence that schizophrenia mutations interfere with excitatory signalling was uncovered in 2011 by the same team, based at Cardiff University's MRC Centre for Neuropsychiatric Genetics and Genomics. Comparing the CNVs found in people with schizophrenia to those found in unaffected people, the team was able to show that the mutations in individuals with the disorder tended to disrupt genes involved in specific aspects of brain function.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150603123946.htm){:target="_blank" rel="noopener"}


