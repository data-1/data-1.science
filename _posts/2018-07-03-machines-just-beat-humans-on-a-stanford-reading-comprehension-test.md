---
layout: post
title: "Machines Just Beat Humans on a Stanford Reading Comprehension Test"
date: 2018-07-03
categories:
author: ""
tags: [Artificial intelligence,Alibaba Group,E-commerce,Technology,Information technology,Information Age,Communication,Computing,Branches of science,Cyberspace]
---


Read Me  Chinese retail giant Alibaba has developed an artificial intelligence model that's managed to outdo human participants in a reading and comprehension test designed by Stanford University. The Stanford Question Answering Dataset is a set of 10,000 questions pertaining to some 500 Wikipedia articles. The answer to each question is a particular span of text from the corresponding piece of writing. Both Alibaba and Microsoft's entries may have only beaten human participants by a whisker, but this is still a huge accomplishment when it comes to machine reading. Alibaba's AI  In October 2017, Alibaba announced its plans to invest $15 billion in emerging technologies such as artificial intelligence, quantum computing, and the Internet of Things.

<hr>

[Visit Link](https://futurism.com/machines-beat-humans-stanford-reading-comprehension-test/){:target="_blank" rel="noopener"}


