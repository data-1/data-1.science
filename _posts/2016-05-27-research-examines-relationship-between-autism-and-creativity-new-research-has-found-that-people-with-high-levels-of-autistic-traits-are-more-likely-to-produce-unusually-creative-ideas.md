---
layout: post
title: "Research examines relationship between autism and creativity: New research has found that people with high levels of autistic traits are more likely to produce unusually creative ideas"
date: 2016-05-27
categories:
author: University of East Anglia
tags: [Autism,Creativity,Autism spectrum,Thought,News aggregator,Cognitive psychology,Neuroscience,Branches of science,Cognitive science,Cognition,Psychology,Behavioural sciences,Psychological concepts]
---


New research has found that people with high levels of autistic traits are more likely to produce unusually creative ideas. While they found that people with high autistic traits produced fewer responses when generating alternative solutions to a problem - known as 'divergent thinking' - the responses they did produce were more original and creative. The research, published today in the Journal of Autism and Developmental Disorders, looked at people who may not have a diagnosis of autism but who have high levels of behaviours and thought processes typically associated with the condition. People with autistic traits may approach creativity problems in a different way, said Dr Doherty. In other words, the associative or memory-based route to being able to think of different ideas is impaired, whereas the specific ability to produce unusual responses is relatively unimpaired or superior.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150813222253.htm){:target="_blank" rel="noopener"}


