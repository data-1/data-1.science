---
layout: post
title: "NASA Issues $2.8 Million in Contracts to Design a Power Solution for the Deep Space Gateway"
date: 2018-06-10
categories:
author: ""
tags: [Lunar Gateway,NASA,Flight,Life in space,NASA programs,Discovery and exploration of the Solar System,Space industry,Astronomy,Spaceflight,Outer space,Astronautics,Spaceflight technology,Spacecraft,Space programs,Space science,Space vehicles,Space exploration,Space program of the United States,Human spaceflight,Aerospace]
---


The companies will investigate means of powering the gateway and propelling it around the Moon for the next four months. NASA wants this propulsion module to be one of the first components of the Deep Space Gateway sent into orbit. Roscosmos confirmed its intention to participate in September 2017, and it's thought that the station's docking system will be one of the projects spearheaded by the agency. The Deep Space Gateway will be an important component of that long-term plan. In 2030, another SLS rocket will ferry supplies and the crew for Mars mission, ahead of the final stage – the voyage to the red planet itself.

<hr>

[Visit Link](https://futurism.com/nasa-issues-million-contracts-design-power-solution-deep-space-gateway/){:target="_blank" rel="noopener"}


