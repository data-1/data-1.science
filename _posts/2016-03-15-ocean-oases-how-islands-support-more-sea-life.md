---
layout: post
title: "Ocean oases: How islands support more sea-life"
date: 2016-03-15
categories:
author: Bangor University
tags: [Coral reef,Phytoplankton,Atoll,Ocean,Marine biology,Human impact on the environment,Pelagic zone,Climate change,Marine life,Oceanography,Biogeography,Fisheries science,Aquatic ecology,Earth sciences,Applied and interdisciplinary physics,Biogeochemistry,Nature,Environmental science,Hydrology,Ecology,Systems ecology,Hydrography,Natural environment,Physical geography]
---


The question of how such a productive system exists in such a seemingly unproductive environment became known as Darwin's paradox. The Island Mass Effect is a hypothesis explaining why waters surrounding small islands, reefs and atolls support a greater abundance of sea-life than is found in the near-by open ocean. They recorded up to 86% more phytoplankton in these waters than is found in open oceans. Human activity also feeds into the cycle. From an understanding of the drivers of phytoplankton production in the tropics, we can begin to explore how this productivity may become altered under future climate change scenarios such as altered ocean circulation patterns and what the biological knock-on effects may be, particularly to local fisheries.

<hr>

[Visit Link](http://phys.org/news/2016-02-ocean-oases-islands-sea-life.html){:target="_blank" rel="noopener"}


