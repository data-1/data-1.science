---
layout: post
title: "Bioengineering a functional vascularized lung scaffold: New approach to bioengineering lungs selectively treats epithelium that lines the lung airway, while preserving lung vasculature"
date: 2017-09-10
categories:
author: "Columbia University School of Engineering and Applied Science"
tags: [Tissue engineering,Lung,Decellularization,Organ transplantation,Respiratory epithelium,Regeneration in humans,Medical specialties,Clinical medicine,Medicine,Life sciences,Anatomy,Biology,Health sciences]
---


Efforts to bioengineer functional lungs from fully decellularized or synthetic scaffolds that lack functional vasculature have been largely unsuccessful until now. A Columbia Engineering team led by Gordana Vunjak-Novakovic, University Professor and the Mikati Foundation Professor at Columbia Engineering and professor of medical sciences (in Medicine) at Columbia University, and N. Valerio Dorrello, assistant professor of pediatrics at Columbia University Medical Center, is the first to successfully bioengineer a functional lung with perfusable and healthy vasculature in an ex vivo rodent lung. We reasoned that an ideal lung scaffold would need to have perfusable and healthy vasculature, and so we developed a method that maintains fully functional lung vasculature while we remove defective epithelial lining of the airways and replace it with healthy therapeutic cells. Lung transplantation is the only definitive treatment we have for patients with end-stage lung disease, but there is a severe shortage of donor organs -- only 20 percent of potential donor lungs are acceptable for transplantation and this leads to many potentially avoidable deaths on the waiting list. This work was supported by the National Institutes of Health (P41 EB002520 to G.V.-N and U01HL134760 to H-W.S and G.V.-N.), Driscoll Children's Fund and Hearts of ECMO (Extracorporeal Membrane Oxygenation) (to N.V.D.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/08/170830141259.htm){:target="_blank" rel="noopener"}


