---
layout: post
title: "Synthetic DNA vaccine against MERS induces immunity in animal study"
date: 2016-05-27
categories:
author: University of Pennsylvania School of Medicine
tags: [Vaccine,MERS,DNA vaccine,Immune system,Infection,Coronavirus,Inovio Pharmaceuticals,Virus,Health sciences,Medical specialties,Immunology,Clinical medicine,Medicine,Microbiology,Health,Infectious diseases,Public health,Causes of death,Diseases and disorders,Life sciences,Epidemiology,Virology,Health care,Biology]
---


A novel synthetic DNA vaccine can, for the first time, induce protective immunity against the Middle East Respiratory Syndrome (MERS) coronavirus in animal species, reported researchers from the Perelman School of Medicine at the University of Pennsylvania. The recent 2015 outbreak in South Korea was of great concern as the infection spread from a single patient to infect more than 181 people, resulting in hospital closings, severe economic impact, and more than 30 deaths. In addition, the vaccine induced antibodies that are linked with protection in camels, a species that is thought to be a major source of transmission to humans in the Middle East, showing that this vaccine could be deployed to break this this link in the MERS transmission cycle. In the field, say the researchers, this vaccine could decrease person-to-person spread of infection in the event of an outbreak and help to protect health care workers or exposed individuals. Collaborators include researchers from the National Institute of Allergy and Infectious Disease, the Public Health Agency of Canada, Inovio Pharmaceuticals, the University of Washington, and the University of South Florida.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150819143637.htm){:target="_blank" rel="noopener"}


