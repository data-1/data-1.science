---
layout: post
title: "'Spectacular' image shows planet formation in action – Physics World"
date: 2016-03-29
categories:
author: Hamish Johnston
tags: [Atacama Large Millimeter Array,European Southern Observatory,Planet,Star,Astronomical objects,Physical sciences,Space science,Astronomy,Outer space,Planetary science,Science,Solar System,Stellar astronomy]
---


This one image alone will revolutionize theories of planet formation”. Then, material in the disc will join up to create large structures such as asteroids, comets and planets. The HL Tauri system appears to be at the point in its evolution when the nascent planets have acquired enough mass to “sweep out” smaller objects from their orbits, creating the observed structure of rings. “Images with this level of detail have, up to now, been relegated to computer simulations or artist’s impressions.”  Spaced-out antennas  The spatial resolution of the image is about five times the distance between the Earth and the Sun, which is better than could be achieved by the Hubble Space Telescope. When connected together, the array functions as a giant radio telescope with a resolution that is much better than an individual antenna.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/b-wrBwHDx3w/spectacular-image-shows-planet-formation-in-action){:target="_blank" rel="noopener"}


