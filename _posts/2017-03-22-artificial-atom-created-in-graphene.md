---
layout: post
title: "'Artificial atom' created in graphene"
date: 2017-03-22
categories:
author: "Vienna University of Technology"
tags: [Graphene,Quantum dot,Electron,Atom,Energy level,Quantum mechanics,Atomic molecular and optical physics,Atomic physics,Building engineering,Nanotechnology,Technology,Condensed matter,Theoretical physics,Physical sciences,Chemical product engineering,Electromagnetism,Condensed matter physics,Physical chemistry,Physics,Materials,Chemistry,Applied and interdisciplinary physics,Materials science]
---


Such additional properties have now been shown for artificial atoms in the carbon material graphene. Building Artificial Atoms  Artificial atoms open up new, exciting possibilities, because we can directly tune their properties, says Professor Joachim Burgdörfer (TU Wien, Vienna). Just like in an atom, where the electrons can only circle the nucleus on certain orbits, electrons in these quantum dots are forced into discrete quantum states. That way, a tiny region is created within the graphene surface, in which low energy electrons can be trapped. The new artificial atoms now open up new possibilities for many quantum technological experiments: Four localized electron states with the same energy allow for switching between different quantum states to store information, says Joachim Burgdörfer.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/vuot-ac082216.php){:target="_blank" rel="noopener"}


