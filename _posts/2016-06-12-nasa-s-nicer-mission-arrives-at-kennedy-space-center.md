---
layout: post
title: "NASA's NICER mission arrives at Kennedy Space Center"
date: 2016-06-12
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Neutron Star Interior Composition Explorer,Neutron star,Star,Pulsar,International Space Station,Goddard Space Flight Center,Spaceflight,Astronomy,Space science,Outer space,Physical sciences,Science,Nature]
---


An upcoming NASA astrophysics mission will uncover the physics governing the ultra-dense interiors of neutron stars. The forthcoming International Space Station (ISS) payload was transported from NASA's Goddard Space Flight Center in Greenbelt, Maryland, aboard a climate-controlled, air-suspension truck. Crushed by its own gravity, the star's core collapses and forms a neutron star. NICER will exploit these pulsations to perform cutting-edge astrophysics investigations while another aspect of the mission - the Station Explorer for X-ray Timing and Navigation Technology (SEXTANT) project - demonstrates a technological first: real-time, autonomous spacecraft navigation using pulsars as beacons, ultimately furthering deep space exploration into the solar system and beyond. NICER is planned for launch from Cape Canaveral Air Force Station in Florida aboard the SpaceX-11 ISS Commercial Resupply Services flight, currently scheduled for February 2017.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/nsfc-nnm060816.php){:target="_blank" rel="noopener"}


