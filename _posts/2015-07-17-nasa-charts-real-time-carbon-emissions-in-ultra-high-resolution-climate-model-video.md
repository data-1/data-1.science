---
layout: post
title: "NASA Charts Real-Time Carbon Emissions in Ultra High-Resolution Climate Model (VIDEO)"
date: 2015-07-17
categories:
author: Lidija Grozdanic
tags: []
---


NASA just launched a new high-resolution computer model that gives a stunning look at the yearly movement of carbon emissions through the planet’s atmosphere. NASA created the model by combining ground-based measurements with data received from the Orbiting Carbon Observatory-2 (OCO-2) which was launched in July. The simulation is one of the highest resolution models ever created, and it clearly shows major emission sources and the way winds disperse greenhouse gases. The carbon dioxide visualization was produced by a computer model called GEOS-5, created by scientists at NASA Goddard’s Global Modeling and Assimilation Office. SIGN UP  The simulation took 75 days to compute and it produced nearly four petabytes of data.

<hr>

[Visit Link](http://inhabitat.com/nasa-charts-real-time-carbon-emissions-in-ultra-high-resolution-climate-model-video/){:target="_blank" rel="noopener"}


