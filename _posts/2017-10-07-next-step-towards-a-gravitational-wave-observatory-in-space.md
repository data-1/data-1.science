---
layout: post
title: "Next step towards a gravitational-wave observatory in space"
date: 2017-10-07
categories:
author: "European Space Agency"
tags: [Laser Interferometer Space Antenna,Gravitational wave,Astronomy,Space science,Physical sciences,Science,Physics,Astrophysics,Outer space,Physical cosmology,Astronomical objects]
---


Now, following the first detection of the elusive waves with ground-based experiments and the successful performance of ESA's LISA Pathfinder mission, which demonstrated some of the key technologies needed to detect gravitational waves from space, the agency is inviting the scientific community to submit proposals for the first space mission to observe gravitational waves. Meanwhile, the LISA Pathfinder mission was launched in December 2015 and started its scientific operations in March this year, testing some of the key technologies that can be used to build a space observatory of gravitational waves. In particular, low-frequency gravitational waves are linked to even more exotic cosmic objects than their higher-frequency counterparts: supermassive black holes, with masses of millions to billions of times that of the Sun, that sit at the centre of massive galaxies. The planned launch date for the mission is 2034. Explore further What happens when black holes collide?

<hr>

[Visit Link](http://phys.org/news/2016-10-gravitational-wave-observatory-space.html){:target="_blank" rel="noopener"}


