---
layout: post
title: "Physicists developing quantum-enhanced sensors for real-life applications"
date: 2018-07-03
categories:
author: "University Of Oklahoma"
tags: [Science,Technology,Computing]
---


A University of Oklahoma physicist, Alberto M. Marino, is developing quantum-enhanced sensors that could find their way into applications ranging from biomedical to chemical detection. The team presents the first implementation of a sensor with sensitivities considered state-of-the-art and shows how quantum-enhanced sensing can find its way into real-life applications. Quantum resources can enhance the sensitivity of a device beyond the classical shot noise limit and, as a result, revolutionize the field of metrology through the development of quantum enhanced sensors, said Marino, a professor in the Homer L. Dodge Department of Physics and Astronomy, OU College of Arts and Sciences. This makes it possible to obtain a quantum-based enhancement of the sensitivity. Explore further Quantum trick blocks background 'chatter' in sensing devices

<hr>

[Visit Link](https://phys.org/news/2018-05-physicists-quantum-enhanced-sensors-real-life-applications.html){:target="_blank" rel="noopener"}


