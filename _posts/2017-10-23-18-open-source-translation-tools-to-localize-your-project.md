---
layout: post
title: "18 open source translation tools to localize your project"
date: 2017-10-23
categories:
author: "Jeff Beatty"
tags: [Language localisation,Computer-assisted translation,Translation,Postediting,Translation memory,Internationalization and localization,Red Hat,Open source,Version control,Information Age,Systems engineering,Computing,Technology,Software engineering,Information science,Applied linguistics,Language,Software,Linguistics,Information technology,Human communication,Software development,Computer science,Information technology management]
---


Open source projects looking to localize into many languages and streamline their localization processes will want to look at open source tools to save money and get the flexibility they need with customization. Tools to check out:  Translation management systems (TMS)  opensource.com  TMS tools are web-based platforms that allow you to manage a localization project and enable translators and reviewers to do what they do best. Tools to check out  Terminology management tools  opensource.com  Terminology management tools give you a GUI to create terminology resources (known as termbases) to add context and ensure translation consistency. Tools to check out  Localization automation tools  opensource.com  Localization automation tools facilitate the way you process localization data. Tools to check out  Why open source is key  Localization is most powerful and effective when done in the open.

<hr>

[Visit Link](https://opensource.com/article/17/6/open-source-localization-tools){:target="_blank" rel="noopener"}


