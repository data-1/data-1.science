---
layout: post
title: "Proteomics identifies DNA repair toolbox"
date: 2015-05-20
categories:
author: ""    
tags: [Proteomics,DNA,Protein,DNA repair,DNA replication,Cancer,Coral reef,Genomics,Macromolecules,Clinical medicine,Molecular biology,Biochemistry,Biology,Biotechnology,Genetics,Life sciences]
---


Proteomics identifies DNA repair toolbox    by Staff Writers    Munich, Germany (SPX) May 09, 2015    Collision of the DNA replication machinery with lesions in the DNA triggers the recruitment of a large number of DNA repair factors (yellow) that help to repair the lesions. Proteomic profiles (right) indicate the maximal accumulation of DNA replication and repair factors on the DNA. To answer this question, scientists in the team of Matthias Mann at the Max Planck Institute (MPI) of Biochemistry in Martinsried near Munich, with colleagues in Copenhagen and at Harvard, have analyzed how the protein composition of the DNA replication machinery changes upon encountering damaged DNA. To monitor such changes, they isolated DNA at several time points during the replication and repair process, and quantified the bound proteins using mass spectrometry based proteomics. They work particularly well if the cancer cells they attack already have defects in the corresponding DNA repair pathways, as it frequently occurs in breast cancer and other tumors.

<hr>

[Visit Link](http://www.terradaily.com/reports/Proteomics_identifies_DNA_repair_toolbox_999.html){:target="_blank" rel="noopener"}


