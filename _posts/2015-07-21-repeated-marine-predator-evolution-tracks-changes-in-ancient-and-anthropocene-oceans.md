---
layout: post
title: "Repeated marine predator evolution tracks changes in ancient and Anthropocene oceans"
date: 2015-07-21
categories:
author: Smithsonian 
tags: [Convergent evolution,Evolution,Marine life,Marine vertebrate,Tetrapod,Animal echolocation,Species,Predation,Whale,Biology,Nature,Animals]
---


The paper also highlights how evolutionary history informs an understanding of the impact of human activities on marine species today. In some cases, similar anatomy evolved among lineages that adapted to marine lifestyles. For example, modern dolphins and extinct marine reptiles called ichthyosaurs descended from distinct terrestrial species, but independently converged on an extremely similar fish-like body plan although they were separated in time by more than 50 million years. The repeated transformation of legs adapted for walking on land into fins is another classic example of convergent evolution. They intend that this comprehensive review will encourage future collaboration between researchers across scientific fields and lead to new insights about evolutionary biology, paleontology and marine conservation.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/s-rmp041315.php){:target="_blank" rel="noopener"}


