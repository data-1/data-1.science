---
layout: post
title: "CMS releases more than one petabyte of open data"
date: 2018-07-02
categories:
author: "Corinne Pralavorio"
tags: [Compact Muon Solenoid,CERN,Large Hadron Collider,Higgs boson,Science,Particle physics,Technology,Physics]
---


CMS researchers have recorded petabytes of data from collisions at the LHC and have so far published hundreds of scientific papers with them. By releasing the data into the public domain, researchers outside the CMS Collaboration have the opportunity to conduct novel research with them. Although the plots appear similar, the analysis with CMS Open Data uses more data (at 8 TeV and overall) than the official CMS one from the original discovery but is a lot less sophisticated and is not scrutinised by the wider CMS community of experts. At the moment, CMS has committed to releasing up to 50% of each year's recorded data a few years after they were collected, once CMS scientists finish most of their analysis of these datasets. Explore further CERN CMS releases 300 terabytes of research data from LHC

<hr>

[Visit Link](https://phys.org/news/2017-12-cms-petabyte.html){:target="_blank" rel="noopener"}


