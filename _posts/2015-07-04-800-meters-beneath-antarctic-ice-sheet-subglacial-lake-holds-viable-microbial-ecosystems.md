---
layout: post
title: "800 meters beneath Antarctic ice sheet, subglacial lake holds viable microbial ecosystems"
date: 2015-07-04
categories:
author: National Science Foundation 
tags: [Subglacial lake,Lake Vostok,Antarctica,Hydrography,Environmental engineering,Oceanography,Earth phenomena,Hydrology,Natural environment,Environmental science,Nature,Physical geography,Earth sciences]
---


In a finding that has implications for life in other extreme environments, both on Earth and elsewhere in the solar system, researchers funded by the National Science Foundation (NSF) this week published a paper confirming that the waters and sediments of a lake that lies 800 meters (2,600 feet) beneath the surface of the West Antarctic ice sheet support viable microbial ecosystems. Other co-authors on the paper include students and researchers from LSU; the University of Venice in Italy; New York University; the Scripps Institution of Oceanography; St. Olaf College in Minnesota; the University of Tennessee; and Aberystwyth University in the United Kingdom. The WISSARD team made scientific and engineering history in late January of 2013 when they used clean hot-water drilling technology to access subglacial Lake Whillans. The latest WISSARD announcement is the first to provide definitive evidence that a functional microbial ecosystem exists beneath the Antarctic ice sheet, confirming more than a decade of speculation about life in this environment. The largest of the subglacial lakes, subglacial Lake Vostok in East Antarctica, is one of the largest lakes on our planet in terms of volume and depth and has been isolated beneath the ice sheet for more than 10 million years.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/nsf-8mb082114.php){:target="_blank" rel="noopener"}


