---
layout: post
title: "Physicists introduce another quantum animal: The Quantum Pigeon"
date: 2015-05-22
categories:
author: Chapman University
tags: [Quantum mechanics,Time,Physics,Time travel,Force,Albert Einstein,Mechanics,Science,Physical sciences,Theoretical physics,Applied and interdisciplinary physics,Concepts in metaphysics,Scientific theories]
---


There are different types of non-locality which quantum mechanics showed could not exist in classical physics. It's remarkable that it's still possible to discover something fundamentally new about quantum mechanics, which has been around for nearly 100 years. But even with this experience, we are already making assumptions about the nature of time on a microscopic scale: the past that is not here, a present that is now here and the future that will be here later. If we could have such time machines, then when they take us back in time, a paradox will ensue: if one goes back in time in a time machine and kills one's grandfather, then that individual would never be born. This is the paradox that happens when we say that you are allowed to come back from the future and effect the past or the present.

<hr>

[Visit Link](http://phys.org/news326465672.html){:target="_blank" rel="noopener"}


