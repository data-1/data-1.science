---
layout: post
title: "Mapping the unknown: What is the function of non-coding RNA in plants?"
date: 2016-03-16
categories:
author: University of Copenhagen - Faculty of Science
tags: [Non-coding DNA,DNA,RNA,Gene,Biotechnology,Molecular biology,Biochemistry,Life sciences,Nucleic acids,Branches of genetics,Molecular genetics,Nucleotides,Biology,Genetics]
---


Postdoc Peter Kindgren from Copenhagen Plant Science Centre at University of Copenhagen has received an EU Marie Curie grant to examine whether non-coding RNA play an important role in plants' ability to cope with temperature changes. However, very little of the DNA in higher organisms are protein coding genes. Plants have a sophisticated defense system  Together with his colleagues in the Marquardt lab at Copenhagen Plant Science Centre, Peter Kindgren studies, if non-coding RNA sequences are vital components in the complex regulation system in plants. Non-coding RNA could play an important role in this. ###  Copenhagen Plant Science Centre  Post doc Peter Kindgren is part of the Marquardt Laboratory at the Copenhagen Plant Science Centre with focus on plant biology and plant biotechnology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/fos--mtu030116.php){:target="_blank" rel="noopener"}


