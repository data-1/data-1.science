---
layout: post
title: "6 ways that scientists are hacking the planet"
date: 2018-06-02
categories:
author: "Greg Beach"
tags: []
---


As much as humanity has altered this planet in ways that are harmful to itself and other species, some humans are now attempting to hack the planet, in big ways and small, for the good of us all. Related: The world’s tallest active geyser keeps erupting in Yellowstone – and scientists don’t know why  Researchers at NASA have proposed drilling into the magma chamber and adding water to cool it down, thereby preventing an eruption. A ‘Spray-on Umbrella’ to Protect Coral Reefs  Coral reefs around the world are under severe pressure, with up to one-quarter of all reefs worldwide already considered too damaged to be saved. A Chemical Sunshade  As global temperatures continue to rise and climate change fundamentally alters ecosystems around the world, scientists are considering what some may see as drastic measures to correct a global climate spiraling into chaos. In this case, public works officials and workers in Los Angeles have figured out a way to hack the light spectrum by painting its streets white to reduce heat absorption.

<hr>

[Visit Link](https://inhabitat.com/6-ways-that-scientists-are-hacking-the-planet){:target="_blank" rel="noopener"}


