---
layout: post
title: "New species evolve faster as mountains form"
date: 2017-09-10
categories:
author: "Field Museum"
tags: [Biodiversity,Species,Mountain,Plant,Fossil,Alps,Himalayas,American Association for the Advancement of Science,Nature,Earth sciences,Science,Natural environment]
---


Our research provides the strongest evidence yet that when mountains form, new species evolve and diversify at an increased rate. Among global biodiversity hotspots, it's unusual in not having a tropical or Mediterranean climate. The team found that new species were formed at an increased rate within the Hengduan Mountains as they were forming, compared to species formation rates in the surrounding regions--evidence that species form faster as mountains are uplifted. It's the first study that brings evidence from lots of different groups to bear on this question in a quantitative framework. Why are there more species here than there?

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/fm-nse033117.php){:target="_blank" rel="noopener"}


