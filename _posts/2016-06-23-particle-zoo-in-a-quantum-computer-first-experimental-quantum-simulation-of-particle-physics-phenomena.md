---
layout: post
title: "Particle zoo in a quantum computer: First experimental quantum simulation of particle physics phenomena"
date: 2016-06-23
categories:
author: "University Of Innsbruck"
tags: [Particle physics,Physics,Standard Model,Elementary particle,Quantum mechanics,Scientific theories,Applied and interdisciplinary physics,Science,Physical sciences,Theoretical physics]
---


Researchers simulated the creation of elementary particle pairs out of the vacuum by using a quantum computer. Simulation of particle-antiparticle pairs using a quantum computer  Gauge theories describe the interaction between elementary particles, such as quarks and gluons, and they are the basis for our understanding of fundamental processes. Combining different fields of physics  With this experiment, the physicists in Innsbruck have built a bridge between two different fields in physics: They have used atomic physics experiments to study questions in high-energy physics. Experimental physicist Rainer Blatt adds: Moreover, we can study new processes by using quantum simulation. For example, in our experiment we also investigated particle entanglement produced during pair creation, which is not possible in a particle collider.

<hr>

[Visit Link](http://phys.org/news/2016-06-particle-zoo-quantum-experimental-simulation.html){:target="_blank" rel="noopener"}


