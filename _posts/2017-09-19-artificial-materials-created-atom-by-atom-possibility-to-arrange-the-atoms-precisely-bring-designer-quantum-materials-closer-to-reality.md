---
layout: post
title: "Artificial materials created atom-by-atom: Possibility to arrange the atoms precisely bring designer quantum materials closer to reality"
date: 2017-09-19
categories:
author: "Aalto University"
tags: [Atom,Scanning tunneling microscope,Microscope,Physical chemistry,Materials science,Quantum mechanics,Theoretical physics,Chemistry,Physics,Science,Physical sciences,Applied and interdisciplinary physics]
---


Researchers at Aalto University have manufactured artificial materials with engineered electronic properties. By arranging atoms in a lattice, it becomes possible to engineer the electronic properties of the material through the atomic structure. Working at a temperature of four degrees Kelvin, the researchers used a scanning tunnelling microscope (STM) to arrange vacancies in a single layer of chlorine atoms supported on a copper crystal. The correspondence between atomic structure and electronic properties is of course what happens in real materials as well, but here we have complete control over the structure. Using their atomic assembly method, the research team demonstrated complete control by creating two real-life structures inspired by fundamental model systems with exotic electronic properties.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/03/170327164933.htm){:target="_blank" rel="noopener"}


