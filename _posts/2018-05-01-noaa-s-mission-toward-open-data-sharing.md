---
layout: post
title: "NOAA’s Mission Toward Open Data Sharing"
date: 2018-05-01
categories:
author: "Esther Shein"
tags: [Linux,National Oceanic and Atmospheric Administration,Cloud computing,Linux Foundation,Computing,Technology]
---


Not surprisingly, NOAA officials have a hard time moving it around and managing it, Kearns said. Data Sharing  NOAA is a big consumer of open source and sharing everything openly is part of the organization’s modus operandi. By adopting some modern licensing practices, he believes the NOAA could actually share even more information with the public. This gave the agency the ability to handle over a billion hits a day during the peak hurricane season. But, he continued, “we are still … just starting to get into how to adopt some of these more modern technologies to do our job better.”  Equal Access  Now the NOAA is looking to find a way to make the data available to an even wider group of people and make it more easily understood.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/noaas-mission-toward-open-data-sharing/){:target="_blank" rel="noopener"}


