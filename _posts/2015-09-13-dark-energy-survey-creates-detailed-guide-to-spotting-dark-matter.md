---
layout: post
title: "Dark Energy Survey creates detailed guide to spotting dark matter"
date: 2015-09-13
categories:
author: Fermi National Accelerator Laboratory
tags: [Dark matter,Void (astronomy),Galaxy,Dark energy,Universe,Dark Energy Survey,Astronomy,Nature,Physics,Science,Physical cosmology,Physical sciences,Space science,Cosmology,Astrophysics]
---


Mass map with images of two galaxy clusters and a cosmic void  Scientists on the Dark Energy Survey have released the first in a series of dark matter maps of the cosmos. Vikram, Chang and their collaborators at Penn, ETH Zurich, the University of Portsmouth, the University of Manchester and other DES institutions worked for more than a year to carefully validate the lensing maps. So far, the DES analysis backs this up: The maps show large filaments of matter along which visible galaxies and galaxy clusters lie and cosmic voids where very few galaxies reside. Zooming into the maps, we have measured how dark matter envelops galaxies of different types and how together they evolve over cosmic time. Explore further Scientists find rare dwarf satellite galaxy candidates in Dark Energy Survey data  More information: View the Dark Energy Survey analysis: View the Dark Energy Survey analysis: deswl.github.io/page1/vikram_p … er/vikram_paper.html

<hr>

[Visit Link](http://phys.org/news348157217.html){:target="_blank" rel="noopener"}


