---
layout: post
title: "Satellites map photosynthesis at high resolution: Precise measurement of the solar-induced chlorophyll fluorescence enables scientists to quantify gross primary production"
date: 2017-10-13
categories:
author: "GFZ GeoForschungsZentrum Potsdam, Helmholtz Centre"
tags: [Photosynthesis,Primary production,Orbiting Carbon Observatory 2,Orbiting Carbon Observatory,Earth sciences,Physical sciences,Nature]
---


Life on Earth is impossible without photosynthesis. They used data of the NASA satellite OCO-2 (Orbiting Carbon Observatory 2) to map the so-called solar-induced chlorophyll fluorescence (SIF) at a much higher spatial resolution than possible from any other space instrument. Among the authors are two researchers from Germany, Martin Jung from the Max Planck Institute for Biogeochemistry (MPI-BGC) in Jena und Luis Guanter from the Helmholtz Centre Potsdam -- GFZ German Research Centre for Geosciences (GFZ). Until the launch of the OCO-2 satellite mission in 2014 we had global maps of SIF but at coarse spatial resolution, each pixel compromising areas of about 50 x 50 kilometres in the best case, says Luis Guanter from GFZ. They allow us to look into relationships between SIF and the gross primary production (GPP) -- the amount of carbon fixed by plants through photosynthesis -- at scales never explored before.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171012143407.htm){:target="_blank" rel="noopener"}


