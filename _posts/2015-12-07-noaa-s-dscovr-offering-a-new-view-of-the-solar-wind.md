---
layout: post
title: "NOAA's DSCOVR: Offering a new view of the solar wind"
date: 2015-12-07
categories:
author: "$author" 
tags: [Deep Space Climate Observatory,Sun,Goddard Space Flight Center,Solar wind,Plasma (physics),Advanced Composition Explorer,Parker Solar Probe,Space weather,Nature,Spaceflight,Bodies of the Solar System,Applied and interdisciplinary physics,Solar System,Science,Physics,Physical sciences,Outer space,Astronomy,Space science]
---


NOAA will use DSCOVR to monitor the solar wind and forecast space weather at Earth -- effects from the material and energy from the sun that can impact our satellites and technological infrastructure on Earth. There the solar wind is denser and its physical processes faster, so the Faraday cup will be built to measure at a rate of 100 times per second. When we proposed the DSCOVR Faraday cup we saw it as a pathfinder for the Solar Probe Plus mission, said Kasper, who is also the instrument lead for the Faraday cup on Solar Probe Plus. This type of magnetometer will also be on Solar Probe Plus. The Earth science data will be processed at NASA's DSCOVR Science Operations Center and archived and distributed by NASA's Atmospheric Science Data Center.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/nsfc-ndo020615.php){:target="_blank" rel="noopener"}


