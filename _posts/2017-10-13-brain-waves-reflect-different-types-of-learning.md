---
layout: post
title: "Brain waves reflect different types of learning"
date: 2017-10-13
categories:
author: "Massachusetts Institute of Technology"
tags: [Learning,Henry Molaison,Amnesia,Implicit learning,Memory,Neural oscillation,Brain,Interdisciplinary subfields,Psychology,Mental processes,Cognition,Cognitive science,Cognitive psychology,Neuroscience,Behavioural sciences]
---


advertisement  Brain waves from earlier studies  When the MIT researchers studied the behavior of animals learning different tasks, they found signs that different tasks might require either explicit or implicit learning. During explicit learning tasks, there was an increase in alpha2-beta brain waves (oscillating at 10-30 hertz) following a correct choice, and an increase delta-theta waves (3-7 hertz) after an incorrect choice. The increase in alpha-2-beta brain waves during explicit learning could reflect the building of a model of the task, Miller explains. By contrast, delta-theta rhythms only increased with correct answers during an implicit learning task, and they decreased during learning. But Miller says that the brain wave study indicates a lot of overlap in these two systems.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171012122820.htm){:target="_blank" rel="noopener"}


