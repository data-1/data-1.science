---
layout: post
title: "What is A dwarf planet?"
date: 2016-05-27
categories:
author: Matt Williams
tags: [Dwarf planet,Planet,Clearing the neighbourhood,IAU definition of planet,Pluto,Solar System,Trans-Neptunian object,Eris (dwarf planet),Kuiper belt,Planetary science,Astronomical objects,Planemos,Bodies of the Solar System,Astronomy,Planets,Space science,Outer space,Substellar objects,Physical sciences,Local Interstellar Cloud,Dwarf planets,Celestial mechanics]
---


Nevertheless, the IAU currently recognizes five bodies within our solar system as dwarf planets, six more could be recognized in the coming years, and as many as 200 or more could exist within the expanse of the Kuiper Belt. However, rotation can also affect the shape of a dwarf planet. Thus, in 2011, Stern still referred to Pluto as a planet and accepted other dwarf planets such as Ceres and Eris, as well as the larger moons, as additional planets. However, other astronomers have countered this opinion by saying that, far from not having cleared their orbits, the major planets completely control the orbits of the other bodies within their orbital zone. Another point of contention is the application of this new definition to planets outside of the solar system.

<hr>

[Visit Link](http://phys.org/news/2015-08-dwarf-planet.html){:target="_blank" rel="noopener"}


