---
layout: post
title: "Light from ancient quasars helps confirm quantum entanglement: Results are among the strongest evidence yet for 'spooky action at a distance'"
date: 2018-08-22
categories:
author: "Massachusetts Institute of Technology"
tags: [Quantum entanglement,Bells theorem,Quantum mechanics,Photon,Scientific theories,Physics,Applied and interdisciplinary physics,Science,Physical sciences,Theoretical physics]
---


Such what-ifs are known to physicists as loopholes to tests of Bell's inequality, the most stubborn of which is the freedom-of-choice loophole: the possibility that some hidden, classical variable may influence the measurement that an experimenter chooses to perform on an entangled particle, making the outcome look quantumly correlated when in fact it isn't. The researchers used distant quasars, one of which emitted its light 7.8 billion years ago and the other 12.2 billion years ago, to determine the measurements to be made on pairs of entangled photons. The Earth is about 4.5 billion years old, so any alternative mechanism -- different from quantum mechanics -- that might have produced our results by exploiting this loophole would've had to be in place long before even there was a planet Earth, let alone an MIT, adds David Kaiser, the Germeshausen Professor of the History of Science and professor of physics at MIT. Meanwhile, researchers at a station located between the two telescopes created pairs of entangled photons and beamed particles from each pair in opposite directions toward each telescope. For each run, they measured 17,663 and 12,420 pairs of entangled photons, respectively.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180820114121.htm){:target="_blank" rel="noopener"}


