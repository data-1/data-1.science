---
layout: post
title: "Photos: Primordial Sea Scorpion Was a Top Predator"
date: 2015-09-02
categories:
author: Laura Geggel
tags: [Pentecopterus]
---


During the Ordovician period, these creatures likely scuttled along the seafloor as juveniles, but grew up to be top predators swimming around the ocean. (Image credit: Patrick Lynch | Yale University)  Excavation site  The Iowa Geological Survey discovered the fossils during a mapping project of the Upper Iowa River. Amazing appendages  The appendages of a juvenile P. decorahensis. Appendage spines  The researchers found remains of both juvenile and adult specimens. As they age, the spines get really long, said study lead researcher James Lamsdell, a postdoctoral associate of paleontology at Yale University.

<hr>

[Visit Link](http://www.livescience.com/52038-photos-ancient-sea-scorpion.html){:target="_blank" rel="noopener"}


