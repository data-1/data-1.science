---
layout: post
title: "Explorers Will Dive Beneath Antarctic Ice Shelf, Looking for Life"
date: 2017-10-31
categories:
author: Tom Metcalfe
tags: [Antarctica,Ross Ice Shelf,Scott Base,Underwater diving,Ice,McMurdo Station,McMurdo Sound,Earth sciences,Oceanography,Physical geography]
---


The expedition members hope to dive beneath the ice up to four times a day. [In Photos: Diving Beneath Antarctica's Ross Ice Shelf]  In addition to their scientific studies and diving duties beneath the ice shelf, the Finnish team members are responsible for shooting the 360-degree video and keeping the world informed through a series of regular updates, photographs and videos on their Facebook page. (Image credit: Patrick Degerman)  Joanna Norkko told Live Science in an email that the expedition team would spend about 20 days diving and taking samples from the sea floor at New Harbour before moving on to a second site on the ice at Cape Evans on Ross Island, about 18 miles (30 km) from Scott Base. [Life on the Edge: Photos from Drilling the Ross Ice Shelf]  The expedition scientists hope to complete up to four dives a day beneath Antarctica's Ross Ice Shelf (Image credit: Patrick Degerman)  The objective of this diving expedition is to examine the sensitivity of Antarctic seafloor communities to climate change, Norkko said. Safety while diving under the ice will be a top priority, she said: Whenever two divers enter the water, one safety diver will be suited up on the surface and ready to jump in immediately if there is a problem.

<hr>

[Visit Link](https://www.livescience.com/60802-explorers-dive-beneath-antarctic-ice.html){:target="_blank" rel="noopener"}


