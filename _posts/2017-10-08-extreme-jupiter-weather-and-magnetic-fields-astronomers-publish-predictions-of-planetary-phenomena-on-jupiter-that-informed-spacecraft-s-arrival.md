---
layout: post
title: "Extreme Jupiter weather and magnetic fields: Astronomers publish predictions of planetary phenomena on Jupiter that informed spacecraft's arrival"
date: 2017-10-08
categories:
author: "University of Leicester"
tags: [Juno (spacecraft),Jupiter,Aurora,Planetary science,Sun,Solar wind,Bodies of the Solar System,Planets,Sky,Astronomical objects,Space science,Science,Physical sciences,Nature,Solar System,Outer space,Astronomy]
---


These collaborations provide the Juno science team with a 'forecast' of the gas giant's intense weather systems and powerful aurorae to compare with Juno close observations. This model, based upon spacecraft flybys and Galileo orbiter observations, details the electric currents which couple the polar upper atmosphere to the planetary field and plasma at large distances, and offers a comparison of Juno's early data with a prediction of what Juno would observe on its first 'perijove'. This is much deeper than we can see with Earth- or space-based telescopes. The University of Leicester is home to the UK science lead for the Juno mission, NASA's programme to study our solar system's largest planet, Jupiter. Planetary scientists and astronomers from the Department of Physics and Astronomy are studying the gas giant's magnetosphere, dynamic atmosphere and its beautiful polar auroras.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/05/170525145844.htm){:target="_blank" rel="noopener"}


