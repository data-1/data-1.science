---
layout: post
title: "Platform uses artificial intelligence to diagnose Zika and other pathogens"
date: 2018-07-18
categories:
author: "Fundação de Amparo à Pesquisa do Estado de São Paulo"
tags: [Infection,Zika fever,Biomarker,Virus,Reverse transcription polymerase chain reaction,Clinical medicine,Medical specialties,Life sciences,Biology,Diseases and disorders,Medicine]
---


Another strength of the platform, he added, is the capacity to identify positive cases of Zika even in blood serum analyzed 30 days after the start of infection, when the acute phase of the disease is over. All the data obtained in the spectrometry analysis of both the group that tested positive for Zika and the control group were then fed into a computer program running a random-forest machine learning algorithm. Each new set of patient data fed into the program enhances its learning capacity and makes it more sensitive, he went on. The UNICAMP group is currently performing tests to evaluate the platform's capacity to diagnose systemic diseases caused by fungi. In the cloud  In theory, any laboratory equipped with a mass spectrometer could use the new diagnostic platform developed at UNICAMP.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/fda-pua061218.php){:target="_blank" rel="noopener"}


