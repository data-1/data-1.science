---
layout: post
title: "Study finds surprising variability in shape of Van Allen Belts"
date: 2016-03-15
categories:
author: DOE/Los Alamos National Laboratory
tags: [Van Allen radiation belt,Van Allen Probes,Electron,Science,Physics,Nature,Physical sciences,Outer space,Astronomy,Space science]
---


The authors of the study, from Los Alamos National Laboratory and the New Mexico Consortium, found that the inner belt--the smaller belt in the classic picture of the belts--is much larger than the outer belt when observing electrons with low energies, while the outer belt is larger when observing electrons at higher energies. Often, the outer electron belt expands inwards toward the inner belt during geomagnetic storms, completely filling in the slot region with lower-energy electrons and forming one huge radiation belt. The twin Van Allen Probes satellites expand the range of energetic electron data we can capture. But the Van Allen Probes measure hundreds. Despite the proton noise, the Van Allen Probes can unambiguously identify the energies of the electrons they're measuring, said Reeves.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/danl-sfs022416.php){:target="_blank" rel="noopener"}


