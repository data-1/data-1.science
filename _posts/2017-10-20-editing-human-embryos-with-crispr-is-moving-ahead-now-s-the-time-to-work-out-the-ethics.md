---
layout: post
title: "Editing Human Embryos With CRISPR Is Moving Ahead – Now’s The Time To Work Out The Ethics"
date: 2017-10-20
categories:
author:  
tags: [CRISPR gene editing,Technology,Assisted reproductive technology,Genetic engineering,Designer baby,General Electric,Genetics]
---


Evidence of more successful (and thus safer) CRISPR use may lead to additional studies involving human embryos.First, this study did not entail the creation of “designer babies,” despite some news headlines. These questions also involve deciding who gets to set the limits and control access to the technology.We may also be concerned about who gets to control the subsequent research using this technology. Should there be state or federal oversight? How should we regulate access to embryo editing for serious diseases? The announcement of the Oregon study is only the next step in a long line of research.

<hr>

[Visit Link](http://www.ge.com/reports/editing-human-embryos-crispr-moving-ahead-nows-time-work-ethics/){:target="_blank" rel="noopener"}


