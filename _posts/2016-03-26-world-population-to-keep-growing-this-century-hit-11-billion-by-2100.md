---
layout: post
title: "World population to keep growing this century, hit 11 billion by 2100"
date: 2016-03-26
categories:
author: University Of Washington
tags: [Statistics,Forecasting,Population,Branches of science,Science]
---


The number of people on Earth is likely to reach 11 billion by 2100, the study concludes, about 2 billion higher than some previous estimates. Most of the anticipated growth is in Africa, where population is projected to quadruple from around 1 billion today to 4 billion by the end of the century. Other regions of the world are projected to see less change. World population projections are based mostly on two things: future life expectancy and fertility rates. The new method uses statistical models to narrow the range, finding an 80 percent probability that the population in 2100 will be between 9.6 billion and 12.3 billion.

<hr>

[Visit Link](http://phys.org/news330252479.html){:target="_blank" rel="noopener"}


