---
layout: post
title: "ESA selects three new mission concepts for study"
date: 2018-08-16
categories:
author: ""
tags: [THESEUS (satellite),SPICA (spacecraft),Star,Outer space,Scientific observation,Astrophysics,Physics,Nature,Astronomical objects,Science,Physical sciences,Space science,Astronomy]
---


Science & Exploration ESA selects three new mission concepts for study 07/05/2018 25299 views 111 likes  A high-energy survey of the early Universe, an infrared observatory to study the formation of stars, planets and galaxies, and a Venus orbiter are to be considered for ESA’s fifth medium class mission in its Cosmic Vision science programme, with a planned launch date in 2032. The three candidates, the Transient High Energy Sky and Early Universe Surveyor (Theseus), the SPace Infrared telescope for Cosmology and Astrophysics (Spica), and the EnVision mission to Venus were selected from 25 proposals put forward by the scientific community. Each of the selected proposals has high scientific value, and would ensure a continuation of Europe’s expertise in the fields of planetary science, astrophysics and cosmology,” says Günther Hasinger, ESA Director of Science. Herschel’s view of new stars and molecular clouds How did the first stars and galaxies form? A comparison of terrestrial planets Why did Earth and Venus evolve so differently?

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/ESA_selects_three_new_mission_concepts_for_study){:target="_blank" rel="noopener"}


