---
layout: post
title: "Researchers engineer CRISPR to edit single RNA letters in human cells"
date: 2017-10-26
categories:
author:  
tags: [CRISPR gene editing,DNA repair,Genome editing,RNA,Molecular biology,Nucleic acids,Branches of genetics,Molecular genetics,Biochemistry,Genetics,Biotechnology,Biology,Life sciences]
---


The Broad Institute and MIT scientists who first harnessed CRISPR for mammalian genome editing have engineered a new molecular system for efficiently editing RNA in human cells. RNA editing, which can alter gene products without making changes to the genome, has profound potential as a tool for both research and disease treatment. REPAIR has the ability to reverse the impact of any pathogenic G-to-A mutation regardless of its surrounding nucleotide sequence, with the potential to operate in any cell type. The team further modified the editing system to improve its specificity, reducing detectable off-target edits from 18,385 to only 20 in the whole transcriptome. “The success we had engineering this system is encouraging, and there are clear signs REPAIRv2 can be evolved even further for more robust activity while still maintaining specificity,” says Omar Abudayyeh, co-first author and a graduate student in Zhang’s lab.

<hr>

[Visit Link](http://news.mit.edu/2017/researchers-engineer-crispr-edit-single-rna-letters-human-cells-1015){:target="_blank" rel="noopener"}


