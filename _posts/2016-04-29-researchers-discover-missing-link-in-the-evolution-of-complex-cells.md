---
layout: post
title: "Researchers discover missing link in the evolution of complex cells"
date: 2016-04-29
categories:
author: Uppsala University
tags: [Lokiarchaeota,Archaea,Microorganism,Biological evolution,Organisms,Biology,Nature]
---


The origin of these complex cell types has long been a mystery to the scientific community, but now researchers from Uppsala University in Sweden have discovered a new group of microorganisms that represents a missing link in the evolutionary transition from simple to complex cells. The puzzle of the origin of the eukaryotic cell is extremely complicated, as many pieces are still missing. The data simply looked spectacular, says Thijs Ettema at the Department of Cell and Molecular Biology, Uppsala University, who lead the scientific team that carried out the study. By studying its genome, we found that Loki represents an intermediate form in-between the simple cells of microbes, and the complex cell types of eukaryotes, says Thijs Ettema. Extreme environments generally contain a lot of unknown microorganisms, which we refer to as microbial dark matter, says Jimmy Saw, researcher at Department of Cell and Molecular Biology, Uppsala University, and co-lead author of the paper.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/uu-rdm050415.php){:target="_blank" rel="noopener"}


