---
layout: post
title: "Linux Foundation introduces new Linux certifications"
date: 2015-06-06
categories:
author: Steven Vaughan-Nichols, Senior Contributing Editor, Aug.
tags: []
---


The Linux Foundation introduced an answer at LinuxCon: a new Linux Foundation Certification Program for both early-career and engineer-level systems administrators. Linux Foundation certifications  The new Certification Program exams and designations for Linux Foundation Certified System Administrator (LFCS) and Linux Foundation Certified Engineer (LFCE) will demonstrate that users are technically competent through a performance-based exam that is available online. Virtual, available anytime, anywhere in the world: The certification tests are designed to be secure exams that can be taken by anyone anywhere with a Web browser, microphone, and webcam. Performance-based exams: Exam takers will be tested on their ability to solve real problems in the command line rather than be tested on theory or be given multiple choice questions. With this, and other Linux Foundation classes, the Linux Foundation Certification Program is designed to expand the talent pool of Linux professionals worldwide.

<hr>

[Visit Link](http://www.zdnet.com/linux-foundation-introduces-new-linux-certifications-7000032791/#ftag=RSS510d04f){:target="_blank" rel="noopener"}


