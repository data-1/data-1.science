---
layout: post
title: "Autonomous car completes 3,400-mile US road trip"
date: 2015-04-03
categories:
author: Bydee-Ann Durbin
tags: [Self-driving car,Collision avoidance system,Radar,Transportation engineering,Transport,Vehicles,Technology]
---


The car drove itself 99 percent of the time, Delphi said Thursday. The person sitting in the car's driver seat intervened once when traffic was weaving around in a construction zone, and again when the car didn't want to move into a busy left lane to avoid police stopped on the right shoulder. But for the most part, it easily navigated bridges, traffic circles and open highways, even in heavy rain, Owens said. Delphi will have to further train its cameras to detect all kinds of lane markings, since that's one way autonomous cars keep themselves centered in a lane, Owens said. Explore further Autonomous car prepares for 3,500-mile US road trip  © 2015 The Associated Press.

<hr>

[Visit Link](http://phys.org/news347211535.html){:target="_blank" rel="noopener"}


