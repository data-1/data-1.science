---
layout: post
title: "Study of birds' sense of smell reveals important clues for behavior and adaptation"
date: 2016-05-16
categories:
author: Oxford University Press
tags: [Sense of smell,Bird,Animals]
---


Now, a large comparative genomic study of the olfactory genes tied to a bird's sense of smell has revealed important differences that correlate with their ecological niches and specific behaviors. The study suggest that specific OR genes are used not only to detect a range of chemicals governing a bird's ability to smell, but that in birds, specialized olfactory skills, such as those found in birds of prey or aquatic birds, was mirrored by the genetic diversity of their OR gene families. In the study, drastic expansion of specific OR's gene families, such as OR51 and OR52, were seen in sea turtles and aquatic birds, which the author's proposed are use to detect water-loving (hydrophilic) compounds. Overall, different ecological partitioning and specialized groups of birds, such as vocal learners, birds of prey, water birds and land birds, was strongly correlated with OR differences suggesting that the OR families may contribute towards olfactory adaptations. For example, birds of prey, including vultures and seabirds, hunt and recognize food by smell, and have relatively large olfactory bulbs, whereas song birds that rely more on cognitive abilities helpful in tool making, vocal learning and feeding innovations have reduced olfactory bulb sizes.

<hr>

[Visit Link](http://phys.org/news/2015-07-birds-reveals-important-clues-behavior.html){:target="_blank" rel="noopener"}


