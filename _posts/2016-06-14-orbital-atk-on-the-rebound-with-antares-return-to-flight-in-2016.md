---
layout: post
title: "Orbital ATK on the rebound with Antares return to flight in 2016"
date: 2016-06-14
categories:
author: "Ken Kremer"
tags: [Antares (rocket),Commercial Resupply Services,Cygnus Orb-3,Cygnus (spacecraft),NK-33,RD-191,Private spaceflight,Commercial launch service providers,Aerospace,Vehicles,Space launch vehicles,Rockets and missiles,Human spaceflight,Private spaceflight companies,Technology,Spacecraft manufacturers,Aerospace companies,Commercial spaceflight,Spacecraft,Space organizations,Space industry,Spaceflight technology,Astronautics,Rocketry,Outer space,Flight,Space access,Space program of the United States,Space programs,Spaceflight,Space vehicles]
---


Return to flight launch is expected sometime during Spring 2016. Credit: NASA/ Terry Zaperach  Orbital ATK is on the rebound with return to flight of their Antares rocket slated in early 2016 following the catastrophic launch failure that doomed the last Antares in October 2014 on a resupply mission for NASA to the International Space Station (ISS). Pre-launch seaside panorama of Orbital Sciences Corporation Antares rocket at the NASA’s Wallops Flight Facility launch pad on Oct 26 – 2 days before the Orb-3 launch failure on Oct 28, 2014. Credit: Ken Kremer – kenkremer.com  However, the Cygnus missions were put on hold when the third operational Antares/Cygnus flight was destroyed in a raging inferno about 15 seconds after liftoff on the Orb-3 mission from launch pad 0A at NASA's Wallops Flight Facility on Virginia's eastern shore. Orbital ATK engineering teams have been working diligently on integrating and testing the new RD-181 main engines. Engineers and technicians have now integrated the two RD-181 engines with a newly designed and built thrust frame adapter and modified first stage airframe. Antares rocket raised at NASA Wallops launch pad 0A bound for the ISS on Sept 18, 2013. Credit: Ken Kremer (kenkremer.com)  SpaceX, NASA's other commercial cargo company under contract to ship supplies to the ISS also suffered a launch failure of with their Falcon 9/Dragon cargo delivery rocket on June 28, 2015.

<hr>

[Visit Link](http://phys.org/news/2015-08-orbital-atk-rebound-antares-flight.html){:target="_blank" rel="noopener"}


