---
layout: post
title: "Improving mid-infrared imaging and sensing"
date: 2018-07-01
categories:
author: "David L. Chandler"
tags: [Optics,Infrared,Lens,Light,Microscope,Applied and interdisciplinary physics,Electrodynamics,Chemistry,Materials science,Physical chemistry,Radiation,Waves,Materials,Physical phenomena,Electrical engineering,Technology,Natural philosophy,Physical sciences,Science,Atomic molecular and optical physics,Electromagnetism,Electromagnetic radiation]
---


A new way of taking images in the mid-infrared part of the spectrum, developed by researchers at MIT and elsewhere, could enable a wide variety of applications, including thermal imaging, biomedical sensing, and free-space communication. “This kind of metasurface can be made using standard microfabrication techniques,” Gu says. They provide nearly arbitrary wavefront manipulation that’s not possible with natural materials at larger scales, but they have a tiny fraction of the thickness, and thus only a tiny amount of material is needed. They have demonstrated the technique on 6-inch wafers with high throughput, a standard in microfabrication, and “we’re looking at even larger-scale manufacturing.”  The devices transmit 80 percent of the mid-IR light with optical efficiencies up to 75 percent, representing significant improvement over existing mid-IR metaoptics, Gu says. These techniques allow the creation of metaoptical devices, which can manipulate light in more complex ways than what can be achieved using conventional bulk transparent materials, Gu says.

<hr>

[Visit Link](http://news.mit.edu/2018/improving-mid-infrared-imaging-and-sensing-0426){:target="_blank" rel="noopener"}


