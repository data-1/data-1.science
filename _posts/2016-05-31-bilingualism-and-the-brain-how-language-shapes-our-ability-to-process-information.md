---
layout: post
title: "Bilingualism and the brain: How language shapes our ability to process information"
date: 2016-05-31
categories:
author: "Singapore Management University"
tags: [Multilingualism,Monolingualism,Socioeconomic status,Language,Cognition,Executive functions,News aggregator,Attention deficit hyperactivity disorder,Cognitive science,Interdisciplinary subfields,Behavioural sciences,Neuroscience,Cognitive psychology,Psychology,Branches of science,Linguistics]
---


There, speaking two languages is limited mostly to those with high socioeconomic status. I study bilingual children, and sometimes even infants raised in a bilingual context. advertisement  Professor Yang's work with children has already seen results, however. Professor Yang found that low socioeconomic status children who spoke two languages performed much better in behavioural tests than their monolingual counterparts. Surprisingly, we found that even bilingual infants from low socioeconomic status demonstrated greater cognitive development than monolingual infants of the same status.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150824114907.htm){:target="_blank" rel="noopener"}


