---
layout: post
title: "NASA soil moisture mission begins science operations"
date: 2015-05-20
categories:
author: Steve Cole
tags: [Soil Moisture Active Passive,Goddard Space Flight Center,Earth sciences,Space science,Outer space,Spaceflight,Science]
---


High-resolution global soil moisture map from SMAP's combined radar and radiometer instruments, acquired between May 4 and May 11, 2015, during SMAP's commissioning phase. Credit: NASA/JPL-Caltech/GSFC  NASA's new Soil Moisture Active Passive (SMAP) mission to map global soil moisture and detect whether soils are frozen or thawed has begun science operations. Bottom: combined radar and radiometer data with a resolution of 5.6 miles (9 kilometers). SMAP data will eventually reveal how soil moisture conditions are changing over time in response to climate and how this impacts regional water availability, said Dara Entekhabi, SMAP science team leader at the Massachusetts Institute of Technology in Cambridge. A few days before SMAP collected data over the central and southern United States on April 27, intense rainstorms pounded northern Texas.

<hr>

[Visit Link](http://phys.org/news351324934.html){:target="_blank" rel="noopener"}


