---
layout: post
title: "Primates have been infected with viruses related to HIV for 16 million years"
date: 2016-05-28
categories:
author: "PLOS"
tags: [TRIM5alpha,Simian immunodeficiency virus,Antiviral drug,HIV,Virus,Lentivirus,Life sciences,Virology,Medical specialties,Microbiology,Biology,Biotechnology,Genetics,Biological evolution,Molecular biology]
---


A study published on August 20th in PLOS Pathogens of antiviral gene sequences in African monkeys suggests that lentiviruses closely related to HIV have infected primates in Africa as far back as 16 million years. TRIM5 is part of a group of antiviral genes called restriction factors, which have evolved to protect host cells from infection by viruses. Because of its unique specificity for retroviruses (whereas other restriction factors often have broader antiviral activity), the researchers hypothesized that the evolution of TRIM5 in African monkeys should reveal selection by lentiviruses closely related to modern SIVs. They identified a cluster of adaptive changes unique to the TRIM5 proteins of a subset of African monkeys--the Cercopithecinae, which include macaques, mangabeys, and baboons--that suggests that ancestral lentiviruses closely related to modern SIVs began colonizing primates in Africa as far back as 11-16 million years ago. The researchers conclude The correlation between lineage specific adaptations and ability to restrict viruses endemic to the same hosts supports the hypothesis that lentiviruses closely related to modern SIVs were present in Africa and infecting the ancestors of cercopithecine primates as far back as 16 million years ago, and provides insight into the evolution of TRIM5 specificity.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150820144714.htm){:target="_blank" rel="noopener"}


