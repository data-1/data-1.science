---
layout: post
title: "Researchers create 'time crystals' envisioned by Princeton scientists"
date: 2017-09-17
categories:
author: "Princeton University"
tags: [Time crystal,Physics,Time,Crystal,Applied and interdisciplinary physics,Physical sciences,Science,Theoretical physics,Physical chemistry,Nature,Chemistry]
---


In time crystals, however, atoms are arranged in patterns not only in space, but also in time. They were exploring questions about how atoms and molecules settle down, or come to equilibrium, to form phases of matter such as solids, liquids and gases. While on sabbatical at the Max Planck Institute for the Physics of Complex Systems in Germany, Sondhi and Khemani realized that these ideas about how to prevent systems from reaching equilibrium would enable the creation of time crystals. That is what our system does, he said. Many-body localization can protect quantum information, according to research published in 2013 by the Princeton team of David Huse, the Cyrus Fogg Brackett Professor of Physics, as well as Sondhi and colleagues Rahul Nandkishore, Vadim Oganesyan and Arijeet Pal.

<hr>

[Visit Link](https://phys.org/news/2017-03-crystals-envisioned-princeton-scientists.html){:target="_blank" rel="noopener"}


