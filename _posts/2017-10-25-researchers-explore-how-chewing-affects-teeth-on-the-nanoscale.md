---
layout: post
title: "Researchers explore how chewing affects teeth on the nanoscale"
date: 2017-10-25
categories:
author: University Of Arkansas
tags: [Tooth enamel,Tooth]
---


This image shows the nanoscale crystallites that make up tooth enamel before and after researchers applied pressure. The researchers used high-powered microscopes to observe the effects of different kinds of wear on the nanostructures that make up tooth enamel. Using tips made from different types of material, the researchers applied pressure to the surface of human molars, which had been extracted for orthodontic purposes. They also indented the surface of the teeth, pressing the tip against the enamel to simulate the pressure caused by crushing food. The findings in the surface tribological chemistry can help us understand the nature of the interfacial chemical bonding between the nanoparticles that Mother Nature uses to make biominerals of all types on demand, said Tian.

<hr>

[Visit Link](https://phys.org/news/2017-10-explore-affects-teeth-nanoscale.html){:target="_blank" rel="noopener"}


