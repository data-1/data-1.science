---
layout: post
title: "X-Rays May Reveal New Clues about Mysterious Dark Matter in Galaxy Clusters"
date: 2014-07-01
categories:
author: Science World Report
tags: [Dark matter,Universe,Matter,Galaxy,Physical cosmology,Celestial mechanics,Astronomical objects,Science,Cosmology,Astronomy,Physical sciences,Physics,Astrophysics,Space science,Nature]
---


Yet scientists may have found uncovered new clues when it comes to this particular type of matter. In this case, the scientists turned to galaxy clusters. Containing hundreds of galaxies and a huge amount of hot gas filling the space between them, galaxy clusters may just be the perfect way to take a closer look of gravitational influence and, in turn, measure dark matter. In order to learn a bit more about dark matter, the researchers measured the characteristic lines in 73 galaxy clusters and found an intriguing faint line at a wavelength never seen before. The findings reveal possible new clues about the nature of dark matter in our universe.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15674/20140626/x-rays-reveal-new-clues-mysterious-dark-matter-galaxy-clusters.htm){:target="_blank" rel="noopener"}


