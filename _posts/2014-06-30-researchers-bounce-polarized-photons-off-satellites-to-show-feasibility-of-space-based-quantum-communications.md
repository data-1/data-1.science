---
layout: post
title: "Researchers bounce polarized photons off satellites to show feasibility of space based quantum communications"
date: 2014-06-30
categories:
author: Bob Yirka
tags: [Quantum key distribution,Satellite,Technology,Telecommunications]
---


Qubit pulses are sent at 100 Mhz repetition rate and are reﬂected back at the single photon level from the satellite, thus mimicking a QKD source on Space. Up till now, most scientists have believed sending quantum messages between earth and space isn't feasible due to interference in the atmosphere—the error rate threshold has been found to be 11 percent. In so doing, they found the control satellite had a high error rate, as expected—it was approximately 50 percent. More progress is likely to come soon as China plans to send a satellite into orbit in 2016 for the express purpose of conducting quantum communications research. We demonstrated the faithful transmission of qubits from space to ground by exploiting satellite corner cube retroreflectors acting as transmitter in orbit, obtaining a low error rate suitable for QKD.

<hr>

[Visit Link](http://phys.org/news323342781.html){:target="_blank" rel="noopener"}


