---
layout: post
title: "The economic territory of Upper Palaeolithic groups is specified by flint"
date: 2015-07-16
categories:
author: University of the Basque Country  
tags: [Early European modern humans,Branches of science,Periods and stages in archaeology,Stone Age]
---


Never before had the mobility patterns and management of lithic resources in the Upper Palaeolithic been determined so precisely. The study of flint remains found in the open-air Ametzagaina site in Donostia-San Sebastian has determined the economic territory of the human groups that lived there for about 2,000 years. Mobility Patterns and the Management of Lithic Resources by Gravettian Hunter-Gatherers in the Western Pyrenees, has been published in the Journal of Anthropological Research. On this site, the oldest in the Donostia-San Sebastian area, however, a significant set of flint items were found. Determining where they obtained the flint makes it possible to establish their economic territory.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/uotb-tet072114.php){:target="_blank" rel="noopener"}


