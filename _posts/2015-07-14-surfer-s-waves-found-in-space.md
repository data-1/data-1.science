---
layout: post
title: "'Surfer's Waves' Found in Space"
date: 2015-07-14
categories:
author: Stephanie Pappas
tags: [Magnetosphere,THEMIS,Advanced Composition Explorer,Solar wind,Space weather,Science,Outer space,Astronomy,Space science]
---


Kelvin-Helmholtz waves are the classic surfer wave pattern seen throughout nature where a fast fluid rushes over a slower-moving fluid. THEMIS provided the observations that allowed researchers to detect the wave patterns; ACE let them measure the conditions of the solar wind when these waves occurred. The solar wind could be fast, slow, or have magnetic fields pointed in any orientation. This plume might increase the density of the magnetosphere's edge, according to NASA Goddard, setting the conditions for Kelvin-Helmholtz waves. Ultimately, the researchers say, a better understanding of how the magnetosphere behaves will help scientists predict space weather — and protect human technology from its perturbations.

<hr>

[Visit Link](http://www.livescience.com/51526-surfer-waves-found-in-space.html){:target="_blank" rel="noopener"}


