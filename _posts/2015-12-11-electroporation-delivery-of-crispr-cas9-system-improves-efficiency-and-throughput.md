---
layout: post
title: "Electroporation delivery of CRISPR/Cas9 system improves efficiency and throughput"
date: 2015-12-11
categories:
author: Jackson Laboratory
tags: [CRISPR gene editing,Genome editing,Cas9,Microinjection,Genetics,Life sciences,Biotechnology,Biology,Branches of genetics,Molecular biology,Biochemistry,Health sciences]
---


Jackson Laboratory researchers have shown that using an electric current to deliver the CRISPR/Cas9 system, in order to engineer genetic changes in laboratory mice, is highly efficient and significantly improves the system's throughput. CRISPR/Cas9 has significantly enhanced the precision, speed and ease with which experimental organisms can be genetically modified in order to create models of human diseases. In a paper highlighted in the June issue of the journal Genetics, Distinguished Visiting Professor Haoyi Wang, Ph.D., JAX associate director of genomic engineering technologies Wenning Qin, Ph.D., and colleagues demonstrated that electroporation--using an electric current to increase the permeability of the cell membrane--can be a faster, higher throughput alternative to microinjection. Through this technique we were able to dramatically increase processing from one zygote at a time through microinjection to 20 to 50 zygotes simultaneously. Qin et al.: Efficient CRISPR/Cas9-Mediated Genome Editing in Mice by Zygote Electroporation of Nuclease.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/jl-edo060815.php){:target="_blank" rel="noopener"}


