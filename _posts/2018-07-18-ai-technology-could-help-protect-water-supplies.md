---
layout: post
title: "AI technology could help protect water supplies"
date: 2018-07-18
categories:
author: "University of Waterloo"
tags: [Artificial intelligence,Cyanobacteria,Research,Engineering,Technology,Branches of science,Science]
---


Progress on new artificial intelligence (AI) technology could make monitoring at water treatment plants cheaper and easier and help safeguard public health. According to Emelko and collaborator Alexander Wong, a systems design engineering professor at Waterloo, the AI system would provide an early warning of problems since testing could be done much more quickly and frequently. The researchers estimate it may take two to three years to refine a fully commercial sample testing system for use in labs or in-house at treatment plants. The technology to provide continuous monitoring could be three to four years away. It's critical to have running water, even if we have to boil it, for basic hygiene, said Monica Emelko, a professor of civil and environmental engineering at Waterloo.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/uow-atc071718.php){:target="_blank" rel="noopener"}


