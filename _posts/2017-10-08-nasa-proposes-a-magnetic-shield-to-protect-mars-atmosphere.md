---
layout: post
title: "NASA proposes a magnetic shield to protect Mars' atmosphere"
date: 2017-10-08
categories:
author: "Matt Williams"
tags: [Mars,Magnetosphere,Terraforming of Mars,Atmosphere,MAVEN,Earth,Planetary habitability,Planets,Terrestrial planets,Science,Spaceflight,Physical sciences,Planets of the Solar System,Nature,Planetary science,Astronomy,Outer space,Space science]
---


In the course of the talk, which was titled A Future Mars Environment for Science and Exploration, Director Jim Green discussed how deploying a magnetic shield could enhance Mars' atmosphere and facilitate crewed missions there in the future. Roughly 4.2 billion years ago, this planet's magnetic field suddenly disappeared, which caused Mars' atmosphere to slowly be lost to space. In addition, the positioning of this magnetic shield would ensure that the two regions where most of Mars' atmosphere is lost would be shielded. What they found was that a dipole field positioned at Mars L1 Lagrange Point would be able to counteract solar wind, such that Mars' atmosphere would achieve a new balance. Credit: NASA  As a result, Mars atmosphere would naturally thicken over time, which lead to many new possibilities for human exploration and colonization.

<hr>

[Visit Link](https://phys.org/news/2017-03-nasa-magnetic-shield-mars-atmosphere.html){:target="_blank" rel="noopener"}


