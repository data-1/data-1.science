---
layout: post
title: "Mile-high Mars mounds built by wind and climate change"
date: 2016-04-05
categories:
author: University Of Texas At Austin
tags: [Mount Sharp,Erosion,Mars,Gale (crater),Terrestrial planets,Earth sciences,Planetary science,Physical geography,Planets of the Solar System,Geology]
---


Gale Crater, the landing spot of the Mars rover Curiosity, has a three-mile-high mound at its center called Mount Sharp. Credit: Mackenzie Day  There's been a theory out there that these mounds formed from billions of years of wind erosion, but no one had ever tested that before, Day said. Plan-view digital elevation of the crater model (upper left) shows the crater interior going from filled to mounded to empty. Credit: Mackenzie Day  To understand the wind dynamics, researchers also built a computer model that simulated how the wind flowed through the crater at different stages of erosion. The mounds' structure helps link their formation to climate change on Mars, Kocurek said, with the bottom being built during a wet time, and the top built and mound shaped in a dry time.

<hr>

[Visit Link](http://phys.org/news/2016-03-mile-high-mars-mounds-built-climate.html){:target="_blank" rel="noopener"}


