---
layout: post
title: "How to convert US to 100 percent renewable energy"
date: 2016-05-07
categories:
author: Stanford University
tags: [Wind power,Renewable energy,Energy,Nature,Sustainable technologies,Sustainable energy,Physical quantities,Sustainable development,Energy and the environment,Natural environment,Environmental technology,Climate change mitigation,Technology,Renewable resources,Climate change,Energy development,Economy and the environment]
---


For each sector, they then analyzed the current amount and source of the fuel consumed -- coal, oil, gas, nuclear, renewables -- and calculated the fuel demands if all fuel usage were replaced with electricity. The researchers focused on meeting each state's new power demands using only the renewable energies -- wind, solar, geothermal, hydroelectric, and tiny amounts of tidal and wave -- available to each state. Jacobson said that several states are already on their way. Washington state, for instance, could make the switch to full renewables relatively quickly, thanks to the fact that more than 70 percent of its current electricity comes from existing hydroelectric sources. The plan calls for no more than 0.5 percent of any state's land to be covered in solar panels or wind turbines.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150609093025.htm){:target="_blank" rel="noopener"}


