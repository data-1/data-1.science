---
layout: post
title: "World's first intra-operative MRI-guided robot for bilateral stereotactic neurosurgery"
date: 2018-07-01
categories:
author: "The University of Hong Kong"
tags: [Neurosurgery,Magnetic resonance imaging,Stereotactic surgery,Clinical medicine,Medical specialties,Medicine,Neuroscience,Nervous system,Diseases and disorders,Surgery,Health care]
---


It involves a technique that can locate targets of surgical interest using an external positioning system, which is widely applied in brain biopsy, tumor ablation, drug delivery, as well as deep brain stimulation (DBS). This facilitates less invasive stereotactic procedures on the patient under general anesthesia, as surgeons could accurately control and evaluate the stereotactic manipulation bilaterally to the left and right brain targets in real time. The team comprising both engineers and surgeons was recently conferred the Best Conference Paper Award in the largest international forum for robotics scientists, the IEEE International Conference on Robotics and Automation 2018 (ICRA'18), which was held in Brisbane, Australia, from May 21 to 25, 2018. A total of 1,030 papers were selected for presentation. The team project was also shortlisted as a finalist for the Medical Robotics paper award at the same Conference.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180619122517.htm){:target="_blank" rel="noopener"}


