---
layout: post
title: "Ytterbium: The quantum memory of tomorrow"
date: 2018-07-28
categories:
author: "University Of Geneva"
tags: [Quantum memory,Technology,Physics,Science,Physical sciences,Quantum mechanics]
---


Researchers at the University of Geneva (UNIGE), Switzerland, in partnership with CNRS, France, have discovered a new material in which an element, ytterbium, can store and protect the fragile quantum information even while operating at high frequencies. This makes ytterbium an ideal candidate for future quantum networks, where the aim is to propagate the signal over long distances by acting as repeaters. However, the fact that it is impossible to copy the signal also prevents scientists from amplifying it to diffuse it over long distances, as is the case with the Wi-Fi network. Finding the right material to produce quantum memories  Since the signal cannot be copied or amplified without it disappearing, scientists are currently working on how to make quantum memories capable of repeating it by capturing the photons and synchronising them so they can be diffused further and further. The difficulty is finding a material capable of isolating the quantum information conveyed by the photons from environmental disturbances so that we can hold on to them for a second or so and synchronise them, explains Mikael Afzelius, a researcher in the Department of Applied Physics of UNIGE's Faculty of Sciences.

<hr>

[Visit Link](https://phys.org/news/2018-07-ytterbium-quantum-memory-tomorrow.html){:target="_blank" rel="noopener"}


