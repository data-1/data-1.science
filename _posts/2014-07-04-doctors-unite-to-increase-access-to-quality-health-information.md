---
layout: post
title: "Doctors unite to increase access to quality health information"
date: 2014-07-04
categories:
author: "Shauna Gordon-McKeon"
tags: [Wikipedia,Cochrane (organisation),Physician,Wikipedia community,Health]
---


I'm still finding lots of articles that need a great deal of work before they reflect the best available medical evidence. Another initiative, the Wikipedia-journal collaboration, states: One reason some academics express for not contributing to Wikipedia is that they are unable to get the recognition they require for their current professional position. Both Wikipedians personally support open access, and would welcome efforts to supplement closed access citations with open ones. A new project, the Open Access Signalling project aims to help readers quickly distinguish what sources they'll be able to access. There are also a number of us who may be willing / able to speak to Universities that wish to learn more about the place of Wikipedia in Medicine.

<hr>

[Visit Link](http://opensource.com/health/14/7/doctors-unite-increase-access-quality-health-information){:target="_blank" rel="noopener"}


