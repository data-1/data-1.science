---
layout: post
title: "Cave-in: How blind species evolve"
date: 2017-09-11
categories:
author: "Arizona State University (ASU)"
tags: [Mexican tetra,Evolution,Natural selection,Biology,Nature,Biological evolution,Evolutionary biology,Science]
---


Under these conditions, we don't typically expect to find such a difference in traits between surface and cave populations. In their model, the selection for blindness would need to be about 48 times stronger than the immigration rate for Mexican tetras to evolve blindness in caves. If sighted fish swim towards the light, the only fish that stay in the cave are blind fish. Which actually is a form of selection, and thus, Darwinian evolution in action, said Cartwright. Cartwright's team hopes that field biologists begin to consider Lankester's 90-year old hypothesis when studying cavefish.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/04/170417085954.htm){:target="_blank" rel="noopener"}


