---
layout: post
title: "Scientists crack 70-year-old mystery of how magnetic waves heat the sun"
date: 2018-07-01
categories:
author: "Queen'S University Belfast"
tags: [Sun,Alfvn wave,Sunspot,Physics,Earth,Space science,Physical sciences,Astronomy,Nature,Science,Physical phenomena]
---


Scientists have suggested for many years that these waves may play an important role in maintaining the sun's extremely high temperatures but until now had not been able to prove it. At Queen's, we have now led a team to detect and pinpoint the heat produced by Alfvén waves in a sunspot. Our research opens up a new window to understanding how this phenomenon could potentially work in other areas such as energy reactors and medical devices. Credit: Queen's University Belfast  The study used advanced high-resolution observations from the Dunn Solar Telescope in New Mexico (USA) alongside complementary observations from NASA's Solar Dynamics Observatory, to analyse the strongest magnetic fields that appear in sunspots. The recent work published in Nature Physics reveals first-time evidence for how a rare breed of magnetic waves, guided upwards from the surface of the Sun, can form shockwaves that heat the surrounding plasma by thousands of degrees.

<hr>

[Visit Link](https://phys.org/news/2018-03-scientists-year-old-mystery-magnetic-sun.html){:target="_blank" rel="noopener"}


