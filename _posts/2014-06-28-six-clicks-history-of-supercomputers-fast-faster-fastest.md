---
layout: post
title: "Six clicks: History of supercomputers -- fast, faster, fastest"
date: 2014-06-28
categories:
author: Steven Vaughan-Nichols, Senior Contributing Editor, June
tags: []
---


Each of these nodes has two Intel Xeon IvyBridge processors and three Xeon Phi processors for a total of 3,120,000 computing cores. Besides the general purpose Xeon IvyBridge CPUs, Tianhe-2 uses Xeon Phi chips, which specialize in floating point calculations. A total of 62 systems on the June 2014 Top 500 supercomputer list are currently using accelerator/co-processor technology, up from 53 from November 2013. As the race to the next supercomputing continues, the next goal is the ExaFlop or a thousand PetaFlops, supercomputer. I don't know who will make it, but I expect it will be running Linux, using two processor types, and still be based on the tried-and-true Beowulf design.

<hr>

[Visit Link](http://www.zdnet.com/six-clicks-history-of-supercomputers-fast-faster-fastest-7000031018/#ftag=RSS510d04f){:target="_blank" rel="noopener"}


