---
layout: post
title: "What Exactly Is IBM Doing Differently With Machine Learning?"
date: 2018-06-29
categories:
author: "Nov."
tags: []
---


Data science is a team sport, involving data engineers, data scientists, business analysts, application developers, and other personas in the enterprise. Figure 2: Specifying performance metrics for a classification model  More and more, we were seeing data science teams pulled away from their real work of brainstorming, feature engineering, and model design — and instead forced to manage routine tasks that could be automated. You can deploy your machine learning models where it makes the most sense, whether that's in the public cloud, behind the firewall, in the optimized IBM Integrated Analytics System, or on the mainframe, just to name a few. An Enterprise Platform for ML  We brought all these capabilities together into Data Science Experience (DSX), IBM's enterprise platform for ML. Working with IBM means the ability to integrate with the broader IBM portfolio, which ranges from master data management to governance to decision optimization to Watson Explorer to Watson Cognitive APIs to PowerAI and more.

<hr>

[Visit Link](https://dzone.com/articles/so-what-exactly-is-ibm-doing-differently-with-mach?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


