---
layout: post
title: "Is making your product free and open source crazy talk?"
date: 2014-07-15
categories:
author: "Scott Nesbitt"
tags: [GNU General Public License,Open-source software,Open source,Business,Computing,Technology,Software,Intellectual works]
---


Making money from open source. He’s also interviewed leaders at a number of successful open source companies to gain insights into what makes a successful open source business. There are plenty of pitfalls going from an OSS project to OSS company, and I’ve seen quite a few in my career. If you are into software, you need to be a product company. I will have something for those companies looking to open source something previously closed source as well.

<hr>

[Visit Link](http://opensource.com/business/14/7/making-your-product-free-and-open-source-crazy-talk){:target="_blank" rel="noopener"}


