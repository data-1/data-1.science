---
layout: post
title: "US scientists celebrate the restart of the Large Hadron Collider"
date: 2016-04-20
categories:
author: Sarah Charley, Fermi National Accelerator Laboratory
tags: [Large Hadron Collider,CERN,Collider,ATLAS experiment,Particle physics,Physics,Science,Particle physics facilities,Experimental physics,Physics laboratories]
---


A highlight of the LHC's first run, which began in 2009, was the discovery of the Higgs boson, the last in the suite of elementary particles that make up scientists' best picture of the universe and how it works. With the LHC operational again, at even higher energies, the possibilities for new discoveries are endless, and the United States will be at the forefront of those discoveries. We are on the threshold of an exciting time in particle physics: the LHC will turn on with the highest energy beam ever achieved, said Fleming Crim, National Science Foundation Assistant Director for Mathematical and Physical Sciences. In addition to the scientists pushing toward new discoveries on the four main experiments, the U.S. provides a significant portion of the computing and data analysis – roughly 23 percent for ATLAS and 33 percent for CMS. Universities and national laboratories are developing new accelerator and detector technology for future upgrades of the LHC and its experiments.

<hr>

[Visit Link](http://phys.org/news347518879.html){:target="_blank" rel="noopener"}


