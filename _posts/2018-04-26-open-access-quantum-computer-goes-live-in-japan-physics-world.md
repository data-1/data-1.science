---
layout: post
title: "Open-access quantum computer goes live in Japan – Physics World"
date: 2018-04-26
categories:
author: "Hamish Johnston"
tags: [Quantum neural network,Physics,Quantum mechanics,Theoretical computer science,Branches of science,Applied mathematics,Computer science,Technology,Computing]
---


In the loop: the Quantum Neural Network  An optical system for solving combinatorial optimization problems has been made available for use online, say its creators in Japan. A familiar example is the “travelling salesman problem” whereby a person wishes to visit several different destinations by the shortest possible route. The process is repeated as the OPOs make as many as 1000 trips around the loop, amplified at each pass by the PSA. This process transforms the OPOs into a specific configuration of 0 and π states, which is then read off as the solution to the problem. Rather than being a universal quantum computer that can address a wide range of problems, QNN is designed to optimize systems that can be described by Ising models.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/nov/27/open-access-quantum-computer-goes-live-in-japan){:target="_blank" rel="noopener"}


