---
layout: post
title: "Project to elucidate the structure of atomic nuclei at the femtoscale"
date: 2018-07-12
categories:
author: "Scott Morley"
tags: [Particle physics,Physics,Exascale computing,Nuclear physics,Science,Technology,Physical sciences,Computing,Branches of science]
---


The Argonne Leadership Computing Facility (ALCF), a U.S. Department of Energy (DOE) Office of Science User Facility, has selected 10 data science and machine learning projects for its Aurora Early Science Program (ESP). The Aurora ESP, which commenced with 10 simulation-based projects in 2017, is designed to prepare key applications, libraries, and infrastructure for the architecture and scale of the exascale supercomputer. Associate professor of physics William Detmold, assistant professor of physics Phiala Shanahan, and principal research scientist Andrew Pochinsky will use new techniques developed by the group, coupling novel machine learning approaches and state-of-the-art nuclear physics tools, to study the structure of nuclei. Shanahan, who began as an assistant professor at MIT this month, says that the support and early access to frontier computing that the award provides will allow the group to study the possible interactions of dark matter particles with nuclei from our fundamental understanding of particle physics for the first time, providing critical input for experimental searches aiming to unravel the mysteries of dark matter while simultaneously giving insight into fundamental particle physics. “Machine learning coupled with the exascale computational power of Aurora will enable spectacular advances in many areas of science,” Detmold adds.

<hr>

[Visit Link](http://news.mit.edu/2018/project-to-elucidate-structure-of-atomic-nuclei-at-femtoscale-0706){:target="_blank" rel="noopener"}


