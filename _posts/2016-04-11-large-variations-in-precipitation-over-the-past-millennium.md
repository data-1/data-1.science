---
layout: post
title: "Large variations in precipitation over the past millennium"
date: 2016-04-11
categories:
author: Stockholm University
tags: [Climate,Precipitation,Climate variability and change,Climate change,Intergovernmental Panel on Climate Change,Earth sciences,Natural environment,Physical geography]
---


Indirect recorders of past precipitation and drought variability such as tree-ring width data were used by the re-searchers to reconstruct twelve centuries of Northern Hemisphere hydroclimate variability. The percentage of the Northern Hemisphere land area with wet and dry anomalies, respectively, during the past the twelve centuries. Instrumental measurements are also too short to test the ability of state-of-the-art climate models to predict which regions of the hemisphere will get drier, or wetter, with global warming, says Charpentier Ljungqvist. However, unlike the climate model simulations, the new precipitation reconstruction does not show an increase of wet and dry anomalies in the twentieth century compared to the natural variations of the past millennium. The anomalies are shown with regard to the long-term mean between AD 1000 and 1899. Credit: Fredrik Charpentier Ljungqvist  The climate models simulate pre-industrial precipitation variability reasonably well but simulate much stronger wet and dry anomalies during the twentieth century than those found in the reconstruction.

<hr>

[Visit Link](http://phys.org/news/2016-04-large-variations-precipitation-millennium.html){:target="_blank" rel="noopener"}


