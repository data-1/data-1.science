---
layout: post
title: "Photonic nanostructures bring bees to flowers – Physics World"
date: 2017-10-25
categories:
author: Marric Stephens
tags: [Diffraction grating,Flower,Ultraviolet,Color,Petal,Structural coloration,Science,Optics,Electromagnetic radiation]
---


Animal attraction: structural colour makes some flowers easy to spot  Repeating nanostructures on the petals of certain flowers help to attract bees, scientists have shown. Pigments responsible for this colouration are difficult to produce, however, and most flowering plants lack the necessary genetic and biochemical machinery. Perfectly periodic gratings produced an iridescent effect with diffraction peaks determined by the wavelength. The bumblebees seemed able to spot a blue halo against any background colour, with yellow and black flowers discovered as efficiently as all-blue flowers. It is not known if natural photonic structures can attract other insect pollinators, or whether other plants use the effect for different purposes.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/oct/24/photonic-nanostructures-bring-bees-to-flowers){:target="_blank" rel="noopener"}


