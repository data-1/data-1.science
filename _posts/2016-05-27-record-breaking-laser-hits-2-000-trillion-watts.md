---
layout: post
title: "Record-Breaking Laser Hits 2,000 Trillion Watts"
date: 2016-05-27
categories:
author: Gianluca Sarri
tags: [Universe,Watt,Nuclear fusion,Laser,Particle accelerator,National Ignition Facility,Chronology of the universe,Plasma (physics),Physics,Physical sciences]
---


This article was originally published at The Conversation.The publication contributed the article to Live Science's Expert Voices: Op-Ed & Insights. These extremely dense and hot environments, which only ultra-powerful lasers can create, have already taught us a lot about the evolution of our universe and its current state. One of the acceleration beams of the LFEX laser in Osaka. The LFEX is mainly applies to the former, since it is built to study nuclear fusion research. Researchers are also now working on using laser-driven ion beams for cancer therapy.

<hr>

[Visit Link](http://www.livescience.com/51889-record-breaking-laser-hits-2000-trillion-watts.html){:target="_blank" rel="noopener"}


