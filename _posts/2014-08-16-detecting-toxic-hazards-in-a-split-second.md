---
layout: post
title: "Detecting toxic hazards in a split second"
date: 2014-08-16
categories:
author: Heriot-Watt University
tags: []
---


A portable laser device is being developed at Heriot-Watt that can instantly identify chemical hazards, increasing the safety of emergency services and military personnel. In an emergency scenario or military operation there can be uncertainty over whether liquids or gases are toxic, causing delays. In a military scenario it could provide a commander with the information needed to continue an operation safely, but there are a number of potential civilian uses. When the light from our laser touches a chemical the colours of the light that bounce back show which chemicals it has interacted with. The laser reads this fingerprint and the chemical is rapidly identified, whether it's benign or toxic.

<hr>

[Visit Link](http://feeds.importantmedia.org/~r/IM-cleantechnica/~3/lbFQFGswxCw/){:target="_blank" rel="noopener"}


