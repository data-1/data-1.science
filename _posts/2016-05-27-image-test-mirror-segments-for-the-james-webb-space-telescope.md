---
layout: post
title: "Image: Test mirror segments for the James Webb Space Telescope"
date: 2016-05-27
categories:
author: European Space Agency
tags: [James Webb Space Telescope,Galaxy,Mirror,Physical sciences,Space science,Astronomy,Science,Optics,Electromagnetic radiation]
---


To look farther away and hence farther back in time, we need powerful telescopes, such as the James Webb Space Telescope (JWST), which will be able to look back 13.5 billion years to see the light from the first stars and galaxies forming in the early Universe. With JWST, that light will be captured by a very large mirror—6.5 m in diameter—and directed to four very sensitive infrared instruments. These combined attributes of a very large mirror and very low operating temperatures are crucial for these infrared measurements of very distant stars and galaxies. This image shows two polished test mirror segments being inspected by an optical engineer: one segment with the gold coating already applied, the other without. It will then be the largest astronomical telescope in space.

<hr>

[Visit Link](http://phys.org/news/2015-08-image-mirror-segments-james-webb.html){:target="_blank" rel="noopener"}


