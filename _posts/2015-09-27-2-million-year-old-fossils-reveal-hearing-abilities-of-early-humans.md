---
layout: post
title: "2-million-year-old fossils reveal hearing abilities of early humans"
date: 2015-09-27
categories:
author: Binghamton University 
tags: [Language,Human evolution,Human,News aggregator,Hominini,Chimpanzee,Hearing,Biological anthropology]
---


We know that the hearing patterns, or audiograms, in chimpanzees and humans are distinct because their hearing abilities have been measured in the laboratory in living subjects, said Quam. The researchers argue that this combination of auditory features may have favored short-range communication in open environments. Does this mean these early hominins had language? We feel our research line does have considerable potential to provide new insights into when the human hearing pattern emerged and, by extension, when we developed language, said Quam. It would be really interesting to study the hearing pattern in this new species, said Quam.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150925142702.htm){:target="_blank" rel="noopener"}


