---
layout: post
title: "Physicists entangle qubits in a semiconductor at room temperature – Physics World"
date: 2015-12-22
categories:
author: Tushna Commissariat
tags: [Quantum entanglement,Quantum mechanics,Physical sciences,Applied and interdisciplinary physics,Science,Technology,Physics,Theoretical physics]
---


Entanglement engineer: Paul Klimov in the lab  The quantum entanglement of a large ensemble of spins in a semiconductor has been carried out at room temperature for the first time, by researchers in the US. The team entangled more than 10,000 copies of two-qubit entangled states in a commercial silicon-carbide (SiC) wafer at ambient conditions. It uses a combination of infrared laser light with microwave and radio-frequency pulses to entangle nearly 10,000 two-qubit electron and neutron spin pairs. The team first “initializes” or polarizes the system, in a very small magnetic field using infrared laser light. This, according to Awschalom, is probably the biggest challenge in scaling up any quantum system – at room or cryogenic temperatures – into a useful quantum technology.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/nov/24/physicists-entangle-qubits-in-a-semiconductor-at-room-temperature){:target="_blank" rel="noopener"}


