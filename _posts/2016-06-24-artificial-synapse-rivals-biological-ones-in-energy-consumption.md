---
layout: post
title: "Artificial synapse rivals biological ones in energy consumption"
date: 2016-06-24
categories:
author: "Pohang University of Science & Technology (POSTECH)"
tags: [Synaptic plasticity,Neuromorphic engineering,Brain,Memory,Synapse,Phase-change memory,Science,Resistive random-access memory,Spike-timing-dependent plasticity,Neural network,Branches of science,Neuroscience,Technology]
---


POSTECH researchers develop an organic nanofiber based artificial synapse that emulates both important functions and energy consumption of biological ones: Implication of potential use in artificial intelligence computing  Creation of an artificial intelligence system that fully emulates the functions of a human brain has long been a dream of scientists. Most recently, great efforts have been made to realize synaptic functions in single electronic devices, such as using resistive random access memory (RRAM), phase change memory (PCM), conductive bridges, and synaptic transistors. Artificial synapses based on highly aligned nanostructures are still desired for the construction of a highly-integrated artificial neural network. Prof. Tae-Woo Lee, research professor Wentao Xu, and Dr. Sung-Yong Min with the Dept. of Materials Science and Engineering at POSTECH have succeeded in fabricating an organic nanofiber (ONF) electronic device that emulates not only the important working principles and energy consumption of biological synapses but also the morphology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/puos-asr061916.php){:target="_blank" rel="noopener"}


