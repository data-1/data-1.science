---
layout: post
title: "NASA finds new way to track ocean currents from space"
date: 2015-11-04
categories:
author: Carol Rasmussen
tags: [GRACE and GRACE-FO,Ocean current,Atlantic meridional overturning circulation,Hydrography,Hydrology,Oceanography,Nature,Applied and interdisciplinary physics,Physical geography,Earth sciences]
---


NASA's GRACE satellites (artist's concept) measured Atlantic Ocean bottom pressure as an indicator of deep ocean current speed. Credit: NASA/JPL-Caltech  A team of NASA and university scientists has developed a new way to use satellite measurements to track changes in Atlantic Ocean currents, which are a driving force in global climate. At the bottom of the ocean, changes in pressure tell us about flowing water, or currents. Landerer and his team developed a way to isolate in the GRACE gravity data the signal of tiny pressure differences at the ocean bottom that are caused by changes in the deep ocean currents. That theory has long been known and is exploited in buoy networks, but this is the first time we've been able to do it successfully from space.

<hr>

[Visit Link](http://phys.org/news/2015-11-nasa-track-ocean-currents-space.html){:target="_blank" rel="noopener"}


