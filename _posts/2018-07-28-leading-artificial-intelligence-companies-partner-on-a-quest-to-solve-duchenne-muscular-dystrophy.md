---
layout: post
title: "Leading artificial intelligence companies partner on a quest to solve Duchenne Muscular Dystrophy"
date: 2018-07-28
categories:
author: "InSilico Medicine"
tags: [Duchenne muscular dystrophy,Muscular dystrophy,Artificial intelligence,Medication,Insilico Medicine,Drug discovery,Antibiotic,Branches of science,Biotechnology,Life sciences,Technology,Medicine,Biology,Clinical medicine,Health,Health sciences]
---


Thursday, July 19, 2018, Rockville, MD - Insilico Medicine a leader in artificial intelligence for drug discovery, biomarker development and aging research, announced a research collaboration agreement with A2A Pharmaceuticals, Inc. A2A is a biotechnology company headquartered in New York and focused on development of novel drugs for unmet needs in oncology, drug resistant bacterial infections, and other life threatening diseases. Both companies will collaborate on research programs devoted to the development of therapeutic approaches for Duchenne muscular dystrophy (DMD) and other severe genetic disorders. Insilico Medicine‘s technology applies advances in deep neural networks to identifying critical disease targets and generation of novel chemistry using next-generation artificial intelligence. A2A uses proprietary computational tools including artificial intelligence to design highly selective therapeutics for difficult to drug targets like protein-protein interactions. The companies agreed to collaborate on drug discovery programs, empowering Insilico AI’s biology and target discovery engine with A2A’s state-of-the-art chemical design expertise in drug development.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/imi-lai071718.php){:target="_blank" rel="noopener"}


