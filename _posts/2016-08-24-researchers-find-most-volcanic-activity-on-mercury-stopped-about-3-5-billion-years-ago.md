---
layout: post
title: "Researchers find most volcanic activity on Mercury stopped about 3.5 billion years ago"
date: 2016-08-24
categories:
author: "North Carolina State University"
tags: [Mercury (planet),Volcano,Geology,Effusive eruption,Impact crater,Planet,Crust (geology),Types of volcanic eruptions,Mars,Volcanism,Venus,Planetary science,Space science,Physical sciences,Astronomy,Planets,Planets of the Solar System,Terrestrial planets,Bodies of the Solar System,Earth sciences,Astronomical objects known since antiquity]
---


New research from North Carolina State University finds that major volcanic activity on the planet Mercury most likely ended about 3.5 billion years ago. These findings add insight into the geological evolution of Mercury in particular, and what happens when rocky planets cool and contract in general. There are two types of volcanic activity: effusive and explosive. Widespread effusive volcanism on Mercury likely ended by about 3.5Ga  DOI: 10.1002/2016GL069412  Authors: Paul Byrne, NC State University; Lillian Ostrach, NASA Goddard Space Flight Center; Caleb Fassett, Mount Holyoke College; Clark Chapman, Southwest Research Institute; Brett Denevi, Johns Hopkins University; Alexander Evans, Southwest Research Institute and Columbia University; Christian Klimczak, Carnegie Institution and University of Georgia; Maria Banks, Smithsonian National Air and Space Museum and Planetary Science Institute; James Head, Brown University; Sean Solomon, Carnegie Institution and Columbia University  Published: Geophysical Research Letters  Abstract: Crater size-frequency analyses have shown that the largest volcanic plains deposits on Mercury were emplaced around 3.7?Ga, as determined with recent model production function chronologies for impact crater formation on that planet. To test the hypothesis that all major smooth plains on Mercury were emplaced by about that time, we determined crater size-frequency distributions for the nine next-largest deposits, which we interpret also as volcanic.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/ncsu-rfm080516.php){:target="_blank" rel="noopener"}


