---
layout: post
title: "Quantum Computing May Mean Quantum Robotics: The Creative and Smart Robots of the Future"
date: 2015-05-22
categories:
author: Science World Report
tags: [Robot,Computing,Artificial intelligence,Quantum mechanics,Research,Technology,Branches of science,Cognitive science,Cybernetics,Applied mathematics,Computer science,Science,Cognition,Emerging technologies,Systems science,Interdisciplinary subfields,Learning,Technological change]
---


Scientists have taken a closer look at quantum computing and have found that quantum tools can help robots learn and respond much faster to the stimuli around them. Yet can these same tools be applied to robots, automatons and other devices that use artificial intelligence? This mean that quantum robots are more creative. The findings reveal that quantum computing could be used to create quantum robotics. It's also a step forward towards artificial intelligence; in fact, in the future it may be possible to create a robot that is both intelligent and creative.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/17722/20141006/quantum-computing-mean-robotics-creative-smart-robots-future.htm){:target="_blank" rel="noopener"}


