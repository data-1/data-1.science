---
layout: post
title: "Explainer: What are fundamental particles?"
date: 2016-04-19
categories:
author: Paul Kyberd, The Conversation
tags: [Matter,Quark,Particle physics,Standard Model,Higgs boson,Antimatter,Universe,Elementary particle,Lepton,Neutron,Proton,Antiparticle,Neutrino,Fermion,Electron,Quantum mechanics,Elementary particles,Atomic physics,Quantum field theory,Nuclear physics,Subatomic particles,Nature,Physical sciences,Theoretical physics,Physics]
---


Matter particles are fermions while force particles are bosons. Matter particles: quarks and leptons  Matter particles are split into two groups: quarks and leptons – there are six of these, each with a corresponding partner. Each of the matter particles above has a partner particle which has the same mass, but opposite electric charge, so we can double the number of matter particles (six quarks and six leptons) to arrive at a final number of 24. Despite what we know about them, their properties have not been measured well enough to allow us to say definitively that these particles are all that is needed to build the universe we see around us, and we certainly don't have all the answers. When we collide two fundamental particles together a number of outcomes are possible.

<hr>

[Visit Link](http://phys.org/news346058299.html){:target="_blank" rel="noopener"}


