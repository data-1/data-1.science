---
layout: post
title: "Study confirms steady warming of oceans for past 75 years"
date: 2018-08-15
categories:
author: "University of York"
tags: [Climate change,Earth sciences,Physical geography,Science,Applied and interdisciplinary physics]
---


Scientists have solved a puzzling break in continuity of ocean warming records that sparked much controversy after climate data was published in the journal Science in 2015. The new study, which uses independent data from satellites and robotic floats as well as buoys, concludes that the NOAA results were correct. Nowadays, buoys cover much of the ocean and that data is beginning to supplant ship data. The buoys report slightly cooler temperatures because they measure water directly from the ocean instead of after a trip through a warm engine room. Using data from only one instrument type - either satellite, buoys or Argo floats - the results matched those of the NOAA group, supporting the case that the oceans warmed 0.12 degrees Celsius per decade over the past two decades.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/uoy-scs010317.php){:target="_blank" rel="noopener"}


