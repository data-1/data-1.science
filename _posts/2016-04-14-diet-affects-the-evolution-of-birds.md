---
layout: post
title: "Diet affects the evolution of birds"
date: 2016-04-14
categories:
author: University Of Amsterdam
tags: [Speciation,Omnivore,Species,Evolution,Extinction,Biodiversity,Bird,Ecology,Nature,Biological evolution,Natural environment,Evolutionary biology]
---


Credit: João Quental  How diet has affected the evolution of the 10,000 bird species in the world is still a mystery to evolutionary biology. How such diverse dietary preferences ultimately lead to differences in diversification dynamics (i.e. the balance between speciation and extinction) of different birds has not yet been examined. Using models of trait-dependent diversification, they then showed that omnivorous bird lineages (with species that feed on many different food items) have lower rates of speciation (i.e. generating less new species) and higher rates of extinction (i.e. losing more existing species) than species which prefer specific food items such as fruits, nectar, or insects. 'Human activities such as habitat destruction and other global change drivers eliminate the resources of many specialist species', says Daniel Kissling from University of Amsterdam. This means that specialists are currently at higher risk of extinction than generalists.

<hr>

[Visit Link](http://phys.org/news/2016-04-diet-affects-evolution-birds.html){:target="_blank" rel="noopener"}


