---
layout: post
title: "Uniting classical and quantum mechanics: Breakthrough observation of Mott transition in a superconductor"
date: 2015-09-12
categories:
author: "$author" 
tags: [Quantum vortex,Physics,Superconductivity,Nature,Theoretical physics,Electromagnetism,Science,Physical sciences,Applied and interdisciplinary physics]
---


The discovery experimentally connects the worlds of classical and quantum mechanics and illuminates the mysterious nature of the Mott transition. When they applied a large enough electric current, however, the scientists saw a dynamic Mott transition as the system flipped to become a conducting metal; the properties of the material had changed as the current pushed it out of equilibrium. This experimentally materializes the correspondence between quantum and classical physics, Vinokur said. We can controllably induce a phase transition between a state of locked vortices to itinerant vortices by applying an electric current to the system, said Hans Hilgenkamp, head of the University of Twente research group. The system could further provide scientists with insight into two categories of physics that have been hard to understand: many-body systems and out-of-equilibrium systems.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150911224341.htm){:target="_blank" rel="noopener"}


