---
layout: post
title: "How brain cells die in Alzheimer's and FTD: Epigenetic regulator LSD1 tangled up; brain cells go haywire without it"
date: 2017-10-09
categories:
author: "Emory Health Sciences"
tags: [Neurodegenerative disease,Frontotemporal dementia,Alzheimers disease,KDM1A,Neurofibrillary tangle,Dementia,Epigenetics,Biochemistry,Neuroscience,Biology,Cell biology,Life sciences]
---


Researchers also discovered that LSD1 protein is perturbed in brain samples from humans with Alzheimer's disease and frontotemporal dementia (FTD). Based on their findings in human patients and mice, the research team is proposing LSD1 as a central player in these neurodegenerative diseases and a drug target. However, they lacked aggregated proteins in their brains, like those thought to drive Alzheimer's disease and FTD. When LSD1 is taken away, gene activity goes a little haywire in neurons. For example, they turn on a set of genes that are usually active in embryonic stem cells.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171009084357.htm){:target="_blank" rel="noopener"}


