---
layout: post
title: "Hawking’s latest black-hole paper splits physicists"
date: 2016-04-15
categories:
author: Castelvecchi, Davide Castelvecchi, You Can Also Search For This Author In
tags: []
---


As a result, once the black hole was gone, the information carried by anything that had previously fallen into the hole would be lost to the Universe. “That paper was responsible for more sleepless nights among theoretical physicists than any paper in history,” Strominger said during his talk. But the authors note that the vacuum in which a black hole sits need not be devoid of particles — only energy — and therefore that soft particles are present there in a zero-energy state. The paper goes on to suggest a mechanism for transferring that information to the black hole — which would have to happen for the paradox to be solved. Strominger, A. J.

<hr>

[Visit Link](http://www.nature.com/news/hawking-s-latest-black-hole-paper-splits-physicists-1.19236){:target="_blank" rel="noopener"}


