---
layout: post
title: "Simulations foresee hordes of colliding black holes in LIGO's future"
date: 2016-06-25
categories:
author: "University of Chicago"
tags: [LIGO,Black hole,Gravitational wave,Binary black hole,Star,Physical sciences,General relativity,Nature,Cosmology,Stellar astronomy,Celestial mechanics,Science,Physical cosmology,Physics,Astrophysics,Astronomy,Space science,Astronomical objects]
---


Here we simulate binary stars, how they evolve, turn into black holes, and eventually get close enough to crash into each other and make gravitational waves that we would observe, Holz said. By then, the Nature study predicts that LIGO might be detecting more than 100 black hole collisions annually. But if the binaries were formed in large enough numbers, a small fraction would survive for longer periods and would end up merging 11 billion years after the Big Bang (2.8 billion years ago), recently enough for LIGO to detect. They assume isolated formation, which involves two stars forming in a binary, evolving in tandem into black holes, and eventually merging with a burst of gravitational wave emission. Binary stars that evolved dynamically are expected to have randomly aligned spins; detecting a preference for aligned spins would be clear evidence in favor of the isolated evolutionary model.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/uoc-sfh062316.php){:target="_blank" rel="noopener"}


