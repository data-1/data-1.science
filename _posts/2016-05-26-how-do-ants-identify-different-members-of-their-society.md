---
layout: post
title: "How do ants identify different members of their society?"
date: 2016-05-26
categories:
author: University Of California - Riverside
tags: [Sense of smell,Eusociality,Ant,Odor,Pheromone,Neuroscience,Cognitive science]
---


The cuticle of the workers and the queen is enriched with specific hydrocarbons, many of which act as pheromones and are sensed by others in the colony. Reporting today in the journal Cell Reports, the researchers note that ants, which have evolved some of the largest families of olfactory receptor genes in insects, use their powerful sense of smell to sense the chemicals present on the cuticle of individuals to precisely identify the different members of their society. What we wanted to study was how ants detect sophisticated pheromones which organize their behaviors efficiently into colonies, and help recognize individuals from different castes, the queen, and non-nestmate intruders. Ray explained that social insects like ants have remarkable ability to organize into large colonies with multiple castes participating for the colony to succeed, and like any large society this coordination requires effective communication between individuals. Ray explained that the prevailing model, proposed in 2005, suggested that worker ants could preferentially smell only non-nestmate cuticular hydrocarbons, and not smell the hydrocarbons from nestmates.

<hr>

[Visit Link](http://phys.org/news/2015-08-ants-members-society.html){:target="_blank" rel="noopener"}


