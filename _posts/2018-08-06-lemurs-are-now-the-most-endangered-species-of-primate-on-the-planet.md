---
layout: post
title: "Lemurs are now the most endangered species of primate on the planet"
date: 2018-08-06
categories:
author: "Luciana Pricop"
tags: []
---


Continue reading below Our Featured Videos  Chair of the Primate Specialist Group and Chief Conservation Officer of Global Wildlife Conservation Russ Mittermeier indicated that the “very high extinction risk to Madagascar’s unique lemurs” would compound, generating “grave threats to Madagascar’s biodiversity as a whole.” Loss of habitat poses the single greatest threat the lemurs now face in the wild. Developments in illegal logging and slash-and-burn agriculture, as well as mining activities and charcoal production, are ultimately determining the fate of these endangered animals. The “lemur action plan” has already had an effect, protecting habitats that contain the densest numbers of lemur species while helping Madagascar boost its ecotourism in the hopes of tackling poverty. By helping the local people economically, the groups involved in the plan are deterring hunting and other activities destructive to the tropical forests that provide the lemurs with their natural habitat. + Global Wildlife Conservation  + IUCN  Via BBC News

<hr>

[Visit Link](https://inhabitat.com/lemurs-are-now-the-most-endangered-species-of-primate-on-the-planet){:target="_blank" rel="noopener"}


