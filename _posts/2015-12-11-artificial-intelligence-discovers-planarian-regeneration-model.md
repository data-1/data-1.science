---
layout: post
title: "Artificial intelligence discovers planarian regeneration model"
date: 2015-12-11
categories:
author: PLOS
tags: [Computational biology,Research,PLOS,Science,Regeneration (biology),Open access,Branches of science,Technology,Cognitive science,Computing]
---


The work, published in PLOS Computational Biology, demonstrates how robot science can help human scientists in the future. To mine the fast-growing mountain of published experimental data in regeneration and developmental biology Lobo and Levin developed an algorithm that would use evolutionary computation to produce regulatory networks able to evolve to accurately predict the results of published laboratory experiments that the researchers entered into a database. All this suggests to me that artificial intelligence can help with every aspect of science, not only data mining but also inference of meaning of the data. Image Link: https://www.plos.org/wp-content/uploads/2015/05/Levin-4-June.jpg  All works published in PLOS Computational Biology are Open Access, which means that all content is immediately and freely available. All works published in PLOS Computational Biology are Open Access.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/p-aid052815.php){:target="_blank" rel="noopener"}


