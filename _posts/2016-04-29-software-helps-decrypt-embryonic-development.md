---
layout: post
title: "Software helps decrypt embryonic development"
date: 2016-04-29
categories:
author: Max Planck Society
tags: [Equation,Differential equation,Biology,Mathematics,Cellular differentiation,Reactiondiffusion system,System,Gene regulatory network,Alan Turing,Computer,Branches of science,Technology,Science]
---


Model of a classical Turing network compared to the extended Turing networks analyzed with the software RDNets. The software can be used to analyze how patterns form during development and to create novel patterns for bioengineering approaches. Although Turing's patterns remarkably resemble the patterns observed during normal development, Turing models have been limited to two mobile signaling molecules and could not take into account our current knowledge about the complex underlying gene regulatory networks, which have only been identified over the last few decades after Turing's death. To include the effect that gene regulatory networks have on pattern formation, a team led by Patrick Müller with scientists from the Friedrich Miescher Laboratory in Tübingen and the Centre for Genomic Regulation in Barcelona developed a new computational method to analyze and simulate the formation of patterns in reaction-diffusion networks with both mobile and immobile molecules. This novel approach of an automated high-throughput mathematical analysis can be used to screen for new biochemical Turing networks that can form self-organizing periodic patterns.

<hr>

[Visit Link](http://phys.org/news/2016-04-software-decrypt-embryonic.html){:target="_blank" rel="noopener"}


