---
layout: post
title: "New algorithm enables data integration at single-cell resolution"
date: 2018-04-20
categories:
author: "New York University"
tags: [DNA sequencing,Computational biology,Single cell sequencing,Research,Evolution,Cell (biology),Bone marrow,Systems biology,Biotechnology,Life sciences,Biology,Technology,Branches of science]
---


Credit: CC0 Public Domain  A team of computational biologists has developed an algorithm that can 'align' multiple sequencing datasets with single-cell resolution. For example, when the team independently analyzed datasets of the same bone-marrow stem cells, produced by two separate labs, they obtained strikingly different results. We needed a new method that could identify and align shared groups of cells present in multiple experiments so that we could integrate the datasets together, says Andrew Butler, a graduate student at NYU and lead author of the study. We realized that we could use these methods to learn how cells modify their behavior—for example, in response to drug treatment, notes Butler. Furthermore, they integrated single-cell datasets of pancreatic tissue from humans and mice, thereby identifying 10 cell types that were shared across species and defining the evolutionary changes occurring in each group.

<hr>

[Visit Link](https://phys.org/news/2018-04-algorithm-enables-single-cell-resolution.html){:target="_blank" rel="noopener"}


