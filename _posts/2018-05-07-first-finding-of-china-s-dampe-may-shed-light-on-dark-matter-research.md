---
layout: post
title: "First finding of China's DAMPE may shed light on dark matter research"
date: 2018-05-07
categories:
author: "Chinese Academy of Sciences Headquarters"
tags: [Dark Matter Particle Explorer,Cosmic ray,Positron,Dark matter,Electronvolt,Electron,Astrophysics,Physical sciences,Physics,Astronomy,Science,Space science,Nature,Particle physics]
---


The Dark Matter Particle Explorer (DAMPE, also known as Wukong) mission published its first scientific results on Nov. 30 in Nature, presenting the precise measurement of cosmic ray electron flux, especially a spectral break at ~0.9 TeV. The DAMPE mission is funded by the strategic priority science and technology projects in space science of CAS. DAMPE is expected to record more than 10 billion cosmic ray events over its useful life - projected to exceed five years given the current state of its instruments. The Silicon-Tungsten tracKer-converter (STK) detector was jointly developed by the Institute of High Energy Physics, CAS, the University of Geneva (UniGE), and INFN Perugia. The BGO imaging calorimeter was jointly developed by the University of Science and Technology of China (USTC) and PMO.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/caos-ffo112317.php){:target="_blank" rel="noopener"}


