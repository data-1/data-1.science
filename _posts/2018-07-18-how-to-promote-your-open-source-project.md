---
layout: post
title: "How to promote your open source project"
date: 2018-07-18
categories:
author: "Michael Boelen"
tags: [Open source,Ubuntu,Linux,Software,Linux distribution,Open-source software,Software engineering,Information Age,Computer engineering,Information technology management,Computer science,Software development,Technology,Computing]
---


When it comes to promoting the open source project, make sure it is clear how it helps the user. Takeaways  Be clear on your website what the tool does and how it helps the user  Features are great, describing the benefits is even better  Provide clear lists, as people prefer that too big chunks of text  Create great documentation  One of the painful lessons I’ve learned is the requirement of great documentation. Takeaways  Open source software is no different than other products and needs promotion  Promotion is needed, especially in crowded areas  Using a marketing plan helps with the promotion and better value proposition (lesson 2)  Software quality  Most users of software expect a certain amount of quality from software. But often we don’t think like the users of our software. Takeaways  Try to learn about your users and their use cases  Ask people how they discovered the project  Use this moment of questioning to predict the future and ask what features they like to see  Be careful with packaging  Lynis had a lot of releases, resulting in work for package and port maintainers.

<hr>

[Visit Link](https://linux-audit.com/how-to-promote-your-open-source-project/){:target="_blank" rel="noopener"}


