---
layout: post
title: "Nine risk factors may contribute to two-thirds of Alzheimer's cases worldwide: All potentially modifiable; could prove promising options for prevention, say researchers"
date: 2016-05-31
categories:
author: "BMJ"
tags: [Alzheimers disease,Risk,Type 2 diabetes,Diseases and disorders,Health,Health sciences,Clinical medicine,Medical specialties,Medicine,Causes of death,Epidemiology,Public health,Health care]
---


Nine potentially modifiable risk factors may contribute to up to two thirds of Alzheimer's disease cases worldwide, suggests an analysis of the available evidence, published online in the Journal of Neurology Neurosurgery & Psychiatry. The analysis indicates the complexity of Alzheimer's disease development and just how varied the risk factors for it are. advertisement  They found grade 1 level evidence in favour of a protective effect for the female hormone oestrogen, cholesterol lowering drugs (statins), drugs to lower high blood pressure, and anti-inflammatory drugs (NSAIDs). Similarly, the pooled data indicated a strong association between high levels of homocysteine--an amino acid manufactured in the body--and depression and a significantly heightened risk of developing Alzheimer's disease. The researchers then assessed the population attributable risk (PAR) for nine risk factors which had strong evidence in favour of an association with Alzheimer's disease in the pooled analysis, and for which there are data on global prevalence.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150820190257.htm){:target="_blank" rel="noopener"}


