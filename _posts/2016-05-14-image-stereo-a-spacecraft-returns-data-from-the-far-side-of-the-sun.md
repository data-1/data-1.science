---
layout: post
title: "Image: STEREO-A spacecraft returns data from the far side of the sun"
date: 2016-05-14
categories:
author:  
tags: [STEREO,Sun,Space science,Spaceflight,Astronomy,Solar System,Outer space,Bodies of the Solar System,Spacecraft]
---


Credit: NASA/STEREO  This image of the sun was taken on July 15, 2015, with the Extreme Ultraviolet Imager onboard NASA's Solar TErrestrial RElations Observatory Ahead (STEREO-A) spacecraft, which collects images in several wavelengths of light that are invisible to the human eye. STEREO-A has been on the far side of the sun since March 24, where it had to operate in safe mode, collecting and saving data from its radio instrument. The first images in over three months were received from STEREO-A on July 11. STEREO-A orbits the sun as Earth does, but in a slightly smaller and faster orbit. STEREO is the third mission in NASA's Solar Terrestrial Probes program (STP).

<hr>

[Visit Link](http://phys.org/news/2015-07-image-stereo-a-spacecraft-side-sun.html){:target="_blank" rel="noopener"}


