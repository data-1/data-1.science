---
layout: post
title: "Songbird's reference genome illuminate key role of epigenetics in evolution of memory and learning: Smart songbird's reference genome is milestone for ecological research"
date: 2016-01-29
categories:
author: Netherlands Institute of Ecology (NIOO-KNAW)
tags: [Genetics,Gene,Epigenetics,Evolution,Species,News aggregator,DNA methylation,Life sciences,Biology]
---


A well-known songbird, the great tit, has revealed its genetic code, offering researchers new insight into how species adapt to a changing planet. Their initial findings suggest that epigenetics -- what's on rather than what's in the gene -- may play a key role in the evolution of memory and learning. People in our field have been waiting for this for decades, explain researchers Kees van Oers and Veronika Laine from the Netherlands Institute of Ecology. The research team sequenced the complete genomes of a further 29 great tit individuals from different parts of Europe. It's evidence of a correlation between epigenetic processes such as methylation and the rate of molecular evolution: the more methylation, the more evolution.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2016/01/160125090617.htm){:target="_blank" rel="noopener"}


