---
layout: post
title: "Entanglement distributed over 1200 km by quantum satellite – Physics World"
date: 2017-09-20
categories:
author: "Hamish Johnston"
tags: [Quantum Experiments at Space Scale,Quantum key distribution,Quantum entanglement,Science,Physics,Quantum mechanics,Electromagnetic radiation,Applied and interdisciplinary physics,Theoretical physics,Quantum information science,Technology,Physical sciences]
---


Quantum satellite: entangled photons have been distributed over 1200 km. Now, a team led by Jian-Wei Pan of the University of Science and Technology of China in Hefei has used the photon source to distribute entangled photons to pairs of three ground stations in China – each up to 1200 km apart. Photon loss increases exponentially with distance travelled, so linking distant cities using fibre would be extremely difficult. Polarization entangled  The source on board QUESS produces nearly six million entangled photon pairs per second. The pairs are split and individual photons are directed at two different receiving stations – covering distances of 500–2000 km.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/jun/16/entanglement-distributed-over-1200-km-by-quantum-satellite){:target="_blank" rel="noopener"}


