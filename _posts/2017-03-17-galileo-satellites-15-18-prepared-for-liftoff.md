---
layout: post
title: "Galileo satellites 15–18 prepared for liftoff"
date: 2017-03-17
categories:
author: ""
tags: [Galileo (satellite navigation),European Space Agency,Guiana Space Centre,Flight,Spacecraft,Space vehicles,Astronomy,European space programmes,Space science,Astronautics,Space programs,Spaceflight,Space policy of the European Union,Space agencies,Technology,Pan-European scientific organizations,Science,Space traffic management,Space exploration,Outer space]
---


This timelapse video shows Galileo satellites 15–18, from final preparations to liftoff on a Ariane 5 launcher, flight VA233, from Europe’s Spaceport in French Guiana, on 17 November 2016, accelerating deployment of the new satellite navigation system. Galileo is the Europe’s own global satellite navigation system. The full system of 24 satellites plus spares is expected to be in place by 2020. The European Commission has the overall responsibility for the programme, managing and overseeing the implementation of all programme activities. The definition, development and in-orbit validation phases were carried out by ESA, and co-funded by ESA and the European Commission.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2016/12/Galileo_satellites_15_18_prepares_for_litoff){:target="_blank" rel="noopener"}


