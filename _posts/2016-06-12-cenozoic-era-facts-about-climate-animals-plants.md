---
layout: post
title: "Cenozoic era: Facts about climate, animals & plants"
date: 2016-06-12
categories:
author: "Kim Ann Zimmermann"
tags: [Cenozoic,Paleogene,Geologic time scale,Earth sciences,Nature,Geology,Units of geologic time,Global natural environment,International Commission on Stratigraphy geologic time scale of Earth]
---


During the Paleogene period, most of the Earth’s climate was tropical. The Neogene period saw a drastic cooling, which continued into the Pleistocene epoch of the Quaternary period. The beginning of the Paleogene period was a time for the mammals that survived from the Cretaceous period. Without the dinosaurs, plant life had an opportunity to flourish during the Cenozoic era. However, as the climate cooled forests died off, creating open land.

<hr>

[Visit Link](http://www.livescience.com/40352-cenozoic-era.html){:target="_blank" rel="noopener"}


