---
layout: post
title: "Eyes came before limbs in the transition to land"
date: 2017-09-25
categories:
author: "Society Of Vertebrate Paleontology"
tags: [Eye,Tetrapod,Tiktaalik,Animals,Zoology]
---


The transition to land from our fishy ancestors is one of the most iconic, and best documented, transitions in the fossil record. We know that fleshy-limbed fish living in shallows used their limbs to move in the shallows and then up onto land, and became tetrapods (4-legged vertebrates) along the way. What has been less well-studied is the transition to vision in air that also occurred during that evolutionary event. Said Schmitz, We were surprised to find that large eyes evolved in aquatic tetrapods, preceding the evolution of complete limbs and terrestriality. Animals like Tiktaalik (an early tetrapod) have their eyes on the top of their heads, suggesting that they may have hunted like modern crocodiles: swimming calmly near the surface, the head slightly raised, with eyes above the water, trying to detect prey across the water or even on land, said Schmitz.

<hr>

[Visit Link](http://phys.org/news/2016-10-eyes-limbs-transition.html){:target="_blank" rel="noopener"}


