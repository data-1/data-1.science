---
layout: post
title: "Being open about data privacy"
date: 2018-04-26
categories:
author: "Mike Bursell
(Alumni)"
tags: [General Data Protection Regulation,Advertising,European Union,Information technology,Technology,Computing]
---


It turns out that the starting point for what data people and governments believe should be available for organisations to use is somewhat different between the U.S. and Europe, with the former generally providing more latitude for entities—particularly, the more cynical might suggest, large commercial entities—to use data they've collected about us as they will. What is important about the GDPR, though, is that it doesn't apply just to European companies, but to any organisation processing data about EU citizens. Unless we want to start paying to use all of these free services, we need to be aware of what we're giving up, and making some choices about what we expose and what we don't. And do you think the public organisations, or those whose data is collected, will get a say in who these third parties are or how they will use it? start with why shouldn't I make this public?

<hr>

[Visit Link](https://opensource.com/article/18/1/being-open-about-data-privacy){:target="_blank" rel="noopener"}


