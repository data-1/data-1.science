---
layout: post
title: "Team develops new generation of artificial neural networks able to predict properties of organic compounds"
date: 2018-07-18
categories:
author: "Skolkovo Institute Of Science"
tags: [Solution (chemistry),Artificial neural network,Bioconcentration,Neural network,Machine learning,Physical chemistry,Privacy,Molecule,Physical sciences,Chemistry,Technology,Branches of science,Science,Cognitive science]
---


Leveraging the classical models of physicochemical interactions between the solvent and the solute and advanced machine learning methods, the new approach makes it possible to predict complex properties of a substance based on a minimum set of input data. One of the most important characteristics of organic substances, BCF represents how much of a substance is concentrated in a tissue relative to how much of that substance exists in the environment in equilibrium conditions. The second method is based on the molecular theory of liquids that describes the behavior of substances in solutions. Scientists from Skoltech, the University of Tartu (Estonia) and the University of Strathclyde (UK), led by Skoltech Professor Maxim Fedorov, developed a hybrid BCF prediction method that consists of two steps: first the researchers make physical-chemical calculations to obtain 3-D densities of hydrogen and oxygen around the molecule under study and then apply 3-D convolutional neural networks ‒ a technology successfully used in image recognition. In the long term, our method will help to predict the properties of various 'exotic' molecules and novel compounds where the existing structure-property relationship methods do not work, said first author and Skoltech Ph.D. student Sergey Sosnin.

<hr>

[Visit Link](https://phys.org/news/2018-07-team-artificial-neural-networks-properties.html){:target="_blank" rel="noopener"}


