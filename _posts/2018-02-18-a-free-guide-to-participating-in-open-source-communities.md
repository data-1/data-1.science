---
layout: post
title: "A Free Guide to Participating in Open Source Communities"
date: 2018-02-18
categories:
author: "Sam Dean"
tags: [Open source,Open-source-software movement,Linux,Linux Foundation,Open-source software,Leadership,Community,Technology,Business,Communication]
---


One of the most important first steps is to rally leadership behind your community participation strategy. “You should really understand the company’s objectives and how to enable them in your open source strategy.”  Building relationships is good strategy  The guide also notes that building relationships at events can make a difference, and that including community members early and often is a good strategy. “Some organizations make the mistake of developing big chunks of code in house and then dumping them into the open source project, which is almost never seen as a positive way to engage with the community,” the guide notes. It can be challenging for organizations to understand how influence is earned within open source projects. The guides are available now to help you run an open source program office where open source is supported, shared, and leveraged.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/free-guide-participating-open-source-communities/){:target="_blank" rel="noopener"}


