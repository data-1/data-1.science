---
layout: post
title: "Underlying mechanism discovered for magnetic effect in superconducting spintronics"
date: 2018-07-12
categories:
author: "American Institute of Physics"
tags: [Superconductivity,Spintronics,Proximity effect (superconductivity),Magnetism,Magnetic field,Physics,Ferromagnetism,Theoretical physics,Science,Chemical product engineering,Phases of matter,Materials,Chemistry,Condensed matter,Electromagnetism,Applied and interdisciplinary physics,Physical sciences,Quantum mechanics,Condensed matter physics,Electricity,Materials science,Electrical engineering]
---


New work on the interactions between superconductors and ferromagnets bridges the gap between theory on the long-range magnetic proximity effect and recent experimental results  WASHINGTON, D.C., July 10, 2018 -- The emerging field of spintronics leverages electron spin and magnetization. An international team of researchers recently revealed a general mechanism of the long-range electromagnetic proximity effect in SF structures in Applied Physics Letters, from AIP Publishing. They explain that SF interactions led to a strong spread of stray magnetic field to the superconductor from the ferromagnet. Buzdin explained how in the case of normal, nonsuperconducting metal, for example, the spread of the magnetic field in the opposite direction from the ferromagnet into the metal layers is possible only at the atomic length scale. The team plans to further study the electrodynamics of SF structures and use their findings to one day create new types of spin valves, which can be used in magnetic sensors and computer memory devices.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/aiop-umd071018.php){:target="_blank" rel="noopener"}


