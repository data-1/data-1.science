---
layout: post
title: "Okinawan pit viper genome reveals evolution of snake venom"
date: 2017-10-04
categories:
author: "Okinawa Institute Of Science, Technology Graduate University"
tags: [Snakebite,Evolution,Snake,Okinawa Prefecture,Snake venom,Venom,Biology]
---


For the first time, researchers have sequenced a habu genome, that of the Taiwan habu (Protobothrops mucrosquamatus), and compared it to that of its sister species, the Sakishima habu (Protobothrops elegans). Credit: Okinawa Institute of Science and Technology Graduate University  By taking samples of venoms and soft tissues from more than 30 specimens of the Taiwan and Sakishima habus, invasive but well-established species on Okinawa, researchers from OIST and the Okinawa Prefectural Institute of Health and Environment were able to map entire sequences of venom genes. Their study shows more than one factor at play in the evolution of this venom. These inefficiencies can be passed down from generation to generation with relatively little effect on the venom's function. One of those is pushing them to be more effective, but another axis actually pushes them to be less effective.

<hr>

[Visit Link](https://phys.org/news/2017-10-okinawan-pit-viper-genome-reveals.html){:target="_blank" rel="noopener"}


