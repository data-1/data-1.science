---
layout: post
title: "Another milestone in hybrid artificial photosynthesis"
date: 2015-09-07
categories:
author: "$author" 
tags: [Hydrogen,Hydrogen economy,Artificial photosynthesis,Lawrence Berkeley National Laboratory,Physical sciences,Chemical substances,Energy,Technology,Materials,Chemistry,Nature]
---


A team of researchers at the U.S. Department of Energy (DOE)'s Lawrence Berkeley National Laboratory (Berkeley Lab) developing a bioinorganic hybrid approach to artificial photosynthesis have achieved another milestone. Having generated quite a buzz with their hybrid system of semiconducting nanowires and bacteria that used electrons to synthesize carbon dioxide into acetate, the team has now developed a hybrid system that produces renewable molecular hydrogen and uses it to synthesize carbon dioxide into methane, the primary constituent of natural gas. By generating renewable hydrogen and feeding it to microbes for the production of methane, we can now expect an electrical-to-chemical efficiency of better than 50 percent and a solar-to-chemical energy conversion efficiency of 10-percent if our system is coupled with state-of-art solar panel and electrolyzer. Whereas in the first study, the team worked with Sporomusa ovata, an anaerobic bacterium that readily accepts electrons from the surrounding environment to reduce carbon dioxide, in the new study the team populated the membrane with Methanosarcina barkeri, an anaerobic archaeon that reduces carbon dioxide using hydrogen rather than electrons. The University of California manages Berkeley Lab for the U.S. Department of Energy's Office of Science.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/dbnl-am082415.php){:target="_blank" rel="noopener"}


