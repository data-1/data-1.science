---
layout: post
title: "Rainforest collapse 307 million years ago impacted the evolution of early land vertebrates"
date: 2018-08-02
categories:
author: "University Of Birmingham"
tags: [Tetrapod,Carboniferous rainforest collapse,Biodiversity,Amniote,Evolution of tetrapods,Nature,Earth sciences]
---


Credit: Mark Ryan/ University of Birmingham  Researchers at the University of Birmingham have discovered that the mass extinction seen in plant species caused by the onset of a drier climate 307 million years ago led to extinctions of some groups of tetrapods, the first vertebrates to live on land, but allowed others to expand across the globe. This research is published today in the journal Proceedings of the Royal Society B. The Carboniferous and Permian periods (358 - 272 million years ago) were critical intervals in the evolution of life on land. However they also found that after the rainforest collapse surviving tetrapod species began to disperse more freely across the globe, colonising new habitats further from the equator. Emma Dunne, from the University of Birmingham's School of Geography, Earth and Environmental Sciences, said: This is the most comprehensive survey ever undertaken on early tetrapod evolution, and uses many newly developed techniques for estimating diversity patterns of species from fossil records, allowing us greater insights into how early tetrapods responded to the changes in their environment.

<hr>

[Visit Link](https://phys.org/news/2018-02-rainforest-collapse-million-years-impacted.html){:target="_blank" rel="noopener"}


