---
layout: post
title: "Tool use is 'innate' in chimpanzees but not bonobos, their closest evolutionary relative"
date: 2016-05-08
categories:
author: University of Cambridge
tags: [Tool use by animals,Chimpanzee,Bipedalism,Bonobo,Cognitive science,Behavioural sciences,Zoology,Animals,Ethology]
---


Chimpanzees and bonobos are the two closest living relatives of the human species -- the ultimate tool-using ape. From nut trees to ant nests, stones to shrubs, the bonobos had access to as many tools and promising foraging opportunities in their stomping ground as the chimpanzees. Nor did social opportunities. Young bonobos also had more social partners than young chimpanzees. Our findings suggest that an innate predisposition, or intrinsic motivation, to manipulate objects was likely also selected for in the hominin lineage and played a key role in the evolution of technology in our own lineage, she said.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150616072318.htm){:target="_blank" rel="noopener"}


