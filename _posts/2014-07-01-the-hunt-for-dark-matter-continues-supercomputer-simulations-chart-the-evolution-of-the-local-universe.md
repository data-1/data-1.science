---
layout: post
title: "The Hunt for Dark Matter Continues: Supercomputer Simulations Chart the Evolution of the Local Universe"
date: 2014-07-01
categories:
author: Science World Report
tags: [Milky Way,Universe,Dark matter,Astronomy,Chronology of the universe,Space science,Physical sciences,Physical cosmology,Astrophysics,Cosmology,Nature,Science,Physics,Astronomical objects,Natural sciences]
---


Dark matter is the key to everything we know about galaxies, but we still don't know its exact nature, said Carlos Frenk, one of the researchers, in a news release. In this case, the simulations showed how and why millions of halos around our galaxy failed to produce galaxy. A few halos combated this by growing early and fast enough to hold onto their gas and form galaxies. Thanks to our simulations we know that if our theories of dark matter are correct then the universe around us should be full of halos that failed to make a galaxy. Perhaps astronomers will one day figure out a way to find them.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15673/20140626/hunt-dark-matter-continues-supercomputer-simulations-chart-evolution-local-universe.htm){:target="_blank" rel="noopener"}


