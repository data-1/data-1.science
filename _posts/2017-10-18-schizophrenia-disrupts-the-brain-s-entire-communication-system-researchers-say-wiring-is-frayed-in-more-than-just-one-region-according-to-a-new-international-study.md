---
layout: post
title: "Schizophrenia disrupts the brain's entire communication system, researchers say: Wiring is frayed in more than just one region, according to a new international study"
date: 2017-10-18
categories:
author: University of Southern California
tags: [Schizophrenia,Mental disorder,Psychiatry,Health,Mental health,Health sciences,Human diseases and disorders,Causes of death,Medicine,Mental disorders,Clinical medicine,Diseases and disorders,Neuroscience]
---


We can definitively say for the first time that schizophrenia is a disorder where white matter wiring is frayed throughout the brain, said Kelly, a researcher at the USC Mark and Mary Stevens Neuroimaging and Informatics Institute at the Keck School of Medicine of USC when the study was conducted. To do that, they need to aggregate and analyze global brain scan data, said Neda Jahanshad, co-lead author of the study and assistant professor of neurology at the Imaging Genetics Center within the USC Stevens Neuroimaging and Informatics Institute. The effect is global. The big data project was integrated from 29 different international studies by the ENIGMA (Enhancing Neuro Imaging Genetics through Meta Analysis) network, a global consortium headed by Paul Thompson at the Keck School of Medicine. These scans allow scientists to locate problem areas in the brain's normally insulated communication system.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171017091913.htm){:target="_blank" rel="noopener"}


