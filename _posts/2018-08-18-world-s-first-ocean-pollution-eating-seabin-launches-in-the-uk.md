---
layout: post
title: "World’s first ocean pollution-eating Seabin launches in the UK"
date: 2018-08-18
categories:
author: "Amanda Froelich"
tags: []
---


The device, which was developed by a pair of Australian surfers, works by sucking in various kinds of pollution (including oil) and spitting out clean water. The Seabin can collect approximately 1.5 kg of waste each day and has a capacity of 12 kg — and in a given year, a single bin can collect 20,000 plastic bottles or 83,000 plastic bags. To fund the invention, founders Andrew Turton and Pete Ceglinski created an IndieGoGo campaign. SIGN UP I agree to receive emails from the site. SIGN UP  The Times reports that the Seabin was installed near the base of the Land Rover Ben Ainslie Racing (BAR) team in the Portsmouth harbor.

<hr>

[Visit Link](https://inhabitat.com/worlds-first-ocean-pollution-eating-seabin-launches-in-the-uk){:target="_blank" rel="noopener"}


