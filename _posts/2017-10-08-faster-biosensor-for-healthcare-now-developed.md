---
layout: post
title: "Faster biosensor for healthcare now developed"
date: 2017-10-08
categories:
author: "DGIST (Daegu Gyeongbuk Institute of Science and Technology)"
tags: [Biosensor,Sensor,News aggregator,Chemistry,Technology,Physical sciences]
---


DGIST research team led by Professor CheolGi Kim has developed a biosensor platform which has 20 times faster detection capability than the existing biosensors using magnetic patterns resembling a spider web. Many research groups in Korea and other countries have been improving the resolution through the development of nanomaterials but there has been a limitation to improve the sensors' sensitivity due to the low diffusion transport of biomolcules toward the sensing region. The research team developed a new biosensor platform using a spider web-shaped micro-magnetic pattern. When a rotating magnetic field is applied to a spider web-shaped magnetic pattern, it can attract biomolecules labeled with superparamagnetic particles faster to the sensor. The magnetic field based biosensor platform improves the collection capability of biomolecules and increases the speed and sensitivity of the biomolecules movement.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/04/170421090840.htm){:target="_blank" rel="noopener"}


