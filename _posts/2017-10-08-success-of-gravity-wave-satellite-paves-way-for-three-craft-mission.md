---
layout: post
title: "Success of gravity-wave satellite paves way for three-craft mission"
date: 2017-10-08
categories:
author: "Castelvecchi, Davide Castelvecchi, You Can Also Search For This Author In"
tags: []
---


LISA Pathfinder — shown before being encapsulated into a rocket for launch — allowed scientists to test technology for detecting gravitational waves. On 1 July, a satellite will wrap up its mission to test technology for the pioneering quest to measure gravitational ripples in the stillness of space. Over the past year, the craft has performed much better than many had hoped. “This is not the impossible task that some people believed it was.”  Currently set to fly in 2034, the full-scale Laser Interferometer Space Antenna (LISA) will be the space analogue of the Laser Interfero-meter Gravitational-Wave Observatory (LIGO), two machines in the United States — each with a pair of 4-kilometre-long arms — that first detected the ripples by ‘hearing’ the merger of two black holes. This puts LISA on schedule to be launched in 2034.

<hr>

[Visit Link](http://www.nature.com/news/success-of-gravity-wave-satellite-paves-way-for-three-craft-mission-1.22205){:target="_blank" rel="noopener"}


