---
layout: post
title: "Optics, nanotechnology combined to create low-cost sensor for gases"
date: 2015-04-03
categories:
author: David Stauth, Oregon State University
tags: [Sensor,Thin film,Plasmon,Gas,Gas detector,Applied and interdisciplinary physics,Chemistry,Technology]
---


Engineers have combined innovative optical technology with nanocomposite thin-films to create a new type of sensor that is inexpensive, fast, highly sensitive and able to detect and analyze a wide range of gases. The technology might find applications in everything from environmental monitoring to airport security or testing blood alcohol levels. The findings were just reported in the Journal of Materials Chemistry C.  University researchers are now seeking industrial collaborators to further perfect and help commercialize the system. After the thin film captures the gas molecules near the surface, the plasmonic materials act at a near-infrared range, help magnify the signal and precisely analyze the presence and amounts of different gases. Gas detection can be valuable in finding explosives, and new technologies such as this might find application in airport or border security.

<hr>

[Visit Link](http://phys.org/news347213624.html){:target="_blank" rel="noopener"}


