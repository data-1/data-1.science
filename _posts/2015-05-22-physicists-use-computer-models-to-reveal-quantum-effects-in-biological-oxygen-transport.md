---
layout: post
title: "Physicists Use Computer Models to Reveal Quantum Effects in Biological Oxygen Transport"
date: 2015-05-22
categories:
author: Trinity College Dublin
tags: [Density functional theory,Myoglobin,Quantum mechanics,Physics,Protein,Biochemistry,Biology,Electron,Physical sciences,Science,Chemistry,Branches of science]
---


This is very dangerous in the case of the iron-containing proteins involved in respiration, since such a union is irreversible and leads to the protein being poisoned and the individual ultimately asphyxiating. More specifically, computer simulations using the most widely-used theoretical approach (density-functional theory, 'DFT') that won the Nobel Prize for Chemistry in 1998, as well as many of its more advanced extensions, consistently predict that CO should bind to myoglobin much more readily than O 2 when the two molecules are both present. Dr O'Regan and his international collaborators used a special variety of DFT that is optimised for large systems (and on which Dr O'Regan has worked for a number of years) to model a large myoglobin structure. Dr O'Regan said: We have succeeded in showing that quantum mechanical effects that we more often think of arising in advanced technological materials can be critical in determining the energy differences that drive biochemical processes occurring in the body. It is remarkable that myoglobin seems to be extremely well adapted to exploit the specific Hund's exchange strength of atomic iron, an intrinsically quantum mechanical property, in order to strongly promote O 2 binding at the expense of CO.

<hr>

[Visit Link](http://phys.org/news324827298.html){:target="_blank" rel="noopener"}


