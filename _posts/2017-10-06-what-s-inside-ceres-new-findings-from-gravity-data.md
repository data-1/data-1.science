---
layout: post
title: "What's inside Ceres? New findings from gravity data"
date: 2017-10-06
categories:
author: "Elizabeth Landau"
tags: [Ceres (dwarf planet),Dawn (spacecraft),Planetary core,Planet,Planets,Space science,Astronomy,Planetary science,Outer space,Solar System,Bodies of the Solar System,Astronomical objects,Physical sciences,Science,Planemos]
---


This artist's concept shows a diagram of how the inside of Ceres could be structured, based on data about the dwarf planet's gravity field from NASA's Dawn mission. Since gravity dominates Dawn's orbit at Ceres, scientists can measure variations in Ceres' gravity by tracking subtle changes in the motion of the spacecraft. We have found that the divisions between different layers are less pronounced inside Ceres than the moon and other planets in our solar system, Park said. Scientists also found that high-elevation areas on Ceres displace mass in the interior. By combining this new information with previous data from Dawn about Ceres' surface composition, they can reconstruct that history: Water must have been mobile in the ancient subsurface, but the interior did not heat up to the temperatures at which silicates melt and a metallic core forms.

<hr>

[Visit Link](http://phys.org/news/2016-08-ceres-gravity.html){:target="_blank" rel="noopener"}


