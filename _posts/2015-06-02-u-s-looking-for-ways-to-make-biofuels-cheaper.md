---
layout: post
title: "U.S. looking for ways to make biofuels cheaper"
date: 2015-06-02
categories:
author: ""   
tags: [Biofuel,Fuels,Nature,Sustainable energy,Sustainable technologies,Energy,Energy and the environment,Sustainable development,Climate change mitigation,Natural resources,Chemical energy sources,Environmental technology]
---


U.S. looking for ways to make biofuels cheaper    by Daniel J. Graeber    Washington (UPI) Jul 16, 2013    The U.S. Energy Department said it was spending $6.3 million on research aimed at generating a biofuel that could be cost competitive by 2017. The funding is part of an effort to make biofuels competitive with regular fuels. The Energy Department said it wants to produce a drop-in biofuel that would cost about $3 per gallon by 2017. Energy Secretary Ernest Moniz said the government is looking for ways to increase energy security and reduce greenhouse-gas emissions by advancing a low-carbon economy. The transportation sector in the United States accounts for nearly 60 percent of domestic oil consumption and 30 percent of overall emissions.

<hr>

[Visit Link](http://www.biofueldaily.com/reports/US_looking_for_ways_to_make_biofuels_cheaper_999.html){:target="_blank" rel="noopener"}


