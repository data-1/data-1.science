---
layout: post
title: "Improving genome editing with drugs"
date: 2015-12-07
categories:
author: Gladstone Institutes 
tags: [CRISPR,Gladstone Institutes,Cell (biology),DNA,Genetics,CRISPR gene editing,Biotechnology,Biology,Life sciences,Biochemistry,Molecular biology,Branches of genetics]
---


However, although this method has incredible potential, the process is extremely inefficient. Currently, there is a trade-off with CRISPR: the technology is very precise, but it is also quite inefficient, says first author Chen Yu, a postdoctoral fellow at the Gladstone Institutes. We improved this by introducing small molecules that are able to maintain the precision of the technology while boosting its efficiency. Published in the journal Cell Stem Cell, the researchers, in collaboration with co-senior author Lei Stanley Qi, PhD, at Stanford University, successfully identified two small molecules that significantly improve the insertion of new genetic information into a cell's DNA. Yanxia Liu and Honglei Liu from Stanford University and Marie La Russa from the University of California, San Francisco also took part in the study.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/gi-ige020515.php){:target="_blank" rel="noopener"}


