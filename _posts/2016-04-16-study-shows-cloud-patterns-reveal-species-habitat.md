---
layout: post
title: "Study shows cloud patterns reveal species habitat"
date: 2016-04-16
categories:
author: NASA/Goddard Space Flight Center
tags: [Biodiversity,Ecology,Ecosystem,Earth phenomena,Systems ecology,Biogeochemistry,Science,Nature,Natural environment,Earth sciences]
---


Much of Earth's biodiversity is concentrated in areas where not enough is known about species habitats and their wider distributions, making management and conservation a challenge. To address the problem, scientists at the University at Buffalo and Yale University used NASA satellite data to study cloud cover, which they found can help identify the size and location of important animal and plant habitats. Clouds influence such environmental factors as rain, sunlight, surface temperature and leaf wetness -- all of which dictate where plants and animals can survive. As part of their study, researchers examined 15 years of data from NASA's Earth-orbiting Terra and Aqua satellites and built a database containing two images per day of cloud cover for nearly every square kilometer of the planet from 2000 to 2014. It is exciting to now be able to tap into this large stack of detailed data to support global biodiversity and ecosystem monitoring and conservation.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/nsfc-ssc041516.php){:target="_blank" rel="noopener"}


