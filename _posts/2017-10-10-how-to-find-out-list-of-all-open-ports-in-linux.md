---
layout: post
title: "How to Find Out List of All Open Ports in Linux"
date: 2017-10-10
categories:
author: "Aaron Kili"
tags: [Port (computer networking),Telecommunications,Information technology,Computer engineering,Computer standards,Protocols,Cyberspace,Wide area networks,Computer science,IT infrastructure,Computer architecture,Digital communication interfaces,Mass media technology,Internet Standards,Internet,Internet architecture,Data transmission,Networking standards,Computer networking,Network protocols,Internet protocols,Computing,Network architecture,Communications protocols,Computer data,Technology,Telecommunications standards]
---


In this article, we will briefly talk about ports in computer networking and move to how you can list all open ports in Linux. In computer networking, and more definitely in software terms, a port is a logical entity which acts as a endpoint of communication to identify a given application or process on an Linux operating system. Below are the different categories of ports:  0-1023 – the Well Known Ports, also referred to as System Ports. You can view a list of different applications and port/protocol combination in /etc/services file in Linux using cat command:  $ cat /etc/services OR $ cat /etc/services | less  Network Services and Ports  # /etc/services: # $Id: services,v 1.48 2009/11/11 14:32:31 ovasik Exp $ # # Network services, Internet style # IANA services version: last updated 2009-11-10 # # Note that it is presently the policy of IANA to assign a single well-known # port number for both TCP and UDP; hence, most entries here have two entries # even if the protocol doesn't support UDP operations. # The Registered Ports are those from 1024 through 49151 # The Dynamic and/or Private Ports are those from 49152 through 65535 # # Each line describes one service, and is of the form: # # service-name port/protocol [aliases ...] [# comment] tcpmux 1/tcp # TCP port service multiplexer tcpmux 1/udp # TCP port service multiplexer rje 5/tcp # Remote Job Entry rje 5/udp # Remote Job Entry echo 7/tcp echo 7/udp discard 9/tcp sink null discard 9/udp sink null systat 11/tcp users systat 11/udp users daytime 13/tcp daytime 13/udp qotd 17/tcp quote qotd 17/udp quote msp 18/tcp # message send protocol msp 18/udp # message send protocol chargen 19/tcp ttytst source chargen 19/udp ttytst source ftp-data 20/tcp ftp-data 20/udp # 21 is registered to ftp, but also used by fsp ftp 21/tcp ftp 21/udp fsp fspd ssh 22/tcp # The Secure Shell (SSH) Protocol ssh 22/udp # The Secure Shell (SSH) Protocol telnet 23/tcp telnet 23/udp  To list all open ports or currently running ports including TCP and UDP in Linux, we will use netstat, is a powerful tool for monitoring network connections and statistics.

<hr>

[Visit Link](http://www.tecmint.com/find-open-ports-in-linux/){:target="_blank" rel="noopener"}


