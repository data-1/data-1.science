---
layout: post
title: "Linguistics team using Ohio Supercomputer Center to translate lesser-known languages"
date: 2018-07-14
categories:
author: "Ross Bishoff, Ohio Supercomputer Center"
tags: [Graphics processing unit,Central processing unit,Language,Linguistics,Parsing,Branches of science,Cognitive science,Science,Cognition,Computer science,Technology,Computing]
---


As part of LORELEI, Schuler and his team are using the Ohio Supercomputer Center's Owens Cluster to develop a grammar acquisition algorithm to discover the rules of lesser-known languages, learning the grammars without supervision so disaster relief teams can react quickly. The computational requirements for learning grammar from statistics are tremendous, which is why we need a supercomputer, Schuler said. But using the GPUs on OSC's Owens System allows Jin to increase the number of categories greatly. Schuler's group used 60 GPUs on the Owens Cluster for seven days for four grammars of two languages, illustrating the importance of OSC's resources to the project. The ability to ask these kinds of questions and get answers is a relatively recent innovation that requires the high performance computing infrastructure OSC gives us.

<hr>

[Visit Link](https://phys.org/news/2017-11-linguistics-team-ohio-supercomputer-center.html){:target="_blank" rel="noopener"}


