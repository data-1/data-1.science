---
layout: post
title: "Paleo-engineering: New study reveals complexity of Triceratops' teeth"
date: 2015-07-21
categories:
author: Florida State University 
tags: [Tooth,Dinosaur,Triceratops,Reptile,Science,Wear,Paleontology,Mammal,Herbivore,Animals]
---


The teeth of most herbivorous mammals self wear with use to create complex file surfaces for mincing plants. It's just been assumed that dinosaurs didn't do things like mammals, but in some ways, they're actually more complex, Erickson said. Krick is an assistant professor of mechanical engineering at Lehigh University and specializes in a relatively new area of materials science called tribology. He discovered that Triceratops teeth were made of five layers of tissue. Each of those tissues does something, Erickson said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/fsu-pns060115.php){:target="_blank" rel="noopener"}


