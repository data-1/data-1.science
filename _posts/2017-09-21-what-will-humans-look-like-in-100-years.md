---
layout: post
title: "What will humans look like in 100 years?"
date: 2017-09-21
categories:
author: "Juan Enriquez"
tags: [TED (conference)]
---


We can evolve bacteria, plants and animals -- futurist Juan Enriquez asks: Is it ethical to evolve the human body? In a visionary talk that ranges from medieval prosthetics to present day neuroengineering and genetics, Enriquez sorts out the ethics associated with evolving humans and imagines the ways we'll have to transform our own bodies if we hope to explore and live in places other than Earth.

<hr>

[Visit Link](http://www.ted.com/talks/juan_enriquez_what_will_humans_look_like_in_100_years){:target="_blank" rel="noopener"}


