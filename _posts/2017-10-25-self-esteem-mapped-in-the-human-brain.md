---
layout: post
title: "Self-esteem mapped in the human brain"
date: 2017-10-25
categories:
author: University College London
tags: [Mental disorder,Psychiatry,Self-esteem,Health,News aggregator,Abnormal psychology,Mental disorders,Branches of science,Behavioural sciences,Neuroscience,Mental health,Cognitive science,Psychology]
---


The researchers used the new equation to identify signals in the human brain that explain why self-esteem goes up and down when we learn other people's judgments of us. They say the findings could help identify people at risk of psychiatric disorders. These social prediction errors -- the difference between expected and received feedback -- were key for determining self-esteem. They found that people who had greater fluctuations in self-esteem during the task also had lower self-esteem more generally and reported more symptoms of depression and anxiety. By combining our mathematical equation for self-esteem with brain scans in people as they found out whether other people liked them, we identified a possible marker for vulnerability to mental health problems.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171024103319.htm){:target="_blank" rel="noopener"}


