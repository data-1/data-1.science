---
layout: post
title: "2-D tin (stanene) without buckling: A possible topological insulator"
date: 2018-07-03
categories:
author: "Nagoya University"
tags: [Graphene,Single-layer materials,Materials,Artificial materials,Phases of matter,Technology,Goods (economics),Building engineering,Nanotechnology,Electromagnetism,Manufacturing,Chemical product engineering,Condensed matter,Physical chemistry,Physical sciences,Applied and interdisciplinary physics,Condensed matter physics,Materials science,Chemistry]
---


Nagoya University-led researchers produce 2D sheets of tin atoms predicted to have exotic uses in electronics. Because of relatively strong spin-orbit interactions for electrons in heavy elements, single-layer tin is predicted to be a topological insulator, also known as a quantum spin Hall (QSH) insulator. This, in theory, makes a single-layered topological insulator an ideal wiring material for nanoelectronics. Planar stanene has exciting prospects in electronics and computing. ###  The article, Large area planar stanene epitaxially grown on Ag(111), was published in 2D Materials at DOI:10.1088/2053-1583/aa9ea0.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/nu-2t011918.php){:target="_blank" rel="noopener"}


