---
layout: post
title: "Protecting quantum computing networks against hacking threats"
date: 2017-09-17
categories:
author: "University of Ottawa"
tags: [Computing,Security hacker,Quantum mechanics,Information,Quantum computing,American Association for the Advancement of Science,Quantum cloning,Research,Qubit,Technology,Information Age,Branches of science,Computer science,Science,Applied mathematics]
---


Even so, researchers at the University of Ottawa have uncovered clues that could help administrators protect quantum computing networks from external attacks. Once we were able to analyze the results, we discovered some very important clues to help protect quantum computing networks against potential hacking threats. Quantum systems were believed to provide perfectly secure data transmission because until now, attempts to copy the transmitted information resulted in an altered or deteriorated version of the original information, thereby defeating the purpose of the initial hack. Ensuring photons contain the largest amount of information possible and monitoring these noises in a secure channel should help strengthen quantum computing networks against potential hacking threats. Karimi and his team hope that their quantum hacking efforts could be used to study quantum communication systems, or more generally to study how quantum information travels across quantum computer networks.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/uoo-pqc020317.php){:target="_blank" rel="noopener"}


