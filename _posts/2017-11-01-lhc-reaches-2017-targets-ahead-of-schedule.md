---
layout: post
title: "LHC reaches 2017 targets ahead of schedule"
date: 2017-11-01
categories:
author: ""
tags: [Large Hadron Collider,CERN,Physics,Science,Technology,Particle physics]
---


Credit: Max Brice and Julien Ordan/CERN  Today, CERN Control Centre operators announced good news, the Large Hadron Collider (LHC) has successfully met its production target for 2017, delivering more than 45 inverse femtobarns to the experiments. This achievement was all the more impressive as it was ahead of schedule. An issue had developed with a small group of magnets known as 16L2 that was affecting machine performance. That being said, physicists are already looking to upgrades tens of years in the future and the physics potential that they bring. In the more immediate future, once the main proton physics run end this year, the LHC will have 15 days of special runs plus machine development before its winter shutdown begins on 11 December.

<hr>

[Visit Link](https://phys.org/news/2017-10-lhc.html){:target="_blank" rel="noopener"}


