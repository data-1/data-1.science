---
layout: post
title: "Shape-shifting frog discovered in Ecuadorian Andes"
date: 2015-07-21
categories:
author: Case Western Reserve University 
tags: [Pristimantis mutabilis,Frog,Animals,Organisms]
---


The researchers, Katherine and Tim Krynak, and colleagues from Universidad Indoamérica and Tropical Herping (Ecuador) co-authored a manuscript describing the new animal and skin texture plasticity in the Zoological Journal of the Linnean Society this week. The spines came back... we simply couldn't believe our eyes, our frog changed skin texture! They found the animals shift skin texture in a little more than three minutes. Carl R. Hutter, from the University of Kansas, studied the frog's calls, finding three songs the species uses, which differentiate them from relatives. Because the appearance of animals has long been one of the keys to identifying them as a certain species, the researchers believe their find challenges the system, particularly for species identified by one or just a few preserved specimens.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/cwru-sfd032315.php){:target="_blank" rel="noopener"}


