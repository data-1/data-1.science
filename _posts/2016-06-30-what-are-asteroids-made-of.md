---
layout: post
title: "What are asteroids made of?"
date: 2016-06-30
categories:
author: "Nancy Atkinson"
tags: [Asteroid,Near-Earth object,Comet,Potentially hazardous object,Asteroid belt,Jupiter,Astronomical objects,Outer space,Bodies of the Solar System,Physical sciences,Local Interstellar Cloud,Solar System,Nature,Astronomy,Space science,Planetary science]
---


The platinum group metals are some of the most rare and useful elements on Earth. The various elements that are found in asteroids. The force of the impact briefly turned the ice into water, which flowed across the surface, creating the gullies. Bright M (metallic) asteroids. Comets usually have tails, which are made from ice and debris sublimating as the comet gets close to the sun.

<hr>

[Visit Link](http://phys.org/news/2015-09-asteroids_1.html){:target="_blank" rel="noopener"}


