---
layout: post
title: "Extent of moon's giant volcanic eruption is revealed"
date: 2015-07-21
categories:
author: Durham University 
tags: [Volcano,ComptonBelkovich Thorium Anomaly,Types of volcanic eruptions,Moon,Lava,Volcanism,Planetary science,Space science,Planets of the Solar System,Astronomy,Physical sciences,Bodies of the Solar System,Astronomical objects known since antiquity,Geology,Outer space]
---


Scientists have produced a new map of the Moon's most unusual volcano showing that its explosive eruption spread debris over an area much greater than previously thought. The research used data from NASA's Lunar Prospector spacecraft which first spotted the volcanic site in 1999 when it detected an isolated deposit of thorium on the Moon's far-side between the Compton and Belkovich impact craters. Jack Wilson, a PhD student in Durham's Institute for Computational Cosmology, said he was surprised by the gigantic scale of the explosion. He said: Volcanoes were common in the early life of the Moon and in fact the dark 'seas' you can observe on the lunar surface were created by runny, iron-rich, lava that flooded large areas, filling in impact craters and low-lying ground. By mapping the radioactive content of the lava from this volcano we have been able to show that molten, radioactive rock was thrown far beyond the slopes of the volcano, reaching several hundred miles in one direction.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/du-eom031815.php){:target="_blank" rel="noopener"}


