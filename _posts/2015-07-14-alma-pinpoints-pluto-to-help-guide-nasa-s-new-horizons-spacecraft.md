---
layout: post
title: "ALMA pinpoints Pluto to help guide NASA's New Horizons spacecraft"
date: 2015-07-14
categories:
author: National Radio Astronomy Observatory
tags: [New Horizons,Pluto,Atacama Large Millimeter Array,Solar System,Astronomical objects,Planetary science,Bodies of the Solar System,Science,Outer space,Physical sciences,Space science,Astronomy]
---


Animated image of ALMA data showing the motion of the moon Charon around the icy dwarf planet Pluto. Credit: B. Saxton (NRAO/AUI/NSF)  To prepare for this first TCM, astronomers needed to pinpoint Pluto's position using the most distant and most stable reference points possible. Normally, stars at great distances are used by optical telescopes for astrometry (the positioning of things on the sky) since they change position only slightly over many years. For New Horizons, however, even more precise measurements were necessary to ensure its encounter with Pluto would be as on-target as possible. Though quasars appear very dim to optical telescopes, they are incredibly bright at radio wavelengths, particularly the millimeter wavelengths that ALMA can see.

<hr>

[Visit Link](http://phys.org/news326472245.html){:target="_blank" rel="noopener"}


