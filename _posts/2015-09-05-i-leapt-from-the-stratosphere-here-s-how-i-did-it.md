---
layout: post
title: "I leapt from the stratosphere. Here's how I did it"
date: 2015-09-05
categories:
author: Alan Eustace
tags: [Aviation,Sky,Aerostats,Balloons (aeronautics),Vehicle operation,Balloons,Aeronautics]
---


On October 24, 2014, Alan Eustace donned a custom-built, 235-pound spacesuit, attached himself to a weather balloon, and rose above 135,000 feet, from which point he dove to Earth, breaking both the sound barrier and previous records for high-altitude jumps. Hear his story of how -- and why.

<hr>

[Visit Link](http://www.ted.com/talks/alan_eustace_i_leapt_from_the_stratosphere_here_s_how_i_did_it){:target="_blank" rel="noopener"}


