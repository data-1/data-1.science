---
layout: post
title: "Orbital ATK Cygnus set to deliver research to space station"
date: 2017-10-08
categories:
author: "Jenny Howard"
tags: [Cygnus (spacecraft),International Space Station,NASA,Atmospheric entry,Spacecraft,Space science,Astronautics,Flight,Outer space,Spaceflight]
---


The Cygnus spacecraft reenters the Earth's atmosphere, as observed by Expedition 40 crewmembers aboard the space station. 3-D cell culturing in space may lead to improved drug development costs  Cells cultured in space spontaneously grow in 3-D, as opposed to cells cultured on Earth which grow in 2-D, resulting in characteristics more representative of how cells grow and function in living organisms. If investigators can identify these effects on the cell's growth, data will be used to help design environments on Earth which mimic microgravity, which could reduce the cost of drug development. Many crystal growth investigations, such as CLYC Crystal Growth and Detached Melt and Vapor Growth of InI, will occur within SUBSA Furnace and Inserts. The Thermal Protection Material Flight Test and Reentry Data Collection (RED-Data2) investigation studies a new type of recording device that rides alongside of a spacecraft reentering the Earth's atmosphere, recording data about the extreme conditions it encounters during reentry, something scientists have been unable to test on a large scale thus far.

<hr>

[Visit Link](https://phys.org/news/2017-03-orbital-atk-cygnus-space-station.html){:target="_blank" rel="noopener"}


