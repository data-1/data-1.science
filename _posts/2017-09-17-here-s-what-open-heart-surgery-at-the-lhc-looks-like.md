---
layout: post
title: "Here's what open-heart surgery at the LHC looks like"
date: 2017-09-17
categories:
author: ""
tags: [Compact Muon Solenoid,Large Hadron Collider,Technology,Particle physics]
---


The FPIX disks were manufactured by 19 institutes in the US. Credit: Maximilien Brice/CERN  The CMS tracking system is made of silicon sensors and has two components that perform a complementary roles: the inner of the two is called the Pixel Tracker and the outer one is the Strip Tracker. To be installed within CMS, the various components of the Pixel Tracker had to be lowered by crane down the 100-metre-deep shaft into the underground experimental cavern of CMS. They were then raised by a second crane onto the installation platform for insertion. Credit: Maximilien Brice/CERN  Explore further Cool running for Large Hadron Collider's CMS Tracker

<hr>

[Visit Link](https://phys.org/news/2017-03-open-heart-surgery-lhc.html){:target="_blank" rel="noopener"}


