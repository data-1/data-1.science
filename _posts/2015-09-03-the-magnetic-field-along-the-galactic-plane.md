---
layout: post
title: "The magnetic field along the Galactic plane"
date: 2015-09-03
categories:
author: "$author"   
tags: [Milky Way,Planck (spacecraft),Cosmic dust,Interstellar medium,Galaxy,Polarization (waves),Magnetism,Star,Magnetic field,Astronomical objects,Sky,Nature,Science,Electromagnetism,Outer space,Physics,Astrophysics,Physical sciences,Space science,Astronomy]
---


While the pastel tones and fine texture of this image may bring to mind brush strokes on an artist’s canvas, they are in fact a visualisation of data from ESA’s Planck satellite. The image portrays the interaction between interstellar dust in the Milky Way and the structure of our Galaxy’s magnetic field. Interstellar clouds of gas and dust are also threaded by the Galaxy’s magnetic field, and dust grains tend to align their longest axis at right angles to the direction of the field. Scientists in the Planck collaboration are using the polarised emission of interstellar dust to reconstruct the Galaxy’s magnetic field and study its role in the build-up of structure in the Milky Way, leading to star formation. This image shows the intricate link between the magnetic field and the structure of the interstellar medium along the plane of the Milky Way.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2014/12/The_magnetic_field_along_the_Galactic_plane){:target="_blank" rel="noopener"}


