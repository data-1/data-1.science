---
layout: post
title: "Physicists break distance record for quantum teleportation"
date: 2015-09-27
categories:
author: National Institute Of Standards
tags: [Quantum teleportation,Optical fiber,Photon,Science,Telecommunications,Physics,Technology]
---


The experiment confirmed that quantum communication is feasible over long distances in fiber. The new record, described in Optica, involved the transfer of quantum information contained in one photon—its specific time slot in a sequence— to another photon transmitted over 102 km of spooled fiber in a NIST laboratory in Colorado. Various quantum states can be used to carry information; the NTT/NIST experiment used quantum states that indicate when in a sequence of time slots a single photon arrives. The teleportation method is novel in that four of NIST's photon detectors were positioned to filter out specific quantum states. Explore further Longer distance quantum teleportation achieved

<hr>

[Visit Link](http://phys.org/news/2015-09-physicists-distance-quantum-teleportation.html){:target="_blank" rel="noopener"}


