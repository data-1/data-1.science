---
layout: post
title: "The effect of the winds of Mars"
date: 2015-09-03
categories:
author: "$author"   
tags: [Mars,Impact crater,Earth,Wind,Arabia Terra,Planets of the Solar System,Earth sciences,Planemos,Astronomical objects,Space science,Planetary science,Astronomical objects known since antiquity,Bodies of the Solar System,Planets,Outer space,Astronomy,Terrestrial planets,Solar System]
---


Here on Earth, we are used to the wind shaping our environment over time, forming smooth, sculpted rocks and rippling dunes. On the Red Planet, strong winds whip dust and sand from the surface into a frenzy, moving it across the planet at high speeds. The craters in this image, caused by impacts in Mars’ past, all show different degrees of erosion. Some still have defined outer rims and clear features within them, while others are much smoother and featureless, almost seeming to run into one another or merge with their surroundings. This colour image was taken by Mars Express’s High Resolution Stereo Camera on 19 November 2014, during orbit 13728.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2015/06/The_effect_of_the_winds_of_Mars){:target="_blank" rel="noopener"}


