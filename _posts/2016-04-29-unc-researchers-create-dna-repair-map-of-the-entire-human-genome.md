---
layout: post
title: "UNC researchers create DNA repair map of the entire human genome"
date: 2016-04-29
categories:
author: University of North Carolina Health Care
tags: [DNA,DNA repair,Genome,Cell (biology),Gene,Human genome,Genetics,Cancer,Aziz Sancar,Chemotherapy,Cisplatin,DNA damage (naturally occurring),Enzyme,Molecular biology,Biochemistry,Biology,Chemistry,Life sciences,Biotechnology]
---


The cancer drugs aren't as effective as patients need. Now we can say to a fellow scientist, 'tell us the gene you're interested in or any spot on the genome, and we'll tell you how it is repaired,' said Sancar, co-senior author and member of the UNC Lineberger Comprehensive Cancer Center. The fragment was stable enough for Sancar's lab to sequence it. Because UV radiation and common chemotherapy drugs such as cisplatin cause DNA damage in similar ways, Sancar's team is now using their new DNA excision repair method - called XR-Seq - to study cells affected by cisplatin. ###  Other authors on the Genes & Development paper are Christopher Selby, PhD, a research instructor in the Sancar lab, and Jason Lieb, PhD, a former UNC researcher who is now a professor of human genetics at the University of Chicago.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/uonc-urc050115.php){:target="_blank" rel="noopener"}


