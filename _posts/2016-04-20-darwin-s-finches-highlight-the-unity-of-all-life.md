---
layout: post
title: "Darwin's finches highlight the unity of all life"
date: 2016-04-20
categories:
author: Frank Nicholas, The Conversation
tags: [Darwins finches,Species,Evolution,Biology,Biological evolution,Evolutionary biology]
---


Presumably, it was his doubt about the available evidence that resulted in Darwin making no mention of Galapagos finches in any edition of Origin of Species. Why, then, do people now label them as Darwin's finches, and why are these finches now regarded as a classical textbook example of his theory of evolution by natural selection? Credit: Wikimedia  Paragons of evolution  Despite not mentioning Galapagos finches, Darwin did make much use of evidence from other Galapagos species (especially mockingbirds) in Origin of Species. In the second half of the 20th century, classic research by Princeton University's Peter and Rosemary Grant provided evidence of quite strong natural selection on beak size and shape. It is an extraordinary illustration of the underlying unity of all life on Earth that Leif Andersson and his colleagues have shown that the ALX1 gene also has a major effect on beak shape in finches, and that this gene has been subject to natural selection during the evolution of the Galapagos finches.

<hr>

[Visit Link](http://phys.org/news347263552.html){:target="_blank" rel="noopener"}


