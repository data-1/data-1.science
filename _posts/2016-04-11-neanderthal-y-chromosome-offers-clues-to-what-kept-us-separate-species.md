---
layout: post
title: "Neanderthal Y chromosome offers clues to what kept us separate species"
date: 2016-04-11
categories:
author: Cell Press
tags: [Gene,Genetics,Neanderthal,Human,Y chromosome,Biology,Life sciences,Branches of genetics,Biotechnology]
---


Researchers reporting in the American Journal of Human Genetics, published by Cell Press, have completed the first in-depth genetic analysis of a Neanderthal Y chromosome. The Y chromosome was the main component remaining to be analyzed from the Neanderthal genome, the researchers say. Their analysis suggests that Neanderthals and modern humans diverged almost 590,000 years ago, consistent with earlier evidence. The functional nature of the mutations we found suggests to us that the Y chromosome may have played a role in barriers to gene flow, Bustamante says. American Journal of Human Genetics, Mendez et al.: The divergence of Neanderthal and modern human Y chromosomes http://dx.doi.org/10.1016/j.ajhg.2016.02.023  The American Journal of Human Genetics (@AJHGNews), published by Cell Press for the American Society of Human Genetics, is a monthly journal that provides a record of research and review relating to heredity in humans and to the application of genetic principles in medicine and public policy, as well as in related areas of molecular and cell biology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/cp-nyc033116.php){:target="_blank" rel="noopener"}


