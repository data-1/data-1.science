---
layout: post
title: "Bioluminescent sensor causes brain cells to glow in the dark"
date: 2017-03-17
categories:
author: "Vanderbilt University"
tags: [Bioluminescence,Optogenetics,Neuron,Luciferase,Neuroscience,Biology,Biotechnology,Chemistry,Life sciences]
---


The new wave is to use optical techniques to record the activity of hundreds of neurons at the same time, said Carl Johnson, Stevenson Professor of Biological Sciences, who headed the effort. Based on their research on bioluminescence in a scummy little organism, the green alga Chlamydomonas, that nobody cares much about Johnson and his colleagues realized that if they could combine luminescence with optogenetics -- a new biological technique that uses light to control cells, particularly neurons, in living tissue -- they could create a powerful new tool for studying brain activity. The light required to produce the fluorescence interferes with the light required to control the cells, said Johnson. The researchers picked calcium ions because they are involved in neuron activation. They tested their new calcium sensor with one of the optogenetic probes (channelrhodopsin) that causes the calcium ion channels in the neuron's outer membrane to open, flooding the cell with calcium.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/vu-bsc102716.php){:target="_blank" rel="noopener"}


