---
layout: post
title: "Water and sunlight the formula for sustainable fuel"
date: 2015-07-24
categories:
author: Australian National University 
tags: [Photosynthesis,Hydrogen,Water,Fuel,Water splitting,Biology,Chemistry,Nature,Energy,Physical sciences,Chemical substances,Technology,Materials]
---


Water is abundant and so is sunlight. Co-researcher Professor Ron Pace said the research opened up new possibilities for manufacturing hydrogen as a cheap and clean source of fuel. The team modified a much-researched and ubiquitous protein, Ferritin, which is present in almost all living organisms. The possibilities inspired visionary researcher Associate Professor Warwick Hillier, who led the research group until his death from brain cancer, earlier this year. It would be a self-replicating system – all you need to do is shine light on it, Dr Hingorani said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/anu-was082014.php){:target="_blank" rel="noopener"}


