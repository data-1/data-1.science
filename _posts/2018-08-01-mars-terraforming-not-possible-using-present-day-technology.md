---
layout: post
title: "Mars terraforming not possible using present-day technology"
date: 2018-08-01
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Mars,Terraforming of Mars,Atmosphere of Mars,MAVEN,Atmosphere,Greenhouse effect,Water,Terraforming,Atmosphere of Earth,Carbon dioxide,Earth,Earth sciences,Physical sciences,Planets of the Solar System,Astronomy,Planets,Nature,Planetary science,Space science,Outer space,Astronomical objects known since antiquity,Bodies of the Solar System,Solar System,Astronomical objects,Terrestrial planets]
---


Proponents of terraforming Mars propose releasing gases from a variety of sources on the Red Planet to thicken the atmosphere and increase the temperature to the point where liquid water is stable on the surface. The researchers analyzed the abundance of carbon-bearing minerals and the occurrence of CO2 in polar ice using data from NASA's Mars Reconnaissance Orbiter and Mars Odyssey spacecraft, and used data on the loss of the Martian atmosphere to space by NASA's MAVEN (Mars Atmosphere and Volatile Evolution) spacecraft. Although Mars has significant quantities of water ice that could be used to create water vapor, previous analyses show that water cannot provide significant warming by itself; temperatures do not allow enough water to persist as vapor without first having significant warming by CO2, according to the team. However, vaporizing the ice caps would only contribute enough CO2 to double the Martian pressure to 1.2 percent of Earth's, according to the new analysis. Using the recent NASA spacecraft observations of mineral deposits, the team estimates the most plausible amount will yield less than 5 percent of the required pressure, depending on how extensive deposits buried close to the surface may be.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/nsfc-mtn073018.php){:target="_blank" rel="noopener"}


