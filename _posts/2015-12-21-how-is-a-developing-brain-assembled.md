---
layout: post
title: "How is a developing brain assembled?"
date: 2015-12-21
categories:
author: NIH/National Institute of Biomedical Imaging & Bioengineering
tags: [Brain,Neuron,National Institutes of Health,Embryo,Medical research,Caenorhabditis elegans,Neuroscience,Life sciences]
---


One significant challenge is determining the formation of complex neuronal structures made up of billions of cells in the human brain. Shroff and his team at NIBIB, in collaboration with Daniel Colon-Ramos at Yale University and Zhirong Bao at Sloan-Kettering, tackled this problem by developing new microscopes that improved the speed and resolution at which they could image worm embryonic development. Understanding the mechanisms that move neurons to their final destination is an important factor in understanding how brains form--and is difficult to determine without knowing where and how a neuron is moving. In addition, users can also mark cells or structures within the worm embryo they want the program to track, allowing the users to follow the position of a cell as it moves and grows in the developing embryo. This feature could help scientists understand how certain cells develop into neurons, as opposed to other types of cells, and what factors influence the development of the brain and neuronal structure.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/niob-hia120415.php){:target="_blank" rel="noopener"}


