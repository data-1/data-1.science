---
layout: post
title: "Comet landing named Physics World 2014 Breakthrough of the Year – Physics World"
date: 2016-04-12
categories:
author: Hamish Johnston
tags: [Rosetta (spacecraft),Philae (spacecraft),Neutrino,Nuclear fusion,National Ignition Facility,Astronomy,Science,Nature,Physical sciences,Physics]
---


The Physics World 2014 Breakthrough of the Year goes to ESA’s Rosetta mission for being the first to land a spacecraft on a comet. Launched in 2004, Rosetta itself reached the comet after completing a journey of 6.4 billion km that involved three gravity-assisted fly-bys of Earth and one of Mars (see “Rosetta scientists land probe on comet for first time”). Although researchers have measured the magnetic field of an individual electron, the magnetic interactions between two electrons have proved much more difficult to observe. The spin waves used in Khitun and colleagues’ magnetic holography device have much shorter wavelengths than visible light, and could therefore be used to store data at higher densities. This supernova was simulated by Gregori, Meinecke and colleagues, who fired three laser beams onto a tiny carbon rod in an argon-filled chamber.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/YC360-b_MCw/comet-landing-named-physics-world-2014-breakthrough-of-the-year){:target="_blank" rel="noopener"}


