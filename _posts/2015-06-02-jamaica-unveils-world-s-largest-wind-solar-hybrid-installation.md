---
layout: post
title: "Jamaica Unveils World’s Largest Wind-Solar Hybrid Installation"
date: 2015-06-02
categories:
author: Beverley Mitchell
tags: []
---


The world’s largest wind and solar hybrid renewable energy project was recently put into operation in Kingston, Jamaica. Expected to generate approximately 106,000kWh annually with a return on investmentin less than four years, the plant should save the firm approximately $2 million in energy costs over the course of its 25-year lifetime. Continue reading below Our Featured Videos  Consisting of 50 SolarMills delivering 25 kW of wind power and 55kW of solar, the installation is the largest hybrid solar and wind installation in the world. Occupying a footprint roughly the size of a solar panel, each SolarMill provides the highest energy density currently available in the renewable market. Photos by WindStream Technologies

<hr>

[Visit Link](http://inhabitat.com/jamaica-unveils-worlds-largest-wind-solar-hybrid-installation/){:target="_blank" rel="noopener"}


