---
layout: post
title: "Synthetic biology needs robust safety mechanisms before real world application"
date: 2016-07-02
categories:
author: ""
tags: [Synthetic biology,Microorganism,Branches of science,Science,Biotechnology,Technology,Biology,Life sciences]
---


Synthetic biology involves engineering microbes like bacteria to program them to behave in certain ways. Synthetic biology has now reached a stage where it's ready to move out of the lab and into the real world, to be used in patients and in the field. The more we discovered about microbes, the easier it was to program them. When scientists build synthetic microbes, they can program in mechanisms called kill switches that cause the microbes to self-destruct if their environment changes in certain ways. Explore further Bacteria's conflicts fuel synthetic ecology research  More information: Synthetic biology expands chemical control of microorganisms by Tyler J Ford and Pamela A Silver (Current Opinion in Chemical Biology, Volume 28 (October 2015) Synthetic biology expands chemical control of microorganisms by Tyler J Ford and Pamela A Silver ( DOI: 10.1016/j.cbpa.2015.05.012 ).

<hr>

[Visit Link](http://phys.org/news/2015-09-synthetic-biology-robust-safety-mechanisms.html){:target="_blank" rel="noopener"}


