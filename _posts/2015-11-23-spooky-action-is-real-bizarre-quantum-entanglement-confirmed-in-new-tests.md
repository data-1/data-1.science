---
layout: post
title: "Spooky Action Is Real: Bizarre Quantum Entanglement Confirmed in New Tests"
date: 2015-11-23
categories:
author: Tia Ghose
tags: [Quantum entanglement,Bells theorem,Hidden-variable theory,Quantum mechanics,Action at a distance,Photon,Physical sciences,Applied and interdisciplinary physics,Scientific theories,Science,Physics,Theoretical physics]
---


They found that the Schrödinger wave equation, the fundamental quantum mechanics equation, could not describe the individual state or position of some groups of particles, dubbed entangled particles, until each individual particle was measured. If spooky action were real, Bell proposed, then entangled particles measured some distance apart would have correlated states more than a certain percentage of the time. And if some hidden variable were affecting these seemingly entangled particles, then entangled particles would have correlated states less than that fraction of the time. One is that detectors used to measure entangled particles such as photons often miss many of the particle duos. Closing the loopholes  Now, researchers are starting to close those loopholes.

<hr>

[Visit Link](http://www.livescience.com/52811-spooky-action-is-real.html){:target="_blank" rel="noopener"}


