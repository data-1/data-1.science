---
layout: post
title: "Why don't plants get sunburn?"
date: 2016-07-21
categories:
author: "Nigel Paul, The Conversation"
tags: [Ultraviolet,Sunscreen,Sunburn,Ozone depletion,Sun tanning,Ozone layer]
---


Others need some exposure to sun to induce protective skin pigments by developing a sun tan. Is there a plant equivalent to sunburn or to the protective pigments we have in our skin? Research back in the 1980s and 1990s showed that the high levels of UVB that would result from ozone depletion could directly damage photosynthesis. If plants produce their sunscreens based on their exposure to UV, how do they detect that exposure? And the more we understand these responses, the more we can use that knowledge to produce more sustainable crops, improving their quality and reducing the use of pesticides.

<hr>

[Visit Link](http://phys.org/news/2016-07-dont-sunburn.html){:target="_blank" rel="noopener"}


