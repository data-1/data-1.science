---
layout: post
title: "INFOGRAPHIC: How wind turbines actually work"
date: 2016-03-24
categories:
author:  
tags: []
---


What makes these contraptions so interesting is their mystique. What’s their purpose? Why do they only exist in the middle of nowhere? To uncover the mystery, Save On Energy put together this fun infographic to explain how wind turbines work in a way that’s easy and accessible. Continue reading below Our Featured Videos  + Save On Energy  The article above was submitted to us by an Inhabitat reader.

<hr>

[Visit Link](http://inhabitat.com/infographic-how-wind-turbines-actually-work/){:target="_blank" rel="noopener"}


