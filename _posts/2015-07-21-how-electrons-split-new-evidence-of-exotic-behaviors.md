---
layout: post
title: "How electrons split: New evidence of exotic behaviors"
date: 2015-07-21
categories:
author: ""   
tags: [Superconductivity,High-temperature superconductivity,Electron,Cuprate superconductor,Particle physics,Electric charge,Physics,Applied and interdisciplinary physics,Materials science,Condensed matter,Chemistry,Physical sciences,Quantum mechanics,Theoretical physics,Phases of matter,Materials,Electromagnetism,Physical chemistry,Condensed matter physics]
---


However, in certain materials where the electrons are constrained in a quasi one-dimensional world, they appear to split into a magnet and an electrical charge, which can move freely and independently of each other. A team lead by EPFL scientists now has uncovered new evidence showing that this can happen in quasi two-dimensional magnetic materials. Henrik M. Ronnow and Bastien Dalla Piazza at EPFL and Martin Mourigal (recently appointed Assistant professor at Georgia Tech) have now led a study that provides both experimental and theoretical evidence showing that this exotic split of the electrons into fractional particles actually does take place in two dimensions. This work marks a new level of understanding in one of the most fundamental models in physics, says Henrik M. Ronnow. It also lends new support for Anderson's theory of high-temperature superconductivity, which, despite twenty-five years of intense research, remains one of the greatest mysteries in the discovery of modern materials.

<hr>

[Visit Link](http://www.spacedaily.com/reports/How_electrons_split_New_evidence_of_exotic_behaviors_999.html){:target="_blank" rel="noopener"}


