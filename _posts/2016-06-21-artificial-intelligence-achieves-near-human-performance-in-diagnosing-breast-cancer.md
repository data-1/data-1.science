---
layout: post
title: "Artificial intelligence achieves near-human performance in diagnosing breast cancer"
date: 2016-06-21
categories:
author: "Beth Israel Deaconess Medical Center"
tags: [Pathology,Artificial intelligence,Machine learning,Deep learning,Cancer,Branches of science,Cognitive science,Technology,Clinical medicine,Science]
---


Identifying the presence or absence of metastatic cancer in a patient's lymph nodes is a routine and critically important task for pathologists, Beck explained. In an objective evaluation in which researchers were given slides of lymph node cells and asked to determine whether or not they contained cancer, the team's automated diagnostic method proved accurate approximately 92 percent of the time, explained Khosla, adding, This nearly matched the success rate of a human pathologist, whose results were 96 percent accurate. But the truly exciting thing was when we combined the pathologist's analysis with our automated computational diagnostic method, the result improved to 99.5 percent accuracy, said Beck. The team trained the computer to distinguish between cancerous tumor regions and normal regions based on a deep multilayer convolutional network. This has been a big mission in the field of pathology for more than 30 years.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/06/160620085204.htm){:target="_blank" rel="noopener"}


