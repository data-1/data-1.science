---
layout: post
title: "Mapping wood production in European forests"
date: 2016-06-30
categories:
author: "European Forest Institute"
tags: [Wood,Forest,Privacy,Technology]
---


The map shows predicted wood production (in cubic meters per ha of land per year) in Europe averaged over the period 2000-2010. Credit: 2015 Elsevier B.V. The benefits that forests provide to people are getting increasing attention in scientific studies and decision making alike. Wood, or timber, production is one of the major benefits and it provides an important raw material that can be used for various purposes. In a recent study, a group of scientists, led by Dr. Hans Verkerk (European Forest Institute), investigated spatial patterns in wood production and, for the first time, developed high-resolution wood production maps for European forests. Explore further Can we increase harvest of woody biomass from European forests?

<hr>

[Visit Link](http://phys.org/news/2015-09-wood-production-european-forests.html){:target="_blank" rel="noopener"}


