---
layout: post
title: "How the insights of the Large Hadron Collider are being made open to everyone"
date: 2017-09-16
categories:
author: "Virginia Barbour, The Conversation"
tags: [CERN,Large Hadron Collider,Open access,Research,Physics,Science,Technology]
---


It's a worldwide collaboration of more than 3,000 libraries (including six in Australia), key funding agencies and research centres in 44 countries, together with three intergovernmental organisations. Open science  The concept of sharing research is not new in physics. The main purpose of the web was to enable researchers contributing to CERN from all over the world share documents, including scientific drafts, no matter what computer systems they were using. Individual papers and the connections between them were only as good as the physical library, with its paper journals, that academics had access to. So in the same way that CERN itself is an example of the power of international collaboration to ask some of the fundamental scientific questions of our time, SCOAP³ provides a way to ensure that the answers, whatever they are, are available to everyone, forever.

<hr>

[Visit Link](https://phys.org/news/2017-01-insights-large-hadron-collider.html){:target="_blank" rel="noopener"}


