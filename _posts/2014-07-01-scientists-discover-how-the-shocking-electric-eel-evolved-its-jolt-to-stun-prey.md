---
layout: post
title: "Scientists Discover How the Shocking Electric Eel Evolved its Jolt to Stun Prey"
date: 2014-07-01
categories:
author: Science World Report
tags: [Evolution,Electric organ (fish),Electric fish,Genetics,Cell (biology),Fish,DNA sequencing,Organism,Skeletal muscle,Human,Organ (biology),Biotechnology,Life sciences,Biology,Nature,Biological evolution,Evolutionary biology]
---


Now, scientists have uncovered the evolutionary origins of this electric field and have found that the eel's charge actually evolved from a muscle. In fact, worldwide there are hundreds of electric fish in six broad lineages. In the end, they found that electric organs in fish worldwide us the same genetic tools and cellular and developmental pathways to independently create the electric organ. Not only did this organ arise independently, though; the fish also used the same genetic toolbox in order to create their electric organ. By learning how nature does this, we may be able to manipulate the process with muscle in other organisms and, in the near future, perhaps use the tools of synthetic biology to create electrocytes for generating electric power in bionic devices within the human body or for uses we have not thought of yet.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15696/20140627/scientists-discover-shocking-electric-eel-evolved-jolt-stun-prey.htm){:target="_blank" rel="noopener"}


