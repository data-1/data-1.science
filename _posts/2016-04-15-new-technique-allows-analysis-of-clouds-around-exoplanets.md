---
layout: post
title: "New technique allows analysis of clouds around exoplanets"
date: 2016-04-15
categories:
author: Helen Knight, Massachusetts Institute Of Technology
tags: [Exoplanet,Kepler-7b,Kepler space telescope,Transiting Exoplanet Survey Satellite,Cloud,Atmosphere,Space science,Astronomy,Planetary science,Physical sciences,Outer space,Planets,Exoplanetology,Science,Nature,Stellar astronomy,Planemos,Astronomical objects]
---


Statistical comparison of more than 1,000 atmospheric models show that these clouds are most likely made of Enstatite, a common Earth mineral that is in vapor form at the extreme temperature on Kepler-7b. In a paper to be published in the Astrophysical Journal, researchers in the Department of Earth, Atmospheric, and Planetary Sciences (EAPS) at MIT describe a technique that analyzes data from NASA's Kepler space observatory to determine the types of clouds on planets that orbit other stars, known as exoplanets. Modeling cloud formation  To find out if this data could be used to determine the composition of these clouds, the MIT researchers studied the light signal from Kepler-7b. What's more, the method could be used to study the properties of clouds on different types of planet, Lewis says: It's one of the few methods out there that can help you determine if a planet even has an atmosphere, for example. The researchers hope to use the method to analyze data from NASA's follow-up to the Kepler mission, known as K2, which began studying different patches of space last June.

<hr>

[Visit Link](http://phys.org/news344586304.html){:target="_blank" rel="noopener"}


