---
layout: post
title: "Should the Higgs boson have caused our Universe to collapse?"
date: 2014-06-24
categories:
author: Royal Astronomical Society
tags: [Universe,BICEP and Keck Array,Physical cosmology,Inflation (cosmology),Higgs boson,Particle physics,Physics,Astrophysics,Nature,Cosmology,Science,Astronomy,Physical sciences]
---


Credit: Robert Hogan, Kings College London  (Phys.org) —British cosmologists are puzzled: they predict that the Universe should not have lasted for more than a second. To do this, they combined the results with recent advances in particle physics. The telescope has led to significant new results on the early universe. The problem is that the BICEP2 results predict that the universe would have received large 'kicks' during the cosmic inflation phase, pushing it into the other valley of the Higgs field within a fraction of a second. If not, there must be some other, as yet unknown, process which prevented the universe from collapsing.

<hr>

[Visit Link](http://phys.org/news322807315.html){:target="_blank" rel="noopener"}


