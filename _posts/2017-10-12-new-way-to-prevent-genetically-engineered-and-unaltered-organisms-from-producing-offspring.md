---
layout: post
title: "New way to prevent genetically engineered and unaltered organisms from producing offspring"
date: 2017-10-12
categories:
author: "University Of Minnesota"
tags: [Genetic engineering,Genetics,Gene,Reproduction,Invasive species,Biology,Life sciences]
---


A major obstacle to applying genetic engineering to benefit humans and the environment is the risk that organisms whose genes have been altered might produce offspring with their natural counterparts, releasing the novel genes into the wild. The approach, called synthetic incompatibility, effectively makes engineered organisms a separate species unable to produce viable offspring with their wild or domesticated relatives. Synthetic incompatibility has applications in controlling or eradicating invasive species, crop pests and disease-carrying insects as well as preventing altered genes from escaping from genetically modified crops into other plant populations. This approach is particularly valuable because we do not introduce any toxic genes, said Maciej Maselko, a postdoctoral scholar from Smanski's lab who performed the work. The next step, Smanski said, is to demonstrate the approach can work in organisms other than yeast We're working on moving into model fish, insects, nematodes and plants, he said.

<hr>

[Visit Link](https://phys.org/news/2017-10-genetically-unaltered-offspring.html){:target="_blank" rel="noopener"}


