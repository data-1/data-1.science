---
layout: post
title: "Plant gravity"
date: 2016-01-27
categories:
author: "$author" 
tags: [Plant,Root,Agriculture,Plant development,European Space Agency,Plants,Botany]
---


How do plants know which way is up? This image shows a lentil seedling root that grew on the International Space Station before being preserved in resin and cut along its length for analysis. The purple dots are starch-filled statoliths that usually drop towards gravity, but this plant grew in space and the statoliths are floating in the middle of their cells. Gravi-2 continued an earlier experiment that examined the limits of how plants perceive gravity, with this second experiment looking in particular at how calcium is used by plants to regulate growth. On Earth, soluble calcium spreads to plant roots and is considered an important part of plant growth because they respond to environmental signals.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2016/01/Plant_gravity){:target="_blank" rel="noopener"}


