---
layout: post
title: "Decades of data on world's oceans reveal a troubling oxygen decline"
date: 2018-08-15
categories:
author: "Georgia Institute of Technology"
tags: [Ocean,Water,Climate variability and change,Sea surface temperature,Oxygen,Hydrology,Environmental engineering,Earth phenomena,Hydrography,Natural environment,Environmental science,Earth sciences,Physical geography,Applied and interdisciplinary physics,Nature,Oceanography]
---


The study, which was published April in Geophysical Research Letters, was sponsored by the National Science Foundation and the National Oceanic and Atmospheric Administration. But the data showed that ocean oxygen was falling more rapidly than the corresponding rise in water temperature. Ocean currents then mix that more highly oxygenated water with subsurface water. But rising ocean water temperatures near the surface have made it more buoyant and harder for the warmer surface waters to mix downward with the cooler subsurface waters. OCE-1357373 and the National Oceanic and Atmospheric Administration under Grant No.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-05/giot-dod050417.php){:target="_blank" rel="noopener"}


