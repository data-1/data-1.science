---
layout: post
title: "Revolutionizing nuclear waste reprocessing and saving money: A 'molecular trap' for capturing radioactive iodides in nuclear waste"
date: 2017-11-19
categories:
author: "Rutgers University"
tags: [Radioactive waste,Nuclear reprocessing,News aggregator,Nuclear fuel,Recycling,Fuel,Manufacturing,Nuclear power,Forms of energy,Nature,Energy,Nuclear chemistry,Physical sciences,Materials,Energy technology,Radioactivity,Nuclear technology,Nuclear energy,Chemistry,Nuclear physics]
---


Seeking a better way to capture radioactive iodides in spent nuclear reactor fuel, Rutgers-New Brunswick scientists have developed an extremely efficient molecular trap that can be recycled and reused. Li is corresponding author of a study on molecular traps for nuclear fuel reprocessing that was published in Nature Communications. When spent fuel is reprocessed, radioactive molecular iodine and organic iodide gases that pose cancer and environmental risks must be captured and sequestered. So Rutgers and other researchers developed a molecular trap that is made of a highly porous metal-organic framework. Its performance exceeds the standard set by nuclear industry rules, which require waste reprocessing plants to remove more than 99.9 percent of radioactive iodides from spent nuclear fuel rods.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/11/171101092019.htm){:target="_blank" rel="noopener"}


