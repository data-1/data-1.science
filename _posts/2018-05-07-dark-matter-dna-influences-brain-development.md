---
layout: post
title: "‘Dark matter’ DNA influences brain development"
date: 2018-05-07
categories:
author: "Maxmen, Amy Maxmen, You Can Also Search For This Author In"
tags: []
---


The conundrum has centred on DNA sequences that do not encode proteins, and yet remain identical across a broad range of animals. Now I’m like, dude, it took 14 years to figure this out,” says Gill Bejerano, a genomicist at Stanford University in California, who described ultraconserved elements in 20042. Even though the sequences do not encode proteins, they thought, their functions must be so vital that they cannot tolerate imperfection. But this hypothesis hit a road block in 2007, when a team reported knocking out four ultraconserved elements in mice — and found that the animals looked fine and reproduced normally3. Mice lacking certain sequences had abnormally low numbers of brain cells that have been implicated in the progression of Alzheimer’s disease.

<hr>

[Visit Link](http://www.nature.com/articles/d41586-018-00920-x?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


