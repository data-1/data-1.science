---
layout: post
title: "India adopts a comprehensive open source policy"
date: 2015-08-07
categories:
author: "Mark Bohannon"
tags: [Free and open-source software,Open-source software,Free software,Computing,Technology]
---


The Government of India (GOI) has adopted a comprehensive and supportive open source policy. It is centered on three key areas:  Digital Infrastructure as a Utility to Every Citizen  Governance & Services on Demand  Digital Empowerment of Citizens  Citizen engagement  Those objectives are consistent with one of the other exciting trends I’m seeing: governments using open source software, as a key component of 'digital agenda' initiatives that include open standards and open data policies, to enhance civic engagement. The GOI policy  In many ways, the GOI policy is consistent with these trends we see around the world. These policies are important to level the playing field, not merely highlighting the benefits of open source to governments (saying it’s ok to use it) but also providing meaningful answers to commonly asked questions by government IT professionals. Secretary Sharma went on to say: It is clarified that the policy does not make it mandatory for all future applications and services to be designed using the open source software (OSS).

<hr>

[Visit Link](http://opensource.com/government/15/8/india-adopts-open-source-policy){:target="_blank" rel="noopener"}


