---
layout: post
title: "New insights found in black hole collisions"
date: 2016-04-19
categories:
author: University Of Cambridge
tags: [Gravitational wave,Black hole,LIGO,Astronomy,Physical sciences,Space science,Science,Astrophysics,Physical phenomena,Physical cosmology,Celestial mechanics,Physics]
---


That energy, rather than going out as visible light, which is easy to see, goes out as gravitational waves, which are much more difficult to detect. Using gravitational waves as an observational tool, you could learn about the characteristics of the black holes that were emitting those waves billions of years ago, information such as their masses and mass ratios, and the way they formed said co-author and PhD student Davide Gerosa, of Cambridge's Department of Applied Mathematics and Theoretical Physics. The equations the researchers solved deal specifically with the spin angular momentum of binary black holes and a phenomenon called precession. Like a spinning top, black hole binaries change their direction of rotation over time, a phenomenon known as precession, said Sperhake. Just as Kepler studied the motion of the earth around the sun and found that orbits can be ellipses, parabola or hyperbolae, the researchers found that black hole binaries can be divided into three distinct phases according to their rotation properties.

<hr>

[Visit Link](http://phys.org/news346664680.html){:target="_blank" rel="noopener"}


