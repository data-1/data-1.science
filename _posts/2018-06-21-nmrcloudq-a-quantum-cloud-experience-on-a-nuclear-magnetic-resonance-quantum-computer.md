---
layout: post
title: "NMRCloudQ: A quantum cloud experience on a nuclear magnetic resonance quantum computer"
date: 2018-06-21
categories:
author: "Science China Press"
tags: [Science,Quantum computing,Cloud,Resonance,American Association for the Advancement of Science,Qubit,Nuclear magnetic resonance,Computer,Quantum,Quantum mechanics,Physics,Theoretical physics,Applied and interdisciplinary physics]
---


Quantum computers are coming and attract attentions from scientists all over the world. At present, most quantum computer prototypes involve less than ten individually controllable qubits, and only exist in laboratories for the sake of either the great costs of devices or professional maintenance requirements. Moreover, scientists believe that quantum computers will never replace our daily, every-minute use of classical computers, but would rather serve as a substantial addition to the classical ones when tackling some particular problems, Due to the above two reasons, cloud-based quantum computing is anticipated to be the most useful and reachable form for public users to experience with the power of quantum. As initial attempts, IBM Q has launched influential cloud services on a superconducting quantum processor in 2016, but no other cloud services have followed up yet in china. Different from the existing cloud services, a joint team led by G. Long at Tsinghua University, B. Zeng at University of Guelph and D. Lu at SUSTech presents a new cloud quantum computing NMRCloudQ which is based on well-established nuclear magnetic resonance.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/scp-naq011918.php){:target="_blank" rel="noopener"}


