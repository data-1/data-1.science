---
layout: post
title: "All missions on board for NASA heliophysics research"
date: 2018-06-10
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Goddard Space Flight Center,THEMIS,Space science,Flight,Nature,Spacecraft,Physical sciences,Bodies of the Solar System,Astronautics,Solar System,Science,Spaceflight,Outer space,Astronomy]
---


To get a global picture, the scientists used data from four individual NASA missions -- the Magnetospheric Multiscale mission, Van Allen Probes mission, Geotail, and the Time History of Events and Macroscale Interactions during Substorms mission -- plus the LANL-GEO spacecraft. The scientists chose an event during a quiet period in the near-Earth space environment, which they assumed would provide a simple case that would be easy to model. However, by combining datasets from spacecraft situated in locations spread around Earth, Turner and his team were able to address big-picture questions about particle movement. Turner and his team are already looking at more events to see if what they've found in the first event is typical of substorms. The four NASA missions used in this study are a part of NASA's Heliophysics fleet of missions under NASA's Solar Terrestrial Probes, Living with a Star and Explorers programs, which aim to understand fundamental plasma physics questions and the dynamic Earth-Sun environment we live in.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/nsfc-amo112717.php){:target="_blank" rel="noopener"}


