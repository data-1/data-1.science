---
layout: post
title: "Preparing to build ESA’s Jupiter mission"
date: 2015-09-03
categories:
author: "$author"   
tags: [Jupiter Icy Moons Explorer,Bodies of the Solar System,Space exploration,Planets,Astronomical objects known since antiquity,Spacecraft,Flight,Planets of the Solar System,Astronomical objects,Astronautics,Spaceflight,Outer space,Space science,Astronomy,Solar System,Planetary science]
---


The agency’s Industrial Policy Committee approved the award of the €350.8 million contract yesterday. Pending the negotiation of contractual details, this should allow work to start by the end of this month. Juice (JUpiter ICy moons Explorer) was selected in May 2012 as the first Large-class mission within ESA’s Cosmic Vision 2015–25 programme. The spacecraft should be launched in 2022 and arrive in the Jovian system in 2030. Gravity assists with Callisto and Ganymede will be used to modify the spacecraft’s trajectory, and two targeted Europa flybys will focus on the composition of non-water-ice material on its frozen surface, and the first subsurface sounding of an icy moon.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Preparing_to_build_ESA_s_Jupiter_mission){:target="_blank" rel="noopener"}


