---
layout: post
title: "Controlling the magnetic properties of individual iron atoms"
date: 2016-01-31
categories:
author: University Of Warsaw
tags: [Quantum dot,Atom,Magnetism,Iron,Cadmium selenide,Materials science,Materials,Physical sciences,Applied and interdisciplinary physics,Physics,Condensed matter physics,Electromagnetism,Physical chemistry,Condensed matter,Chemical product engineering,Quantum mechanics,Chemistry]
---


Although the iron is usually associated with magnetism, it has been known already since the 1960s that the iron atom of 2+ charge state becomes non-magnetic after incorporation into a typical semiconductor. The d-shell electrons of the iron atom have only one lowest-energy configuration, in which the total magnetic moment of the iron vanishes, even upon the application of a small external magnetic field. Using molecular beam epitaxy, Tomasz Smolenski and co-workers fabricated zinc selenide crystals integrated with cadmium selenide nanocrystals of larger lattice constant. Therefore, by means of the photoluminescence studies of a single quantum dot containing an individual iron atom, it was possible to determine both the electronic configuration and the magnetic properties of the iron atom. Furthermore, it was also found that the magnetic moment of this atom can be induced by light.

<hr>

[Visit Link](http://phys.org/news/2016-01-magnetic-properties-individual-iron-atoms.html){:target="_blank" rel="noopener"}


