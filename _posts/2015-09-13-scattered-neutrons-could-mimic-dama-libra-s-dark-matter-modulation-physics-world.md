---
layout: post
title: "Scattered neutrons could mimic DAMA-LIBRA's 'dark matter' modulation – Physics World"
date: 2015-09-13
categories:
author: Tushna Commissariat
tags: [Dark matter,Neutrino,DAMANaI,DAMALIBRA,Muon,Neutron,Matter,Science,Physics,Space science,Nature,Physical sciences,Astronomy,Particle physics,Astrophysics,Physical cosmology]
---


But apart from the CoGENT dark-matter experiment in the US, no other dark-matter searches have seen a similar effect. The most recent cosmic microwave background (CMB) data from the Planck mission reveal a universe that is composed of 26.8% dark matter and 68.3% dark energy, with less than 5% of “normal” visible matter, such as galaxies and gas clouds. “When combined, this means that the neutrons from both of these sources also have a rate that varies annually but peaks somewhere in between the two and can match the DAMA phase that is in late May,” explains Davis. “Indeed, the cross-section – which gives you the interaction rate – for neutron production from neutrinos and muons is particularly high. “However, these are directly down to neutrino scattering, not neutrons from neutrinos.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/zRX43SRZ_Tw/scattered-neutrons-could-mimic-dama-libras-dark-matter-modulation){:target="_blank" rel="noopener"}


