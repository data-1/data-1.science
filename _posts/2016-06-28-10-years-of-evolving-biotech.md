---
layout: post
title: "10 years of evolving biotech"
date: 2016-06-28
categories:
author: "Diana Enriquez"
tags: [DNA,Gene,Genome,Human genome,Genetics,Francis Crick,Technology,Genome editing,DNA sequencing,MicroRNA,Life,Cancer,Base pair,RNA,Species,Biotechnology,Biology,Life sciences]
---


The race to code the entire human genome started in the 1990s. We will have a $1,000 genome within the next five to eight years. And that is what makes microRNAs such a promising biomarker for cancer, because as you know, cancer is a disease of altered gene expression. In 2015, Enriquez came back to TED with a bold talk on the ethical questions we’ll have when we start modifying the next human species. You can take an entire gene out, put one in, or even edit just a single letter within a gene.

<hr>

[Visit Link](http://blog.ted.com/10-years-of-evolving-biotech/){:target="_blank" rel="noopener"}


