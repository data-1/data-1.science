---
layout: post
title: "Driving mosquito evolution to fight malaria"
date: 2017-03-19
categories:
author: "University of California - Berkeley"
tags: [Malaria,Insecticide,DDT,Mosquito net,Insect repellent,Mosquito,Nature,Biology]
---


Because insecticides don't kill 100 percent of mosquitoes, those with some resistance survive and come to dominate the population, rendering the insecticide ineffective. A mediocre repellent will drive mosquitoes to evolve so that the repellent becomes more effective. One advantage of the proposed strategy is that it can be implemented without interrupting current mosquito control programs, such as spraying insecticides around homes and using bed nets. Boots and Lynch plan to team up with scientists in Africa or Asia to test the hypothesis that mosquitoes can actually evolve to be more repelled by mediocre repellents, and determine what levels of effectiveness are necessary for both repellent and insecticide. DDT was used so widely that it is very interesting to consider whether it was always an effective repellent or has become so, and is a nice example of an evolved spatial repellent, Lynch said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/uoc--dme102416.php){:target="_blank" rel="noopener"}


