---
layout: post
title: "Virtual humans work better than current ways to identify post-traumatic stress in soldiers: Computer-generated interviewers combine anonymity with social rapport to help troops open up about mental he"
date: 2017-10-12
categories:
author: "Frontiers"
tags: [Post-traumatic stress disorder,Mental disorder,Health,News aggregator,Mental health,Health sciences,Psychology]
---


A computer-generated 'human' interviewer combines the advantages of anonymity with social connection and rapport, which could help soldiers to reveal more about their mental health symptoms. However, human interviewers can build rapport with interviewees, which isn't possible in an anonymous survey. A computer-generated 'human' interviewer could provide a solution that combines the rapport-building skills of real human interviewers with the feelings of anonymity and safety provided by anonymous surveys. Lucas and her colleagues hypothesized that a virtual interviewer would help soldiers to disclose PTSD symptoms more easily. In this second experiment, soldiers and veterans with milder symptoms of post-traumatic stress disorder opened up and disclosed more symptoms to the virtual interviewer compared to the anonymous PDHA survey.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171012091020.htm){:target="_blank" rel="noopener"}


