---
layout: post
title: "Earth BioGenome project to sequence all life"
date: 2018-06-17
categories:
author: "University of California - Davis"
tags: [Biodiversity,World Economic Forum,Life,Biology,Genome,Species,Branches of science,Natural environment,Nature]
---


In an effort to protect and preserve the Earth's biodiversity and kick-start an inclusive bio-economy, the World Economic Forum today announced a landmark partnership between the Earth BioGenome Project, chaired by Harris Lewin, distinguished professor at the University of California, Davis, and the Earth Bank of Codes to map the DNA of all life on Earth. The Earth Biogenome Project aims to sequence the DNA of all the planet's eukaryotes, some 1.5 million known species including all known plants, animals and single-celled organisms. Of the estimated 15 million eukaryotic species, only 10 percent have been taxonomically classified. Of that percentage, scientists have sequenced the genomes of around 15,000 species, less than 0.1 percent of all life on Earth. The partnership between the Earth BioGenome Project and the Earth Bank of Codes is part of the World Economic Forum's 4IR for the Earth Initiative.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/uoc--ebp012318.php){:target="_blank" rel="noopener"}


