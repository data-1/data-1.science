---
layout: post
title: "ASU, USGS project yields sharpest map of Mars' surface properties"
date: 2015-10-03
categories:
author: Arizona State University
tags: [Mars,Thermal Emission Imaging System,Planetary science,Bodies of the Solar System,Astronomy,Earth sciences,Outer space,Astronomical objects known since antiquity,Terrestrial planets,Space science,Planets of the Solar System]
---


Fine-grain materials, such as dust and sand, show up as dark areas, most notably in the streaky rays made of fine material flung away in the aftermath of the meteorite's impact. We used more than 20,000 THEMIS nighttime temperature images to generate the highest resolution surface property map of Mars ever created, says Fergason, who earned her doctorate at ASU in 2006. Darker means cooler and dustier  The new map uses nighttime temperature images to derive the thermal inertia for areas of Mars, each the size of a football field. As day and night alternate on Mars, loose, fine-grain materials such as sand and dust change temperature quickly and thus have low values of thermal inertia. This map provides data not previously available, and it will enable regional and global studies of surface properties.

<hr>

[Visit Link](http://phys.org/news324661113.html){:target="_blank" rel="noopener"}


