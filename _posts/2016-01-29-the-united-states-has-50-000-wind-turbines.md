---
layout: post
title: "The United States Has 50,000 Wind Turbines"
date: 2016-01-29
categories:
author: Cynthia Shahan, U.S. Department Of Energy, U.S. Energy Information Administration, George Harvey, Tina Casey, Written By
tags: [Wind power,Greenhouse gas emissions,Clean technology,Sustainable energy,Natural environment,Climate change,Physical quantities,Sustainable technologies,Sustainable development,Energy,Nature]
---


The United States has 50,000 spinning wind turbines on its lands. Wind power is a critical solution to CO2 emissions and air and water pollution from power plants, and these 50,000 wind turbines result in steep emissions cuts  Though, Climate Central notes “how uneven the decline in those emissions” was across the US in recent years. The American Wind Energy Association (AWEA) notes that these 50,000 wind turbines produce enough electricity for ~19 million homes, with a total installed power capacity of 70 gigawatts (GW). And the turbines are also beautiful at sunset…  Related Stories:  About Wind Energy/ Why Wind Energy  Wind Energy Facts  What I (Don’t) Hate about Wind Power  Chart by Climate Central; Photo by Chauncey Davis  Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage? Or follow us on Google News Have a tip for CleanTechnica, want to advertise, or want to suggest a guest for our CleanTech Talk podcast?

<hr>

[Visit Link](http://cleantechnica.com/2016/01/07/united-states-50000-wind-turbines/){:target="_blank" rel="noopener"}


