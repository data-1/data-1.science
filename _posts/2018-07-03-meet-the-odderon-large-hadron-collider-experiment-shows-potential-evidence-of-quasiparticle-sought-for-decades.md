---
layout: post
title: "Meet the 'odderon': Large Hadron Collider experiment shows potential evidence of quasiparticle sought for decades"
date: 2018-07-03
categories:
author: "University of Kansas"
tags: [Particle physics,Gluon,Odderon,Standard Model,Proton,Large Hadron Collider,Matter,Science,Physics,Physical sciences,Nature,Quantum mechanics,Quantum field theory,Theoretical physics,Nuclear physics,Subatomic particles]
---


Now, a team of high-energy experimental particle physicists, including several from the University of Kansas, has uncovered possible evidence of a subatomic quasiparticle dubbed an odderon that -- until now -- had only been theorized to exist. In all previous experiments, scientists detected collisions involving only even numbers of gluons exchanged between different protons. The protons interact like two big semi-trucks that are transporting cars, the kind you see on the highway, said Timothy Raben, a particle theorist at KU who has worked on the odderon. It's a kind of discovery that we might have seen for the first time, this odd exchange of the number of gluons. The rho parameter is essentially measuring the ratio of one signature to another of this high energy growth.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/uok-mt020118.php){:target="_blank" rel="noopener"}


