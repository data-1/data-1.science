---
layout: post
title: "3D ocean map tracks ecosystems in unprecedented detail"
date: 2018-08-15
categories:
author: "Witze, Alexandra Witze, You Can Also Search For This Author In"
tags: []
---


A new 3D map sorts global water masses — from deep, frigid circumpolar waters to the oxygen-starved Black Sea — into 37 categories. The researchers, including some at Esri, combined information on geology and vegetation to generate nearly 4,000 ‘ecological land units’. Next, the team moved their focus from land to the oceans. “It’s like total world domination in ecosystem mapping,” says Sayre, who heads the EMU project with Wright. For now, they are hoping to expand on the land and marine units by creating new categories for coastal and freshwater ecosystems.

<hr>

[Visit Link](http://www.nature.com/news/3d-ocean-map-tracks-ecosystems-in-unprecedented-detail-1.21240){:target="_blank" rel="noopener"}


