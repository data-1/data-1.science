---
layout: post
title: "New tools advance bio-logic: Researchers build more sophisticated synthetic gene circuits"
date: 2015-10-22
categories:
author: Mike Williams, Rice University
tags: [Synthetic biology,Genetics,Transcription factor,Gene,Cell (biology),Biology,Molecular biology,Biochemistry,Life sciences,Biotechnology,Technology]
---


Rice's David Shis (left) and Matthew Bennett are advancing synthetic biology with their work to develop more complicated, and capable, genetic circuits that mimic computer circuits to perform tasks in cells. Logic gates designed by Bennett's team and others react in a programmed way when they sense chemicals in their immediate environment. If certain combinations of chemicals are present in the environment, the gate will turn on a gene that may either repress or promote the expression of a protein. A lot of work in synthetic biology has gone into programming cells to make decisions better and more efficiently, Bennett said. We've been able to eliminate the need for that by programming transcription factors—which are specific proteins that turn genes on and off—to respond to their environment directly and activate a specific gene in a very modular way.

<hr>

[Visit Link](http://phys.org/news326355556.html){:target="_blank" rel="noopener"}


