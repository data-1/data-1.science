---
layout: post
title: "Quantum chemistry solves mystery why there are these 20 amino acids in the genetic code"
date: 2018-07-03
categories:
author: "Johannes Gutenberg Universitaet Mainz"
tags: [Chemistry,Protein,Amino acid,Genetic code,Life,Biochemistry,Acid,Genetics,DNA,Nature,Biotechnology,Physical sciences,Biology,Molecular biology,Life sciences]
---


All life on Earth is based on 20 amino acids, which are governed by the DNA to form proteins. The resultant grid of codons is what is known as the genetic code. In a new approach, the researchers compared the quantum chemistry of all amino acids used by life on Earth with the quantum chemistry of amino acids from space, brought in on meteorites, as well as with that of modern reference biomolecules. They found that the newer amino acids had become systematically softer, i.e., more readily reactive or prone to undergo chemical changes. This oxygen promoted the formation of toxic free radicals, which exposes modern organisms and cells to massive oxidative stress.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/jgum-qcs020118.php){:target="_blank" rel="noopener"}


