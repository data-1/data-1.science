---
layout: post
title: "Characterizing the forces that hold everything together"
date: 2015-10-02
categories:
author: University of Massachusetts Amherst 
tags: [Van der Waals force,Molecule,Chemistry,Physical sciences,Applied and interdisciplinary physics,Chemical product engineering,Materials science,Materials,Physical chemistry,Technology,Nanotechnology]
---


This is because the physics of interactions at these scales is difficult, say physicists at the University of Massachusetts Amherst, who with colleagues elsewhere this week unveil a project known as Gecko Hamaker, a new computational and modeling software tool plus an open science database to aid those who design nano-scale materials. The Gecko Hamaker project makes available to its online users a large variety of calculations for nanometer-level interactions that help to predict molecular organization and evaluate whether new combinations of materials will actually stick together and work. This new database and our calculations are going to be important to many different kinds of scientists interested in colloids, biomolecular engineering, those assembling molecular aggregates and working with virus-like nanoparticles, and to people working with membrane stability and stacking. Podgornik explains, Our work is fundamentally different from other approaches, as we don't talk only about forces but also about torques. Van der Waals interactions are known in simple cases, but we've taken on the most difficult ones.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/uoma-ctf092215.php){:target="_blank" rel="noopener"}


