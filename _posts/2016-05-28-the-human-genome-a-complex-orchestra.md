---
layout: post
title: "The human genome: A complex orchestra"
date: 2016-05-28
categories:
author: "University of Geneva"
tags: [Genetics,Genome,Gene,Human genome,DNA,Chromatin,Gene expression,Cis-regulatory element,Cell (biology),Genetic variation,Life sciences,Molecular genetics,Branches of genetics,Biochemistry,Biology,Molecular biology,Biotechnology]
---


Chromatin, a complex of protein and DNA, packages the genome in a cell. The scientists' study in Cell reports how genetic variation affected three molecular layers in immune cell lines that were derived from 47 individuals whose genomes had been fully sequenced: transcription factor-DNA interactions, chromatin states, and gene expression levels. We observed that genetic variation at a single genomic position impacted multiple, separated gene regulatory elements at the same time. This extensive coordination was quite surprising, much like a music conductor (i.e. genetic variant) directing all the performers (i.e. transcription factors, chromatin modifications) of a musical ensemble to change the volume (i.e. gene expression) of the music, explains Professor Bart Deplancke from EPFL. Contrary to the traditional model, which holds that gene regulatory elements impact gene expression in a quasi-independent fashion, researchers identified a much more harmonized and synergistic behavior: far from being linear, gene regulatory elements are actually coordinated in their actions.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150820123641.htm){:target="_blank" rel="noopener"}


