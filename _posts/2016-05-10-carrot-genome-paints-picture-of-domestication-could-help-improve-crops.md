---
layout: post
title: "Carrot genome paints picture of domestication, could help improve crops"
date: 2016-05-10
categories:
author: University of Wisconsin-Madison
tags: [Carrot,Plant,Carotenoid,Gene,Genetics,Domestication,Plant breeding,Vitamin A,Biology]
---


Carrots are the richest crop source of vitamin A in the American diet. The accumulation of orange pigments is an accumulation that normally wouldn't happen, says Simon, one of just a few carrot researchers around the world, along with another UW-Madison scientist, Irwin Goldman, who was not part of this study. Now, we know what the genes are and what they do. Additionally, the study confirmed a gene called Y is responsible for the difference between white carrots and yellow or orange ones, and that a variation of it leads to the accumulation of carotenoids. It's a repurposing of genes plants usually use when growing in light, says Simon.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/uow-cgp050916.php){:target="_blank" rel="noopener"}


