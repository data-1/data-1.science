---
layout: post
title: "Ocean energy: EU leads in technology development and deployment"
date: 2015-05-06
categories:
author: European Commission Joint Research Centre
tags: [Marine energy,Energy development,Wave power,Privacy,Energy,Nature,Technology,Economy]
---


Among the various types of ocean energy technologies, tidal and wave power are those poised to provide the most significant contribution to the European energy system Credit: © mimadeo, Fotolia.com  New technologies in the last decade have shown slow but steady progress of ocean and sea energy power: about 30 tidal and 45 wave energy companies are currently at an advanced stage of technological development worldwide, many of them nearing pre-commercial array demonstration and others deploying full-scale prototypes in real-sea environment, according to a new JRC ocean energy status report. Ocean energy represents one of the few untapped renewable energy sources and its development is attracting the interests of policymakers, utilities and technology developers. The JRC report analyses the sector, assessing the status of ocean energy technologies, ongoing developments, related policies and markets. Background  To support the 'blue energy' sector move towards full industrialisation, in 2014 the European Commission proposed a Blue Energy Action for the creation of an Ocean Energy Forum - bringing together stakeholders to build capacity and foster cooperation. In a later phase (2017-2020), a European Industrial Initiative could be developed, based on the outcomes of the Ocean Energy Forum.

<hr>

[Visit Link](http://phys.org/news350048537.html){:target="_blank" rel="noopener"}


