---
layout: post
title: "In CRISPR advance, scientists successfully edit human T cells: Research has implications for autoimmune diseases, AIDS, cancer"
date: 2016-05-16
categories:
author: University of California - San Francisco
tags: [CRISPR gene editing,Cas9,Jennifer Doudna,Genome editing,Biotechnology,Biology,Life sciences,Genetics,Biochemistry,Molecular genetics,Medical specialties,Molecular biology,Clinical medicine,Branches of genetics,Medicine,Health sciences]
---


But in practice, editing T cell genomes with CRISPR/Cas9 has proved surprisingly difficult, said Alexander Marson, PhD, a UCSF Sandler Fellow, and senior and co-corresponding author of the new study. Genome editing in human T cells has been a notable challenge for the field, Marson said. advertisement  Cas9, an enzyme in the CRISPR system that makes cuts in DNA and allows new genetic sequences to be inserted, has generally been introduced into cells using viruses or circular bits of DNA called plasmids. Until recently, however, editing human T cells with CRISPR/Cas9 has been inefficient, with only a relatively small percentage of cells being successfully modified. We tried for a long time to introduce Cas9 with plasmids or lentiviruses, and then to express separately the single-guide RNA in the cell, Schumann said.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150727153727.htm){:target="_blank" rel="noopener"}


