---
layout: post
title: "Bananas: Facts, benefits and nutrition"
date: 2017-10-26
categories:
author: Jessie Szalay, Jeanna Bryner, Daisy Dobrijevic
tags: [Banana,Dietary fiber,Nutrient,Tryptophan,Berry,Health,Vitamin,Potassium,Vitamin B6,Weight loss,Fruit,Nutrition]
---


Can you eat too many bananas? If you eat dozens of bananas every day, there may be a risk of consuming excessively high amounts of vitamins and minerals. It contains high amounts of vitamin B6 and B12, as well as magnesium and potassium. Overall, the authors said that green bananas and their products are a good source of fibers, vitamins C and B6, as well as several minerals, including potassium, phosphorus, magnesium and zinc. According to Guinness World Records, (opens in new tab) the fastest time to eat a banana (no hands) is 20.33 seconds.

<hr>

[Visit Link](https://www.livescience.com/45005-banana-nutrition-facts.html){:target="_blank" rel="noopener"}


