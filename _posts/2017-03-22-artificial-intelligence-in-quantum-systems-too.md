---
layout: post
title: "Artificial intelligence in quantum systems, too"
date: 2017-03-22
categories:
author: "University of the Basque Country"
tags: [Quantum mechanics,Quantum information,Natural selection,Simulation,System,Quantum simulator,Memory,Research,Algorithm,Quantum superposition,Applied mathematics,Branches of science,Cognitive science,Science,Technology,Computer science,Systems science,Cybernetics,Computing,Theoretical computer science]
---


Unai Alvarez-Rodriguez is a researcher in the 'Quantum Technologies for Information Science (QUTIS)' research group, attached to the UPV/EHU's Department of Physical Chemistry, and an expert in quantum information and technologies. Explained briefly, quantum information uses quantum physics to encode computational tasks. We developed this final mechanism so that the individuals would have a finite lifetime, specified the researcher. On the other hand we managed to encode a function in a quantum system but not to write it directly; the system did it autonomously, we could say that it learnt by means of the mechanism we designed so that it would happen. From computational models to the real world  All these methods and protocols developed in his research have provided the means to resolve all kinds of systems.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/uotb-aii030717.php){:target="_blank" rel="noopener"}


