---
layout: post
title: "Scientists identify a possible therapeutic target for regulating body weight"
date: 2017-10-05
categories:
author: "Federation of American Societies for Experimental Biology"
tags: [Obesity,Insulin,Leptin,Causes of death,Clinical medicine,Health,Health sciences]
---


New research in The FASEB Journal suggests that over-expression of GTRAP3-18 gene could be related to obesity in humans  A new study published online in The FASEB Journal reveals a novel gene involved in maintaining body weight. Specifically, the study suggests that GTRAP3-18 interacts with pro-opiomelanocortin (POMC) in the hypothalamus to regulate food intake and blood glucose levels. Inhibiting the interaction between GTRAP3-18 and POMC might be a strategy for treating leptin/insulin resistance in patients with obesity and/or type 2 diabetes. Eating too much or too little could actually be a genetic problem, rather than an insulin issue, said Toshio Nakaki, M.D., Ph.D., a researcher in the Department of Pharmacology, Teikyo University School of Medicine, in Tokyo, Japan. The FASEB Journal is published by the Federation of the American Societies for Experimental Biology (FASEB).

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/foas-sia100417.php){:target="_blank" rel="noopener"}


