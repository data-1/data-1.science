---
layout: post
title: "The Real Sun Shower: How Rain and Waterfalls Form and Pour on our Nearest Star (VIDEO)"
date: 2014-06-25
categories:
author: Science World Report
tags: [Sun,Stellar corona,Rain,Weather,Phases of matter,Astronomy,Nature,Astronomical objects,Solar System,Outer space,Bodies of the Solar System,Applied and interdisciplinary physics,Sky,Physical sciences,Planetary science,Physical phenomena,Astronomical objects known since antiquity,Stellar astronomy,Space science,Planets]
---


Yet this rain isn't made of water. Now, scientists have taken a closer look at this bad weather, revealing a bit more about the processes that shape our nearest star. The plasma rain that falls on the sun comes from the outer solar atmosphere, called the corona, and falls to the sun's surface. In addition, the material that makes up the hot rain clouds reaches the corona through a rapid evaporation process, like water evaporates on Earth. Want to see solar rain?

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15648/20140625/real-sun-shower-rain-waterfalls-form-pour-nearest-star-video.htm){:target="_blank" rel="noopener"}


