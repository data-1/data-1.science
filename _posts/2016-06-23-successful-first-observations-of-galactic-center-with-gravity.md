---
layout: post
title: "Successful first observations of galactic center with GRAVITY"
date: 2016-06-23
categories:
author: "ESO"
tags: [European Southern Observatory,Very Large Telescope,Black hole,Star,Orbit,Gravity,S2 (star),Astronomy,Science,Astronomical objects,Celestial mechanics,Outer space,Physics,Physical sciences,Space science]
---


The GRAVITY instrument is now operating with the four 8.2-metre Unit Telescopes of ESO's Very Large Telescope (VLT - http://www.eso.org/public/teles-instr/paranal/) , and even from early test results it is already clear that it will soon be producing world-class science. Although the position and mass of the black hole have been known since 2002, by making precision measurements of the motions of stars orbiting it, GRAVITY will allow astronomers to probe the gravitational field around the black hole in unprecedented detail, providing a unique test of Einstein's general theory of relativity. The GRAVITY team [2] has used the instrument to observe a star known as S2 as it orbits the black hole at the centre of our galaxy with a period of only 16 years. [3] The team will, for the first time, be able to measure two relativistic effects for a star orbiting a massive black hole -- the gravitational redshift and the precession of the pericentre. At Paranal, ESO operates the Very Large Telescope, the world's most advanced visible-light astronomical observatory and two survey telescopes.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/e-sfo062216.php){:target="_blank" rel="noopener"}


