---
layout: post
title: "How our sense of smell evolved, including in early humans"
date: 2016-05-10
categories:
author: Manchester University
tags: [Sense of smell,Odor,Sense,Androstenone,Neuroscience,Biology]
---


Most receptors can detect more than one smell, but one, called OR7D4, enables us to detect a very specific smell called androstenone, which is produced by pigs and is found in boar meat. They found that different populations tend to have different gene sequences and therefore differ in their ability to smell this compound. This shows that when humans first evolved in Africa, they would have been able to detect this odour. One possible explanation of this selection is that the inability to smell androstenone was involved in the domestication of pigs by our ancestors -- andostroneone makes pork from uncastrated boars taste unpleasant to people who can smell it. The group found that Neanderthal OR7D4 DNA was like our own -- they would have been able to smell androstenone.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150702112110.htm){:target="_blank" rel="noopener"}


