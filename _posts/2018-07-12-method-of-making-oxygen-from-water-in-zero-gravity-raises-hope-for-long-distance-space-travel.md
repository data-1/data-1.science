---
layout: post
title: "Method of making oxygen from water in zero gravity raises hope for long-distance space travel"
date: 2018-07-12
categories:
author: "Charles W. Dunnill, The Conversation"
tags: [Water,Hydrogen,Interplanetary spaceflight,Sun,Electron,Oxygen,Proton,Atmosphere of Earth,Nature,Chemistry,Physical sciences]
---


Once in space, special technology could split the water into hydrogen and oxygen which in turn could be used to sustain life or to power electronics via fuel cells. Meanwhile, the hole can absorb electrons from water to form protons and oxygen. The process using photo catalysts is the best option for space travel as the equipment weighs much less than the one needed for electrolysis. However, as water is split to create gas, bubbles form. On Earth, gravity makes the bubbles automatically float to the surface (the water near the surface is denser than the bubbles, which makes them buyonant) – freeing the space on the catalyst for the next bubble to be produced.

<hr>

[Visit Link](https://phys.org/news/2018-07-method-oxygen-gravity-long-distance-space.html){:target="_blank" rel="noopener"}


