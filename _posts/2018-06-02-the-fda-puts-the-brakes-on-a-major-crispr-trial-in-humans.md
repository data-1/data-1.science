---
layout: post
title: "The FDA Puts the Brakes on A Major CRISPR Trial in Humans"
date: 2018-06-02
categories:
author: ""
tags: [Fetal hemoglobin,CRISPR gene editing,Health care,Medical specialties,Medicine,Health sciences,Health,Diseases and disorders,Life sciences,Causes of death,Medical treatments,Clinical medicine]
---


According to a CRISPR Therapeutics press release, the FDA has certain questions it wants resolved before it gives the go-ahead to the human CRISPR study. Finally, they reintroduce the cells to the patient's body where they generate new red blood cells with higher levels of HbF. But until they get the go-ahead to move forward with their human CRISPR study, we can't know for sure whether CTX001 will work as expected. This trial, should it move forward, won't be the first time CRISPR has been tested in humans — that first try was in China in 2016. Hopefully, CRISPR Therapeutics and Vertex are able to answer the FDA's questions in a way that promotes confidence in the treatment.

<hr>

[Visit Link](https://futurism.com/human-crispr-trial-fda-stops/){:target="_blank" rel="noopener"}


