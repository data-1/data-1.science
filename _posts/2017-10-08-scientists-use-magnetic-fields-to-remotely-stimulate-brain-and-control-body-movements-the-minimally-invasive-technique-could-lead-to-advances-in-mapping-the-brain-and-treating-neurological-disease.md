---
layout: post
title: "Scientists use magnetic fields to remotely stimulate brain -- and control body movements: The minimally invasive technique could lead to advances in mapping the brain and treating neurological disease"
date: 2017-10-08
categories:
author: "University at Buffalo"
tags: [Brain,Optogenetics,Neuron,Nanoparticle,Neurology,Neuroscience,Medicine,Clinical medicine,Medical specialties,Health sciences]
---


There is a lot of work being done now to map the neuronal circuits that control behavior and emotions, says lead researcher Arnd Pralle, PhD, a professor of physics in the University at Buffalo College of Arts and Sciences. advertisement  Magneto-thermal stimulation involves using magnetic nanoparticles to stimulate neurons outfitted with temperature-sensitive ion channels. The brain cells fire when the nanoparticles are heated by an external magnetic field, causing the channels to open. advertisement  How magneto-thermal stimulation works  Magneto-thermal stimulation enables researchers to use heated, magnetic nanoparticles to activate individual neurons inside the brain. An advance over other methods, like optogenetics  Pralle has been working to advance magneto-thermal stimulation for about a decade.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/08/170816134658.htm){:target="_blank" rel="noopener"}


