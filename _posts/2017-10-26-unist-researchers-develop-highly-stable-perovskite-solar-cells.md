---
layout: post
title: "UNIST researchers develop highly stable perovskite solar cells"
date: 2017-10-26
categories:
author: Ulsan National Institute of Science and Technology(UNIST)
tags: [Perovskite solar cell,Fluorine,Solar cell,Polytetrafluoroethylene,Technology,Applied and interdisciplinary physics,Nanotechnology,Condensed matter physics,Physical chemistry,Nature,Chemical substances,Goods (economics),Physical sciences,Manufacturing,Materials science,Chemistry,Materials,Artificial materials]
---


This study has been jointly led by Professor Jin Young Kim in the School of Energy and Chemical Engineering at UNIST in collaboration with Dong Suk Kim of Korea Institute of Energy Research (KIER). Perovskite solar cells (PSCs) have attracted more attention in the past few years, as the next-generation solar cells with the potential to surpass silicon cells' efficiency. To solve these issues and make progress toward the commercialization of PSCs, Professor Kim and his team introduced a highly stable p-i-n structure for PSCs using fluorine functionalized EFGnPs to fully cover the perovskite active layer and protect against the ingress of water for high-stability PSCs. This study overcame weakness of perovskite solar cells that have high efficiencies but low stability, says Professor Jin Young Kim. Journal Reference  Gi-Hwan Kim, et al., Fluorine Functionalized Graphene Nano Platelets for Highly Stable Inverted Perovskite Solar Cells, Nano Letters, (2017).

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/unio-urd102517.php){:target="_blank" rel="noopener"}


