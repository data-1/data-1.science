---
layout: post
title: "Scientist underlines threat of inevitable 'solar super-storms'"
date: 2015-09-04
categories:
author: IOP Publishing 
tags: [Coronal mass ejection,Solar flare,Space science,Astronomy,Nature,Solar System,Outer space,Science]
---


In this month's issue of Physics World, Ashley Dale from the University of Bristol warns of the catastrophic and long-lasting impacts of solar super-storms and the dangers we face if the threat continues to go unnoticed. Dale, who was a member of an international task force – dubbed SolarMAX – set up to identify the risks of a solar storm and how its impact could be minimized, explains how it is only a matter of time before an exceptionally violent solar storm is propelled towards Earth. CMEs are often preceded by a solar flare – a massive release of energy from the Sun in the form of gamma rays, X-rays, protons and electrons. Physics World is the international monthly magazine published by the Institute of Physics. Our campaign, Opportunity Physics, offers you the chance to support the work that we do.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/iop-sut073014.php){:target="_blank" rel="noopener"}


