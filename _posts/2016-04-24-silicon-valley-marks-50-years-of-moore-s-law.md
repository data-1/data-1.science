---
layout: post
title: "Silicon Valley marks 50 years of Moore's Law"
date: 2016-04-24
categories:
author: Pete Carey, San Jose Mercury News
tags: [Moores law,Integrated circuit,Microprocessor,Intel,Computer science,Manufactured goods,Electronic engineering,Information and communications technology,Electrical engineering,Information Age,Computers,Electronics,Electronics industry,Computer engineering,Computing,Technology]
---


In 1975, based on industry developments, he updated the prediction to doubling every two years. As you drive around Silicon Valley, 99 percent of the companies you see wouldn't be here without cheap computer processors due to Moore's Law. What's starting to happen is people are looking to other innovations on silicon to give them performance as a way to extend Moore's Law, said Spike Narayan, director of science and technology at IBM's Almaden Research Center. Bohr is spending his time on the generation after that, in which transistors will shrink to 7 nanometers. Hu says what's likely is that at some point the doubling every two years will slow to every four or five years.

<hr>

[Visit Link](http://phys.org/news349088149.html){:target="_blank" rel="noopener"}


