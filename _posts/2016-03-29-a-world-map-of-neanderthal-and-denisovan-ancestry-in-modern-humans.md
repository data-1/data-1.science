---
layout: post
title: "A world map of Neanderthal and Denisovan ancestry in modern humans"
date: 2016-03-29
categories:
author: Cell Press
tags: [Interbreeding between archaic and modern humans,Denisovan,Genetics,Human populations,Biology,Human evolution,Biological anthropology]
---


This map shows the proportion of the genome inferred to be Denisovan in ancestry in diverse non-Africans. Credit: Sankararaman et al./Current Biology 2016  Most non-Africans possess at least a little bit Neanderthal DNA. There are certain classes of genes that modern humans inherited from the archaic humans with whom they interbred, which may have helped the modern humans to adapt to the new environments in which they arrived, says senior author David Reich, a geneticist at Harvard Medical School and the Broad Institute. The researchers collected their data by comparing known Neanderthal and Denisovan gene sequences across more than 250 genomes from 120 non-African populations publically available through the Simons Genome Diversity Project (there is little evidence for Neanderthal and Denisovan ancestry in Africans). The interactions between modern humans and archaic humans are complex and perhaps involved multiple events, Reich says.

<hr>

[Visit Link](http://phys.org/news/2016-03-world-neanderthal-denisovan-ancestry-modern.html){:target="_blank" rel="noopener"}


