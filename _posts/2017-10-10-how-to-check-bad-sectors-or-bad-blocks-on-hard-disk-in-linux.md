---
layout: post
title: "How to Check Bad Sectors or Bad Blocks on Hard Disk in Linux"
date: 2017-10-10
categories:
author: "Aaron Kili"
tags: [SMART,Device file,Hard disk drive,Ext3,Flash memory,Badblocks,File system,Disk storage,Disk partitioning,Computer architecture,Storage software,Technology,Computing,Data,Computer hardware,Information technology management,Utility software,Computer data,Computer science,Software,Computer data storage,Computers,Computer engineering,System software,Operating system technology,Data management]
---


That said, below are the methods:  Check Bad Sectors in Linux Disks Using badblocks Tool  A badblocks program enables users to scan a device for bad sectors or blocks. Firstly, use the fdisk command with superuser privileges to display information about all your disk drives or flash memory plus their partitions:  $ sudo fdisk -l  Then scan your Linux disk drive to check for bad sectors/blocks by typing:  $ sudo badblocks -v /dev/sda10 > badsectors.txt  In the command above, badblocks is scanning device /dev/sda10 (remember to specify your actual device) with the -v enabling it to display details of the operation. You will need to employ e2fsck (for ext2/ext3/ext4 file systems) or fsck command with the badsectors.txt file and the device file as in the command below. ------------ Specifically for ext2/ext3/ext4 file-systems ------------ $ sudo e2fsck -l badsectors.txt /dev/sda10 OR ------------ For other file-systems ------------ $ sudo fsck -l badsectors.txt /dev/sda10  Scan Bad Sectors on Linux Disk Using Smartmontools  This method is more reliable and efficient for modern disks (ATA/SATA and SCSI/SAS hard drives and solid-state drives) which ship in with a S.M.A.R.T (Self-Monitoring, Analysis and Reporting Technology) system that helps detect, report and possibly log their health status, so that you can figure out any impending hardware failures. $ sudo smartctl -H /dev/sda10  The result above indicates that your hard disk is healthy, and may not experience hardware failures any soon.

<hr>

[Visit Link](http://www.tecmint.com/check-linux-hard-disk-bad-sectors-bad-blocks/){:target="_blank" rel="noopener"}


