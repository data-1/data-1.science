---
layout: post
title: "Qubits team up to detect errors – Physics World"
date: 2014-06-22
categories:
author: ""         
tags: [Qubit,Quantum computing,Quantum entanglement,Physical and logical qubits,Quantum information science,Applied and interdisciplinary physics,Branches of science,Computer science,Scientific theories,Quantum mechanics,Physics,Theoretical physics,Theoretical computer science,Science,Applied mathematics]
---


Therefore, to preserve quantum states long enough to perform useful algorithms, scientists need to join physical qubits into information-processing units called “logical qubits” that can detect and correct errors in their individual components. Because of how the qubits were entangled, the researchers could tell which qubit had an error and what kind of error it was by measuring light fluorescing from the trapped ions. To scale up towards a working quantum computer, scientists will also need to develop methods of trapping ions not just in lines but in 2D grids. Blatt’s team also performed computational operations on its logical qubit by using lasers to change the states of individual qubits in a controlled way. By adding additional qubits to their scheme, the scientists think they can perform all of the operations needed for quantum algorithms.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/_Jkj4un0lOg/qubits-team-up-to-detect-errors){:target="_blank" rel="noopener"}


