---
layout: post
title: "First science with ALMA's highest-frequency capabilities"
date: 2018-08-18
categories:
author: "National Radio Astronomy Observatory"
tags: [Atacama Large Millimeter Array,Electromagnetic spectrum,Astronomy,Observatory,Spectral line,Star formation,Telescope,Associated Universities Inc,National Radio Astronomy Observatory,Star,Nature,Science,Space science,Physical sciences]
---


Recently, scientists pushed ALMA to its limits, harnessing the array's highest-frequency (shortest wavelength) capabilities, which peer into a part of the electromagnetic spectrum that straddles the line between infrared light and radio waves. The lower black portion shows the lines detected by the European Space Agency's Herschel Space Observatory. Jets of Steam from Protostar  One of ALMA's first Band 10 results was also one of the most challenging, the direct observation of jets of water vapor streaming away from one of the massive protostars in the region. A portion of this material, however, is propelled away from the growing protostar as a pair of jets, which carry away gas and molecules, including water. We detected a wealth of complex organic molecules surrounding this massive star-forming region, said McGuire.

<hr>

[Visit Link](https://phys.org/news/2018-08-science-alma-highest-frequency-capabilities.html){:target="_blank" rel="noopener"}


