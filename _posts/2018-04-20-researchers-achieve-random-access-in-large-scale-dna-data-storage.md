---
layout: post
title: "Researchers achieve random access in large-scale DNA data storage"
date: 2018-04-20
categories:
author: "University Of Washington"
tags: [DNA digital data storage,DNA sequencing,Primer (molecular biology),DNA,Polymerase chain reaction,Computer data storage,Life sciences,Computing,Genetics,Biochemistry,Molecular biology,Biology,Biotechnology,Technology]
---


In a paper published in Nature Biotechnology, the members of the Molecular Information Systems Laboratory (MISL) describe the science behind their world record-setting achievement of 200 megabytes stored in synthetic DNA. While this is not the first time researchers have achieved random access in DNA, the UW and Microsoft team have produced the first demonstration of random access at such a large scale. They then used these primers later to select the desired strands through random access, and used a new algorithm designed to more efficiently decode and restore the data to its original, digital state. Explore further Researchers break record for DNA data storage  More information: Lee Organick et al. Random access in large-scale DNA data storage, Nature Biotechnology (2018). Lee Organick et al. Random access in large-scale DNA data storage,(2018).

<hr>

[Visit Link](https://phys.org/news/2018-02-random-access-large-scale-dna-storage.html){:target="_blank" rel="noopener"}


