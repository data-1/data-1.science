---
layout: post
title: "'New physics' charmingly escapes us"
date: 2018-08-03
categories:
author: "The Henryk Niewodniczanski Institute of Nuclear Physics Polish Academy of Sciences"
tags: [LHCb experiment,Lambda baryon,Elementary particle,Baryon,Quark,Hadron,Standard Model,Quantum chromodynamics,Science,Physical sciences,Quantum field theory,Subatomic particles,Nuclear physics,Physics,Particle physics]
---


Prof. Witek led a five-member group of physicists from Cracow searching for nonresonant decays of charmed baryon Lambda c in data collected in 2011 and 2012 by the international LHCb experiment at the Large Hadron Collider in Geneva. Why was the attention of the researchers drawn this time to Lambda c baryons, i.e. particles made of down (d), up (u) and charm (c) quarks? The Standard Model predicts that nonresonant decays of Lambda c baryons into three particles: a proton and two muons, should occur more or less once in hundreds of billions of decays. During the analyses, the Cracow-based researchers also observed resonant decays, in which the Lambda c baryon decayed into a proton and omega meson. The Henryk Niewodniczanski Institute of Nuclear Physics (IFJ PAN) is currently the largest research institute of the Polish Academy of Sciences.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/thni-pc080218.php){:target="_blank" rel="noopener"}


