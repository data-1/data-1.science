---
layout: post
title: "How a single particle simultaneously modifies the physical reality of two distant others: a quantum nonlocality and weak value study"
date: 2017-09-16
categories:
author: "De Lima Bernardo, Departamento De Física, Universidade Federal De Campina Grande, Caixa Postal, Campina Grande, Canabarro, Grupo De Física Da Matéria Condensada, Núcleo De Ciências Exatas - Ncex, Campus Arapiraca, Universidade Federal De Alagoas"
tags: []
---


After the calculations, it can be found that the normalized resultant state of the system is  with p(2) = 〈ψ|Π 2 |ψ〉 = 1/6 as the probability of detecting photon 2 at D 2 (See the note in ref. 1; and another observer, “Bob”, who is in charge of analyzing photon 3 in the apparatus MZ 3 (composed by the devices BS 3 , M 2 and BS 5 ), localized at the outputs of it to measure the probabilities to obtain photon 3 in the paths A 3 and B 3 . Nevertheless, since we are interested in the cases in which Alice does measure photon 1 at one of the two outputs of MZ 1 , the state of photon 1 after M 1 and BS 4 is given by the normalized state  By a similar analysis, if we are interested in the cases in which Bob necessarily measures photon 3 at one of the output ports of MZ 3 , the state of photon 3 after M 2 and BS 5 is found to be  In this case, the state of photons 1 and 3 after M 1 , M 2 , BS 4 and BS 5 is simply  As a consequence, Alice must expect that the probabilities to obtain photon 1 in the states 1 and 1 , given that photon 3 remains in MZ 3 , are respectively  and  In a similar fashion, Bob must expect that the probabilities to obtain photon 3 in the states 3 and 3 , given that photon 1 remains in MZ 1 , are  and  respectively. Weak traces of the photons when the state is postselected  Let us investigate now the entanglement mediation protocol by using weak measurements when the singlet state of photons 1 and 3 is obtained after detecting photon 2 at D 2 . Weak traces of the photons when the state is postselected  Now we turn to the analysis of the weak traces left by photons 1, 2 and 3 when the state of photons 1 and 3 is postselected in the entanglement mediation protocol.

<hr>

[Visit Link](http://www.nature.com/srep/2017/170103/srep39767/full/srep39767.html?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


