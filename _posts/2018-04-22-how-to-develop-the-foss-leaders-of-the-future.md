---
layout: post
title: "How to develop the FOSS leaders of the future"
date: 2018-04-22
categories:
author: "VM (Vicky) Brasseur
(Alumni)"
tags: [Mentorship,Free and open-source software]
---


Once you've identified the roles and determined which ones are critical to your project, the next step is to list all of the duties and responsibilities for each of those critical roles. List the duties and responsibilities you think each role has, then ask the person who performs that role to list the duties the role actually has. Setting term limits also helps those who are currently performing the role. You'll learn how the role is done, and you can document it to help with the succession planning process. This history is very important, especially for new contributors or people stepping into critical roles.

<hr>

[Visit Link](https://opensource.com/article/18/4/succession-planning-how-develop-foss-leaders-future){:target="_blank" rel="noopener"}


