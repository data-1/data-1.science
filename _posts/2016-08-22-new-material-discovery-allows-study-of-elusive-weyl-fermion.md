---
layout: post
title: "New material discovery allows study of elusive Weyl fermion"
date: 2016-08-22
categories:
author: "DOE/Ames Laboratory"
tags: [Weyl semimetal,Particle physics,Physics,Condensed matter,Theoretical physics,Quantum mechanics,Physical sciences,Applied and interdisciplinary physics,Science,Nature,Condensed matter physics,Scientific theories]
---


Researchers at the U.S. Department of Energy's Ames Laboratory have discovered a new type of Weyl semimetal, a material that opens the way for further study of Weyl fermions, a type of massless elementary particle hypothesized by high-energy particle theory and potentially useful for creating high-speed electronic circuits and quantum computers. From my perspective as solid state physicist it is absolutely extraordinary to observe two bands touching each other at certain points and being connected by Fermi arcs - objects that are prohibited to exist in ordinary materials. The research is further discussed in a paper, Spectroscopic evidence for a type II Weyl semimetallic state in MoTe2; authored by Lunan Huang, Timothy M. McCormick, Masayuki Ochi, Zhiying Zhao, Michi-To Suzuki, Ryotaro Arita, Yun Wu, Daixiang Mou, Huibo Cao, Jiaqiang Yan, Nandini Trivedi and Adam Kaminski; and published in Nature Materials. ###  Ames Laboratory is a U.S. Department of Energy Office of Science national laboratory operated by Iowa State University. We use our expertise, unique capabilities and interdisciplinary collaborations to solve global problems.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/dl-nmd081516.php){:target="_blank" rel="noopener"}


