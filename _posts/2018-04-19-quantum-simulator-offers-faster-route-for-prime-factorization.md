---
layout: post
title: "Quantum simulator offers faster route for prime factorization"
date: 2018-04-19
categories:
author: "Lisa Zyga"
tags: [Integer factorization,Quantum mechanics,Quantum computing,Prime number,Factorization,Ensemble (mathematical physics),Conjecture,Quantum entanglement,Simulation,Theoretical physics,Science,Applied mathematics,Mathematics,Physics]
---


The new method determines the probability that any prime number is one of the two prime factors of a given number. Instead it involves a physical quantum system—a quantum simulator—that, when encoded with the number to factor, exhibits a probability distribution of energy values that is equivalent to the probability distribution of the prime factor candidates of the encoded number. Our aim is to develop a new quantum theory of the factorization problem using a quantum simulator, Rosales said. In their paper, they report the results of using their method to determine the probability distribution of the prime factors of a number with 24 digits. One final point of interest is that the new method has strong connections to the Riemann hypothesis, which, if true, would suggest that the prime numbers are distributed in a predictable way—in the same way as the distribution of the zeros of the Riemann-zeta function.

<hr>

[Visit Link](https://phys.org/news/2018-04-quantum-simulator-faster-route-prime.html){:target="_blank" rel="noopener"}


