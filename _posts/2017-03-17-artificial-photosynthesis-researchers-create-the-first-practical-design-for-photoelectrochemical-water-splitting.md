---
layout: post
title: "Artificial photosynthesis: Researchers create the first practical design for photoelectrochemical water splitting"
date: 2017-03-17
categories:
author: "Forschungszentrum Juelich"
tags: [Solar energy,Water splitting,Solar cell,Photovoltaics,Forschungszentrum Jlich,Artificial photosynthesis,Sustainable energy,Chemistry,Sustainable technologies,Nature,Energy technology,Energy,Technology]
---


The photosynthesis system of the Jülich solar cell scientists is compact and self-contained, and the flexible design allows for upscaling. Credit: Forschungszentrum Jülich  Scientists from Forschungszentrum Juelich have developed the first complete and compact design for an artificial photosynthesis facility. The fluctuating nature of these renewable energy sources means that current research is focusing more intensively on efficient storage technologies. As yet, research has focused on materials science for new absorber materials and catalysts to further improve efficiency. The individual components and materials have been improved, but nobody has actually tried to achieve a real application.

<hr>

[Visit Link](http://phys.org/news/2016-09-artificial-photosynthesis-photoelectrochemical.html){:target="_blank" rel="noopener"}


