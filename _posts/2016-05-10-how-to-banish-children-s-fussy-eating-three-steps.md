---
layout: post
title: "How to banish children's fussy eating: Three steps"
date: 2016-05-10
categories:
author: Aston University
tags: [Child,Health,Health sciences,Nutrition]
---


Introducing the 'three Rs' -- Repetition, Role Modelling and Rewards -- at meal times could help parents to get their children to eat, and even like, new vegetables, according to new research from Aston and Loughborough Universities. It can be very challenging for families to encourage their children to eat a healthy, balanced diet as children naturally go through stages during their toddler years when they are often fussy and will refuse new foods, particularly vegetables. Our research shows that a combination of repeatedly exposing children to vegetables, rewarding them for trying the food and modelling enjoying eating the vegetable yourself, can help to encourage children to taste and eventually like vegetables which they did not previously like eating. At the end of the study, the group of children introduced to the 'three Rs' or 'two Rs' (rewards and repeated exposure) showed significant increases in the amount of vegetable they would eat and in their liking for the previously disliked vegetable. A recent survey conducted by the BBC found that half of all children in the UK aged between seven and 12 do not eat fruit or vegetables every day.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150629075941.htm){:target="_blank" rel="noopener"}


