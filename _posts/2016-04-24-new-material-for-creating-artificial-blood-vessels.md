---
layout: post
title: "New material for creating artificial blood vessels"
date: 2016-04-24
categories:
author: Vienna University of Technology
tags: [Blood vessel,Biomaterial,Materials,Technology,Clinical medicine,Chemistry,Medical specialties,Medicine]
---


TU Wien and MedUni Vienna have developed artificial blood vessels, which are broken down by the body and replaced with its own tissue  This news release is available in German. It is often necessary to replace a blood vessel - either by another vessel taken from the body or even by artificial vascular prostheses. Over time, these artificial blood vessels are replaced by endogenous material. TU Wien has therefore developed new polymers. Endogenous cells had colonized the vascular prostheses and turned the artificial constructs into natural body tissue.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/vuot-nmf042815.php){:target="_blank" rel="noopener"}


