---
layout: post
title: "Geoengineering Earth's Atmosphere: How It Could Affect Astronomy"
date: 2017-03-17
categories:
author: "Nola Taylor Redd"
tags: [Climate engineering,Climate change,Atmosphere of Earth,Astronomy,Carbon dioxide,Environmental impact,Environmental issues with fossil fuels,Applied and interdisciplinary physics,Societal collapse,Climate,Physical geography,Natural environment,Earth sciences,Climate variability and change,Global environmental issues,Nature]
---


Panelists at the 229th meeting of the American Astronomical Society discussed geoengineering Earth's atmosphere to reduce the impact of climate change and the effect it might have on night-sky viewing and astronomy. He was joined by Jane Long, a retired scientist at Lawrence Livermore National Laboratory; Tom Ackerman, a professor of atmospheric science at the University of Washington; and Mel Ulmer, an astronomer at Northwestern University. Geoengineering Earth's atmosphere includes several different proposals for removing carbon dioxide from the atmosphere. 'More important than astronomy'  The panel members agreed that geoengineering was only a short-term solution to help scale down the planet's temperature before it reaches catastrophic levels — it's not a long-term, permanent solution. Fixing global warming is more important than astronomy, Grinspoon said.

<hr>

[Visit Link](http://www.livescience.com/57567-geoengineering-earth-atmosphere-could-affect-astronomy.html){:target="_blank" rel="noopener"}


