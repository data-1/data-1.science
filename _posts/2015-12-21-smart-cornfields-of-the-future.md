---
layout: post
title: "Smart cornfields of the future"
date: 2015-12-21
categories:
author: Washington University in St. Louis
tags: [Photorespiration,RuBisCO,Photosynthesis,Photochemistry,Nature,Biology,Plant metabolism,Biochemistry,Physical sciences,Chemistry]
---


A plant is probably never going to reach solar cell efficiencies, but solar cells are not going to make you lunch, Blankenship said. If plants had antenna complexes sized for the light intensity at their level of the canopy they could absorb light more judiciously. They use some of the other light, the light in the near infrared. RuBisCO catalyzes the first major step in carbon fixation, the process by which the carbon atom in atmospheric carbon dioxide is added to a carbon molecule. All of the enzymes needed to do C4 photosynthesis are already in the plant.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/wuis-sco071415.php){:target="_blank" rel="noopener"}


