---
layout: post
title: "ATLAS Experiment searches for new symmetries of nature"
date: 2017-10-08
categories:
author: "Atlas Experiment"
tags: [Symmetry (physics),Elementary particle,W and Z bosons,ATLAS experiment,Lepton,Invariant mass,W and Z bosons,Force,Fundamental interaction,Mass,Gauge theory,Weak interaction,Neutrino,Theoretical physics,Physics,Particle physics,Science,Physical sciences,Quantum mechanics,Scientific theories,Quantum field theory,Applied and interdisciplinary physics]
---


Figure 1: ATLAS event display of the electron channel event with the highest transverse mass found in the 13 TeV data in the search for the W' boson. The particular symmetry related to the forces acting among particles is called gauge symmetry. The resulting gauge bosons that carry the forces are: the massless photon for electromagnetism, the massless gluons for the strong interaction, and the massive W and Z bosons for the weak interaction. If nature has symmetries beyond those we currently know of, we could observe additional force carrying particles. Explore further From supersymmetry to the Standard Model: New results from the ATLAS experiment  More information: Paper: Search for a new heavy gauge boson resonance decaying into a lepton and missing transverse momentum in 36 fb−1 of proton-proton collisions at 13 TeV with the ATLAS experiment: Paper: Search for a new heavy gauge boson resonance decaying into a lepton and missing transverse momentum in 36 fb−1 of proton-proton collisions at 13 TeV with the ATLAS experiment: atlas.web.cern.ch/Atlas/GROUPS … ATLAS-CONF-2017-016/

<hr>

[Visit Link](https://phys.org/news/2017-04-atlas-symmetries-nature.html){:target="_blank" rel="noopener"}


