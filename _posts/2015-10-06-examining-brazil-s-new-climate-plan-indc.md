---
layout: post
title: "Examining Brazil’s New Climate Plan (INDC)"
date: 2015-10-06
categories:
author: World Resources Institute, Steve Hanley, Guest Contributor, Carolyn Fortuna, Written By
tags: [Nationally determined contribution,Renewable energy,Greenhouse gas emissions,Low-carbon economy,Deforestation,Climate change,Natural environment,Global environmental issues,Sustainable development,Nature,Climate variability and change,Environmental issues with fossil fuels,Energy,Economy,Environmental impact,Societal collapse,Economy and the environment,Change]
---


The country’s INDC comes on the heels of joint climate change declarations it’s made in recent months with China, Germany and the United States, showing that the country is committed to a creating a successful international climate agreement in Paris later this year. For example, the Climate Observatory, a network of Brazilian NGOs, proposed an INDC to reduce Brazil’s GHG emissions by 57 percent compared to 2005 levels, with actions focused on eliminating emissions from deforestation and increasing substantially the share of modern renewables, mostly wind and solar. The emphasis on non-hydro sources means that biomass, solar and wind will play a greater role in Brazil’s overall energy mix than at present—hydropower currently generates about 66 percent of Brazil’s electricity; other renewable energy sources, less than 10 percent. Adaptation  The Brazilian government conducted stakeholder consultation on its INDC with members of academia, NGOs, and the private and public sectors. Brazil is playing a productive role in the global effort to fight climate change by becoming the first major developing country to take on an absolute emissions-reduction target.

<hr>

[Visit Link](http://cleantechnica.com/2015/10/05/examining-brazils-new-climate-plan-indc/){:target="_blank" rel="noopener"}


