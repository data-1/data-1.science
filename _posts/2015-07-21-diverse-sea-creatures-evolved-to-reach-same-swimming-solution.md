---
layout: post
title: "Diverse sea creatures evolved to reach same swimming solution"
date: 2015-07-21
categories:
author: PLOS 
tags: [Fin,Evolution,Animal,Biology,Engineering,Gymnotiformes,Nature]
---


And, remarkably, this so-called convergent evolution happened at least eight times across three different phyla, or animal groups, supporting the belief that necessity played a larger role than chance in developing this trait. In our study, we have quantified how an unusual group of swimming animals optimizes force and, therefore, speed. Technically, it's chance versus physics, said Neelesh A. Patankar, a close collaborator of MacIver's and the study's other senior author. The researchers call the recurring ratio of 20 the optimal specific wavelength (OSW). Key Findings:  Elongated fins use same mechanical motion to optimize speed across eight animal groups  Necessity triumphed over chance in developing vital trait, researchers say  Rare instance of quantifying mechanical basis for convergent evolution  Study furthers understanding of interaction between chance and necessity in biology  Important engineering implications for next generation of underwater vehicles  ###  Please mention PLOS Biology as the source for this article and include the links below in your coverage to take readers to the online, open access articles.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/p-dsc042415.php){:target="_blank" rel="noopener"}


