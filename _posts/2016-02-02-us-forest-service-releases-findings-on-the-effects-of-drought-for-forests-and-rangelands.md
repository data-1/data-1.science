---
layout: post
title: "US Forest Service releases findings on the effects of drought for forests and rangelands"
date: 2016-02-02
categories:
author: Usda Forest Service
tags: [Wildfire,United States Forest Service,Rangeland,Drought,Climate change,Ecology,Earth sciences,Natural environment,Global environmental issues,Physical geography]
---


The U.S. Forest Service today released a new report, Effects of Drought on Forests and Rangelands in the United States: A Comprehensive Science Synthesis, that provides a national assessment of peer-reviewed scientific research on the impacts of drought on U.S. forests and rangelands. This report will help the Forest Service better manage forests and grasslands impacted by climate change. The report establishes a comprehensive baseline of available data that land managers can use to test how well their efforts to improve drought resilience and adaptation practices are working nationwide. Major findings from the report include:  Drought projections suggest that some regions of the U.S. will become drier and that most will have more extreme variations in precipitation. The collaborative effort, authored by 77 scientists from the Forest Service, other Federal agencies, research institutions and universities across the United States, examines ways to understand and mitigate the effects of drought on forests and rangeland including the 193 million acres of National Forest System lands.

<hr>

[Visit Link](http://phys.org/news/2016-02-forest-effects-drought-forests-rangelands.html){:target="_blank" rel="noopener"}


