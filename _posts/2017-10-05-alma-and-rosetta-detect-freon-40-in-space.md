---
layout: post
title: "ALMA and Rosetta detect freon-40 in space"
date: 2017-10-05
categories:
author: "University of Copenhagen - Faculty of Science"
tags: [Chloromethane,67PChuryumovGerasimenko,Planetary science,Astronomy,Physical sciences,Space science,Nature,Outer space,Astronomical objects,Chemistry,Science]
---


Organohalogens are formed by organic processes on Earth, but this is the first ever detection of them in interstellar space. This discovery suggests that organohalogens may not be as good markers of life as had been hoped, but that they may be significant components of the material from which planets form. Using data captured by ALMA in Chile and from the ROSINA instrument on ESA's Rosetta mission, a team of astronomers has found faint traces of the chemical compound Freon-40 (CH3Cl), also known as methyl chloride and chloromethane, around both the infant star system IRAS 16293-2422 [1], about 400 light-years away, and the famous comet 67P/Churyumov-Gerasimenko (67P/C-G) in our own Solar System. This new discovery of one of these compounds, Freon-40, in places that must predate the origin of life, can be seen as a disappointment, as earlier research had suggested that these molecules could indicate the presence of life. Jes Jørgensen adds: This result shows the power of ALMA to detect molecules of astrobiological interest toward young stars on scales where planets may be forming.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/fos--aar100417.php){:target="_blank" rel="noopener"}


