---
layout: post
title: "World’s largest chain of volcanoes discovered in Australia"
date: 2015-09-15
categories:
author: "$author"   
tags: []
---


“This volcanic chain was created over the past 33 million years, as Australia moved north-northeast over a mantle plume hotspot which we believe is now located in Bass Strait,” the study’s lead author Rhodri Davies of Australian National University, said. “This track, which we’ve named the Cosgrove hotspot track, is nearly three times as long as the famous Yellowstone hotspot tracks on the North American continent,” he said, adding this kind of volcanic activity is surprising because it occurs away from tectonic plate boundaries where most volcanoes are found. “The volcanoes in central Queensland showed an age progression, so they got younger towards the south, and so too did those in New South Wales and Victoria,” Mr. Davies said. “It is always nice to discover something like this. We are getting much better at understanding volcanism in Australia.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/energy-and-environment/worlds-largest-chain-of-volcanoes-discovered-in-australia/article7654940.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


