---
layout: post
title: "A new experiment to understand dark matter"
date: 2018-06-17
categories:
author: "Max Planck Society"
tags: [Pulsar,Dark matter,Fifth force,Equivalence principle,Matter,Neutron star,Orbit,Binary pulsar,Universe,Science,Astronomy,Physical sciences,Physics,Space science,Nature,Astrophysics,Physical cosmology,Celestial mechanics,Astronomical objects]
---


The two arrows indicate the direction of the attractive forces, towards the standard matter—stars, gas, etc. More recently, the satellite experiment MICROSCOPE managed to confirm the universality of free fall in the gravitational field of the Earth with a precision of 1:100 trillion. With the unknown nature of dark matter another important question arises: is gravity the only long-range interaction between normal matter and dark matter? The orbit of a binary pulsar can be obtained with high precision by measuring the arrival time of the radio signals of the pulsar with radio telescopes. If the pulsar feels a different acceleration towards dark matter than the white dwarf companion, one should see a deformation of the binary orbit over time, i.e. a change in its eccentricity.

<hr>

[Visit Link](https://phys.org/news/2018-06-dark.html){:target="_blank" rel="noopener"}


