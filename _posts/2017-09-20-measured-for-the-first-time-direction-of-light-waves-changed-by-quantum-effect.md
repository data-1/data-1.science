---
layout: post
title: "Measured for the first time: Direction of light waves changed by quantum effect"
date: 2017-09-20
categories:
author: "Vienna University of Technology"
tags: [Quantum Hall effect,Light,Topological insulator,Wave,Topology,Time,Quantum,Measurement,Physics,Hall effect,Optics,Fine-structure constant,Electrical engineering,Electromagnetism,Applied and interdisciplinary physics,Electromagnetic radiation,Science,Physical sciences,Physical phenomena,Quantum mechanics,Theoretical physics]
---


The 'quantized magneto-electric effect' has been demonstrated for the first time in topological insulators at TU Wien, which is set to open up new and highly accurate methods of measurement  A light wave sent through empty space always oscillates in the same direction. This is known as a 'magneto-optical' effect. Rather than switching the direction of the light wave continually, special materials called 'topological insulators' do so in quantum steps in clearly defined portions. The extent of these quantum steps depends solely on fundamental physical parameters, such as the fine-structure constant. Topological insulators  We have been working on materials that can change the direction of oscillation of light for some time now, explains Prof. Andrei Pimenov from the Institute of Solid State Physics at TU Wien.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-05/vuot-mft052417.php){:target="_blank" rel="noopener"}


