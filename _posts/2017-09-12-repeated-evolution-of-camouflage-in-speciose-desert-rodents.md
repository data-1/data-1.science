---
layout: post
title: "Repeated evolution of camouflage in speciose desert rodents"
date: 2017-09-12
categories:
author: "Boratyński, Cibio-Inbio Associate Laboratory, Research Center In Biodiversity, Genetic Resources, University Of Porto, Vairão, Brito, José C., Department Of Biology, Faculty Of Science"
tags: []
---


To test for repeatability of colouration estimates (representativeness of dorsal colour), a total of 40 samples were randomly selected with “Sampling Design Tool” (in ArcGIS 10.166) on which selected areas for colour estimation were moved to different locations, and calculations were repeated. Finally, PVR decomposes phylogenetic distance matrices into the phylogenetic eigenvectors that can be included in statistical modelling of predictor variables4, 75. The phylogenetic eigenvectors for Gerbillus species were generated while including the species’ mean phenotypic values (fur reflectance and RGB colours) in R 3.1.3 “PVR” package. The tree topology and branch lengths were used to test the concordance of mode of evolution in the studied traits and Brownian motion evolutionary model by estimating phylogenetic signal-representation (PSR) curves81, and to select phylogenetic eigenvectors for statistical modelling75. Due to multicollinearity in our dataset, the relationship between the response variables (animal fur reflectance and RGB colours), habitat variables (substrate reflectance and RGB colours) and phylogenetic signal (represented by phylogenetic eigenvectors) was tested on comprehensive sample size (n = 460) with partial least squares regression (PLSR), a method extremely resilient to the correlations between predictor variables82.

<hr>

[Visit Link](http://www.nature.com/articles/s41598-017-03444-y?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


