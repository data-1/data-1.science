---
layout: post
title: "Venus Express snaps swirling vortex"
date: 2015-09-03
categories:
author: "$author"   
tags: [Venus,Venus Express,Atmosphere of Earth,Vortex,Polar vortex,Astronomical objects known since antiquity,Terrestrial planets,Astronomical objects,Solar System,Bodies of the Solar System,Planetary science,Sky,Outer space,Planets,Astronomy,Planets of the Solar System,Space science]
---


Venus has a very choppy and fast-moving atmosphere – although wind speeds are sluggish at the surface, they reach dizzying speeds of around 400 km/h at the altitude of the cloud tops, some 70 km above the surface. This is very rapid; even Earth’s fastest winds move at most about 30% of our planet’s rotation speed. However, other than brief glimpses from the Pioneer Venus and Mariner 10 missions in the 1970s, Venus’ south pole had not been seen in detail until ESA’s Venus Express first entered orbit in April 2006. One of Venus Express’ first discoveries, made during its very first orbit, was confirming the existence of a huge atmospheric vortex circulation at the south pole with a shape matching the one glimpsed at the north pole. The swirling region shown in this VIRTIS image is about 60 km above the planet’s surface.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2015/01/Venus_Express_snaps_swirling_vortex){:target="_blank" rel="noopener"}


