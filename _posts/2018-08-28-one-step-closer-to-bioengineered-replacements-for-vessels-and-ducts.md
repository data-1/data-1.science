---
layout: post
title: "One step closer to bioengineered replacements for vessels and ducts"
date: 2018-08-28
categories:
author: "Brigham and Women's Hospital"
tags: [3D bioprinting,Blood vessel,Tissue (biology),Medical specialties,Clinical medicine,Anatomy,Medicine]
---


The 3-D bioprinting technique allows fine-tuning of the printed tissues' properties, such as number of layers and ability to transport nutrients. Many disorders damage tubular tissues: arteritis, atherosclerosis and thrombosis damage blood vessels, while urothelial tissue can suffer inflammatory lesions and deleterious congenital anomalies. They fitted the bioprinter with a custom nozzle that would allow them to continuously print tubular structures with up to three layers. The researchers found that they could print tissues mimicking both vascular tissue and urothelial tissue. According to Zhang, structural complexity of bioprinted tissue is critical to its viability as a replacement for native tissue.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/bawh-osc082418.php){:target="_blank" rel="noopener"}


