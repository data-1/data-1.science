---
layout: post
title: "Feeding Future Generations With AI"
date: 2018-07-18
categories:
author: "May."
tags: []
---


Identifying crop seedlings from weed seedlings can help incite specific weed control. We will use Deep Learning Studio (DLS) for building this deep learning model. You can learn more about this dataset by checking out the original paper at https://arxiv.org/abs/1711.05458  The processed dataset has been compiled for use with Deep Learning Studio and has a total of 4750 images with a resolution of 256x256. If you want to see how to implement a ResNet in Keras, checkout these repositories:  https://github.com/raghakot/keras-resnet  https://github.com/broadinstitute/keras-resnet  Classifying Seedling Images With Resnet50  ResNet architectures (https://arxiv.org/abs/1512.03385 ) that use residual connections have been very successful at image classification tasks. Hyperparameters      Finally, you can start the training from Training Tab and monitor the progress with training dashboard.

<hr>

[Visit Link](https://dzone.com/articles/feeding-future-generations-with-ai?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


