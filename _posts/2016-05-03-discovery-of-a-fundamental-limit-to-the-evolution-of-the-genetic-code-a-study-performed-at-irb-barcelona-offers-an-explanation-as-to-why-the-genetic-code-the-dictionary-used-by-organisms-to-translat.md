---
layout: post
title: "Discovery of a fundamental limit to the evolution of the genetic code: A study performed at IRB Barcelona offers an explanation as to why the genetic code, the dictionary used by organisms to translat"
date: 2016-05-03
categories:
author: Institute for Research in Biomedicine (IRB Barcelona)
tags: [Genetics,Transfer RNA,Genetic code,Nucleic acid sequence,Organism,Protein,Gene,Translation (biology),Synthetic biology,Amino acid,Evolution,Life sciences,Nature,Molecular biology,Molecular genetics,Biology,Biotechnology,Biochemistry,Branches of genetics,Chemistry]
---


Nature is constantly evolving--its limits determined only by variations that threaten the viability of species. This halt in the increase in the complexity of life happened more than 3,000 million years ago, before the separate evolution of bacteria, eukaryotes and archaebacteria, as all organisms use the same code to produce proteins from genetic information. The authors of the study explain that the machinery that translates genes into proteins is unable to recognise more than 20 amino acids because it would confuse them, which would lead to constant mutations in proteins and thus the erroneous translation of genetic information with catastrophic consequences, in Ribas' words. A limitation imposed by shape  Saturation of the genetic code has its origin in transfer RNAs (tRNAs), the molecules responsible for recognising genetic information and carrying the corresponding amino acid to the ribosome, the place where chain of amino acids are made into proteins following the information encoded in a given gene. Application in synthetic biology  One of the goals of synthetic biology is to increase the genetic code and to modify it to build proteins with different amino acids in order to achieve novel functions.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/05/160502093700.htm){:target="_blank" rel="noopener"}


