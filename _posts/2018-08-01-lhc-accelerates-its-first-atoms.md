---
layout: post
title: "LHC accelerates its first 'atoms'"
date: 2018-08-01
categories:
author: "Sarah Charley"
tags: [Large Hadron Collider,Electron,Matter,Gamma ray,CERN,Atom,Particle physics,Subatomic particle,Particle beam,Muon,Photon,Nuclear physics,Physical sciences,Nature,Applied and interdisciplinary physics,Atomic physics,Physics]
---


During a special one-day run, LHC operators injected lead atoms containing a single electron into the machine. On Wednesday, 25 July, for the very first time, operators injected not just atomic nuclei but lead atoms containing a single electron into the LHC. This special LHC run was really the last step in a series of tests, says physicist Witold Krasny, who is coordinating a study group of about 50 scientists to develop new ways to produce high-energy gamma rays. They kept the beam circulating for two hours before intentionally dumping it. These gamma rays would have sufficient energy to produce normal matter particles, such as quarks, electrons and even muons.

<hr>

[Visit Link](https://phys.org/news/2018-07-lhc-atoms.html){:target="_blank" rel="noopener"}


