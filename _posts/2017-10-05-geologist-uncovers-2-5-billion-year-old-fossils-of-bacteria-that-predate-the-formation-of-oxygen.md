---
layout: post
title: "Geologist uncovers 2.5 billion-year-old fossils of bacteria that predate the formation of oxygen"
date: 2017-10-05
categories:
author: "Melanie Schefft, University Of Cincinnati"
tags: [Geologic time scale,Great Oxidation Event,Vaalbara,Fossil,Geology,Life,Supercontinent,Bacteria,Earth sciences,Physical sciences,Nature]
---


Credit: Andrew Czaja, UC assistant professor of geology  Somewhere between Earth's creation and where we are today, scientists have demonstrated that some early life forms existed just fine without any oxygen. These are the oldest reported fossil sulfur bacteria to date, says Andrew Czaja, UC assistant professor of geology. In his research published in the December issue of the journal Geology of the Geological Society of America, Czaja and his colleagues Nicolas Beukes from the University of Johannesburg and Jeffrey Osterhout, a recently graduated master's student from UC's department of geology, reveal samples of bacteria that were abundant in deep water areas of the ocean in a geologic time known as the Neoarchean Eon (2.8 to 2.5 billion years ago). Video/Andrew Czaja  According to Czaja, scientists through the years have theorized that South Africa and Western Australia were once part of an ancient supercontinent called Vaalbara, before a shifting and upending of tectonic plates split them during a major change in the Earth's surface. There is an ongoing debate about when sulfur-oxidizing bacteria arose and how that fits into the earth's evolution of life, Czaja adds.

<hr>

[Visit Link](http://phys.org/news/2016-11-geologist-uncovers-billion-year-old-fossils-bacteria.html){:target="_blank" rel="noopener"}


