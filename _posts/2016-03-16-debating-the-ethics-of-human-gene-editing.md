---
layout: post
title: "Debating the ethics of human gene editing"
date: 2016-03-16
categories:
author:  
tags: []
---


‘Germline’ editing can have an impact on future generations. DNA double helix. The sequence of the base groups (adenine, cytosine, guanine, thymine) along the DNA helix is known as the genetic code. It’s a question that gained urgency after Chinese researchers made the first attempt at editing genes in human embryos, a laboratory experiment that didn’t work well but did raise the prospect of one day altering human heredity passing modified DNA to future generations. On the other side are critics who say that so-called germline editing altering sperm, eggs or embryos to affect future generations has been widely regarded as a line science shouldn’t cross.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/debating-the-ethics-of-human-gene-editing/article7937779.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


