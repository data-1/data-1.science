---
layout: post
title: "Study: 1 in 6 Deaths Worldwide Caused by Pollution in 2015"
date: 2017-10-23
categories:
author:  
tags: [Pollution,Violence,Health,Public sphere,Natural environment,Politics,Health sciences,Environment,Environmental issues]
---


Deadly Pollution  We all know that pollution, in general, is harmful to human health. That's roughly 9 million people. Of these 9 million annual deaths, 92% occur in less wealthy nations, signaling an obvious socioeconomic divide. According to the study, pollution causes approximately 16% of all deaths worldwide — a figure that is 15 times larger than deaths caused by war and violence. Besides causing the deaths of millions, pollution has also been affecting our planet in other ways: it has even sparked lightning storms.

<hr>

[Visit Link](https://futurism.com/pollution-deaths-worldwide/){:target="_blank" rel="noopener"}


