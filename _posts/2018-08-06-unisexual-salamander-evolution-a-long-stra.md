---
layout: post
title: "Unisexual salamander evolution: A long, stra"
date: 2018-08-06
categories:
author: "Ohio State University"
tags: [Species,Mole salamander,Reproduction,Salamander,Evolution,Life sciences,Evolutionary biology,Nature,Genetics,Biological evolution,Biology]
---


In a new study, a team of researchers at The Ohio State University traced the animals' genetic history back 3.4 million years and found some head-scratching details - primarily that they seem to have gone for millions of years without any DNA contributions from male salamanders and still have managed to persist. Going into the study, the Ohio State team figured this sperm-borrowing happened with regularity throughout history, said study co-author H. Lisle Gibbs, a professor of evolution, ecology and organismal biology. This research shows that millions of years went by where they weren't taking DNA from other species, and then there were short bursts where they did it more frequently, said Rob Denton, who led the project as an Ohio State graduate student and is currently a postdoctoral researcher at the University of Connecticut. We think these have been around for 5 million years, he said. Gibbs said it's possible that this research could inform other areas of study, including plant science, because many plants are - like the unisexual salamanders - polyploid organisms.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/osu-use072518.php){:target="_blank" rel="noopener"}


