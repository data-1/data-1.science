---
layout: post
title: "Hubble 25 (without title)"
date: 2015-09-03
categories:
author: "$author"   
tags: [Hubble Space Telescope,Sky,Outer space,Physical sciences,Astronomical objects,Space science,Astronomy,Stellar astronomy,Science]
---


From planets to planetary nebula, and from star formation to supernova explosions, the NASA/ESA Hubble Space Telescope has captured a wealth of astronomical objects in its 25-year career. This montage presents 25 images that sample the space telescope’s rich contribution to our understanding of the Universe around us. Follow the links below for more information and credits. At the centre of the montage is star cluster Westerlund 2, the image released on the occasion of Hubble’s 25th anniversary. From left to right; top row:  Second row:  Third row:  Fourth row:  Fifth row:

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2015/04/Hubble_25_without_title){:target="_blank" rel="noopener"}


