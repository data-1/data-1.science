---
layout: post
title: "Earliest-known arboreal and subterranean ancestral mammals discovered"
date: 2015-07-20
categories:
author: University of Chicago Medical Center 
tags: [Docofossor]
---


With claws for climbing and teeth adapted for a tree sap diet, Agilodocodon scansorius is the earliest-known tree-dwelling mammaliaform (long-extinct relatives of modern mammals). The other fossil, Docofossor brachydactylus, is the earliest-known subterranean mammaliaform, possessing multiple adaptations similar to African golden moles such as shovel-like paws. The spines and ribs of both Agilodocodon and Docofossor also show evidence for the influence of genes seen in modern mammals. That these ancient mammaliaforms had similar developmental patterns is an evidence that these gene networks could have functioned in a similar way long before true mammals evolved. We can now provide fossil evidence that gene patterning that causes variation in modern mammalian skeletal development also operated in basal mammals all the way back in the Jurassic.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/uocm-eaa020615.php){:target="_blank" rel="noopener"}


