---
layout: post
title: "Cameras delivered for OSIRIS-REx mission as launch prep continues"
date: 2016-05-31
categories:
author: "Nancy Neal Jones, Nasa'S Goddard Space Flight Center"
tags: [OSIRIS-REx,Goddard Space Flight Center,101955 Bennu,Solar System,Bodies of the Solar System,NASA,Space program of the United States,Discovery and exploration of the Solar System,Planetary science,Flight,Spacecraft,Science,Space exploration,Astronautics,Astronomy,Space science,Outer space,Spaceflight]
---


The University of Arizona’s camera suite, OCAMS, sits on a test bench that mimics its arrangement on the OSIRIS-REx spacecraft. They will map the asteroid Bennu, help choose a sample site, and ensure that the sample is correctly stowed on the spacecraft. OSIRIS-REx is the first U.S. mission to sample an asteroid, and will return the largest sample from space since the Apollo lunar missions. NASA's Goddard Space Flight Center in Greenbelt, Maryland, provides overall mission management, systems engineering and safety and mission assurance for OSIRIS-REx. Dante Lauretta is the mission's principal investigator at the University of Arizona, Tucson.

<hr>

[Visit Link](http://phys.org/news/2015-08-cameras-osiris-rex-mission-prep.html){:target="_blank" rel="noopener"}


