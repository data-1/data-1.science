---
layout: post
title: "Researchers harness DNA as the engine of super-efficient nanomachine"
date: 2016-07-09
categories:
author: "McMaster University"
tags: [Molecular machine,DNA,Bacteria,Virus,Genetics,Physical sciences,Chemistry,Biology,Life sciences,Molecular biology,Biochemistry,Biotechnology]
---


It's a completely new platform that can be adapted to many kinds of uses, says John Brennan, director of McMaster's Biointerfaces Insitute and co-author of a paper in the journal Nature Communications that describes the technology. The new method shapes separately programmed pieces of DNA material into pairs of interlocking circles. We can design the lock to be specific to a certain key. Yingfu Li is available at liying@mcmaster.ca and 905-525-9140, ext. 27988  C: 289-925-8382  hemswor@mcmaster.ca  Michelle Donovan  Media Relations Manager  905-525-9140 ext.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/mu-rhd070616.php){:target="_blank" rel="noopener"}


