---
layout: post
title: "Evaporation takes place differently than previously thought: Implications for global warming"
date: 2015-10-27
categories:
author: Institute of Physical Chemistry of the Polish Academy of Sciences
tags: [Evaporation,Gas,Liquid,Pressure,Thermodynamic equilibrium,Science,Physical chemistry,Chemistry,Physics,Nature,Physical sciences,Applied and interdisciplinary physics]
---


How does the given process really take place? What follows from it is quite an intuitive prediction: that at a given temperature the rate of evaporation of the liquid depends on how different the actual pressure at the surface is from the pressure which would be present if the evaporating liquid were to be in thermodynamic equilibrium with its environment. Advanced computer simulations carried out using molecular dynamics showed that the values of some parameters describing evaporation are even several times larger than those predicted by the Hertz-Knudsen equation. Since there is an increase in momentum, there must be recoil, and if there is recoil, the pressure felt by the molecules on the surface of the liquid will be different, says Prof. Holyst. The new computer simulations were also used to measure the velocities of the molecules released from the liquid surface.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151022103535.htm){:target="_blank" rel="noopener"}


