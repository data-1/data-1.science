---
layout: post
title: "Earth's core contains 90 percent of Earth's sulfur, new research shows"
date: 2016-05-08
categories:
author: European Association Of Geochemistry
tags: [Giant-impact hypothesis,Planetary core,Impact event,Physical sciences,Nature,Planetary science,Chemistry,Earth sciences,Geology]
---


New research confirms that the Earth's core does in fact contain vast amounts of sulphur, estimated to be up to 8.5 x 1018 tonnes. This is the first time that scientists have conclusive geochemical evidence for sulphur in the Earth's core, lending weight to the theory that the Moon was formed by a planet-sized body colliding with the Earth. The work comprised 3 distinct stages:  Firstly, the researchers had to estimate the isotopic composition of copper in the Earth's mantle and crust. Because the isotopes of copper divide unevenly between a sulphur-rich liquid and the rest of Earth's mantle, this shows that a large amount of sulphur must have been removed from the mantle. Paul Savage said: This study is the first to show clear geochemical evidence that a sulphide liquid must have separated from the mantle early on in Earth's history - which most likely entered the core.

<hr>

[Visit Link](http://phys.org/news353670203.html){:target="_blank" rel="noopener"}


