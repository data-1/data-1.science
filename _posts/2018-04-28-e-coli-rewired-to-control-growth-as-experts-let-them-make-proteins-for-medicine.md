---
layout: post
title: "E. coli rewired to control growth as experts let them make proteins for medicine"
date: 2018-04-28
categories:
author: "Caroline Brogan, Imperial College London"
tags: [Bacteria,Protein,Gene,Cell (biology),Biotechnology,Protein production,Escherichia coli,Synthetic biology,DNA,Gene expression,Chemistry,Technology,Biology,Genetics,Biochemistry,Molecular biology,Life sciences]
---


A bacterial cell equipped with the feedback loop. Credit: Imperial College London  Experts have equipped biotech workhorse bacteria with feedback control mechanism to balance growth with making protein products. The solution  To create the feedback system, the researchers first used RNA sequencing - a technique that measures the expression of all genes in a cell - to observe which genes inside the bacteria naturally change behaviour when the cell is burdened. The researchers showed that bacteria equipped with this feedback control system altered the level of proteins they produced depending on the burden. DOI: 10.1038/s41467-018-03970-x  Francesca Ceroni et al. Burden-driven feedback control of gene expression, Nature Methods (2018).

<hr>

[Visit Link](https://phys.org/news/2018-04-coli-rewired-growth-experts-proteins.html){:target="_blank" rel="noopener"}


