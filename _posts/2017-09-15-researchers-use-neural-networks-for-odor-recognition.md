---
layout: post
title: "Researchers use neural networks for odor recognition"
date: 2017-09-15
categories:
author: "National Research University Higher School Of Economics"
tags: [Electronic nose,Odor,Branches of science,Technology,Cognitive science,Computing]
---


Credit: National Research University Higher School of Economics  Researchers from the HSE Laboratory of Space Research, Technologies, Systems and Processes have applied fast-learning artificial intelligence systems to odour recognition and developed an electronic nose device capable of recognising the olfactory patterns of a wide range of chemicals. In addition to discriminating between different gas mixtures, the electronic nose will be able to capture and memorise new smells. Electronic nose devices are gas analysers used for measuring the qualitative and quantitative composition of gas mixtures. MIEM HSE researchers are now working on algorithms, software solutions and techniques for neural network odour recognition. In this case, the new olfactory pattern will be uploaded to the database and a new neural network trained for this smell.

<hr>

[Visit Link](https://phys.org/news/2017-08-neural-networks-odor-recognition.html){:target="_blank" rel="noopener"}


