---
layout: post
title: "wattOS Energizes Aging Hardware"
date: 2017-10-10
categories:
author: ""
tags: []
---


wattOS is a lightweight and fast desktop Linux distribution based on Ubuntu 16.04.1 LTS built around the LXDE. One of the settings panels in that system tool lets you check for and add additional drivers for your hardware. The included tools for better power management and performance optimization are also a nice addition. Unlike with the Xfce lightweight desktop, you can not access the software main menu by right-clicking on the desktop. Is there a Linux software application or distro you’d like to suggest for review?

<hr>

[Visit Link](http://www.linuxinsider.com/story/84076.html?rss=1){:target="_blank" rel="noopener"}


