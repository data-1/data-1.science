---
layout: post
title: "ARIEL mission to reveal 'Brave New Worlds' among exoplanets"
date: 2016-05-14
categories:
author: Europlanet Media Centre
tags: [ARIEL,Exoplanet,Astronomical objects,Astronomy,Space science,Outer space,Planetary science,Planets,Science,Physical sciences]
---


Concept view of the ARIEL spacecraft. Credit ESA  An ambitious European mission is being planned to answer fundamental questions about how planetary systems form and evolve. ARIEL will investigate the atmospheres of several hundred planets orbiting distant stars. Hot exoplanets represent a natural laboratory in which to study the chemistry and formation of planets. This will spread the light into a 'rainbow' and extract the chemical fingerprints of gases in the planets' atmospheres, as the planet passes in front or behind the star.

<hr>

[Visit Link](http://phys.org/news/2015-07-ariel-mission-reveal-brave-worlds.html){:target="_blank" rel="noopener"}


