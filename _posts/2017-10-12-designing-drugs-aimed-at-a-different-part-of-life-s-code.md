---
layout: post
title: "Designing drugs aimed at a different part of life's code"
date: 2017-10-12
categories:
author: "Duke University"
tags: [RNA,Protein,Biology,Life sciences,Biotechnology,Molecular biology,Biochemistry,Chemistry,Genetics,Biomolecules]
---


Hargrove and her team at Duke are working to design new types of drugs that target RNA rather than proteins. RNA-targeted drug molecules have the potential help treat diseases like prostate cancer and HIV, but finding them is no easy task. Most drugs have been designed to interfere with proteins, and just don't have the same effects on RNA. They then analyzed 20 different properties of these molecules, and compared their properties to those of collections of drug molecules known to interact with proteins. What that means is that we could start to enrich our screening libraries with these types of molecules, and make these types of molecules, to have better luck at targeting RNA.

<hr>

[Visit Link](https://phys.org/news/2017-10-drugs-aimed-life-code.html){:target="_blank" rel="noopener"}


