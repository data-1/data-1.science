---
layout: post
title: "Drug discovery could accelerate hugely with machine learning"
date: 2018-07-14
categories:
author: "University of Warwick"
tags: [Drug design,Chemistry,Science,Branches of science,Technology,Physical sciences]
---


The algorithm - partly devised by Dr James Kermode from Warwick's School of Engineering - can accurately predict the interactions between a protein and a drug molecule based on a handful of reference experiments or simulations. Using just a few training references, it can predict whether or not a candidate drug molecule will bind to a target protein with 99% accuracy. The approach, developed by scientists at the University of Warwick, the École polytechnique fédérale de Lausanne's Laboratory of Computational Science and Modelling, the University of Cambridge, the UK Science and Technology Facilities Council and the U.S. Dr James Kermode, from the University of Warwick's Warwick Centre for Predictive Modelling and the School of Engineering, commented on the research:  This work is exciting because it provides a general-purpose machine learning approach that is applicable both to materials and molecules. New algorithms allow us to predict the behaviour of new materials and molecules with great accuracy and little computational effort, saving time and money in the process.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/uow-ddc121417.php){:target="_blank" rel="noopener"}


