---
layout: post
title: "New plant research leads to discovery of a gene that increases seed yield in maize"
date: 2017-03-22
categories:
author: "VIB (the Flanders Institute for Biotechnology)"
tags: [Maize,Agriculture,Industries (economics),Food industry,Primary sector of the economy]
---


Researchers from VIB-UGent have discovered a gene that significantly increases plant growth and seed yield in maize. Nowadays, there is no crop in the world cultivated more than maize. PLA1 improves crop yield in maize  VIB-UGent scientists, headed by Prof. Dirk Inzé and Dr. Hilde Nelissen, are conducting research into the molecular mechanisms behind leaf growth in maize. Dirk Inzé (VIB-UGent) says: We have succeeded in significantly boosting biomass and seed production by increasing PLA1 expression in the plant, which leads to a yield increase of 10 to 15% on the same agricultural area. The field trials in Belgium were conducted in cooperation with ILVO [Institute for Agricultural and Fisheries Research].

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/vfi-npr031617.php){:target="_blank" rel="noopener"}


