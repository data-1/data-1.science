---
layout: post
title: "Rivers, lakes impact ability of forests to store carbon"
date: 2015-12-22
categories:
author: University Of Washington
tags: [Carbon cycle,Natural environment,Environmental science,Earth sciences,Environmental engineering,Systems ecology,Physical geography,Nature]
---


Butman, who is also an affiliate researcher with the U.S. Geological Survey, along with collaborators from the federal agency and the Wisconsin Department of Natural Resources, scoured all existing data and studies that accounted for carbon in freshwater rivers, lakes and reservoirs around the country, then synthesized the data and created new models to take a first-ever countrywide look at how much carbon is moving in the water. They found that freshwater rivers and streams transport or store more than 220 billion pounds of carbon each year. With these new U.S.-wide numbers, scientists may be overestimating the ability of terrestrial landscapes to store carbon by almost 30 percent, the study found. The first report, released in 2007, didn't include freshwater ecosystems as part of a national carbon assessment, Butman said. Results varied by region, but researchers found that the Pacific Northwest's high volume of rainfall each year moves carbon relatively quickly through the landscape and to coastal waters faster than in other regions.

<hr>

[Visit Link](http://phys.org/news/2015-12-rivers-lakes-impact-ability-forests.html){:target="_blank" rel="noopener"}


