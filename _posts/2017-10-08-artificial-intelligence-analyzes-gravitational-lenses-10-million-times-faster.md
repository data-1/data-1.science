---
layout: post
title: "Artificial intelligence analyzes gravitational lenses 10 million times faster"
date: 2017-10-08
categories:
author: "DOE/SLAC National Accelerator Laboratory"
tags: [SLAC National Accelerator Laboratory,Artificial neural network,Neural network,Particle accelerator,Lens,Universe,Office of Science,Science,Physics]
---


SLAC and Stanford researchers demonstrate that brain-mimicking 'neural networks' can revolutionize the way astrophysicists analyze their most complex data  Menlo Park, Calif. -- Researchers from the Department of Energy's SLAC National Accelerator Laboratory and Stanford University have for the first time shown that neural networks - a form of artificial intelligence - can accurately analyze the complex distortions in spacetime known as gravitational lenses 10 million times faster than traditional methods. Until now this type of analysis has been a tedious process that involves comparing actual images of lenses with a large number of computer simulations of mathematical lensing models. To train the neural networks in what to look for, the researchers showed them about half a million simulated images of gravitational lenses for about a day. This goes far beyond recent applications of neural networks in astrophysics, which were limited to solving classification problems, such as determining whether an image shows a gravitational lens or not. The amazing thing is that neural networks learn by themselves what features to look for, said KIPAC staff scientist Phil Marshall, a co-author of the paper.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-08/dnal-aia082917.php){:target="_blank" rel="noopener"}


