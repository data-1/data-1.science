---
layout: post
title: "From sticks to stones—getting a grip on the human genus"
date: 2016-04-24
categories:
author: Darren Curnoe, The Conversation
tags: [Australopithecus,Homo habilis,Homo,Olduvai Gorge,Paranthropus boisei,Human evolution,Oldowan,Human,Biological anthropology]
---


Probably the first hominin to make stone tools, according to new research. Deconstructing the Homo package  Ever since tool making was linked exclusively to Homo there have been dissenters. This pushed tool use well beyond Homo and Paranthropus to Kenyanthropus or even Australopithecus. Archaeologists have had a hard time wrestling with the problem of the uniqueness of human culture, especially in the face of the complexity of ape cultures, but as well as the use of tools across very diverse kinds of animals. Many of the hallmarks of the human genus have been convincingly found with other, much more ancient hominin species, and some even with other kinds of animals.

<hr>

[Visit Link](http://phys.org/news349081366.html){:target="_blank" rel="noopener"}


