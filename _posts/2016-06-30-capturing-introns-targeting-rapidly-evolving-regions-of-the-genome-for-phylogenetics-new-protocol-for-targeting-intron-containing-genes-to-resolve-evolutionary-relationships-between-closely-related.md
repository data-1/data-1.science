---
layout: post
title: "Capturing introns: Targeting rapidly evolving regions of the genome for phylogenetics: New protocol for targeting intron-containing genes to resolve evolutionary relationships between closely related "
date: 2016-06-30
categories:
author: "Botanical Society of America"
tags: [Phylogenetic tree,Intron,Evolution,Species,DNA sequencing,Gene,Non-coding DNA,DNA,Human genome,Biology,Biotechnology,Life sciences,Genetics,Branches of genetics,Molecular biology,Bioinformatics,Biochemistry,Evolutionary biology]
---


By analyzing the similarities and differences between DNA sequences of different organisms, biologists can infer relatedness between them. The new study describes a method for targeting introns by utilizing publicly available genomic data to find these rapidly evolving regions. Targeting a DNA region depends on sequence recognition between the unknown sample and a known 'target' sequence from a close relative. But our results show that introns were not particularly difficult to sequence as compared to the more commonly targeted exons. We are optimistic that our new method for targeting intronic regions will provide scientists with a valuable tool to answer questions in these difficult groups.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150909090615.htm){:target="_blank" rel="noopener"}


