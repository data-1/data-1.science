---
layout: post
title: "ISRG and The Linux Foundation to Collaborate"
date: 2015-05-22
categories:
author: ""    
tags: [Linux Foundation,Linux,Lets Encrypt,Computing,Technology,Information Age]
---


Internet Security Research Group (ISRG), the non-profit entity behind Let’s Encrypt, is pleased to announce our collaboration with The Linux Foundation. The Linux Foundation is a non-profit organization dedicated to advancing Linux and collaborative development. It also hosts the Core Infrastructure Initiative, which identifies and funds critical open source projects that support the world’s infrastructure. ISRG’s collaboration with The Linux Foundation will allow our staff and management to focus on carrying out our mission. Furthermore, sharing support infrastructure with other projects allows us to put more of our donors' money directly towards carrying out our mission.

<hr>

[Visit Link](https://letsencrypt.org//2015/04/09/isrg-lf-collaboration.html){:target="_blank" rel="noopener"}


