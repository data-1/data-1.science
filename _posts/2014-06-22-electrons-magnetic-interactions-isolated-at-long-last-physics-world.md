---
layout: post
title: "Electrons' magnetic interactions isolated at long last – Physics World"
date: 2014-06-22
categories:
author: Tushna Commissariat
tags: [Quantum entanglement,Quantum mechanics,Magnetism,Electron,Spin (physics),Magnetic moment,Time,Force,Spectroscopy,Science,Theoretical physics,Scientific theories,Applied and interdisciplinary physics,Physical sciences,Physics]
---


Spinning around: illustration showing magnetic interactions  A measurement of the extremely weak magnetic interaction between two single electrons has been carried out by an international team of physicists. Apart from measuring magnetism at the shortest length scale thus far, the researchers say that their technique could be applied to other measurement scenarios where noise is a dominant factor, such as for quantum-error corrections. In the researchers’ experiment, one electron is trying to sense the magnetic field of the other. Elongated entanglement  Even more surprising is that this naturally created entanglement lasts for 15 s – a surprisingly long time for a system to remain in a coherent, quantum state. By varying the separation between the two ions, they were able to measure the strength of the magnetic interaction as a function of distance – confirming the expected inverse-cubic (1/d3) dependence of the interaction.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/WhFXtTrIqyE/electrons-magnetic-interactions-isolated-at-long-last){:target="_blank" rel="noopener"}


