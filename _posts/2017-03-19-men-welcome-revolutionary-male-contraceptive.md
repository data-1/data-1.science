---
layout: post
title: "Men welcome revolutionary male contraceptive"
date: 2017-03-19
categories:
author: "Taylor & Francis"
tags: [Reversible inhibition of sperm under guidance,Birth control,Male contraceptive,News aggregator,Reproductive rights,Human reproduction,Sexual health,Fertility,Human overpopulation,Health,Reproduction in mammals,Health sciences,Public health,Family planning]
---


A new study has found that men have positive attitudes towards an innovative male contraceptive, Vasalgel. The landmark study, published in Cogent Medicine, is the first insight into how men perceive the new contraceptive and gives promising signs that Vasalgel may revolutionise approaches to reproductive health. Vasalgel is a long-acting reversible contraceptive (LARC) currently under development which will redress this imbalance by offering a reliable contraceptive for men. The results of the survey showed that attitudes towards Vasalgel were predominantly favourable, 41% of all participants either moderately or strongly agree with the statement 'I would use Vasalgel if it became available' compared to only 22% who either moderately or strongly disagreed. Aisha King, one of the authors of the study commented 'this research strongly implies that young men today are ready to shoulder the contraceptive responsibility that has traditionally rested upon women.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/11/161102080117.htm){:target="_blank" rel="noopener"}


