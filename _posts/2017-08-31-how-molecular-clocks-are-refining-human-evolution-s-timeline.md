---
layout: post
title: "How molecular clocks are refining human evolution's timeline"
date: 2017-08-31
categories:
author: "Bridget Alex And Priya Moorjani, The Conversation"
tags: [Evolution,Human evolution,Mutation,Human,Interbreeding between archaic and modern humans,Molecular clock,Genetic recombination,Neanderthal,Genetics,Genetic variation,DNA,Genome,Most recent common ancestor,Human genome,Evolutionary biology,Branches of genetics,Biological evolution,Life sciences,Biology]
---


In humans, about 36 recombination events occur per generation, one or two per chromosome. The recombination clock, on the other hand, ticks at a rate appropriate for dates within the last 100,000 years. But when geneticists directly measure nucleotide differences between living parents and children (using human pedigrees), the mutation rate is half the other estimate: about 0.5x10⁻⁹ per site per year, or only about three mutations per year. Credit: Bridget Alex, CC BY-ND  For the divergence between Neanderthals and modern humans, the slower rate provides an estimate between 765,000-550,000 years ago. Oase’s segments are longer because he had a Neanderthal ancestor just 4–6 generations before he lived, based on estimates using the recombination clock.

<hr>

[Visit Link](https://phys.org/news/2017-04-molecular-clocks-refining-human-evolution.html){:target="_blank" rel="noopener"}


