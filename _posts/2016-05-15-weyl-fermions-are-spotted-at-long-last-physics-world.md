---
layout: post
title: "Weyl fermions are spotted at long last – Physics World"
date: 2016-05-15
categories:
author: Hamish Johnston
tags: [Elementary particle,Weyl semimetal,Fermion,Majorana fermion,Theoretical physics,Particle physics,Quantum mechanics,Physical sciences,Applied and interdisciplinary physics,Condensed matter,Condensed matter physics,Science,Quantum field theory,Physics]
---


Weyl points: the photonic crystal  Evidence for the existence of particles called Weyl fermions in two very different solid materials has been found by three independent groups of physicists. First predicted in 1929, Weyl fermions also have unique properties that could make them useful for creating high-speed electronic circuits and quantum computers. For particles with charge and mass, he found that the Dirac equation predicts the existence of the electron and its antiparticle the positron, the latter being discovered in 1932. Fermi arcs have also been predicted and then spotted in TaAs by an independent research group that includes Hongming Weng and colleagues at the Chinese Academy of Sciences. This allowed the researchers to map out the photonic band structure of the crystal, which reveals which microwave frequencies can travel through the crystal and which cannot.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/jul/23/weyl-fermions-are-spotted-at-long-last){:target="_blank" rel="noopener"}


