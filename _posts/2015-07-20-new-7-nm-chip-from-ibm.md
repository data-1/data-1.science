---
layout: post
title: "New 7 nm chip from IBM"
date: 2015-07-20
categories:
author: ""   
tags: []
---


IBM recently announced that they had produced the first 7-nanometre semiconductor test chip with functioning transistors. The level of miniaturisation is huge. “The two key ones were the use of silicon germanium channel transistors instead of traditional silicon, and using extreme ultraviolet (EUV) lithography integration at multiple levels, instead of optical lithography,” he said about the innovation. The development of this chip was the result of a public-private partnership between New York State and a joint development alliance with Global Foundries, Samsung and equipment suppliers along with IBM. The breakthrough is a direct result of IBM's $3 billion, five-year investment in chip R&D that we announced in 2014,” said Mr Khare.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/new-7-nm-chip-from-ibm/article7438235.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


