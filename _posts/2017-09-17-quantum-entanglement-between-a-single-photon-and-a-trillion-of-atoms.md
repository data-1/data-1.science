---
layout: post
title: "Quantum entanglement between a single photon and a trillion of atoms"
date: 2017-09-17
categories:
author: "University of Warsaw, Faculty of Physics"
tags: [Photon,Physics,Quantum entanglement,Quantum memory,Atom,Elementary particle,Physical sciences,Theoretical physics,Quantum mechanics,Science,Scientific theories,Applied and interdisciplinary physics,Scientific method]
---


A group of researchers from the Faculty of Physics at the University of Warsaw has created a multidimensional entangled state of a single photon and a trillion of hot rubidium atoms. In their famous Physical Review article published in 1935, A. Einstein, B. Podolsky and N. Rosen have considered a decay of a particle into two products. In the Quantum Memories Laboratory at the University of Warsaw, the group of three physicists was first to create such an entangled state consisting of a macroscopic object - a group of about one trillion atoms, and a single photon - a particle of light. A single registered photon carries information about the quantum state of the entire group of atoms. The results of the experiment confirm that the atoms and the single photon are in a joint, entangled state.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/fopu-qeb030117.php){:target="_blank" rel="noopener"}


