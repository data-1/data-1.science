---
layout: post
title: "USTC develops all-optically controlled non-reciprocal multifunctional photonic devices"
date: 2018-07-01
categories:
author: "University of Science and Technology of China"
tags: [Photonics,Circulator,Optics,Resonator,Waves,Physics,Atomic molecular and optical physics,Electrodynamics,Electronics,Electricity,Applied and interdisciplinary physics,Natural philosophy,Technology,Science,Electromagnetism,Radiation,Electrical engineering,Electromagnetic radiation]
---


Breaking this reciprocity in the direction of light transmission is of great significance in classical and quantum information processing. Optical circulators, isolators, directional amplifiers, etc. Yet the most common optical non-reciprocal devices are based on the Faraday effects using magneto-optical materials, which are difficult to integrate on-chip. Therefore, in recent years, interest has increased in realizing on-chip all-optical non-reciprocal devices. When only focusing on ports 1 and 2, it is also an efficient optical isolator; for directional amplifiers, signal light incident from port 1 is amplified and exits from port 2, not the other way around.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/uosa-uda050818.php){:target="_blank" rel="noopener"}


