---
layout: post
title: "Genome sequence of polar alga explains evolutionary adaptation to extreme variable climate"
date: 2017-03-22
categories:
author: "University of East Anglia"
tags: [Evolution,Genetics,Genome,Organism,Allele,Phytoplankton,Diatom,Gene,Ocean,Genomics,Biology,DNA sequencing,Nature]
---


The team led by Prof Thomas Mock from the University of East Anglia (UEA) School of Environmental Sciences investigated the evolutionary genomics of the polar diatom Fragilariopsis cylindrus, which has evolved to thrive in the Southern Ocean but also occurs in the Arctic Ocean. Species such as Fragilariopsis cylindrus have evolved adaptations to cope with these drastic environmental changes. The study, published today in the journal Nature, is a significant step in understanding how polar organisms have evolved to cope with their extremely variable environmental conditions and therefore their potential to adapt to environmental changes induced by human activity. Scientists at EI used cutting edge genomics technology and bioinformatics expertise to carry out additional studies on the original genome assembly provided by the Joint Genome Institute (US). With funding from an Institute Development Grant1, Dr Mark McMullan, Dr Pirita Paajanen in the group of Dr Matt Clark (Technology Development) used the latest PacBio long-read sequencing and assembly software to generate a new genome assembly and assign the allelic variations to separate chromosomes.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-01/uoea-gso011117.php){:target="_blank" rel="noopener"}


