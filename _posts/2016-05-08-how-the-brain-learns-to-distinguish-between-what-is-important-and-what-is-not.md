---
layout: post
title: "How the brain learns to distinguish between what is important and what is not"
date: 2016-05-08
categories:
author: University of Basel
tags: [Perception,Brain,Stimulus (physiology),Learning,Sonja Hofer,Neuron,Psychology,Cognitive science,Cognition,Mental processes,Interdisciplinary subfields,Psychological concepts,Cognitive psychology,Behavioural sciences,Neuropsychology,Neuropsychological assessment,Cognitive neuroscience,Neuroscience]
---


The brain learns to discriminate between images  To do this, Prof. Hofer's team investigated the visual cortex of mice. This part of the brain is responsible for the processing and perception of visual stimuli. While the responses in the brain to the relevant visual stimuli were quite unspecific in beginner mice, many more neurons reacted specifically to the shown images after one week of training. He speculates that such changes in the brain might also allow us to process important information from our environment more efficiently, and perhaps underlies our ability to react promptly to important visual stimuli. Internal signals influence visual perception  Traditionally it was thought that the visual cortex exclusively processes visual information.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/uob-htb061715.php){:target="_blank" rel="noopener"}


