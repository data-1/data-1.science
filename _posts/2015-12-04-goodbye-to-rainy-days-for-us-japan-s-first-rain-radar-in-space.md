---
layout: post
title: "Goodbye to rainy days for US, Japan's first rain radar in space"
date: 2015-12-04
categories:
author: "$author" 
tags: [Tropical Rainfall Measuring Mission,Rain,Meteorology,Sky,Physical geography,Earth sciences,Nature,Branches of meteorology,Atmospheric sciences,Weather,Earth phenomena]
---


As a result, NASA ceased station-keeping maneuvers that would keep the satellite at its operating altitude of 402 km (249.8 miles). Data from TRMM will continue to foster science well after the mission ends, and, when combined with data from the new Global Precipitation Measurement Core Observatory (GPM), launched earlier this year by NASA's partner the Japan Aerospace Exploration Agency (JAXA), will contribute to a long-term precipitation climate record. Another key instrument is the TRMM Microwave Imager, or TMI, that provides images of rainfall across a swath more than three times as wide as that from the radar. JAXA, which manages the Precipitation Radar data, recently stopped distribution of the radar data when TRMM fell below 392.5 km (243.9 miles) altitude. The Core Observatory continues and expands upon TRMM's capabilities, carrying an advanced GPM Microwave Imager and JAXA's Dual-frequency Precipitation Radar.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/nsfc-gtr103114.php){:target="_blank" rel="noopener"}


