---
layout: post
title: "What causes lightning?"
date: 2016-05-12
categories:
author: Matt Williams
tags: [Lightning,Jupiter,Sky,Physical phenomena,Nature,Space science]
---


Credit: noaanews.noaa.gov  Thunder and lightning. For example, there is intra-cloud lighting (IC), which takes place between electrically charged regions of a cloud; cloud-to-cloud (CC) lighting, where it occurs between one functional thundercloud and another; and cloud-to-ground (CG) lightning, which primarily originates in the thundercloud and terminates on an Earth surface (but may also occur in the reverse direction). Thunder is also a direct result of electrostatic discharge. In addition to ground-based lightning detection, several instruments aboard satellites have been constructed to observe lightning distribution. Credit: NASA  Explore further Scientist seeks new insights to study lightning

<hr>

[Visit Link](http://phys.org/news/2015-07-lightning.html){:target="_blank" rel="noopener"}


