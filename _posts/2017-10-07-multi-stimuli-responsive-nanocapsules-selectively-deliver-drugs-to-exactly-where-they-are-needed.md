---
layout: post
title: "Multi stimuli-responsive nanocapsules selectively deliver drugs to exactly where they are needed"
date: 2017-10-07
categories:
author: "Agency For Science, Technology, Research, A Star"
tags: [Nanoparticle drug delivery,Nanocapsule,Technology,Emerging technologies,Physical chemistry,Health sciences,Nanotechnology,Artificial materials,Physical sciences,Materials science,Chemistry,Materials]
---


Credit: American Chemical Society  A nanoparticle-based drug delivery system that can sense and respond to different conditions in the body, as well as to an externally applied magnetic field, could enhance doctors' ability to target drugs to specific sites of disease. The team has already shown that the nanoparticles can selectively deliver the toxic antitumor drug doxorubicin to cancer cells. By selectively delivering chemotherapy drugs to a tumor, the drug's harmful effect on healthy cells can be minimized. When the researchers loaded their PLA-PDMAEMA coated nanoparticles with the anticancer drug doxorubicin, they showed that the drug was released significantly faster under acidic conditions. Further exploration of using these nanoparticles for combined drug delivery and bioimaging are also in progress, Li says.

<hr>

[Visit Link](http://phys.org/news/2016-09-multi-stimuli-responsive-nanocapsules-drugs.html){:target="_blank" rel="noopener"}


