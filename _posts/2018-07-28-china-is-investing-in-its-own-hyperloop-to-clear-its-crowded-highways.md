---
layout: post
title: "China Is Investing In Its Own Hyperloop To Clear Its Crowded Highways"
date: 2018-07-28
categories:
author: ""
tags: [Hyperloop,Maglev,Service industries,Sustainable transport,Transportation engineering,Land transport,Technology,Transport]
---


The race to open China's first hyperloop track is on. The latest move to address this urban traffic nightmare: Chinese state-backed companies are making heavy investments in U.S. hyperloop startups Arrivo and Hyperloop Transportation Technologies, lining up $1 billion and $300 million in credit respectively. Transport company Arrivo is focusing on relieving highway traffic by creating a separate track that allows cars to zip along at 200 miles per hour (320 km/h) on magnetically levitated sleds inside vacuum-sealed tubes (it's not yet clear if this will be above ground or underground). Building a hyperloop is expensive. We don't know yet whether China's hyperloop investments will pay off and significantly reduce traffic in China's urban centers.

<hr>

[Visit Link](https://futurism.com/china-hyperloop-crowded-highways/){:target="_blank" rel="noopener"}


