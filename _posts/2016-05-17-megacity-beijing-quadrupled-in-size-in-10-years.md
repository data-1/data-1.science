---
layout: post
title: "Megacity: Beijing Quadrupled in Size in 10 Years"
date: 2016-05-17
categories:
author: Elizabeth Howell
tags: [QuikSCAT,City,Wind,NASA,Satellite,Natural environment,Pollution,Infrastructure,Earth sciences,Nature]
---


Data from NASA’s QuikScat satellite show the changing extent of Beijing between 2000 and 2009 through changes to its infrastructure. Then, they estimated how these urban developments impacted winds and pollution in the city. (Image credit: NASA/JPL-Caltech)  Study co-leader Son Nghiem, a researcher at NASA's Jet Propulsion Laboratory in Pasadena, California, developed a technique to quantify urban growth. Artificial, or human-built, structures tend to produce more backscatter than vegetation or soil. Other satellites, including the Landsat satellites and the Suomi National Polar-orbiting Partnership satellite, have tracked urbanization from space, but these studies were likely not as precise because they relied on visible markers (such as city lights or swaths of land cleared of vegetation) to map the extent of urban growth.

<hr>

[Visit Link](http://www.livescience.com/51799-beijing-quadrupled-in-decade.html){:target="_blank" rel="noopener"}


