---
layout: post
title: "Saturn's radiation belts: A stranger to the solar wind"
date: 2017-11-01
categories:
author: "Max Planck Society"
tags: [Van Allen radiation belt,Saturn,Solar wind,Sun,Radiation,CassiniHuygens,Magnetosphere,Planet,Cosmic ray,Astronomy,Planets,Space science,Sky,Physical sciences,Outer space,Solar System,Astronomical objects,Planetary science,Bodies of the Solar System,Nature,Planets of the Solar System,Astronomical objects known since antiquity,Physics]
---


When Galactic Cosmic Rays reach the planet's atmosphere, it sets in motion a chain of reactions, at the end of which high-energy electrons and protons are created. In the first years of the Cassini mission, we observed that the solar wind could cause dramatic changes in Saturn's magnetosphere, says Roussos. Nevertheless, at first everything indicated that the solar wind still helps to shape the radiation belts – if only indirectly: the first years of the Cassini mission coincided with a decline in the Sun's activity; the intensity of the radiation belts increased as expected. We observe that the intensity drop in the proton radiation belts of Saturn coincides exactly with strong changes in the EUV radiation from the Sun, Roussos describes the new results. It is therefore possible that while the solar wind has no impact on the radiation belts, the Sun still may.

<hr>

[Visit Link](https://phys.org/news/2017-10-saturn-belts-stranger-solar.html){:target="_blank" rel="noopener"}


