---
layout: post
title: "Birds measure magnetic fields using long-lived quantum coherence – Physics World"
date: 2016-04-11
categories:
author:  
tags: [Radical (chemistry),Spin (physics),Spin quantum number,Magnetoreception,Electron,Magnetism,Protein,Cryptochrome,Physics,Physical sciences,Applied and interdisciplinary physics,Science,Quantum mechanics]
---


(CC BY-SA Peripitus)  Long-lived spin coherence in proteins found in the eyes of migratory birds could explain how the creatures are able to navigate along the Earth’s magnetic field with extraordinary precision. The two electron spins form a coherent quantum state that is affected by weak external magnetic fields, such as the geomagnetic field. Most previous research has modelled “simple radicals containing a single nuclear spin”, but “the radicals in cryptochromes have many nuclear spins – 10 or 15 in each of the two radicals,” Hore explains. The team found that when the spin coherence times in its model were set to be longer than a few microseconds, there was a marked “spike” in the yield of the signalling state produced by spin-selective reactions of the radical pairs. The long-lived spin coherences provide enough time “for the interconversion of the singlet and triplet states of the radical pairs, and therefore the yield of the signalling state, to respond sensitively to the direction of the geomagnetic field”, Hore explains.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2016/apr/07/birds-measure-magnetic-fields-using-long-lived-quantum-coherence){:target="_blank" rel="noopener"}


