---
layout: post
title: "NASA mission uncovers a dance of electrons in space"
date: 2017-10-08
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Magnetic reconnection,Magnetosphere,Goddard Space Flight Center,Electron,Magnetism,Spaceflight,Solar System,Physical phenomena,Science,Electromagnetism,Space science,Nature,Physical sciences,Physics,Outer space,Astronomy]
---


Scientists with NASA's Magnetospheric Multiscale, or MMS, mission study the electrons' dynamics to better understand their behavior. As MMS flew around Earth, it passed through an area of a moderate strength magnetic field where electric currents run in the same direction as the magnetic field. As the incoming particles encountered the region, they started gyrating in spirals along the guide field, like they do in a strong magnetic field, but in larger spirals. The magnetic field environment where the electrons' motions were observed was uniquely created by magnetic reconnection, which caused the current sheet to be tightly confined by bunched-up magnetic fields. Understanding the speed of reconnection is essential for predicting the intensity of the explosive energy release.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-05/nsfc-nmu051817.php){:target="_blank" rel="noopener"}


