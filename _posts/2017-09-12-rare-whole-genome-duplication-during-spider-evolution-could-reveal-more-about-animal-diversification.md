---
layout: post
title: "Rare whole genome duplication during spider evolution could reveal more about animal diversification"
date: 2017-09-12
categories:
author: "Baylor College Of Medicine"
tags: [Evolution,Gene duplication,Gene,Spider,Genome,Whole genome sequencing,Paleopolyploidy,Species,Human genome,Biology,Genetics,Life sciences,Biological evolution]
---


Analysis of these genomes revealed that spiders and scorpions evolved from a shared ancestor more than 400 million years ago, which made new copies of all of the genes in its genome, a process called whole genome duplication. Dr. Stephen Richards, associate professor in the Human Genome Sequencing Center, who led the genome sequencing at Baylor, said, It is tremendously exciting to see rapid progress in our molecular understanding of a species that we coexist with on planet earth. The new finding of a whole genome duplication in spiders and scorpions therefore provides a valuable comparison to the events in vertebrates and could help reveal genes and processes that have been important to our own evolution. Comparing the whole genome duplication in spiders and scorpions with the independent events in vertebrates reveals a striking similarity. The study also found that the copies of spider Hox genes show differences in when and where they are expressed, suggesting they have evolved new functions.

<hr>

[Visit Link](https://phys.org/news/2017-07-rare-genome-duplication-spider-evolution.html){:target="_blank" rel="noopener"}


