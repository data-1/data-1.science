---
layout: post
title: "Scientists identify undiscovered role of enzyme in regulating blood pressure"
date: 2017-03-17
categories:
author: "Northwell Health"
tags: []
---


In a study published today in Nature Biotechnology, scientists from The Feinstein Institute for Medical Research and Karolinska Institutet discovered that T-cells capable of producing the neurotransmitter acetylcholine can regulate blood pressure. Having now identified ChAT cells' new role and previously knowing that they respond to vagus nerve stimulation, Feinstein Institute researchers will explore using bioelectronic medicine to treat blood pressure and hypertension - a condition that affects more than 70 million Americans. Bioelectronic medicine is a new field of research that uses technology to treat disease and injury. Previous studies conducted at the Feinstein Institute found that the immune function could be controlled by neural mechanisms through the spleen, and with this study we were looking to identify triggers that could reach deeper into the smaller arteries to aid with conditions such as high blood pressure, said Kevin J. Tracey, MD, president and chief executive officer of the Feinstein Institute and one of lead authors of the Nature Biotechnology paper. The Feinstein Institute and Karolinska Institutet collaborate to help propel the field of bioelectronic medicine.

<hr>

[Visit Link](http://phys.org/news/2016-09-scientists-undiscovered-role-enzyme-blood.html){:target="_blank" rel="noopener"}


