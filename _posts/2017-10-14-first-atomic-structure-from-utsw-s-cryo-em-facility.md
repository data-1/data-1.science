---
layout: post
title: "First atomic structure from UTSW's cryo-EM facility"
date: 2017-10-14
categories:
author: "UT Southwestern Medical Center"
tags: [University of Texas Southwestern Medical Center,Cryogenic electron microscopy,Ion channel,Protein,Medicine,Cell biology,Biochemistry,Chemistry,Biotechnology]
---


The work marks the first such structure determined using the university’s $17 million cryo-electron microscopy (cryo-EM) facility that opened last year. Their study, published online by Nature, comes just a week after three developers of cryo-EM technology were recognized with the 2017 Nobel Prize in Chemistry for their work on the instruments and methodologies that sparked the “resolution revolution.” Cryo-EM enables atomic-level views of proteins that resist the crystallization necessary for traditional X-ray crystallography. “Functioning ion channels are needed for the proper movement of electrically charged particles – ions – in and out of cells and organelles to run cellular processes,” said Dr. Youxing Jiang, Professor of Physiology and Biophysics, an Investigator in the Howard Hughes Medical Institute (HHMI), and co-corresponding author of the study. People have suggested that detergent might change the protein structure from its native state,” said Dr. Xiaochen Bai, an Assistant Professor of Biophysics and Cell Biology and the study’s second corresponding author. About UT Southwestern Medical Center  UT Southwestern, one of the premier academic medical centers in the nation, integrates pioneering biomedical research with exceptional clinical care and education.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/usmc-fas101217.php){:target="_blank" rel="noopener"}


