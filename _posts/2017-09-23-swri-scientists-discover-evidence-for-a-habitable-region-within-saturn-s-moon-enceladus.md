---
layout: post
title: "SwRI scientists discover evidence for a habitable region within Saturn's moon Enceladus"
date: 2017-09-23
categories:
author: "Southwest Research Institute"
tags: [Enceladus,CassiniHuygens,Hydrothermal vent,Planetary science,Space science,Planets of the Solar System,Astronomy,Bodies of the Solar System,Science,Solar System,Outer space,Physical sciences]
---


Hydrogen is a source of chemical energy for microbes that live in the Earth's oceans near hydrothermal vents, said SwRI's Dr. Hunter Waite, principal investigator of Cassini's Ion Neutral Mass Spectrometer (INMS). Waite is the lead author of Cassini Finds Molecular Hydrogen in the Enceladus Plume: Evidence for Hydrothermal Processes, published in the April 14, 2017, issue of the journal Science. Previous flybys provided evidence for a global subsurface ocean residing above a rocky core. Molecular hydrogen in the plumes could serve as a marker for hydrothermal processes, which could provide the chemical energy necessary to support life. We developed new operations methods for INMS for Cassini's final flight through Enceladus' plume, said SwRI's Rebecca Perryman, the INMS operations technical lead.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/sri-ssd041317.php){:target="_blank" rel="noopener"}


