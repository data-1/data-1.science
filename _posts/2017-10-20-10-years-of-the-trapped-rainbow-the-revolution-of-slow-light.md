---
layout: post
title: "10 years of the trapped rainbow—the revolution of slow light"
date: 2017-10-20
categories:
author: Imperial College London
tags: [Laser,Light,Photon,Electrical engineering,Electromagnetism,Atomic molecular and optical physics,Electromagnetic radiation,Optics,Science,Physics,Applied and interdisciplinary physics,Electrodynamics]
---


Credit: Imperial College London  A decade on from suggesting light can be dramatically slowed - or even stopped - by new materials, Ortwin Hess reviews the progress and applications. We talked to Professor Hess about how slow light forms a 'trapped rainbow', and how the potential applications now extend to magnetic storage, lasers, biological imaging and even earthquake shields. The process of creating a trapped rainbow relies on metamaterials or nanoplasmonic structures endowed with special negative properties, surrounded by 'normal' materials. One way slow light is useful is in increasing interactions between light and matter. And one really innovative idea, actually being tested by a team including Imperial researchers, is slowing seismic waves.

<hr>

[Visit Link](https://phys.org/news/2017-10-years-rainbowthe-revolution.html){:target="_blank" rel="noopener"}


