---
layout: post
title: "Enterprise Guide to Winding Down an Open Source Project"
date: 2018-04-29
categories:
author: "Sam Dean"
tags: [Open source,Linux,Linux Foundation,Open-source-software movement,Software engineering,Computing,Technology,Software,Intellectual works,Software development,Open-source movement,Computer science]
---


“In this way, you can also set proper expectations for users, ensure that long-term project code dependencies are supported, and preserve your company’s reputation within the open source community as a responsible participant.”  The free guide includes sound advice on the following topics:  You’ll also find direct advice from open source experts in the guide. Give them time to transition, and work on ways to help with the transition. Typically, we’ll just move it to a different organization. Additionally, The Linux Foundation and The TODO Group (Talk Openly Develop Openly) have published an entire collection of enterprise guides to assist in developing open source programs and understanding how best to work with open source tools. The guides are available for free, and they cover everything from How to Create an Open Source Program to Starting an Open Source Project.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/free-guide-winding-open-source-project/){:target="_blank" rel="noopener"}


