---
layout: post
title: "Single-photon detector can count to 4"
date: 2018-07-02
categories:
author: "Duke University"
tags: [Superconducting nanowire single-photon detector,Applied and interdisciplinary physics,Physics,Science,Electrical engineering,Electromagnetism,Technology]
---


DURHAM, N.C. -- Engineers have shown that a widely used method of detecting single photons can also count the presence of at least four photons at a time. The study was a collaboration between Duke University, the Ohio State University and industry partner Quantum Opus, and appeared online on December 14 in the journal Optica. That loss, in turn, causes an electrical signal to mark the presence of the photon. For other labs to make use of the discovery, all they would need is a specific type of amplifier for boosting the SNSPD's tiny electrical signal. The results will allow researchers around the world working in quantum mechanics to immediately gain new abilities with their existing equipment.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/du-sdc121517.php){:target="_blank" rel="noopener"}


