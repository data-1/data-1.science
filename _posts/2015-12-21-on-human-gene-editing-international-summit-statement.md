---
layout: post
title: "On human gene editing: International summit statement"
date: 2015-12-21
categories:
author: National Academies of Sciences, Engineering, and Medicine
tags: [Genome editing,Genetics,Mutation,Gene,CRISPR gene editing,DNA sequencing,Germline,Biotechnology,Biology,Life sciences,Health sciences]
---


Intensive basic and preclinical research is clearly needed and should proceed, subject to appropriate legal and ethical rules and oversight, on (i) technologies for editing genetic sequences in human cells, (ii) the potential benefits and risks of proposed clinical uses, and (iii) understanding the biology of human embryos and germline cells. Many promising and valuable clinical applications of gene editing are directed at altering genetic sequences only in somatic cells - that is, cells whose genomes are not transmitted to the next generation. Clinical Use: Germline. It would be irresponsible to proceed with any clinical use of germline editing unless and until (i) the relevant safety and efficacy issues have been resolved, based on appropriate understanding and balancing of risks, potential benefits, and alternatives, and (ii) there is broad societal consensus about the appropriateness of the proposed application. We therefore call upon the national academies that co-hosted the summit - the U.S. National Academy of Sciences and U.S. National Academy of Medicine; the Royal Society; and the Chinese Academy of Sciences - to take the lead in creating an ongoing international forum to discuss potential clinical uses of gene editing; help inform decisions by national policymakers and others; formulate recommendations and guidelines; and promote coordination among nations.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/naos-ohg120315.php){:target="_blank" rel="noopener"}


