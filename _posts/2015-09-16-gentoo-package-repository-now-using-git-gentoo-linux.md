---
layout: post
title: "Gentoo Package Repository now using Git – Gentoo Linux"
date: 2015-09-16
categories:
author: "$author"   
tags: [Gentoo Linux,Portage (software),Git,Computer science,Free software,Technology,Software engineering,Software development,Computing,Software,Information technology management,System software,Open-source movement,Free content,Digital media,Intellectual works,Computer engineering]
---


Gentoo Package Repository now using Git  Aug 12, 2015  Good things come to those who wait: The main Gentoo package repository (also known as the Portage tree or by its historic name gentoo-x86) is now based on Git. Timeline  The Gentoo Git migration has arrived and is expected to be completed soon. As previously announced, the CVS freeze occurred on 8 August and Git commits for developers were opened soon after. History  Work on migrating the repository from CVS to Git began in 2006 with a proof-of-concept migration project during the Summer of Code. While the last issues are being worked on, join us in thanking everyone involved in the project.

<hr>

[Visit Link](https://www.gentoo.org/news/2015/08/12/git-migration.html){:target="_blank" rel="noopener"}


