---
layout: post
title: "Tech titans want to conquer death – but do you really want to live forever?"
date: 2016-04-30
categories:
author: Richard Gunderman, The Conversation
tags: []
---


Compared to every other living organism, an immortal one might not be alive in the same sense. But there are still deeper problems with the tech titans' quest for immortality. If we faced no deadlines at all, and the time available to complete every task were infinite, it is quite possible that we would get nothing done. The prospect of mortality can also bring out the best in us. Mortality gives us courage  Some of the greatest works of humankind, including the Bible and Homer's Iliad, brim with reminders that our days are numbered.

<hr>

[Visit Link](http://phys.org/news350207498.html){:target="_blank" rel="noopener"}


