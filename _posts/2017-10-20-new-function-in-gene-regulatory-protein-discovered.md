---
layout: post
title: "New function in gene-regulatory protein discovered"
date: 2017-10-20
categories:
author: Umea University
tags: [Gene,RNA,Protein,DNA,Molecular biology,CREB-binding protein,Cell biology,Nucleic acids,Molecular biophysics,Structural biology,Branches of genetics,Biomolecules,Macromolecules,Chemistry,Molecular genetics,Genetics,Life sciences,Biology,Biotechnology,Biochemistry]
---


DNA contains instructions for how the cell should assemble functional proteins. This RNA molecule is a copy of the gene that contains the instructions for a specific protein. RNA polymerase is the enzyme that produces the RNA copy, and the amount of RNA produced from each gene depends on two main steps: the recruitment of RNA polymerase to the start of a gene; and the release of the enzyme from the start position so that it can begin the copying process. When the function of CBP is experimentally disrupted, genes have a hard time recruiting RNA polymerase, which leads to a lower number of proteins to be produced. The researchers discovered that CBP also affects the efficiency of the release of RNA polymerase from the gene start so that it can initiate the copying process.

<hr>

[Visit Link](https://phys.org/news/2017-10-function-gene-regulatory-protein.html){:target="_blank" rel="noopener"}


