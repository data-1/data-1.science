---
layout: post
title: "Cold snap: Climate cooling and sea-level changes caused crocodilian retreat: Fluctuating sea levels and global cooling caused a significant decline in the number of crocodilian species over millions o"
date: 2015-09-28
categories:
author: Imperial College London 
tags: [Crocodilia,CretaceousPaleogene extinction event,Extinction event,Extinction,Biodiversity,Sea level rise,Dinosaur,Ocean,Earth sciences,Natural environment,Physical geography,Nature,Earth phenomena,Ecology,Global natural environment]
---


Fluctuating sea levels and global cooling caused a significant decline in the number of crocodylian species over millions of years, according to new research. For example, in Africa around ten million years ago, the Sahara desert was forming, replacing the vast lush wetlands in which crocodylians thrived. Interestingly, the Cretaceous-Paleogene mass extinction event, which wiped out many other creatures on Earth nearly 66 million years ago including nearly all of the dinosaurs, had positive outcomes for the crocodylians and their extinct relatives. In the future, the team suggest that a warming world caused by global climate change may favour crocodylian diversification again, but human activity will continue to have a major impact on their habitats. This may help us to determine how they will cope with future changes.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150924082452.htm){:target="_blank" rel="noopener"}


