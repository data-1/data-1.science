---
layout: post
title: "Release of the first Roadmap for European Astrobiology"
date: 2016-04-05
categories:
author: European Science Foundation
tags: [Astrobiology,Life,European Science Foundation,Research,Science,Branches of science]
---


While it addresses life beyond the Earth, topics it puts forward are also very relevant to life in Earth extreme environments and to the understanding of our evolving ecosystem. The five AstRoMap Research Topics are:  Research Topic 1 Origin and Evolution of Planetary Systems Research Topic 2 Origins of Organic Compounds in Space Research Topic 3 Rock-Water-Carbon Interactions, Organic Synthesis on Earth, and Steps to Life Research Topic 4 Life and Habitability Research Topic 5 Biosignatures as Facilitating Life Detection  Besides putting forward scientific priority topics the AstRoMap roadmap also strongly recommends that a European Astrobiology Platform (or Institute) should be set up to streamline scientific investigations, maximise interdisciplinary collaboration and optimise the use and development of infrastructures. ###  The AstRoMap European Astrobiology Roadmap is published in the Astrobiology journal and available open access at http://online.liebertpub.com/doi/abs/10.1089/ast.2015.1441  About Astromap  AstRoMap was a Coordination Action supported by the European Commission under Grant Agreement 313102 and was coordinated by the Centro de Astrobiologia (Spain). AstRoMap tackled the issues of: i) improving the knowledge of the scientific field of astrobiology in Europe and beyond, ii) providing new networking tools and opportunity for the scientific community and iii) defining future scientific priorities and consolidating them into a research roadmap. (DLR) - Germany, the Belgian User Support and Operations Centre (B-USOC) - Belgium, the Istituto Nazionale di Astrofisica (INAF) - Italy and the European Astrobiology Network Association (EANA)  About the European Science Foundation  The European Science Foundation (ESF) was established in 1974 to provide a common platform for its Member Organisations - the main research funding and research performing organisations in Europe - to advance European research collaboration and explore new directions for research.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/esf-rot032916.php){:target="_blank" rel="noopener"}


