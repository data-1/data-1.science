---
layout: post
title: "Algorithm could construct first images of black holes"
date: 2016-06-12
categories:
author: "Massachusetts Institute of Technology"
tags: [Radio telescope,Event Horizon Telescope,Telescope,Interferometry,Radio,Astronomy,Science,Technology]
---


Researchers from MIT's Computer Science and Artificial Intelligence Laboratory and Harvard University have developed a new algorithm that could help astronomers produce the first image of a black hole. But because of their long wavelengths, radio waves also require large antenna dishes. Bouman will present her new algorithm -- which she calls CHIRP, for Continuous High-resolution Image Reconstruction using Patch priors -- at the Computer Vision and Pattern Recognition conference in June. She's joined on the conference paper by her advisor, professor of electrical engineering and computer science Bill Freeman, and by colleagues at MIT's Haystack Observatory and the Harvard-Smithsonian Center for Astrophysics, including Sheperd Doeleman, director of the Event Horizon Telescope project. Preserving continuity  Even with atmospheric noise filtered out, the measurements from just a handful of telescopes scattered around the globe are pretty sparse; any number of possible images could fit the data equally well.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/miot-acc060616.php){:target="_blank" rel="noopener"}


