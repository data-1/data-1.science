---
layout: post
title: "Study of human body fluid shifts aboard space station advances journey to Mars"
date: 2016-05-15
categories:
author:  
tags: [International Space Station,NASA,Roscosmos,Intracranial pressure]
---


Using an ultrasound, NASA's Human Research Program is currently testing noninvasive techniques to evaluate and measure intracranial pressure as part of the One-Year Mission research. Credit: NASA  NASA and the Russian Space Agency (Roscosmos) are studying the effects of how fluids shift to the upper body in space and how this adaptation to space flight affects changes in vision. The Cerebral and Cochlear Fluid Pressure device is being used in place of the invasive methods to measure changes in intracranial pressure. Credit: NASA  This is not only the largest investigation on the space station, but one of the most challenging to set up. Several months without gravity is a challenge to the human body, which is why the Fluid Shifts study is so important.

<hr>

[Visit Link](http://phys.org/news/2015-07-human-body-fluid-shifts-aboard.html){:target="_blank" rel="noopener"}


