---
layout: post
title: "Diamond defect images magnetic domain walls – Physics World"
date: 2014-06-24
categories:
author: Isabelle Dumé
tags: [Microscope,Domain wall (magnetism),Nanowire,Magnetism,Racetrack memory,Materials science,Materials,Technology,Chemistry,Electrical engineering,Applied and interdisciplinary physics,Physical sciences,Optics,Electromagnetism,Chemical product engineering]
---


Using a point-like defect in diamond attached to a scanning atomic force microscope (AFM), they were able to map out the energy “landscape” for a domain wall and could even make the walls themselves move using the laser light from the microscope. Magnetic domain walls could be used to make new types of spintronics devices, such as racetrack memories in which data are stored as a sequence of magnetic domains along a nanowire. They managed to control these jumps using the heat generated by the laser light in the microscope, which in turn allowed them to “drag” the domain wall along the wire and position it at any point on the structure. “Our process allows us to calculate the energy landscape ‘seen’ by the domain wall along the wire,” says Jacques. As mentioned, a crucial step towards making these memories will involve characterizing the magnetic terrain for these domain walls, because how they move across the track will determine how well they actually perform as devices.” The technique is not just limited to studying domain walls either, he added.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/mD-_gEy5dZw/diamond-defect-images-magnetic-domain-walls){:target="_blank" rel="noopener"}


