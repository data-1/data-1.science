---
layout: post
title: "22 years of SOHO"
date: 2018-07-01
categories:
author: ""
tags: [Sun,Solar cycle,Stellar corona,Solar and Heliospheric Observatory,Sunspot,Stellar astronomy,Electromagnetism,Space science,Astronomy,Bodies of the Solar System,Solar System,Outer space,Astronomical objects known since antiquity,Nature,Astronomical objects,Space plasmas,Physical sciences,Physical phenomena,Ancient astronomy,Astrophysics]
---


Sunspot cycles are known to occur over about 11 years, but the full cycle is double this length owing to the behaviour of the magnetic fields. At the end of a 22-year cycle, the orientation of the magnetic field is the same as it was at the start. When the Sun is at its most active, strong magnetic fields show up as bright spots in the ultraviolet images of the corona. Activity also becomes obvious on the photosphere, which is the surface we see in visible light. When the Sun is active, sunspots appear on the surface.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2018/02/22_years_of_SOHO){:target="_blank" rel="noopener"}


