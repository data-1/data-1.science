---
layout: post
title: "When highest perceptual ability occurs in a day?"
date: 2015-10-28
categories:
author: Neural Regeneration Research 
tags: [Neuropsychology,Neuropsychological assessment,Cognition,Science,Cognitive science,Neuroscience]
---


Many previous chronobiological studies have reported on detection of circadian fluctuation in performing simple motor tasks, fine skilled movement, and anaerobic exercise. Therefore, Yong Hyun Kwon and co-workers from Yeungnam University College of Science and Technology in Republic of Korea observed and compared the circadian fluctuations in tactile sense, joint reposition sense and two-point discrimination in 21healthy adult subjects at approximately 9:00, 13:00 and 18:00 in a day. These findings have been published in the Neural Regeneration Research (Vol. ###  Article: Circadian fluctuations in three types of sensory modules in healthy subjects by Yong Hyun Kwon, Ki Seok Nam (Department of Physical Therapy, Yeungnam University College of Science and Technology, Hyunchung-ro, Nam-gu, Daegu, Republic of Korea)  Kwon YH, Nam KD. Circadian fluctuations in three types of sensory modules in healthy subjects.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/nrr-whp080414.php){:target="_blank" rel="noopener"}


