---
layout: post
title: "Research Confirms That 20% of the Neanderthal Genome Can Be Found In Modern Humans"
date: 2018-04-19
categories:
author: ""
tags: [Denisovan,Neanderthal,Neanderthal genetics,Early modern human,Human,Genetics,Biology]
---


Though it was disputed for many years, there is conclusive evidence that Neanderthals bred with modern humans (Homo sapiens). The first complete mapping of a Neanderthal genome took place about five years ago - supporting the human-Neanderthal hook-up and also showing that Neanderthal DNA in humans is a thing. Scientist Joshua Akey believes that the genes which have lived on are mostly the advantageous ones – he says genes which caused more harm than good have hardly been passed on to modern humans. ( CC BY NC ND 2.0 )  Akey and his colleague Benjamin Vernot, both of the University of Washington in Seattle, also searched for Neanderthal DNA in the genomes of 665 living people. That isn’t the only study which has suggested there are unknown species waiting to be discovered in the hominid family tree.

<hr>

[Visit Link](http://www.ancient-origins.net/human-origins-science/research-confirms-neanderthal-dna-makes-20-modern-human-genome-009817){:target="_blank" rel="noopener"}


