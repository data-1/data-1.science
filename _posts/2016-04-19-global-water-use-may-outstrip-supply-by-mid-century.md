---
layout: post
title: "Global water use may outstrip supply by mid-century"
date: 2016-04-19
categories:
author: Duke University
tags: [Economic growth,Water,Innovation,Heinz von Foerster,Population growth,Economy,Branches of science,Science]
---


Using a delayed-feedback mathematical model that analyzes historic data to help project future trends, the researchers identified a regularly recurring pattern of global water use in recent centuries. Periods of increased demand for water—often coinciding with population growth or other major demographic and social changes—were followed by periods of rapid innovation of new water technologies that helped end or ease any shortages. Researchers in other fields have previously used this model to predict earthquakes and other complex processes, including events like the boom and bust of the stock market during financial crises, but this is the first time it's been applied to water use, said Anthony Parolari, postdoctoral research associate in civil and environmental engineering at Duke, who led the new study. The model suggests we may reach a tipping point where efficiency measures are no longer sufficient and water scarcity either impacts population growth or pushes us to find new water supplies, Parolari said. Parolari was inspired to conduct his study by the work of Austrian physicist and philosopher Heinz von Foerster, who in 1960 collaborated with students to publish a tongue-in-cheek study in the journal Science predicting that through feedbacks between human demographics and technological development, population growth would overcome any limitation imposed on it by finite resources and become infinite by November 13, 2026 - the 115th anniversary of von Foerster's birthday.

<hr>

[Visit Link](http://phys.org/news346349628.html){:target="_blank" rel="noopener"}


