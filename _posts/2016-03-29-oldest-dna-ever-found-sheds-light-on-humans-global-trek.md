---
layout: post
title: "Oldest DNA ever found sheds light on humans' global trek"
date: 2016-03-29
categories:
author: Richard Ingham
tags: [Max Planck Institute for Evolutionary Anthropology,Neanderthal,Ust-Ishim man,Human populations,Human evolution,Biological anthropology,Evolution of primates,Pliocene]
---


The Ust'-Ishim femur. Previous research has found that Neanderthals and H. sapiens interbred, leaving a tiny Neanderthal imprint of just about two percent in humans today, except for Africans. Neanderthal interbreeding  The bone found at the Irtyush River, near the settlement of Ust'-Ishim, carries slightly more Neanderthal DNA than non-Africans today, the team found. This provides a rough date for estimating when H. sapiens headed into South Asia, Chris Stringer, a professor at Britain's Natural History Museum, said in a comment on the study. While it is still possible that modern humans did traverse southern Asia before 60,000 years ago, those groups could not have made a significant contribution to the surviving modern populations outside of Africa, which contain evidence of interbreeding with Neanderthals.

<hr>

[Visit Link](http://phys.org/news333205296.html){:target="_blank" rel="noopener"}


