---
layout: post
title: "Voyager 1 Just Fired Its Thrusters for the First Time in 37 Years"
date: 2018-06-10
categories:
author: ""
tags: [Voyager 1,Spacecraft propulsion,Voyager program,Spacecraft,Outer space,Space science,Spaceflight,Astronomy,Astronautics,Flight,Solar System,Space vehicles,Planetary science,Space probes,Space program of the United States,Science,Sky,Astronomical objects,Aerospace,Discovery and exploration of the Solar System,Space exploration,Spaceflight technology,Bodies of the Solar System]
---


The Voyager team had noticed diminishing returns on these thrusters since 2014, with the thrusters needing to fire up more often to give off the same amount of energy. They also hadn't been switched on since the craft's encounter with Saturn in 1980 and had never been used for the purpose of orienting the craft for communication. Voyager's Voyage  The TCM thrusters will officially take over for the attitude control thrusters in January, and the Voyager team predicts that the backup thrusters will add another two to three years to Voyager 1's mission. The gravity of one of Saturn's moons, Titan, disrupted the trajectory of Voyager 1, so instead of flying by the rest of the solar system, the craft headed toward interstellar space. Thirty-four years after launching, Voyager 1 became the first spacecraft to travel beyond our solar system.

<hr>

[Visit Link](https://futurism.com/voyager-1-fired-thrusters-first-time-37-years/){:target="_blank" rel="noopener"}


