---
layout: post
title: "Event Horizon Telescope reveals magnetic fields at Milky Way's central black hole"
date: 2015-12-22
categories:
author: Harvard-Smithsonian Center for Astrophysics
tags: [Event Horizon Telescope,Sagittarius A,Black hole,Astrophysics,Physics,Astronomical objects,Physical sciences,Space science,Astronomy,Science,Physical phenomena]
---


For the first time, astronomers have detected magnetic fields just outside the event horizon of the black hole at the center of our Milky Way galaxy. This feat was achieved using the Event Horizon Telescope (EHT) -- a global network of radio telescopes that link together to function as one giant telescope the size of Earth. As a result, this light directly traces the structure of the magnetic field. Those magnetic fields are dancing all over the place. As the EHT adds more radio dishes around the world and gathers more data, it will achieve greater resolution with the goal of directly imaging a black hole's event horizon for the first time.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/12/151203150233.htm){:target="_blank" rel="noopener"}


