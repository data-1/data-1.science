---
layout: post
title: "Scientists watch living taste cells in action"
date: 2016-04-23
categories:
author: Australian National University
tags: [Taste,Sense,Tongue,Anatomy]
---


Scientists have for the first time captured live images of the process of taste sensation on the tongue. We've watched live taste cells capture and process molecules with different tastes, said biomedical engineer Dr Steve Lee, from The Australian National University (ANU). With this new imaging tool we have shown that each taste bud contains taste cells for different tastes, said Professor Yun. The team imaged the tongue by shining a bright infrared laser on to the mouse's tongue, which caused different parts of the tongue and the flavour molecules to fluoresce. They were able to pick out the individual taste cells within each taste bud, as well as blood vessels up to 240 microns below the surface of the tongue.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/anu-swl042215.php){:target="_blank" rel="noopener"}


