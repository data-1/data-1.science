---
layout: post
title: "ESA image: Saturn's moon Rhea, Epimetheus transiting"
date: 2015-10-29
categories:
author: European Space Agency
tags: [Natural satellite,Rhea (moon),Saturn,Epimetheus (moon),CassiniHuygens,Planets,Space science,Outer space,Solar System,Bodies of the Solar System,Astronomical objects,Planetary science,Moons,Astronomy,Planets of the Solar System,Gas giants,Astronomical objects known since antiquity,Outer planets]
---


The other 55 small satellites whizzing around Saturn make up the tiny remainder along with the gas giant's famous rings. One of the subjects of this Cassini image, Rhea, belongs to that group of six. Set against a backdrop showing Saturn and its intricate system of icy rings, Rhea dominates the scene and dwarfs its tiny companion, one of the 55 small satellites known as Epimetheus. Although they appear to be close to one another, this is a trick of perspective – this view was obtained when Cassini was some 1.2 million km from Rhea, and 1.6 million km from Epimetheus, meaning the moons themselves had a hefty separation of 400 000 km. This image was taken by Cassini's narrow-angle camera on 24 March 2010, and processed by amateur astronomer Gordan Ugarković.

<hr>

[Visit Link](http://phys.org/news327039531.html){:target="_blank" rel="noopener"}


