---
layout: post
title: "ATLAS Experiment studies photon-tagged jet quenching in the quark-gluon plasma"
date: 2017-10-25
categories:
author: Atlas Experiment
tags: [Quarkgluon plasma,Jet (particle physics),Gluon,Jet quenching,Quark,ATLAS experiment,Nuclear physics,Theoretical physics,Quantum mechanics,Applied and interdisciplinary physics,Quantum field theory,Quantum chromodynamics,Subatomic particles,Standard Model,Particle physics,Physical sciences,Physics]
---


Figure 1: Event display of a lead-lead collision with a large transverse momentum photon. Events in which a jet is produced opposite a high momentum photon are particularly useful since the photon does not appreciably interact with the quarks and gluons composing the medium. These so called fragmentation functions have been measured for the first time by ATLAS for jets opposite to a photon in proton-proton and lead-lead collisions. In peripheral lead–lead collisions (where the nuclei have modest overlap and create a moderate-sized QGP region), the fragmentation function for photon-balancing jets is found to be significantly modified compared to that in proton–proton collisions, reflecting the effects of the distorting medium. Credit: ATLAS Collaboration/CERN  Intriguingly, studies of the fragmentation function for inclusive jets observe a different behaviour – which is that past a certain QGP size, the emerging jets do not continue to be modified.

<hr>

[Visit Link](https://phys.org/news/2017-10-atlas-photon-tagged-jet-quenching-quark-gluon.html){:target="_blank" rel="noopener"}


