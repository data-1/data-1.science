---
layout: post
title: "Supercomputer-powered supernova simulations shed light on distant explosions"
date: 2014-08-16
categories:
author: Aaron Dubrow, National Science Foundation
tags: [Star,Magnetohydrodynamics,Neutron star,Supernova,Physical sciences,Astronomy]
---


Volume rendering of the entropy of a differentially rotating and highly magnetized progenitor to a supernova in a full 3-D. Red colors indicate high entropy (hot) material while blue represents low entropy (cold) material. The death of these collapsing stars leads to energetic, jet-driven supernova explosions. A typical simulation by Ott and Moesta uses six to eight million hours of computer processing time to recreate the death of the star and approximately 200 milliseconds of the star's evolution after the collapse of its core. It's also possible that the explosion never gains traction and the stellar envelope falls onto the neutron star, which will then collapse to a black hole. Explore further A 3-D model of stellar core collapse

<hr>

[Visit Link](http://phys.org/news323504713.html){:target="_blank" rel="noopener"}


