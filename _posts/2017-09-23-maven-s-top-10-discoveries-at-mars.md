---
layout: post
title: "MAVEN's top 10 discoveries at Mars"
date: 2017-09-23
categories:
author: "NASA/Goddard Space Flight Center"
tags: [MAVEN,Solar wind,Aurora,Sun,Goddard Space Flight Center,Mars,Atmosphere of Mars,Atmosphere,Atmosphere of Earth,Spaceflight,Sky,Astronomical objects known since antiquity,Planets of the Solar System,Astronomical objects,Physical sciences,Solar System,Planetary science,Planets,Nature,Astronomy,Outer space,Space science,Bodies of the Solar System]
---


MAVEN has made tremendous discoveries about the Mars upper atmosphere and how it interacts with the sun and the solar wind, said Bruce Jakosky, MAVEN principal investigator from the University of Colorado, Boulder. When particles from these storms hit the Martian atmosphere, they also can increase the rate of loss of gas to space, by a factor of ten or more. MAVEN has measured the rate at which the sun and the solar wind are stripping gas from the top of the atmosphere to space today, along with the details of the removal processes. MAVEN began its primary science mission on November 2014, and is the first spacecraft dedicated to understanding Mars' upper atmosphere. The University of California at Berkeley's Space Sciences Laboratory also provided four science instruments for the mission.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-06/nsfc-mt1061617.php){:target="_blank" rel="noopener"}


