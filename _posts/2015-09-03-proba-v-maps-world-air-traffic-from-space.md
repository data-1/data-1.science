---
layout: post
title: "Proba-V maps world air traffic from space"
date: 2015-09-03
categories:
author: "$author"   
tags: [Automatic Dependent SurveillanceBroadcast,Air traffic control,SES SA,Outer space,Technology,Spaceflight,Aerospace,Space science]
---


Launched two years ago today, Proba-V has picked up upwards of 25 million positions from more than 15 000 separate aircraft. “We’ve shown that detection of aircraft can work from space with no showstoppers, despite the fact that these signals were never designed to be picked up from so far away. All aircraft entering European airspace are envisaged to carry ADS-B in the coming years. “In the event, we have also had very good detections in the much more densely trafficked airspace of the US, Western Europe and Southeast Asia.” In those parts of the world with radar coverage, air traffic controllers can shepherd aircraft very precisely, with separation distances down to 5.5–9 km. “Right now, some makes of aircraft are more easily detected than others, which typically comes down to the age and make of their ADS-B systems.”  Contrails from air traffic An operational ADS-B detection system is being hosted on the IridumNEXT constellation, while SES is working with ESA to determine the market for a European version.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/Proba_Missions/Proba-V_maps_world_air_traffic_from_space){:target="_blank" rel="noopener"}


