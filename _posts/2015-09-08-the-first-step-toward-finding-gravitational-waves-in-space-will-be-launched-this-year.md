---
layout: post
title: "The first step toward finding gravitational waves in space will be launched this year"
date: 2015-09-08
categories:
author: Arielle Duhaime-Ross, Sep
tags: [Laser Interferometer Space Antenna,LISA Pathfinder,Flight,Physical cosmology,Gravity,Astrophysics,Astronomy,Space science,Science,Outer space,Spaceflight,Physics,Physical sciences,Astronautics]
---


The LISA Pathfinder, the European spacecraft that houses the technology needed to detect the waves, will launch into space later this year — an event that will give researchers a chance to test the measurement system in space for the first time. Confirming that they exist could let scientists listen to the universe  The extreme precision of measurements and control required in this domain pose a great technical challenge, César García Marirrodriga, ESA’s project manager, said in a press release. Despite the violent events that trigger them, gravitational waves only cause very small perturbations in the fabric of spacetime. That's why the instruments' precision is so important. Eight weeks later, the spacecraft will begin a six-month technology demonstration.

<hr>

[Visit Link](http://www.theverge.com/2015/9/7/9271897/lisa-pathfinder-esa-gravitional-waves-physics-einstein){:target="_blank" rel="noopener"}


