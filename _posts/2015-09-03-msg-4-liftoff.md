---
layout: post
title: "MSG-4 liftoff"
date: 2015-09-03
categories:
author: "$author"   
tags: [Meteosat,Space industry,Flight,Space vehicles,Arianespace,Space policy,Space science,Satellite meteorology,Airbus joint ventures,Commercial launch service providers,Aerospace organizations,European Organisation for the Exploitation of Meteorological Satellites,Commercial spaceflight,Space launch vehicles of Europe,Spacecraft,Earth observation satellites,Aerospace,Satellites orbiting Earth,Space access,Space exploration,Aerospace companies,Bodies of the Solar System,Space technology,Ariane (rocket family),Remote sensing,Astronautics,Earth orbits,Rocket families,Transport authorities,Aerospace companies of France,Weather satellites,European space programmes,Space policy of the European Union,Outer space,Pan-European scientific organizations,Space traffic management,Spaceflight,European Space Agency,Space organizations,Space programs,Space agencies,Satellites]
---


The last weather satellite in Europe’s highly successful Meteosat Second Generation (MTG) series lifted off on an Ariane 5 launcher at 21:42 GMT (23:42 CEST) on 15 July from Europe’s Spaceport in Kourou, French Guiana. The MSG satellites provide full-disc images over Europe and Africa every 15 minutes and ‘rapid scan’ imagery over Europe every five minutes. They are operated by Eumetsat – the European Organisation for the Exploitation of Meteorological Satellites – and ESA is responsible for their design, development and in-orbit delivery.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2015/07/MSG-4_liftoff5){:target="_blank" rel="noopener"}


