---
layout: post
title: "'Molecular movie' captures chemical reaction on atomic scale"
date: 2018-07-12
categories:
author: "University of Nebraska-Lincoln"
tags: [Atom,Molecule,Chemical reaction,Energy,Chemical bond,Electron,Energy level,Photochemistry,Iodine,Photon,SLAC National Accelerator Laboratory,Light,Quantum mechanics,Applied and interdisciplinary physics,Physical chemistry,Atomic molecular and optical physics,Nature,Physics,Chemistry,Physical sciences]
---


Laser lights. Reaction. This time around, the researchers recorded the behavior of a more complex molecule - trifluoromethyl iodide - featuring three fluorine atoms, one carbon and one iodine. In doing so, they captured the molecule's iodine atom breaking free of its bond with a carbon atom: a photochemical reaction. If you want to build solar cells, you want a molecule that converts all the energy into some sort of chemical reaction that you can use, Centurion said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/uon-mc070518.php){:target="_blank" rel="noopener"}


