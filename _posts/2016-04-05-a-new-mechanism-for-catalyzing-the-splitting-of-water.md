---
layout: post
title: "A new mechanism for catalyzing the splitting of water"
date: 2016-04-05
categories:
author: Denis Paiste, Massachusetts Institute Of Technology
tags: [Catalysis,Electrolysis,Water splitting,Chemical reaction,Electrolysis of water,Electrochemistry,Water,Hydrogen,Physical chemistry,Chemistry,Physical sciences,Materials,Applied and interdisciplinary physics,Nature,Chemical substances]
---


Mobilizing oxygen atoms from the crystal surface of perovskite-oxide electrodes to participate in the formation of oxygen gas is key to speeding up water-splitting reactions, researchers at MIT, the Skoltech Institute of Technology, and the University of Texas at Austin show in a new paper published online in Nature Communications. By using ab initio density functional theory calculations, we illustrated an unprecedented new electrolysis pathway occurring on SrCoO 2.7 , and showed that it occurs as a result of the nature of the electronic structure in cobalt and oxygen, MIT mechanical engineering graduate student Xi (Jerry) Rong says. The generation of oxygen from water remains a significant bottleneck in the development of water electrolyzers and also in the development of fuel cell and metal-air battery technologies. In addition, this work demonstrated a fundamental correlation between electrolysis mechanism and catalyst stability, which may lead to discovery of new catalysts that might otherwise be missed. Lattice oxygen participation is a prerequisite for the high efficiency of the catalyst, so how to tune the compositions, how to tune the properties, of the catalysts to be beneficial for this mechanism is the key fundamental step for designing catalysts in the future, Rong says.

<hr>

[Visit Link](http://phys.org/news/2016-03-mechanism-catalyzing.html){:target="_blank" rel="noopener"}


