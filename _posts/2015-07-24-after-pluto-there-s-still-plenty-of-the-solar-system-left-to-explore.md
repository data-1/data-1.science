---
layout: post
title: "After Pluto there's still plenty of the solar system left to explore"
date: 2015-07-24
categories:
author: Chris Arridge, The Conversation
tags: [Solar System,Jupiter,Jupiter Icy Moons Explorer,New Horizons,Planet,Juno (spacecraft),Comet,Pluto,Neptune,Ganymede (moon),Moon,Europa (moon),Mars,Planemos,Local Interstellar Cloud,Spaceflight,Astronomical objects,Astronautics,Science,Physical sciences,Planets of the Solar System,Planets,Bodies of the Solar System,Outer space,Planetary science,Space science,Astronomy]
---


We've watched as spacecraft made visits to Mars, comet 67P and, just last week, Pluto, which for decades marked the edge of our solar system. Europe's ExoMars mission aims to search for signatures of life on Mars using two spacecraft. In 2022, the European Space Agency (ESA) will send the Jupiter Icy Moon Explorer (JUICE) mission on a 10-year journey to Ganymede, the largest moon in the Solar System. This will be the first time we have put a spacecraft in orbit around the moon of a giant planet. Many of science's big questions, such as: how did our Solar System evolve?

<hr>

[Visit Link](http://phys.org/news/2015-07-pluto-plenty-solar-left-explore.html){:target="_blank" rel="noopener"}


