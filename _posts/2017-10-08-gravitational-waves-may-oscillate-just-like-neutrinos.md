---
layout: post
title: "Gravitational waves may oscillate, just like neutrinos"
date: 2017-10-08
categories:
author: "Lisa Zyga"
tags: [Neutrino oscillation,Graviton,Neutrino,Bimetric gravity,General relativity,Gravitational wave,Matter,Particle physics,Physics,Theoretical physics,Astrophysics,Physical cosmology,Science,Physical sciences,Scientific theories,Quantum mechanics,Cosmology]
---


The oscillating gravitational waves arise in a modified theory of gravity called bimetric gravity, or bigravity, and the physicists show that the oscillations may be detectable in future experiments. In our work, we ask what signals we could expect from a modification of gravity, and it turns out that bigravity features a unique such signal and can therefore be discriminated from other theories. Two gravitons instead of one  Currently, the best theory of gravity is Einstein's theory of general relativity, which uses a single metric to describe spacetime. The existence of two metrics (and two gravitons) in the bigravity framework eventually leads to the oscillation phenomenon. The same is true in bigravity: g is a mixture of the massive and the massless graviton, and therefore as the gravitational wave travels through the Universe, it will oscillate between g- and f-type gravitational waves.

<hr>

[Visit Link](https://phys.org/news/2017-09-gravitational-oscillate-neutrinos.html){:target="_blank" rel="noopener"}


