---
layout: post
title: "Opportunity of a lifetime—NASA's 4,000 days roving Mars"
date: 2016-04-29
categories:
author: Christian Schroeder, The Conversation
tags: [Opportunity (rover),Mars,Rover (space exploration),Endeavour (crater),Spirit (rover),Mars sol,Spaceflight,Astronomy,Solar System,Terrestrial planets,Space science,Planets of the Solar System,Astronomical objects known since antiquity,Outer space,Planetary science,Bodies of the Solar System,Exploration of Mars,Discovery and exploration of the Solar System]
---


The rover that could, and still is, running scientific marathons on Mars. Credit: NASA/JPL-Caltech/Cornell/USGS/ASU  From 20-metre-wide Eagle crater via the 150-metre Endurance crater and 800-metre Victoria crater eventually to the 22km-wide Endeavour crater: with each successive crater the next seemed always beyond reach – or so it seemed to us, so far away. Between craters, Opportunity could investigate rock fragments that had landed on top of the sand sheet and in doing so discovered meteorites among the rubble. Credit: NASA/JPL-Caltech/Cornell/USGS/ASU  Danger everywhere  There were occasions where it was touch and go whether Opportunity would be able to continue its journey. Credit: NASA/JPL-Caltech/Cornell/ASU  So, how did Opportunity celebrate its 4,000 days?

<hr>

[Visit Link](http://phys.org/news350028857.html){:target="_blank" rel="noopener"}


