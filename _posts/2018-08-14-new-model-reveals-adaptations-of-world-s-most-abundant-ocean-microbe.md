---
layout: post
title: "New model reveals adaptations of world's most abundant ocean microbe"
date: 2018-08-14
categories:
author: "University of Hawaii at Manoa"
tags: [Prochlorococcus,Metabolism,Microorganism,Marine microorganisms,Ocean,Gene,Evolution,Life,Life sciences,Nature,Biology]
---


Ocean monitoring and advances in oceanographic sensors have enabled a more detailed look than ever before at the environmental conditions that are both the consequence of microbial activity and act as stressors on the growth of microbes in the ocean. In the case of phosphorus, a limiting resource in vast oceanic regions, the cosmopolitan Prochlorococcus thrives by adopting all three strategies and a fourth, previously unknown strategy. By generating the first detailed model of metabolism for an ecologically important marine microbe, we found that Prochlorococcus has evolved a way to reduce its dependence on phosphate by minimizing the number of enzymes involved in phosphate transformations, thus relieving intracellular demands said John Casey, an oceanography doctoral candidate in the UHM School of Ocean and Earth Science and Technology and lead author of the recently published study. This will allow us to simulate marine microbial community metabolism at an unprecedented level of detail; embedding these fine-scale simulations within global ocean circulation models promises to deliver insights into how microbial assemblages interact with their environment and amongst each other, said Casey. ###  This work was funded by the National Science Foundation (NSF) through the competitive Graduate Research Fellowship Program which provided Casey with three years of research support.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/uoha-nmr111416.php){:target="_blank" rel="noopener"}


