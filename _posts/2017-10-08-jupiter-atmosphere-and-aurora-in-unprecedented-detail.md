---
layout: post
title: "Jupiter: Atmosphere and aurora in unprecedented detail"
date: 2017-10-08
categories:
author: "National Institutes of Natural Sciences"
tags: [Jupiter,Juno (spacecraft),Aurora,Atmosphere,Infrared,Great Red Spot,Magnetosphere,News aggregator,Telescope,Planet,Planetary science,Space science,Astronomy,Outer space,Planets,Solar System,Sky,Astronomical objects,Science,Bodies of the Solar System,Physical sciences]
---


Subaru Telescope images reveal weather in Jupiter's atmosphere in the mid-infrared. During our May 2017 observations that provided real-time support for Juno's sixth perijove, we obtained images and spectra of the Great Red Spot and its surroundings. Our observations showed that the Great Red Spot, the largest known vortex in the solar system, had a cold and cloudy interior increasing toward its center, with a periphery that was warmer and clearer. Another set of supporting observations that were simultaneous with the Subaru observations were made by the Gemini North telescope's NIRI instrument, which imaged Jupiter in the near-infrared, measuring reflected sunlight from cloud and haze particle in Jupiter's upper troposphere and lower stratosphere -- levels generally higher in Jupiter's atmosphere than most of the Subaru measurements, providing complementary information. Subaru's mid-infrared imaging and spectroscopy with COMICS are particularly useful to Juno's instrument, by providing information about the temperature field and the distribution of ammonia, a condensate in Jupiter similar to water in the Earth's atmosphere.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/07/170703083231.htm){:target="_blank" rel="noopener"}


