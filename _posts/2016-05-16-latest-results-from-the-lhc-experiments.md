---
layout: post
title: "Latest results from the LHC experiments"
date: 2016-05-16
categories:
author: CERN
tags: [Large Hadron Collider,ATLAS experiment,CERN,Particle physics,Compact Muon Solenoid,Theoretical physics,Physics,Science,Physical sciences,Quantum field theory,Quantum mechanics,Nuclear physics]
---


Nevertheless, the LHC experiments have already recorded 100 times more data for the summer conferences this year than they had around the same time after the LHC started up at 7 TeV in 2010. The LHC has run at the record high energy with each beam containing up to 476 bunches of 100 billion protons, delivering collisions every 50 nanoseconds. At the EPS-HEP2015 conference, the ATLAS and CMS collaborations presented the first measurements at 13 TeV on the production of charged strongly-interacting particles (hadrons). The LHC experiments have also made the first measurements of cross-sections at 13 TeV. In Run 2 data, ATLAS reported that the near-side ridge is seen in 13 TeV proton-proton collisions, with characteristics very similar to those observed by CMS in Run 1.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150727120428.htm){:target="_blank" rel="noopener"}


