---
layout: post
title: "Nanomedicine researchers target disease at the molecular level"
date: 2017-10-20
categories:
author: Wake Forest Baptist Medical Center
tags: [DNA sequencing,Nanopore,Nucleic acid,MicroRNA,Nanomedicine,RNA,Biology,Chemistry,Life sciences,Biotechnology,Technology]
---


What exactly is a nanometer? Technology, in the broadest sense, has evolved to the point where we can look at things closer and closer and effectively work with smaller and smaller amounts of stuff, said Hall, an assistant professor of biomedical engineering at Wake Forest School of Medicine, which is part of Wake Forest Baptist Medical Center. Using a nanopore device with a pore less than 10 nanometers wide, Hall and colleagues have developed a methodology capable of detecting and counting specific DNA and RNA sequences, including those of microRNA, a class of nucleic acid no more than 10 nanometers long and a single nanometer wide that can be a biomarker of disease. As long as we know the sequence of a nucleic acid that's linked to a certain disease, we can seek out those particular molecules so only they will produce a signal that we can record when they pass through the nanopore. But these nanoparticles only attach to the targeted cells and when they're heated up, they kill just those cells and they kill them for good.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171019115747.htm){:target="_blank" rel="noopener"}


