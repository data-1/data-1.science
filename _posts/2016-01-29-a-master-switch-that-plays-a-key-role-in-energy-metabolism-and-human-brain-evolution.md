---
layout: post
title: "A master switch that plays a key role in energy metabolism and human brain evolution"
date: 2016-01-29
categories:
author: SMBE Journals (Molecular Biology and Evolution and Genome Biology and Evolution)
tags: [Gene,Biology,Evolution,Human,Brain,Mutation,Genome,DNA sequencing,Nervous system,Cell (biology),Molecular biology,Biochemistry,Life sciences,Biotechnology,Genetics]
---


Scientists have long used comparative animal studies to better understand the nuances of human evolution, from making diverse body plans to the emergence of entirely powerful and unique features structures, including the human brain. Now, in a new study appearing in the advanced online edition of Molecular Biology and Evolution, corresponding authors Katja Nowick and Robert Querfurth et al. have explored the global gene regulator GABPa to better understand its influence as a master switch. By performing ChIP-Seq experiments in a human cell line, the group found that GABPa exercises its influence across the genome at 11,619 putative GABPa binding sites representing nearly 4,000 genes. Through a tour de force sequence comparison of the human GABPa binding sites with similar sequences from 34 mammals, they identified 224 GABPa binding sites unique to humans. For instance, when the human GABPa binding sites were introduced into primate cells, gene expression was increased up to 5 times in 17 out of 18 cases, indicating that a recent human mutation in GABPa resulted in higher gene expression and forces behind the evolution of novel, uniquely human functions.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/mbae-ams012516.php){:target="_blank" rel="noopener"}


