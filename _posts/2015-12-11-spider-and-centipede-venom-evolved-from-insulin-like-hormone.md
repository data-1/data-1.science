---
layout: post
title: "Spider and centipede venom evolved from insulin-like hormone"
date: 2015-12-11
categories:
author: Cell Press
tags: [Venom,Spider,Centipede,Predation,Animals,Life sciences,Biology]
---


But when we did a structural search and it pulled up the hormone, that's what really surprised us--the sequence didn't tell us where the toxins evolved from, but the structure did pretty clearly. Venom molecules are extremely complex (some are made up of over 3,000 peptides), so once the structure is known, researchers such as King can more easily evolve a toxin by making changes to its sequence to add or remove functions. King's group found that centipede venom has more subtle alterations of the hormone that make it more stable and therefore a better engineering template. In an example of convergent evolution, centipedes and spider neurotoxins used different strategies to turn the hormone into a potent neurotoxin. Structure, Undheim et al.: Weaponization of a Hormone: Convergent Recruitment of  Hyperglycemic Hormone into the Venom of Arthropod Predators http://dx.doi.org/10.1016/j.str.2015.05.003  Structure, published by Cell Press, is a monthly journal that aims to publish papers of exceptional interest in the field of structural biology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/cp-sac060915.php){:target="_blank" rel="noopener"}


