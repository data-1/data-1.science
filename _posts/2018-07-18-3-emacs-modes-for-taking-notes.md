---
layout: post
title: "3 Emacs modes for taking notes"
date: 2018-07-18
categories:
author: "Scott Nesbitt
(Alumni)"
tags: [Computer file,Emacs,Computers,Intellectual works,Computer science,Information Age,Software engineering,Digital media,Software development,Software,Technology,Computing]
---


Deft  On those rare occasions I'm forced to use a Mac, there's one tool I can't do without: the nvALT note-taking application. The summary is taken from the first line of the text file. If you add, say, Markdown, LaTeX, or even Emacs Org mode formatting to the first line, Deft ignores the formatting and displays only the text. Org mode  There would be a couple or three people who would have jumped all over me if I didn't include Org mode in this article. You can link between sections of your notes, quickly move sections without cutting and pasting, and attach files to your notes.

<hr>

[Visit Link](https://opensource.com/article/18/7/emacs-modes-note-taking){:target="_blank" rel="noopener"}


