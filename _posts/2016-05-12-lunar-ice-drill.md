---
layout: post
title: "Lunar ice drill"
date: 2016-05-12
categories:
author:  
tags: [Moon,Luna 27,Lunar south pole,Solar System,Flight,Space exploration,Moons,Discovery and exploration of the Solar System,Bodies of the Solar System,Planets of the Solar System,Planetary-mass satellites,Astronautics,Outer space,Space science,Planetary science,Spaceflight,Astronomy,Astronomical objects known since antiquity]
---


A drill designed to penetrate 1–2 m into the lunar surface is envisaged by ESA to fly to the Moon’s south pole on Russia’s Luna-27 lander in 2020. “This region is of great interest to lunar researchers and explorers because the low angle of the Sun over the horizon leads to areas of partial or even complete shadow. These shadowed areas and permanently dark crater floors, where sunlight never reaches, are believed to hide water ice and other frozen volatiles.”  Developed by Finmeccanica in Nerviano, Italy, the drill would first penetrate into the frozen ‘regolith’ and then deliver the samples to a chemical laboratory, which is being developed by the UK’s Open University. Prospect is one of the packages being developed by ESA for flight to the Moon as part of cooperation on Russia’s lunar programme. Pilot – Precise Intelligent Landing using On-board Technology – is an autonomous precision landing system incorporating ‘laser radar’ lidar for hazard detection and avoidance.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2016/05/Lunar_ice_drill){:target="_blank" rel="noopener"}


