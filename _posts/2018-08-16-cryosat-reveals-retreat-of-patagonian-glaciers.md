---
layout: post
title: "CryoSat reveals retreat of Patagonian glaciers"
date: 2018-08-16
categories:
author: ""
tags: [Glacier,CryoSat,Greenland ice sheet,Sea level rise,Ice sheet,Nature,Physical geography,Hydrography,Earth sciences,Cryosphere,Hydrology,Glaciology,Oceanography,Geography,Earth phenomena,Applied and interdisciplinary physics]
---


While ESA’s CryoSat continues to provide clear insight into how much sea ice is being lost and how the Antarctic and Greenlandic ice sheets are changing, the mission has again surpassed its original scope by revealing exactly how mountain glaciers are also succumbing to change. Glaciers all over the globe are retreating – and for the last 15 years, glacial ice has been the main cause of sea-level rise. Apart from Antarctica, Patagonia is home to the biggest glaciers in the southern hemisphere, but some are retreating faster than anywhere else in the world. This is because the weather is relatively warm and these glaciers typically terminate in fjords and lakes, exacerbating surface melting and causing them to flow faster and lose ice as icebergs at their margins. There is a clear need to monitor and understand glacial dynamics, not only in Patagonia but globally.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/CryoSat/CryoSat_reveals_retreat_of_Patagonian_glaciers){:target="_blank" rel="noopener"}


