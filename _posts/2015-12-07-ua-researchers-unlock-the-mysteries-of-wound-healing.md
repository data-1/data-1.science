---
layout: post
title: "UA researchers unlock the mysteries of wound healing"
date: 2015-12-07
categories:
author: University of Arizona 
tags: [Genetics,Cell (biology),Cancer,Wound,Messenger RNA,Cell migration,Regeneration (biology),Wound healing,Healing,Clinical medicine,Biotechnology,Biology,Health sciences,Life sciences,Cell biology,Biochemistry,Medicine]
---


A multidisciplinary research team discovers how cells know to rush to a wound and heal it -- opening the door to new treatments for diabetes, heart disease and cancer  Researchers at the University of Arizona have discovered what causes and regulates collective cell migration, one of the most universal but least understood biological processes in all living organisms. The UA researchers discovered that when mechanical force disappears -- for example at a wound site where cells have been destroyed, leaving empty, cell-free space -- a protein molecule, known as DII4, coordinates nearby cells to migrate to a wound site and collectively cover it with new tissue. The leader cells, in turn, send signals to follower cells, which do not express the genetic messenger. But precisely how leader cells formed, what controlled their behavior, and their genetic makeup were all mysteries -- until now. ###  Wong and his co-authors, UA College of Pharmacy professor Donna Zhang and four current and former UA Engineering graduate students, reported their findings in the Nature Communications article Notch1-DII4 Signalling and Mechanical Force Regulate Leader Cell Formation During Collective Cell Migration.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/uoa-uru031315.php){:target="_blank" rel="noopener"}


