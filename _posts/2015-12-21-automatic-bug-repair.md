---
layout: post
title: "Automatic bug repair"
date: 2015-12-21
categories:
author: Massachusetts Institute of Technology
tags: [Computer program,Software bug,Computer programming,Source code,Programming language,Computing,Software development,Computer science,Software engineering,Computers,Information Age,Information technology management,Computer engineering,Systems engineering,Software,Information technology,Digital media,Technology,Application software]
---


At the Association for Computing Machinery's Programming Language Design and Implementation this month, MIT researchers presented a new system that repairs dangerous software bugs by automatically importing functionality from other, more secure applications. Again, it builds up a symbolic expression that represents the operations the donor performs. The divergence represents a constraint that the safe input met and the crash-inducing input does not. CodePhage then analyzes the recipient to find locations at which the input meets most, but not quite all, of the constraints described by the new symbolic expression. Automated future  The researchers tested CodePhage on seven common open-source programs in which DIODE had found bugs, importing repairs from between two and four donors for each.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/miot-abr062915.php){:target="_blank" rel="noopener"}


