---
layout: post
title: "Mars Express detects liquid water hidden under planet’s south pole"
date: 2018-07-28
categories:
author: ""
tags: [Extraterrestrial liquid water,Mars,Planet,Poles of astronomical bodies,Water,Solar System,Space science,Outer space,Astronomy,Planetary science,Planets of the Solar System,Spaceflight,Terrestrial planets,Planets,Astronomical objects known since antiquity,Physical sciences,Bodies of the Solar System]
---


Science & Exploration Mars Express detects liquid water hidden under planet’s south pole 25/07/2018 98068 views 467 likes  Radar data collected by ESA’s Mars Express point to a pond of liquid water buried under layers of ice and dust in the south polar region of Mars. Orbiters, together with landers and rovers exploring the martian surface, also discovered minerals that can only form in the presence of liquid water. The radar investigation shows that south polar region of Mars is made of many layers of ice and dust down to a depth of about 1.5 km in the 200 km-wide area analysed in this study. Analysing the properties of the reflected radar signals and considering the composition of the layered deposits and expected temperature profile below the surface, the scientists interpret the bright feature as an interface between the ice and a stable body of liquid water, which could be laden with salty, saturated sediments. Notes for editors  “Radar evidence of subglacial liquid water on Mars” by R. Orosei et al is published in the journal Science.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Mars_Express/Mars_Express_detects_liquid_water_hidden_under_planet_s_south_pole){:target="_blank" rel="noopener"}


