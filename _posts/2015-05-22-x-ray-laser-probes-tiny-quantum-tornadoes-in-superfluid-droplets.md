---
layout: post
title: "X-ray laser probes tiny quantum tornadoes in superfluid droplets"
date: 2015-05-22
categories:
author: Slac National Accelerator Laboratory
tags: [SLAC National Accelerator Laboratory,Superfluidity,Liquid,Quantum mechanics,Physics,Physical chemistry,Applied and interdisciplinary physics,Science,Physical sciences,Chemistry]
---


A patterned 3-D grid of tiny whirlpools, called quantum vortices, populate a nanoscale droplet of superfluid helium. We did not expect the beauty and clarity of the results, said Christoph Bostedt, a co-leader of the experiment and a senior scientist at SLAC's Linac Coherent Light Source (LCLS), the DOE Office of Science User Facility where the experiment was conducted. The X-ray laser took snapshots of individual droplets, revealing dozens of tiny twisters, called quantum vortices, with swirling cores that are the width of an atom. The fast rotation of the chilled helium nanodroplets caused a regularly spaced, dense 3-D pattern of vortices to form. In a normal liquid, droplets can form peanut shapes when rotated swiftly, but the superfluid droplets took a very different form.

<hr>

[Visit Link](http://phys.org/news327843860.html){:target="_blank" rel="noopener"}


