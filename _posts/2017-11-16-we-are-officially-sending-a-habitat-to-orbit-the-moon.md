---
layout: post
title: "We Are Officially Sending a Habitat to Orbit the Moon"
date: 2017-11-16
categories:
author: ""
tags: [B330,Space exploration,Space vehicles,Space organizations,Space programs,Space-based economy,Spacecraft,Aerospace,Space industry,Spaceflight,Space science,Outer space,Flight,Astronautics,Astronomy]
---


Establishing a Lunar Habitat  Now that the United States is officially getting back into space exploration, the Moon now seems to be the focus — or at least the starting point — of a lot of plans involving space travel. To continue the trend, Bigelow Aerospace and the United Launch Alliance (ULA) announced last week they would be collaborating to design an inflatable habitat. Bigelow Aerospace is designing two B330 expandable modules, while ULA is providing the Vulcan 562 configuration rocket that will carry the module into low Earth orbit. Russia and China are set to sign a multi-year space exploration deal that covers lunar and deep space exploration, and Russia is working with NASA on the Deep Space Gateway project to build a lunar space station. Earlier this year, the China National Space Administration (CNSA) and the European Space Agency (ESA) expressed a desire to work together on an international lunar base, or a moon village, by 2020.

<hr>

[Visit Link](https://futurism.com/we-are-officially-sending-a-habitat-to-orbit-the-moon/){:target="_blank" rel="noopener"}


