---
layout: post
title: "Rocket Lab Has Successfully Launched Its Electron Rocket Into Orbit"
date: 2018-03-30
categories:
author: ""
tags: [SpaceX,Rocket Lab Electron,Rocket,Rocket Lab,Small satellite,Spaceflight,Falcon 9,Satellite,Astronomy,Vehicles,Rockets and missiles,Space program of the United States,Space advocacy,Space launch vehicles,Technology,Space programs,Space access,Outer space,Flight,Astronautics,Space vehicles,Spacecraft,Aerospace,Rocketry,Space science,Space industry,Spaceflight technology]
---


Rocket Race  The United States-based space flight startup, Rocket Lab, launched its Electron rocket for a second time on January 21, 2018. The Electron rocket, which carries smaller payloads into orbit, can launch for a cool $4.9 million. Smaller companies can offer competition in the industry, and make spaceflight more accessible for companies unable to afford costly SpaceX launches. To the Moon  While this was the Electron rocket's first successful mission, it wasn't its first flight. The company hopes to launch another one of its five available Electron rockets in early 2018.

<hr>

[Visit Link](https://futurism.com/rocket-lab-successfully-launched-electron-rocket-orbit/){:target="_blank" rel="noopener"}


