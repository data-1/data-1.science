---
layout: post
title: "Genetic engineering boosts energy transport in artificial photosynthesis – Physics World"
date: 2015-10-20
categories:
author: "$author"  
tags: [Virus,Exciton,Fluorescence,Organic solar cell,Solar cell,Photosynthesis,Applied and interdisciplinary physics,Physical sciences,Chemistry]
---


This exciton then diffuses through neighbouring chromophores until it reaches an acceptor, where excitons are collected and separated to create a voltage. When scientists create artificial structures by bringing chromophores into close proximity, the excitons “quench” each other and usually decay within 500 ps – about eight times faster than unbound chromophores. In the first virus they produced, called M13CF, the difference between the binding sites was about 33 Å – far enough apart that theory predicts the transfer would have to take place by the semi-classical random walk process. One type acts as the “donor”, which generates the exciton when hit by a photon, the other acts as an “acceptor”, which collects excitons from surrounding donors and, when excited, emits fluorescent light. The researchers used these results to calculate the distance the excitons had been able to diffuse in the two types of nanostructure.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/oct/13/genetic-engineering-boosts-energy-transport-in-artificial-photosynthesis){:target="_blank" rel="noopener"}


