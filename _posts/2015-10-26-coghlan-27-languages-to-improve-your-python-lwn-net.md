---
layout: post
title: "Coghlan: 27 languages to improve your Python [LWN.net]"
date: 2015-10-26
categories:
author: Posted October, Corbet
tags: [Computers,Linux Foundation,Software development,Open-source movement,System software,Kernel programmers,Linux websites,Linux organizations,Finnish families of Swedish ancestry,Computing,Software engineering,Free content,Computer architecture,Technology,Free software websites,Free software programmers,Computer science,Free software people,Linux kernel programmers,Intellectual works,Open content,Unix people,Linux,Unix variants,Unix,Finnish computer programmers,Torvalds family,Finnish computer scientists,Free software,Free system software,Operating system families,Linux people,Linus Torvalds,Software]
---


[Development] Posted Oct 25, 2015 1:00 UTC (Sun) by corbet  Python language developer Nick Coghlan has posted a survey of 27 languages that, he thinks, have lessons for Python. One of the things we do as part of the Python core development process is to look at features we appreciate having available in other languages we have experience with, and see whether or not there is a way to adapt them to be useful in making Python code easier to both read and write. This means that learning another programming language that focuses more specifically on a given style of software development can help improve anyone's understanding of that style of programming in the context of Python. Comments (38 posted)

<hr>

[Visit Link](http://lwn.net/Articles/661958/rss){:target="_blank" rel="noopener"}


