---
layout: post
title: "ESO's VLT detects unexpected giant glowing halos around distant quasars"
date: 2017-09-21
categories:
author: "ESO"
tags: [European Southern Observatory,Quasar,Paranal Observatory,Astronomy,Galaxy,Physical sciences,Astrophysics,Physics,Natural sciences,Science,Astronomical objects,Space science]
---


This new study, however, has thrown up a surprise, with the detection of large halos around all 19 quasars observed -- far more than the two halos that were expected statistically. The gaseous components of this web are normally extremely difficult to detect, so the illuminated halos of gas surrounding the quasars deliver an almost unique opportunity to study the gas within this large-scale cosmic structure. It is the first time that MUSE and its unique observing capabilities have been used for a survey of this kind. At Paranal, ESO operates the Very Large Telescope, the world's most advanced visible-light astronomical observatory and two survey telescopes. VISTA works in the infrared and is the world's largest survey telescope and the VLT Survey Telescope is the largest telescope designed to exclusively survey the skies in visible light.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/e-evd102416.php){:target="_blank" rel="noopener"}


