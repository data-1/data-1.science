---
layout: post
title: "How The Electric Grid Works, Why Renewables Will Dominate"
date: 2015-12-18
categories:
author: Christopher Arcus, Remeredzai Joseph Kuhudzai, U.S. Energy Information Administration, Steve Hanley, George Harvey, Written By
tags: [Renewable energy,Energy storage,Wind power,Solar power,Electric generator,Concentrated solar power,Peaking power plant,Hydroelectricity,Environmental technology,Sustainable development,Renewable resources,Energy,Nature,Physical quantities,Power (physics),Electricity,Sustainable technologies,Electrical engineering,Electric power,Sustainable energy,Technology,Energy technology]
---


Others are generating electricity to meet demand. You can see that wind and solar displace the natural gas peak generation. And with so much flexible power available most of the year, wind and solar can be integrated even more often. As time goes on, a mix of variable renewables like wind and solar, and flexible renewables like hydro, biomass, geothermal, and concentrating solar with thermal storage, as well as grid storage from flow batteries, lithium batteries, compressed air storage, and pumped hydro can make up the rest. The combination of grid practices, transmission, flexible sources like CSP with thermal storage, geothermal, hydro, and biomass, plus 10% storage is all that is required to meet 80% renewables by 2050 economically by 2050.

<hr>

[Visit Link](http://cleantechnica.com/2015/12/16/how-the-grid-works-why-renewables-can-dominate/){:target="_blank" rel="noopener"}


