---
layout: post
title: "Stanford researchers genetically engineer yeast to produce opioids"
date: 2016-05-26
categories:
author: Stanford University School of Engineering
tags: [Opioid,Opium,Yeast,Hydrocodone,Analgesic,Artemisinin,Papaver somniferum,Morphine,Biology,Biotechnology,Medicine,Life sciences]
---


A process that had taken a year from farm to pharmaceutical factory now occurs in three to five days in yeast genetically engineered to biosynthesize the active ingredients for opioid painkillers; this could broaden access to many plant-based medicines. Now researchers at Stanford have genetically engineered yeast to make painkilling medicines, a breakthrough that heralds a faster and potentially less expensive way to produce many different types of plant-based medicines. Smolke's team is modernizing the process by inserting precisely engineered snippets of DNA into cells, such as yeast, to reprogram the cells into custom chemical assembly lines to produce medicinal compounds. Many plants, including opium poppies, produce (S)-reticuline, a molecule that is a precursor to active ingredients with medicinal properties. The molecules we produced and the techniques we developed show that it is possible to make important medicines from scratch using only yeast, she said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/ssoe-srg080715.php){:target="_blank" rel="noopener"}


