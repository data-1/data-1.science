---
layout: post
title: "Fruit flies journey to International Space Station to study effects of zero gravity on the heart"
date: 2017-10-08
categories:
author: "Sanford Burnham Prebys Medical Discovery Institute"
tags: [International Space Station,News aggregator,Weightlessness,Drosophila melanogaster,Spaceflight]
---


As interest in space travel grows -- for both research and commercial aims -- it is increasingly important to understand the effect a microgravity environment can have on the human heart for both the traveler and their potential future children, said Karen Ocorr, Ph.D., assistant professor of the Development, Aging and Regeneration Program at SBP. This experiment will help reveal the short- and long-term effects of space travel on the cardiovascular system, using fruit flies as a model. Once we understand these molecular changes we can work on creating interventions that could help protect the heart in space, and potentially help us treat cardiovascular disorders in humans on Earth as well. Generational studies of the offspring of the flies that traveled to space will be conducted, which will help reveal the impact space travel could have for individuals considering having children. In addition to potential therapeutic value, studying the progeny of these fruit flies will help us better understand the effects space travel could have on our children or grandchildren.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/05/170525195536.htm){:target="_blank" rel="noopener"}


