---
layout: post
title: "How to Monitor Progress of (Copy/Backup/Compress) Data using ‘pv’ Command"
date: 2015-10-02
categories:
author: Aaron Kili
tags: [Package manager,Standard streams,Computer file,ZIP (file format),Online and offline,Fedora Linux,APT (software),Digital media,Information technology management,Information Age,Computer data,Computer science,Computers,Computer engineering,Computing,Software,System software,Technology,Computer architecture,Software development,Operating system technology,Software engineering,Utility software]
---


When using the pv command, it gives you a visual display of the following information:  The time that has elapsed. How to Install pv Command in Linux? The syntax of pv command as follows:  pv file pv options file pv file > filename.out pv options | command > filename.out comand1 | pv | command2  The options used with pv are divided into three categories, display switches, output modifiers and general options. To turn on the display bar, use the -p option. # pv opensuse.vdi > /tmp/opensuse.vdi  2.

<hr>

[Visit Link](http://www.tecmint.com/monitor-copy-backup-tar-progress-in-linux-using-pv-command/){:target="_blank" rel="noopener"}


