---
layout: post
title: "Sharper imaging using X-rays"
date: 2014-06-24
categories:
author: Helmholtz Association Of German Research Centres
tags: [Angular resolution,X-ray microscope,Microscope,Optics,Zone plate,X-ray optics,Microscopy,X-ray,Physical chemistry,Science,Electromagnetic radiation,Radiation,Materials science,Applied and interdisciplinary physics,Atomic molecular and optical physics,Electrodynamics,Chemistry,Electromagnetism,Natural philosophy]
---


3D X-ray optics of this kind allow the resolutions and optical intensities to be considerably improved. These three-dimensional nanostructures focus the incident X-rays much more efficiently and enable improved spatial resolution below ten nanometers. Visible light can resolve structures on the order of a quarter micron, while the considerably shorter wavelength of X-rays can in principle resolve features down to a few nanometres. In a first step towards three-dimensional X-ray optics, the experts at HZB have manufactured three layers of Fresnel zone plates nearly perfectly above one another. If we are successful in positioning five zone plate layers above one another, which is our next goal, we will be able to utilise a many times higher fraction of the incident X-ray light for imaging than has been available up to now, says Werner.

<hr>

[Visit Link](http://phys.org/news322746196.html){:target="_blank" rel="noopener"}


