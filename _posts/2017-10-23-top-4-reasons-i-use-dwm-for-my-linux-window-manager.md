---
layout: post
title: "Top 4 reasons I use dwm for my Linux window manager"
date: 2017-10-23
categories:
author: "Jimmy Sjölund"
tags: [Desktop environment,Xfce,Window manager,Graphical user interfaces,Technology,Software development,System software,Software engineering,Computer science,Computers,Computing,Software,User interfaces]
---


In short, a desktop environment such as KDE, Gnome, or Xfce includes many things, of which a window manager is one, but also with select applications. Different desktop environments use different window managers. Dynamic window management  The killer feature for dwm, as with Awesome and xmonad, is the part where the tool automatically arranges the windows for you, filling the entire space of your screen. Sure, for most desktop environments today it's possible to create keyboard shortcuts to arrange windows to the left, right, top, bottom or full screen, but with dwm it's just one less thing to think about. Dwm has a similar function called tags.

<hr>

[Visit Link](https://opensource.com/article/17/7/top-4-reasons-i-use-dwm-linux-window-manager){:target="_blank" rel="noopener"}


