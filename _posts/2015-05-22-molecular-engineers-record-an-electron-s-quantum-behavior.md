---
layout: post
title: "Molecular engineers record an electron's quantum behavior"
date: 2015-05-22
categories:
author: University Of Chicago
tags: [Quantum mechanics,Nitrogen-vacancy center,Electron,Physical sciences,Technology,Applied and interdisciplinary physics,Science,Physics]
---


Their technique uses ultrafast pulses of laser light both to control the defect's entire quantum state and observe how that single electron state changes over time. Being able to test a wide range of these timescales gives a far more complete picture of the dynamics of the NV center than has been obtained previously. Our goal was to push the limits of quantum control in these remarkable defect systems, explained Lee Bassett, co-lead author on the paper and now an assistant professor of electrical and systems engineering at the University of Pennsylvania, but the technique also provides an exciting new measurement tool. This technique also provides a means of control of the spin state—an important precursor for any quantum information system, said Evelyn Hu, a professor of applied physics and electrical engineering at Harvard University, who is not connected with the new work. You only have to be able to use light to transfer an electron between a ground state and an excited state, said Awschalom.

<hr>

[Visit Link](http://phys.org/news327243955.html){:target="_blank" rel="noopener"}


