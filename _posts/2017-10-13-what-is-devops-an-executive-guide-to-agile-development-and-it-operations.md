---
layout: post
title: "What is DevOps? An executive guide to agile development and IT operations"
date: 2017-10-13
categories:
author: "Steven Vaughan-Nichols, Senior Contributing Editor, Oct."
tags: []
---


How can DevOps help help you? DevOps: Why use it? These are continuous integration/continuous development (CI/CD) programs, DevOps software, and container orchestration programs. While these DevOps programs work well for servers and server VMs, they're not designed to manage containers. Rolling upgrades and rollback: When you deploy a new version of the container, or the applications running within the containers, the container management tools automatically update them across your container cluster.

<hr>

[Visit Link](http://www.zdnet.com/article/what-is-devops-an-executive-guide-to-agile-development-and-it-operations/#ftag=RSSbaffb68){:target="_blank" rel="noopener"}


