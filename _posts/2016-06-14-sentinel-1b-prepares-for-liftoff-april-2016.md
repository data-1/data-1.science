---
layout: post
title: "Sentinel-1B prepares for liftoff (April 2016)"
date: 2016-06-14
categories:
author: ""
tags: [Guiana Space Centre,Sentinel-1,Satellites,Astronomy,Spaceflight technology,Bodies of the Solar System,Space exploration,European space programmes,Space agencies,Space programs,Outer space,Spaceflight,Space science,Spacecraft,Astronautics,Flight,Space vehicles]
---


This timelapse video shows Sentinel-1B, from final preparations to liftoff on a Soyuz launcher from Europe’s Spaceport in French Guiana, on 25 April 2016 at 21:02 GMT (23:02 CEST). With the Sentinel-1 mission designed as a two-satellite constellation, Sentinel-1B will join its identical twin, Sentinel-1A, which was launched two years ago from Kourou. Both satellites carry an advanced radar that images Earth’s surface through cloud and rain regardless of whether it is day or night. By orbiting 180° apart, global coverage and data delivery are optimised for the environmental monitoring Copernicus programme. The mission provides radar imagery for a multitude of services and applications to improve everyday life and understand our changing planet.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2016/05/Sentinel-1B_prepares_for_liftoff_April_2016){:target="_blank" rel="noopener"}


