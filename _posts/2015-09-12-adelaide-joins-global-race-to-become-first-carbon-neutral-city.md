---
layout: post
title: "Adelaide Joins Global Race To Become First Carbon Neutral City"
date: 2015-09-12
categories:
author: Joshua S Hill, David Waterworth, Written By
tags: [Low-carbon economy,Clean technology,Climate change,Australia,Economy and the environment,Environment,Economy,Energy,Nature,Environmental issues with fossil fuels,Climate variability and change,Natural environment,Energy and the environment,Global environmental issues,Environmental impact]
---


“South Australia has led the nation in renewable energy assets by setting policy frameworks and regulatory and approvals processes to provide greater consistency, transparency, and certainty needed by investors,” write Jay Weartherill, the Premier of South Australia, and Tom Koutsantonis MP, the Minister for Mineral Resources and Energy in the introduction to a strategy paper recently published by the State Government. South Australia already generates 38% of its electricity from renewable energy, which according to the State Government “translates into $6.6 billion in investment in renewable energy generation in South Australia,” of which “around 40%, or $2.4 billion, of this was realised in regional areas.”  Unsurprisingly, such investment has had a dramatic impact on job creation and regional economic development. Therefore, in an effort to build upon their “position of strength,” the South Australian State Government has set an investment target of $10 billion in low carbon generation by 2025. “We know we need to decarbonise our economy and we know there is $4bn already in the pipeline for low carbon economic activity. Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2015/09/11/adelaide-joins-global-race-become-first-carbon-neutral-city/){:target="_blank" rel="noopener"}


