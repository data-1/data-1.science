---
layout: post
title: "Tiny plant fossils offer window into Earth's landscape millions of years ago"
date: 2015-09-05
categories:
author: National Science Foundation 
tags: [Phytolith,Fossil,Plant,Leaf,Leaf area index,Earth,Paleontology,Ecosystem,Soil,Nature,Earth sciences,Natural environment,Ecology]
---


The team focused its fieldwork on several sites in Patagonia, which have some of the best preserved fossils in the world. Plant cell patterns change with sun exposure  Work by other scientists has shown that the cells found in a plant's outermost layer, called the epidermis, change in size and shape depending on how much sun it's exposed to while its leaves develop. When compared with tree coverage estimated from the corresponding photos, Dunn and co-authors found that the curves and sizes of the cells directly related to how shady their environment was. Testing this relationship between leaf area index and plant cell structures in modern environments allowed the scientists to develop an equation that can be used to predict vegetation openness at any time in the past, provided there are preserved plant fossils. We should be able to reconstruct leaf area index by using all kinds of fossil plant preservation, not just phytoliths.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/nsf-tpf012015.php){:target="_blank" rel="noopener"}


