---
layout: post
title: "New paths into the world of quasiparticles"
date: 2015-01-20
categories:
author: University of Innsbruck 
tags: [Quantum mechanics,Quasiparticle,Quantum entanglement,Rainer Blatt,Chemistry,Physics,Theoretical physics,Applied and interdisciplinary physics,Physical sciences,Science,Scientific theories,Physical chemistry,Scientific method]
---


Christian Roos' research team at the Institute for Quantum Optics and Quantum Information at the Austrian Academy of Sciences in Innsbruck has established a new experimental platform for investigating quantum phenomena: In a string of trapped ultracold ions they can precisely initialise, control and measure the states and properties of quasiparticle excitations in a many-body quantum system. The precise excitation of one of the particles also affects the other particles. Excitation distribution has previously been observed in experiments with neutral atoms, where correlations between particles have also been shown. New research with quasiparticles  With this new scheme we can precisely manipulate the quasiparticles, says an excited Philipp Hauke, one of the authors of this study. In addition, this platform could also be used to study the role of transport processes in biological systems.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/uoi-npi070814.php){:target="_blank" rel="noopener"}


