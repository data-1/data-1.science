---
layout: post
title: "Altamura Man yields oldest Neanderthal DNA sample"
date: 2016-04-20
categories:
author: Bob Yirka
tags: [Altamura Man]
---


Altamura Man, surrounded by limestone deposits. Credit: Wikipedia  (Phys.org)—A team of researchers working in Italy has confirmed that Altamura Man was a Neanderthal and dating of pieces of calcite which were on the remains has revealed that the bones are 128,000 to 187,000 years old. Altamura Man was discovered in a cave in southern Italy in 1993 by cave explorers. The team also reports that samples of DNA have also been retrieved from the sample, and because of the age, represent the oldest such samples ever recovered from Neanderthal remains. Explore further The stapes of a neanderthal child points to the anatomical differences with our species  More information: The Neanderthal in the karst: First dating, morphometric, and paleogenetic data on the fossil skeleton from Altamura (Italy), Journal of Human Evolution, Available online 21 March 2015 The Neanderthal in the karst: First dating, morphometric, and paleogenetic data on the fossil skeleton from Altamura (Italy),, Available online 21 March 2015 DOI: 10.1016/j.jhevol.2015.02.007  Abstract  In 1993, a fossil hominin skeleton was discovered in the karst caves of Lamalunga, near Altamura, in southern Italy.

<hr>

[Visit Link](http://phys.org/news347266610.html){:target="_blank" rel="noopener"}


