---
layout: post
title: "Ceres' bright spots seen in striking new detail"
date: 2016-06-30
categories:
author: "Jpl Nasa"
tags: [Dawn (spacecraft),Ceres (dwarf planet),Space science,Spacecraft,Discovery and exploration of the Solar System,Astronomical objects,Space exploration,Bodies of the Solar System,Science,Solar System,Spaceflight,Astronomy,Planetary science,Outer space]
---


Credit: NASA/JPL-Caltech/UCLA/MPS/DLR/IDA  The brightest spots on the dwarf planet Ceres gleam with mystery in new views delivered by NASA's Dawn spacecraft. The new up-close view of Occator crater from Dawn's current vantage point reveals better-defined shapes of the brightest, central spot and features on the crater floor. Views from Dawn's current orbit, taken at an altitude of 915 miles (1,470 kilometers), have about three times better resolution than the images the spacecraft delivered from its previous orbit in June, and nearly 10 times better than in the spacecraft's first orbit at Ceres in April and May. The spacecraft has already completed two 11-day cycles of mapping the surface of Ceres from its current altitude, and began the third on Sept. 9. By imaging Ceres at a slightly different angle in each mapping cycle, Dawn scientists will be able to assemble stereo views and construct 3-D maps.

<hr>

[Visit Link](http://phys.org/news/2015-09-ceres-bright.html){:target="_blank" rel="noopener"}


