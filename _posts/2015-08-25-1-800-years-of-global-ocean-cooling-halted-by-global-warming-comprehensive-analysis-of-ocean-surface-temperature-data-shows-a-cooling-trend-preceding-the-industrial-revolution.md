---
layout: post
title: "1,800 years of global ocean cooling halted by global warming: Comprehensive analysis of ocean surface temperature data shows a cooling trend preceding the Industrial Revolution"
date: 2015-08-25
categories:
author: University of Maryland 
tags: [Climate change,Climate,Volcano,Earth,Ocean,Sea surface temperature,Earth phenomena,Atmospheric sciences,Global natural environment,Atmosphere,Climate variability and change,Applied and interdisciplinary physics,Natural environment,Nature,Physical geography,Earth sciences]
---


But when events such as volcanic eruptions cluster together in a relatively short period of time, the temperature changes can become prolonged. Volcanic eruptions have a short-term cooling effect on the atmosphere, but our results showed that when volcanic eruptions occurred more frequently, there was long-term ocean cooling, said lead author Helen McGregor, an Australian Research Council (ARC) Future Fellow at the University of Wollongong in Australia. The team compiled the data within 200-year brackets to observe long-term trends, and then compared the findings to land-based reconstructions, which revealed similar cooling trends. Model simulations by others have shown us that the oceans can impart a substantial delay in the warming of the surface climate, said Evans, who is also the lead of the Ocean2k working group of the Past Global Changes (PAGES) program. With much of the heat from global warming entering our oceans, recent ocean surface warming may foreshadow additional future warming, in the same way ocean cooling appeared as a long-term response to large and frequent volcanic events in recent centuries.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150817132013.htm){:target="_blank" rel="noopener"}


