---
layout: post
title: "Extraordinary animation reveals ocean's role in El Ninos"
date: 2018-08-15
categories:
author: "University Of New South Wales"
tags: [El Nio,Earth sciences]
---


Australian researchers from the National Computational Infrastructure (NCI) and the ARC Centre of Excellence for Climate System Science have produced a remarkable high-resolution animation of the largest El Niño ever recorded. It is so detailed that it took 30,000 computer hours crunching ocean model data on Australia's most powerful supercomputer, Raijin, before it could be extracted by the NCI visualisation team to produce the animation. The animation looks beneath the ocean surface to reveal the oceanic processes that led to the 1997/98 El Niño - an event that caused billions of dollars of damage worldwide and was followed by consecutive strong La Niña events. The animation shows how shifting pools of warmer or cooler than average water 300m below the surface of the ocean can trigger these powerful events, said Dr Alex Sen Gupta, a member of the visualisation team from the ARC Centre of Excellence for Climate System Science. The animation shows us that a well developed deep ocean observation system can give us advance warning of extreme El Niños and La Niñas, said team member Dr Shayne McGregor from Monash University.

<hr>

[Visit Link](http://phys.org/news/2016-12-extraordinary-animation-reveals-ocean-role.html){:target="_blank" rel="noopener"}


