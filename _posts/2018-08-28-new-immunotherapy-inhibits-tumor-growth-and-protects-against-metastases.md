---
layout: post
title: "New immunotherapy inhibits tumor growth and protects against metastases"
date: 2018-08-28
categories:
author: "VIB (the Flanders Institute for Biotechnology)"
tags: [Cancer,Immune system,Immunotherapy,Neoplasm,Necroptosis,Clinical medicine,Medical specialties,Diseases and disorders,Neoplasms,Causes of death,Medicine,Health,Biology,Health sciences]
---


The research team developed a treatment in mice that destroys part of the tumor and stimulates the immune system to attack persistent surviving cancer cells. Previous VIB research has demonstrated that when cells die from necroptosis, the immune system is alarmed. PhD student Lien Van Hoecke (VIB-UGent Center for Medical Biotechnology): This phenomenon is also called immunogenic cell death, as the dying cells become examples for the immune system, which then learns and remembers which cells to search for and attack. Publication  Treatment with mRNA coding for the necroptosis mediator MLKL induces antitumor immunity directed against neo-epitopes, Van Hoecke et al., Nature Communications 2018  Questions from patients  A breakthrough in research is not the same as a breakthrough in medicine. Everyone can submit questions concerning this and other medically-oriented research directly to VIB via this address  ###

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/vfi-nii082318.php){:target="_blank" rel="noopener"}


