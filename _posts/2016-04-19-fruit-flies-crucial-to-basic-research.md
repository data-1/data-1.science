---
layout: post
title: "Fruit flies crucial to basic research"
date: 2016-04-19
categories:
author: Marco Gallio, The Conversation
tags: [Brain,Drosophila melanogaster,Drosophila,Model organism,Genetics,Fly,Biology,Neuroscience]
---


With the success of Morgan's flyroom, the humble fruit fly was set on its way to becoming one of the leading models in modern biology, contributing vast amounts of knowledge to many areas – including genetics, embryology, cell biology, neuroscience. Studying fly brains to understand our own  Here I am, ready to answer many of your biological questions. Can the humble fly really contribute to our understanding of how our own brain works? Credit: Elissa Lei, PhD, NIH, CC BY  Of course, we can do these kinds of experiments in a number of animal models. The fruit fly brain, tiny compared to the mouse and minuscule compared to the human – but still so useful in research.

<hr>

[Visit Link](http://phys.org/news346924687.html){:target="_blank" rel="noopener"}


