---
layout: post
title: "Evolution of marine crocodilians constrained by ocean temperatures"
date: 2016-01-01
categories:
author: University Of Bristol
tags: [Crocodilia,Ocean,Fossil,Evolution,Cretaceous,Jurassic,Natural environment,Extinction,Climate change,Earth sciences,Nature]
---


Credit: Guillaume Suan  The ancestors of today's crocodiles colonised the seas during warm phases and became extinct during cold phases, according to a new Anglo-French study which establishes a link between marine crocodilian diversity and the evolution of sea temperature over a period of more than 140 million years. Dr Martin, with a team of paleontologists and geochemists from the Université de Lyon and the University of Bristol, compared the evolution of the number of marine crocodilian fossil species to the sea temperature curve during the past 200 million years. These first marine crocodilians became extinct about 25 million years later, during a period of global freezing. Then, another crocodilian lineage appeared and colonised the marine environment during another period of global warming. Jurassic metriorhynchoids did not go extinct during the cold spells of the early Cretaceous, unlike the teleosaurids, another group of marine crocodilians.

<hr>

[Visit Link](http://phys.org/news327669076.html){:target="_blank" rel="noopener"}


