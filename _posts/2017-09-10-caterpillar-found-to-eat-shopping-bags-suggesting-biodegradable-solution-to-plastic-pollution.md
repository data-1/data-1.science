---
layout: post
title: "Caterpillar found to eat shopping bags, suggesting biodegradable solution to plastic pollution"
date: 2017-09-10
categories:
author: "University of Cambridge"
tags: [Waxworm,Polyethylene,Plastic,Galleria mellonella,Chemistry,Materials,Nature,Chemical substances,Artificial materials]
---


The worms were temporarily kept in a typical plastic shopping bag that became riddled with holes. If a single enzyme is responsible for this chemical process, its reproduction on a large scale using biotechnological methods should be achievable, said Cambridge's Paolo Bombelli, first author of the study published today in the journal Current Biology. This discovery could be an important tool for helping to get rid of the polyethylene plastic waste accumulated in landfill sites and oceans. The researchers conducted spectroscopic analysis to show the chemical bonds in the plastic were breaking. As the molecular details of the process become known, the researchers say it could be used to devise a biotechnological solution on an industrial scale for managing polyethylene waste.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/uoc-cft042117.php){:target="_blank" rel="noopener"}


