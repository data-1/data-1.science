---
layout: post
title: "Latest experiment at Large Hadron Collider reports first results"
date: 2015-10-19
categories:
author: Jennifer Chu, Massachusetts Institute Of Technology
tags: [Subatomic particle,Collider,Large Hadron Collider,Proton,Compact Muon Solenoid,Chronology of the universe,Particle physics,Physical sciences,Physics,Applied and interdisciplinary physics,Nature,Science,Nuclear physics]
---


The team analyzed 20 million snapshots of the interacting proton beams, and identified 150,000 events containing proton-proton collisions. Compared with the collider's first run, at an energy intensity of 7 TeV, the recent experiment at 13 TeV produced 30 percent more particles per collision. To shrink this uncertainty and more precisely count the number of particles produced in an average proton collision, Lee and his team used the Large Hadron Collider's CMS detector. To count these charged, lightweight particles, the scientists analyzed the data with the detector's magnet off. With lead ion collisions, we can reproduce the early universe in a 'small bang.'

<hr>

[Visit Link](http://phys.org/news/2015-10-latest-large-hadron-collider-results.html){:target="_blank" rel="noopener"}


