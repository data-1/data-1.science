---
layout: post
title: "PNAS: Childhood leukemias forged by different evolutionary forces than in older adults"
date: 2016-01-29
categories:
author: University of Colorado Anschutz Medical Campus
tags: [Mutation,Hematopoietic stem cell,Cancer,Genetic drift,Natural selection,Leukemia,Carcinogenesis,Evolution,Stem cell,Evolutionary biology,Biological evolution,Biology,Clinical medicine,Genetics,Diseases and disorders]
---


Andrii Rozhok, Jennifer Salstrom and James DeGregori provide evidence that the evolutionary force of genetic drift contributes to the ability of cancerous cells to overtake populations of healthy cells in young children. In fact, they discovered two factors that influence the development of early-life leukemia: the small HSC pool size at birth and the high rate of cell division necessary for body growth early in life. Drift is the role of chance - the possibility that despite being less fit, an animal, organism or blood stem cell with cancer-causing mutation will survive to shift the genetic makeup of the population. In small stem cell pools, such as for HSC pools very early in life, drift (chance) becomes much more important as a lucky genotype may end up with a larger share of the total HSC pool than warranted by its fitness status. Thus this paper shows that in early life, leukemias are driven by mutation and drift whereas in later life, leukemias are driven by mutation and selection.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/uoca-pcl010816.php){:target="_blank" rel="noopener"}


