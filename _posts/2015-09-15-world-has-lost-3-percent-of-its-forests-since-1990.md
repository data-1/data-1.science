---
layout: post
title: "World has lost 3 percent of its forests since 1990"
date: 2015-09-15
categories:
author: University of Melbourne 
tags: [Forest,Deforestation,Natural environment,Natural resources,Natural resource management,Sustainable development,Economy]
---


The UN's Global Forest Resources Assessment (GFRA) 2015 was released this week, revealing that while the pace of forest loss has slowed, the damage over the past 25 years has been considerable. These are not good stats, Professor Keenan said of the latest report. Brazil and Indonesia, both among the highest deforestation offenders, have significantly improved their ways - with Brazil's current net loss rate 40 per cent lower than in the 1990s. Some have policies and regulations to protect forests, but they do not have the capacity and resources to implement them. Significant findings:  In 2015, total forest cover is 3,999 million hectares globally (or 31 per cent of global land)  Since 1990, there has been a loss of three per cent of total forest area, six per cent of total natural forested area and ten per cent decrease in tropical forests  Average rate of loss has halved from 7.3 million hectares in the 1990s, to 3.3 million hectares between 2010 and 2015  Decline in natural forests has been offset by 66 per cent rise in planted forest, from 168 million hectares to 278 million hectares  Loss occurring more quickly in some of the lowest-income countries  ###

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/uom-whl091415.php){:target="_blank" rel="noopener"}


