---
layout: post
title: "Ancient alga knew how to survive on land before it left water & evolved into first plant"
date: 2015-10-06
categories:
author: John Innes Centre 
tags: [Plant,Arbuscular mycorrhiza,Fungus,Biology,Nature,Organisms,Biological evolution]
---


A team of scientists has solved a long-running mystery about the first stages of plant life on Earth  A team of scientists led by Dr Pierre-Marc Delaux (John Innes Centre / University of Wisconsin, Madison) has solved a long-running mystery about the first stages of plant life on earth. Dr Delaux and colleagues analysed DNA and RNA of some of the earliest known land plants and green algae and found evidence that their shared algal ancestor living in the Earth's waters already possessed the set of genes, or symbiotic pathways, it needed to detect and interact with the beneficial AM fungi. The team of scientists believes this capability was pivotal in enabling the alga to survive out of the water and to colonise the earth. Dr Delaux said:  At some point 450 million years ago, alga from the earth's waters splashed up on to barren land. Our discovery shows for the first time that the alga already knew how to survive on land while it was still in the water.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/jic-aak100215.php){:target="_blank" rel="noopener"}


