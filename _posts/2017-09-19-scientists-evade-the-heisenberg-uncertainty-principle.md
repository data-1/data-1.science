---
layout: post
title: "Scientists evade the Heisenberg uncertainty principle"
date: 2017-09-19
categories:
author: "ICFO-The Institute of Photonic Sciences"
tags: [Uncertainty principle,Spin (physics),Atom,Magnetic resonance imaging,ICFO,Electron,Atomic clock,Applied and interdisciplinary physics,Theoretical physics,Quantum mechanics,Physics,Science]
---


In MRI, for example, the pointing angle of the spin gives information about where in the body the atom is located, while the amount of spin (the amplitude) is used to distinguish different kinds of tissue. The sensitivity of this kind of measurement was long thought to be limited by Heisenberg's uncertainty principle, which states that accurately measuring one property of an atom puts a limit to the precision of measurement you can obtain on another property. Since most atomic instruments measure two properties (spin amplitude and angle), the principle seems to say that the readings will always contain some quantum uncertainty. The ICFO team showed how to put nearly all of the uncertainty into the angle that is not measured by the instrument. ICFO hosts an active Corporate Liaison Program that aims at creating collaborations and links between industry and ICFO researchers.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/iiop-set032017.php){:target="_blank" rel="noopener"}


