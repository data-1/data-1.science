---
layout: post
title: "World's oldest chameleon found in amber fossil"
date: 2016-03-15
categories:
author: University Of Florida
tags: [Fossil,Lizard,Chameleon,Amber,Biodiversity]
---


Over time, the resin fossilized into amber, leaving the lizard remarkably preserved. Seventy-eight million years older than the previous oldest specimen on record, the dime-size chameleon along with 11 more ancient fossil lizards were pulled, encased in amber, from a mine decades ago, but it wasn't until recently that scientists had the opportunity to analyze them. The fossil record is sparse because the delicate skin and fragile bones of small lizards do not usually preserve, especially in the tropics, which makes the new amber fossils an incredibly rare and unique window into a critical period of diversification. The amber gecko, for example, confirms the group already had highly advanced adhesive toe pads used for climbing, suggesting this adaptation originated earlier. Stanley said the fact that these incredibly ancient lizards have modern counterparts living today in the Old World tropics speaks to the stability of tropical forests.

<hr>

[Visit Link](http://phys.org/news/2016-03-world-oldest-chameleon-amber-fossil.html){:target="_blank" rel="noopener"}


