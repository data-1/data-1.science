---
layout: post
title: "Earth-Size Diamond Detected in Space by Astronomers : Space : Science World Report"
date: 2014-06-24
categories:
author: Science World Report
tags: [Star,Pulsar,White dwarf,Neutron star,Physical sciences,Astronomy,Space science,Astronomical objects,Stellar astronomy,Stars,Outer space,Physics,Nature]
---


A team of astronomers has identified the coldest, faintest white dwarf star that is approximately 11 billion years old. Using the National Radio Astronomy Observatory's (NRAO) Green Bank Telescope (GBT) and Very Long Baseline Array (VLBA), David Kaplan along with his colleagues at the University of Wisconsin-Milwaukee, successfully identified the star that is the coldest and faintest white dwarf ever detected. The mass of the pulsar was 1.2 times that of the Sun and the companion mass was 1.05 times that of the Sun. After confirming that the pulsar's companion was just another neutron star, the researchers then identified it to be a white dwarf. The finding appears in The Astrophysical Journal.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15619/20140624/earth-size-diamond-in-space-detected-astronomers.htm){:target="_blank" rel="noopener"}


