---
layout: post
title: "First measurements taken of South Africa's Iron Age magnetic field history"
date: 2015-09-07
categories:
author: University of Rochester 
tags: [Earths magnetic field,Magnetism,Magnetic field,Earth,Large low-shear-velocity provinces,Geology,Planetary science,Nature,Planets of the Solar System,Geophysics,Electromagnetism,Earth sciences,Space science,Applied and interdisciplinary physics,Physical sciences]
---


A team of researchers has for the first time recovered a magnetic field record from ancient minerals for Iron Age southern Africa (between 1000 and 1500 AD). It has long been thought reversals start at random locations, but our study suggests this may not be the case, said Tarduno, a leading expert on Earth's magnetic field. As Tarduno points out, it is only speculation because weakening magnetic fields can recover without leading to a reversal of the poles. The top of the core beneath this region is overlain by unusually hot and dense mantle rock, said Tarduno. Most of the global decay of intensity is related to the weakening field of the Southern Hemisphere that includes Southern Africa.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/uor-fmt072315.php){:target="_blank" rel="noopener"}


