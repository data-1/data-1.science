---
layout: post
title: "Massive, computer-analyzed geological database reveals chemistry of ancient ocean"
date: 2018-08-15
categories:
author: "University of Wisconsin-Madison"
tags: [Stromatolite,Limestone,Shallow water marine environment,Geologic time scale,Geology,American Association for the Advancement of Science,Ocean,Dolomite (rock),Sedimentary rock,Nature,Earth sciences]
---


Geologists have known for a long time that stromatolites were abundant in shallow marine environments during the Precambrian, before the emergence of multi-cellular life more than 560 million years ago, says Jon Husson, a post-doctoral researcher and co-author of a study now online in the journal Geology. The best predictor of stromatolite prevalence, both before and after the evolution of animals, is the abundance of dolomite in shallow marine sediments, says Husson. Dolomite is harder to make than low-magnesium carbonate and it forms today in only a narrow range of marine environments. GeoDeepDive is a digital library built on high throughput computing technology that can read millions of papers and siphon off specific information. Beyond answering a fundamental question of Earth's history, the new study allows us to do the kind of analyses that scientists used to only dream about, Peters says: 'If we could just compile all the published information on...

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/uow-mcg033017.php){:target="_blank" rel="noopener"}


