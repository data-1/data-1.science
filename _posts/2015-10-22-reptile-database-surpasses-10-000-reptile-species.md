---
layout: post
title: "Reptile Database surpasses 10,000 reptile species"
date: 2015-10-22
categories:
author: Sathya Achia Abraham, Virginia Commonwealth University
tags: [Reptile,Taxonomy (biology),Species,Taxa,Biology,Science]
---


Credit: Truong Nguyen  More than 10,000 reptile species have been recorded into the Reptile Database, a web-based catalogue of all living reptile species and classification, making the reptile species among the most diverse vertebrate groups in the world, alongside bird and fish species. Previously, 10,000 was considered the landmark number because there are approximately 10,000 bird species. Finally, reptiles will be the most speciose vertebrate group after fish, said Uetz. The discoveries that are entered into the Reptile Database are made by taxonomists working in the field studying reptiles worldwide, and those working in museum collections or laboratories. Additionally, there is a lot of distribution and natural history data that needs to be entered, and then finally, all that data needs to be analyzed for various biological projects, including conservation efforts.

<hr>

[Visit Link](http://phys.org/news326118030.html){:target="_blank" rel="noopener"}


