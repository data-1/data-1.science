---
layout: post
title: "Air quality-monitoring satellite in orbit"
date: 2017-10-13
categories:
author: ""
tags: [Sentinel-5,Copernicus Programme,Satellite,Spaceflight,Space science,Spacecraft,Astronautics,Satellites,Flight,Outer space,Bodies of the Solar System,Space vehicles]
---


Applications Air quality-monitoring satellite in orbit 13/10/2017 13640 views 128 likes  The first Copernicus mission dedicated to monitoring our atmosphere, Sentinel‑5P, has been launched from the Plesetsk Cosmodrome in northern Russia. The first stage separated 2 min 16 sec after liftoff, followed by the fairing and second stage at 3 min 3 sec and 5 min 19 sec, respectively. Once completed after a few weeks, the cooler door will be opened and the calibration and validation of Sentinel-5P’s main Tropomi instrument will be performed. “The Sentinel-5P satellite is now safely in orbit so it is up to our mission control teams to steer this mission into its operational life and maintain it for the next seven years or more.” Sentinel-5P – the P standing for Precursor – is the first Copernicus mission dedicated to monitoring our atmosphere. Both missions will be carried on meteorological satellites operated by Eumetsat.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-5P/Air_quality-monitoring_satellite_in_orbit){:target="_blank" rel="noopener"}


