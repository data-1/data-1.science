---
layout: post
title: "First successful test of Einstein's general relativity near supermassive black hole (Update)"
date: 2018-07-28
categories:
author: ""
tags: [Very Large Telescope,European Southern Observatory,Black hole,S2 (star),General relativity,Sagittarius A,Theory of relativity,Astronomy,Space science,Physical sciences,Physics,Science,Astronomical objects,Astrophysics,Physical cosmology,Celestial mechanics]
---


Kornmesser  Observations made with ESO's Very Large Telescope have for the first time revealed the effects predicted by Einstein's general relativity on the motion of a star passing through the extreme gravitational field near the supermassive black hole in the centre of the Milky Way. This extreme environment—the strongest gravitational field in our galaxy—makes it the perfect place to explore gravitational physics, and particularly to test Einstein's general theory of relativity. New infrared observations from the exquisitely sensitive GRAVITY, SINFONI and NACO instruments on ESO's Very Large Telescope (VLT) have now allowed astronomers to follow one of these stars, called S2, as it passed very close to the black hole during May 2018. This is the first time that this deviation from the predictions of the simpler Newtonian theory of gravity has been observed in the motion of a star around a supermassive black hole. This is a perfect laboratory to test gravitational physics and specifically Einstein's general theory of relativity.

<hr>

[Visit Link](https://phys.org/news/2018-07-gravity-relativity-galactic-centre-massive.html){:target="_blank" rel="noopener"}


