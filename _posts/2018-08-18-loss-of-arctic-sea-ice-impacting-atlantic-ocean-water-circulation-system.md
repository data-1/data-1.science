---
layout: post
title: "Loss of Arctic sea ice impacting Atlantic Ocean water circulation system"
date: 2018-08-18
categories:
author: "Yale University"
tags: [Atlantic meridional overturning circulation,Hydrography,Environmental issues with fossil fuels,Earth sciences,Physical geography,Oceanography,Natural environment,Applied and interdisciplinary physics,Nature,Climate,Global environmental issues,Earth phenomena,Hydrology,Environmental science,Climate variability and change,Climate change,Physical oceanography,Global natural environment,Environmental impact]
---


But we have found another, overlooked, mechanism by which sea ice actively affects AMOC on multi-decadal time scales, said professor Alexey Fedorov, climate scientist at the Yale Department of Geology and Geophysics and co-author of a study detailing the findings in the journal Nature Climate Change. Sea ice loss is clearly important among the mechanisms that could potentially contribute to AMOC collapse. That is a significant amount, and it would accelerate the collapse of AMOC if it were to occur, Fedorov said. In the short-term, changes in the subpolar North Atlantic have the greatest impact on AMOC, the researchers found; but over the course of multiple decades, it was changes in the Arctic that became most important to AMOC, they said. We suggest that Arctic changes on a multi-decadal timescale, such as the decline in sea ice cover that we are currently experiencing, is the most efficient way to weaken the large-scale ocean circulation of the North Atlantic, which is responsible for the oceanic transport of heat from the equator to high latitudes, Sévellec said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-07/yu-loa073117.php){:target="_blank" rel="noopener"}


