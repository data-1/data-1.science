---
layout: post
title: "Tooth buried in bone shows prehistoric predators tangled across land, sea"
date: 2015-07-04
categories:
author: Virginia Tech 
tags: [Tooth,Phytosaur,Predation,Triassic,Rauisuchia,Paleontology,Animals,Taxa]
---


Phytosaurs were thought to be dominant aquatic predators because of their large size and similarity to modern crocodylians, said Stocker, but we were able to provide the first direct evidence they targeted both aquatic and large terrestrial prey. A tooth. No one had recognized the importance of this specimen before but we were able to borrow it and make our study. The large rauisuchid thigh bone at the center of the research has the tooth of the attacker, which the researchers recreated using CT scans and a 3-D printer. ###  The Paleobiology and Geobiology Research Group at Virginia Tech is ranked among the top 10 schools in the nation for paleontology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/vt-tbi092614.php){:target="_blank" rel="noopener"}


