---
layout: post
title: "Scientists combine satellite data and machine learning to map poverty"
date: 2017-03-22
categories:
author: "Stanford University"
tags: [Poverty,Machine learning,Research,Privacy,Science,Technology,Branches of science,Computing]
---


Stanford researchers combine high-resolution satellite imagery with powerful machine learning algorithms to predict poverty in Nigeria, Uganda, Tanzania, Rwanda and Malawi. In the current issue of Science, Stanford researchers propose an accurate way to identify poverty in areas previously void of valuable survey information. The researchers used machine learning - the science of designing computer algorithms that learn from data - to extract information about poverty from high-resolution satellite imagery. There are few places in the world where we can tell the computer with certainty whether the people living there are rich or poor, said study lead author Neal Jean, a doctoral student in computer science at Stanford's School of Engineering. Our paper demonstrates the power of machine learning in this context, said study co-author Stefano Ermon, assistant professor of computer science and a fellow by courtesy at the Stanford Woods Institute of the Environment.

<hr>

[Visit Link](http://phys.org/news/2016-08-scientists-combine-satellite-machine-poverty.html){:target="_blank" rel="noopener"}


