---
layout: post
title: "Ultra-high-speed 'electron camera' catches molecules at a crossroads: To break, or not to break"
date: 2018-07-12
categories:
author: "DOE/SLAC National Accelerator Laboratory"
tags: [Molecule,Chemical reaction,Optics,Quantum mechanics,Molecular physics,Nature,Atomic physics,Physics,Applied and interdisciplinary physics,Atomic molecular and optical physics,Physical chemistry,Physical sciences,Chemistry]
---


Now we've been able to observe directly for the first time how the atomic nuclei of a molecule rearrange at such an intersection. Ultra-High-Speed Snapshots of Atoms in Motion  The first steps in light-driven reactions are extremely fast. At SLAC, the researchers flashed laser light into a gas of trifluoroiodomethane molecules and observed over the course of hundreds of femtoseconds how bonds between carbon and iodine atoms elongated to a point at which the bond either broke, splitting off iodine from the molecules, or contracted, setting off vibrations of the atoms along the bond. Molecular states like these can be described by energy landscapes, with mountains of more energy and valleys of less energy. When the landscapes of different molecular states intersect, the reaction can proceed in several directions.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180705144240.htm){:target="_blank" rel="noopener"}


