---
layout: post
title: "Researchers tailor E. coli to convert plants into renewable chemicals"
date: 2018-05-21
categories:
author: "Sandia National Laboratories"
tags: [Enzyme,Lignin,Biofuel,Bacteria,Biorefinery,Fermentation,Technology]
---


Sandia National Laboratories scientists Seema Singh, left; and Fang Liu hold vials of vanillin and fermentation broth, which are critical for turning plant matter into biofuels and other valuable chemicals. For years, we've been researching cost-effective ways to break down lignin and convert it into valuable platform chemicals, Singh said. The first was cost. Our engineering turns the substrate toxicity problem on its head by enabling the very chemical that is toxic to the E. coli to initiate the complex process of lignin valorization. Once the vanillin in the fermentation broth activates the enzymes, the E. coli starts to convert the vanillin into catechol, our desired chemical, and the amount of vanillin never reaches a toxic level, Singh said.

<hr>

[Visit Link](https://phys.org/news/2018-05-tailor-coli-renewable-chemicals.html){:target="_blank" rel="noopener"}


