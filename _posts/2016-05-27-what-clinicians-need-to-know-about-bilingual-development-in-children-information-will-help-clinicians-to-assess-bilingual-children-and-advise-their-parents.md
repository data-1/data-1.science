---
layout: post
title: "What clinicians need to know about bilingual development in children: Information will help clinicians to assess bilingual children and advise their parents"
date: 2016-05-27
categories:
author: Florida Atlantic University
tags: [Multilingualism,Monolingualism,Language development,English language,Language,Child,Culture,Cognition,Applied linguistics,Branches of science,Cognitive psychology,Behavioural sciences,Branches of linguistics,Human communication,Education,Learning,Interdisciplinary subfields,Linguistics,Cognitive science,Communication,Psycholinguistics]
---


Underdiagnosis happens when a bilingual child scores below monolingual norms and the clinician overcorrects for the child's bilingualism, thereby failing to identify a child whose ability to acquire language is truly impaired. According to Hoff, there is a widely held but mistaken belief that children's ability to acquire language is such that once they get to school they will quickly reach the same level of English proficiency as their monolingual classmates, and therefore early exposure to English is not necessary. The data are clear that language input provided by nonnative speakers is less supportive of language development than input provided by native speakers. The data are clear than an optimal environment for English language development is exposure rich, grammatically varied English of the sort that is characteristic of educated, native English speakers. Conclusions on bilingual development of children in Hoff's article reveal that:

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150817110635.htm){:target="_blank" rel="noopener"}


