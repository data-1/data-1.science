---
layout: post
title: "Dark Energy Survey kicks off second season cataloging the wonders of deep space"
date: 2015-09-13
categories:
author: "$author" 
tags: [National Optical Astronomy Observatory,Dark Energy Survey,Astronomy,Space science,Physical sciences,Physics,Science,Astrophysics,Astronomical objects,Nature]
---


With its second year under way, the DES team posts highlights and prepares to release images from its first year  On Aug. 15, with its successful first season behind it, the Dark Energy Survey (DES) collaboration began its second year of mapping the southern sky in unprecedented detail. Using the Dark Energy Camera, a 570-megapixel imaging device built by the collaboration and mounted on the Victor M. Blanco Telescope in Chile, the survey's five-year mission is to unravel the fundamental mystery of dark energy and its impact on our universe. Scientists on the survey will use these images to unravel the secrets of dark energy, the mysterious substance that makes up 70 percent of the mass and energy of the universe. The first season was a resounding success, and we've already captured reams of data that will improve our understanding of the cosmos, said DES Director Josh Frieman of the U.S. Department of Energy's Fermi National Accelerator Laboratory and the University of Chicago. While results on the survey's probe of dark energy are still more than a year away, a number of scientific results have already been published based on data collected with the Dark Energy Camera.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/dnal-des081814.php){:target="_blank" rel="noopener"}


