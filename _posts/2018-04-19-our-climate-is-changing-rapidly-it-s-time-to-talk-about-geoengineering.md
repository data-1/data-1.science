---
layout: post
title: "Our Climate Is Changing Rapidly. It's Time to Talk About Geoengineering."
date: 2018-04-19
categories:
author: ""
tags: [Climate engineering,Climate change,Iron fertilization,Ocean,Atmosphere of Earth,Dead zone (ecology),Stratospheric sulfur aerosols,Earth,Stratospheric aerosol injection,Greenhouse effect,Algal bloom,Climate,Phytoplankton,Oceanography,Environmental impact,Earth sciences,Societal collapse,Human impact on the environment,Earth phenomena,Atmosphere,Climate variability and change,Applied and interdisciplinary physics,Nature,Natural environment,Physical geography]
---


To ameliorate the potential damage, some people suggest its time we start a new experiment on Earth, one we initiate on purpose to undo some of the damage wrought by climate change. Lou Patrick Mackay/Futurism  At best, these proposals are logistically challenging. The leading method among geoengineering proponents is stratospheric aerosol injection, a process in which reflective particles are released into the upper atmosphere. Therefore, this scheme would also require that we maintain this layer of reflective gas continuously, for decades, while also slashing our overall emissions. Climate: Divided  For most geoengineering plans to truly mitigate the effects of climate change, a one-off investment wouldn't cut it.

<hr>

[Visit Link](https://futurism.com/climate-change-geoengineering/){:target="_blank" rel="noopener"}


