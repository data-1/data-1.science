---
layout: post
title: "Acidic oceans: Action plan to combat changes to seawater chemistry: Global carbon dioxide emissions are acidifying waters of the North American West Coast at an accelerating rate; severe ecological co"
date: 2016-04-05
categories:
author: Ocean Science Trust
tags: [Ocean acidification,Ocean,Human impact on the environment,Climate variability and change,Societal collapse,Environmental impact,Global environmental issues,Applied and interdisciplinary physics,Environmental technology,Environmental issues with fossil fuels,Environmental science,Environmental social science,Environment,Climate change,Physical geography,Oceanography,Environmental issues,Natural environment,Earth sciences,Nature]
---


Global carbon dioxide emissions are triggering permanent and alarming changes to ocean chemistry along the North American West Coast that require immediate, decisive action to combat, including development of a coordinated regional management strategy, a panel of scientific experts has unanimously concluded. Increases in atmospheric carbon dioxide emissions from human activities are not just responsible for global climate change; these emissions also are being absorbed by the world's oceans, said Dr. Alexandria Boehm, co-chair of the Panel and a Professor of Civil and Environmental Engineering at Stanford University. advertisement  Although ocean acidification is a global problem that will require global solutions, the Panel deliberately focused its recommendations around what West Coast ocean management and natural resource agencies can do collectively to combat the challenge at the regional level. West Coast policymakers will use the Panel's recommendations to continue to advance management actions aimed at combatting ocean acidification and hypoxia. It is crucial that we comprehend how ocean chemistry is changing in different places, so we applaud the steps the West Coast Ocean Acidification and Hypoxia Science Panel has put forward in understanding and addressing this issue.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/04/160404090551.htm){:target="_blank" rel="noopener"}


