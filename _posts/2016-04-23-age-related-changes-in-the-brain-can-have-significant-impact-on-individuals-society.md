---
layout: post
title: "Age-related changes in the brain can have significant impact on individuals, society"
date: 2016-04-23
categories:
author: National Academies of Sciences, Engineering, and Medicine
tags: [Health,Ageing,Brain training,Cognition,Dietary supplement,Aging brain,Neuroscience,Cognitive science,Medicine,Health sciences,Health care]
---


As they get older, individuals of all ages should take the following three steps to help promote cognitive health:  Be physically active. To provide necessary assistance and support to older adults, the committee called for the development of cognitive aging information resources and tools that can help individuals and families. Programs and services used by older adults, including those in financial institutions and departments of motor vehicles, should be improved to help them avoid exploitation, optimize independence, and make sound decisions. By calling attention to this issue, we can learn more about the risk and protective factors and needed research so older adults can better maintain their cognitive health to the fullest extent possible. (vice chair)  Scola Endowed Chair,  Professor of Psychiatry, Neurology, and Epidemiology  University of California  San Francisco  Marilyn Albert, Ph.D.  Director, Division of Cognitive Neuroscience, and  Professor of Neurology  Johns Hopkins University School of Medicine  Baltimore  Sara J. Czaja, Ph.D.  Leonard M. Miller Professor of Psychiatry and Behavioral Sciences and  Scientific Director, Center on Aging  University of Miami  Miami  Donna Fick, R.N., Ph.D., FGSA, FAAN  Distinguished Professor of Nursing, and  Co-Director  Hartford Center of Geriatric Nursing Excellence  Pennsylvania State University  University Park  Lisa Gwyther, M.S.W., L.C.S.W.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/naos-aci041415.php){:target="_blank" rel="noopener"}


