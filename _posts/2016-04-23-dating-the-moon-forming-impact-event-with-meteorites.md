---
layout: post
title: "Dating the moon-forming impact event with meteorites"
date: 2016-04-23
categories:
author: University Of Arizona
tags: [Impact event,Giant-impact hypothesis,Asteroid,Moon,Meteorite,Asteroid belt,Chondrite,Planetary science,Astronomy,Solar System,Science,Outer space,Space science,Astronomical objects,Physical sciences,Local Interstellar Cloud,Bodies of the Solar System]
---


Credit: Vishnu Reddy, Planetary Science Institute  Through a combination of data analysis and numerical modeling work, researchers have found a record of the ancient Moon-forming giant impact observable in stony meteorites. The work was done by NASA Solar System Exploration Research Virtual Institute (SSERVI) researchers led by Principal Investigator Bill Bottke of the Institute for the Science of Exploration Targets (ISET) team at the Southwest Research Institute and included Tim Swindle, director of the University of Arizona's Lunar and Planetary Laboratory. Numerical simulations of the giant impact indicate this event not only created a disk of debris near Earth that formed the Moon, but it also ejected huge amounts of debris completely out of the Earth-Moon system. Evidence that the giant impact produced a large number of kilometer-sized fragments can be inferred from laboratory and numerical impact experiments, the ancient lunar impact record itself, and the numbers and sizes of fragments produced by major main belt asteroid collisions. The most ancient Solar System materials found in meteorites are about one hundred million years older than this age.

<hr>

[Visit Link](http://phys.org/news348409541.html){:target="_blank" rel="noopener"}


