---
layout: post
title: "The Science of How Our Minds and Our Bodies Converge in the Healing of Trauma"
date: 2016-06-24
categories:
author: "Maria Popova"
tags: [Psychological trauma,Attachment theory,Emotion,Post-traumatic stress disorder,Experience,Sense,Mind,Concepts in metaphysics,Behavioural sciences,Psychological concepts,Cognitive science,Psychology]
---


Trauma and its psychological consequences, Van der Kolk argues, is not a mental disease but an adaptation. Education, Van der Kolk notes, tends to engage the cognitive capacities of the mind rather than the bodily-emotional engagement system, which makes for an ultimately incomplete model of human experience. People’s lives will be held hostage to fear until that visceral experience changes… Self-regulation depends on having a friendly relationship with your body. In order to change, people need to become aware of their sensations and the way that their bodies interact with the world around them. Another paradox of healing is that although contact and connection are often terrifying to the traumatized, social support and a sense of community are the foundation upon which a health relationship with our own feelings and sensations is built.

<hr>

[Visit Link](https://www.brainpickings.org/2016/06/20/the-body-keeps-the-score-van-der-kolk/){:target="_blank" rel="noopener"}


