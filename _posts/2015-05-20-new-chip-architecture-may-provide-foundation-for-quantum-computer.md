---
layout: post
title: "New chip architecture may provide foundation for quantum computer"
date: 2015-05-20
categories:
author: ""    
tags: [Integrated circuit,Quantum computing,Quantum mechanics,Computing,Ball grid array,Qubit,Computer,Technology,Electrical engineering,Electronics,Science,Branches of science]
---


Such simulations could revolutionize chemistry, biology and material science, but the development of quantum computers has been limited by the ability to increase the number of quantum bits, or qubits, that encode, store and access large amounts of data. This is why it's impossible to fully simulate even a modest sized quantum system, let alone something like chemistry of complex molecules, unless we can build a quantum computer to do it. The scalability of current trap architectures is limited since the connections for the electrodes needed to generate the trapping fields come at the edge of the chip, and their number are therefore limited by the chip perimeter. The ball grid array's key feature is that it can bring electrical signals directly from the backside of the mount to the surface, thus increasing the potential density of electrical connections. The BGA project demonstrated that it's possible to fit more and more electrodes on a surface trap chip while wiring them from the back of the chip in a compact and extensible way.

<hr>

[Visit Link](http://www.spacedaily.com/reports/New_chip_architecture_may_provide_foundation_for_quantum_computer_999.html){:target="_blank" rel="noopener"}


