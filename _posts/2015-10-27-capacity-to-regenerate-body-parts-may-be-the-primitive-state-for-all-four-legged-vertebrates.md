---
layout: post
title: "Capacity to regenerate body parts may be the primitive state for all four-legged vertebrates"
date: 2015-10-27
categories:
author: Brown University
tags: [Regeneration (biology),Tetrapod,Salamander,Amphibian,Vertebrate,News aggregator,Animals,Vertebrate zoology]
---


A team of paleontologists of the Museum für Naturkunde Berlin, the State University of New York at Oswego and Brown University shows in a new study of fossil amphibians that the extraordinary regenerative capacities of modern salamanders are likely an ancient feature of four-legged vertebrates that was subsequently lost in the course of evolution. Salamanders are extraordinary among modern four-legged vertebrates in showing an astonishing capacity to regenerate limbs, tails, and internal organs that were injured or lost due to amputation repeatedly and throughout their entire lifespan. New data from the fossil record offers a new perspective on the evolution of the enormous regenerative capacities of modern salamanders. We were able to show salamander-like regenerative capacities in both -- fossil groups that develop their limbs like the majority of modern four-legged vertebrates as well in groups with the reversed pattern of limb development seen in modern salamanders, said Dr. Jennifer Olori of State University of New York at Oswego, co-author on the study. The fossil record shows that the form of limb development of modern salamanders and the high regenerative capacities are not something salamander-specific, but instead were much more wide spread and may even represent the primitive condition for all four-legged vertebrates says Nadia Fröbisch.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151026125031.htm){:target="_blank" rel="noopener"}


