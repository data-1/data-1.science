---
layout: post
title: "What is fiber?"
date: 2016-06-14
categories:
author: "Jessie Szalay"
tags: [Dietary fiber,Low-fiberlow-residue diet,Whole grain,Glycated hemoglobin,Carbohydrate,Self-care,Health sciences,Food and drink,Clinical medicine,Medical specialties,Nutrition,Health]
---


Most plant-based foods contain both soluble and insoluble fiber, but the amounts of each vary in different foods. A more recent animal study (opens in new tab) suggested that fiber might only cause this benefit if a person possesses the right kind and amount of gut bacteria. While Smathers reported that supplements are not as good as fiber from whole foods, fiber supplements can be helpful for people looking to regulate their bowel movements or who suffer from constipation. How to eat a high-fiber diet  In order to get all the benefits of fiber, many people adopt a high-fiber diet. People on a low-fiber diet should avoid high-fiber foods that make the intestinal tract work harder, like legumes, beans, whole grains and many raw or fried vegetables and fruits, according to the National Institutes of Health (opens in new tab) (NIH).

<hr>

[Visit Link](http://www.livescience.com/51998-dietary-fiber.html){:target="_blank" rel="noopener"}


