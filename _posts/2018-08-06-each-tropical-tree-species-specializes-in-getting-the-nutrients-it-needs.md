---
layout: post
title: "Each tropical tree species specializes in getting the nutrients it needs"
date: 2018-08-06
categories:
author: "Smithsonian Tropical Research Institute"
tags: [Soil,Reforestation,Biodiversity,Tree,Nitrogen fixation,Smithsonian Tropical Research Institute,Root,Climate change mitigation,Nutrient,Natural environment,Ecosystem,Nature,Environmental social science,Natural resource management,Environmental conservation,Natural resources,Systems ecology,Ecology,Physical geography,Environmental science,Biogeochemistry,Earth sciences]
---


A super-sized experiment at the Smithsonian Tropical Research Institute (STRI) to address this paradox showed that each species has its own unique nutrient-capture strategies, underscoring the importance of biodiversity for successful reforestation projects. People speculated that nitrogen-fixing species might channel extra nitrogen into making the phosphatase enzyme to capture phosphorus, said Jefferson Hall, director of the Smithsonian's Panama Canal watershed experiment--the Agua Salud Project. We were hoping to find evidence for the nutrient trading hypothesis--that nitrogen fixers invest in nitrogen-rich phosphatase enzymes, which would resolve the paradox of why there are more nitrogen-fixing trees in these nitrogen-rich tropical forest soils, Batterman said. So then we considered the nutrient balance hypothesis--that trees adjust their nutrient-capture strategies to satisfy their needs--fixing more nitrogen in nitrogen-poor soils, making more phosphatase in phosphorus-poor soils. The lead author also received support from Princeton University, a STRI short-term fellowship program and a United Kingdom Natural Environment Research Council grant.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/stri-ett080318.php){:target="_blank" rel="noopener"}


