---
layout: post
title: "New Horizons only one year from Pluto (w/ Video)"
date: 2015-07-14
categories:
author: Dr. Tony Phillips
tags: [New Horizons,Pluto,Solar System,Planets,Science,Planets of the Solar System,Astronautics,Planemos,Astronomical objects,Spaceflight,Astronomy,Space science,Outer space,Bodies of the Solar System,Planetary science]
---


Credit: NASA  In July 2015, NASA will discover a new world. On July 14th, 2015, NASA's New Horizons spacecraft will make a close flyby of that distant world. At closest approach in July 2015, New Horizons will be a scant 10,000 km above the surface of Pluto. During the approach to Pluto, the science team will keep a wary eye out for debris, and guide the spacecraft away from danger. Think about seeing something for the first time and discovering the unknown, she concludes.

<hr>

[Visit Link](http://phys.org/news324634423.html){:target="_blank" rel="noopener"}


