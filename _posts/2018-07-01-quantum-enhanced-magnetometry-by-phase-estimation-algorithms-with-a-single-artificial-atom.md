---
layout: post
title: "Quantum-enhanced magnetometry by phase estimation algorithms with a single artificial atom"
date: 2018-07-01
categories:
author: "Danilin, Low Temperature Laboratory, Department Of Applied Physics, Aalto University School Of Science, Aalto, Lebedev, A. V., Theoretische Physik, Zürich, Moscow Institute Of Physics"
tags: []
---


Here, Φ i = (i − 1)[δΦ] step + Φ 1 with the index i chosen from the flux-index set I 0 = [1, 161] [δΦ] step ≃ 1.59 × 10−5 Φ 0 , Φ 1 ≃ 0.137 Φ 0 , and discrete time delays τ j = (j − 1) × 2 ns, j = 1, … 241 quantifying the time separation between the two π/2 rf-pulses of the Ramsey sequence. Quantum Fourier algorithm  This algorithm starts from the Ramsey measurement with an optimal time delay τ(s)~T 2 . The Fourier algorithm starts from the Ramsey measurement at large delay τ(s) = 360 ns, with the first step returning a probability distribution with six out of twelve flux intervals assuming a non-vanishing value. The distributions obtained at the step number 4 for the Kitaev and Fourier estimation algorithms and in the standard (classical) measurement are shown in the inset Full size image  Results  The superiority of our quantum metrological algorithms is clearly demonstrated by the scaling behavior of the magnetic flux resolution with the total sensing time of the flux measurement, see Fig. The magnetic flux sensitivities A quant range within 5.6–7.1 × 10−6 Φ 0 Hz−1/2 for the Kitaev algorithm and within 6.5–8.5 × 10−6 Φ 0 Hz−1/2 for the Fourier procedure.

<hr>

[Visit Link](http://www.nature.com/articles/s41534-018-0078-y?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


