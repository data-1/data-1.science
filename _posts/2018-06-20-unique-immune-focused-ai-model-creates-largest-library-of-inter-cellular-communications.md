---
layout: post
title: "Unique immune-focused AI model creates largest library of inter-cellular communications"
date: 2018-06-20
categories:
author: "CytoReason"
tags: [Artificial intelligence,Machine learning,System,Communication,Science,Cognitive science,Technology,Branches of science]
---


Tel Aviv -- June 18, 2018 -- New data published in Nature Biotechnology, represents the largest ever analysis of immune cell signaling research, mapping more than 3,000 previously unlisted cellular interactions, and yielding the first ever immune-centric modular classification of diseases. These data serve to rewrite the reference book on immune-focused inter-cellular communications and disease relationships. These alone, represent discoveries born out of a better contextual understanding of existing immune system knowledge. This potential becomes even more powerful when such knowledge can be integrated with other rich data sources and AI technologies to generate significant new clues in the fight against disease. This work, combined with our Cell-Centred Model, doesn't just describe what happens between the cells etc, but also defines who initiates and who acts on it - this is the key to the uniquely 3-dimensional view of the immune system that CytoReason builds.”  ###  About CytoReason  Born out of 10 years' research from Stanford and the Technion, CytoReason is the only AI company to focus entirely on the immune system in developing its proprietary data and AI / machine-learning approach.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/c-uia061818.php){:target="_blank" rel="noopener"}


