---
layout: post
title: "Heightened scents: Do ambient fragrances make consumers purchase more?"
date: 2016-04-12
categories:
author: American Marketing Association
tags: [Odor,Aroma compound,Psychological concepts,Psychology,Cognitive science,Neuroscience,Cognition]
---


A new study in the Journal of Marketing shows for the first time that the temperature of scents in a store atmosphere may have a powerful effect on what and how much customers buy. People smelling warm fragrances such as cinnamon feel that the room they are in is more crowded, and feel less powerful as a result, write authors Adriana V. Madzharov (Stevens Institute of Technology), Lauren G. Block (City University of New York), and Maureen Morrin (Temple University). The people in the warm, crowded room felt less powerful as a result of the perceived crowding, and were more likely to compensate for this loss of power by buying items which they felt were prestigious and which helped raise their personal status. This study, to the best of our knowledge, is the first to show how fragrance in the environment can affect how we feel about the space surrounding us, and how that in turn can drive customers to choose prestigious products. The Cool Scent of Power: Effects of Ambient Scent on Consumer Preferences and Choice Behavior.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/ama-hsd012115.php){:target="_blank" rel="noopener"}


