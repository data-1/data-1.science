---
layout: post
title: "Historic Delft Experiments tests Einstein's 'God does not play dice' using quantum 'dice'"
date: 2015-10-22
categories:
author: Icfo-The Institute Of Photonic Sciences
tags: [Quantum entanglement,Principle of locality,Electron,Randomness,Hidden-variable theory,Experiment,Faster-than-light,Science,Theoretical physics,Quantum mechanics,Scientific method,Scientific theories,Physics]
---


As described in Hanson's group web the Delft experiment first entangled two electrons trapped inside two different diamond crystals, and then measured the electrons' orientations. Moreover, the measurements were made so quickly that there wasn't time for the electrons to communicate, not even with signals traveling at the speed of light. This puts local realism in a very tight spot: if the electron orientations are real, the electrons must have communicated. With the help of ICFO's quantum random number generators, the Delft experiment gives a nearly perfect disproof of Einstein's world-view, in which nothing travels faster than light and God does not play dice. Credit: ICFO  The fastest quantum random number generator to date.

<hr>

[Visit Link](http://phys.org/news/2015-10-historic-delft-einstein-god-dice.html){:target="_blank" rel="noopener"}


