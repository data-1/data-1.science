---
layout: post
title: "The gas (and ice) giant Neptune"
date: 2015-09-14
categories:
author: Matt Williams
tags: [Neptune,Irregular moon,Triton (moon),Moons of Neptune,Natural satellite,Planet,Rings of Neptune,Solar System,Kuiper belt,Atmosphere,Ring system,Uranus,Physical sciences,Bodies of the Solar System,Planets,Outer space,Astronomical objects,Planets of the Solar System,Outer planets,Space science,Gas giants,Ice giants,Astronomy,Planemos,Planetary science]
---


Size, Mass and Orbit:  With a mean radius of 24,622 ± 19 km, Neptune is the fourth largest planet in the Solar System and four times as large as Earth. Neptune's irregular moons consist of the planet's remaining satellites (including Triton). With the exception of Triton and Nereid, Neptune's irregular moons are similar to those of other giant planets and are believed to have been gravitationally captured by Neptune. Credit: NASA/JPL  Ring System:  Neptune has five rings, all of which are named after astronomers who made important discoveries about the planet – Galle, Le Verrier, Lassell, Arago, and Adams. This is similar to the rings of Uranus, but very different to the icy rings around Saturn.

<hr>

[Visit Link](http://phys.org/news/2015-09-gas-ice-giant-neptune.html){:target="_blank" rel="noopener"}


