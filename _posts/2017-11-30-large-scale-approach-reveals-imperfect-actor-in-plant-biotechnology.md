---
layout: post
title: "Large-scale approach reveals imperfect actor in plant biotechnology"
date: 2017-11-30
categories:
author: "Whitehead Institute for Biomedical Research"
tags: [Glufosinate,Protein,Enzyme,Biology,Life sciences,Biotechnology,Biochemistry,Chemistry,Nature]
---


(Nov. 27) - A research team led by Whitehead Institute for Biomedical Research has harnessed metabolomic technologies to unravel the molecular activities of a key protein that can enable plants to withstand a common herbicide. As he and his colleagues searched for an explanation, they narrowed in on the source: an enzyme, called BAR, that was engineered into the plants as a kind of chemical beacon, enabling scientists to more readily study them. But BAR is more than just a tool for scientists. That explains why they can detect the unintended products (acetyl-tryptophan and acetyl-aminoadipate) in crops genetically engineered to carry BAR, such as soybeans and canola. Whitehead Institute for Biomedical Research, Cambridge, MA 02142, USA  2.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-11/wifb-lar112717.php){:target="_blank" rel="noopener"}


