---
layout: post
title: "Muon machine makes milestone magnetic map"
date: 2018-06-21
categories:
author: "Fermi National Accelerator Laboratory"
tags: [Muon g-2,Muon,Applied and interdisciplinary physics,Particle physics,Physical sciences,Science,Physics]
---


Muon mystery  Muon g-2 is following up on an intriguing result seen at Brookhaven National Laboratory in New York in the early 2000s, when the experiment made observations of muons that didn't match with theoretical predictions. By measuring this precession, it is possible to precisely extract the value of g.  The strength of magnetic field to which the muons are exposed directly affects how they precess, so it's absolutely crucial to make extremely precise measurements of the field strength and maintain its uniformity throughout the ring – not an easy task. All known particles do this, but their total effect doesn't quite account for Brookhaven's results. Probing the field  The magnetic field strength measurements are made using small, sensitive electronic devices called probes. The trolley probes are themselves calibrated by a plunging probe, which can move in and out of its own chamber at a specific location in the ring when needed.

<hr>

[Visit Link](https://phys.org/news/2018-01-muon-machine-milestone-magnetic.html){:target="_blank" rel="noopener"}


