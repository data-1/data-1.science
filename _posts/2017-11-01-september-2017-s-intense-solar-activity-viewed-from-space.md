---
layout: post
title: "September 2017's intense solar activity viewed from space"
date: 2017-11-01
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Coronal mass ejection,Solar flare,Sun,Stellar corona,Solar Dynamics Observatory,Space weather,Solar phenomena,STEREO,Solar and Heliospheric Observatory,Solar Radiation and Climate Experiment,Ancient astronomy,Outer space,Astronomical objects,Nature,Space science,Plasma physics,Astrophysics,Physical sciences,Physical phenomena,Stellar astronomy,Astronomy,Bodies of the Solar System,Solar System,Space plasmas,Astronomical objects known since antiquity]
---


Effects from September's solar activity were observed as Martian aurora and across the globe on Earth, in the form of events known as ground-level enhancements -- showers of neutrons detected on the ground, produced when energetic particles accelerated by a solar eruption stream along Earth's magnetic field lines and flood the atmosphere. Credit: NOAA/GOES  SDO  NASA's Solar Dynamics Observatory watches the corona at 10 different wavelengths on a 12-second cadence, enabling scientists to track highly dynamic events on the Sun such as these X2.2 and X9.3 solar flares. The X9.3 flare was the most intense flare recorded during the current solar cycle. On Sept. 9, 2017, STEREO watched a CME erupt from the Sun. While the Sun produced high levels of extreme ultraviolet light, SORCE actually detected a dip in total irradiance during the month's intense solar activity.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/nsfc-s2i102717.php){:target="_blank" rel="noopener"}


