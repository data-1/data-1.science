---
layout: post
title: "What Trees Teach Us About Human Nature, Relationships, and the Secret to Lasting Love: Wisdom from a 17th-Century Gardener"
date: 2015-07-10
categories:
author: Maria Popova
tags: [Love,Virtue]
---


Since the dawn of time, trees — the oldest living things in our world — have been our silent companions, which we’ve transmuted into the myths and metaphors through which we make sense of the world — from their deity-like role in ancient Indian legends to their long history as the perfect visual metaphor for visualizing human knowledge to their symbolic representation of the cycle of life. Waltham Abbey,  September 26, 1847  In the original 1653 “Preface to the Reader,” Austen vows to “endeavour to make some spiritual use, and improvement of [fruit trees]” and writes:  When we have gone through all the works and labours to be performed in the orchard, and have received thereby a rich recompense of temporal profits and pleasures in the use of the trees and fruits, we may (besides all that) make a spiritual use of them, and receive more and greater profits and pleasures thereby. Trees, he assures us, contain great gospels of truth:  The world is a great library, and fruit trees are some of the books wherein we may read and see plainly the attributes of God, his power, wisdom, goodness &c. … for as trees (in a metaphorical sense)* are books, so like-wise in the same sense they have a voice, and speak plainly to us, and teach us many good lessons. And when we (after a serious search) do make some use and result of what we see in them, when we collect something from them concerning the power, wisdom, goodness, and perfections of God, or our duty to God, that is the answer of the fruit trees; then fruit trees speak to the mind, and tell us many things, and teach us many good lessons. If that love flows according to that likeness of natures, then let this teach us to strive for increase of grace…  Austen seems to remind us, too, that lasting, nourishing relationships are daily work:  Every act of grace adds something to the habit, so that the habits of grace are mightily confirmed by their frequent operations.

<hr>

[Visit Link](http://www.brainpickings.org/2015/06/11/the-spiritual-uses-of-fruit-trees-relationship-advice-and-wisdom-on-human-nature-from-a-17th-century-gardener/){:target="_blank" rel="noopener"}


