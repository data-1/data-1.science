---
layout: post
title: "Pumpkin genomes sequenced, revealing uncommon evolutionary history"
date: 2017-10-31
categories:
author: Boyce Thompson Institute
tags: [Cucurbita,Plant breeding,Genome,Polyploidy,Pumpkin,Ploidy,Whole genome sequencing,Genetics,Biology]
---


The researchers sequenced the two different pumpkin species to better understand their contrasting desirable traits: Cucurbita moschata is known for its resistance to disease and other stresses, such as extreme temperatures, while C. maxima is better known for its fruit quality and nutrition. While the ultimate goal for genome sequencing is to be able to link specific genes to the traits they control, the pumpkin sequencing results also revealed an interesting evolutionary history for Cucurbita species. Although the pumpkin is considered a diploid today, meaning that it has only two copies of each chromosome, the genome sequence analysis revealed that between 3-20 million years ago, two different ancestral species combined their genomes to create an allotetraploid - a new species with four (tetra-) copies of each chromosome, from two different (allo-) species. The ancient Cucurbita allotetraploid lost its duplicated genes randomly from both of the contributing diploids. The next time you carve a pumpkin, take a moment to think about the curious evolutionary path it took to get here, and how breeders, now armed with the genome sequence, will be better able to improve the pumpkin to help feed millions around the world.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/bti-pgs102917.php){:target="_blank" rel="noopener"}


