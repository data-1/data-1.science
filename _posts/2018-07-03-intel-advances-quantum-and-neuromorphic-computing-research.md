---
layout: post
title: "Intel advances quantum and neuromorphic computing research"
date: 2018-07-03
categories:
author: ""
tags: [Quantum computing,Neuromorphic engineering,Cognitive computer,Computing,Integrated circuit,Intel,Artificial intelligence,Qubit,Semiconductor device fabrication,Information Age,Technology,Branches of science,Computer science,Applied mathematics]
---


Intel Corporations self-learning neuromorphic research chip, code-named Loihi. Credit: Intel Corporation  Today at the 2018 Consumer Electronics Show in Las Vegas, Intel announced two major milestones in its efforts to research and develop future computing technologies including quantum and neuromorphic computing, which have the potential to help industries, research institutions and society solve problems that currently overwhelm today's classical computers. Scaling the Quantum Computing System  Today, two months after delivery of a 17-qubit superconducting test chip, Intel unveiled Tangle Lake, a 49-qubit superconducting quantum test chip. In fact, Intel has already invented a spin qubit fabrication flow on its 300mm process technology. The Promise of Neuromorphic Computing  Krzanich also showcased Intel's research into neuromorphic computing – a new computing paradigm inspired by how the brain works that could unlock exponential gains in performance and power efficiency for the future of artificial intelligence.

<hr>

[Visit Link](https://phys.org/news/2018-01-intel-advances-quantum-neuromorphic.html){:target="_blank" rel="noopener"}


