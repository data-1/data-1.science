---
layout: post
title: "Agriculture, declining mobility drove humans' shift to lighter bones"
date: 2016-05-02
categories:
author: Johns Hopkins University School Of Medicine
tags: [Human,Bone,Osteoporosis,Privacy]
---


Cross-sections of an Upper Paleolithic, left, and Early Medieval, right, thigh bone, showing the change in bone shape and reduction in strength in the later individual. Credit: Study authors  Modern lifestyles have famously made humans heavier, but, in one particular way, noticeably lighter weight than our hunter-gatherer ancestors: in the bones. Now a new study of the bones of hundreds of humans who lived during the past 33,000 years in Europe finds the rise of agriculture and a corresponding fall in mobility drove the change, rather than urbanization, nutrition or other factors. When they analyzed the geometry of bones over time, the researchers found a decline in leg bone strength between the Mesolithic era, which began about 10,000 years ago, and the age of the Roman Empire, which began about 2,500 years ago. The difference in bone strength between a professional tennis player's arms is about the same as that between us and Paleolithic humans, he says.

<hr>

[Visit Link](http://phys.org/news351169990.html){:target="_blank" rel="noopener"}


