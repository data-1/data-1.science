---
layout: post
title: "Astronauts' skin changes before and after missions are studied"
date: 2016-05-15
categories:
author: Nancy Owano
tags: [Astronaut,NASA,Spaceflight,Science]
---


This May 2010 NASA handout image shows a NASA astronaut participating in the mission's first session of extravehicular activity (EVA) as construction and maintenance continue on the International Space Station. According to recent reports, scientists have found that skin of astronauts who spend a lot of time in space gets thinner. The scientists said that the epidermis got thinner by almost 20 percent than it usually is, said Moceri. So with these two signals we can build up images and get a precise look into the skin with a high resolution. So far we have no explanation yet, and we are waiting for the other astronauts to figure out what's going on and maybe to try to figure out how we can protect, how we can help so that this epidermis is not shrinking, said Koenig in the Reuters report.

<hr>

[Visit Link](http://phys.org/news/2015-07-astronauts-skin-missions.html){:target="_blank" rel="noopener"}


