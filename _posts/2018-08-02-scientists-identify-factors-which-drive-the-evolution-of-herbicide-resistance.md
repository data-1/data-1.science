---
layout: post
title: "Scientists identify factors which drive the evolution of herbicide resistance"
date: 2018-08-02
categories:
author: "University of Sheffield"
tags: [Pesticide resistance,Herbicide,Agriculture,Evolution,University of Sheffield,Biodiversity,Weed,Biology]
---


Lead author of the study Rob Freckleton, Professor of Population Biology from the University of Sheffield, said: The driver for this spread is evolved herbicide resistance: we found that weeds in fields with higher densities are more resistant to herbicides. The research offer important insights into diversifying management which is suggested as a possible technique for reducing the evolution of resistance. The study showed the technique will work to reduce resistance only if farmers reduce their inputs of herbicides. The new findings show the volume and diversity of herbicide products are positively related to each other. ###  For further information please contact: Amy Huxtable, Media Relations Officer, University of Sheffield, 0114 222 9859 Notes to editors The University of Sheffield  With almost 29,000 of the brightest students from over 140 countries, learning alongside over 1,200 of the best academics from across the globe, the University of Sheffield is one of the world's leading universities.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/uos-sif021318.php){:target="_blank" rel="noopener"}


