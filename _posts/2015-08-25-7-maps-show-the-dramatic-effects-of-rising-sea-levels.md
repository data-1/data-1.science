---
layout: post
title: "7 maps show the dramatic effects of rising sea levels"
date: 2015-08-25
categories:
author: Colm Gorey, Colm Gorey Was A Senior Journalist With Silicon Republic
tags: [Sea,Earth sciences,Physical oceanography,Physical geography,Geography,Applied and interdisciplinary physics,Hydrography,Hydrology,Oceanography]
---


With the news that Washington DC appears doomed to be submerged in the years to come with the onset of rising sea levels, here are 7 maps showing how even a small rise will see cities being submerged. Over the past 60 years, the area around Washington DC known as Chesapeake has been shown to be significantly worse off when it comes to rising sea levels with a rate twice as fast as much of the east coast of the US. If the calculations are correct, the Washington DC area could drop six inches by 2100, resulting in a 3ft rise in sea levels. Washington DC  Taking into account the sinking of Washington DC, the sea levels seen in the city will bring it right to the door of the US president at The White House. Vast areas of the city and surrounding area will be submerged by a 3m rise, almost similar in scale to Amsterdam.

<hr>

[Visit Link](https://www.siliconrepublic.com/earth-science/2015/07/31/7-maps-show-rising-sea-levels){:target="_blank" rel="noopener"}


