---
layout: post
title: "How to successfully enter the FOSS emerging market"
date: 2017-09-24
categories:
author: "VM (Vicky) Brasseur
(Alumni)"
tags: [Market (economics),Liminality,Free and open-source software,Internet,Red Hat,Innovation,Communication,Business]
---


Operations in a liminal space, such as the meeting of an emerging market and a business, are not as constrained by a business as usual mentality; therefore, they are free to explore new solutions. Being very familiar with and confident in how things are done, organizations enter emerging markets with a business-as-usual attitude. This business-as-usual approach leads to expectation mismatch. While those of us on the management and business side of things learn how we enter the emerging market of FOSS, there's no reason FOSS communities, projects, and supporters can't do a little to help out our end. For a successful entry into this emerging market, a business must learn the FOSS language, culture, and needs.

<hr>

[Visit Link](https://opensource.com/article/17/1/cultivating-business-foss-market){:target="_blank" rel="noopener"}


