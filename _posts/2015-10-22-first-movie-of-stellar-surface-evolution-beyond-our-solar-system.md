---
layout: post
title: "First movie of stellar-surface evolution beyond our Solar System"
date: 2015-10-22
categories:
author: Leibniz-Institut für Astrophysik Potsdam (AIP)
tags: [Star,Solar cycle,Stellar magnetic field,Sun,Sunspot,Leibniz Institute for Astrophysics Potsdam,Physical sciences,Astronomy,Physical phenomena,Science,Space science,Stellar astronomy]
---


Astronomers from the Leibniz Institute for Astrophysics Potsdam (AIP), present for the first time a movie that shows the evolution of stellar spots on a star other than our Sun. In order to not only image but also to reconstruct the evolution of star spots, well-sampled time series of high-resolution spectra are needed. Such long-term, highly-sampled, phase-resolved spectroscopic data for Doppler imaging of the red giant star XX Tri presented here were made possible with the STELLA robotic telescopes on Tenerife over an observing period of 6 years and are continued. The decay rate of (magnetic) star spots is of great interest as it is directly related to the magnetic diffusivity in the convective layer of the star, which itself is a key quantity for the length of a magnetic-activity cycle. STELLA was built at AIP and is a long-term project for observing and monitoring activity tracers on cool stars.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151020091659.htm){:target="_blank" rel="noopener"}


