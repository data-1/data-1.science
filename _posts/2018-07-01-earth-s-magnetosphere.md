---
layout: post
title: "Earth's magnetosphere"
date: 2018-07-01
categories:
author: "Science Nasa"
tags: [Magnetosphere,Earth,Sun,Solar wind,Space weather,Mars,Planet,Atmosphere,Solar System,Bodies of the Solar System,Science,Space science,Astronomy,Outer space,Sky,Physical sciences,Nature,Spaceflight,Planetary science]
---


Without the magnetosphere, the relentless action of these solar particles could strip the Earth of its protective layers, which shield us from the Sun's ultraviolet radiation. The Solar wind is thought to have stripped away most of Mars' atmosphere, possibly after the red planet's magnetic field dissipated. Eftyhia Zesta of the Geospace Physics Laboratory at NASA's Goddard Space Flight Center notes, If there were no magnetic field, we might have a very different atmosphere left without life as we know it. Credit: Science@NASA  Zesta says, The Earth's magnetosphere absorbs the incoming energy from the solar wind, and explosively releases that energy in the form of geomagnetic storms and substorms. NASA's Magnetospheric Multiscale Mission, or MMS, was launched in March 2015 to observe the electron physics of magnetic reconnection for the first time.

<hr>

[Visit Link](https://phys.org/news/2018-03-earth-magnetosphere.html){:target="_blank" rel="noopener"}


