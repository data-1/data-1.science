---
layout: post
title: "Pigments, organelles persist in fossil feathers"
date: 2015-09-02
categories:
author: Brown University 
tags: [Melanosome,Feather,Melanin,Anchiornis,Dinosaur]
---


PROVIDENCE, R.I. [Brown University] -- A study provides multiple lines of new evidence that pigments and the microbodies that produce them can remain evident in a dinosaur fossil. The idea that melanosomes, which produce melanin pigment, are preserved in fossils has been hotly debated among scientists during the last several years. That morphological evidence alone, however, would not advance the debate, so in addition the team performed two different kinds of chemical analyses to see if they could detect animal eumelanin pigment. The researchers also analyzed the observed spectral signatures to compare them with melanins produced by various microbes, just to make sure that the pigments were not from any other source. This is animal melanin, not microbial melanin, and it is associated with these melanosome-like structures in the fossil feathers, Carney said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/bu-pop082515.php){:target="_blank" rel="noopener"}


