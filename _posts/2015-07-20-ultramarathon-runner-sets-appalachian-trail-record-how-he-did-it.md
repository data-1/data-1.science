---
layout: post
title: "Ultramarathon Runner Sets Appalachian Trail Record: How He Did It"
date: 2015-07-20
categories:
author: Elizabeth Goldbaum
tags: [Ultramarathon,Running,Science,Carbohydrate,Exercise,Marathon,Fat,Sleep,Long-distance running,Health,Determinants of health]
---


Running that long with a heart rate elevated for that long of a time will cause some minor breakdown of heart tissue and heart muscle, which can cause stress on your heart, Emmett said. Jurek is an experienced ultra-runner and has won several elite ultramarathon races. It's not so much a sport, as it is usually trying to break a record for a distance like that, she explained. And as more and more people run marathons, runners will likely continue to break records, Emmett said. There was a time when people doubted anyone would break a 4-minute mile, but runners did it, he added.

<hr>

[Visit Link](http://www.livescience.com/51594-ultramarathon-record-appalachian-trail.html){:target="_blank" rel="noopener"}


