---
layout: post
title: "One of the World's Largest Oil Companies is Spending $1 Billion a Year on Green Energy Research"
date: 2018-07-19
categories:
author: ""
tags: [Algae fuel,Biofuel,ExxonMobil,Fuel,Renewable energy,Sustainable energy,Energy,Nature,Sustainable technologies,Fuels,Sustainable development,Climate change mitigation,Energy and the environment,Natural environment,Natural resources,Chemical substances,Environmental technology,Chemistry]
---


We bring the science, the commitment to research.”  Oil is Out  The company will cast a wide net when it comes to emerging technologies. Exxon Mobil will continue to investigate uses of algae, processing it into biofuel. The company is also embarking on research alongside the University of Georgia, which seeks to develop a better way of processing crude oil and turning it into plastic. Swarup estimates that projects like the algae biofuel and the carbonate fuel cells are still more than a decade away. It's crucial that a company like Exxon Mobil gets ahead of the curve when it comes to new technology and new approaches, lest it end up left behind as the world moves on from fossil fuels.

<hr>

[Visit Link](https://futurism.com/worlds-largest-oil-companies-spending-1-billion-green-energy-research/){:target="_blank" rel="noopener"}


