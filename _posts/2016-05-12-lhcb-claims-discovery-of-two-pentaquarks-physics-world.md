---
layout: post
title: "LHCb claims discovery of two pentaquarks – Physics World"
date: 2016-05-12
categories:
author: Hamish Johnston
tags: [Pentaquark,Quark,LHCb experiment,Hadron,Baryon,Meson,Physical sciences,Science,Hadrons,Quantum mechanics,Fermions,Theoretical physics,Nuclear physics,Subatomic particles,Standard Model,Quantum chromodynamics,Quantum field theory,Physics,Particle physics]
---


Pentaquark molecule: a meson and baryon bound together  The best evidence yet for the existence of a new type of particle called a pentaquark has been unveiled by physicists working on the LHCb experiment on the Large Hadron Collider (LHC) at CERN. Most known hadrons are either mesons, which contain a quark and an antiquark, or baryons, which comprise three quarks. In particular, it allows for particles containing four quarks and one antiquark. LHCb physicist Tim Gershon of the University of Warwick in the UK explains why he is confident of the result: “The LHCb analysis is significantly different from those of previous experiments that found hints of [pentaquarks] that were later disproved.” He adds that “LHCb has analysed all of the available information in the decay distribution to prove that the peak in the mass distribution cannot be a fake caused by other processes.”  Subatomic molecules  The LHCb data do not, however, reveal how the five quarks are bound within the pentaquark. The meson and baryon could then be bound to each other to create a structure resembling a subatomic molecule.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/jul/14/lhcb-claims-discovery-of-two-pentaquarks){:target="_blank" rel="noopener"}


