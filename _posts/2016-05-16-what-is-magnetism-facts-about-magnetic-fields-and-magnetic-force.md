---
layout: post
title: "What is magnetism? Facts about magnetic fields and magnetic force"
date: 2016-05-16
categories:
author: Jim Lucas, Paul Sutter
tags: [Magnetism,Magnetic field,Diamagnetism,Magnet,Force,Ferromagnetism,Electricity,Physical sciences,Electrical engineering,Physics,Electromagnetism]
---


Other forms of magnetism  Magnetism takes many other forms, but except for ferromagnetism, they are usually too weak to be observed except by sensitive laboratory instruments or at very low temperatures. Anton Brugnams first discovered diamagnetism (opens in new tab) in 1778 while using permanent magnets in his search for materials containing iron. Direct current can also produce a constant field in one direction that can be switched on and off with the current. This is the basis for a number of devices, most notably, the microphone (opens in new tab). (opens in new tab).

<hr>

[Visit Link](http://www.livescience.com/38059-magnetism.html){:target="_blank" rel="noopener"}


