---
layout: post
title: "Bats use polarized light to navigate"
date: 2015-10-05
categories:
author: Natural Environment Research Council
tags: [Bat,Polarization (waves),Sky,Bird,Animals]
---


Credit: Stefan Greif  Scientists have discovered that greater mouse-eared bats use polarisation patterns in the sky to navigate – the first mammal that's known to do this. 'We know that other animals use polarisation patterns in the sky, and we have at least some idea how they do it: bees have specially-adapted photoreceptors in their eyes, and birds, fish, amphibians and reptiles all have cone cell structures in their eyes which may help them to detect polarisation,' says Dr Richard Holland of Queen's University Belfast, co-author of the study. They're clearest in a strip across the sky 90° from the position of the sun at sunset or sunrise. They found the bats that had been shown a shifted pattern of polarised light headed off in a direction shifted at right angles from the controls released at the same time. 'We know that bats must be 'seeing' the turbines, but it seems that the air pressure patterns around working turbines give the bats what's akin to the bends,' says Holland.

<hr>

[Visit Link](http://phys.org/news325240523.html){:target="_blank" rel="noopener"}


