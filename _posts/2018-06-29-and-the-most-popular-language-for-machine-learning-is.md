---
layout: post
title: "And the Most Popular Language for Machine Learning Is..."
date: 2018-06-29
categories:
author: "Nov."
tags: []
---


I searched for skills used in conjunction with machine learning and data science, where skills are one of the prominent programming language Java, C, C++, and JavaScript. Will Julia turn in one of the popular languages for machine learning and data science? Python, Java, and R are most popular skills when it comes to machine learning and data science jobs. You may get a different answer if you are looking for a job in academia, or if you just want to have fun learning about machine learning and data science during your spare time. Besides having support from many top machine learning frameworks, Python is a good fit for me because I have a computer science background.

<hr>

[Visit Link](https://dzone.com/articles/and-the-most-popular-language-for-machine-learning?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


