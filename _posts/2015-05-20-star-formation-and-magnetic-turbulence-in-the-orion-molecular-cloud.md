---
layout: post
title: "Star formation and magnetic turbulence in the Orion Molecular Cloud"
date: 2015-05-20
categories:
author: ""    
tags: [Cosmic dust,Orion (constellation),Molecular cloud,Star formation,Milky Way,Star,Interstellar medium,Galaxy,Outer space,Nature,Stellar astronomy,Physics,Sky,Astrophysics,Astronomical objects,Physical sciences,Space science,Astronomy]
---


With blue hues suggestive of marine paradises and a texture evoking the tranquil flow of sea waves, this image might make us daydream of sandy beaches and exotic holiday destinations. Instead, the subject of the scene is intense and powerful, because it depicts the formation of stars in the turbulent billows of gas and dust of the Orion Molecular Cloud. The red clumps at the centre of the image are part of the Orion Molecular Cloud Complex, one of the closest large regions of star formation, only about 1300 light-years from the Sun. However, the field becomes less regular in the central and lower parts of the image, in the region of the Orion Molecular Cloud. The emission from dust is computed from a combination of Planck observations at 353, 545 and 857 GHz, whereas the direction of the magnetic field is based on Planck polarisation data at 353 GHz.

<hr>

[Visit Link](http://www.esa.int/spaceinimages/Images/2015/05/Magnetic_Orion){:target="_blank" rel="noopener"}


