---
layout: post
title: "International team of scientists launches fossil database"
date: 2015-07-20
categories:
author: Bruce Museum 
tags: [Paleontology,Evolution,Fossil,American Association for the Advancement of Science,Molecular clock,Science,Biological evolution,Nature]
---


Fossils provide the critical age data we need to unlock the timing of major evolutionary events, says Dr. Ksepka. This new resource will provide the crucial fossil data needed to calibrate 'molecular clocks' which can reveal the ages of plant and animal groups that lack good fossil records. The Fossil Calibration Database addresses this issue by providing molecular biologists with paleontologist-approved data for organisms across the Tree of Life. ###  About the Bruce Museum  The Bruce Museum is a museum of art and science and is located at One Museum Drive in Greenwich, Connecticut. Individual admission is free on Tuesday.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/bm-ito022315.php){:target="_blank" rel="noopener"}


