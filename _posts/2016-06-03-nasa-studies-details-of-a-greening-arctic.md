---
layout: post
title: "NASA studies details of a greening Arctic"
date: 2016-06-03
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Landsat program,Tundra,Goddard Space Flight Center,Advanced very-high-resolution radiometer,Remote sensing,Nature,Natural environment,Physical geography,Earth sciences]
---


In a changing climate, almost a third of the land cover - much of it Arctic tundra - is looking more like landscapes found in warmer ecosystems. With Landsat 5 and Landsat 7 data, Masek and his colleague Junchang Ju, a remote sensing scientist at Goddard, found that there was extensive greening in the tundra of western Alaska, the northern coast of Canada, and the tundra of Quebec and Labrador. The more detailed look - now available to other researchers as well - will also let scientists see if a correlation exists between habitat characteristics and greening or browning trends. With the higher resolution Landsat data, the researchers also found a lot of differences within areas - one pixel would be brown, and its neighbors green, noted Ju. With the large map complete, researchers will focus on these short distances - looking at the smaller scale to see what might control the greening patterns, whether it's local topography, nearby water sources, or particular types of habitat.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/nsfc-nsd060216.php){:target="_blank" rel="noopener"}


