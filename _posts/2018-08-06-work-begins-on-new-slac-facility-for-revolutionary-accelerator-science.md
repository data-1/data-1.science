---
layout: post
title: "Work begins on new SLAC facility for revolutionary accelerator science"
date: 2018-08-06
categories:
author: "DOE/SLAC National Accelerator Laboratory"
tags: [Particle accelerator,SLAC National Accelerator Laboratory,Plasma acceleration,Plasma (physics),Electron,Linear particle accelerator,Electromagnetism,Physics,Applied and interdisciplinary physics,Science,Nature,Physical sciences,Chemistry]
---


The goal: Develop plasma technologies that could shrink future accelerators up to 1,000 times, potentially paving the way for next-generation particle colliders and powerful light sources  Menlo Park, Calif. -- The Department of Energy's SLAC National Accelerator Laboratory has started to assemble a new facility for revolutionary accelerator technologies that could make future accelerators 100 to 1,000 times smaller and boost their capabilities. The project is an upgrade to the Facility for Advanced Accelerator Experimental Tests (FACET), a DOE Office of Science user facility that operated from 2011 to 2016. As a strategically important national user facility, FACET-II will allow us to explore the feasibility and applications of plasma-driven accelerator technology, said James Siegrist, associate director of the High Energy Physics (HEP) program of DOE's Office of Science, which stewards advanced accelerator R&D in the U.S. for the development of applications in science and society. The project team has done an outstanding job in securing DOE approval for the facility, said DOE's Hannibal Joma, federal project director for FACET-II. Thanks to the DOE's continued support we'll soon be able to open FACET-II for groundbreaking new science.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-06/dnal-wbo061118.php){:target="_blank" rel="noopener"}


