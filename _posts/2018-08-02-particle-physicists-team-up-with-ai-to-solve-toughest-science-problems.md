---
layout: post
title: "Particle physicists team up with AI to solve toughest science problems"
date: 2018-08-02
categories:
author: "Manuel Gnida, Slac National Accelerator Laboratory"
tags: [ATLAS experiment,Particle physics,Deep learning,Neutrino,Machine learning,Large Hadron Collider,Physics,Branches of science,Science,Technology]
---


Neutrino experiments also benefit from machine learning. NOvA's detectors are watching out for charged particles produced when neutrinos hit the detector material, and machine learning algorithms identify them. From machine learning to deep learning  Recent developments in machine learning, often called deep learning, promise to take applications in particle physics even further. Computer vision technology could help identify features in jets. Therefore, establishing machine learning in particle physics requires constant efforts to better understand the inner workings of the algorithms and to do cross-checks with real data whenever possible.

<hr>

[Visit Link](https://phys.org/news/2018-08-particle-physicists-team-ai-toughest.html){:target="_blank" rel="noopener"}


