---
layout: post
title: "Cassini concludes pioneering mission at Saturn"
date: 2017-09-23
categories:
author: ""
tags: [CassiniHuygens,Saturn,Titan (moon),Enceladus,Planemos,Physical sciences,Moons,Planets of the Solar System,Astronomical objects,Spaceflight,Bodies of the Solar System,Outer planets,Gas giants,Planets,Solar System,Space science,Planetary science,Astronomical objects known since antiquity,Outer space,Astronomy]
---


Science & Exploration Cassini concludes pioneering mission at Saturn 15/09/2017 25130 views 212 likes  The international Cassini mission has concluded its remarkable exploration of the Saturnian system in spectacular style, by plunging into the gas planet’s atmosphere. Cassini's final image – natural colour view Atmospheric entry began about a minute before loss of signal, and the spacecraft sent scientific data in near real-time until its antenna could no longer point towards Earth. First colour view of Titan's surface “This mission has changed the way we view ocean-worlds in the Solar System, offering tantalising hints of places which could offer potentially habitable environments, with Titan giving us a planet-sized laboratory to study processes that may even be relevant to the origin of life on Earth.” Launched on 15 October 1997 and arriving in Saturn’s orbit on 30 June 2004 (PDT), Cassini carried ESA’s Huygens probe that landed on Titan on 14 January 2005. Cassini would continue to make exciting discoveries at Titan from orbit, with its radar finding lakes and seas filled with methane and other hydrocarbons, making it the only other known place in our Solar System with a stable liquid on its surface. Saturn’s moon zoo Saturn’s moons continued to surprise, with one of the major discoveries of the entire mission the detection of icy plumes erupting from fissures in the southern hemisphere of Enceladus.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Cassini-Huygens/Cassini_concludes_pioneering_mission_at_Saturn){:target="_blank" rel="noopener"}


