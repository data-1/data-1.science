---
layout: post
title: "New Neandertal and archaic human genomes advance our understanding of human evolution"
date: 2017-10-06
categories:
author: "American Association for the Advancement of Science (AAAS)"
tags: [Human,Human populations,Pleistocene,Pliocene,Biological anthropology,Evolution of primates,Human evolution,Biology,Genetics]
---


Five Neandertal genomes have been sequenced to date, yet only one yielded high-quality data, an individual found in Siberia known as the Altai Neandertal. Three less well-defined genomes come from individuals found in a cave, Vindija, in Croatia, and one from Mezmaiskaya Cave, in Russia. Similar to previous findings, the genetic data suggest that Neandertals lived in small and isolated populations of about 3,000 individuals. Vindija 33.19 does appear to share a maternal ancestor with two of the three other individuals from the Croatian cave who were genetically sequenced, however. Among many findings, they report that early modern human gene flow into Neandertal populations occurred between 130,000 and 145,000 years ago, before the Croatian and Siberian Neandertals diverged.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/aaft-nna100217.php){:target="_blank" rel="noopener"}


