---
layout: post
title: "The call of the sea: Mammalian evolutionary transitions back to the sea"
date: 2016-06-23
categories:
author: "SMBE Journals (Molecular Biology and Evolution and Genome Biology and Evolution)"
tags: [Evolution,Gene,Mammal,Biology,Rate of evolution,DNA repair,Adaptation,Surfactant protein B,Molecular biology,Biochemistry,Life sciences,Biotechnology,Genetics]
---


Though mammals adapted on land, a new study by Maria Chikina and Nathan Clark has shown that during three major independent evolutionary events, a number of mammals harkened back to the sea. For the manatee, walrus, dolphin, and killer whale, the return to the sea involved many evolutionary trade-offs amongst hundreds of genes: a general loss of the number of sensory genes for smell and taste, new functions for genes forming skin and connective tissue, and genes involved in muscle structure and metabolism. To hone in identifying key genes, they used a new approach: rather than catalog single genetic mutations from genome-wide studies, or look for candidate genes in key pathways, or catalog single amino acid changes at the protein level, they calculated a genome-wide average rate of evolution across all species. Then, they determined whether sea mammals had put the evolutionary gas pedal or brake on compared to the average rate. The marine-accelerated genes were significantly enriched for functional categories that align well with current knowledge of marine mammal adaptations: sensory systems, muscle function, skin and connective tissue, lung function, and lipid metabolism.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/mbae-tco062216.php){:target="_blank" rel="noopener"}


