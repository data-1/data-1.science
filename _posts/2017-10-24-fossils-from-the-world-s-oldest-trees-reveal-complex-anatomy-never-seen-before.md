---
layout: post
title: "Fossils from the world's oldest trees reveal complex anatomy never seen before"
date: 2017-10-24
categories:
author: Cardiff University
tags: [Tree,Fossil,Science,University,Xylem,Cardiff University,American Association for the Advancement of Science,Research,Woody plant,Bark (botany)]
---


Fossils from a 374-million-year-old tree found in north-west China have revealed an interconnected web of woody strands within the trunk of the tree that is much more intricate than that of the trees we see around us today. The team, which includes researchers from Cardiff University, Nanjing Institute of Geology and Palaeontology, and State University of New York, also show that the development of these strands allowed the tree's overall growth. Co-author of the study Dr Chris Berry, from Cardiff University's School of Earth and Ocean Sciences, said: There is no other tree that I know of in the history of the Earth that has ever done anything as complicated as this. Dr Berry has been studying cladoxylopsids for nearly 30 years, uncovering fragmentary fossils from all over the world. Yet Dr Berry was amazed when a colleague uncovered a massive, well-preserved fossil of a cladoxylopsid tree trunk in Xinjiang, north-west China.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/cu-fft101917.php){:target="_blank" rel="noopener"}


