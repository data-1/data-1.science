---
layout: post
title: "Solar research: On the generation of solar spicules and Alfvenic waves"
date: 2017-10-14
categories:
author: "Instituto de Astrofísica de Canarias (IAC)"
tags: [Plasma (physics),Sun,Alfvn wave,Science,Space science,Physical sciences,Astronomy,Nature,Physics,Astrophysics,Electromagnetism]
---


Their model is based in the dynamics of plasma -- the hot gas of charged particles that streams along magnetic fields and constitutes the sun. Some particles are still neutral, and neutral particles aren't subject to magnetic fields like charged particles are. Scientists based previous models on a uniform plasma in order to simplify the problem -- modeling is computationally expensive, and the final model took roughly a year to run with NASA's supercomputing resources -- but they realized neutral particles are a necessary piece of the puzzle. Usually magnetic fields are tightly coupled to charged particles, said Juan Martínez-Sykora, lead author of the study and a solar physicist at Lockheed Martin. With only charged particles in the model, the magnetic fields were stuck, and couldn't rise to the surface.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171013123126.htm){:target="_blank" rel="noopener"}


