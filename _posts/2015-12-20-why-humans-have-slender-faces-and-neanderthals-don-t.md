---
layout: post
title: "Why Humans Have Slender Faces and Neanderthals Don't"
date: 2015-12-20
categories:
author: Laura Geggel
tags: [Bone,Neanderthal,Human,Early modern human]
---


Neanderthals had protruding facial features because of the way their bodies deposited and dealt with bone, a new study finds. In Neanderthals, facial bone deposits continue into the teenage years, whereas in humans (Homo sapiens), bone removal during childhood leads to a flatter face, the researchers found. Bone in human faces has bone-absorbing cells on its outermost layers. The scientists also looked at four teenage hominin faces from the Sima de los Huesos site in north-central Spain, all dating to about 400,000 years ago. The finding shows that Neanderthals and the Sima fossils share a similar facial growth pattern, Lacruz said.

<hr>

[Visit Link](http://www.livescience.com/53102-neanderthals-protruding-jaws-bone-deposits.html){:target="_blank" rel="noopener"}


