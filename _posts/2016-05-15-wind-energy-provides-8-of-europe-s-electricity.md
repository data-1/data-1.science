---
layout: post
title: "Wind energy provides 8% of Europe's electricity"
date: 2016-05-15
categories:
author: Joint Research Centre, Jrc
tags: [Wind power,Renewable energy,Renewable resources,Sustainable energy,Sustainable development,Sustainable technologies,Economy,Electric power,Physical quantities,Nature,Energy,Power (physics)]
---


According to a JRC report, the impressive growth of the industry will allow at least 12% electricity share by 2020, a significant contribution to the goal of the European energy and climate package of 20% share of energy from renewable sources. Wind power is the renewable energy which has seen the widest and most successful deployment over the last two decades, increasing the global cumulative capacity from 3 GW to 370 GW. With 23.2 GW of new installations and a market share of 44%, China is well ahead of EU's member states which together installed 13.05 GW. The EU however still leads in cumulative capacity and its 129 GW onshore and offshore wind installations, allowed six countries – Denmark, Portugal, Ireland, Spain, Romania and Germany – to generate between 10 and 40 % of their electricity from wind. In a context of high competition and diminishing turbine prices, manufacturers managed to improve their balance sheet thanks to better cost management and reduced raw materials costs.

<hr>

[Visit Link](http://phys.org/news/2015-07-energy-europe-electricity.html){:target="_blank" rel="noopener"}


