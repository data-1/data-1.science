---
layout: post
title: "How to use fpaste to share problem reports"
date: 2015-09-28
categories:
author: Mike Ruckman
tags: []
---


fpaste testpaste.txt -d my super secret password  If you want to see all the options fpaste has to offer, check the man page. man fpaste  fpaste your output  While pasting files is nice, sometimes the stuff you need to paste isn’t a file, but output from a command. Output redirects allow you to use the output of one command as the input to another. lspci | grep VGA | fpaste  This would then send that output to the Fedora pastebin. If you want to paste the output of the command and be 100% certain you captured all the output (from stdout and stderr), use the “|&” operator.

<hr>

[Visit Link](http://lxer.com/module/newswire/ext_link.php?rid=219677){:target="_blank" rel="noopener"}


