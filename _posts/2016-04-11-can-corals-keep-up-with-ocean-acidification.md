---
layout: post
title: "Can corals keep up with ocean acidification?"
date: 2016-04-11
categories:
author: University of Delaware
tags: [Ocean acidification,PH,Marine biogenic calcification,Coral,Physical sciences,Oceanography,Physical geography,Nature,Chemistry,Environmental science,Applied and interdisciplinary physics,Earth sciences]
---


An interdisciplinary team of researchers led by University of Delaware professors Wei-Jun Cai and Mark Warner has successfully measured both pH and carbonate ion concentration directly inside the calcifying fluid found in coral, an important development in the study of how ocean acidification will affect marine calcifying organisms such as corals and shellfish. According to Cai, Mary A.S. Lighthipe Chair of Earth, Ocean, and Environment, the research team, which includes colleagues from University of Georgia and Ohio State University, is the first to measure carbonate ion concentration inside coral using a specialized carbonate sensor developed in Cai's laboratory. This is very important, Warner continued, because while scientists know how OA is changing the seawater chemistry, they don't yet understand how the chemistry of the fluid deep inside the coral -- where coral make their calcium carbonate skeletons -- is changing. The researchers' results confirmed what other scientists have measured before - the pH inside the coral's calcifying layer is high. Andréa Grottoli, professor in the School of Earth Sciences at Ohio State is principal investigator (PI) of one NSF award with Cai and Mark Warner as co-PIs.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/uod-cck040716.php){:target="_blank" rel="noopener"}


