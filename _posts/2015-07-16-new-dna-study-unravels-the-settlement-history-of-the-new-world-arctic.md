---
layout: post
title: "New DNA study unravels the settlement history of the New World Arctic"
date: 2015-07-16
categories:
author: University of Copenhagen 
tags: [Paleo-Eskimo,Dorset culture,Inuit,Hunter-gatherers of the Arctic,Indigenous peoples of the Americas,Eskimos,Arctic]
---


In this study, researchers show that the Paleo-Eskimo, who lived in the Arctic from about 5,000 years ago until about 700 years ago, represented a distinct wave of migration, separate from both Native Americans – who crossed the Bering Strait much earlier – and the Inuit, who came from Siberia to the Arctic several thousand years after the Paleo-Eskimos. In the archaeological literature, distinctions are drawn between the different cultural units in the Arctic in the period up to the rise of the Thule culture, which replaced all previous Arctic cultures and is the source of today's Inuit in Alaska, Canada and Greenland. These facts have further raised questions regarding the possibility of several waves of migration from Siberia to Alaska, or perhaps Native Americans migrating north during the first 4,000 years of the Arctic being inhabited. - Our study shows that, genetically, all of the different Paleo-Eskimo cultures belonged to the same group of people. Genetics and archaeology  The genetic study underpins some archaeological findings, but not all of them.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/uoc-nds082814.php){:target="_blank" rel="noopener"}


