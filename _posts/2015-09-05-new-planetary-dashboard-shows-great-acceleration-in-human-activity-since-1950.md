---
layout: post
title: "New planetary dashboard shows 'Great Acceleration' in human activity since 1950"
date: 2015-09-05
categories:
author: SAGE 
tags: [Anthropocene,Human impact on the environment,Planetary boundaries,Earth,Global natural environment,Physical geography,Nature,Natural environment,Earth sciences]
---


Human activity, predominantly the global economic system, is now the prime driver of change in the Earth System (the sum of our planet's interacting physical, chemical, biological and human processes), according to a set of 24 global indicators, or planetary dashboard, published in the journal Anthropocene Review (19 January 2015). We can say that around 1950 was the start of the Great Acceleration, said Professor Steffen, a researcher at the Australian National University and the Stockholm Resilience Centre. After 1950 you can see that major Earth System changes became directly linked to changes largely related to the global economic system. It is only beyond the mid-20th century that there is clear evidence for fundamental shifts in the state and functioning of the Earth System that are beyond the range of variability of the Holocene, and driven by human activities and not by natural variability. ###  Further information  Research papers  The trajectory of the Anthropocene: The Great Acceleration (Anthropocene Review) 15 January 2015.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/sp-npd011515.php){:target="_blank" rel="noopener"}


