---
layout: post
title: "Physics boosts artificial intelligence methods"
date: 2017-10-18
categories:
author: California Institute of Technology
tags: [Large Hadron Collider,Particle physics,Higgs boson,Maria Spiropulu,Physics,Machine learning,Quantum annealing,Applied mathematics,Technology,Science,Branches of science]
---


Researchers from Caltech and the University of Southern California (USC) report the first application of quantum computing to a physics problem. Despite the central role of physics in quantum computing, until now, no problem of interest for physics researchers has been resolved by quantum computing techniques. In this new work, the researchers successfully extracted meaningful information about Higgs particles by programming a quantum annealer--a type of quantum computer capable of only running optimization tasks--to sort through particle-measurement data littered with errors. A popular computing technique for classifying data is the neural network method, known for its efficiency in extracting obscure patterns within a dataset. Surprisingly, it was actually advantageous to use the excited states, the suboptimal solutions, says Lidar.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/ciot-pba101817.php){:target="_blank" rel="noopener"}


