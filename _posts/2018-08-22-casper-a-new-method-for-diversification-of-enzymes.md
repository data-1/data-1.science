---
layout: post
title: "CasPER -- a new method for diversification of enzymes"
date: 2018-08-22
categories:
author: "Technical University of Denmark"
tags: [CRISPR,Cas9,Enzyme,Chemistry,Biotechnology,Biology,Biochemistry,Life sciences,Genetics,Molecular biology]
---


However, the new tool enables scientists to engineer enzymes or their active domains by integrating much longer diversified fragments providing the opportunity to target every single nucleotide in a specific region. Discovery of enzymes variants  In depth characterisation of the new method concludes that the main difference between already existing CRISPR/Cas9 methods is that CasPER allows very efficient integration and in multiplex manner of large DNA fragments bearing various mutations to generate pools of cells with hundreds of thousands of enzymes variants. You need to produce a certain amount of product to make it commercially relevant, and a tool like CasPER will definitely help to speed up and upscale this process, says Tadas Jakociunas. To prove the applicability and efficiency of CasPER scientists targeted two essential enzymes in the mevalonate pathway and were able to select cell factories with up to 11-fold increased production of carotenoids. Although the main application of the method was to speed up and lower the costs for engineering and optimizing cell factories, the method can also be applied for any experiment where diversification of DNA is needed.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180821094301.htm){:target="_blank" rel="noopener"}


