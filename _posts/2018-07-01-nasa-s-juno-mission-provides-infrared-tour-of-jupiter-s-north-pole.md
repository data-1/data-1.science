---
layout: post
title: "NASA's Juno mission provides infrared tour of Jupiter's north pole"
date: 2018-07-01
categories:
author: "Jet Propulsion Laboratory"
tags: [Juno (spacecraft),Jupiter,Jovian Infrared Auroral Mapper,Dynamo theory,Magnetosphere of Jupiter,Space science,Bodies of the Solar System,Astronomical objects,Planets,Physical sciences,Planets of the Solar System,Solar System,Science,Planetary science,Outer space,Astronomy]
---


This infrared 3-D image of Jupiter's north pole was derived from data collected by the Jovian Infrared Auroral Mapper (JIRAM) instrument aboard NASA's Juno spacecraft. Credit: NASA/JPL-Caltech/SwRI/ASI/INAF/JIRAM  Scientists working on NASA's Juno mission to Jupiter shared a 3-D infrared movie depicting densely packed cyclones and anticyclones that permeate the planet's polar regions, and the first detailed view of a dynamo, or engine, powering the magnetic field for any planet beyond Earth. Credit: Jet Propulsion Laboratory  Prior to Juno, we could not distinguish between extreme models of Jupiter's interior rotation, which all fitted the data collected by Earth-based observations and other deep space missions, said Tristan Guillot, a Juno co-investigator from the Université Côte d'Azur, Nice, France. NASA's Juno mission has provided the first view of the dynamo, or engine, powering Jupiter's magnetic field. The map Connerney's team made of the dynamo source region revealed unexpected irregularities, regions of surprising magnetic field intensity, and that Jupiter's magnetic field is more complex in the northern hemisphere than in the southern hemisphere.

<hr>

[Visit Link](https://phys.org/news/2018-04-nasa-juno-mission-infrared-jupiter.html){:target="_blank" rel="noopener"}


