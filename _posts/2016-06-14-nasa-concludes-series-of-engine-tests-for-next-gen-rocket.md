---
layout: post
title: "NASA concludes series of engine tests for next-gen rocket"
date: 2016-06-14
categories:
author: "Kathryn Hambleton"
tags: [RS-25,Stennis Space Center,Space Launch System,NASA,Space vehicles,Space access,Space launch vehicles,Spacecraft,Aerospace,NASA programs,Vehicles,Space exploration,Technology,Astronautics,Rocketry,Space programs,Outer space,Flight,Space program of the United States,Spaceflight]
---


Credit: NASA  NASA has completed the first developmental test series on the RS-25 engines that will power the agency's new Space Launch System (SLS) rocket on missions deeper into space than ever before. The test series wrapped up Thursday with a seventh hot fire test of a developmental RS-25 engine on the A-1 Test Stand at NASA's Stennis Space Center in Bay St. Louis, Mississippi. It is one of the most experienced large rocket engines in the world, with more than a million seconds of ground test and flight operations time. The series was designed to collect valuable data on performance of the RS-25 engine, a former space shuttle main engine operating at higher thrust levels in order to provide the power needed for the SLS vehicle. Testing of RS-25 flight engines for the initial SLS missions will begin at Stennis this fall.

<hr>

[Visit Link](http://phys.org/news/2015-08-nasa-series-next-gen-rocket.html){:target="_blank" rel="noopener"}


