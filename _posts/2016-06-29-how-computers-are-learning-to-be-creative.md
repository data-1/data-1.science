---
layout: post
title: "How computers are learning to be creative"
date: 2016-06-29
categories:
author: "Blaise Agüera Y Arcas"
tags: [Perception,TED (conference),Creativity,Cognition,Mental processes,Computational neuroscience,Systems science,Neuropsychology,Cognitive science,Cybernetics,Interdisciplinary subfields,Neuroscience,Learning,Cognitive neuroscience,Branches of science,Cognitive psychology,Technology]
---


In this captivating demo, he shows how neural nets trained to recognize images can be run in reverse, to generate them. The results: spectacular, hallucinatory collages (and poems!) that defy categorization. Perception and creativity are very intimately connected, Agüera y Arcas says. Any creature, any being that is able to do perceptual acts is also able to create.

<hr>

[Visit Link](http://www.ted.com/talks/blaise_aguera_y_arcas_how_computers_are_learning_to_be_creative){:target="_blank" rel="noopener"}


