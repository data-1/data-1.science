---
layout: post
title: "Evolutionary advantage of genetic recombination in the genome measured for first time"
date: 2016-01-29
categories:
author: Universitat Autonoma de Barcelona
tags: [Genetics,Genetic recombination,Genetic linkage,Evolution,Natural selection,Mutation,Genome,Adaptation,Gene,Biological evolution,Biology,Evolutionary biology,Branches of genetics,Life sciences,Biotechnology]
---


If a new selected mutation is surrounded by others that are also exposed to selection, these mutations will interfere (compete) with each other as they do not segregate independently, so that joint selection will be less efficient than if selection acts on each mutation separately. If linkage cost exists, wherever recombination is low there will be a greater density of selective variants that do not segregate freely, lowering the efficiency of the selection and therefore the adaptation rate. An infinite rate of recombination would not increase the adaptive rate of the genome more than a recombination value of 2 cM/Mb (the estimated threshold recombination). The researchers found that the D. melanogaster genome has an adaptation rate around 27% below the optimal adaptation rate, the rate it would have if the effects of the mutations did not interfere with one another. This work is also yet another step in the measurement of natural selection at the nucleotide, gene or genome level, as it addresses the question of how the genomic context, whether the current rate of recombination or the rate of mutation, conditions the efficiency of natural selection.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/uadb-eao011416.php){:target="_blank" rel="noopener"}


