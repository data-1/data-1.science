---
layout: post
title: "Preshistoric plumage patterns"
date: 2015-10-29
categories:
author: University of Alberta 
tags: [Feather,Dinosaur,Ornithomimus]
---


An undergraduate University of Alberta paleontology student has discovered an Ornithomimus dinosaur with preserved tail feathers and soft tissue. The discovery is shedding light on the convergent evolution of these dinosaurs with ostriches and emus relating to thermoregulation and is also tightening the linkages between dinosaurs and modern birds. We now know what the plumage looked like on the tail, and that from the mid-femur down, it had bare skin, says Aaron van der Reest. Because the plumage on this specimen is virtually identical to that of an ostrich, we can infer that Ornithomimus was likely doing the same thing, using feathered regions on their body to maintain body temperature. If we can better understand the processes behind the preservation of the feathers in this specimen, we can better predict whether other fossilized animals in the ground will have soft tissues, feathers, or skin impressions preserved.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/uoa-ppp102815.php){:target="_blank" rel="noopener"}


