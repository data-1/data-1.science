---
layout: post
title: "Growing skin in the lab"
date: 2016-04-10
categories:
author: RIKEN
tags: [Tissue (biology),Induced pluripotent stem cell,Skin,Tissue engineering,Zoology,Life sciences,Anatomy,Biology,Cell biology,Developmental biology,Biotechnology,Morphology (biology)]
---


Using reprogrammed iPS cells, scientists from the RIKEN Center for Developmental Biology (CDB) in Japan have, along with collaborators from Tokyo University of Science and other Japanese institutions, successfully grown complex skin tissue--complete with hair follicles and sebaceous glands--in the laboratory. The researchers created EBs from iPS cells using Wnt10b signaling and then implanted multiple EBs into immune-deficient mice, where they gradually changed into differentiated tissue, following the pattern of an actual embryo. Once the tissue had differentiated, the scientists transplanted them out of those mice and into the skin tissue of other mice, where the tissues developed normally as integumentary tissue?the tissue between the outer and inner skin that is responsible for much of the function of the skin in terms of hair shaft eruption and fat excretion. According to Takashi Tsuji of the RIKEN Center for Developmental Biology, who led the study, Up until now, artificial skin development has been hampered by the fact that the skin lacked the important organs, such as hair follicles and exocrine glands, which allow the skin to play its important role in regulation. With this new technique, we have successfully grown skin that replicates the function of normal tissue.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/r-gsi033016.php){:target="_blank" rel="noopener"}


