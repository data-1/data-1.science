---
layout: post
title: "Researchers first to create a single-molecule diode"
date: 2016-05-03
categories:
author: Columbia University School Of Engineering, Applied Science
tags: [Molecular electronics,Nanotechnology,Molecule,Rectifier,Electronics,Electrical engineering,Applied and interdisciplinary physics,Physical sciences,Materials,Chemistry,Electromagnetism,Technology,Electricity]
---


Diodes are fundamental building blocks of integrated circuits; they allow current to flow in only one direction. Credit: Latha Venkataraman, Columbia Engineering  Under the direction of Latha Venkataraman, associate professor of applied physics at Columbia Engineering, researchers have designed a new technique to create a single-molecule diode, and, in doing so, they have developed molecular diodes that perform 50 times better than all prior designs. This goal, which has been the 'holy grail' of molecular electronics ever since its inception with Aviram and Ratner's 1974 seminal paper, represents the ultimate in functional miniaturization that can be achieved for an electronic device. Ideally, the ratio of 'on' current to 'off' current, the rectification ratio, should be very high. Explore further Electronics play by a new set of rules at the molecular scale

<hr>

[Visit Link](http://phys.org/news351765588.html){:target="_blank" rel="noopener"}


