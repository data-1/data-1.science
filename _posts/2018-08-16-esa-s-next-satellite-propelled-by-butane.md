---
layout: post
title: "ESA’s next satellite propelled by butane"
date: 2018-08-16
categories:
author: ""
tags: [Rocket engine,CubeSat,Cold gas thruster,Satellite,Spacecraft,Spaceflight,Flight,Outer space,Space vehicles,Astronautics,Rocketry,Spaceflight technology,Aerospace,Technology,Vehicles,Space science]
---


Thanks to a compact thruster resembling a butane cigarette lighter, the cereal box-sized satellite will fly around its near-twin to test their radio communications. Much quicker to build and cheaper to launch than traditional satellites, ESA is making use of CubeSats for testing new technologies in space. “Rather than burning propellant, these are simpler ‘cold-gas’ thrusters designed specifically for such a small mission. “Storing it as a liquid, like in a cigarette lighter, allows us to pack as many butane molecules as possible inside the small available volume – its liquid form being some 1000 times denser than its gas.” Each thruster will provide only 1 millinewton – the weight you would feel holding a feather in your hand – but enough to move the 8 kg satellite over time. GomX-4B The thrusters will typically be fired in pairs although they can also work individually, for a few minutes at a time and up to an hour.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/ESA_s_next_satellite_propelled_by_butane){:target="_blank" rel="noopener"}


