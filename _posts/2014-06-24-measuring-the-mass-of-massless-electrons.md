---
layout: post
title: "Measuring the mass of 'massless' electrons"
date: 2014-06-24
categories:
author: Harvard University
tags: [Graphene,Electron,Mass in special relativity,Physics,Inductance,Mass,Force,Inductor,Technology,Electrical engineering,Applied and interdisciplinary physics,Electricity,Electromagnetism,Physical sciences]
---


After two years of effort, researchers led by Donhee Ham, Gordon McKay Professor of Electrical Engineering and Applied Physics at the Harvard School of Engineering and Applied Sciences (SEAS), and his student Hosang Yoon, Ph.D.'14, have successfully measured the collective mass of 'massless' electrons in motion in graphene. Without this mass, the field of graphene plasmonics cannot work, so Ham's team knew it had to be there—but until now, no one had accurately measured it. Yoon and Ham knew that if they could apply an electric field to a graphene sample and measure the electrons' resulting collective acceleration, they could then use that data to calculate the collective mass. In those past experiments, electrons would accelerate but very quickly scatter as they collided with the impurities and imperfections. The scattering time was so short in those studies that you could never see the acceleration directly, says Ham.

<hr>

[Visit Link](http://phys.org/news322749966.html){:target="_blank" rel="noopener"}


