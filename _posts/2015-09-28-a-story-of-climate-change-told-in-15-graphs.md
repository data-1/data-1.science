---
layout: post
title: "A Story of Climate Change Told In 15 Graphs"
date: 2015-09-28
categories:
author: "$author"   
tags: [Arctic Ocean,Glacier,Ocean,Ice,Applied and interdisciplinary physics,Hydrography,Earth sciences,Physical geography,Natural environment,Hydrology,Oceanography,Nature,Climate,Climate variability and change,Earth phenomena,Environmental science]
---


Recently, on Twitter and Facebook I noticed graphs of climate change and its impacts being posted. Below I provide 15 graphs of climate change providing their sources. I encourage you to post other graphs below as long as the meet my three criteria (reliable source, unspeculative connection, no projections) and you provide a source link. Current atmospheric carbon dioxide at highest in last 400,000 years  3. Arctic ice cover decreases since 1980  6.

<hr>

[Visit Link](http://www.deepseanews.com/2015/09/a-story-of-climate-change-told-in-15-graphs/){:target="_blank" rel="noopener"}


