---
layout: post
title: "Quantum physics provides startling insights into biological processes"
date: 2015-07-12
categories:
author: ""   
tags: [Quantum mechanics,Quantum superposition,Experiment,Physics,Odor,Photosynthesis,Science,Applied and interdisciplinary physics]
---


Researchers in the PAPETS project are exploring this and other phenomena on the frontier between biology and quantum physics. Quantum 'superposition' makes photosynthesis more efficient  Quantum effects in a biological system, namely in a photosynthetic complex, were first observed by Greg Engel and collaborators in 2007, in the USA. Superposition contributes to more efficient energy transport. 'Only one door leads to the exit but the exciton can probe both left and right at the same time. Applications: more efficient solar cells and odour detection  While PAPETS is essentially an exploratory project, it is generating insights that could have practical applications.

<hr>

[Visit Link](http://phys.org/news/2015-07-quantum-physics-startling-insights-biological.html){:target="_blank" rel="noopener"}


