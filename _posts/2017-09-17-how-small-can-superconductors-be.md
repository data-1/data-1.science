---
layout: post
title: "How small can superconductors be?"
date: 2017-09-17
categories:
author: "Lisa Zyga"
tags: [Superconductivity,Cooper pair,Electron,BCS theory,Materials,Science,Electromagnetism,Condensed matter,Condensed matter physics,Materials science,Physical chemistry,Theoretical physics,Chemistry,Physical sciences,Quantum mechanics,Applied and interdisciplinary physics,Physics]
---


Nature Communications  For the first time, physicists have experimentally validated a 1959 conjecture that places limits on how small superconductors can be. If the superconducting gap energy gets too small and vanishes—which can occur, for example, when the temperature increases—then the electron collisions resume and the object stops being a superconductor. The Anderson limit shows that small size is another way that an object may stop being a superconductor. For large nanocrystals, the addition energy displays superconducting parity effect, a direct consequence of Cooper pairing. Studying this parity effect as a function of nanocrystal volume, we find the suppression of Cooper pairing when the mean electronic level spacing overcomes the superconducting gap energy, thus demonstrating unambiguously the validity of the Anderson criterion.

<hr>

[Visit Link](https://phys.org/news/2017-03-small-superconductors.html){:target="_blank" rel="noopener"}


