---
layout: post
title: "Research reveals trend in bird-shape evolution on islands"
date: 2016-04-13
categories:
author: The University of Montana
tags: [Flightless bird,Bird,Evolution,Predation,Flight,Hummingbird,Rail (bird),Biology,Animals,Zoology]
---


MISSOULA - In groundbreaking new work, Natalie Wright, a postdoctoral fellow at the University of Montana, has discovered a predictable trend in the evolution of bird shape. Like the flightless species on their islands, they generally have fewer predators than their relatives on the mainland. Her team found that flying birds evolved smaller flight muscles and longer legs on islands, just as occurred in flightless birds, but to a lesser extent. But these birds are still reducing the size of their flight muscles when relief from predators allows them to do so. But systematic evolution of smaller flight muscles may make it less likely that island populations will colonize new areas from those islands.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/tuom-rrt041216.php){:target="_blank" rel="noopener"}


