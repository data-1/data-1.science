---
layout: post
title: "Russia finds two 'well-preserved' dinosaur skeletons"
date: 2014-06-24
categories:
author: ""         
tags: [Cyberspace,Technology]
---


People work on an excavation site in the mountains south of Kislovodsk, Russia in October, 2009  Russian paleontologists have dug up well-preserved skeletons of two dinosaurs that roamed the Earth 100 million to 120 million years ago, a museum involved with the dig said Monday. The team found two dinosaur skeletons at a depth of some 2.5 metres (eight feet) below the surface after spending three weeks at a well-known excavation site near the village of Shestakovo in the Siberian region of Kemerovo. The skeletons are intact and in great condition, Olga Feofanova, director of a local museum involved with the excavation works, told AFP. She identified the species as Psittacosaurus sibiricus, noting that each skeleton was around two metres long. These discoveries have global significance.

<hr>

[Visit Link](http://phys.org/news322746894.html){:target="_blank" rel="noopener"}


