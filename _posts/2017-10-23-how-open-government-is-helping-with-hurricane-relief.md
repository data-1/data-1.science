---
layout: post
title: "How open government is helping with hurricane relief"
date: 2017-10-23
categories:
author: "Daniel Rubinstein"
tags: [Federal Emergency Management Agency,Hurricane Maria,Geographic information system,Technology]
---


Although changes in Hurricane Irma's path spared Florida from the bulk of the damage, both Irma and Maria directly hit Puerto Rico and the U.S. Virgin Islands. opensource.com  Using open data: OnTheMap for Emergency Management  Reliable data is crucial to any relief effort. Government agencies such as FEMA need information about the populations with whom they're working. Citizen involvement: Crowd Rescue HQ  In the days after the hurricane, information about the state of roads, bridges, and flooded areas in Puerto Rico was scarce. This initiative provides a framework for nonprofits, citizens, and government agencies to work together more effectively.

<hr>

[Visit Link](https://opensource.com/article/17/10/open-government-hurricane-relief){:target="_blank" rel="noopener"}


