---
layout: post
title: "Formation of coastal sea ice in North Pacific drives ocean circulation and climate"
date: 2015-10-22
categories:
author: University of California - Santa Cruz 
tags: [Ocean,Ice age,Ocean current,Sea,Atlantic Ocean,Sea ice,Climate change,Climate,Pacific Ocean,General circulation model,Earth phenomena,Physical geography,Oceanography,Applied and interdisciplinary physics,Hydrology,Hydrography,Environmental science,Natural environment,Nature,Environmental engineering,Earth sciences,Geography,Water]
---


New understanding of changes in North Pacific ocean circulation over the past 1.2 million years could lead to better global climate models  An unprecedented analysis of North Pacific ocean circulation over the past 1.2 million years has found that sea ice formation in coastal regions is a key driver of deep ocean circulation, influencing climate on regional and global scales. One result is a flow of cold deep water toward the equator and warm surface water toward the poles, and this overturning circulation plays a crucial role in moving heat around the globe. In the North Pacific, overturning circulation driven by formation of the North Pacific Intermediate Water is not as strong as in the North Atlantic, but it plays a major role in the region's climate. The expedition drilled sediment cores from the floor of the Bering Sea that preserve records of the regional climate and ocean circulation covering the past 1.2 million years, much longer than any other oceanographic records from that region. What the climate models were missing, she said, was the strong brine production from sea ice formation in the Bering Sea.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/uoc--foc102015.php){:target="_blank" rel="noopener"}


