---
layout: post
title: "How is artificial intelligence changing science?"
date: 2018-07-18
categories:
author: ""
tags: [Artificial intelligence,Deep learning,Machine learning,Technology,Branches of science,Learning,Interdisciplinary subfields,Systems science,Applied mathematics,Cognition,Computing,Science,Cognitive science]
---


Scientific exploration is going through a transition that, in the last 100 years, might only be compared to what happened in the '50s and '60s, moving to data and large data systems. This is an instance of searching without a known equation, where you are able to give examples, and through them, let the deep learning system learn what to look for and ultimately find out a particular pattern. In this case you can use AI to achieve comparable results in 10,000 times less time and computing. You iteratively feed sample cases to this system of equations, and you get the results days later. The other aspect is that I'm looking at it as a change that is brewing in the interaction between humans and machines.

<hr>

[Visit Link](https://phys.org/news/2018-05-artificial-intelligence-science.html){:target="_blank" rel="noopener"}


