---
layout: post
title: "Earliest humans had diverse range of body types, just as we do today"
date: 2015-07-21
categories:
author: University of Cambridge 
tags: [Homo,Human evolution,Human,Human populations,Pleistocene,Pliocene,Evolution of primates,Biological anthropology]
---


One of the dominant theories of our evolution is that our genus, Homo, evolved from small-bodied early humans to become the taller, heavier and longer legged Homo erectus that was able to migrate beyond Africa and colonise Eurasia. This has been known for several years, but we now know that consistently larger body size evolved in Eastern Africa after 1.7 million years ago, in the Koobi Fora region of Kenya. By comparing these bones to measurements taken from over 800 modern hunter-gatherer skeletons from around the world and applying various regression equations, the researchers were able to estimate body size for many new fossils that have never been studied in this way before. In human evolution we see body size as one of the most important characteristics, and from examining these 'scrappier' fossils we can get a much better sense of when and where human body size diversity arose. Before 1.7 million years ago our ancestors were seldom over 5 foot tall or particularly heavy in body mass.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/uoc-ehh032615.php){:target="_blank" rel="noopener"}


