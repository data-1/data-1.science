---
layout: post
title: "Chromosomal barcoding as a tool for multiplexed phenotypic characterization of laboratory evolved lineages"
date: 2018-08-03
categories:
author: "Jahn, Leonie Johanna, Novo Nordisk Foundation Center For Biosustainability, Technical University Of Denmark, Kongens Lyngby, Porse, Munck, Department Of Systems Biology, Columbia University, New York"
tags: []
---


For the ALE experiment, we inoculated each replicate with a uniquely barcoded clone. The most resistant clones for each drug were ~50 (AMK), ~60 (FEP) and ~30 (DOX) fold more resistant compared to the WT, while the least resistant clones were only 5-fold more resistant than the ancestor (Fig. For AMK and DOX adapted lineages those clones were among the most resistant ones while no correlation between the resistance level and the growth kinetics could be established for FEP evolved clones. All lineages adapted to AMK and FEP were more resistant than 8 fold of the WT IC 90 of the respective drug. Each column represents the different conditions: lineages grown in the media without drug (media), media containing sub-inhibitory drug concentrations (0.25 WT IC90) or three different concentrations above the WT IC 90 (2×, 4× and 8× WT IC 90 ).

<hr>

[Visit Link](http://www.nature.com/articles/s41598-018-25201-5?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


