---
layout: post
title: "Gene associated with thinking skills: Gene underlying healthy information processing identified"
date: 2016-05-12
categories:
author: University of Mississippi Medical Center
tags: [Genetics,Ageing,Aging brain,Alzheimers disease,News aggregator,Complex traits,Neuroscience]
---


It is well known that genetic variation plays an important role in explaining individual differences in thinking skills such as memory and information processing speed, said Dr. Tom Mosley, director of the Memory Impairment and Neurodegenerative Dementia (MIND) Center at UMMC and senior scientist on the study. In addition, they examined genetic variations across 2.5 million sites along each individual's DNA, looking for associations between genetic variants and performance on several different tests of cognitive function. Of the different cognitive skills examined, the strongest genetic association was related to performance on a test of information processing speed. We are finding that for complex traits, like cognitive function, not a single gene, but several genes or genetic regions come into play, with each making a relatively small contribution, Mosley said. Mosley said the study complements two other discoveries by the CHARGE team that identified genetic variants associated with both memory performance and general cognitive functioning in older adults.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150715103558.htm){:target="_blank" rel="noopener"}


