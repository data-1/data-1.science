---
layout: post
title: "Celebrating 100 years of crystallography"
date: 2016-02-05
categories:
author: American Chemical Society
tags: [American Chemical Society,American Association for the Advancement of Science,Chemistry,Physical sciences,Science]
---


To commemorate the 100th anniversary of a revolutionary technique that underpins much of modern science, Chemical & Engineering News (C&EN) magazine last week released a special edition on X-ray crystallography — its past, present and a tantalizing glimpse of its future. C&EN is the weekly news magazine of the American Chemical Society (ACS), the world's largest scientific society. The technique got its start when German physicist Max von Laue published the first paper on X-ray diffraction from a crystal in 1912. In the century following von Laue's discovery, which was recognized with a Nobel Prize in 1914, scientists went on to use crystallography to describe hundreds of thousands of molecular structures, influencing every corner of science from chemistry to biology, from the Earth to outer space. The issue leads readers on a multimedia journey through crystallography's far-reaching influence through key molecules, from table salt to DNA, that the technique was able to see when no other method could.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/acs-c1y082014.php){:target="_blank" rel="noopener"}


