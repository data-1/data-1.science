---
layout: post
title: "Rapid evolution of body size enhanced dinosaur and bird survival"
date: 2015-11-17
categories:
author: Jon Tennant, Plos Blogs
tags: [Bird,Dinosaur,Evolution,Allometry,Pterosaur,Organism,Species,Biology,Animals]
---


Therefore, being able to accurately estimate body mass in extinct birds has important implications for our understanding of the origins of flight. What they found, using a range of analyses and datasets, was a strong positive association between body mass and skeletal mass, as we might expect – as the skeleton of an animal gets bigger, so does its overall mass. What does this mean overall for estimating body sizes in extinct organisms? Sexual variation between total body mass vs. skeletal mass relationships in birds. Exploring the Relationship between Skeletal Mass and Total Body Mass in Birds, PLOS ONE (2015).

<hr>

[Visit Link](http://phys.org/news/2015-11-rapid-evolution-body-size-dinosaur.html){:target="_blank" rel="noopener"}


