---
layout: post
title: "Fossil of multicellular life moves evolutionary needle back 60 million years"
date: 2015-07-16
categories:
author: Virginia Tech 
tags: [Multicellular organism,Fossil,Cell (biology),Animal,Life,Doushantuo Formation,Nature,Biology,Biological evolution,Organisms]
---


A Virginia Tech geobiologist with collaborators from the Chinese Academy of Sciences have found evidence in the fossil record that complex multicellularity appeared in living things about 600 million years ago – nearly 60 million years before skeletal animals appeared during a huge growth spurt of new life on Earth known as the Cambrian Explosion. This opens up a new door for us to shine some light on the timing and evolutionary steps that were taken by multicellular organisms that would eventually go on to dominate the Earth in a very visible way, said Shuhai Xiao, a professor of geobiology in the Virginia Tech College of Science. Fossils similar to these have been interpreted as bacteria, single-cell eukaryotes, algae, and transitional forms related to modern animals such as sponges, sea anemones, or bilaterally symmetrical animals. The complex multicellularity evident in the fossils is inconsistent with the simpler forms such as bacteria and single-celled life typically expected 600 million years ago. He worked for three years at Tulane University before arriving at Virginia Tech in 2003.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-09/vt-fom092414.php){:target="_blank" rel="noopener"}


