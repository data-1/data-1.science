---
layout: post
title: "Machine learning helps to predict the treatment outcomes of schizophrenia: Building better tools for diagnosis and treatment of mental health disorders"
date: 2018-07-14
categories:
author: "University of Alberta Faculty of Medicine & Dentistry"
tags: [Schizophrenia,Psychiatry,Mental disorder,Health,Health care,Psychology,Medical specialties,Causes of death,Cognitive science,Neuroscience,Abnormal psychology,Health sciences,Human diseases and disorders,Medicine,Diseases and disorders,Mental health,Mental disorders,Clinical medicine]
---


Could the diagnosis and treatment of mental health disorders one day be aided through the help of machine learning? The research was led by Bo Cao at the U of A's Department of Psychiatry, with the collaboration of Xiang Yang Zhang at the University of Texas Health Science Center at Houston. They used a machine-learning algorithm to examine functional magnetic resonance imaging (MRI) images of both newly diagnosed, previously untreated schizophrenia patients and healthy subjects. In the future, with the help of machine learning, if the doctor can select the best medicine or procedure for a specific patient at the first visit, it would be a good step forward. According to Cao, early diagnosis of schizophrenia and many mental disorders is an ongoing challenge.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/07/180712141053.htm){:target="_blank" rel="noopener"}


