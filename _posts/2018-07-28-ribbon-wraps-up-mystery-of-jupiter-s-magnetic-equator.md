---
layout: post
title: "'Ribbon' wraps up mystery of Jupiter's magnetic equator"
date: 2018-07-28
categories:
author: "University of Leicester"
tags: [Jupiter,Equator,Aurora,Juno (spacecraft),Ionosphere,Planet,Magnetosphere of Jupiter,Poles of astronomical bodies,Earth,Magnetism,Magnetic dip,Astronomy,Astronomical objects,Planets,Space science,Nature,Physical sciences,Planets of the Solar System,Outer space,Science,Planetary science]
---


Study shows first evidence of an ionospheric interaction with Jupiter's equatorial magnetic field  Contrary to past theories, the magnetic equator is surprisingly simple but the ionosphere between the equator and the pole is very complex  Findings complement recent data from NASA's Juno mission  The discovery of a dark ribbon of weak hydrogen ion emissions that encircles Jupiter has overturned previous thinking about the giant planet's magnetic equator. Lead author Dr Tom Stallard, Associate Professor in Planetary Astronomy from the University of Leicester, said: The first time we saw the dark ribbon winding its way around Jupiter in our data, we felt sure we were seeing something special at Jupiter. It was a great relief to us that a few months before our paper was published the first magnetic model of Jupiter was released from the Juno spacecraft, providing an unprecedented view of Jupiter's equatorial magnetic field, and the measured magnetic equator lined up almost exactly with our dark ribbon of emission. Our measurements also support that, because even though the equator is surprisingly simple, we see lots of complexity in the ionosphere between the equator and the pole. This suggests Jupiter's magnetic field in these regions is much more complex than that of Earth.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/uol-wu072018.php){:target="_blank" rel="noopener"}


