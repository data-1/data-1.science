---
layout: post
title: "New insight into how brain makes memories"
date: 2016-04-23
categories:
author: Vanderbilt University
tags: [Dendrite,Dendritic spine,Axon,Neuron,Neuroscience,Cell biology,Anatomy]
---


Every time you make a memory, somewhere in your brain a tiny filament reaches out from one neuron and forms an electrochemical connection to a neighboring neuron. A team of biologists at Vanderbilt University, headed by Associate Professor of Biological Sciences Donna Webb, studies how these connections are formed at the molecular and cellular level. The filaments that make these new connections are called dendritic spines and, in a series of experiments described in the April 17 issue of the Journal of Biological Chemistry, the researchers report that a specific signaling protein, Asef2, a member of a family of proteins that regulate cell migration and adhesion, plays a critical role in spine formation. Autism has been associated with immature spines, which do not connect properly with axons to form new synaptic junctions. Visit Research News @ Vanderbilt for more research news from Vanderbilt.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/vu-nii042315.php){:target="_blank" rel="noopener"}


