---
layout: post
title: "Neanderthal genes gave modern humans an immunity boost, allergies"
date: 2016-01-29
categories:
author: Cell Press
tags: [Interbreeding between archaic and modern humans,Denisovan,Gene,Toll-like receptor,Evolution,Human genome,Neanderthal,Genetics,Biology]
---


We found that interbreeding with archaic humans--the Neanderthals and Denisovans--has influenced the genetic diversity in present-day genomes at three innate immunity genes belonging to the human Toll-like-receptor family, says Janet Kelso of the Max Planck Institute for Evolutionary Anthropology in Leipzig, Germany. Both new studies highlight the functional importance of this inheritance on Toll-like receptor (TLR) genes--TLR1, TLR6, and TLR10. Quintana-Murci and his colleagues set out to explore the evolution of the innate immune system over time. The archaic-like variants are associated with an increase in the activity of the TLR genes and with greater reactivity to pathogens. The American Journal of Human Genetics, published by Cell Press for the American Society of Human Genetics, is a monthly journal that provides a record of research and review relating to heredity in humans and to the application of genetic principles in medicine and public policy, as well as in related areas of molecular and cell biology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/cp-ngg123015.php){:target="_blank" rel="noopener"}


