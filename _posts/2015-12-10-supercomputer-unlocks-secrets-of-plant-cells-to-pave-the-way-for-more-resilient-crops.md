---
layout: post
title: "Supercomputer unlocks secrets of plant cells to pave the way for more resilient crops"
date: 2015-12-10
categories:
author: University of Melbourne
tags: [Cellulose,American Association for the Advancement of Science,Biology,Plant,Cell wall,Science,Technology]
---


Scientists from IBM Research and the Universities of Melbourne and Queensland have moved a step closer to identifying the nanostructure of cellulose -- the basic structural component of plant cell walls. The insights could pave the way for more disease resistant varieties of crops and increase the sustainability of the pulp, paper and fibre industry -- one of the main uses of cellulose. We are a keen supporter of the Victorian Life Sciences Computation Initiative and we're very excited to see the scientific impact this work is now having,  Using the IBM Blue Gene/Q supercomputer at VLSCI, known as Avoca, scientists were able to perform the quadrillions of calculations required to model the motions of cellulose atoms. Thanks to IBM's expertise in molecular modelling and VLSCI's computational power, we have been able to create models of the plant wall at the molecular level which will lead to new levels of understanding about the formation of cellulose. While we don't fully understand the molecular pathway of pathogen infection and plant response, we are exploring ways to manipulate the composition of the wall in order to make it more resistant to disease,  ###  The work was undertaken by biologists at the Australian Research Council (ARC) Centre of Excellence in Plant Cell Walls within the universities of Melbourne and Queensland, in partnership with the IBM Research Collaboratory for Life Sciences.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/uom-sus051915.php){:target="_blank" rel="noopener"}


