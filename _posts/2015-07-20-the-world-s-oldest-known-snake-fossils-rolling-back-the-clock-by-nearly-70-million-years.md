---
layout: post
title: "The world's oldest known snake fossils: Rolling back the clock by nearly 70 million years"
date: 2015-07-20
categories:
author: University of Alberta 
tags: [Snake,Paleontology,Fossil,Anatomy,Taxa]
---


(Edmonton, AB) - Fossilized remains of four ancient snakes have been dated between 140 and 167 million years old - nearly 70 million years older than the previous record of ancient snake fossils - and are changing the way we think about the origins of snakes, and how and when it happened. The findings have been published in the prestigious peer-reviewed journal Nature Communications. The study explores the idea that evolution within the group called 'snakes' is much more complex than previously thought, says lead author and professor Michael Caldwell in the Faculty of Science at the University of Alberta. This new study makes it clear that the sudden appearance of snakes, some 100 million years ago, reflects a gap in the fossil record, not an explosive radiation of early snakes. Based on the new evidence and through comparison to living legless lizards that are not snakes, explains Caldwell, the paper explores the novel idea that the evolution of the characteristic snake skull and its parts appeared long before snakes lost their legs.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/uoa-two012615.php){:target="_blank" rel="noopener"}


