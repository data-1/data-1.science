---
layout: post
title: "Particle jets reveal the secrets of the most exotic state of matter"
date: 2016-04-17
categories:
author: The Henryk Niewodniczanski Institute of Nuclear Physics Polish Academy of Sciences
tags: [Quarkgluon plasma,ATLAS experiment,Large Hadron Collider,Particle physics,CERN,Physics,Physical sciences,Nature,Nuclear physics,Science,Applied and interdisciplinary physics]
---


Analysis of the streams of particles penetrating the plasma has led to new findings about the properties of the plasma, and was recently published in the prestigious journal Physical Review Letters by the international team of physicists working at the ATLAS detector. After gathering the data on the reconstructed jets in the collision of lead nuclei, the team of physicists can correlate and compare the results with those obtained from proton-proton collisions. In turn, theoretical models of heavy ions in collision predict the formation of dense plasma in a head-on ion-ion collision of extremely high energy. It is an important result, because it allows us to discard some of the theoretical models of quark-gluon plasma which do not provide for such a high rate of suppression, explains Prof. Wosiek. The Henryk Niewodniczanski Institute of Nuclear Physics (IFJ PAN) is currently the largest research institute of the Polish Academy of Sciences.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/thni-pjr031115.php){:target="_blank" rel="noopener"}


