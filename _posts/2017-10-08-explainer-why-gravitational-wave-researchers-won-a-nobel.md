---
layout: post
title: "Explainer: Why gravitational wave researchers won a Nobel"
date: 2017-10-08
categories:
author: "Seth Borenstein"
tags: [Gravitational wave,Barry Barish,Spacetime,LIGO,Universe,Physics,Science,Physical sciences,Astronomy,Space science,Theory of relativity,Astrophysics,Physical cosmology]
---


Scientists Barry Barish, left, and Kip Thorne, both of the California Institute of Technology, share a toast to celebrate winning the Nobel Prize in Physics Tuesday, Oct. 3, 2017, in Pasadena, Calif. Barish and Thorne won the Nobel Physics Prize on Tuesday for detecting faint ripples flying through the universe, the gravitational waves predicted a century ago by Albert Einstein that provide a new understanding of the universe. WHAT IS A GRAVITATIONAL WAVE? And when massive but compact objects like black holes or neutron stars collide, their immense gravity causes space-time to stretch or compress. Weiss, of the Massachusetts Institute of Technology, is one of three awarded this year's Nobel Prize in physics for their discoveries in gravitational waves. Other types of gravitational detectors are being built including one in India.

<hr>

[Visit Link](https://phys.org/news/2017-10-gravitational-won-nobel.html){:target="_blank" rel="noopener"}


