---
layout: post
title: "Study reveals evolution of malaria"
date: 2016-03-24
categories:
author: Field Museum
tags: [Malaria,Parasitism,Evolution,Species,Plasmodium,Bat,Genetics,Host (biology),Molecular phylogenetics,DNA,Mammal,Biology]
---


Credit: © Holly Lutz  Malaria affects close to 500 million people every year, but we're not the only ones—different species of malaria parasite can infect birds, bats, and other mammals too. Different species of malaria live in different species of host animals. The analysis revealed that malaria has its roots in bird hosts, from which it spread to bats, and then on to other mammals. It's not that bats are spreading malaria—we get different species of malaria than they do, and we can't get it from them, explained Lutz. In learning more about how parasites live within their hosts, who is infecting who, and how these organisms coexist in these living, breathing ecosystems, we can learn more about how they are connected to and affected by the natural environments that we share with animals and plants.

<hr>

[Visit Link](http://phys.org/news/2016-03-reveals-evolution-malaria.html){:target="_blank" rel="noopener"}


