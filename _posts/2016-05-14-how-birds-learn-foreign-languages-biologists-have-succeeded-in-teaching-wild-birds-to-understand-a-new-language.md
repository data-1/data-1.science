---
layout: post
title: "How birds learn foreign languages: Biologists have succeeded in teaching wild birds to understand a new language"
date: 2016-05-14
categories:
author: Australian National University
tags: [News aggregator,Communication,Branches of science,Science,Cognitive science,Cognition]
---


Biologists have succeeded in teaching wild birds to understand a new language. After only two days of training, fairy wrens learnt to flee when they heard an alarm call that was foreign to them, showing that birds can learn to eavesdrop on the calls of other species. The research, led by biologists at The Australian National University (ANU), could be used to help train captive animals to recognise signals of danger before they are released in to the wild. It's like understanding multiple foreign languages, Professor Magrath said. After only eight playbacks the birds had learned to flee, while they did not flee when played unfamiliar sounds that had not been paired with the gliders.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150716124120.htm){:target="_blank" rel="noopener"}


