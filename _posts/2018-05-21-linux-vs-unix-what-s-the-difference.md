---
layout: post
title: "Linux vs. Unix: What's the difference?"
date: 2018-05-21
categories:
author: "Phil Estes"
tags: [Linux,Unix,Berkeley Software Distribution,GNU,Linux distribution,Operating system,UNIX System V,Free and open-source software,Ken Thompson,Operating system families,Computer science,Computer engineering,System software,Software engineering,Free software,Open-source movement,Unix variants,Free content,Intellectual works,Computing,Software,Technology,Software development,Computer architecture,Computers]
---


So, what is this Unix? Linux distributions came to life with the components of GNU, the Linux kernel, MIT's X-Windows GUI, and other BSD components that could be used under the open source BSD license. Commercial Linux offerings, which provide support on top of the free and open source components, became viable as many enterprises, including IBM, migrated from proprietary Unix to offering middleware and software solutions atop Linux. The remaining differences between Linux and Unix are mainly related to the licensing model: open source vs. proprietary, licensed software. Also, the BSD branch of the Unix tree is open source, and NetBSD, OpenBSD, and FreeBSD all have strong user bases and open source communities that may not be as visible or active as Linux, but are holding their own in recent server share reports, with well above the proprietary Unix numbers in areas like web serving.

<hr>

[Visit Link](https://opensource.com/article/18/5/differences-between-linux-and-unix){:target="_blank" rel="noopener"}


