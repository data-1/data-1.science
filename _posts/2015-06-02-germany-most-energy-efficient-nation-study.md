---
layout: post
title: "Germany most energy efficient nation: study"
date: 2015-06-02
categories:
author: ""   
tags: []
---


Germany most energy efficient nation: study    by Staff Writers    Washington (AFP) July 17, 2014    Germany is the world's most energy efficient nation with strong codes on buildings while China is quickly stepping up its own efforts, an environmental group said Thursday. We are pleased to win a second title in a week's time, Philipp Ackermann, the deputy chief of mission at the German embassy in Washington, told a conference call, alluding to his country's World Cup victory. Echoing the views of the report's authors, Ackermann pointed out that Germany has achieved economic growth while improving efficiency and reducing harmful environmental effects of the energy trade. Australia was ranked 10th, with the council praising the country's efforts on building construction and manufacturing but placing it last on energy efficiency in transportation. The study ranked the United States in 13th place, saying that the world's largest economy has made progress but on a national level still wastes a tremendous amount of energy.

<hr>

[Visit Link](http://feeds.importantmedia.org/~r/IM-cleantechnica/~3/xHEfhwxG7y4/){:target="_blank" rel="noopener"}


