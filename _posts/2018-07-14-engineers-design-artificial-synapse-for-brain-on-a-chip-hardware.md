---
layout: post
title: "Engineers design artificial synapse for “brain-on-a-chip” hardware"
date: 2018-07-14
categories:
author: "Jennifer Chu"
tags: [Neural network,Neuromorphic engineering,Artificial neural network,Handwriting recognition,Brain,Technology,Computing,Computer science,Neuroscience,Branches of science]
---


Now engineers at MIT have designed an artificial synapse in such a way that they can precisely control the strength of an electric current flowing across it, similar to the way ions flow between neurons. They applied voltage to each synapse and found that all synapses exhibited more or less the same current, or flow of ions, with about a 4 percent variation between synapses — a much more uniform performance compared with synapses made from amorphous material. Kim and his colleagues ran a computer simulation of an artificial neural network consisting of three sheets of neural layers connected via two layers of artificial synapses, the properties of which they based on measurements from their actual neuromorphic chip. The team is in the process of fabricating a working neuromorphic chip that can carry out handwriting-recognition tasks, not in simulation but in reality. Looking beyond handwriting, Kim says the team’s artificial synapse design will enable much smaller, portable neural network devices that can perform complex computations that currently are only possible with large supercomputers.

<hr>

[Visit Link](http://news.mit.edu/2018/engineers-design-artificial-synapse-brain-on-a-chip-hardware-0122){:target="_blank" rel="noopener"}


