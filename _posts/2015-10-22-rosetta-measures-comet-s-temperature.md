---
layout: post
title: "Rosetta measures comet's temperature"
date: 2015-10-22
categories:
author: European Space Agency
tags: [Rosetta (spacecraft),Comet,Lander (spacecraft),67PChuryumovGerasimenko,Philae (spacecraft),Sun,Space science,Outer space,Astronomy,Solar System,Bodies of the Solar System,Planetary science,Astronomical objects,Spaceflight,Local Interstellar Cloud]
---


The observations were made by the spacecraft’s visible, infrared and thermal imaging spectrometer, VIRTIS, and revealed an average surface temperature of –70ºC. Credit: ESA  (Phys.org) —ESA's Rosetta spacecraft has made its first temperature measurements of its target comet, finding that it is too hot to be covered in ice and must instead have a dark, dusty crust. The observations of comet 67P/Churyumov–Gerasimenko were made by Rosetta's visible, infrared and thermal imaging spectrometer, VIRTIS, between 13 and 21 July, when Rosetta closed in from 14 000 km to the comet to just over 5000 km. But, using the sensor to collect infrared light emitted by the whole comet, scientists determined that its average surface temperature is about –70ºC. Rosetta will be the first mission in history to rendezvous with a comet, escort it as it orbits the Sun, and deploy a lander.

<hr>

[Visit Link](http://phys.org/news326111431.html){:target="_blank" rel="noopener"}


