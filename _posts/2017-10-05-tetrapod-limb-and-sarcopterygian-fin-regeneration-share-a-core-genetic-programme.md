---
layout: post
title: "Tetrapod limb and sarcopterygian fin regeneration share a core genetic programme"
date: 2017-10-05
categories:
author: "Nogueira, Acacio F., Instituto De Ciências Biológicas, Universidade Federal Do Pará, Belém, Costa, Carinne M., Lorena, Moreira, Rodrigo N."
tags: []
---


Library preparation and Illumina sequencing  Total RNA extraction from FB or NRF tissue for transcriptome or qPCR was achieved using TRIzol Reagent (Life Technologies) according to the manufacturer’s protocol. The differential gene expression test was performed using t-test of CLC genomic workbench software, considering two conditions (NRF and FB) and three independent biological replicates. Reverse transcription-PCR verification of lungfish LSGs  Total RNA was isolated from heart tissue using TRIzol Reagent (Life Technologies) according to the manufacturer’s protocol. LSG expression in NRF was used as a reference to obtain relative expression levels the other tissues assayed. Data availability  Sequence data that support the findings of this study have been deposited in GenBank with the following BioProject accession numbers: PRJNA301439, three from FB libraries (SRX1411321, SRX1411322 and SRX1411324) and three from NRF libraries (SRX1411325, SRX1411326 and SRX1411327).

<hr>

[Visit Link](http://www.nature.com/ncomms/2016/161102/ncomms13364/full/ncomms13364.html?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


