---
layout: post
title: "Study identifies first-ever human population adaptation to toxic chemical, arsenic"
date: 2015-12-07
categories:
author: SMBE Journals (Molecular Biology and Evolution and Genome Biology and Evolution) 
tags: [Arsenic,Evolution,Biology,Biochemistry,Chemistry,Life sciences,Genetics,Biotechnology]
---


High up in the high Andes mountains of Argentina, researchers have identified the first-ever evidence of a population uniquely adapted to tolerate the toxic chemical arsenic. In a new study published in the advanced online edition of Molecular Biology and Evolution, a Swedish research team led by Karolinska Institutet and Uppsala University professor Karin Broberg, performed a genome wide survey from a group of 124 Andean women screened for the ability to metabolize arsenic (measured by levels in the urine). The study pinpointed a key set of nucleotide variants in a gene, AS3MT, which were at much lower frequencies in control populations from Columbia and Peru. The researchers estimate that the increase in frequency of these variants occurred recently, between 10,000-7,000 years ago, based on the age of a recently excavated mummy that was found to have high arsenic levels in its hair. The set of AS3MT nucleotide variants, harbored on chromosome 10, were distributed worldwide, with the highest frequencies in Peruvians, Native Americans, Eastern Asia and Vietnam.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/mbae-sif022715.php){:target="_blank" rel="noopener"}


