---
layout: post
title: "Sentinel-5P prepared for liftoff"
date: 2017-11-19
categories:
author: ""
tags: [Sentinel-5,Copernicus Programme,Outer space,Nature,Atmosphere,Earth sciences]
---


This timelapse video shows Sentinel-5P satellite, from final preparations to liftoff on a Rockot launcher, from the Plesetsk Cosmodrome in northern Russia, on 13 October 2017. The Sentinels are a fleet of satellites designed to deliver the wealth of data and imagery that are central to the European Commission’s Copernicus programme. This unique environmental monitoring programme is providing a step change in the way we view and manage our environment, understand and tackle the effects of climate change and safeguard everyday lives. Information from this new mission will be used through the Copernicus Atmosphere Monitoring Service for air quality forecasts and for decision-making. In the future, both the geostationary Sentinel-4 and polar-orbiting Sentinel-5 missions will monitor the composition of the atmosphere for Copernicus Atmosphere Services.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2017/10/Sentinel-5P_prepared_for_liftoff){:target="_blank" rel="noopener"}


