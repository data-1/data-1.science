---
layout: post
title: "Physicists determine 3-D positions of individual atoms for the first time: Finding will help scientists better understand the structural properties of materials"
date: 2016-07-11
categories:
author: "University of California - Los Angeles"
tags: [Transmission electron microscopy,Scanning transmission electron microscopy,Electron,Crystallography,Atom,X-ray crystallography,News aggregator,Microscope,Atomic molecular and optical physics,Optics,Technology,Physical chemistry,Applied and interdisciplinary physics,Chemistry,Science,Physical sciences]
---


Now, scientists at UCLA have used a powerful microscope to image the three-dimensional positions of individual atoms to a precision of 19 trillionths of a meter, which is several times smaller than a hydrogen atom. However, X-ray crystallography only yields information about the average positions of many billions of atoms in the crystal, and not about individual atoms' precise coordinates. advertisement  Miao and his team used a technique known as scanning transmission electron microscopy, in which a beam of electrons smaller than the size of a hydrogen atom is scanned over a sample and measures how many electrons interact with the atoms at each scan position. As the sample was tilted 62 times, the researchers were able to slowly assemble a 3-D model of 3,769 atoms in the tip of the tungsten sample. Miao and his team showed that the atoms in the tip of the tungsten sample were arranged in nine layers, the sixth of which contained a point defect.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150921182111.htm){:target="_blank" rel="noopener"}


