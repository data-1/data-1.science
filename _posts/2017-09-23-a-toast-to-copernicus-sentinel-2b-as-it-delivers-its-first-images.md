---
layout: post
title: "A ‘toast’ to Copernicus Sentinel-2B as it delivers its first images"
date: 2017-09-23
categories:
author: ""
tags: [Copernicus Programme,Sentinel-2,Spacecraft,Space science,Spaceflight,Outer space,Satellites,Astronautics,Remote sensing,Science,Bodies of the Solar System,Flight]
---


Crotone, Italy The multispectral imager is being calibrated during the commissioning phase – which will take about three months. Sentinel-2B is the second in the two-satellite mission for Europe’s Copernicus programme. “Sentinel-2B will be one of the workhorses of Copernicus, as it will enable a whole range of applications with a focus on land,” said Josef Aschbacher, Director of ESA’s Earth Observation Programmes. “With the second Sentinel-2 satellite in orbit, we now have much better coverage – which is especially important for monitoring areas frequently covered by clouds. “This will further improve data availability of the Copernicus Services and increase the usage of the Copernicus data.”  Karavasta Lagoon, Albania “The Copernicus Services in the areas of land and coastal monitoring rely on Sentinel-2 data,” said Philippe Brunet, Director for Space Policy, Copernicus and Defence at the European Commission.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-2/A_toast_to_Copernicus_Sentinel-2B_as_it_delivers_its_first_images){:target="_blank" rel="noopener"}


