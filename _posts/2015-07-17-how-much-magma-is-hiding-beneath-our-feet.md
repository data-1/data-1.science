---
layout: post
title: "How much magma is hiding beneath our feet?"
date: 2015-07-17
categories:
author: Université de Genève 
tags: [Magma,Volcano,Magma chamber,Geology,Earth sciences,Nature,Planetary science,Volcanology]
---


Our understanding of these phenomena is, however, limited by the fact that most magma cools and solidifies several kilometres beneath our feet, only to be exposed at the surface, millions of years later, by erosion. Scientists have never been able to track the movements of magma at such great depths… that is, until a team from the University of Geneva (UNIGE) discovered an innovative technique, details of which will be published in the next issue of the journal Nature. It is a story of three scientists: a modelling specialist, an expert in a tiny mineral known as zircon, and a volcanologist. «The zircon crystals that are found in solidified magma hold key information about the injection of molten rock into a magma chamber before it freezes underground,» explains the professor. As Guy Simpson, a researcher at UNIGE further explains: «Modelling meant that we could establish how the age of crystallised zircon in a cooled magma reservoir depends on the flow rate of injected magma and the size of the reservoir.»  Applications for society and industry  In the Nature article, the researchers propose a model that is capable of determining with unprecedented accuracy the age, volume and injection rate of magma that has accumulated at inaccessible depths.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/udg-hmm071714.php){:target="_blank" rel="noopener"}


