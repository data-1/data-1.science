---
layout: post
title: "Temperature drives biodiversity"
date: 2017-03-17
categories:
author: "University of Würzburg"
tags: [Ecology,Biodiversity,Research,Hypothesis,Species,Physical geography,Biogeochemistry,Nature,Science,Natural environment,Earth sciences]
---


The diversity of plants and animals in Earth's arctic regions is moderate. Overall, the team examined eight groups of plants and 17 groups of animals, from bees to bats. Diversity increases with temperature  The study revealed that biodiversity in communities is mainly determined by temperature. The warmer it is, the greater the diversity. The scientists believe that this is strong evidence supporting the assumption that temperature is actually more decisive for distribution patterns of overall biodiversity than productivity or size of habitats.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-12/uow-tdb121916.php){:target="_blank" rel="noopener"}


