---
layout: post
title: "Predicting solar flares"
date: 2015-11-22
categories:
author: Queen'S University Belfast
tags: [Sun,Space weather,Solar flare,Space science,Astronomy,Nature,Physical sciences,Science,Outer space]
---


Experts have warned that even a single 'monster' solar flare could cause up to $2 trillion worth of damage on Earth, including the loss of satellites and electricity grids, as well the potential knock-on dangers to human life and health. Lead researcher Dr. David Jess from Queen's Astrophysics Research Centre said: Continual outbursts from our Sun, in the form of solar flares and associated space weather, represent the potentially destructive nature of our nearest star. Queen's is increasingly becoming a major player on the astrophysics global stage. The waves were found to propagate with speeds approaching half a million (500,000) mph, and when coupled with temperatures of around 1,000,000 degrees in the Sun's outer atmosphere, the researchers were able to determine the magnetic field strengths to a high degree of precision  (3) The strength of the magnetic fields decreases by a factor of 100 as they travel from the surface of the Sun out into the tenuous, hot corona (the region of the Sun's atmosphere visible during total solar eclipses). The team's methods provide a much faster way of examining magnetic field changes in the lead up to solar flares, which can ultimately be used to provide advanced warning against such violent space weather  Explore further Image: Supercomputer simulation of magnetic field loops on the Sun

<hr>

[Visit Link](http://phys.org/news/2015-11-solar-flares.html){:target="_blank" rel="noopener"}


