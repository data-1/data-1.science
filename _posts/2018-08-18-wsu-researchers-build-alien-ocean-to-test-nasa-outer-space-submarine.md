---
layout: post
title: "WSU researchers build alien ocean to test NASA outer space submarine"
date: 2018-08-18
categories:
author: "Washington State University"
tags: [Titan (moon),Cryogenics,Space science,Planetary science,Science,Astronomy,Physical sciences,Outer space]
---


In WSU's cryogenic lab, which studies materials at very cold temperatures, Richardson re-created the atmosphere of Titan and tested how a small heated machine might work under such conditions. Simulating Titan seas  The WSU research team built a test chamber that housed the liquid mixture at very cold temperatures to simulate the seas of Titan. One of the biggest challenges for researchers was understanding bubbles in the Titan seas. Shooting video at -300 degrees  The next big problem, said Richardson, was getting a video in difficult conditions. The researchers are looking to continue the work with NASA to update the Titan Submarine design.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/wsu-wrb020718.php){:target="_blank" rel="noopener"}


