---
layout: post
title: "Genetic Algorithms with Apache Ignite"
date: 2018-07-18
categories:
author: "Jun."
tags: []
---


In Apache Ignite, GAs are part of the larger machine learning library. In Figure 3 (from the Apache Ignite GA documentation), we can see the sequence of operations when using a GA. Therefore, we can set the Chromosome length to 11, as follows:  gaConfig.setChromosomeLength(11);  Fitness Calculation  To compare different solutions and find the optimal one, we need to determine a fitness score for each chromosome. Termination Condition  In our example, once a chromosome matches our target of HELLO WORLD, we have achieved our goal of finding the optimal solution. Some example output is shown below:  ########################################################################################## Generation: 255 Fittest is Chromosome Key: Chromosome [fitnessScore=11.0, id=131, genes=[8, 5, 12, 12, 15, 27, 23, 15, 18, 12, 4]] Chromosome: Chromosome [fitnessScore=11.0, id=131, genes=[8, 5, 12, 12, 15, 27, 23, 15, 18, 12, 4]] HELLO WORLD Avg Chromosome Fitness: 5.668 ##########################################################################################  Web Console  We can also run some GA queries using the Apache Ignite Web Console.

<hr>

[Visit Link](https://dzone.com/articles/genetic-algorithms-with-apache-ignite?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


