---
layout: post
title: "New study sheds light on how earliest forms of life evolved on Earth"
date: 2017-11-19
categories:
author: "Australian National University"
tags: [Life,Abiogenesis,Physical sciences,Chemistry,Nature,Science]
---


In a major advance on previous work, the study found a compound commonly used in hair bleach, hydrogen peroxide, made the eventual emergence of life possible. Lead researcher Associate Professor Rowena Ball from ANU said hydrogen peroxide was the vital ingredient in rock pores around underwater heat vents that set in train a sequence of chemical reactions that led to the first forms of life. The research team made a model using hydrogen peroxide and porous rock that simulated the dynamic, messy environment that hosted the origin of life. Hydrogen peroxide played multiple roles in the emergence of living systems, and this study investigated how it ensured the randomly fluctuating temperatures and pH levels necessary to energise the production of a chemical world that made life on Earth possible, Dr Ball said. Any chance of rebuilding that bridge was permanently rubbed out by the persistence of catalases throughout subsequent evolution.

<hr>

[Visit Link](https://phys.org/news/2017-11-earliest-life-evolved-earth.html){:target="_blank" rel="noopener"}


