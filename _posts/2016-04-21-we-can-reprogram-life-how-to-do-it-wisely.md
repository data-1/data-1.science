---
layout: post
title: "We can reprogram life. How to do it wisely"
date: 2016-04-21
categories:
author: Juan Enriquez
tags: [TED (conference),Human,Biology,Biological evolution,Evolutionary biology]
---


For four billion years, what lived and died on Earth depended on two principles: natural selection and random mutation. Then humans came along and changed everything — hybridizing plants, breeding animals, altering the environment and even purposefully evolving ourselves. Juan Enriquez provides five guidelines for a future where this ability to program life rapidly accelerates. This is the single most exciting adventure human beings have been on, Enriquez says. This is the single greatest superpower humans have ever had.

<hr>

[Visit Link](http://www.ted.com/talks/juan_enriquez_we_can_reprogram_life_how_to_do_it_wisely){:target="_blank" rel="noopener"}


