---
layout: post
title: "Scotland to ban GM crops"
date: 2016-05-17
categories:
author:  
tags: [Genetically modified food,Technology]
---


Scotland is set to ban the cultivation of genetically modified (GM) crops, officials announce  Scotland is set to ban the cultivation of genetically modified (GM) crops, officials announced Sunday. It is set to implement the move under European Union rules introduced earlier this year which allow countries to opt out of growing the crops, a statement from the Scottish government said. The Scottish government will shortly submit a request that Scotland is excluded from any European consents for the cultivation of GM crops, including the variety of genetically modified maize already approved and six other GM crops that are awaiting authorisation, the statement said. The Scottish government in Edinburgh has responsibility for some mainly domestic policies including agriculture since power was devolved from the British government in London in 1999. While no GM crops are currently grown commercially in Britain, the government in London supports the use of GM crops as long as they are assessed to be safe.

<hr>

[Visit Link](http://phys.org/news/2015-08-scotland-gm-crops.html){:target="_blank" rel="noopener"}


