---
layout: post
title: "Enlisting symmetry to protect quantum states from disruptions"
date: 2015-08-11
categories:
author: Joint Quantum Institute
tags: [Quantum mechanics,Spin (physics),Energy level,Energy,Quantum simulator,Spin-12,Ion,Symmetry,Phase (matter),Magnetism,Physics,Physical sciences,Science,Scientific theories,Applied and interdisciplinary physics,Theoretical physics]
---


In quantum mechanics, symmetry describes more than just the patterns that matter takes—it is used to classify the nature of quantum states. Using a magnetic field, internal states of each ion are tailored to represent a qutrit, with a (+) state, (-) state and (0) state denoting the three available energy levels (see figure). Changing the fields that the ions spins are exposed to causes the spins to readjust in order to remain in the lowest energy configuration. For instance, when the team added a third ion, they could not smoothly guide the system into its ground state, indicating the possible existence of a state with some additional symmetry protections. Monroe explains, These symmetry-protected states may be the only way to build a large-scale stable quantum computer in many physical systems, especially in the solid-state.

<hr>

[Visit Link](http://phys.org/news/2015-07-symmetry-quantum-states-disruptions.html){:target="_blank" rel="noopener"}


