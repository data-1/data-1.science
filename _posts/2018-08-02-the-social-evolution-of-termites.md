---
layout: post
title: "The social evolution of termites"
date: 2018-08-02
categories:
author: "University of Freiburg"
tags: [Eusociality,Biology,Biological evolution,Evolutionary biology,Evolution,Nature,Zoology]
---


This was the case with termites and ants, which have the same eusocial lifestyle. A team headed by evolutionary biologist Prof. Dr. Judith Korb from the University of Freiburg, bioinformatician Prof. Dr. Erich Bornberg-Bauer, evolutionary biologist Dr. Mark Harrison and evolutionary biologist Dr. Evelien Jongepier from the Westphalian Wilhelms University in Münster has now compared the molecular basis for the evolution of the eusocial lifestyle. The researchers showed that the termites have distinctive genetic features in the genomic regions which encode chemoreceptors - proteins involved in chemical communication. This is, therefore, a classic case of convergent evolution: both groups evolved similar molecular mechanisms for a eusocial lifestyle under similar selection pressures, says genomics expert Dr. Mark Harrison. They are associated with protein families that play a key role in chemical communication.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/uof-tse020718.php){:target="_blank" rel="noopener"}


