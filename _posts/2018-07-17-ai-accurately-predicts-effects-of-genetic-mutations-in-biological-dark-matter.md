---
layout: post
title: "AI accurately predicts effects of genetic mutations in biological dark matter"
date: 2018-07-17
categories:
author: "Simons Foundation"
tags: [Genetics,Gene,Mutation,Non-coding DNA,Genome,DNA,Gene expression,Research,Biology,Protein,Human genome,Biochemistry,Molecular biology,Life sciences,Biotechnology]
---


The new deep learning technique could help researchers identify the genetic origin of diseases and better understand how evolution shaped our genes  A new machine learning framework, dubbed ExPecto, can predict the effects of genetic mutations in the so-called dark matter regions of the human genome. Mutations in the noncoding region can sometimes cause genes to express or not express in the wrong part of your body at the wrong time, increasing the risk of diseases such as cancer. From this information, ExPecto can predict the effect of any mutation, even mutations that scientists have never seen before. Once you know which protein is affected and what the protein does, then you can design drugs that can fix the problem, says study co-author Jian Zhou, a Flatiron research fellow at CCB. Anyone can access ExPecto's predictions of the effects of more than 140 million possible mutations near protein-encoding genes.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/sf-aap071518.php){:target="_blank" rel="noopener"}


