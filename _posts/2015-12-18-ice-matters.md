---
layout: post
title: "Ice matters"
date: 2015-12-18
categories:
author: "$author" 
tags: [Climate change,Sea ice,Arctic,Ice,Arctic Ocean,Climate system,Ocean,Climate,Sea,CryoSat,Sea level rise,Geography,Hydrology,Oceanography,Hydrography,Climate variability and change,Natural environment,Earth phenomena,Earth sciences,Physical geography,Nature,Applied and interdisciplinary physics]
---


Arctic sea ice, for example, is particularly sensitive to our warming climate and is often cited as a barometer of global change. Around the North Pole, an area roughly the size of Europe melts every summer and then freezes again the following year. Arctic sea-ice thickness in October–November 2015 as measured by ESA’s CryoSat For example, in September 2007, it was discovered that the sea ice had shrunk to its lowest level since satellite measurements began, opening up the Northwest Passage, a long-sought shortcut between Europe and Asia that had been historically impassable. ESA’s CryoSat mission manager, Tommaso Parrinello, said, “By measuring the height of the ice, both of that floating in the polar oceans and of the vast ice sheets covering Greenland and Antarctica, CryoSat is providing essential information on how the ice thickness is changing. For example, we now know that between 2010 and 2012 the volume of summer sea ice decreased by around 10%.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_for_climate/Ice_matters){:target="_blank" rel="noopener"}


