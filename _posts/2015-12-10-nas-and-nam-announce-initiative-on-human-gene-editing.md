---
layout: post
title: "NAS and NAM announce initiative on human gene editing"
date: 2015-12-10
categories:
author: National Academies of Sciences, Engineering, and Medicine
tags: [Genetic engineering,Research,Asilomar Conference on Recombinant DNA,Genetics,Life sciences,Health sciences,Biology,Science,Medicine,Biotechnology,Technology]
---


WASHINGTON -- The National Academy of Sciences and the National Academy of Medicine are launching a major initiative to guide decision making about controversial new research involving human gene editing. However, recent experiments to attempt to edit human genes also have raised important questions about the potential risks and ethical concerns of altering the human germline. The committee will consider and recommend standards, guidelines, and practices governing the use of gene-editing technologies in biomedical research and medicine. In 1975, the Asilomar conference convened by the National Academy of Sciences led to guidelines for recombinant DNA research. Ralph J. Cicerone is the president of the National Academy of Sciences, a private, nonprofit institution that provides science policy advice to the nation under an 1863 congressional charter.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/naos-nan051815.php){:target="_blank" rel="noopener"}


