---
layout: post
title: "Astronomers confirm faintest early-universe galaxy ever seen"
date: 2016-05-24
categories:
author: University of California - Los Angeles
tags: [Chronology of the universe,Gravitational lens,Galaxy,Astronomy,Observatory,Hubble Space Telescope,Universe,Cosmology,Nature,Physical sciences,Astronomical objects,Natural sciences,Space science,Science,Physics,Physical cosmology,Astrophysics]
---


An international team of scientists, including two professors and three graduate students from UCLA, has detected and confirmed the faintest early-universe galaxy ever. Using the W. M. Keck Observatory on the summit on Mauna Kea in Hawaii, the researchers detected the galaxy as it was 13 billion years ago. Tommaso Treu, a professor of physics and astronomy in the UCLA College and a co-author of the research, said the discovery could be a step toward unraveling one of the biggest mysteries in astronomy: how a period known as the cosmic dark ages ended. The researchers made the discovery using an effect called gravitational lensing to see the incredibly faint object, which was born just after the Big Bang. The detected galaxy was behind a galaxy cluster known as MACS2129.4-0741, which is massive enough to create three different images of the galaxy.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/uoc--acf052316.php){:target="_blank" rel="noopener"}


