---
layout: post
title: "Proteins for anxiety in humans and moulting in insects have common origin"
date: 2016-04-23
categories:
author: Queen Mary, University Of London
tags: [Protein,Evolution,Neuropeptide,Common descent,Brain,Cell signaling,Biology]
---


Sea urchin. Credit: MR Elphick/QMUL  Neuropeptides are small proteins in the brains of all animals that bind to receptor proteins and cause activity in cells. The last common ancestor of humans, sea urchins and insects probably lived over 600 million years ago but we'll almost certainly never know what it looked like or even find an example of it in the fossil record but we can tell a lot about it by looking at genes and proteins in its evolutionary descendants. For this reason, as with this discovery, they can help us determine the evolutionary history and origins of important molecules in our brain. Credit: MR Elphick/QMUL  Explore further Complex nerve-cell signaling traced back to common ancestor of humans and sea anemones  More information: Semmens DC, Beets I, Rowe ML, Blowes LM, Oliveri P, Elphick MR. 2015, Discovery of sea urchin NGFFFamide receptor unites a bilaterian neuropeptide family.

<hr>

[Visit Link](http://phys.org/news348855151.html){:target="_blank" rel="noopener"}


