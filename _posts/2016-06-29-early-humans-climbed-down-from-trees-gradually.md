---
layout: post
title: "Early Humans Climbed Down from Trees Gradually"
date: 2016-06-29
categories:
author: "Charles Q. Choi"
tags: [Australopithecus,Homininae,Human evolution,Homo,Human,Knuckle-walking,Chimpanzee,Ape,Animals,Mammalogy,Pliocene,Catarrhini,Primates,Biological anthropology,Apes]
---


The last common ancestor of humans and chimpanzees may have had shoulders that were similar to those of modern African apes, researchers say. [See Images of Our Closest Human Ancestor]  A lot of people use chimpanzees as a model for the last common ancestor, Young told Live Science. (Image credit: Courtesy of Nathan Young)  Humans aren't the only species that have evolved and changed over time — chimpanzees and gorillas have evolved and changed over time, too, so looking at their modern forms for insights into what the last common ancestor was like could be misleading in a lot of ways, Young said. The scientists compared these data with 3D models that other scientists previously generated of ancient, extinct relatives of modern humans, such as Australopithecus afarensis, Australopithecus sediba, Homo ergaster and Neanderthals. The scientists found the strongest model showed the human shoulder gradually evolving from an African apelike form to its modern state.

<hr>

[Visit Link](http://www.livescience.com/52110-chimpanzee-human-shoulder-evolution.html){:target="_blank" rel="noopener"}


