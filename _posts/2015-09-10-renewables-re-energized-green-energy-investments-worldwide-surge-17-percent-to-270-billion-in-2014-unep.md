---
layout: post
title: "Renewables re-energized: Green energy investments worldwide surge 17 percent to $270 billion in 2014 (UNEP)"
date: 2015-09-10
categories:
author: Terry Collins Assoc 
tags: [Renewable energy,Solar power,Energy development,Wind power,Electric power,Natural resources,Power (physics),Climate change mitigation,Sustainable development,Physical quantities,Energy and the environment,Renewable resources,Sustainable energy,Sustainable technologies,Economy,Nature,Energy]
---


Major expansion of solar installations in China and Japan and record investments in offshore wind projects in Europe helped propel global 2014 investments to $270 billion, a 17% surge from the 2013 figure of $232 billion. The 103Gw of generating capacity added around the world made 2014 the best year ever for newly installed capacity, according to the UNEP's 9th annual Global Trends in Renewable Energy Investments report, prepared by the Frankfurt School-UNEP Collaborating Centre, and Bloomberg New Energy Finance. China saw by far the biggest renewable energy investments last year -a record $83.3 billion, up 39% from 2013. In Japan, on the other hand, investment was dominated by small scale projects of less than a megawatt, which accounted for 81% of a total solar investment of $34.8 billion, a 13% increase on 2013. Over $2 trillion invested in renewables since 2004  The 2014 global investment of $270 billion in renewables followed investments of $232 billion (2013), $256 billion (2012), $279 billion (2011), $237 billion (2010), $178 billion (2009), $182 billion (2008), $154 billion (2007), $112 billion (2006), $73 billion (2005) and $45 billion (2004) - an 11-year total of $2.02 trillion (unadjusted for inflation).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/tca-rrg033015.php){:target="_blank" rel="noopener"}


