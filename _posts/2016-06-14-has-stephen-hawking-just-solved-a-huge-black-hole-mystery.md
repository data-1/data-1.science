---
layout: post
title: "Has Stephen Hawking Just Solved a Huge Black-Hole Mystery?"
date: 2016-06-14
categories:
author: "Mike Wall"
tags: [Black hole,Stephen Hawking,Event horizon,Theory of relativity,Scientific theories,Physical cosmology,Physics,Astrophysics,General relativity,Science,Theoretical physics,Physical sciences]
---


Stephen Hawking may have just solved one of the most vexing mysteries in physics — the information paradox. Hawking — working with Malcolm Perry, of the University of Cambridge in England, and Harvard University's Andrew Stromberg — has come up with a possible solution: The quantum-mechanical information about infalling particles doesn't actually make it inside the black hole. I propose that the information is stored not in the interior of the black hole, as one might expect, but on its boundary, the event horizon,Stephen Hawking said during a talk today (Aug. 25) at the Hawking Radiation conference, which is being held at the KTH Royal Institute of Technology in Stockholm, Sweden. It's possible that black holes could actually be portals to other universes, he said. But you couldn't come back to our universe, Hawking said at the lecture, according to a KTH Royal Institute of Technology statement.

<hr>

[Visit Link](http://www.livescience.com/51980-stephen-hawking-black-hole-mystery.html){:target="_blank" rel="noopener"}


