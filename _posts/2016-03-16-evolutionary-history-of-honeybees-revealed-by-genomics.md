---
layout: post
title: "Evolutionary history of honeybees revealed by genomics"
date: 2016-03-16
categories:
author: Uppsala University
tags: [Honey bee,Evolution,Adaptation,Biodiversity,Evolutionary biology,Biology,Genetics,Biological evolution]
---


The findings show a surprisingly high level of genetic diversity in honeybees, and indicate that the species most probably originates from Asia, and not from Africa as previously thought. The findings may also indicate that high levels of inbreeding are not a major cause of global colony losses, says Matthew Webster, researcher at the department of Medical Biochemistry and Microbiology, Uppsala University. New findings show a surprisingly high level of genetic diversity in honeybees, and indicate that the species most probably originates from Asia, and not from Africa as previously thought. This indicates that climate change has strongly impacted honeybee populations historically. The study provides new insights into evolution and genetic adaptation, and establishes a framework for investigating the biological mechanisms behind disease resistance and adaptation to climate, knowledge that could be vital for protecting honeybees in a rapidly changing world, says Matthew Webster.

<hr>

[Visit Link](http://phys.org/news328034190.html){:target="_blank" rel="noopener"}


