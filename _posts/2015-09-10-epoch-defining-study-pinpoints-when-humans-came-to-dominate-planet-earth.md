---
layout: post
title: "Epoch-defining study pinpoints when humans came to dominate planet Earth"
date: 2015-09-10
categories:
author: University College London 
tags: [Anthropocene,Planet,Earth,Geologic time scale,Anthropocene Working Group,Atmosphere,Earth sciences,Nature,Physical sciences,Planetary science]
---


The human-dominated geological epoch known as the Anthropocene probably began around the year 1610, with an unusual drop in atmospheric carbon dioxide and the irreversible exchange of species between the New and Old Worlds, according to new research published today in Nature. Human actions are now changing the planet, but are we really a geological force of nature driving Earth into a new epoch that will last millions of years? The researchers also found a golden spike that can be dated to the same time: a pronounced dip in atmospheric carbon dioxide centred on 1610 and captured in Antarctic ice-core records (1). Thus, the second requirement of a golden spike marker is met. The Anthropocene probably began when species jumped continents, starting when the Old World met the New.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/ucl-esp031015.php){:target="_blank" rel="noopener"}


