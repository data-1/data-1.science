---
layout: post
title: "The shortest DNA sequences reveal insights into the world's tallest trees"
date: 2015-12-09
categories:
author: Botanical Society of America
tags: [Sequoia sempervirens,Cloning,Genetics,Polyploidy,Mutation,Botany,Genotype,Cupressaceae,Microsatellite,Tree,Life sciences,Biology,Biotechnology]
---


Researchers from the University of California, Berkeley, are uncovering important information about patterns of coast redwood clones with a new DNA analysis method that could help forest management and preservation efforts. The new method, described in a recent issue of Applications of Plant Sciences, will enable scientists to identify clonal lineages and study how clonal diversity varies throughout the geographic range of this species. Narayan and her colleagues tested the clonal identification protocol by collecting DNA samples from 770 redwoods and successfully identified 449 distinct clones. A key aspect of the new method is the use of short repeating DNA sequences from different coast redwood tissue types. Applications in Plant Sciences 3(3): 1400110. doi:10.3732/apps.1400110  Applications in Plant Sciences (APPS) is a monthly, peer-reviewed, open access journal focusing on new tools, technologies, and protocols in all areas of the plant sciences.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/bsoa-tsd032715.php){:target="_blank" rel="noopener"}


