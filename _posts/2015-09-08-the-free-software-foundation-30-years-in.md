---
layout: post
title: "The Free Software Foundation: 30 years in"
date: 2015-09-08
categories:
author: "Jono Bacon"
tags: [Free-software license,Free Software Foundation,GNU General Public License,Free software,Digital rights management,Free and open-source software,GNU,Digital media,Intellectual property activism,Computer science,Open content,Computing,Free content,Open-source movement,Intellectual works,Software engineering,Technology,Software development,Software]
---


Things have changed a lot in 30 years. These days we have software, services, social networks, and more to consider. Expanding the base of free software users and the free software movement has two parts: convincing people to care, and then making it possible for them to act on that. Most of the specific things the FSF does now it wasn't doing 30 years ago, but the vision is little changed from the original paperwork—we aim to create a world where everything users want to do on any computer can be done using free software; a world where users control their computers and not the other way around. While John may not be great with metaphors (like I am one to talk), the FSF is great at setting a mission and demonstrating a devout commitment to it.

<hr>

[Visit Link](http://opensource.com/business/15/9/free-software-foundation-30-years){:target="_blank" rel="noopener"}


