---
layout: post
title: "Making sense of our evolution"
date: 2016-05-12
categories:
author: Darren Curnoe, The Conversation
tags: [Sense of smell,Sense,Odor,Primate,Human,Eye,Neuroscience]
---


Human eyes are unique among primates for their range of iris colours and unpigmented sclera. Trichromatic vision allows humans and many other primates to perceive perhaps 10 million colours; its evolution probably keyed into the eating of fruit by our distant primate ancestors, allowing it to be distinguished against a forested backdrop of leaves. While Darwin's was clearly an overstatement, the dramatic loss of functional genes strongly hints at major differences between our sense of smell and that of most other mammals including our ape cousins. They found that 10 functional olfaction genes in humans were inactive (pseudogenes) in Neanderthals, and 8 in the Denisovans. But with vast numbers of olfactory genes available for study in the genome of living humans, our extinct cousins like the Neanderthals and Denisovans, and many living primate relatives, we've still a lot to learn about our remarkable sense of smell.

<hr>

[Visit Link](http://phys.org/news/2015-07-evolution.html){:target="_blank" rel="noopener"}


