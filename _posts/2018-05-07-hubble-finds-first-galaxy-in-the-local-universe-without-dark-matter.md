---
layout: post
title: "Hubble finds first galaxy in the local universe without dark matter"
date: 2018-05-07
categories:
author: "ESA/Hubble Information Centre"
tags: [Galaxy,Dark matter,Milky Way,Star cluster,Hubble Space Telescope,NGC 1052-DF2,Globular cluster,Edwin Hubble,Astronomy,Star,Astrophysics,Sky,Extragalactic astronomy,Physical cosmology,Science,Celestial mechanics,Outer space,Physical sciences,Astronomical objects,Space science]
---


An international team of researchers using the NASA/ESA Hubble Space Telescope and several other observatories have, for the first time, uncovered a galaxy that is missing most -- if not all -- of its dark matter. I spent an hour just staring at this image, lead researcher Pieter van Dokkum of Yale University says as he recalls first seeing the Hubble image of NGC 1052-DF2. This mass is comparable to the mass of the stars in the galaxy, leading to the conclusion that NGC 1052-DF2 contains at least 400 times less dark matter than astronomers predict for a galaxy of its mass, and possibly none at all [2]. Although counterintuitive, the existence of a galaxy without dark matter negates theories that try to explain the Universe without dark matter being a part of it [3]: The discovery of NGC 1052-DF2 demonstrates that dark matter is somehow separable from galaxies. Image credit: NASA, ESA, P. van Dokkum (Yale University)  Links  * Images of Hubble - http://www.spacetelescope.org/images/archive/category/spacecraft/  * Hubblesite release - http://hubblesite.org/news_release/news/2018-16  * Science paper - https://www.nature.com/articles/doi:10.1038/nature25676  Contacts  Pieter van Dokkum  Yale University  Yale, USA  Tel: +001 2034323019  Email: pieter.vandokkum@yale.edu  Allison Merritt  Max Planck Institute for Astronomy  Heidelberg, Germany  Tel: +49 6221 528-455  Email: merritt@mpia.de  Mathias Jäger  ESA/Hubble, Public Information Officer  Garching bei München, Germany  Tel: +49 176 62397500  Email: mjaeger@partner.eso.org

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-03/eic-hff032818.php){:target="_blank" rel="noopener"}


