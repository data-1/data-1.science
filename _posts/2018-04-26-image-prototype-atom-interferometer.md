---
layout: post
title: "Image: Prototype atom interferometer"
date: 2018-04-26
categories:
author: "European Space Agency"
tags: [Quantum mechanics,Interferometry,Space,Physics,Science]
---


Credit: RAL Space/IQO Hannover  A prototype atom interferometer chip in a vacuum chamber, harnessing the quantum behaviour of atoms to perform ultra-precise measurements of gravity. Quantum physics and space travel are two of the greatest scientific achievements of the last century, comments ESA's Bruno Leone, who this month organised the latest Agency workshop on quantum technologies. We now see huge great promise in bringing them together: many quantum experiments can be performed much more precisely in space, away from terrestrial perturbations. In addition, the new generation of quantum devices offer huge improvements to space-related technology. Potential is there for the use of quantum technologies in areas such as Earth observation, planetary exploration, secure communications, fundamental physics, microgravity research and navigation.

<hr>

[Visit Link](https://phys.org/news/2017-11-image-prototype-atom-interferometer.html){:target="_blank" rel="noopener"}


