---
layout: post
title: "Understanding Mercury's magnetic tail: Researchers have evidence that might explain the unexpected presence of energetic electrons in Mercury's magnetic tail"
date: 2018-07-01
categories:
author: "American Institute of Physics"
tags: [Magnetosphere,Magnetic reconnection,Comet tail,Planet,MESSENGER,Mercury (planet),Sun,Nature,Physical sciences,Electromagnetism,Applied and interdisciplinary physics,Phases of matter,Astrophysics,Science,Physics,Astronomy,Space science]
---


On Mercury, magnetic substorms in the tail are bigger and more rapid than those observed on Earth. Mercury's magnetic field is 100 times weaker than Earth's, so it surprised physicists that MESSENGER detected signs of energetic electrons in the planet's magnetic tail -- the Hermean magnetotail. These plasmoids accelerate energetic electrons. The simulation results are supported by MESSENGER measurements of plasmoid species and plasmoid reconnection in the Hermean magnetotail. The researchers also used a mean-turbulence model to describe the turbulence of subgrid-scale physical processes.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180417115752.htm){:target="_blank" rel="noopener"}


