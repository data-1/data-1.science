---
layout: post
title: "The BIG Bell Test"
date: 2018-07-03
categories:
author: "University of Science and Technology of China"
tags: [Bell test,Bells theorem,Quantum entanglement,Physics,Science,Quantum mechanics,Theoretical physics,Scientific theories,Applied and interdisciplinary physics,Physical sciences,Scientific method]
---


On November 30th, 2016, more than 100,000 people around the world contributed to a suite of first-of-a-kind quantum physics experiments known as The BIG Bell Test. The BIG Bell Test asked human volunteers, known as Bellsters, to choose the measurements, in order to close the so-called freedom-of-choice loophole - the possibility that the particles themselves influence the choice of measurement. This loophole cannot be closed by choosing with dice or random number generators, because there is always the possibility that these physical systems are coordinated with the entangled particles. Participants contributed with more than 90 million bits, making possible a strong test of local realism, as well as other experiments on realism in quantum mechanics. Each of the twelve labs around the world carried out a different experiment, to test local realism in different physical systems and to test other concepts related to realism.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/uosa-tbb051018.php){:target="_blank" rel="noopener"}


