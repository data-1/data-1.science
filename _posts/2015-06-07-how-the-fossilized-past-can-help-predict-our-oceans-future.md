---
layout: post
title: "How the fossilized past can help predict our oceans' future"
date: 2015-06-07
categories:
author: Ryan Mcnutt, Dalhousie University
tags: [Biodiversity,Extinction,Ocean,Paleontology,Species,Ecology,Nature,Environmental social science,Natural environment]
---


We set out to answer a question: over the past 23 million years, what was it that made some species more vulnerable to extinction? Lotze and Tittensor collaborated together in a working group through the National Evolutionary Synthesis Center (NESCent), a non-profit science centre dedicated to evolutionary research. One is that what marine ecologists call range size can determine a species' risk of extinction. Based on combining these findings, the study identified areas like the Caribbean and Indo-Pacific as extinction hotspots due to their rich biodiversity, high impact levels and concentration of extinction-vulnerable species. Explore further Fossils help identify marine life at high risk of extinction today

<hr>

[Visit Link](http://phys.org/news350808675.html){:target="_blank" rel="noopener"}


