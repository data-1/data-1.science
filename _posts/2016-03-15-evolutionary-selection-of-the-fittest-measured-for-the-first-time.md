---
layout: post
title: "Evolutionary 'selection of the fittest' measured for the first time"
date: 2016-03-15
categories:
author: Uppsala University
tags: [Genetic code,Natural selection,Evolution,Bacteria,Gene,Translation (biology),Fitness (biology),Protein,Codon usage bias,Organism,Genetics,Biological evolution,Branches of genetics,Evolutionary biology,Biochemistry,Molecular biology,Biology,Biotechnology,Life sciences]
---


At each generation the 'fittest' individuals are selected and this is a major force shaping the biological world we see today. Over time evolution selects against cheetahs that cannot run fast enough. But, how fast is fast enough, and big does the difference have to be before selection is effective? To grow, bacteria, like all living organisms, must translate their genetic code into amino acids that are joined together to make proteins. Brandis and Hughes quantified the fitness cost of changing codons in this gene.

<hr>

[Visit Link](http://phys.org/news/2016-03-evolutionary-fittest.html){:target="_blank" rel="noopener"}


