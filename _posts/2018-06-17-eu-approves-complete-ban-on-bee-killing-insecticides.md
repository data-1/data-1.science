---
layout: post
title: "EU approves complete ban on bee-killing insecticides"
date: 2018-06-17
categories:
author: "Greg Beach"
tags: []
---


In a monumental decision that has been years in the making, all member nations of the European Union have approved a total ban of neonicotinoids, the most widely used insecticide in the world and a well-documented danger to bees and other pollinators. The rapidly declining population of pollinator species in recent years is in part due to the widespread use of harmful pesticides. The ban should result in a healthier pollinator population, which is essential for global food production. “The commission had proposed these measures months ago, on the basis of the scientific advice from [the EU‘s scientific risk assessors],” Vytenis Andriukaitis, European commissioner for Health and Food Safety, told the Guardian. SIGN UP  This policy change pleased activists.

<hr>

[Visit Link](https://inhabitat.com/eu-approves-complete-ban-on-bee-killing-insecticides){:target="_blank" rel="noopener"}


