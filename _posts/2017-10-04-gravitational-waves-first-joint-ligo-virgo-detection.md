---
layout: post
title: "Gravitational waves: First joint LIGO-Virgo detection"
date: 2017-10-04
categories:
author: "CNRS"
tags: [Virgo interferometer,LIGO,Gravitational wave,Gravitational-wave observatory,Celestial mechanics,Theories of gravitation,Physical phenomena,Astronomical objects,Theory of relativity,Physical cosmology,General relativity,Gravity,Science,Astrophysics,Physics,Physical sciences,Space science,Astronomy]
---


Merging black holes have already been observed three times by the LIGO detectors, in 2015 and early 2017 . This new event confirms that pairs of black holes are relatively common, and will contribute towards the study of such objects. As for previous events, no optical signals were detected. The Virgo researchers work together in the eponymous collaboration, which includes over 250 physicists, engineers and technicians belonging to 20 European laboratories, including 6 at CNRS in France, 8 at INFN in Italy, and 2 at Nikhef in the Netherlands. The LIGO Scientific Collaboration (LSC), a group of over 1000 scientists working at universities in the US and 14 other countries, was formed around these instruments.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-09/c-gwf092917.php){:target="_blank" rel="noopener"}


