---
layout: post
title: "Google's Optical Character Recognition (OCR) software works for 248+ languages"
date: 2015-09-28
categories:
author: "Subhashish Panigrahi"
tags: [Optical character recognition,Tesseract (software),Computing,Technology,Human communication,Software,Language]
---


Developed as a community project during 1995-2006 and later taken over by Google, Tesseract is considered one of the most accurate OCR engines and works for over 60 languages. However, detecting these elements is difficult and we may not always succeed. Native speakers of the following Indian lanaguages—Bangla, Malayalam, Kannada, Odia, Tamil, and Telugu—also commented on a Facebook post with feedback after testing the OCR. A tutorial to convert text in Odia (Indian language) from a scanned image using Google's OCR. If you have additional feedback on the article or technology, please let us know in the comments.

<hr>

[Visit Link](http://opensource.com/life/15/9/open-source-extract-text-images){:target="_blank" rel="noopener"}


