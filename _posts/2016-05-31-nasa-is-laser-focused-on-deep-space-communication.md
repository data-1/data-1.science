---
layout: post
title: "NASA is laser-focused on deep space communication"
date: 2016-05-31
categories:
author: "Andrea Dunn"
tags: [NASA,International Space Station,Flight controller,Spaceflight,Technology]
---


Researchers for the Comm Delay Assessment investigation set out to determine whether communication delays, like those astronauts could experience on a long-duration mission to an asteroid or to Mars, will result in impacts to individual and crew performance and well-being. Crew members reported getting frustrated when they needed communication quickly, said Principal Investigator Larry Palinkas, Ph.D. Tasks took longer and there was frustration when the crew members or Mission Control had to ask additional questions, because they were not getting the information they needed in a timely fashion, or there was miscommunication that needed clarification. Communication delays between Mission Control and crew members during the Comm Delay Assessment that took place during a recent International Space Station increment posed challenges for crew members. Credit: Mark Kelly  According to Palinkas, crew members expressed that being understood by others was the most important aspect of the communication quality. The autonomy boosted performance and moods during the tasks.

<hr>

[Visit Link](http://phys.org/news/2015-08-nasa-laser-focused-deep-space.html){:target="_blank" rel="noopener"}


