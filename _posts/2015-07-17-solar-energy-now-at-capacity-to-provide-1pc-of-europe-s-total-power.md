---
layout: post
title: "Solar energy now at capacity to provide 1pc of Europe’s total power"
date: 2015-07-17
categories:
author: Colm Gorey, Colm Gorey Was A Senior Journalist With Silicon Republic
tags: [Solar power in Italy,Solar energy,Watt,Solar power,Sustainable energy,Economy,Technology,Renewable energy,Energy]
---


It might not seem much, but the world has now passed the first of what is hoped to be many milestones with the news that solar power now has the capacity to account for 1pc of Europe’s entire energy creation. Among the findings of the report, the analysis shows that the world’s total solar energy capacity now stands at 178GW, with 40GW created in 2014, up from 37GW created in 2013. However, the UK were the most proactive in 2014, producing 2.4GW-worth of solar energy, followed by Germany (1.9GW) and France (927MW). China has maintained its position as the world’s largest creator of solar energy — producing five times as much solar energy as Germany — installing 10.6GW in 2014, with its neighbour, Japan, following suit with 9.7GW, and the US somewhat behind with just over 6.5GW in the same time period. Making predictions for 2020, SPE are very optimistic for the technology, predicting that, by then, world solar energy creation could leap to 540GW, but says that the more likely figure is a still-impressive 450GW.

<hr>

[Visit Link](https://www.siliconrepublic.com/earth-science/2015/06/12/solar-energy-now-accounts-for-1pc-of-worlds-total-power){:target="_blank" rel="noopener"}


