---
layout: post
title: "Extreme volcanism: Image captures 1 of the brightest volcanoes in the solar system"
date: 2015-10-28
categories:
author: Gemini Observatory 
tags: [Io (moon),Gemini Observatory,Volcano,Observatory,Types of volcanic eruptions,Astronomy,Space science,Astronomical objects,Outer space,Science,Physical sciences,Planetary science]
---


De Kleer's paper examines the powerful late-August eruption in detail, concluding that the energy emitted was about 20 Terawatts and expelled many cubic kilometers of lava. These particular observations were timed to follow up on a different outburst eruption that was detected earlier in the month by Imke de Pater, also of UCB. The Gemini Observatory is an international collaboration with two identical 8-meter telescopes. The Gemini Observatory provides the astronomical communities in six partner countries with state-of-the-art astronomical facilities that allocate observing time in proportion to each country's contribution. The national research agencies that form the Gemini partnership include: the U.S. National Science Foundation (NSF); the Canadian National Research Council (NRC); the Brazilian Ministério da Ciência, Tecnologia e Inovação (MCTI); the Australian Research Council (ARC); the Argentinean Ministerio de Ciencia, Tecnología e Innovación Productiva; and the Chilean Comisión Nacional de Investigación Cientifica y Tecnológica (CONICYT).

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/go-ev080414.php){:target="_blank" rel="noopener"}


