---
layout: post
title: "Evolving a bigger brain with human DNA"
date: 2016-04-13
categories:
author: Duke University
tags: [Enhancer (genetics),Brain,Gene,Genetics,Development of the nervous system,Evolution,Human,Genome,Life sciences,Biotechnology,Neuroscience,Biology]
---


The human version of a DNA sequence called HARE5 turns on a gene important for brain development (gene activity is stained blue), and causes a mouse embryo to grow a 12 percent larger brain by the end of pregnancy than an embryo injected with the chimpanzee version of HARE5. Yet, in mouse embryos the researchers found that the human enhancer was active earlier in development and more active in general than the chimpanzee enhancer. The researchers found that in the mouse embryos equipped with Frizzled8 under control of human HARE5, progenitor cells destined to become neurons proliferated faster compared with the chimp HARE5 mice, ultimately leading to more neurons. The group also hopes to explore the role of the other HARE sequences in brain development. What we found is a piece of the genetic basis for why we have a bigger brain, Wray said.

<hr>

[Visit Link](http://phys.org/news343558525.html){:target="_blank" rel="noopener"}


