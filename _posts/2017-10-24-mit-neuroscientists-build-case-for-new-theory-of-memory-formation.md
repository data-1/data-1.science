---
layout: post
title: "MIT neuroscientists build case for new theory of memory formation"
date: 2017-10-24
categories:
author: Anne Trafton
tags: [Memory,Hippocampus,Hippocampus anatomy,Engram (neuropsychology),Encoding (memory),Hippocampus proper,Neuroscience,Brain,Mental processes,Cognitive science,Cognition,Cognitive psychology,Psychology]
---


The researchers also showed that even though memories held by silent engrams cannot be naturally recalled, the memories persist for at least a week and can be “awakened” days later by treating cells with a protein that stimulates synapse formation. Silent memories  Neuroscientists have long believed that memories of events are stored when synaptic connections, which allow neurons to communicate with each other, are strengthened. They found that while the mice could not recall those memories in response to natural cues, such as being placed in the cage where a fearful event took place, the memories were still there and could be artificially retrieved using a technique known as optogenetics. The researchers have dubbed these memory cells “silent engrams,” and they have since found that these engrams can also be formed in other situations. “What we are saying is that even without new cellular protein synthesis, once a new connection is made, or a pre-existing connection is strengthened during encoding, that new pattern of connections is maintained,” Tonegawa says.

<hr>

[Visit Link](http://news.mit.edu/2017/neuroscientists-build-case-new-theory-memory-formation-1023){:target="_blank" rel="noopener"}


