---
layout: post
title: "Tibetan people have multiple adaptations for life at high altitudes: Study of 27 Tibetan genomes finds adaptations and relationships to Han Chinese, Denisovans"
date: 2017-08-31
categories:
author: "PLOS"
tags: [Denisovan,EPAS1,Gene,Adaptation,Genetics,Whole genome sequencing,Biology]
---


The people of Tibet have survived on an extremely high and arid plateau for thousands of years, due to their amazing natural ability to withstand low levels of oxygen, extreme cold, exposure to UV light and very limited food sources. Researchers sequenced the whole genomes of 27 Tibetans and searched for advantageous genes. The analysis identified two genes already known to be involved in adaptation to high altitude, EPAS1 and EGLN1, as well as two genes related to low oxygen levels, PTGIS and KCTD12. The Tibetan variant of the EPAS1 gene originally came from the archaic Denisovan people, but the researchers found no other genes related to high altitude with Denisovan roots. The study represents a comprehensive analysis of the demographic history of the Tibetan population and its adaptations to the challenges of living at high altitudes.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/04/170427141746.htm){:target="_blank" rel="noopener"}


