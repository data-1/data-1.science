---
layout: post
title: "New radio map of Jupiter reveals what's beneath colorful clouds"
date: 2016-06-03
categories:
author: "University of California - Berkeley"
tags: [Cloud,Jupiter,Atmosphere of Earth,Very Large Array,Atmosphere,Astronomy,Great Red Spot,Juno (spacecraft),Planet,Ammonia,Radio astronomy,Planets,Astronomical objects,Outer space,Planetary science,Science,Physical sciences,Space science]
---


Astronomers using the upgraded Karl G. Jansky Very Large Array in New Mexico have produced the most detailed radio map yet of the atmosphere of Jupiter, revealing the massive movement of ammonia gas that underlies the colorful bands, spots and whirling clouds visible to the naked eye. The University of California, Berkeley researchers measured radio emissions from Jupiter's atmosphere in wavelength bands where clouds are transparent. Between these hotspots are ammonia-rich upwellings that bring ammonia from deeper in the planet. We now see high ammonia levels like those detected by Galileo from over 100 kilometers deep, where the pressure is about eight times Earth's atmospheric pressure, all the way up to the cloud condensation levels, de Pater said. These Jupiter maps really show the power of the upgrades to the VLA.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/uoc--nrm052516.php){:target="_blank" rel="noopener"}


