---
layout: post
title: "Researchers study the sun's coronal rain in great detail"
date: 2014-06-24
categories:
author: Dr Robert Massey, Royal Astronomical Society
tags: [Stellar corona,Sun,Rain,Solar physics,Weather,Space science,Astronomical objects known since antiquity,Stellar astronomy,Transparent materials,Solar System,Bodies of the Solar System,Astronomy,Nature,Physical sciences,Applied and interdisciplinary physics,Phases of matter,Space plasmas,Physical phenomena,Astronomical objects]
---


A mosaic of images obtained using the Swedish Solar Telescope (SST), revealing evidence of a large-scale coronal rain shower pouring relentlessly into the dark sunspot on the surface of the Sun. But unlike the all-too-frequent storms of the UK and Ireland, rain on the Sun is made of electrically charged gas (plasma) and falls at around 200,000 kilometres an hour from the outer solar atmosphere, the corona, to the Sun's surface. A movie made from a series of high-resolution observations (44-km per pixel image scale), taken over a period of 1 hour and 10 minutes using the Swedish 1-m Solar Telescope (SST) and showing coronal rain formation. half-way through the movie), which is followed by many dark threaded flows (i.e. coronal rain) in the second half of the movie, which appear to be falling back to the solar surface from bottom-right to top-left. Dr Scullion and his team have now developed a new insight into how coronal rain forms.

<hr>

[Visit Link](http://phys.org/news322806790.html){:target="_blank" rel="noopener"}


