---
layout: post
title: "Mars is the next step for humanity – we must take it"
date: 2016-04-13
categories:
author: Ashley Dove-Jay, The Conversation
tags: [Mars,Sievert,Outer space]
---


It takes about 50% more energy to put something on the surface of the Moon than it does on Mars. It's feasible to introduce biological life to Mars, but not the Moon. Radiation: An astronaut would receive a lifetime allowable dose of radiation in a single 30-month round-trip, including 18 months on the surface. Reduced gravity: The effects of microgravity on astronauts' health have been studied for decades, and a range of techniques have been developed to mitigate the wasting effects on muscle and bone. Any Martian life will have evolved independently and is unlikely to be capable even of interacting with Earth life on a molecular level.

<hr>

[Visit Link](http://phys.org/news342694192.html){:target="_blank" rel="noopener"}


