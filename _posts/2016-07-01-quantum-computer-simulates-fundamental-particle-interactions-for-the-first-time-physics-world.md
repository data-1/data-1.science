---
layout: post
title: "Quantum computer simulates fundamental particle interactions for the first time – Physics World"
date: 2016-07-01
categories:
author: ""
tags: [Quantum mechanics,Electron,Particle physics,Quantum computing,Physics,Quantum optics,Laser,Science,Scientific theories,Applied and interdisciplinary physics,Physical sciences,Theoretical physics]
---


Gauge theories  Peter Zoller, Rainer Blatt and colleagues at the University of Innsbruck and the Institute for Quantum Optics and Quantum Information (IQOQI) have created a digital quantum computer and have used it to simulate the physics of a gauge theory. These theories describe how fundamental particles such as quarks or electrons interact with one another, and are at the heart of the Standard Model of particle physics. But over the last few years, theorists have begun to put forward algorithms that would allow quantum computers to model gauge theories. Those pulses can represent three effects: the creation or annihilation of electron–positron pairs in the vacuum; long-range electrical (Coulomb) interactions between the particles; or the energy associated with the particles’ masses. Although the calculation can be easily done using a normal desktop computer, team member Christine Muschik of IQOQI says the results provide a proof-of-principle demonstration that quantum computers can be used to simulate the interactions described by gauge theories.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2016/jun/30/quantum-computer-simulates-fundamental-particle-interactions-for-the-first-time){:target="_blank" rel="noopener"}


