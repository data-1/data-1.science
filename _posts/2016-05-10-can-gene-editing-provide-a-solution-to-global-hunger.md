---
layout: post
title: "Can gene editing provide a solution to global hunger?"
date: 2016-05-10
categories:
author: Utibe Effiong And Ramadhani Noor, The Conversation
tags: [Genetic engineering,Genome editing,Transcription activator-like effector nuclease,Biotechnology,Genetically modified organism,Food and drink,Biology]
---


In most African countries, however, GM crops remain a farfetched idea. But a different technique known as gene editing modifies plant, as well as animal and human, genomes without the introduction of foreign genetic materials. Significant genetic errors have been produced by the commonly applied techniques of genome editing, including TALENs, in the past. Research is currently under way to improve these techniques, reduce the frequency of unwanted mutations and improve the safety of genome editing. Regulatory protections  TALEN is one gene-editing technique that can modify the genome of a crop plant.

<hr>

[Visit Link](http://phys.org/news355386937.html){:target="_blank" rel="noopener"}


