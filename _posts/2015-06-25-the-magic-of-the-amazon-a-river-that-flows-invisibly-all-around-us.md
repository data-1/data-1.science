---
layout: post
title: "The magic of the Amazon: A river that flows invisibly all around us"
date: 2015-06-25
categories:
author: Antonio Donato Nobre
tags: [TED (conference)]
---


The Amazon River is like a heart, pumping water from the seas through it, and up into the atmosphere through 600 billion trees, which act like lungs. Clouds form, rain falls and the forest thrives. In a lyrical talk, Antonio Donato Nobre talks us through the interconnected systems of this region, and how they provide environmental services to the entire world. A parable for the extraordinary symphony that is nature.

<hr>

[Visit Link](http://feedproxy.google.com/~r/TEDTalks_video/~3/q6NUOhi8DB8/antonio_donato_nobre_the_magic_of_the_amazon_a_river_that_flows_invisibly_all_around_us){:target="_blank" rel="noopener"}


