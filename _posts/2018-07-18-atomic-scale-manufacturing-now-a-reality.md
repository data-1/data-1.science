---
layout: post
title: "Atomic-scale manufacturing now a reality"
date: 2018-07-18
categories:
author: "University of Alberta"
tags: [Automation,Technology,Information Age,Manufacturing,Nanotechnology,Atom,American Association for the Advancement of Science,Branches of science]
---


Most of us thought we'd never be able to automate atomic writing and editing, but stubborn persistence has paid off, and now Research Associate Moe Rashidi has done it, said Robert Wolkow, professor of physics at the University of Alberta, who along with his Research Associate has just published a paper announcing their findings. Until now, we printed with atoms about as efficiently as medieval monks produced books, explained Wolkow. Machine learning has automated the atom fabrication process, and an atom-scale manufacturing revolution is sure to follow. The physicist has devoted his career to pushing atomic-scale manufacturing forward in response to not only the rapidly changing needs of our information age but also the changes to our climate. For Wolkow, this all adds up to an urgent need for a new basis for our electronics, something which he predicts will be powered by atomic-scale fabrication and mass manufacturing, now possible thanks to his new discovery.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/uoa-amn052218.php){:target="_blank" rel="noopener"}


