---
layout: post
title: "Gabon's unique 'orange crocodiles' intrigue scientists"
date: 2018-06-29
categories:
author: "Caroline Chauvet"
tags: []
---


Coloured croc: The orange reptiles live in caves in remote southern Gabon  The West African state of Gabon is famous for its biodiversity but in a galaxy of spectacular finds, one stands out: orange crocodiles. It was only two years later when they hauled one out into the light that they realised it was orange. At first we thought the colour came from their food, because we saw that these reptiles ate orange bats, said Oslisly. Under this theory, the bat guano began to attack their skin and transformed their colour, said speleologist Olivier Testa, a member of the research team. Dwarf crocodiles (Osteolaemus tetraspis) are a well-studied species, but the ones in the cave complex stand out in the way they have adapted to their habitat.

<hr>

[Visit Link](https://phys.org/news/2018-06-gabon-unique-orange-crocodiles-intrigue.html){:target="_blank" rel="noopener"}


