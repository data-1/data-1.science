---
layout: post
title: "Study suggests active volcanism on Venus"
date: 2016-05-09
categories:
author: Kevin Stacey, Brown University
tags: [Venus,Volcano,Ganis Chasma,Venus Express,Geology,Physical sciences,Planets of the Solar System,Planetary science,Terrestrial planets,Earth sciences,Space science,Astronomical objects known since antiquity]
---


Although geologically relatively recent in the 4.5 billion year history of Venus, these terrains could be many hundreds of million years old and no longer active. New images and measurements from the European Space Agency Venus Express Camera, however, show evidence that parts of the rift zones are the locations of active volcanism, providing compelling evidence that Venus in general, and the rift zones in particular, continue to be sites of volcanic and tectonic activity well into the modern era. In combing through data from the European Space Agency's Venus Express mission, the scientists found transient spikes in temperature at several spots on the planet's surface. The spots were clustered in a large rift zone called Ganiki Chasma. The observation of hotspots by Venus Express, combined with the geologic mapping from Venera and Magellan, make a strong case for a volcanically active Venus, Head says.

<hr>

[Visit Link](http://phys.org/news353843766.html){:target="_blank" rel="noopener"}


