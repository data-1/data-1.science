---
layout: post
title: "Researchers find brain area that integrates speech's rhythms"
date: 2016-05-02
categories:
author: Duke University
tags: [Speech,Superior temporal sulcus,Sound,American Association for the Advancement of Science,Functional magnetic resonance imaging,Auditory cortex,Neuroscience,Cognitive science,Cognition,Interdisciplinary subfields,Mental processes,Cognitive psychology]
---


DURHAM, N.C. -- Duke and MIT scientists have discovered an area of the brain that is sensitive to the timing of speech, a crucial element of spoken language. By comparison, syllables take longer: 200 to 300 milliseconds. In a study appearing May 18 in the journal Nature Neuroscience, Overath and his collaborators cut recordings of foreign speech into short chunks ranging from 30 to 960 milliseconds in length, and then reassembled the pieces using a novel algorithm to create new sounds that the authors call 'speech quilts'. We knew we were onto something, said Overath, who is a member of the Duke Institute for Brain Sciences  The superior temporal sulcus is known to integrate auditory and other sensory information. We really went to great lengths to be certain that the effect we were seeing in STS was due to speech-specific processing and not due to some other explanation, for example, pitch in the sound or it being a natural sound as opposed to some computer-generated sound, Overath said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/du-rfb051815.php){:target="_blank" rel="noopener"}


