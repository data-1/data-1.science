---
layout: post
title: "The dwarf planet Quaoar"
date: 2016-06-14
categories:
author: "Matt Williams"
tags: [50000 Quaoar,Pluto,Kuiper belt,Dwarf planet,Eris (dwarf planet),90482 Orcus,Planet,Haumea,Bodies of the Solar System,Astronomical objects,Solar System,Planetary science,Planemos,Outer space,Planets,Local Interstellar Cloud,Substellar objects,Physical sciences,Celestial mechanics,Dwarf planets,Space science,Astronomy]
---


Artist’s impression of the Kuiper Belt Object and possible dwarf planet Quaoar. First observed in 2005 by Mike Brown and his team, the discovery of Eris overturned decades of astronomical conventions. This includes the Kuiper Belt Object (KBO) 5000 Quaoar (or just Quaoar), which was actually discovered three years before Eris. And with an estimated mass of 1.4 ± 0.1 × 1021 kg, Quaoar is about as massive as Pluto's moon Charon, equivalent to 0.12 times the mass of Eris, and approximately 2.5 times as massive as Orcus. Credit: Chad Trujillo & Michael Brown (Caltech)  Composition:  At the time of its discovery, not much was known about Kuiper belt objects.

<hr>

[Visit Link](http://phys.org/news/2015-08-dwarf-planet-quaoar.html){:target="_blank" rel="noopener"}


