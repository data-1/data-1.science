---
layout: post
title: "Watch Live As SpaceX Launches the Koreasat-5A on a Falcon 9 Rocket"
date: 2018-03-30
categories:
author: ""
tags: [Kennedy Space Center Launch Complex 39A,Outer space,Space access,Aerospace,Space science,Commercial launch service providers,Space industry,SpaceX,Spaceflight technology,Rocketry,Space program of the United States,Space programs,Space vehicles,Flight,Astronautics,Spacecraft,Spaceflight,Space organizations,Spacecraft manufacturers,Commercial spaceflight,Space exploration,Private spaceflight,NASA,Human spaceflight,Private spaceflight companies,Hawthorne California]
---


Copyright ©, Camden Media Inc All Rights Reserved. See our User Agreement Privacy Policy and Data Use Policy . The material on this site may not be reproduced, distributed, transmitted, cached or otherwise used, except with prior written permission of Futurism. Fonts by Typekit and Monotype.

<hr>

[Visit Link](https://futurism.com/videos/watch-spacex-launch-koreasat-falcon-rocket/){:target="_blank" rel="noopener"}


