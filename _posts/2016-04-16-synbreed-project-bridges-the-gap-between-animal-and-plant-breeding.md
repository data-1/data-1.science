---
layout: post
title: "Synbreed project bridges the gap between animal and plant breeding"
date: 2016-04-16
categories:
author: Technical University Munich
tags: [Plant breeding,Genetics,Biodiversity,Biology]
---


According to the head of the Synbreed project, Prof. Chris-Carolin Schön, optimizing breeding methods will be an important technology for the future. The methods used are based on DNA analysis, which allows scientists to identify characteristics in animals and plants that are particularly advantageous for breeding. During the Synbreed project, the team under Prof. Ruedi Fries (TUM Chair of Animal Breeding) discovered which gene sequences were responsible for the dark pigmentation around the eyes of Fleckvieh cattle. The Synbreed findings discovered by Fries have also been fed into the international 1000 bull genomes project, in which genomes of the world's four most common cattle breeds have been sequenced. Synbreed project breeds success  The Synbreed project has achieved massive breakthroughs in breeding research, concludes Schön.

<hr>

[Visit Link](http://phys.org/news344773856.html){:target="_blank" rel="noopener"}


