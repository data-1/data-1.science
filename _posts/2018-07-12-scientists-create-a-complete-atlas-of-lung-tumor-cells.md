---
layout: post
title: "Scientists create a complete atlas of lung tumor cells"
date: 2018-07-12
categories:
author: "VIB (the Flanders Institute for Biotechnology)"
tags: [Neoplasm,Cell (biology),Cancer,Single cell sequencing,Tumor microenvironment,Lung cancer,Malignancy,Biology,Life sciences,Health sciences,Health,Causes of death,Diseases and disorders,Medical specialties,Neoplasms,Medicine,Clinical medicine]
---


Researchers from VIB, Leuven University and University Hospital Leuven studied thousands of healthy and cancerous lung cells to create the first comprehensive atlas of lung tumor cells. The results of the study will be published in the leading journal Nature Medicine. The researchers used single-cell RNAseq technology to study almost 100,000 individual cells, focusing on both cancerous cells and non-cancerous cells in tumors such as blood vessels, immune cells and fibrous cells to create the very first 'atlas' of cell phenotypes found in lung tumors. Because the team analyzed both tumor cells and lung cells found outside the tumor and compared the two, they were moreover able to observe how each cell type is altered by the tumor. Everyone can submit questions concerning this and other medically-oriented research directly to VIB via this address.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/vfi-sca070918.php){:target="_blank" rel="noopener"}


