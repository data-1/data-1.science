---
layout: post
title: "Ocean acidification research makes a strong case for limiting climate change"
date: 2018-08-18
categories:
author: "Helmholtz Association Of German Research Centres"
tags: [Ocean acidification,Climate change,Ocean,Environmental science,Natural environment,Earth sciences,Nature,Physical geography,Human impact on the environment,Global environmental issues,Environmental issues,Environmental social science,Environmental impact,Climate variability and change,Ecology,Oceanography,Environment]
---


As a gigantic carbon sink, the ocean absorbs about a third of the carbon dioxide (CO2) released into the atmosphere by human activities. Ocean acidification affects ecosystems and important benefits the ocean provides to humankind. Because everyone in this global community will be affected by climate change, it will be for our own benefit if we manage to reduce carbon dioxide emissions in such a way that global warming is limited to less than 2 degrees Celsius, says Prof. Ulf Riebesell, marine biologist at GEOMAR Helmholtz Centre for Ocean Research Kiel and coordinator of BIOACID. Ocean acidification reduces the ocean's ability to store carbon. Following the precautionary principle is the best way to act when considering potential risks to the environment and humankind, including future generations.

<hr>

[Visit Link](https://phys.org/news/2017-10-ocean-acidification-strong-case-limiting.html){:target="_blank" rel="noopener"}


