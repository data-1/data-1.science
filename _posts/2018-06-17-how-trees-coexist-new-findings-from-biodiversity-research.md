---
layout: post
title: "How trees coexist—new findings from biodiversity research"
date: 2018-06-17
categories:
author: "Dresden University Of Technology"
tags: [Biodiversity,Forest,Tree,Ecosystem,Forestry,Ecology,Earth sciences,Systems ecology,Biogeochemistry,Environmental conservation,Nature,Natural resource management,Natural resources,Environmental social science,Nature conservation,Natural environment]
---


Tree diversity experiment BEF-China, September 2016. Credit: Goddert von Oheimb  For a decade, researchers explore how tree species diversity affects the coexistence of trees and their growth performance in the largest biodiversity experiment with trees worldwide, the so-called BEF-China' experiment. One of the main interests of the BEF-China team is to explore the relationship between tree diversity and multiple ecosystem functions, specifically those benefitting society, such as wood production or the mitigation of soil erosion. The findings now shed new light on tree-tree interactions: The local environment of a tree strongly determine its productivity, meaning that tree individuals growing in a species-rich neighbourhood produce more wood than those surrounded by neighbours of the same species. Particularly impressive is the finding that the interrelations of a tree with its immediate neighbours induce higher productivity of the entire tree community (i.e. the forest stand), and that such local neighbourhood interactions explain more than 50% of the total forest stand productivity, says forest ecologist Dr. Andreas Fichtner. These findings show that the coexistence of neighbouring trees and their small-scale interactions are substantial in explaining the productivity of species-rich mixed forests.

<hr>

[Visit Link](https://phys.org/news/2018-03-trees-coexistnew-biodiversity.html){:target="_blank" rel="noopener"}


