---
layout: post
title: "New GMO Controversy: Are the Herbicides Dangerous?"
date: 2016-05-27
categories:
author: Rachael Rettner
tags: [24-Dichlorophenoxyacetic acid,Genetic engineering,Glyphosate,Herbicide,Genetically modified crops,Genetically modified organism,Agriculture,Chuck Benbrook,Genetically modified food,Agent Orange,Health]
---


The authors argued that because some studies have linked cancer risk to the herbicides used on GM crops — in particular, a widely used herbicide called glyphosate (sold under the brand name Roundup) — the United States should reconsider creating labeling requirements for GM foods. Landrigan and Benbrook wrote that the emergence of weeds that are resistant to herbicides led farmers to increase their use of these chemicals on crops. Bradford also noted that herbicides are used on all crops, not just those that are genetically modified. Margaret Smith, a professor of plant breeding and genetics at Cornell University in New York state, said she agreed with the authors of the NEJM article that use of GM crops resistant to herbicides, and use of glyphosate, have gone up in recent years. But Smith also said that glyphosate has a relatively benign impact on the environment, and that its use replaced more environmentally hazardous products.

<hr>

[Visit Link](http://www.livescience.com/51917-gmo-herbicides-health.html){:target="_blank" rel="noopener"}


