---
layout: post
title: "Asteroid attacks significantly altered ancient Earth"
date: 2015-09-04
categories:
author: Arizona State University 
tags: [Earth,Plate tectonics,Moon,Terrestrial planet,Geology,Impact event,Crust (geology),Nature,Planetary science,Astronomical objects,Terrestrial planets,Space science,Astronomy,Planets,Outer space,Solar System,Bodies of the Solar System,Earth sciences,Planets of the Solar System,Physical sciences]
---


A new terrestrial bombardment model based on existing lunar and terrestrial data sheds light on the role asteroid bombardments played in the geological evolution of the uppermost layers of the Hadean Earth (approximately 4 to 4.5 billion years ago). When we look at the present day, we have a very high fidelity timeline over the last about 500 million years of what's happened on Earth, and we have a pretty good understanding that plate tectonics and volcanism and all these kinds of processes have happened more or less the same way over the last couple of billion years, says Lindy Elkins-Tanton, director of the School of Earth and Space Exploration at Arizona State University. While researchers estimate accretion during late bombardment contributed less than one percent of Earth's present-day mass, giant asteroid impacts still had a profound effect on the geological evolution of early Earth. And this study is a major step in that direction, trying to bridge that time from the last giant accretionary impact that largely completed the Earth and produced the Moon to the point where we have something like today's plate tectonics and habitable surface. Large impacts had particularly severe effects on existing ecosystems.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/asu-aas073114.php){:target="_blank" rel="noopener"}


