---
layout: post
title: "Decision-making involves a little known brain region in the thalamus"
date: 2016-07-25
categories:
author: "CNRS"
tags: [Reward system,Thalamus,Orbitofrontal cortex,Decision-making,News aggregator,Psychology,Psychological concepts,Cognitive psychology,Brain,Interdisciplinary subfields,Cognition,Behavioural sciences,Mental processes,Cognitive science,Neuroscience]
---


The team then tested the role of these two brain structures (the submedius thalamic nucleus and orbitofrontal cortex) in decision-making and adaptive behavior. The initial learning phase allowed the animals to learn that two different sounds (S1 and S2) each signaled a specific food reward. The lesions did not prevent the animals from learning that an auditory stimulus predicts a reward. On the other hand, animals with a lesion -- of either the orbitofrontal cortex or the submedius thalamic nucleus -- proved incapable of making this distinction, and thus of adapting. This study therefore identified the existence of a circuit between the thalamus and the cortex, which proved crucial to adaptive decision-making.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150923134243.htm){:target="_blank" rel="noopener"}


