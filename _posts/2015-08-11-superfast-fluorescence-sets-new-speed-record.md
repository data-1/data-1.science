---
layout: post
title: "Superfast fluorescence sets new speed record"
date: 2015-08-11
categories:
author: Duke University 
tags: [Quantum dot,Plasmon,Photon,Integrated circuit,Optical computing,Laser,Optics,Electron,Computing,Light,Computer,Semiconductor,Physics,Electricity,Physical chemistry,Materials science,Applied and interdisciplinary physics,Electronics,Physical sciences,Electromagnetism,Electrodynamics,Science,Materials,Atomic molecular and optical physics,Electrical engineering,Technology,Chemistry,Electromagnetic radiation]
---


Duke University researchers are now one step closer to such a light source. In a new study, a team from the Pratt School of Engineering pushed semiconductor quantum dots to emit light at more than 90 billion gigahertz. This so-called plasmonic device could one day be used in optical computing chips or for optical communication between traditional electronic microchips. This field interacts with quantum dots -- spheres of semiconducting material just six nanometers wide -- that are sandwiched in between the nanocube and the gold. The eventual goal is to integrate our technology into a device that can be excited either optically or electrically, said Thang Hoang, also a postdoctoral researcher in Mikkelsen's laboratory.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/du-sfs072315.php){:target="_blank" rel="noopener"}


