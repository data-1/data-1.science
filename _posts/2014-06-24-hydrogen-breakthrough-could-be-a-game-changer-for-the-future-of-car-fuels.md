---
layout: post
title: "Hydrogen breakthrough could be a game-changer for the future of car fuels"
date: 2014-06-24
categories:
author: Marion O'Sullivan, Science, Technology Facilities Council
tags: [Ammonia,Fuel cell,Hydrogen,Fuel cell vehicle,Car,Electric vehicle,Hydrogen production,Fuel,Liquefied petroleum gas,Technology,Alternative fuel,Energy,Chemistry,Sustainable technologies,Nature]
---


A new discovery by scientists at the UK's Science and Technology Facilities Council (STFC), offers a viable solution to the challenges of storage and cost by using ammonia as a clean and secure hydrogen-containing energy source to produce hydrogen on-demand in situ. Many catalysts can effectively crack ammonia to release the hydrogen, but the best ones are very expensive precious metals. Professor Bill David, who led the STFC research team at the ISIS Neutron Source, said Our approach is as effective as the best current catalysts but the active material, sodium amide, costs pennies to produce. Batteries play a significant role in these cars but the vehicle range, which will be equivalent to conventional cars, will be provided by a fuel cell powered by hydrogen. Explore further A new solution for storing hydrogen fuel for alternative energy

<hr>

[Visit Link](http://phys.org/news322819044.html){:target="_blank" rel="noopener"}


