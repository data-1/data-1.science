---
layout: post
title: "Five years of soil moisture, ocean salinity and beyond"
date: 2015-09-03
categories:
author: "$author"   
tags: [Soil Moisture and Ocean Salinity,Weather forecasting,Oceanography,Space science,Hydrology,Natural environment,Meteorology,Earth phenomena,Applied and interdisciplinary physics,Nature,Physical geography,Earth sciences]
---


Applications Five years of soil moisture, ocean salinity and beyond 03/11/2014 8318 views 57 likes  ESA’s SMOS satellite has clocked up more than one billion kilometres orbiting Earth to improve our understanding of our planet’s water cycle. These images correspond to microwave radiation emitted from Earth’s surface and can be related to soil moisture and ocean salinity – two key variables in Earth’s water cycle. The animation above uses five years of SMOS data to show how, on average, moisture in the soil changes with the seasons around the world. For example, integrating these accurate near-realtime observations into the European Centre for Medium-Range Weather Forecasts’ (ECMWF) system is helping to improve air temperature and humidity forecasts near the surface. This multitalented mission also provides information to measure thin ice floating in the polar seas accurately enough for forecasting and ship routing.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/SMOS/Five_years_of_soil_moisture_ocean_salinity_and_beyond){:target="_blank" rel="noopener"}


