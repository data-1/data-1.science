---
layout: post
title: "Fossil find pushes back date of earliest fused bones in birds by 40 million years"
date: 2017-10-10
categories:
author: "Bob Yirka"
tags: [Bird,Hand,Fossil,Enantiornithes,Skeleton,Theropoda,Evolution,Paraves,Origin of birds,Taxa]
---


In their paper published in Proceedings of the National Academy of Sciences, Min Wang, Zhiheng Li and Zhonghe Zhou describe their study of the fossilized remains of a bird dated to approximately 120 million years ago. They note that such fusions have rarely been reported with birds of the Early Cretaceous and that the birds appeared to have followed a growth pattern similar to that of modern birds. However, the historical origins of these avian bone fusions remain elusive because of the rarity of transitional fossils and developmental studies on modern birds. Here, we describe an Early Cretaceous bird (120 Mya) that has fully fused alular-major metacarpals and pelvis. The fusions of these bones are rare in known nonavian theropods and Early Cretaceous birds but are well established among Late Cretaceous and modern birds, revealing a complicated evolution pattern unrecognized previously.

<hr>

[Visit Link](https://phys.org/news/2017-10-fossil-date-earliest-fused-bones.html){:target="_blank" rel="noopener"}


