---
layout: post
title: "Most detailed view ever of star formation in the distant universe"
date: 2016-05-07
categories:
author:  
tags: [Galaxy,Atacama Large Millimeter Array,Star formation,Einstein ring,Gravitational lens,Milky Way,European Southern Observatory,Star,Black hole,Universe,Nebula,Physical sciences,Astronomical objects,Science,Physics,Space science,Astronomy,Outer space,Astrophysics]
---


ALMA's Long Baseline Campaign has produced a spectacularly detailed image of a distant galaxy being gravitationally lensed, revealing star-forming regions - something that has never seen before at this level of detail in a galaxy so remote. The left panel shows the foreground lensing galaxy (observed with Hubble), and the gravitationally lensed galaxy SDP.81, which forms an almost perfect Einstein Ring, is hardly visible. The middle image shows the sharp ALMA image of the Einstein ring, with the foreground lensing galaxy being invisible to ALMA. A large galaxy sitting between SDP.81 and ALMA is acting as a lens, warping the more distant galaxy's light and creating a near-perfect example of a phenomenon known as an Einstein Ring. Credit: Credit: Y. Tamura (The University of Tokyo)/ALMA (ESO/NAOJ/NRAO) National Astronomical Observatory of Japan  Using the spectral information gathered by ALMA, astronomers also measured how the distant galaxy rotates, and estimated its mass.

<hr>

[Visit Link](http://phys.org/news352967351.html){:target="_blank" rel="noopener"}


