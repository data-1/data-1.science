---
layout: post
title: "Watch out! Brain network calculates impact of approaching object"
date: 2017-10-10
categories:
author: "Society for Neuroscience"
tags: [Neuroscience,American Association for the Advancement of Science,Cognition,Branches of science,Interdisciplinary subfields,Science,Cognitive science]
---


Using functional resonance magnetic imaging in two resus monkeys, Suliann Ben Hamed and colleagues found a brain network linking occipital, parietal, premotor and prefrontal regions that is maximally activated when a virtual cone rapidly approaching the monkey was paired with a puff of air to the monkey's cheek, delivered when the object would be expected to reach the face in a real-world setting. This finding suggests that the network actively used visual and to predict tactile information and enhance its processing, which may help the animal prepare for an imminent impact. ###  Article: The prediction of impact of a looming stimulus onto the body is subserved by multisensory integration mechanisms  DOI: https://doi.org/10.1523/JNEUROSCI.0610-17.2017  Corresponding author: Suliann Ben Hamed (Institut des Sciences Cognitives Marc Jeannerod, Bron, France) benhamed@isc.cnrs.fr  About JNeurosci  JNeurosci, the Society for Neuroscience's first journal, was launched in 1981 as a means to communicate the findings of the highest quality neuroscience research to the growing field. Today the journal remains committed to publishing cutting-edge neuroscience that will have an immediate and lasting scientific impact while responding to authors' changing publishing needs, representing breadth of the field and diversity in authorship. About The Society for Neuroscience  The Society for Neuroscience is the world's largest organization of scientists and physicians devoted to understanding the brain and nervous system.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/sfn-wob100517.php){:target="_blank" rel="noopener"}


