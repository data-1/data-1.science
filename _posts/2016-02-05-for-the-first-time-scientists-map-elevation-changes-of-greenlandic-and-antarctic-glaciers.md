---
layout: post
title: "For the first time scientists map elevation changes of Greenlandic and Antarctic glaciers"
date: 2016-02-05
categories:
author: Helmholtz Association Of German Research Centres
tags: [Ice sheet,Greenland ice sheet,Antarctica,Jakobshavn Glacier,Glacier,Cryosphere,CryoSat-2,Physical geography,Earth sciences]
---


This is 500,000 square kilometres more than any previous elevation model from altimetry, says lead-author Dr. Veit Helm, glaciologist at the Alfred Wegener Institute in Bremerhaven. For the new digital maps, the AWI scientists had evaluated all data by the CryoSat-2 altimeter SIRAL. The team derived the elevation change maps using over 200 million SIRAL data points for Antarctica and around 14.3 million data points for Greenland. Credit: Credit: Helm et al., The Cryosphere, 2014  The areas where the researchers detected the largest elevation changes were Jakobshavn Isbrae (Jakobshavn Glacier) in West Greenland and Pine Island Glacier in West Antarctica. But whereas both the West Antarctic Ice Sheet and the Antarctic Peninsula, on the far west of the continent, are rapidly losing volume, East Antarctica is gaining volume – though at a moderate rate that doesn't compensate the losses on the other side of the continent.

<hr>

[Visit Link](http://phys.org/news327746205.html){:target="_blank" rel="noopener"}


