---
layout: post
title: "Research unlocks molecular key to animal evolution and disease"
date: 2017-09-11
categories:
author: "Vanderbilt University Medical Center"
tags: [Animal,Cell (biology),Tissue engineering,Ctenophora,Multicellular organism,Collagen,Evolution,Life,News aggregator,Sponge,Organism,Nature,Biology]
---


The dawn of the Animal Kingdom began with a collagen scaffold that enabled the organization of cells into tissues. The fundamental principles of tissue development are present in ancient animals, Hudson said. It sets us up to develop a deeper understanding of tissue biology and the cause of a multitude of diseases. The team analyzed tissues from creatures as ancient as sponges and comb jellies. We found that among all the collagens that make up the human body, collagen IV was the key innovation enabling single-celled organisms to evolve into multicellular animals, said first author Aaron Fidler, a graduate student mentored by Hudson.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/04/170418181755.htm){:target="_blank" rel="noopener"}


