---
layout: post
title: "CLOCK gene may hold answers to human brain evolution"
date: 2018-08-02
categories:
author: "UT Southwestern Medical Center"
tags: [Brain,CLOCK,Circadian rhythm,Evolution of the brain,Evolution,Neuroscience,Cognitive science,Biology]
---


The findings from the O'Donnell Brain Institute open new paths of research into how CLOCK proteins produced by the CLOCK gene affect brain function and the processes by which neurons find their proper place in the brain. We now have evidence that CLOCK regulates many genes outside of circadian rhythms, so we can place it as a key point in the hierarchy of important molecular pathways for human brain development and evolution. CLOCK regulates genes linked to cognitive disorders, and has an important role in human neuronal migration -- the process by which neurons born in other parts of the brain travel to the appropriate neural circuits. A novel function of the CLOCK gene in the brain not directly related to circadian rhythms is unexpected, and its possible role in the evolution of the human neocortex is very exciting, said Dr. Takahashi, a corresponding author on the new study, Chairman of Neuroscience at UT Southwestern, Investigator for the Howard Hughes Medical Institute, and holder of the Loyd B. Sands Distinguished Chair in Neuroscience. Dr. Konopka's research will also involve humanized mice, which have been given a boost of CLOCK in their neocortex.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/12/171206141624.htm){:target="_blank" rel="noopener"}


