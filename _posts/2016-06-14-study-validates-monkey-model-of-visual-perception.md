---
layout: post
title: "Study validates monkey model of visual perception"
date: 2016-06-14
categories:
author: "Society for Neuroscience (SfN)"
tags: [Visual object recognition (animal test),Perception,Visual perception,News aggregator,Human,Branches of science,Cognition,Interdisciplinary subfields,Psychology,Cognitive psychology,Mental processes,Behavioural sciences,Psychological concepts,Neuroscience,Cognitive science,Cognitive neuroscience,Neuropsychology,Concepts in metaphysics,Academic discipline interactions]
---


A new study from The Journal of Neuroscience shows that humans and rhesus monkeys have very similar abilities in recognizing objects at a glance, validating the use of this animal model in the study of human visual perception. Images were presented for less than a second and then the monkeys selected the correct object from two choices. The researchers found that:  · Humans' and monkeys' performance across a large number of object recognition tasks was highly correlated, suggesting that they have similar abilities to recognize objects. The study shows that monkeys are similar to humans, not only in their ability to recognize objects, but also in their patterns of errors, said Nikolaus Kriegeskorte, a neuroscientist at the University of Cambridge who studies visual object recognition and was not involved in the study. This is consistent with the similarity of the brain representations of objects between the two species, which had been demonstrated previously.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150825210544.htm){:target="_blank" rel="noopener"}


