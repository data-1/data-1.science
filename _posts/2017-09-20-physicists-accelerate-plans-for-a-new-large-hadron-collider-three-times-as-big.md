---
layout: post
title: "Physicists accelerate plans for a new Large Hadron Collider three times as big"
date: 2017-09-20
categories:
author: "Joe Dodgshun, Horizon Magazine, Horizon, The Eu Research, Innovation Magazine"
tags: [Particle physics,Large Hadron Collider,CERN,Collider,Particle accelerator,Future Circular Collider,Applied and interdisciplinary physics,Physical sciences,Science,Physics]
---


Credit: CERN  An international league of scientists is kicking off the decades-long process of developing the successor to the Large Hadron Collider, the world's largest and most powerful particle accelerator. The event was organised by the Future Circular Collider (FCC) Study, an international collaboration of physicists, and focused on developing the next Large Hadron Collider (LHC), which will be seven times more powerful. According to Professor Michael Benedikt, leader of the FCC, this energy leap could let us spot previously unobserved particles even heavier than the Higgs boson, which would give a deeper insight into the laws that govern the universe. Future physics  Prof. Benedikt is confident the accelerator design concepts will lead to the performance we want and need. I put a paper box on the table, the camera and app see it as an ion particle source sitting on my office table—similar to Pokémon Go—and here I can see particles flying all over my desk.

<hr>

[Visit Link](https://phys.org/news/2017-05-physicists-large-hadron-collider-big.html){:target="_blank" rel="noopener"}


