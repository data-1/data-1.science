---
layout: post
title: "Best of Blue Dot"
date: 2015-09-03
categories:
author: "$author"   
tags: [Spaceflight technology,Outer space,Astronautics,Human spaceflight,Spaceflight,Space programs,Life in space,Flight,Spacecraft,Space exploration,Space program of the United States,NASA,Space vehicles,Space agencies,Space science,Human spaceflight programs,Space research,Space-based economy,NASA programs,Aerospace]
---


This image gallery features the best images taken by ESA astronaut Alexander Gerst during his Blue Dot mission as chosen by his followers on Facebook. Alexander spent six months on the International Space Station in 2014 to run experiments for scientists on Earth and maintain the weightless research station. His tasks included a spacewalk to upgrade a power supply, overseeing the arrival of ATV Georges Lemaître and installing a new furnace to research metal alloys. Together with crewmates NASA astronaut Reid Wiseman and Russian commander Maxim Suraev, Alexander took thousands of images of Earth from the Cupola observatory in his free time. Read more about the Blue Dot mission at http://www.esa.int/Our_Activities/Human_Spaceflight/Blue_dot  Follow Alexander via alexandergerst.esa.int

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Blue_dot/Highlights/Best_of_Blue_Dot2){:target="_blank" rel="noopener"}


