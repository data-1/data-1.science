---
layout: post
title: "The symmetry of the universe"
date: 2016-06-25
categories:
author: "Technical University of Munich (TUM)"
tags: [ALICE experiment,Quarkgluon plasma,Universe,Antiparticle,Physics,Nuclear physics,Quantum mechanics,Particle physics,Theoretical physics,Physical sciences,Nature,Applied and interdisciplinary physics,Science]
---


A state is created that is very similar to the one after the Big Bang, explains Laura Fabbietti, Professor in the Physics Department. According to the CPT theorem (charge, parity, time), there is a fundamental symmetry between particles and antiparticles in our Universe. Specifically, there should be no difference between our Universe and one where all particles are exchanged with antiparticles (and vice versa), if the Universe is also inverted in time and space. ALICE is attempting to find a difference by means of high-precision measurements of the properties of particles and their antiparticles which are produced in particle collisions at the LHC, explains Dahms. The researchers are currently working on improvements to the ALICE detectors with the aim of making the investigations even more precise.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/tuom-tso090215.php){:target="_blank" rel="noopener"}


