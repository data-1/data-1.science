---
layout: post
title: "Pterodactyl: Facts about pteranodon & other pterosaurs"
date: 2016-03-23
categories:
author: Joseph Castro
tags: [Pterosaur,Pterodactylus,Quetzalcoatlus,Dinosaur,Pteranodon,Taxa,Animals]
---


Rather, pterosaurs were flying reptiles. The remains of this flying reptile revealed that over half of the length of its wing was occupied by a long finger, which anchored the membrane that made up the wing to the body, according to the Carnegie Museum of Natural History (opens in new tab). One of the largest pterosaurs is believed to be Quetzalcoatlus northropi, whose wingspan reached 36 feet (11 m), according to the journal PLOS ONE (opens in new tab). What the reptiles ate depended on where they lived — some species spent their lives around water, while others were more terrestrial. For example, dimorphodon – a roughly 3 feet (1 meter) long pterosaur – had previously believed to be a fishing reptile, until dental evidence revealed wear patterns that would have been produced by eating insects and land vertebrates rather than marine life.

<hr>

[Visit Link](http://www.livescience.com/24071-pterodactyl-pteranodon-flying-dinosaurs.html){:target="_blank" rel="noopener"}


