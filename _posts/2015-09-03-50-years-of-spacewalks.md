---
layout: post
title: "50 years of spacewalks"
date: 2015-09-03
categories:
author: "$author"   
tags: [Extravehicular activity,Astronaut,Human spaceflight programs,Crewed spacecraft,Astronauts,Space programs,Spaceflight technology,Aerospace,Life in space,Vehicle operation,Spaceflight,NASA programs,Outer space,Flight,Spacecraft,Space vehicles,NASA,Space science,Aeronauts,Space program of the United States,Human spaceflight,Space exploration,Astronautics]
---


Taking a hair-raising decision, he opened a valve on the suit to let enough air escape for him to enter the airlock. His spacewalk lasted only 12 minutes but proved that astronauts could work outside a spacecraft. The first European to do a spacewalk was the French spationaute Jean-Loup Chrétien, who flew to the Russian Mir space station in 1988. The most recent spacewalk by an ESA astronaut was Alexander Gerst during his Blue Dot mission in 2014. In the 50 years since Leonov’s first spacewalk, more than 200 astronauts from 10 countries have left their spacecraft to work outside.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Highlights/50_years_of_spacewalks){:target="_blank" rel="noopener"}


