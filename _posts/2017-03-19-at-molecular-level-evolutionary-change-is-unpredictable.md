---
layout: post
title: "At molecular level, evolutionary change is unpredictable"
date: 2017-03-19
categories:
author: "Gillian Klucas, University Of Nebraska-Lincoln"
tags: [Evolution,Genetics,Mutation,Species,Natural selection,Phenotypic trait,Biology,Hemoglobin,Adaptation,Protein,Nature,Science,Biological evolution,Evolutionary biology]
---


The team's findings appear in the Oct. 21 issue of the journal Science. There's this really long-standing question in evolutionary genetics about the predictability of genetic change, said Storz, Susan J. Rosowski professor of biological sciences. In other words, did species with a common, beneficial trait undergo the same genetic changes to evolve that trait? Storz and his team tested the hemoglobin proteins from numerous high-altitude bird species and identified which differences, or mutations, in the proteins' makeup were responsible for the high-altitude trait. As evolution advances through time, different mutations accumulate in distinct species and settings.

<hr>

[Visit Link](http://phys.org/news/2016-10-molecular-evolutionary-unpredictable.html){:target="_blank" rel="noopener"}


