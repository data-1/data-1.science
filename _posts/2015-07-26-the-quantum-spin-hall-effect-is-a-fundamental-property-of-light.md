---
layout: post
title: "The quantum spin Hall effect is a fundamental property of light"
date: 2015-07-26
categories:
author: ""   
tags: [Spin (physics),Evanescent field,Photon,Electron,Light,Wave,Electromagnetic radiation,Hall effect,Electromagnetism,Physical phenomena,Quantum mechanics,Physical sciences,Theoretical physics,Physics,Applied and interdisciplinary physics,Science]
---


The spin—a measure of the intrinsic angular momentum—can be thought of as an equivalent of the spin of a top. In the research published in Science, the team found that photons share with electrons a property related to spin—the quantum spin Hall effect. We had previously done work looking at evanescent electromagnetic waves, says Konstantin Bliokh, who led the research, and we realized the remarkable properties we found, an unusual transverse spin—was a manifestation of the fact that free-space light exhibits an intrinsic quantum spin Hall effect, meaning that evanescent waves with opposite spins will travel in opposite directions along an interface between two media. Bliokh continues, On a purely scientific level, this research deepens our understanding of the classical theory of light waves developed by James Clark Maxwell 150 years ago, and it could also lead to applications using optical devices that are based on the direction of spin. Franco Nori, who organized the project, says, This work was made possible by the interdisciplinary nature of RIKEN, as we were able to bring together discoveries made in several different areas, to show that transverse spin, locked to the direction of propagation of waves, seems to be a universal feature of surface waves, even when they are of different nature.

<hr>

[Visit Link](http://phys.org/news354449063.html){:target="_blank" rel="noopener"}


