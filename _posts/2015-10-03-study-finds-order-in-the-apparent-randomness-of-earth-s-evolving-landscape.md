---
layout: post
title: "Study finds order in the apparent randomness of Earth's evolving landscape"
date: 2015-10-03
categories:
author: Miles Traer, Stanford University
tags: [Titan (moon),Mars,Science]
---


Stanford Earth scientists have created tools to analyze branched networks of Earth-bound channels formed by water and erosion. The work could provide insights into processes that form branched networks like those on the surface of Mars (left) and Titan (center) and in the human circulatory system. Shelef's research could help scientists better understand the processes acting on Titan today and the processes that carved out channel networks on Mars millions, if not billions, of years ago. The pair's analysis of branched networks needn't be limited to flowing liquids. By seeing past the randomness inferred from previous research, Shelef and Hilley can better understand the processes that shape the world around us.

<hr>

[Visit Link](http://phys.org/news324536343.html){:target="_blank" rel="noopener"}


