---
layout: post
title: "Single gene controls fish brain size and intelligence"
date: 2016-05-10
categories:
author: University College London
tags: [Zebrafish,Brain,Genetics,Biology,Biotechnology,Life sciences,Molecular biology,Neuroscience]
---


A single gene called Angiopoietin-1 (Ang-1) drives brain size and intelligence in fish according to a new study by researchers at UCL, Stockholm University and University of Helsinki. Ang-1 could play an important role in the brain development of other vertebrates, including humans, but future research is required to establish this say the scientists involved. Populations of guppies selected for either large or small brains, with associated differences in intelligence, were used for the first step in the study which was a complete genome analysis of differently expressed genes. There was a 10% difference in brain size between the large and small-brain guppies and from the genetic analysis, Ang-1 was identified as the only gene expressed at different levels in each replicate population. Future studies will aim to investigate the role of Ang-1 and possibly other genes in the formation of differently sized brains in developing embryos.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-06/ucl-sgc062215.php){:target="_blank" rel="noopener"}


