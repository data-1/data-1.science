---
layout: post
title: "Mainstream academia embraces open source hardware"
date: 2018-04-22
categories:
author: "Joshua Pearce
(Alumni)"
tags: [Open-source hardware,Open access,Academic journal,Free and open-source software,Research,Academia,Science,Technology]
---


Consider the graph below, which shows the number of articles on open source hardware indexed by Google Scholar each year from 2000 to 2017.  opensource.com  In the last 17 years, the concept of open source hardware has erupted in ivory towers throughout the world. There is also the Journal of Open Source Hardware published by Ubiquity Press, PLOS One from the Public Library of Science, and numerous titles from MDPI such as Designs, all of which publish free, open access, open source hardware articles. Today, due to the rate of free and open source hardware papers being published, it is now relatively easy for academics to dedicate their careers to advancing free hardware. This means we can expect a lot more growth, as most academics, who have already dedicated their lives to information sharing, are familiar with the ethics behind the open source hardware culture. Finally, making valuable equipment open source is a strategic move for professors who want to garner citations when others use and cite it for years to come.

<hr>

[Visit Link](https://opensource.com/article/18/4/mainstream-academia-embraces-open-source-hardware){:target="_blank" rel="noopener"}


