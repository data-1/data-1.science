---
layout: post
title: "Open-source microprocessor"
date: 2016-04-10
categories:
author: ETH Zurich
tags: [Open-source software,Open-source hardware,Microprocessor,Central processing unit,Integrated circuit,Computers,Computer architecture,Electronics industry,Information Age,Office equipment,Computer hardware,Manufactured goods,Computer science,Electrical engineering,Digital media,Computer engineering,Computing,Technology]
---


Not so with open-source products. A few days ago, scientists at ETH Zurich and the University of Bologna, led by ETH Professor Luca Benini, open sourced the full design of one of their microprocessor systems - in a way that maximises the freedom of other developers to use and change the system, says Benini. The scientists want to work with other project partners to jointly develop academically interesting extensions to PULPino; these would also be open source, thus allowing the number of the hardware's functional components to steadily grow. The production of microchips has become cheap in recent years because semiconductor manufacturers have built up large production capacities that they must use, explains Benini. Development costs are reduced considerably with the open-source royalty-free design, which benefits SMEs as well as ETH, says Benini: It could result in new research and development partnerships with industry to jointly develop novel chip components on the basis of PULPino.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/ez-om033016.php){:target="_blank" rel="noopener"}


