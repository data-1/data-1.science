---
layout: post
title: "Brain patterns underlying mothers' responses to infant cries: Behaviors and brain activity consistent between mothers from different countries"
date: 2017-10-24
categories:
author: NIH/Eunice Kennedy Shriver National Institute of Child Health and Human Development
tags: [Functional magnetic resonance imaging,Developmental psychology,Infant crying,Infant,Brain,Health,News aggregator,Psychological concepts,Cognitive science,Neuroscience,Behavioural sciences,Psychology,Human development,Branches of science]
---


Infant cries activate specific brain regions related to movement and speech, according to a National Institutes of Health study of mothers in 11 countries. The findings, led by researchers at NIH's Eunice Kennedy Shriver National Institute of Child Health and Human Development (NICHD), identify behaviors and underlying brain activities that are consistent among mothers from different cultures. The study team conducted a series of behavioral and brain imaging studies using functional magnetic resonance imaging (fMRI). In a group of 684 new mothers in Argentina, Belgium, Brazil, Cameroon, France, Israel, Italy, Japan, Kenya, South Korea and the United States, researchers observed and recorded one hour of interaction between the mothers and their 5-month-old babies at home. The team analyzed whether mothers responded to their baby's cries by showing affection, distracting, nurturing (like feeding or diapering), picking up and holding, or talking.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171023182701.htm){:target="_blank" rel="noopener"}


