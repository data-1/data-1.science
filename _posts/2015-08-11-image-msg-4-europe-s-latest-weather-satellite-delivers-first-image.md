---
layout: post
title: "Image: MSG-4, Europe's latest weather satellite, delivers first image"
date: 2015-08-11
categories:
author: European Space Agency
tags: [Meteosat,Weather satellite,European Organisation for the Exploitation of Meteorological Satellites,Outer space,Technology,Satellites,Spaceflight,Space science]
---


Credit: Eumetsat  Today, the Spinning Enhanced Visible and Infrared Imager instrument on MSG-4 captured its first image of Earth. This demonstrates that Europe's latest geostationary weather satellite, launched on 15 July, is performing well and is on its way to becoming fully operational when needed after six months of commissioning. ESA was responsible for the initial operations after launch (the so-called launch and early orbit phase) of MSG-4 and handed over the satellite to EUMETSAT on 26 July. The first image is a joint achievement by ESA, EUMETSAT and European space industry. For its mandatory programmes, EUMETSAT relies on ESA to develop new satellites and procure the recurrent satellites like MSG-4.

<hr>

[Visit Link](http://phys.org/news/2015-08-image-msg-europe-latest-weather.html){:target="_blank" rel="noopener"}


