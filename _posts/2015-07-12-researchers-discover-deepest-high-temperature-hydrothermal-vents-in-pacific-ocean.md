---
layout: post
title: "Researchers discover deepest high-temperature hydrothermal vents in Pacific Ocean"
date: 2015-07-12
categories:
author: Monterey Bay Aquarium Research Institute
tags: [Hydrothermal vent,Monterey Bay Aquarium Research Institute,Seabed,Earth sciences,Physical geography,Hydrography,Geology,Hydrology,Oceanography]
---


These delicate carbonate spires formed at an active vent site in the newly discovered Pescadero Basin hydrothermal field. They are also the only vents in the Pacific known to emit superheated fluids rich in both carbonate minerals and hydrocarbons. The vents have been colonized by dense communities of tubeworms and other animals unlike any other known vent communities in the in the eastern Pacific. Like another vent field in the Gulf that MBARI discovered in 2012, the Pescadero Basin vents were initially identified in high-resolution sonar data collected by an autonomous underwater vehicle (AUV). Clague's and Vrijenhoek's dives revealed at least three different types of hydrothermal vents in the southern Gulf of California—black smokers, carbonate chimneys, and hydrothermal seeps.

<hr>

[Visit Link](http://phys.org/news352487317.html){:target="_blank" rel="noopener"}


