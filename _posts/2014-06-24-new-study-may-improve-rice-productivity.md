---
layout: post
title: "New study may improve rice productivity"
date: 2014-06-24
categories:
author: University Of Illinois At Urbana-Champaign
tags: [Rice,Photosynthesis,Drought tolerance,Realizing Increased Photosynthetic Efficiency]
---


In this picture, professor Yu Tanaka of Kyoto University plants rice varieties in a paddy at the University of Illinois at Urbana-Champaign. This experiment is part of the Realizing Increased Photosynthetic Efficiency (RIPE) project, a five-year effort funded by a $25 million grant from the Bill & Melinda Gates Foundation to substantially improve the productivity of worldwide staple food crops. When we consider actual production, or the crops' physiological responses and performance, it is really important that we grow the rice in the fields, said Yu Tanaka, a visiting professor from Kyoto University who is leading the study at Illinois. When rice is grown in a paddy field, there is definitely no shortage of water, Tanaka said. I was impressed by Steve Long's progress to achieve increased crop production through photosynthesis, Tanaka said.

<hr>

[Visit Link](http://phys.org/news322745943.html){:target="_blank" rel="noopener"}


