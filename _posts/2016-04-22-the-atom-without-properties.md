---
layout: post
title: "The atom without properties"
date: 2016-04-22
categories:
author: University of Basel
tags: [Quantum mechanics,Quantum entanglement,News aggregator,Bells theorem,Atom,Scientific method,Scientific theories,Physical sciences,Applied and interdisciplinary physics,Science,Theoretical physics,Physics]
---


Experimental test of Bell correlations  With the (false) assumption that atoms possess their properties independently of measurements and independently of each other, a so-called Bell inequality can be derived. If it is violated by the results of an experiment, it follows that the properties of the atoms must be interdependent. This is described as Bell correlations between atoms, which also imply that each atom takes on its properties only at the moment of the measurement. A team of researchers led by professors Nicolas Sangouard and Philipp Treutlein from the University of Basel, along with colleagues from Singapore, have now observed these Bell correlations for the first time in a relatively large system, specifically among 480 atoms in a Bose-Einstein condensate. After the atoms have become entangled through collisions, researchers count how many of the atoms are actually in each of the two states.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/04/160421141510.htm){:target="_blank" rel="noopener"}


