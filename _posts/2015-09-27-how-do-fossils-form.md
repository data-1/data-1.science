---
layout: post
title: "How Do Fossils Form?"
date: 2015-09-27
categories:
author: Joseph Castro
tags: [Fossil,Sedimentary rock,Nature]
---


These crystallized minerals cause the remains to harden along with the encasing sedimentary rock. If an organism completely dissolves in sedimentary rock, it can leave an impression of its exterior in the rock, called an external mold. It's unclear how the organic material is preserved, but iron might help the proteins become cross-linked and unrecognizable, or unavailable to the bacteria that would otherwise consume them, Lacovara said. Moreover, sandstone — rock made of sand-size grains of minerals, sediments or inorganic material — seems to be the best type of environment for preserving organic material in fossils. Original article on Live Science.

<hr>

[Visit Link](http://www.livescience.com/37781-how-do-fossils-form-rocks.html){:target="_blank" rel="noopener"}


