---
layout: post
title: "Magnetic skyrmions could help make low-energy artificial 'brains' – Physics World"
date: 2017-10-08
categories:
author: "Anna Demming"
tags: [Synapse,Skyrmion,Chemical synapse,Spintronics,Neuromorphic engineering,Neuroscience,Physics]
---


Magnetic brain: skyrmions could mimic the memory and learning. Efforts to emulate the way the brain is wired have led to work on “artificial synapses” as connections for use in “neuromorphic” computers that try to emulate the functionality of a biological brain. Energy barrier  Skyrmion racetracks have been studied before as possible electronic memory components. The simulations suggest that the skyrmion synaptic devices operate with very low energy dissipation, explains Huang. “Only a simulation”  “But it is only a simulation,” adds Huang, emphasising that most other synaptic devices have already been built and demonstrated.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2017/jan/31/magnetic-skyrmions-could-help-make-low-energy-artificial-brains){:target="_blank" rel="noopener"}


