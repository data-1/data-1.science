---
layout: post
title: "Galileo satellites set for year-long Einstein experiment"
date: 2015-11-23
categories:
author: "$author"  
tags: [Gravity Probe A,Atomic clock,Orbit,Satellite navigation,Galileo Galilei,Satellite,Navigation,Albert Einstein,Physical sciences,Spaceflight,Outer space,Science,Astronomy,Physics,Space science]
---


Applications Galileo satellites set for year-long Einstein experiment 09/11/2015 13062 views 137 likes  Europe’s fifth and sixth Galileo satellites – subject to complex salvage manoeuvres following their launch last year into incorrect orbits – will help to perform an ambitious year-long test of Einstein’s most famous theory. But the faulty upper stage stranded them in elongated orbits that blocked their use for navigation. Albert Einstein “In the meantime, the satellites have accidentally become extremely useful scientifically, as tools to test Einstein’s General Theory of Relativity by measuring more accurately than ever before the way that gravity affects the passing of time.” Although the satellites’ orbits have been adjusted, they remain elliptical, with each satellite climbing and falling some 8500 km twice per day. It has been verified experimentally, most significantly in 1976 when a hydrogen maser atomic clock on Gravity Probe A was launched 10 000 km into space, confirming the prediction to within 140 parts in a million. For more information, please contact: Javier Ventura-Traveset  ESA Global Navigation Satellite Systems Senior Advisor  Email: Javier.ventura-traveset@esa.int Pacôme Delva  SYRTE, Observatoire de Paris  Email: pacome.delva@obspm.fr Sven Hermann  ZARM Center of Applied Space Technology and Microgravity  Email: sven.herrmann@zarm.uni-bremen.de

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Navigation/Galileo_satellites_set_for_year-long_Einstein_experiment){:target="_blank" rel="noopener"}


