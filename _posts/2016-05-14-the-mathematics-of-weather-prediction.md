---
layout: post
title: "The mathematics of weather prediction"
date: 2016-05-14
categories:
author: Noaa Headquarters
tags: [Numerical weather prediction,Weather forecasting,Weather,Earth sciences,Meteorology,Physical geography,Applied and interdisciplinary physics,Atmospheric sciences,Nature]
---


Every day, the supercomputers collect and organize billions of earth observations, such as temperature, air pressure, moisture, wind speed and water levels, which are critical to initialize all numerical weather prediction models. All these observations are represented by numbers. Using math to model the future state of the atmosphere is called numerical weather prediction, a branch of atmospheric sciences that was pioneered after World War II, but really took off in helping make reliable weather predictions in the U.S. in the 1980s with advancements in computing and the development of the global model system. Model agreement leads to higher certainty in forecasts. Today's forecasts and the improved forecasts of the future are made possible by sustained investments in observing systems, weather and climate models, and the supercomputers that power them.

<hr>

[Visit Link](http://phys.org/news/2016-05-mathematics-weather.html){:target="_blank" rel="noopener"}


