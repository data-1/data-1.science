---
layout: post
title: "Directly reprogramming a cell's identity with gene editing"
date: 2016-08-22
categories:
author: "Duke University"
tags: [Cell potency,Gene,Stem cell,Induced pluripotent stem cell,CRISPR,Biochemistry,Molecular biology,Genetics,Biotechnology,Life sciences,Biology,Cell biology,Branches of genetics]
---


If you want to change that destination, one option is to push the cell vertically back up the mountain -- that's the idea behind reprogramming cells to be induced pluripotent stem cells. If you have the ability to specifically turn on all the neuron genes, maybe you don't have to go back up the hill, said Gersbach. In the new study, Black, Gersbach, and colleagues used CRISPR to precisely activate the three genes that naturally produce the master transcription factors that control the neuronal gene network, rather than having a virus introduce extra copies of those genes. The tests showed that, once activated by CRISPR, the three neuronal master transcription factor genes robustly activated neuronal genes. The method that introduces extra genetic copies with the virus produces a lot of the transcription factors, but very little is being made from the native copies of these genes, explained Black.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/du-dra081116.php){:target="_blank" rel="noopener"}


