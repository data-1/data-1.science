---
layout: post
title: "Permanent data storage with light"
date: 2015-10-02
categories:
author: Karlsruher Institut für Technologie (KIT)  
tags: [Computer data storage,Computer,Non-volatile memory,Karlsruhe Institute of Technology,Optics,Photonics,American Association for the Advancement of Science,Electronics,Technology,Information and communications technology,Information Age,Computer science,Computing,Computer engineering,Electrical engineering]
---


The first all-optical permanent on-chip memory has been developed by scientists of Karlsruhe Institute of Technology (KIT) and the universities of Münster, Oxford, and Exeter. Phase change materials that change their optical properties depending on the arrangement of the atoms allow for the storage of several bits in a single cell. But on a computer, data are still processed and stored electronically. The memory is compatible not only with conventional optical fiber data transmission, but also with latest processors, Professor Harish Bhaskaran of Oxford University adds. The change from crystalline to amorphous (storing data) and from amorphous to crystalline (erasing data) is initiated by ultrashort light pulses.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/kift-pds092215.php){:target="_blank" rel="noopener"}


