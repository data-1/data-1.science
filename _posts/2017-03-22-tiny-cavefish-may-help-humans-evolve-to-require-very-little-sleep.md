---
layout: post
title: "Tiny cavefish may help humans evolve to require very little sleep"
date: 2017-03-22
categories:
author: "Florida Atlantic University"
tags: [Sleep,Evolution,Brain,Sleep deprivation,Sensory nervous system,Convergent evolution,Florida Atlantic University,Nervous system,Cognition,Neuroscience,Cognitive science,Psychology,Psychological concepts,Interdisciplinary subfields,Branches of science,Behavioural sciences]
---


The study also provides a model for understanding how the brain's sensory systems modulate sleep and sheds light into the evolution of the significant differences in sleep duration observed throughout the animal kingdom. Animals have dramatic differences in sleep with some sleeping as much as 20 hours and others as little as two hours and no one knows why these dramatic differences in sleep exist, said Alex C. Keene, Ph.D., corresponding author of the study and an associate professor in the Department of Biological Sciences in FAU's Charles E. Schmidt College of Science. Our study suggests that differences in sensory systems may contribute to this sleep variability. It is possible that evolution drives sensory changes and changes in sleep are a secondary consequence, or that evolution selects for changes in sensory processing in order to change sleep. We were surprised to find that there are multiple independent mechanisms regulating sleep loss in different cave populations and this can be a significant strength moving forward, said James Jaggard, first author and a graduate student at FAU working with Keene.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/fau-tcm022217.php){:target="_blank" rel="noopener"}


