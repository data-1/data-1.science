---
layout: post
title: "Magnetospheric Multiscale (MMS) mission puts magnetic reconnection under the microscope"
date: 2016-05-14
categories:
author: Southwest Research Institute
tags: [Magnetic reconnection,Solar wind,Plasma (physics),Magnetosphere,Sun,Goddard Space Flight Center,Magnetopause,Physics,Solar System,Applied and interdisciplinary physics,Electrical engineering,Space plasmas,Science,Phases of matter,Astrophysics,Plasma physics,Physical sciences,Space science,Outer space,Nature,Electromagnetism,Astronomy]
---


In this boundary between the solar wind and Earth's magnetosphere, the scientists are searching for locations where the solar wind magnetic field and the terrestrial magnetic field reconnect. The spacecraft passed directly through the electron dissipation region, and we were able to perform the first-ever physics experiment in this environment. Examining the data from the encounter, the MMS team saw a drop in the magnetic field to near zero, oppositely directed ion flows, accelerated electrons, an enhanced electric field, and a strong electrical current -- all indications that the spacecraft had entered the dissipation region. Another feature observed for the first time by MMS as it traversed the dissipation region was a rapid change in the electrons as they streamed into the dissipation region and were accelerated outward along field lines opened during reconnection. This observation was the first definitive measurement of the interconnection of the solar and terrestrial magnetic fields.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/sri-mm050916.php){:target="_blank" rel="noopener"}


