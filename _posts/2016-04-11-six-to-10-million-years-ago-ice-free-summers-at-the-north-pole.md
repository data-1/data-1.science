---
layout: post
title: "Six to 10 million years ago: Ice-free summers at the North Pole"
date: 2016-04-11
categories:
author: Alfred Wegener Institute
tags: [Arctic Ocean,Alfred Wegener Institute for Polar and Marine Research,Climate,Arctic,Sea ice,Water,Ocean,Paleoclimatology,Sea,Oceanography,Hydrology,Environmental science,Natural environment,Applied and interdisciplinary physics,Nature,Physical geography,Earth sciences]
---


In this context, one of our expedition's aims was to recover long sediment cores from the central Arctic, that can be used to reconstruct the history of the ocean's sea ice cover throughout the past 50 million years. Some scientists were of the opinion that the central Arctic Ocean was already covered with dense sea ice all year round six to ten million years ago - roughly to the same extent as today. Our data clearly indicate that six to ten million years ago, the North Pole and the entire central Arctic Ocean must in fact have been ice-free in the summer, says Stein. Ruediger Stein: By combining our data records on surface water temperature and on sea ice, we are now able to prove for the first time that six to ten million years ago, the central Arctic Ocean was ice-free in the summer. New climate data help to improve climate models  These new findings of the Arctic Ocean climate history reconstructed from sediment data, are further corroborated by climate simulations, as was shown by the AWI modellers who participated in this study.

<hr>

[Visit Link](http://phys.org/news/2016-04-million-years-ice-free-summers-north.html){:target="_blank" rel="noopener"}


