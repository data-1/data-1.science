---
layout: post
title: "Neutron tomography: Insights into the interior of teeth, root balls, batteries, and fuel cells"
date: 2018-06-07
categories:
author: "Helmholtz-Zentrum Berlin für Materialien und Energie"
tags: [Neutron,News aggregator,Medical imaging,Chemistry]
---


The authors report on the latest developments in neutron tomography, illustrating the possible applications using examples of this non-destructive method. Neutron tomography has facilitated breakthroughs in so diverse areas such as art history, battery research, dentistry, energy materials, industrial research, magnetism, palaeobiology and plant physiology. Neutrons can penetrate deep into a sample without destroying it. This makes them a versatile and powerful tool for materials research. Faster images are also possible now, which makes observing processes in materials feasible, such as the in operando measurements of a fuel cell during its actual operation that shows precisely how the water is distributed in it.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180605112121.htm){:target="_blank" rel="noopener"}


