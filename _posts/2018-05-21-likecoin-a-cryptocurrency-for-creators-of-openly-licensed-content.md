---
layout: post
title: "LikeCoin, a cryptocurrency for creators of openly licensed content"
date: 2018-05-21
categories:
author: "Kin Ko"
tags: [GitHub,Ethereum,Information Age,Communication,Information technology,Computing,Technology,Mass media,Software]
---


Conventional wisdom indicates that writers, photographers, artists, and other creators who share their content for free, under Creative Commons and other open licenses, won't get paid. Because openly licensed content has more opportunity to be reused and earn LikeCoin tokens, the system encourages content creators to publish under Creative Commons licenses. LikeCoin tokens will be distributed to creators using information about a work's derivation history. Based on content-footprint tracing in the LikeCoin protocol, the LikeRank measures the importance (or creativity as we define it in this context) of a creative content. In general, the more derivative works a creative content generates, the more creative the creative content is, and thus the higher LikeRank of the content.

<hr>

[Visit Link](https://opensource.com/article/18/5/likecoin){:target="_blank" rel="noopener"}


