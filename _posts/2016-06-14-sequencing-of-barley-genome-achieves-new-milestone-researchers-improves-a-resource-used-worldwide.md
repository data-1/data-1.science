---
layout: post
title: "Sequencing of barley genome achieves new milestone: Researchers improves a resource used worldwide"
date: 2016-06-14
categories:
author: "University of California - Riverside"
tags: [Plant breeding,Gene,Genome,Genetic recombination,Barley,DNA sequencing,Genetics,DNA,Biotechnology,Biology]
---


The researchers have sequenced large portions of the genome that together contain nearly two-thirds of all barley genes. When a favorable form of a gene (allele) lies within a gene-dense, low recombination region it requires much more work to bring that favorable allele into an existing variety without also dragging in neighboring genes that may exist in undesirable forms. Key innovations on the computational side are:  the amount of genome sequences the researchers released is about four times the size of the entire rice genome  the work took advantage of two very substantial algorithm innovations that Close and Lonardi published with their students in the past year: one a new computational invention to make better use of today's high-volume sequence data; the other a very efficient method of classifying sequences into specific groups  the new information has been integrated into a national wheat/barley project (TriticeaeCAP) and has been shared with the International Barley Sequencing Consortium  the new information enabled the researchers to clarify aspects of the barley genome that are important in the context of genome evolution and for practical use of genome knowledge by plant breeders and basic researchers--namely, the locations of gene-rich regions including some that have low recombination  When breeders make crosses to develop new varieties, they are seeking new combinations of alleles that are better suited to the agricultural environment or market for the crop product, Close explained. Now we have completed the last genome resource task that we took on, and there are still a few years left to look again at dehydrin genes of barley. There are gene-rich regions that are in low recombination regions, which is critically important for plant breeding, Close said.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150825141313.htm){:target="_blank" rel="noopener"}


