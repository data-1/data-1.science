---
layout: post
title: "Using neural networks to predict outcomes of organic chemistry"
date: 2018-07-14
categories:
author: ""
tags: [Organic chemistry,Chemistry,Simplified molecular-input line-entry system,Artificial intelligence,Science,Branches of science,Technology,Computing,Cognitive science,Information science]
---


To address this we asked ourselves, can we use deep learning and artificial intelligence to predict reactions of organic compounds? Using SMILES, this molecule is translated into BrCCOC1OCCCC1. Credit: IBM  The secret behind our tool is what is called a simplified molecular-input line-entry system or SMILES. SMILES represents a molecule as a sequence of character. We trained our model using an openly available chemical reaction dataset, which correspond to 1 million patent reactions.

<hr>

[Visit Link](https://phys.org/news/2017-12-neural-networks-outcomes-chemistry.html){:target="_blank" rel="noopener"}


