---
layout: post
title: "New analysis links tree height to climate"
date: 2015-10-29
categories:
author: David Tenenbaum, University Of Wisconsin-Madison
tags: [Eucalyptus regnans,Tree,Photosynthesis,Rain,Eucalyptus,Natural environment,Nature,Earth sciences,Botany]
---


Our study talks about the kind of constraints that could limit maximum tree height, and how those constraints and maximum height vary with climate. In Tasmania, an especially rainy part of southern Australia, the tallest living E. regnans is 330 feet tall. Infrastructure—things like wood and roots that are essential to growth but do not contribute to the production of energy through photosynthesis—affect resource allocation, and can explain the importance of the ratio of moisture supply to evaporative demand. The constraints on tree height imposed by resource allocation and hydraulics should both increase in drier areas. The isotopic composition should also remain stable if resource allocation alone sets maximum height, because resource allocation does not directly affect the stomata.

<hr>

[Visit Link](http://phys.org/news327249280.html){:target="_blank" rel="noopener"}


