---
layout: post
title: "The quantum dance of oxygen"
date: 2014-08-17
categories:
author: International School of Advanced Studies (SISSA) 
tags: [Magnetism,Spin (physics),Phase (matter),Oxygen,Electron,Molecule,Superconductivity,Erio Tosatti,Theoretical physics,Quantum mechanics,Physical chemistry,Chemistry,Applied and interdisciplinary physics,Nature,Electromagnetism,Phases of matter,Physics,Materials,Science,Condensed matter physics,Physical sciences]
---


Like oxygen, for example: while exhibiting magnetic properties at intermediate pressures, oxygen molecules lose their magnetism at pressures above 80,000 atmospheres. The first non-magnetic phase, called epsilon, has been studied for years. The four scientists observed, in fact, that the four oxygen molecules in each group constantly exchanged magnetic moments. In the epsilon 1 phase of oxygen, the molecules retain their spins, but these fluctuate coherently within and across quartets like a chorus of cicadas, explains Tosatti. This study therefore divided the epsilon phase into two, epsilon 1 (from 80,000 to 200,000 atmospheres), with fluctuating magnetic properties, and epsilon 0 (from 200,000 to 1,000,000 atmospheres), without magnetic properties.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/isoa-tqd070414.php){:target="_blank" rel="noopener"}


