---
layout: post
title: "Molecular clock could greatly improve smartphone navigation"
date: 2018-07-14
categories:
author: "Rob Matheson"
tags: [Atomic clock,Clock,Global Positioning System,Integrated circuit,Molecule,Hertz,Frequency,Atom,Science,Electricity,Electronics,Electrical engineering,Technology]
---


MIT researchers have developed the first molecular clock on a chip, which uses the constant, measurable rotation of molecules — when exposed to a certain frequency of electromagnetic radiation — to keep time. Your smartphone, therefore, has a much less accurate internal clock that relies on three satellite signals to navigate and can still calculate wrong locations. “The output of the system is linked to that known number — about 231 gigahertz,” Han says. The chip could also be used on the battlefield, Han says. “Soldiers themselves then don’t have GPS signals anymore,” Han says.

<hr>

[Visit Link](http://news.mit.edu/2018/molecular-clocks-improve-smartphone-navigation-performance-0713){:target="_blank" rel="noopener"}


