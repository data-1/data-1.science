---
layout: post
title: "Solved: One of the mysteries of globular clusters"
date: 2017-10-05
categories:
author: "Instituto de Astrofísica de Canarias (IAC)"
tags: [Star,Star cluster,Globular cluster,Asymptotic giant branch,Stellar population,Stellar evolution,Astronomy,Nature,Outer space,Physics,Sky,Stars,Space science,Physical sciences,Stellar astronomy,Astronomical objects,Astrophysics]
---


Until now, explains Aníbal García-Hernández, researcher at the IAC and the second author of the article, various different types of stars had been prepared as candidates: supermassive stars, rapidly rotating massive stars, massive interacting binaries, and massive AGB stars. The role of the AGB stars  Historically, globular clusters have been used as laboratories for studying stellar evolution, because it was thought that all the stars in a globular cluster formed at the same time and thus have the same age. In the first generation the chemical abundances, for example those of elements such as aluminium and magnesium, show the composition of the original interstellar (or intra-cluster) medium. Researchers think that some of the most massive stars in the first generation produce and destroy the heavy elements in their interiors (nucleosynthesis) and by rapid mass loss contaminate the interstellar medium where the second generation of stars then forms with different chemical abundances. These variations in the anticorrelations are exactly what is observed in the globular clusters, and agrees very well with the theoretical predictions for massive AGB stars, which produce these elements in their interiors, and then eject them during a phase of extremely rapid mass loss.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-11/idad-soo110816.php){:target="_blank" rel="noopener"}


