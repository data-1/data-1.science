---
layout: post
title: "Biodiversity belowground is just as important as aboveground"
date: 2015-09-02
categories:
author: University of Copenhagen - Faculty of Science 
tags: [Soil,Ecosystem,Biodiversity,Climate,Climate change,Soil biodiversity,Physical geography,Organisms,Earth phenomena,Environmental conservation,Environmental science,Biogeochemistry,Systems ecology,Earth sciences,Natural environment,Nature,Ecology]
---


That is the conclusions of a new study published in Nature Communications led by Peking University and the Center for Macroecology, Evolution and Climate at the University of Copenhagen. The study is unique in relating soil biodiversity to a whole suite of ecosystem functions rather than focusing on a few. Therefore, we need to be concerned with the multiple functions of ecosystems, what controls them and how this might change with climate change, says Dr. Xin Jing from Peking University. That is important because scientific studies often focus on temperature - not precipitation - when predicting how ecosystems will respond to future changes such as climate change, says Associate Professor Aimée Classen. The links between ecosystem multifunctionality and above- and belowground biodiversity are mediated by climate.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/fos--bbi090115.php){:target="_blank" rel="noopener"}


