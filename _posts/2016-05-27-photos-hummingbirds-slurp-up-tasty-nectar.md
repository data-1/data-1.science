---
layout: post
title: "Photos: Hummingbirds Slurp Up Tasty Nectar"
date: 2016-05-27
categories:
author: Laura Geggel
tags: [Hummingbird,Black-throated mango,Tongue,Science]
---


Hummingbirds zip from flower to flower, licking up nectar with their tongues. The exact way these tongues work has long eluded scientists, but a new study shows that hummingbird tongues are elastic micropumps that rapidly pull in nectar once they reach the inside of the flower. Hummingbirds feed on long, tubular flowers that make nectar hard to reach. (Image credit: Kristiina Hurme)  Split second  A high-speed image of a black-chinned hummingbird (Archilochus alexandri) photographed near Las Vegas. (Image credit: Don Carroll)  Follow Laura Geggel on Twitter @LauraGeggel.

<hr>

[Visit Link](http://www.livescience.com/51907-photos-hummingbird-tongues.html){:target="_blank" rel="noopener"}


