---
layout: post
title: "NASA's multi-purpose NICER/SEXTANT mission on track for 2016 launch"
date: 2016-05-02
categories:
author: Lori Keesey
tags: [Neutron Star Interior Composition Explorer,International Space Station,NASA,Pulsar,Spaceflight,Outer space,Astronomy,Science,Space science]
---


Technicians assemble a new 25-foot test facility, equipped with a one-meter parabolic optical mirror, which will be used to align NICER/SEXTANT’s 56 optics and detectors. We're on schedule to deliver the instrument for integration aboard the SpaceX Dragon cargo spacecraft on a Falcon 9 rocket this time next year, said Keith Gendreau, the principal investigator of the Neutron-star Interior Composition Explorer/Station Explorer for X-ray Timing and Navigation Technology (NICER/SEXTANT). One-of-a-kind multi-purpose investigation  NICER/SEXTANT, which NASA's Science Mission Directorate selected in 2013 as its next Explorer Mission of Opportunity, is a one-of-a-kind investigation that not only will gather important scientific data, but also demonstrate advanced navigation technologies—all from a relatively low-cost instrument that takes advantage of an already-existing platform, the International Space Station (ISS). Two-in-one mission  To demonstrate XNAV, the payload will detect X-ray photons within the pulsars' sweeping beams of light to estimate the arrival times of the pulses. As the craft approached the space station, MXS would transmit data via the modulated X-rays, which the NICER/SEXTANT hardware would then receive.

<hr>

[Visit Link](http://phys.org/news350728723.html){:target="_blank" rel="noopener"}


