---
layout: post
title: "Ocean circulation explains why the Arctic affected by global warming more than the Antarctic"
date: 2016-03-16
categories:
author: Genevieve Wanucha, Massachusetts Institute Of Technology
tags: [Climate change,Ozone depletion,Ocean,Antarctica,Sea ice,Greenhouse gas,Climate,Arctic,Sea,Environmental science,Societal collapse,Human impact on the environment,Global natural environment,Earth sciences,Physical geography,Applied and interdisciplinary physics,Natural environment,Nature,Climate variability and change,Environmental impact,Earth phenomena,Oceanography,Atmosphere,Global environmental issues]
---


Marshall's group also showed that the ocean's response to the ozone hole can help explain the lack of warming to date around Antarctica. They found that this intensification of winds initially cools the sea surface and expands sea ice, but then a slow process of warming and sea ice shrinkage takes over. However, by 2050, ozone hole-effects may instead add to the warming around Antarctica, an effect that will diminish as the ozone hole heals. This MIT model joins several other recent demonstrations of the concerning, but uncertain, future effects of climate change on Antarctic sea ice and glaciers and, in turn, ecosystems and sea-level rise. Explore further How wind helps Antarctic sea ice grow, even as the Arctic melts

<hr>

[Visit Link](http://phys.org/news328165796.html){:target="_blank" rel="noopener"}


