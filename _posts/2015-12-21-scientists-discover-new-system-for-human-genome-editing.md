---
layout: post
title: "Scientists discover new system for human genome editing"
date: 2015-12-21
categories:
author: Broad Institute of MIT and Harvard
tags: [Cas12a,Cas9,Broad Institute,CRISPR,Genome editing,Genome,CRISPR gene editing,DNA,DNA sequencing,Cell (biology),Branches of genetics,Genetics,Biotechnology,Biology,Life sciences,Molecular biology,Biochemistry,Molecular genetics,Biological engineering,Genomics,Bioinformatics]
---


A team including the scientist who first harnessed the revolutionary CRISPR-Cas9 system for mammalian genome editing has now identified a different CRISPR system with the potential for even simpler and more precise genome engineering. In a study published today in Cell, Feng Zhang and his colleagues at the Broad Institute of MIT and Harvard and the McGovern Institute for Brain Research at MIT, with co-authors Eugene Koonin at the National Institutes of Health, Aviv Regev of the Broad Institute and the MIT Department of Biology, and John van der Oost at Wageningen University, describe the unexpected biological features of this new system and demonstrate that it can be engineered to edit the genomes of human cells. Second, and perhaps most significantly: Cpf1 cuts DNA in a different manner than Cas9. The unexpected properties of Cpf1 and more precise editing open the door to all sorts of applications, including in cancer research, said Levi Garraway, an institute member of the Broad Institute, and the inaugural director of the Joint Center for Cancer Precision Medicine at the Dana-Farber Cancer Institute, Brigham and Women's Hospital, and the Broad Institute. Zhang, Broad Institute, and MIT plan to share the Cpf1 system widely.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/biom-sdn092515.php){:target="_blank" rel="noopener"}


