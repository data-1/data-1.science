---
layout: post
title: "Corals are becoming more tolerant of rising ocean temperatures: Scientists replicate landmark study to determine changes in coral sea temperature tolerance over time"
date: 2018-08-14
categories:
author: "PeerJ"
tags: [Coral reef,Coral bleaching,Ecology,Environmental social science,Applied and interdisciplinary physics,Biogeography,Biogeochemistry,Aquatic ecology,Nature,Oceanography,Environmental science,Systems ecology,Physical geography,Earth sciences,Natural environment]
---


To better understand coral acclimatization and adaptation, most studies compare corals from different reef locations, whereas this is the first study to compare the same coral species from the same location over time. Results show a substantial increase in temperature tolerance within the tested corals. In the three species of Hawaiian corals retested, bleaching occurred later, with higher survivorship and growth rates than corals in 1970. Such dramatic differences in coral bleaching temperature thresholds indicate a capacity for adjustment in temperature tolerance, either by changes in physiological process or shifts in symbiotic zooxanthellae types (acclimatization), or natural selection for the survival of more temperature tolerant corals (adaptation). Although these results are encouraging in their indication that acclimatization/adaptation of corals and their symbionts can occur at an unexpectedly rapid rate, increased bleaching tolerance may not be enough for widespread coral survival Dr. Ku'ulei Rodgers said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180807095151.htm){:target="_blank" rel="noopener"}


