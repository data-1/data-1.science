---
layout: post
title: "How hot is Schrödinger's coffee?"
date: 2018-08-15
categories:
author: "University of Exeter"
tags: [Temperature,Thermometer,Quantum mechanics,Uncertainty principle,Temperature measurement,Physics,Nanotechnology,Uncertainty,Physical chemistry,Chemistry,Metrology,Scientific theories,Applied and interdisciplinary physics,Science,Physical sciences,Theoretical physics]
---


To obtain precise measurements one needs to use tiny nanoscale thermometers made up of just a few atoms. It turns out that under certain circumstances the uncertainty in temperature readings are prone to additional fluctuations, which arise because of quantum effects. This research is published in the leading scientific journal Nature Communications. The discovery establishes a new connection between quantum uncertainty, arising from such superposition states, and the accuracy of temperature measurements. ###  Energy-temperature uncertainty relation in quantum thermodynamics is published in Nature Communications.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/uoe-hhi081418.php){:target="_blank" rel="noopener"}


