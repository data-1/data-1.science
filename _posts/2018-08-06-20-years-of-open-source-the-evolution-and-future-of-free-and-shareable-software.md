---
layout: post
title: "20 Years of Open Source: The Evolution and Future of Free and Shareable Software"
date: 2018-08-06
categories:
author: "Jul."
tags: []
---


As we take a look back on the history of free and shareable software, we see that its evolution over the past two decades has produced many groundbreaking applications, paving the way for a free and open future. In 1983, he introduced the GNU project, the first free operating system, and in 1985, he followed with the creation of the Free Software Foundation to further support the free software community. In the late 1990s, mainstream recognition of Linux and the release of the Netscape browser source code increased interest and participation in openly sharing software. The “open source” label was created at a strategy session held on February 3rd, 1998 in Palo Alto, California, shortly after the Netscape source code was released. With proprietary geospatial software, subscriptions determine not only how many data sources can be considered, but also how much it will cost to determine optimal routing.

<hr>

[Visit Link](https://dzone.com/articles/20-years-of-open-source-the-evolution-and-future-o?utm_medium=feed&utm_source=feedpress.me&utm_campaign=Feed%3A+dzone){:target="_blank" rel="noopener"}


