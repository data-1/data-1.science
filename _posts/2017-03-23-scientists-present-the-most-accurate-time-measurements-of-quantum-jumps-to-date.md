---
layout: post
title: "Scientists present the most accurate time measurements of quantum jumps to date"
date: 2017-03-23
categories:
author: "Vienna University Of Technology"
tags: [Electron,Atom,Ionization,Laser,Quantum mechanics,Max Planck Institute of Quantum Optics,Particle physics,Energy,Physics,Time,Atomic physics,Physical sciences,Science,Theoretical physics,Atomic molecular and optical physics,Applied and interdisciplinary physics,Physical chemistry,Chemistry]
---


One of the electron is ripped out of the atom, the other electron may change its quantum state. Credit: TU Wien  When a quantum system changes its state, this is called a quantum jump. Usually, these quantum jumps are considered to be instantaneous. The Most Accurate Time Measurement of Quantum Jumps  A neutral helium atom has two electrons. It remains in the atom, but it is lifted up to a state of higher energy, says Stefan Nagele (TU Wien).

<hr>

[Visit Link](http://phys.org/news/2016-11-scientists-accurate-quantum-date.html){:target="_blank" rel="noopener"}


