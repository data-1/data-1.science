---
layout: post
title: "Memory capacity of brain is 10 times more than previously thought"
date: 2016-01-29
categories:
author: Salk Institute
tags: [Neuron,Brain,Synapse,Hippocampus,Memory,Dendrite,Cognitive science,Neuroscience]
---


We discovered the key to unlocking the design principle for how hippocampal neurons function with low energy but high computation power. We were amazed to find that the difference in the sizes of the pairs of synapses were very small, on average, only about eight percent different in size. Because the memory capacity of neurons is dependent upon synapse size, this eight percent difference turned out to be a key number the team could then plug into their algorithmic models of the brain to measure how much information could potentially be stored in synaptic connections. When a signal travels from one neuron to another, it typically activates that second neuron only 10 to 20 percent of the time. About the Salk Institute for Biological Studies:  The Salk Institute for Biological Studies is one of the world's preeminent basic research institutions, where internationally renowned faculty probes fundamental life science questions in a unique, collaborative and creative environment.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/si-mco012016.php){:target="_blank" rel="noopener"}


