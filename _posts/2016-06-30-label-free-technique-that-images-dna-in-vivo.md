---
layout: post
title: "Label-free technique that images DNA in vivo"
date: 2016-06-30
categories:
author: "Heather Zeiger"
tags: [Cell cycle,Mitosis,Histology,Raman spectroscopy,Skin cancer,Staining,Medical imaging,Cell biology,Biology]
---


Credit: PNAS, Fa-Ke Lu, doi: 10.1073/pnas.1515121112  (Phys.org)—A group of researchers from Harvard University report being able to observe DNA dynamics during cell division in vivo using time-lapse stimulated Raman scattering microscopy and without using fluorescent labels. By looking at these C-H stretching vibration regions and conducting a linear decomposition of the images, Lu, et al. were able to map the content and distribution of DNA, proteins, and lipids within the cell, allowing them to observe the cell division process in a label-free manner. In vivo label-free SRS imaging cell division dynamics in cancer. Lu, et al. used their SRS technique to look at cell division in HeLa cells. Explore further Imaging glucose uptake activity inside single cells  More information: Label-free DNA imaging in vivo with stimulated Raman scattering microscopy PNAS, Fa-Ke Lu, Label-free DNA imaging in vivo with stimulated Raman scattering microscopy, Fa-Ke Lu, DOI: 10.1073/pnas.1515121112  Abstract  Label-free DNA imaging is highly desirable in biology and medicine to perform live imaging without affecting cell function and to obtain instant histological tissue examination during surgical procedures.

<hr>

[Visit Link](http://phys.org/news/2015-09-label-free-technique-images-dna-vivo.html){:target="_blank" rel="noopener"}


