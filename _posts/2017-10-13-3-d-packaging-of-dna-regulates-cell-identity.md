---
layout: post
title: "3-D packaging of DNA regulates cell identity"
date: 2017-10-13
categories:
author: "Perelman School Of Medicine At The University Of Pennsylvania"
tags: [Cellular differentiation,Cell (biology),Gene,Muscle cell,Cancer,Developmental biology,Nuclear lamina,Cell nucleus,Skeletal muscle,DNA,Stem cell,Genome,Genetics,Cell biology,Biology,Life sciences,Biotechnology]
---


Genomic regions containing cardiac genes are released from the nuclear periphery upon differentiation of stem cells into cardiac myocytes. A new study from the Perelman School of Medicine at the University of Pennsylvania suggests that the ability of a stem cell to differentiate into cardiac muscle (and by extension other cell types) depends on what portions of the genome are available for activation, which is controlled by the location of DNA in a cell's nucleus. The Cell study suggests that the specific regions of silenced DNA at the periphery help define a cell's identity. In addition, in many diseases, including cancer, cells express genes that they normally would not, which changes their identity. Jain, Epstein, and others are working to determine if changes in genome domains at the nuclear periphery, or the molecular tethers that keep them there, are responsible for cancer susceptibility.

<hr>

[Visit Link](https://phys.org/news/2017-10-d-packaging-dna-cell-identity.html){:target="_blank" rel="noopener"}


