---
layout: post
title: "Getting more out of microbes"
date: 2018-08-14
categories:
author: "NASA/Johnson Space Center"
tags: [Shewanella,Micro-g environment,NASA,Shewanella oneidensis,Nature]
---


This study advances research for fundamental science and biotechnology applications by testing the performance of an unusual bacterial microorganism known as Shewanella oneidensis MR-1 (Shewanella) in microgravity conditions. Shewanella uses metals in low or no oxygen environments to create energy for itself - a trait that could come in handy for space travel. One way to do this is to use waste from one system to power others. To this end, Micro-12 will examine the organism's use of biofilms, extracellular electron transport, and overall fitness and performance in microgravity. Micro-12 stands to pave the way for many future microgravity studies of similar organisms.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/nsc-gmo081318.php){:target="_blank" rel="noopener"}


