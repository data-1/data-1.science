---
layout: post
title: "Brain of world's first known predators discovered"
date: 2015-10-03
categories:
author: University Of Arizona
tags: [Radiodonta,Onychophora,Arthropod,Fossil,Brain,Predation,Animal,Paleontology,Animals]
---


Credit: Peiyun Cong and co-authors  An international team of paleontologists has identified the exquisitely preserved brain in the fossil of one of the world's first known predators that lived in the Lower Cambrian, about 520 million years ago. Credit: Nicholas Strausfeld/University of Arizona  And – surprise, surprise – that is what we also found in our fossil, Strausfeld said, pointing out that anomalocaridids had a pair of clawlike grasping appendages in front of the eyes. The similarities of their brains and other attributes suggest that the anomalocaridid predators could have been very distant relatives of today's velvet worms, Strausfeld said. Long nerves from the frontal appendages extend to paired ganglia lying in front of the optic nerve and connect to the main brain mass in front of the mouth. Credit: Illustration by Nicholas Strausfeld  With this paper and our previous reports in Nature, we have identified the three principal brain arrangements that define the three groups of arthropods that exist today, Strausfeld said.

<hr>

[Visit Link](http://phys.org/news324705759.html){:target="_blank" rel="noopener"}


