---
layout: post
title: "AIMing a light across millions of kilometres"
date: 2015-10-20
categories:
author: "$author"  
tags: [Laser communication in space,European Space Agency,Outer space,Space science,Spaceflight,Astronomy,Spacecraft,Astronautics,Solar System,Science,Technology]
---


ESA’s proposed Asteroid Impact Mission is intended to do just that: demonstrate laser communications across an unprecedented void. Among its innovative technologies, laser communications would return results to scientists several times faster than standard radio signals. “Optical communications in general is not yet a well-established technology for space and ESA’s European Data Relay System (EDRS) will be the first commercial application,” explains ESA optics engineer Zoran Sodnik. “But AIM will need to operate much further: we are benchmarking a maximum span of 75 million kilometres, or half the distance between Earth and the Sun. AIM laser “While radio communications is a very mature technology and close to optimum efficiency, there’s still lots of room for development with optical communications.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/Asteroid_Impact_Mission/AIMing_a_light_across_millions_of_kilometres){:target="_blank" rel="noopener"}


