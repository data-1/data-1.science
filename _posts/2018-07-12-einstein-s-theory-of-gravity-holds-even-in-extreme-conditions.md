---
layout: post
title: "Einstein's theory of gravity holds – even in extreme conditions"
date: 2018-07-12
categories:
author: "Laura Otto, University Of Wisconsin - Milwaukee"
tags: [Neutron star,Pulsar,Star,White dwarf,Orbit,General relativity,Nature,Astronomical objects,Science,Physics,Physical sciences,Space science,Astronomy]
---


The pulsar and the inner white dwarf fall in the gravitational pull of the outer white dwarf (in red). In most theories of gravity, the very strong gravity of the pulsar means it will fall with a different acceleration than the inner white dwarf. Triple star system  Their test subject is a triple star system called PSR J0337+1715, consisting of a neutron star in a 1.6-day orbit with a white dwarf. We still don't understand how stars move. Advancements in radio telescopes offer more chances at finding the perfect triple system to test, said Jason Hessels, associate professor at ASTRON and the University of Amsterdam.

<hr>

[Visit Link](https://phys.org/news/2018-06-einstein-theory-gravity-extreme-conditions.html){:target="_blank" rel="noopener"}


