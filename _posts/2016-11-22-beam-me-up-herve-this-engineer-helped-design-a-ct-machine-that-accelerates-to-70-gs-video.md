---
layout: post
title: "Beam Me Up, Herve: This Engineer Helped Design A CT Machine That Accelerates To 70 Gs [Video]"
date: 2016-11-22
categories:
author: ""
tags: [General Electric,CT scan,Technology,Gantry (medical),Computer network,Medical imaging,3D printing,Innovation,Electrical grid,Culture,Energy development,Engine,Analytics,Sustainable energy,Governance,Energy,X-ray tube,Manufacturing,G-force]
---


[embed width=800]https://www.youtube.com/watch?v=JWF805_iX34[/embed]Why does the X-ray tube move so fast?You have to complete one full rotation on the gantry around the body in one heartbeat in order to get a clear and detailed image of the heart. CT images the body one slice after another. [embed width=800]https://www.youtube.com/watch?v=Wvvo6T3NubE[/embed]What else can it do?The design allows us to change the energy of the imaging spectrum during a scan. By changing the energy spectrum — the color of your glasses, if you will — you are able to add another dimension to your pictures that describes the material content of the body. [embed width=800]https://www.youtube.com/watch?v=DwFez7TtCiI[/embed]What are you working on right now?The Revolution CT has already allowed us to increase the quality of the images and reduce the amount of radiation at the same time.

<hr>

[Visit Link](http://www.gereports.com/picture-worth-70gs-space-physics-medical-imaging-collide-views-can-world/){:target="_blank" rel="noopener"}


