---
layout: post
title: "Born an addict: Clues from umbilical cords"
date: 2016-05-14
categories:
author: AACC
tags: [Neonatal withdrawal,Infant,Health,Health care,Causes of death,Medicine,Diseases and disorders,Health sciences,Clinical medicine,Medical specialties]
---


Because umbilical cord tissue can be sent for testing immediately after birth, this specimen type offers logistical advantages over meconium, the traditional specimen for detecting drug-exposed newborns. As a result, 29.5 percent of babies born to these mothers tested positive for illicit drugs at birth -- approximately 109 babies per year. In Utah, the majority of cord specimens come from the Intermountain Medical Center while the University of Utah hospital still primarily sends ARUP meconium specimens. Whether a baby is addicted to stimulants or downers will result in different withdrawal symptoms and require different treatment. About ARUP Laboratories Founded in 1984, ARUP Laboratories is a leading national reference laboratory and a nonprofit enterprise of the University of Utah and its Department of Pathology.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150716124746.htm){:target="_blank" rel="noopener"}


