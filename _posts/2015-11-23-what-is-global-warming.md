---
layout: post
title: "What is global warming?"
date: 2015-11-23
categories:
author: "$author"  
tags: [Greenhouse gas,Greenhouse effect,Carbon dioxide,Atmosphere of Earth,Global warming potential,Climate change,Atmosphere,Chemistry,Change,Climate forcing,Applied and interdisciplinary physics,Physical geography,Environmental issues,Human impact on the environment,Earth sciences,Climate variability and change,Natural environment,Environmental impact,Global environmental issues,Environmental issues with fossil fuels,Climate,Societal collapse,Nature]
---


Human activities such as burning coal and oil inject additional CO2 into the atmosphere, which acts as an extra blanket to trap solar radiation, worsening the greenhouse effect  Soon, the world will gather in Paris to forge a global pact to reduce greenhouse gas emissions blamed for dangerous levels of global warming. Pollution sources  Humanity's annual output of greenhouse gases is higher than ever, totalling the equivalent of just under 53 billion tonnes of CO2 in 2014, according to the UN. It jumped 2.2 percent per year during the 2000s, compared to 1.3 percent per year from 1970-2000. Warmer planet  Earth's average temperature has already climbed about 1.0 C from 1880 to 2015—halfway to the 2 C target. To stay under the 2 C ceiling, greenhouse gas emissions must be cut by 40-70 percent over 2010 levels by 2050, and eliminated entirely by century's end.

<hr>

[Visit Link](http://phys.org/news/2015-11-global_1.html){:target="_blank" rel="noopener"}


