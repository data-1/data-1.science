---
layout: post
title: "OSIRIS-REx spacecraft completes assembly stage, begins environmental testing"
date: 2015-10-26
categories:
author: University of Arizona 
tags: [OSIRIS-REx,NASA,Goddard Space Flight Center,Aerospace,Spaceflight,Outer space,Discovery and exploration of the Solar System,Spaceflight technology,Astronautics,NASA programs,Space missions,Science,Space vehicles,Astronomy,Space science,Spacecraft,Space program of the United States,Space exploration,Space programs,Flight]
---


OSIRIS-REx, led by the University of Arizona, will be the first U.S. mission to return samples from an asteroid to Earth for further study. The simulation concludes with a test in which the spacecraft and its instruments are placed in a vacuum chamber and cycled through the extreme hot and cold temperatures it will face during its journey to Bennu. OSIRIS-REx is scheduled to ship from Lockheed Martin's facility to NASA's Kennedy Space Center next May, where it will undergo final preparations for launch. ###  NASA's Goddard Space Flight Center in Greenbelt, Maryland, provides overall mission management, systems engineering and safety and mission assurance for OSIRIS-REx. Dante Lauretta is the mission's principal investigator at the University of Arizona.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/uoa-osc102215.php){:target="_blank" rel="noopener"}


