---
layout: post
title: "Open Source AI For Everyone: Three Projects to Know"
date: 2018-05-26
categories:
author: "Sam Dean"
tags: [Artificial intelligence,Linux Foundation,Machine learning,Open source,Apache Spark,TensorFlow,Linux,Software,Computer science,Technology,Computing,Branches of science,Software engineering,Software development,Information Age,Information technology]
---


At the intersection of open source and artificial intelligence, innovation is flourishing, and companies ranging from Google to Facebook to IBM are open sourcing AI and machine learning tools. In essence, Sparkling Water is an API that allows Spark users to leverage H2O’s open source machine learning platform instead of — or alongside — the algorithms that are included in Spark’s existing machine-learning library. With Driverless AI, non-technical users can gain insights from data, optimize algorithms, and apply machine learning to business processes. Acumos  Acumos is another open source project aimed at simplifying access to AI. Acumos AI, which is part of the LF Deep Learning Foundation, is a platform and open source framework that makes it easy to build, share, and deploy AI apps.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/open-source-ai-for-everyone-three-projects-to-know/){:target="_blank" rel="noopener"}


