---
layout: post
title: "Near instantaneous evolution discovered in bacteria"
date: 2017-09-12
categories:
author: "Grove Potter, University At Buffalo"
tags: [Mutation,Evolution,Bacteria,Biology,Life sciences,Nature,Genetics,Biological evolution,Biotechnology]
---


We had the DNA of the bacteria sequenced on campus, and we discovered they had mutated and were using the new compound to take iron in to grow, he said. So how big is the discovery of near instantaneous evolution? O'Brian said other researchers may take up work on how the new knowledge could impact human health. The mutation that O'Brian observed resulted in a gain of function, a much more complicated event than the adaptation to block an antibiotic, he said. Organisms can adapt by switching genes on and off.

<hr>

[Visit Link](https://phys.org/news/2017-06-instantaneous-evolution-bacteria.html){:target="_blank" rel="noopener"}


