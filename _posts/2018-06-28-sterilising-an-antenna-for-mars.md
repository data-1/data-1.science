---
layout: post
title: "Sterilising an antenna for Mars"
date: 2018-06-28
categories:
author: ""
tags: [Outer space,Space science,Astronomy,Planetary science,Solar System,Spaceflight,Physical sciences,Astronautics,Planets of the Solar System,Astronomical objects known since antiquity]
---


A ground penetrating radar antenna for ESA’s ExoMars 2020 rover being pre-cleaned in an ultra-cleanroom environment in preparation for its sterilisation process, in an effort to prevent terrestrial microbes coming along for the ride to the red planet. m ‘ISO Class 1’ cleanroom is one of the cleanest places in Europe. The item seen here is the WISDOM (Water Ice Subsurface Deposit Observation on Mars) radar antenna flight model, designed to sound the subsurface of Mars for water ice. “After pre-cleaning and then the taking of sample swabs, the antenna was placed into our dry heat steriliser, to target the required 99.9% bioburden reduction to meet ExoMars 2020’s cleanliness requirements,” explains technician Alan Dowson. By international planetary protection agreement, space agencies are legally required to prevent terrestrial microbes hitchhiking to other planets and moons in our Solar System where past or present alien life is a possibility.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2018/06/Sterilising_an_antenna_for_Mars){:target="_blank" rel="noopener"}


