---
layout: post
title: "More Companies Join RE100, Commit To 100% Renewable Energy"
date: 2015-12-18
categories:
author: Joshua S Hill, Remeredzai Joseph Kuhudzai, Steve Hanley, José Pontes, Zachary Shahan, Written By
tags: [The Climate Group,Economy and the environment,Energy economics,Energy and the environment,Climate change mitigation,Sustainable energy,Natural environment,Physical quantities,Economy,Climate change,Nature,Energy]
---


BMW Group, Coca-Cola, and others join Microsoft, Adobe, and Google in committing to source 100% of their electricity from renewable energy as part of the RE100 campaign. The initiative believes that switching the private sector’s electricity consumption to renewables “will accelerate the transformation of the global energy market and aid the transition to a low carbon economy.” RE100 is led by The Climate Group in partnership with CDP, as part of the We Mean Business coalition. The Climate Group and CDP also revealed new figures that highlight the potential impact of the RE100 campaign. “Our analysis of the private sector’s electricity consumption and carbon emissions indicated that a switch to power from renewable sources could cut global CO2 by nearly 15%.”  The news comes at the same time as the Science Based Targets initiative has reported the number of companies it has approved has grown to 114, including Coca-Cola, among others. Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2015/12/09/companies-join-re100-commit-100-renewable-energy/){:target="_blank" rel="noopener"}


