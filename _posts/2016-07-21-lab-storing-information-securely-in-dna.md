---
layout: post
title: "Lab storing information securely in DNA"
date: 2016-07-21
categories:
author: "Sandia National Laboratories"
tags: [News aggregator,Computer data storage,Technology,Computing]
---


Marlene Bachand, a biological engineer at Sandia and George Bachand's spouse, added, We are taking advantage of a biological component, DNA, and using its unique ability to encode huge amounts of data in an extremely small volume to develop DNA constructs that can be used to transmit and store vast amounts of encrypted data for security purposes. advertisement  Encrypting text into DNA and producing the message  To achieve this proof-of-principle, the first step was to develop the software to generate the encryption key and encrypt text into a DNA sequence. Encoding the message into 550 bases was easy; actually making the DNA was hard. Identifying potential national security applications  Since successfully encoding, making, reading and decoding the 180-character message and the 700-character Truman letter, the Bachands are now working on even longer test sequences. However, what the Bachands really want to do is move beyond tests and apply their technique to national security problems.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/07/160711122612.htm){:target="_blank" rel="noopener"}


