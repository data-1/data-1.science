---
layout: post
title: "Vision-restoring gene therapy also strengthens visual processing pathways in brain: Surprisingly rapid regrowth of unused brain connections after decades of near blindness"
date: 2016-05-14
categories:
author: University of Pennsylvania School of Medicine
tags: [Leber congenital amaurosis,Retina,Visual impairment,Medicine,Clinical medicine,Neuroscience,Medical specialties,Vision,Health,Nervous system,Health sciences]
---


Since 2007, clinical trials using gene therapy have resulted in often-dramatic sight restoration for dozens of children and adults who were otherwise doomed to blindness. The patients had received the gene therapy in just one eye (their worse seeing eye), and though we imaged their brains only about two years later, on average, we saw big differences between the side of the brain connected to the treated region of the injected eye and the side connected to the untreated eye, said lead author Manzar Ashtari, PhD, director of CNS Imaging at the Center for Advanced Retinal and Ocular Therapeutics in the Department of Ophthalmology at Penn. The data showed that the retina-brain pathways in the treated eyes in the LCA2 group seemed nearly as robust as the corresponding pathways in the sighted control group, implying that these pathways had largely rebuilt themselves in the LCA2 patients, following their retinal intervention. advertisement  In further tests Ashtari used functional magnetic resonance imaging (fMRI) to confirm the stronger connectivity of the brain's visual fibers for the treated-eye. In connection with that larger trial, Ashtari is now doing a comprehensive imaging study, among other comparisons of the brains of patients before and after treatment.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/07/150715155319.htm){:target="_blank" rel="noopener"}


