---
layout: post
title: "Flowers' secret signal to bees and other amazing nanotechnologies hidden in plants"
date: 2017-10-19
categories:
author: Stuart Thompson, The Conversation
tags: [Flower,Petal,Iridescence,Color,Lotus effect,Chemistry,Materials]
---


But some flowers also use iridescence, a different type of colour produced when light reflects from microscopically spaced structures or surfaces. And they weren't quite perfect in very similar ways in all of the types of flowers that they looked at. These imperfections meant that instead of giving a rainbow as a CD does, the patterns worked much better for blue and ultra-violet light than other colours, creating what the researchers called a blue halo. But in some plants, such as the lotus, this property is enhanced by the shape of the wax coating in a way that effectively makes it self-cleaning. Humans have always used cellulose as a natural polymer, for example in paper or cotton, but scientists are now developing ways to release individual microfibrils to create new technologies.

<hr>

[Visit Link](https://phys.org/news/2017-10-secret-bees-amazing-nanotechnologies-hidden.html){:target="_blank" rel="noopener"}


