---
layout: post
title: "Red Hat and AWS Extend Strategic Alliance to Package Access to AWS Services Within Red Hat OpenShift"
date: 2017-10-23
categories:
author:  
tags: [Cloud computing,Red Hat,Amazon Web Services,Amazon (company),Internet of things,Technology,Computing,Information technology,Information Age]
---


BOSTON - RED HAT SUMMIT 2017 - May 2, 2017 —  Unique offering will allow customers to deploy AWS services from within Red Hat OpenShift Container Platform both on-premises and in the cloud Alliance will also bring closer product alignment to enable new AWS services to be available with Red Hat Enterprise Linux faster than ever  Today, Red Hat, Inc. (NYSE: RHT), the world's leading provider of open source solutions, and Amazon Web Services, Inc. (AWS), an Amazon.com company and the world’s leading cloud computing provider (NASDAQ:AMZN), announced an extended strategic alliance to natively integrate access to AWS services into Red Hat OpenShift Container Platform. Through this unique offering, Red Hat will make AWS services accessible directly within Red Hat OpenShift Container Platform, allowing customers to take advantage of the world’s most comprehensive and broadly adopted cloud whether they’re using Red Hat OpenShift Container Platform on AWS or in an on-premises environment. With this alliance, AWS and Red Hat will give customers the ability to easily build and extend container-based enterprise applications with Red Hat OpenShift Container Platform using a range of AWS compute, database, analytics, machine learning, networking, mobile, and various application services. This will enable Red Hat OpenShift Container Platform customers to be more agile as they'll be able to use the same application development platform to build on premises or in the cloud. In addition to providing an easier way for developers to deploy their applications in containers, Red Hat and AWS are working together to more rapidly enable new AWS services with Red Hat Enterprise Linux.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/red-hat-and-aws-extend-strategic-alliance-package-access-aws-services-within-red-hat-openshift){:target="_blank" rel="noopener"}


