---
layout: post
title: "Strong interaction between herbivores and plants"
date: 2017-09-10
categories:
author: "University Of Cologne"
tags: [Biodiversity,Ecosystem,Herbivore,Biodiversity loss,Earth sciences,Natural environment,Ecology,Systems ecology,Biogeochemistry,Environmental science,Environmental social science]
---


Credit: commonfree  A research project conducted at the University of Cologne's Zoological Institute reveals important findings on the interaction between nutrient availability and the diversity of consumer species in freshwater environments. Hence it is indispensable to understand the mechanisms that impact biodiversity, particularly in the case of primary producers such as algae and plants that form the basis of nearly all natural food webs and ecosystems. In an experimental study on algae from freshwater biofilms, she was able to show that both the availability of nutrients and the diversity of consumer species (herbivores) have a significant positive impact on the diversity of photosynthetic organisms. For example, if nutrient pollution is high - which is often the case in the overfertilization of freshwater caused by humans - the positive effect of the greater diversity of herbivorous consumers on the biodiversity of the algae population is lost. In an overfertilized system, it appears that one of the most important mechanisms sustaining producer diversity no longer functions correctly, which has far-reaching ramifications for our understanding of how to maintain biodiversity - not only in freshwater environments, but in all kinds of ecosystems.

<hr>

[Visit Link](https://phys.org/news/2017-03-strong-interaction-herbivores.html){:target="_blank" rel="noopener"}


