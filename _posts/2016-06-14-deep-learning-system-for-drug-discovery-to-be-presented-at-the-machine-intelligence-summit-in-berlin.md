---
layout: post
title: "Deep learning system for drug discovery to be presented at the Machine Intelligence Summit in Berlin"
date: 2016-06-14
categories:
author: "InSilico Medicine"
tags: [Insilico Medicine,Artificial intelligence,Medication,Pharmaceutical industry,Biomarker,Deep learning,Research,Drug discovery,Pharmacology,Life sciences,Medical research,Biotechnology,Branches of science,Science,Biology,Health sciences,Medicine,Technology]
---


June 14, 2016, Baltimore, MD, Following the publication of the first proof of concept of predicting the functional properties of drugs by their transcriptional response signature, scientists at Insilico Medicine developed a multimodal input drug discovery engine capable of predicting therapeutic use, toxicity and adverse effects of thousands of molecules. One of these scientists is Polina Mamoshina, senior research scientist at the Pharmaceutical Artificial Intelligence division of Insilico Medicine. We are now partnering with various institutions to validate these predictions in vitro and in vivo, said Alex Aliper, president of Insilico Medicine, Inc.  Insilico Medicine previously presented the deep learned biomarkers of aging at the Re-Work Deep Learning in Healthcare conference in London before the publication of the research paper. Through its Pharma.AI division the company provides advanced machine learning services to biotechnology, pharmaceutical and skin care companies. Brief company video: https://www.youtube.com/watch?v=l62jlwgL3v8  About Re-Work  RE* WORK is an all-female run events organising company that brings together breakthrough technology, cutting-edge science and entrepreneurship shaping the future of business and society.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/imi-dls061316.php){:target="_blank" rel="noopener"}


