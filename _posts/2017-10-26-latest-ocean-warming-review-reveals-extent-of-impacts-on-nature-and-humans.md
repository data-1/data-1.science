---
layout: post
title: "Latest ocean warming review reveals extent of impacts on nature and humans"
date: 2017-10-26
categories:
author: International Union for Conservation of Nature
tags: [Climate change,International Union for Conservation of Nature,Effects of climate change on oceans,Ocean,Conservation biology,Physical geography,Nature,Environmental science,Global environmental issues,Environmental issues,Oceanography,Ecology,Environmental social science,Human impact on the environment,Earth phenomena,Earth sciences,Natural environment]
---


Honolulu, Hawai?i, 5 September (IUCN) - Ocean warming is affecting humans in direct ways and the impacts are already being felt, including effects on fish stocks and crop yields, more extreme weather events and increased risk from water-borne diseases, according to what has been called the most comprehensive review available on the issue, launched today by the International Union for Conservation of Nature (IUCN) at the IUCN World Conservation Congress in Hawai'i. The report, Explaining ocean warming: Causes, scale, effects and consequences, reviews the effects of ocean warming on species, ecosystems and on the benefits oceans provide to humans. By damaging fish habitats and causing fish species to move to cooler waters, warming oceans are affecting fish stocks in some areas and are expected to lead to reduced catches in tropical regions, the report states. In South-East Asia, harvests from marine fisheries are expected to fall by between 10% and 30% by 2050 relative to 1970-2000, as the distributions of fish species shift, under a high 'business as usual' greenhouse gas emission scenario, the report states. The report's recommendations include recognising the severity of ocean warming impacts on ocean ecosystems and the benefits they provide to humans, expanding marine protected areas, introducing legal protection for the high seas, better evaluating the social and economic risks associated with warming oceans and continuing to fill gaps in scientific knowledge, as well as cutting greenhouse gas emissions rapidly and substantially.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-09/iufc-low090216.php){:target="_blank" rel="noopener"}


