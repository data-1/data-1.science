---
layout: post
title: "Skull reconstruction sheds new light on tetrapod transition from water to land"
date: 2015-07-16
categories:
author: PLOS 
tags: [Acanthostega,Skull,Tetrapod,Jaw]
---


The results publish March 11, 2015 in the open-access journal PLOS ONE by Laura Porro from University of Bristol, UK, and colleagues. This species is crucial for understanding the anatomy and ecology of the earliest tetrapods; however, after hundreds of millions of years buried in the ground, fossils are often damaged and deformed. Researchers found that the reconstructed skull had a longer postorbital region and a more strongly hooked lower jaw than previously thought. The researchers plan to apply these methods to other flattened fossils of the earliest tetrapods to better understand how modifications to these early animals' bones and teeth helped them meet the challenges of living on land. PLoS ONE 10(3): e0118882.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/p-srs030915.php){:target="_blank" rel="noopener"}


