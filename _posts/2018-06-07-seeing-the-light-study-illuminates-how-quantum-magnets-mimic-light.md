---
layout: post
title: "Seeing the light? Study illuminates how quantum magnets mimic light"
date: 2018-06-07
categories:
author: "Andrew Scott, Okinawa Institute Of Science"
tags: [Light,Subatomic particle,Spin ice,Quantum mechanics,Neutron,Spin (physics),Magnetism,Atom,Wave,Physics,Photon,Matter,Science,Physical sciences,Theoretical physics,Electromagnetism,Applied and interdisciplinary physics,Nature]
---


It was only in the 20thcentury through the work of Einstein, that light was finally understood to be made up of fundamental particles called photons, which act like both particles and waves. Recently, researchers realized that quantum effects at low temperatures can introduce an emergent electric field in spin ice, with an amazing consequence: Emergent electric and magnetic fields combine to produce magnetic excitations that behave exactly like photons of light. Credit: OIST  In 2012 Prof. Shannon and his then Ph.D. student Owen Benton proposed a way to detect the light inside a quantum spin ice by bouncing neutrons off the magnetic atoms inside the crystal. A research team lead by Dr. Romain Sibille from the Paul Scherrer Institut (PSI) in Switzerland, in collaboration with colleagues at the University of Warwick in the UK, managed to generate a perfect crystal of a quantum spin ice material with which they could finally test the hypothesis. Dr. Benton and Prof Shannon's theory bore an uncanny similarity to the experimental energy maps.

<hr>

[Visit Link](https://phys.org/news/2018-06-illuminates-quantum-magnets-mimic.html){:target="_blank" rel="noopener"}


