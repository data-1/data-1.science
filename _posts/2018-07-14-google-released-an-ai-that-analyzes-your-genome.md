---
layout: post
title: "Google Released an AI That Analyzes Your Genome"
date: 2018-07-14
categories:
author: ""
tags: [DNA sequencing,Artificial intelligence,Whole genome sequencing,Genome,Genetics,Technology,Biotechnology,Branches of science,Life sciences,Health sciences]
---


Genome Analysis  In the 15 years since the human genome was first sequenced in a historic scientific achievement, genomic sequencing has become relatively routine, with huge genomes being sequenced at incredible speeds. On December 4, Google released a tool that may help: DeepVariant, which utilizes artificial intelligence (AI) techniques and machine learning to more accurately build a picture of a person's genome from sequencing data. Machine learning is an application of AI that allows systems to improve without external programming or interference. By automatically identifying small insertion and deletion mutations and single base pair mutations, identified by a rapid method of genetic analysis known as high-throughput sequencing, Google's new AI can reportedly create an accurate picture of a full genome with little effort. Google's advanced analysis capability reportedly goes even further beyond what has before been capable.

<hr>

[Visit Link](https://futurism.com/google-released-ai-analyzes-genome/){:target="_blank" rel="noopener"}


