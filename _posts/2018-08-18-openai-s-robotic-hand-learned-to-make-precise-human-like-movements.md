---
layout: post
title: "OpenAI's Robotic Hand Learned to Make Precise, Human-like Movements"
date: 2018-08-18
categories:
author: ""
tags: [Technology,Computing,Artificial intelligence,Emerging technologies,Cyberspace]
---


Copyright ©, Camden Media Inc All Rights Reserved. See our User Agreement Privacy Policy and Data Use Policy . The material on this site may not be reproduced, distributed, transmitted, cached or otherwise used, except with prior written permission of Futurism. Fonts by Typekit and Monotype.

<hr>

[Visit Link](https://futurism.com/videos/openai-robotic-hand/){:target="_blank" rel="noopener"}


