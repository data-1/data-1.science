---
layout: post
title: "Rosetta's big day in the sun"
date: 2016-05-27
categories:
author: European Space Agency
tags: [67PChuryumovGerasimenko,Rosetta (spacecraft),Comet,Comet nucleus,Coma (cometary),Astronomy,Outer space,Solar System,Space science,Bodies of the Solar System,Astronomical objects,Planetary science,Local Interstellar Cloud,Comets]
---


Credit: ESA/Rosetta/MPS for OSIRIS Team MPS/UPD/LAM/IAA/SSO/INTA/UPM/DASP/IDA  ESA's Rosetta today witnessed Comet 67P/Churyumov–Gerasimenko making its closest approach to the sun. One image taken by Rosetta's navigation camera was acquired at 01:04 GMT, just an hour before the moment of perihelion, from a distance of around 327 km. This is a projected map of Comet 67P/Churyumov–Gerasimenko using data from Rosetta’s OSIRIS camera. They have now identified four new geological regions on the southern hemisphere, which includes parts of both comet lobes, bringing the total number of regions to 23. Anhur and Khonsu can be found on the underside of the comet's larger lobe, Wosret on the smaller lobe, and Sobek is located on the comet's neck.

<hr>

[Visit Link](http://phys.org/news/2015-08-rosetta-big-day-sun.html){:target="_blank" rel="noopener"}


