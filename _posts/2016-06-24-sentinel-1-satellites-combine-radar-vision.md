---
layout: post
title: "Sentinel-1 satellites combine radar vision"
date: 2016-06-24
categories:
author: ""
tags: [Sentinel-1A,Satellite,Sentinel-1,Astronomy,Flight,Space vehicles,Astronautics,Earth sciences,Bodies of the Solar System,Satellites,Spacecraft,Spaceflight,Space science,Outer space]
---


Applications Sentinel-1 satellites combine radar vision 22/06/2016 14114 views 133 likes  The twin Sentinel-1 satellites have – for the first time – combined to show their capability for revealing even small deformations in Earth’s surface. Radar scans combined This technique is particularly useful for generating accurate maps of surface deformation over wide areas, such as those caused by tectonic processes, volcanic activities or landslides. It is also an ideal tool for monitoring glacier flow and changes in Arctic and Antarctic ice shelves. Sentinel-1’s first such paired ‘interferogram’ combined a Sentinel-1A scan over southern Romania on 9 June with a Sentinel-1B acquisition over the same area just one day before reaching its target orbit position. The rainbow-coloured patterns are related to topography, and they demonstrate that the two satellites’ identical radars are accurately synchronised, pointing in the same direction and that the satellites are in their correct orbits.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-1/Sentinel-1_satellites_combine_radar_vision){:target="_blank" rel="noopener"}


