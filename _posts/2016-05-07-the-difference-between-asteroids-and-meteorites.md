---
layout: post
title: "The difference between asteroids and meteorites"
date: 2016-05-07
categories:
author: Nancy Atkinson
tags: [Meteoroid,Asteroid,Meteorite,Impact event,Chelyabinsk meteor,Space science,Physical sciences,Nature,Local Interstellar Cloud,Outer space,Astronomical objects,Bodies of the Solar System,Solar System,Astronomy,Planetary science]
---


In simplest terms here are the definitions:  Asteroid: a large rocky body in space, in orbit around the Sun. Meteorite: If a small asteroid or large meteoroid survives its fiery passage through the Earth's atmosphere and lands on Earth's surface, it is then called a meteorite. One of the more famous impact craters on Earth is Meteor Crater in Arizona in the US, which was made by an impact about 50,000 years ago. Credit: NASA  Meteorite  If any part of a meteoroid survives the fall through the atmosphere and lands on Earth, it is called a meteorite. Asteroids are always found in space.

<hr>

[Visit Link](http://phys.org/news352541577.html){:target="_blank" rel="noopener"}


