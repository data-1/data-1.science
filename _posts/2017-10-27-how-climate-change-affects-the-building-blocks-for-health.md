---
layout: post
title: "How climate change affects the building blocks for health"
date: 2017-10-27
categories:
author: Alistair Woodward, The Conversation
tags: [Climate change,Greenhouse gas emissions,Health,Food,Earth sciences,Environment,Natural environment,Human impact on the environment,Physical geography]
---


More intense rainfalls have caused flooding throughout New Zealand, as seen here in Northland. There have been many reports, such as those published by the Intergovernmental Panel on Climate Change and the Lancet Commission on Climate Change, that detail how aspects of human physical and mental are effected by a changing climate. The RSNZ report is organised around eight prerequisites for good health, including community, shelter, water and food—all of which are threatened by climate change. The report refers to the particular threat climate change poses to Māori. Transition risks and opportunities  There is another dimension to health impacts that is not discussed in the RSNZ report.

<hr>

[Visit Link](https://phys.org/news/2017-10-climate-affects-blocks-health.html){:target="_blank" rel="noopener"}


