---
layout: post
title: "Pluto's escaping atmosphere extends 1,000 miles out into space"
date: 2015-07-20
categories:
author: Loren Grush, Jul
tags: [New Horizons,Atmosphere of Pluto,Atmosphere,Pluto,Atmosphere of Earth,Sun,Space science,Astronomy,Planetary science,Outer space,Astronomical objects,Bodies of the Solar System,Solar System,Planets,Planemos,Planets of the Solar System,Physical sciences]
---


Data collected by NASA's New Horizons spacecraft reveals that the gas surrounding the dwarf planet extends as far as 1,000 miles outward into space. That's more than 13 times farther than Earth's atmosphere, which tapers off at around 75 miles above our planet's surface. Previous observations from Earth have only been able to record Pluto's atmosphere reaching 170 miles out into space. It's an event known as solar occultation, and it allows the spectrograph to measure the wavelengths of the Sun's light filtered through Pluto's atmosphere. But she also said it's possible solar wind is colliding with the escaping atmosphere and carrying it outward into space.

<hr>

[Visit Link](http://www.theverge.com/2015/7/17/8994661/plutos-escaping-atmosphere-extends-1000-miles-out-into-space){:target="_blank" rel="noopener"}


