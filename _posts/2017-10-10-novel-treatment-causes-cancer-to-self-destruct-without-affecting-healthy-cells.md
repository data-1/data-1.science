---
layout: post
title: "Novel treatment causes cancer to self-destruct without affecting healthy cells"
date: 2017-10-10
categories:
author: "Albert Einstein College of Medicine"
tags: [Apoptosis,Cancer,Acute myeloid leukemia,Leukemia,Bcl-2-associated X protein,Chemotherapy,Medical specialties,Health sciences,Clinical medicine,Diseases and disorders,Medicine,Causes of death,Health,Biology,Cell biology,Life sciences,Biochemistry,Biotechnology]
---


Our novel compound revives suppressed BAX molecules in cancer cells by binding with high affinity to BAX's activation site, says Dr. Gavathiotis. A compound dubbed BTSA1 (short for BAX Trigger Site Activator 1) proved to be the most potent BAX activator, causing rapid and extensive apoptosis when added to several different human AML cell lines, says lead author Denis Reyna, M.S., a doctoral student in Dr. Gavathiotis' lab. Strikingly, BTSA1 induced apoptosis in the patients' AML cells but did not affect patients' healthy blood-forming stem cells. On average, the BTSA1-treated mice survived significantly longer (55 days) than the control mice (40 days), with 43 percent of BTSA1-treated AML mice alive after 60 days and showing no signs of AML. BTSA1 activates BAX and causes apoptosis in AML cells while sparing healthy cells and tissues -- probably because the cancer cells are primed for apoptosis, says Dr. Gavathiotis.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171009123906.htm){:target="_blank" rel="noopener"}


