---
layout: post
title: "Researchers explore possibilities of growing plants on Mars"
date: 2017-09-23
categories:
author: "Florida Institute of Technology"
tags: [Mars,Martian soil,NASA,Soil,Space science,Outer space]
---


A little more than a year after the Buzz Aldrin Space Institute opened at Florida Tech with the overarching mission to get humans to Mars, this horticultural research will attempt to address one of the most critical issues facing the first Martian settlers: how to grow food on a cold and toxic world. The current Mars regolith simulant isn't perfect. Eventually, it may be possible with the addition of fertilizer and removal of the perchlorates to grow various plants in a Martian soil. With our academic partners working on planetary surface food production, NASA is able to focus on the near-term technologies and systems needed to get our crews to the Red Planet, said Trent Smith, veggie project manager at NASA's Kennedy Space Center. Thinking about ways to live on Mars is the ultimate test of sustainability, says Daniel Batcheldor, professor of physics and space sciences at Florida Tech and project lead for the Buzz Aldrin Space Institute.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-10/fiot-rep100316.php){:target="_blank" rel="noopener"}


