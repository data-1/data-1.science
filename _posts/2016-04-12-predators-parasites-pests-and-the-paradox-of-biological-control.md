---
layout: post
title: "Predators, parasites, pests and the paradox of biological control"
date: 2016-04-12
categories:
author: University of Michigan
tags: [Biological pest control,Pest (organism),Predation,Parasitism,Caterpillar,Ecology,Insects,Natural environment]
---


But what if that caterpillar is infected with larvae from a tiny parasitic wasp--another agent of biological pest control. But in their Nature Communications paper, Ong and Vandermeer show that separately unstable control agents can combine to create an effective pest management program with a stability that is reminiscent of natural systems. An example of a biological control system involving a predator, a parasite and a pest is a backyard vegetable garden where caterpillars feed on plants, tiny parasitic wasps lay eggs inside the caterpillars, and black-capped chickadees eat the caterpillars. The birds and wasps compete for the available caterpillars. When the two control agents are combined in that backyard garden, the bird prevents the wasp from overexploiting the pest resource by eating wasps inside the caterpillars, Ong said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/uom-ppp011415.php){:target="_blank" rel="noopener"}


