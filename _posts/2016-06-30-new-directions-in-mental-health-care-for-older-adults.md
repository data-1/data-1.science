---
layout: post
title: "New directions in mental health care for older adults"
date: 2016-06-30
categories:
author: "Wolters Kluwer Health"
tags: [Cognitive disorder,Mental disorder,Human diseases and disorders,Diseases and disorders,Medical specialties,Cognitive science,Abnormal psychology,Causes of death,Behavioural sciences,Medicine,Health care,Mental health,Health,Health sciences,Mental disorders,Clinical medicine]
---


The aging of the population, shifting diagnostic criteria, and new health care policy initiatives are some of the factors driving changes in mental health treatment for older Americans, according to the September special issue of the Harvard Review of Psychiatry. These include the new diagnosis of hoarding disorder; a newly defined category of neurocognitive disorders that includes major neurocognitive disorder (replacing the term dementia), mild neurocognitive disorder, (replacing mild cognitive impairment), and delirium; and other diagnostic criteria changes that will increase the accuracy of assessment of common mood disorders. These symptom differences--along with the effects of accompanying medical disorders--may contribute to the challenges of assessing anxiety in older adults. A palliative care approach has gained increasing support among caregivers who treat terminal disorders in the elderly. Alternatives to antipsychotic medications, including nondrug approaches, may provide urgently needed new treatments.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150909130424.htm){:target="_blank" rel="noopener"}


