---
layout: post
title: "Microsoft’s AI push to help disabled"
date: 2018-07-18
categories:
author: ""
tags: []
---


CEO Satya Nadella announced the new “AI for Accessibility” effort as he kicked off Microsoft’s annual conference for software developers  CEO Satya Nadella announced the new “AI for Accessibility” effort as he kicked off Microsoft’s annual conference for software developers  Microsoft is launching a $25 million initiative to use artificial intelligence to build better technology for people with disabilities. CEO Satya Nadella announced the new “AI for Accessibility” effort as he kicked off Microsoft’s annual conference for software developers. The Build conference in Seattle is meant to foster enthusiasm for the company’s latest ventures in cloud computing, artificial intelligence, internet-connected devices and virtual reality. Microsoft competes with Amazon and Google to offer internet-connected services to businesses and organizations. In unusually serious terms for a tech conference keynote, Mr. Nadella name-checked the dystopian fiction of George Orwell and Aldous Huxley, declared that “privacy is a human right” and warned of the dangers of building new technology without ethical principles in mind.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/technology/microsofts-ai-push-to-help-disabled/article23815717.ece?utm_source=rss_feed&utm_medium=referral&utm_campaign=rss_syndication){:target="_blank" rel="noopener"}


