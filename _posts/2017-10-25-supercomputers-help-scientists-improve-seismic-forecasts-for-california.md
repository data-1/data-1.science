---
layout: post
title: "Supercomputers help scientists improve seismic forecasts for California"
date: 2017-10-25
categories:
author: University of Texas at Austin, Texas Advanced Computing Center
tags: [Earthquake,UCERF3,Southern California Earthquake Center,Moment magnitude scale,Aftershock,Earths crust,Solid mechanics,Geophysics,Earthquakes,Seismology]
---


High-performance computing on TACC's Stampede system, and during the early user period of Stampede2, allowed us to create what is, by all measures, the most advanced earthquake forecast in the world, said Thomas H. Jordan, director of the Southern California Earthquake Center and one of the lead authors on the paper. It is also the first model capable of evaluating the short-term hazards that result from multi-event sequences of complex faulting. Based on these and other new factors, the new model increases the likelihood of powerful aftershocks but downgrades the predicted frequency of earthquakes between magnitude 6.5 and 7.0, which did not match historical records. The model estimates the statewide financial losses to the region (the costs to repair buildings and other damages) caused by an earthquake and its aftershocks. More importantly, the model was able to quantify how expected losses change with time due to recent seismic activity.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171024133733.htm){:target="_blank" rel="noopener"}


