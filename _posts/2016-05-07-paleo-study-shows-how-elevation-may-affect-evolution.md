---
layout: post
title: "Paleo study shows how elevation may affect evolution"
date: 2016-05-07
categories:
author: Brown University
tags: [Eocene,Paleontology,Mammal,Nature,Earth sciences]
---


The rising mountains dried out the interior, preparing mammals for a major climate change event 34 million years ago, researchers say. A new study explains why: The rise of the Rocky Mountains had forced North American mammals to adapt to a colder, drier world. 'We suggest that the late Eocene mammalian faunas of North America were already 'pre-adapted' to the colder and drier global conditions that followed the EO climatic cooling.' Estimated rainfall based on plant fossils in Wyoming, for example, dropped from about 1,200 millimeters a year 56 million years ago to only 750 millimeters a year about 49 million years ago. Explore further Over 65 million years North American mammal evolution has tracked with climate change

<hr>

[Visit Link](http://phys.org/news352523513.html){:target="_blank" rel="noopener"}


