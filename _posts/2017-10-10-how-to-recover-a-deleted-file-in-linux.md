---
layout: post
title: "How to Recover a Deleted File in Linux"
date: 2017-10-10
categories:
author: "Gabriel Cánepa"
tags: [Computer file,Disk partitioning,CentOS,Data management,Computing,Technology,System software,Software,Computer architecture,Operating system technology,Information technology management,Computer data,Computers,Utility software,Information Age,Software development,Computer science,Computer engineering,Storage software,Computer data storage]
---


Suggested Read: How to Recover Deleted Files/Directories Using Scalpel Tool  In this article we will share a tip that may be helpful to prevent this from happening to you, and a tool that you may consider using if at any point you are careless enough to do it anyway. ~/.bashrc  The forensics tool – Foremost  Hopefully, you will be careful with your files and will only need to use this tool while recovering a lost file from an external disk or USB drive. However, if you realize you accidentally removed a file in your system and are going to panic – don’t. We will begin by removing an image file named nosdos.jpg from the /boot/images directory:  # cd images # rm nosdos.jpg  To recover it, use foremost as follows (you’ll need to identify the underlying partition first – /dev/sda1 is where /boot resides in this case):  # foremost -t jpg -i /dev/sda1 -o /home/gacanepa/rescued  where /home/gacanepa/rescued is a directory on a separate disk – keep in mind that recovering files on the same drive where the removed ones were located is not a wise move. If, during the recovery, you occupy the same disk sectors where the removed files used to be, it may not be possible to recover anything.

<hr>

[Visit Link](http://www.tecmint.com/recover-deleted-file-in-linux/){:target="_blank" rel="noopener"}


