---
layout: post
title: "Open source and open data's role in modern meteorology"
date: 2015-07-23
categories:
author: "Ben Cotton"
tags: [Meteorology,Data,Science,Information technology,Computing,Technology,Branches of science]
---


While many forecasters still practice this art, computers have changed operations, research, and education. Open source software and open data are poised to bring more changes to the field. And while much of the software in use by educational institutions is open source, until recently there was little interoperability. Different visualization suites had their own data formats, so raw data would have to be processed several times. Just as chemistry students learn not only chemistry but lab procedures and equipment, so too must meteorology students learn about the tools of their trade: If you're forecasting today, you're using computers.

<hr>

[Visit Link](http://opensource.com/education/15/7/open-source-meteorology-tools){:target="_blank" rel="noopener"}


