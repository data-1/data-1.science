---
layout: post
title: "CryoSat extends its reach on the Arctic"
date: 2015-09-03
categories:
author: "$author"   
tags: [Sea ice,CryoSat,Arctic ice pack,Arctic Ocean,Ice,Sea ice thickness,Climate,Science,Nature,Physical geography,Applied and interdisciplinary physics,Earth sciences]
---


Applications CryoSat extends its reach on the Arctic 15/12/2014 8057 views 67 likes  CryoSat has delivered this year’s map of autumn sea-ice thickness in the Arctic, revealing a small decrease in ice volume. The volume is the second-highest since measurements began in 2010, and the five-year average is relatively stable. This, however, does not necessarily indicate a turn in the long-term downward trend. Ice thickness for operational applications Tommaso Parrinello, ESA’s CryoSat Mission Manager, said, “CryoSat has already achieved outstanding results, both within its original mission objectives and for unexpected applications. “Looking ahead, we are working hard to prototype new operational capabilities so that the measurements can be used for routine assessments in climate science and for services affected by Arctic sea ice.” To test this, scientists have produced an assessment of sea-ice thickness north of Alaska and eastern Russia with data acquired over the last month.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/CryoSat/CryoSat_extends_its_reach_on_the_Arctic){:target="_blank" rel="noopener"}


