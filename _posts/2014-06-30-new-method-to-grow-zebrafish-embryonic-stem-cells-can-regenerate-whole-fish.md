---
layout: post
title: "New method to grow zebrafish embryonic stem cells can regenerate whole fish"
date: 2014-06-30
categories:
author: "$author" 
tags: [Embryonic stem cell,Zebrafish,Stem cell,Cell biology,Biotechnology,Biology,Life sciences,Developmental biology]
---


For the first time, researchers report the ability to maintain zebrafish-derived ESCs for more than 2 years without the need to grow them on a feeder cell layer, in a study published in Zebrafish, a peer-reviewed journal from Mary Ann Liebert, Inc., publishers. The article is available free on the Zebrafish website. Ho Sing Yee and coauthors from the Malaysian Ministry of Science, Technology and Innovation (Pulau Pinang), Universiti Sains Malaysia (Penang), and National University of Singapore describe the approach they used to be able to maintain zebrafish stem cells in culture and in an undifferentiated state for long periods of time. About the Publisher  Mary Ann Liebert, Inc., publishers is a privately held, fully integrated media company known for establishing authoritative peer-reviewed journals in many promising areas of science and biomedical research, including DNA and Cell Biology, Stem Cells and Development, and Cellular Reprogramming. A complete list of the firm's 80 journals, books, and newsmagazines is available on the Mary Ann Liebert, Inc., publishers website.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/mali-nmt063014.php){:target="_blank" rel="noopener"}


