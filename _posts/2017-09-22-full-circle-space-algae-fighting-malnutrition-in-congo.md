---
layout: post
title: "Full circle: space algae fighting malnutrition in Congo"
date: 2017-09-22
categories:
author: ""
tags: [Spirulina (dietary supplement),Nutrition,Food and drink]
---


ESA astronaut Samantha Cristoforetti ate the first food containing spirulina in space and now the knowledge is being applied to a pilot project in Congo as a food supplement. Preparing for long missions far from Earth, astronauts will need to harvest their own food. Spirulina astronaut food The Arthrospira bacteria – better known as spirulina – have been a staple part of MELiSSA for many years because they is easy to grow and multiply rapidly. From space to Congo Researching spirulina A Belgian partner in the Melissa project, the SCK·CEN research centre, has been involved since the early days. And back to space Spirulina Experiments are also planned on the Space Station because nobody knows how some of the organisms in the MELiSSA system will grow in space.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Research/Full_circle_space_algae_fighting_malnutrition_in_Congo){:target="_blank" rel="noopener"}


