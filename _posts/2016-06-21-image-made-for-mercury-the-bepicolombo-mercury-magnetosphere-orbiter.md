---
layout: post
title: "Image: Made for Mercury—the BepiColombo Mercury Magnetosphere Orbiter"
date: 2016-06-21
categories:
author: "European Space Agency"
tags: [BepiColombo,Mercury (planet),Astronomical objects known since antiquity,Astronomical objects,Space science,Outer space,Astronomy,Spaceflight,Astronautics,Solar System,Flight,Spacecraft,Bodies of the Solar System,Planetary science,Sky,Planets of the Solar System,Space vehicles,Space exploration]
---


Credit: Airbus DS GmbH 2015  The shining face of the Mercury Magnetosphere Orbiter, Japan's contribution to the BepiColombo mission to the Solar System's innermost planet. The octagonal spacecraft is seen here at ESA's test centre in the Netherlands, where it is being tested alongside the other elements of this dual-spacecraft mission. During cruise, it will sit above ESA's Mercury Planetary Orbiter at the top of the BepiColombo stack, to be launched in April 2018. The Mercury Transfer Module will deliver them to Mercury using highly efficient electric propulsion. While ESA's craft will go into a 480 x 1500 km mapping orbit around Mercury, Japan's will enter a highly elliptical 590 x 11 640 km orbit to study the planet's environment and its magnetic field.

<hr>

[Visit Link](http://phys.org/news/2016-06-image-mercurythe-bepicolombo-mercury-magnetosphere.html){:target="_blank" rel="noopener"}


