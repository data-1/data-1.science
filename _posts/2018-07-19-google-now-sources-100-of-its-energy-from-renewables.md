---
layout: post
title: "Google Now Sources 100% of Its Energy From Renewables"
date: 2018-07-19
categories:
author: ""
tags: [Renewable energy,Wind power,Sustainable development,Climate change mitigation,Electric power,Energy and the environment,Energy,Nature,Physical quantities,Sustainable energy,Renewable resources,Sustainable technologies,Power (physics),Economy]
---


A Promise Fulfilled  In December 2016, about a year ago now, Google announced that the energy for its data centers would be from renewable sources. Google recently signed contracts for three wind power plants — for a total of 535 MW — which put their total energy infrastructure investment at over $3.5 billion, according to Electrek; a move which also gives the company over 3 gigawatts in total solar and wind capacity. The Renewable Revolution  Google's recent purchases have been driven by the continually decreasing costs of renewable energy, particularly solar and wind, which have both gone down 60 to 80 percent. Google is hardly the only company that's invested heavily in clean energy. Amazon's total renewable energy purchases are currently around 1.5GW.

<hr>

[Visit Link](https://futurism.com/google-energy-renewables/){:target="_blank" rel="noopener"}


