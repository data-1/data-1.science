---
layout: post
title: "Magnetic antiparticles offer new horizons for information technologies"
date: 2018-08-16
categories:
author: "Johannes Gutenberg Universitaet Mainz"
tags: [Electron,Antiparticle,Positron,Magnetic field,Electric charge,Matter,Force,Antimatter,Physics,Nature,Science,Electrical engineering,Particle physics,Quantum mechanics,Theoretical physics,Electromagnetism,Physical sciences,Applied and interdisciplinary physics]
---


In the same way that spheres and doughnuts have different topologies, skyrmions possess a special property called topological charge which plays a similar role to electric charges when their dynamics are concerned. Simulation shows periodical evolution of skyrmion-antiskyrmions pairs  Now physicists have shown that much richer phenomena can occur in nanometer-thick ferromagnets in which both skyrmions and antiskyrmions coexist. By using state-of-the-art simulation techniques to compute the magnetic properties and dynamics in such films, they studied how skyrmions and antiskyrmions respond when electric currents are applied to exert a force on them. By increasing the amount of energy transferred to the system from the applied currents, the researchers found that the trochoidal motion can evolve to skyrmion-antiskyrmions pairs being created periodically. In the nanoscale magnetic universe, at least, matter can arise naturally from a single antiparticle seed, Dupé said.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/jgum-mao081518.php){:target="_blank" rel="noopener"}


