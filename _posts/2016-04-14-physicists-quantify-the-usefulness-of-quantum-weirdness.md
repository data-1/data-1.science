---
layout: post
title: "Physicists quantify the usefulness of 'quantum weirdness'"
date: 2016-04-14
categories:
author: Lisa Zyga, Phys.Org
tags: [Quantum mechanics,Coherence (physics),Privacy,Science,Branches of science]
---


Recently, physicists have been developing ways to measure the amount of quantum coherence in a system. The new measurement method can answer questions such as how useful will a system's quantum coherence be for a task like encoding and decoding secret messages? In other words, the new method quantifies the advantage of using quantum mechanics. As the scientists explain, the usefulness of quantum coherence can be described by a measure that they introduce as the robustness of quantum coherence. DOI:  Also at Carmine Napoli, et al. Robustness of coherence: An operational and observable measure of quantum coherence.. DOI: 10.1103/PhysRevLett.116.150502 Also at arXiv:1601.03781 [quant-ph]  Marco Piani, et al. Robustness of asymmetry and coherence of quantum states.

<hr>

[Visit Link](http://phys.org/news/2016-04-physicists-quantify-quantum-weirdness.html){:target="_blank" rel="noopener"}


