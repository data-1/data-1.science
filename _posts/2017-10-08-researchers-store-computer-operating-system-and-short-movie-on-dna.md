---
layout: post
title: "Researchers store computer operating system and short movie on DNA"
date: 2017-10-08
categories:
author: "Columbia University School Of Engineering, Applied Science"
tags: [Technology,Computing,Biotechnology,Genetics,Biology,Life sciences,Information science]
---


In a study in Science, researchers Yaniv Erlich and Dina Zielinski describe a new coding technique for maximizing the data-storage capacity of DNA molecules. In a new study in Science, a pair of researchers at Columbia University and the New York Genome Center (NYGC) show that an algorithm designed for streaming video on a cellphone can unlock DNA's nearly full storage potential by squeezing more information into its four base nucleotides. Credit: Columbia University  To retrieve their files, they used modern sequencing technology to read the DNA strands, followed by software to translate the genetic code back into binary. The capacity of DNA data-storage is theoretically limited to two binary digits for each nucleotide, but the biological constraints of DNA itself and the need to include redundant information to reassemble and read the fragments later reduces its capacity to 1.8 binary digits per nucleotide base. But the price of DNA synthesis can be vastly reduced if lower-quality molecules are produced, and coding strategies like DNA Fountain are used to fix molecular errors, says Erlich.

<hr>

[Visit Link](https://phys.org/news/2017-03-short-movie-dna.html){:target="_blank" rel="noopener"}


