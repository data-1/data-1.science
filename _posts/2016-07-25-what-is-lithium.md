---
layout: post
title: "What Is Lithium?"
date: 2016-07-25
categories:
author: "Stephanie Pappas"
tags: [Lithium,Chemical element,Atoms,Chemical elements,Nature,Chemical substances,Materials,Sets of chemical elements,Physical sciences,Chemistry]
---


It's also used in mental health: Lithium carbonate is a common treatment of bipolar disorder, helping to stabilize wild mood swings caused by the illness. In a study with worms, biologists at MIT found that lithium inhibits a key protein in the worms' brain, making neurons linked to an avoidance behavior go dormant. However, according to the Big Bang Theory, the universe should hold three times as much lithium as can be accounted for in the oldest stars, an issue called the missing lithium problem. In fact, researchers recently found a giant star holding 3,000 times more lithium than normal giants, they reported in August 2018 in the journal Nature Astronomy. According to the U.S. Geological Survey, Argentina and Chile increased their lithium production 15 percent each in 2014 alone to meet the growing demand.

<hr>

[Visit Link](http://www.livescience.com/28579-lithium.html){:target="_blank" rel="noopener"}


