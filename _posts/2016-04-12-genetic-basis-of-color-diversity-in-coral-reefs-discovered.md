---
layout: post
title: "Genetic basis of color diversity in coral reefs discovered"
date: 2016-04-12
categories:
author: University Of Southampton
tags: [Coral reef,Color,Polymorphism (biology),Algae,Biology]
---


They have found that instead of using a single gene to control pigment production, corals use multiple copies of the same gene. We show that increased light levels switch the genes on that are responsible for the production of the colourful sunscreening pigments. Locally Accelerated Growth Is Part of the Innate Immune Response and Repair Mechanisms in Reef-Building Corals as Detected by Green Fluorescent Protein (Gfp)-Like Pigments. Coral Reefs 31, no. Coral Reefs 32, no.

<hr>

[Visit Link](http://phys.org/news341775678.html){:target="_blank" rel="noopener"}


