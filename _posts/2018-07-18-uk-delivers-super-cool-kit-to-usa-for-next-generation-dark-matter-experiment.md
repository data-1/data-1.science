---
layout: post
title: "UK delivers super-cool kit to USA for Next-Generation Dark Matter Experiment"
date: 2018-07-18
categories:
author: "Jake Gilmore, Science, Technology Facilities Council"
tags: [XENON,Weakly interacting massive particles,Large Underground Xenon experiment,Dark matter,Physics,Particle physics,Physical sciences,Science,Nature]
---


Credit: SURF  A huge U.K.-built titanium chamber designed to keep its contents at a cool -100C and weighing as much as an SUV has been shipped to the United States, where it will soon become part of a next-generation dark matter detector to hunt for the long-theorised elusive dark matter particle called a WIMP (Weakly Interacting Massive Particle). The cryostat chamber was built by a team of engineers at the UK's Science and Technology Facilities Council's Rutherford Appleton Laboratory in Oxfordshire, and journeyed around the world to the LUX-Zeplin (LZ) experiment, located 1400m underground at the Sanford Underground Research Facility (SURF) in South Dakota. We look forward to seeing these components fully assembled and installed underground in preparation for the start of LZ science. Seeing the cryostat arrive is a milestone moment as it has been years in the making. LZ will be at least 100 times more sensitive to finding signals from dark matter particles than its predecessor, the Large Underground Xenon experiment (LUX).

<hr>

[Visit Link](https://phys.org/news/2018-07-uk-super-cool-kit-usa-next-generation.html){:target="_blank" rel="noopener"}


