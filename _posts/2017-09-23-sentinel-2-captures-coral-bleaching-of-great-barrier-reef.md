---
layout: post
title: "Sentinel-2 captures coral bleaching of Great Barrier Reef"
date: 2017-09-23
categories:
author: ""
tags: [Coral bleaching,Coral reef,Satellite imagery,Physical geography,Earth sciences,Natural environment,Oceanography]
---


Applications Sentinel-2 captures coral bleaching of Great Barrier Reef 24/05/2017 12879 views 95 likes  Scientists observed the bleaching of Australia’s Great Barrier Reef early this year using satellite images. Such a pattern requires systematic and frequent monitoring to reliably identify a coral bleaching event from space. Studying Sentinel-2 images captured over the reef between January and April, scientists working under ESA’s Sen2Coral project noticed areas that were likely to be coral appearing to turn bright white, then darken as time went on. “Sadly, in the areas where bleaching can be seen, the abundant coral cover we observed in January was in April mostly overgrown with turf algae, with only some individual coral species surviving. Field campaign results Bleaching is also difficult to monitor in satellite imagery because of constant variations in the overlying water and other changes in the bottom such as micro-algal blooms.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-2/Sentinel-2_captures_coral_bleaching_of_Great_Barrier_Reef){:target="_blank" rel="noopener"}


