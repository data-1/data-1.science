---
layout: post
title: "Amazon Turns To Wind To Power Its Business"
date: 2017-10-19
categories:
author:  
tags: [Wind power,Amazon Web Services,General Electric,Renewable energy,Wind turbine,Technology,Infrastructure,Energy,Economy,Nature]
---


Above: Amazon today announced that its largest wind farm yet—Amazon Wind Farm Texas—is now up and running, adding more than 1,000,000 MWh of clean energy to the grid each year. In fact, Amazon Web Services (AWS), the company’s cloud computing business, has a long-term commitment to achieve 100 percent renewable energy usage for its global infrastructure footprint.Amazon has become a big player in the renewable energy industry. Jeff Bezos, Amazon's CEO, christened the farm today.Built by Lincoln Clean Energy, the Amazon Wind Farm Texas includes a fleet of GE wind turbines. In 2015, AWS announced the construction of Amazon Solar Farm US East, Amazon Wind Farm Fowler Ridge, Amazon Wind Farm US Central and Amazon Wind Farm US East. With the addition of Amazon Wind Farm Texas, Amazon is poised to generate almost 3.6 million megawatt-hours of renewable energy by year-end 2017.Scurry County, Texas, was selected as the site for the Amazon Wind Farm Texas because of its plentiful wind and affordable production and transmission costs.

<hr>

[Visit Link](http://www.gereports.com/amazon-turns-ge-wind-turbines-power-business/){:target="_blank" rel="noopener"}


