---
layout: post
title: "How olive oil is processed"
date: 2016-05-02
categories:
author: Tara Mchugh, Institute Of Food Technologists
tags: [Decanter centrifuge,Olive,Olive oil,Chemistry]
---


Therefore, harvesting time is frequently a compromise between harvesting efficiency and final oil quality. Traditional Olive Oil Processing  Traditional olive oil processing begins with crushing the olives into a paste. This particular mill is motorized and also includes wiper blades, both of which are more recent additions to the traditional stone mill. Pressure is applied to the disks, compacting the solid phase and percolating the liquid phases (oil and vegetation water). Although not necessary, virgin olive oil can be filtered prior to bottling.

<hr>

[Visit Link](http://phys.org/news350895107.html){:target="_blank" rel="noopener"}


