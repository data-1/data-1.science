---
layout: post
title: "In Photos: The World's Most Efficient Pollinators"
date: 2015-07-10
categories:
author: Tia Ghose
tags: [Bumblebee,Bee,Pollinator,Flower,Bees,Insects]
---


(Read the full story on the pollinators' decline)  Pollinators at risk  Bumblebees evolved about 35 million years ago in cooler climates. Shrinking range  In the new study, researchers found that bumblebee populations have disappeared from its historic southern edges but have not colonized northern areas that have warmed. Bumblebees aren't the only pollinators in the insect world; about 20,000 other bee species also play a critical role. The buff-tailed bumblebee will then move on to other flowers, spreading the plants pollen as it goes. Resilient holdouts  The buff-tailed bumblebes, like the one shown here, is one species of bee that is doing just fine despite global warming.

<hr>

[Visit Link](http://www.livescience.com/51488-photos-worlds-best-pollinators.html){:target="_blank" rel="noopener"}


