---
layout: post
title: "Low-cost clocks for landing on the Moon"
date: 2017-10-26
categories:
author:  
tags: [Lidar,Space science,Spaceflight,Technology,Science,Astronomy,Astronautics,Outer space]
---


Science & Exploration Low-cost clocks for landing on the Moon 26/10/2017 9311 views 166 likes  A European clock accurate to a trillionth of a second is set to be used on satellites and missions to the Moon. “We are the Ferrari of timers with the components of a tractor,” says Nikolai Adamovitch of Eventech. “We provide extreme timing accuracy using reliable and basic electronics. The company is already the world leader in timers for satellite laser stations but is looking to send its technology into space. The clock will measure the time light pulses take to return to Luna-27 after bouncing off the surface during landing.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Low-cost_clocks_for_landing_on_the_Moon){:target="_blank" rel="noopener"}


