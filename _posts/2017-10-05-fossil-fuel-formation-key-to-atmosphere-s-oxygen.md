---
layout: post
title: "Fossil fuel formation: Key to atmosphere’s oxygen?"
date: 2017-10-05
categories:
author: "University of Wisconsin-Madison"
tags: [Oxygen,Carbon,Atmosphere of Earth,Earth,Carbon dioxide,Photosynthesis,Plant,Nature,Physical sciences,Chemistry,Earth sciences,Materials]
---


So it may be no coincidence that animals appeared and evolved during the Cambrian explosion, which coincided with a spike in atmospheric oxygen roughly 500 million years ago. When you store sediment, it contains organic matter that was formed by photosynthesis, which converted carbon dioxide into biomass and released oxygen into the atmosphere. Burial removes the carbon from Earth's surface, preventing it from bonding molecular oxygen pulled from the atmosphere. Today, burning billions of tons of stored carbon in fossil fuels is removing large amounts of oxygen from the atmosphere, reversing the pattern that drove the rise in oxygen. The organic carbon in this shale was fixed from the atmosphere by photosynthesis, and its burial and preservation in this rock liberated molecular oxygen.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/12/161230185406.htm){:target="_blank" rel="noopener"}


