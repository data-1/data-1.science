---
layout: post
title: "NASA's new Dellingr Spacecraft baselined for pathfinding CubeSat to Van Allen belts"
date: 2018-05-07
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Van Allen radiation belt,Van Allen Probes,Goddard Space Flight Center,CubeSat,Bodies of the Solar System,Physical sciences,Space vehicles,Science,Solar System,Sky,Spacecraft,Flight,Astronautics,Astronomy,Spaceflight,Outer space,Space science]
---


Expected to launch in early 2021, the $4.5-million GTOSat will gather measurements from a highly elliptical Earth orbit that is a standard transfer orbit for communications satellites operating in geostationary orbit about 22,000 miles from Earth. Named for James Van Allen, their discoverer, these radiation belts are located in the inner region of Earth's magnetic environment, the magnetosphere. GTOSat will provide new data after the Van Allen Probes mission is over, Blum said. Dellingr-X Offers Solution  To continue gathering measurements of the Van Allen belts, however, the GTOSat team needed to assure that the spacecraft bus and its instruments could withstand the hostile environment that larger, more traditional spacecraft -- like the Van Allen probes -- are built to tolerate. NASA is studying a couple constellation-type missions that might employ as many as 36 SmallSats -- spacecraft larger than CubeSats but smaller and less expensive than more traditional satellites -- in multiple locations within the magnetosphere that encompasses the Van Allen belts.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/nsfc-nnd050118.php){:target="_blank" rel="noopener"}


