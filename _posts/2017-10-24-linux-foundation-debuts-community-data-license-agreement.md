---
layout: post
title: "Linux Foundation Debuts Community Data License Agreement"
date: 2017-10-24
categories:
author: The Linux Foundation
tags: [Open source,Linux,Linux Foundation,Free and open-source software,Open-source software,Artificial intelligence,Open data,Big data,Machine learning,Business models for open-source software,Software,Infrastructure,Technology,Computing]
---


New open data licenses make it easier for individuals and organizations of all types to share, analyze and use data collaboratively  PRAGUE (OPEN SOURCE SUMMIT EUROPE 2017), October 23, 2017 – The Linux Foundation, the nonprofit advancing professional open source management for mass collaboration, today announced the Community Data License Agreement (CDLA) family of open data agreements. The CDLA licenses are designed to help governments, academic institutions, businesses and other organizations open up and share data, with the goal of creating communities that curate and share data openly. The CDLA licenses are a key step in that direction and will encourage the continued growth of applications and infrastructure.”  CDLA Licenses Promote Sharing While Reducing Risk  The Linux Foundation, in collaboration with a broad set of participating organizations, drafted the CDLA licenses with the needs of companies, organizations and communities that have valuable data assets such as these to share. There are two CDLA licenses: a Sharing license that encourages contributions of data back to the data community and a Permissive license that puts no additional sharing requirements on recipients or contributors of open data. # # #  The Linux Foundation has registered trademarks and uses trademarks, including The Linux Foundation.

<hr>

[Visit Link](https://www.linuxfoundation.org/press-release/linux-foundation-debuts-community-data-license-agreement/){:target="_blank" rel="noopener"}


