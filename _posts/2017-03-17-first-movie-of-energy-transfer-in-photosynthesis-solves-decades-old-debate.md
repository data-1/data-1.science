---
layout: post
title: "First movie of energy transfer in photosynthesis solves decades-old debate"
date: 2017-03-17
categories:
author: "Imperial College London"
tags: [Photosynthetic reaction centre,Photosystem,Photosynthesis,Photosystem II,Chemical reaction,Energy,Artificial photosynthesis,Chemistry,Physical sciences,Nature]
---


This should help scientists understand how nature has perfected the process of photosynthesis, and how this might be copied to produce fuels by artificial photosynthesis. Light energy is harvested by 'antennae', and transferred to the reaction centre of Photosystem II, which strips electrons from water. It was previously thought that the process of charge separation in the reaction centre was a 'bottleneck' in photosynthesis - the slowest step in the process - rather than the transfer of energy along the antennae. Credit: M Kaucikas et al 2016  The team used a sophisticated system of lasers to cause reactions in crystals of Photosystem II, and then to measure in space and time the movement of excitations of electrons - and hence the transfer of energy - across the antennae and reaction centre. This proved that the initial step of separating charges for the water-splitting reaction takes place relatively quickly, but that the light harvesting and transfer process is slower.

<hr>

[Visit Link](http://phys.org/news/2016-12-movie-energy-photosynthesis-decades-old-debate.html){:target="_blank" rel="noopener"}


