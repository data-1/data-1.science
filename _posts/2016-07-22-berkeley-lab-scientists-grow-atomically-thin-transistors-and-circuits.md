---
layout: post
title: "Berkeley Lab scientists grow atomically thin transistors and circuits"
date: 2016-07-22
categories:
author: "DOE/Lawrence Berkeley National Laboratory"
tags: [Transistor,Moores law,Molybdenum disulfide,Electronics,Graphene,Semiconductor,Lawrence Berkeley National Laboratory,Office of Science,Electrical engineering,Semiconductors,Manufacturing,Electromagnetism,Electricity,Materials science,Chemistry,Materials,Technology]
---


In an advance that helps pave the way for next-generation electronics and computing technologies--and possibly paper-thin gadgets --scientists with the U.S. Department of Energy's Lawrence Berkeley National Laboratory (Berkeley Lab) developed a way to chemically assemble transistors and circuits that are only a few atoms thick. Both of these materials are single-layered crystals and atomically thin, so the two-part assembly yielded electronic structures that are essentially two-dimensional. This approach allows for the chemical assembly of electronic circuits, using two-dimensional materials, which show improved performance compared to using traditional metals to inject current into TMDCs, says Mervin Zhao, a lead author and Ph.D. student in Zhang's group at Berkeley Lab and UC Berkeley. ###  The research was supported by the Office of Naval Research and the National Science Foundation. The University of California manages Berkeley Lab for the U.S. Department of Energy's Office of Science.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/dbnl-bls070816.php){:target="_blank" rel="noopener"}


