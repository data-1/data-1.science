---
layout: post
title: "When mediated by superconductivity, light pushes matter million times more"
date: 2015-05-20
categories:
author: ""    
tags: [Radiation pressure,Electromagnetic radiation,Light,Photon,Quantum mechanics,Physics,Radiation,Applied and interdisciplinary physics,Physical sciences,Electromagnetism,Science,Physical chemistry,Theoretical physics,Chemistry,Nature,Physical phenomena]
---


It can be used to couple the electromagnetic laser field to, for example, the movement of the small mechanical oscillators that can be found inside ordinary watches. Radiation pressure physics in these systems have become measurable only when the oscillator is hit by millions of photons, explains theorist Jani Tuorila from the University of Oulu. They placed a superconducting island in between the electromagnetic field and the oscillator to mediate the interaction. Because of the increased radiation pressure coupling, the oscillator observes the electromagnetic field with the precision of a single photon. Such strong coupling allows, in principle, the measurement of quantum information from an oscillator nearly visible to the naked eye, explains professor Tero Heikkila from the University of Jyvaskyla who was in charge of the theoretical studies.

<hr>

[Visit Link](http://www.solardaily.com/reports/When_mediated_by_superconductivity_light_pushes_matter_million_times_more_999.html){:target="_blank" rel="noopener"}


