---
layout: post
title: "First laboratory generation of astrophysical shock waves created"
date: 2017-10-08
categories:
author: "DOE/Princeton Plasma Physics Laboratory"
tags: [Plasma (physics),Particle physics,Astrophysics,News aggregator,Wave,Science,Astronomy,Space science,Nature,Physical sciences,Physics]
---


The most high-energy of these astrophysical shocks occur too far outside the solar system to be studied in detail and have long puzzled astrophysicists. We have for the first time developed a platform for studying highly energetic shocks with greater flexibility and control than is possible with spacecraft, said Derek Schaeffer, a physicist at Princeton University and the U.S. Department of Energy's (DOE) Princeton Plasma Physics Laboratory (PPPL), and lead author of a July paper in Physical Review Letters that outlines the experiments. To produce the wave, scientists used a laser to create a high-energy plasma -- a form of matter composed of atoms and charged atomic particles -- that expanded into a pre-existing magnetized plasma. The rapid velocity represented a high magnetosonic Mach number and the wave was collisionless, emulating shocks that occur in outer space where particles are too far apart to frequently collide. To investigate the flow of plasma in the experiment, researchers installed a new diagnostic on the Rochester laser facility.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/07/170714140243.htm){:target="_blank" rel="noopener"}


