---
layout: post
title: "How to give a great talk, the lazy way"
date: 2015-09-16
categories:
author: Donnie Berkholz, View All Posts Donnie Berkholz, Tech Strategist, Leader In The Digital Economy
tags: [Microsoft PowerPoint,Flickr]
---


I give a lot of talks. What I put on my notes is the main points of the current slide, followed by my transition to the next slide. Presentations look a lot more natural when you say the transition before you move to the next slide rather than after. Remember presenter mode? Don’t go over time.

<hr>

[Visit Link](http://dberkholz.com/2015/04/20/how-to-give-a-great-talk-the-lazy-way/){:target="_blank" rel="noopener"}


