---
layout: post
title: "New technique scours the genome for genes that combat disease"
date: 2017-10-12
categories:
author: "Anne Trafton, Massachusetts Institute Of Technology"
tags: [Cas9,Gene,Genome editing,Guide RNA,RNA,Protein,CRISPR,Molecular biology,Molecular genetics,Branches of genetics,Biology,Life sciences,Genetics,Biochemistry,Biotechnology]
---


MIT researchers have adapted the CRISPR genome-editing system to identify genes that protect cells from a protein associated with Parkinson’s disease. However, the MIT team adapted it to randomly turn on or off distinct gene sets across large populations of cells, allowing the researchers to identify genes that protect cells from a protein associated with Parkinson's disease. The researchers are now investigating whether the guide RNA turns on each of these genes individually or whether it activates one or more regulatory genes that then turn the others on. Protective effects  Once the researchers identified these genes in yeast, they tested the human equivalents in human neurons, grown in a lab dish, that also overproduce alpha synuclein. It is also interesting to see that they could use yeast as the starting point of a genetic screen and identify guide RNAs that are protective to alpha-synuclein toxicity in mammalian cells, says Wong, who was not involved in the research.

<hr>

[Visit Link](https://phys.org/news/2017-10-technique-scours-genome-genes-combat.html){:target="_blank" rel="noopener"}


