---
layout: post
title: "Building Leadership in Open Source: A Free Guide"
date: 2018-06-02
categories:
author: "The Linux Foundation"
tags: [Linux,Open source,Leadership,Linux Foundation,Open-source-software movement,Communication]
---


Contributing code is just one aspect of creating a successful open source project. The path toward leadership is not always straightforward, however, so the latest Open Source Guide for the Enterprise from The TODO Group provides practical advice for building leadership in open source projects and communities. Being a good leader and earning trust within a community takes time and effort, and this free guide discusses various aspects of leadership within a project, including matters of governance, compliance, and culture. Building Leadership in an Open Source Community, featuring contributions from Gil Yehuda of Oath and Guy Martin of Autodesk, looks at how decisions are made, how to attract talent, when to join vs. when to create an open source project, and it offers specific approaches to becoming a good leader in open source communities. Presenting the company’s open source projects at conferences and contributing code in communities are the best ways to raise your company’s visibility.

<hr>

[Visit Link](https://www.linuxfoundation.org/blog/building-leadership-in-open-source-a-free-guide/){:target="_blank" rel="noopener"}


