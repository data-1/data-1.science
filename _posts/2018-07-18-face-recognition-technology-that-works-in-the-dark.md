---
layout: post
title: "Face recognition technology that works in the dark"
date: 2018-07-18
categories:
author: "U.S. Army Research Laboratory"
tags: [Facial recognition system,Forward-looking infrared,Deep learning,Biometrics,Surveillance,Technology,Branches of science,Artificial intelligence,Cognitive science,Computing]
---


This technology enables matching between thermal face images and existing biometric face databases/watch lists that only contain visible face imagery, said Riggan, a research scientist. When using thermal cameras to capture facial imagery, the main challenge is that the captured thermal image must be matched against a watch list or gallery that only contains conventional visible imagery from known persons of interest, Riggan said. They showed how the thermal-to-visible mapped representations from both global and local regions in the thermal face signature could be used in conjunction to synthesize a refined visible face image. The optimization problem for synthesizing an image attempts to jointly preserve the shape of the entire face and appearance of the local fiducial details. Using the synthesized thermal-to-visible imagery and existing visible gallery imagery, they performed face verification experiments using a common open source deep neural network architecture for face recognition.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180416142443.htm){:target="_blank" rel="noopener"}


