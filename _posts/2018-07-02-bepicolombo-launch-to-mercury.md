---
layout: post
title: "BepiColombo launch to Mercury"
date: 2018-07-02
categories:
author: ""
tags: [BepiColombo,Flight,Astronomical objects,Solar System,Space exploration,Space vehicles,Bodies of the Solar System,Sky,Discovery and exploration of the Solar System,Spaceflight technology,Planetary science,Planets of the Solar System,Space missions,Space research,Astronomical objects known since antiquity,Local Interstellar Cloud,Planets,Space probes,Space programs,Astronomy,Astronautics,Spaceflight,Space science,Outer space,Spacecraft]
---


Animation visualising BepiColombo's launch and cruise to Mercury. Some aspects have been simplified for the purpose of this animation. The joint ESA-JAXA mission comprises the European Mercury Planetary Orbiter and Japan's Mercury Magnetospheric Orbiter, which will be transported to the innermost planet by the Mercury Transfer Module. The animation highlights several key milestones, including the solar array and antenna deployments once in space, through to the arrival at Mercury seven years later. More information: esa.int/bepicolombo

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2018/06/BepiColombo_launch_to_Mercury){:target="_blank" rel="noopener"}


