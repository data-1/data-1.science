---
layout: post
title: "Plant cells survive but stop dividing upon DNA damage: NAIST researchers show the molecular pathway through which plants cease cell division upon DNA damage"
date: 2017-10-08
categories:
author: "Nara Institute of Science and Technology"
tags: [Cell cycle,Cyclin-dependent kinase,DNA repair,Cell growth,Cell (biology),Transcription (biology),DNA,News aggregator,Biological processes,Molecular biology,Biotechnology,Life sciences,Cell biology,Cellular processes,Biochemistry,Genetics,Macromolecules,Biology,Systems biology]
---


The cell cycle is the system through which a cell grows and divides. DNA damage suppresses CDK activity, which prevents progression to M phase. The study further found that the accumulated Rep-MYB proteins target genes responsible for transitioning the cell to M phase. Prof. Umeda says that the study provides a new paradigm for how plant cell division ceases upon DNA damage, thus preventing damaged cells from accumulating under stressful conditions. Without DNA damage, CDK prevents Rep-MYB from activating, which allows the cell cycle to progress to cell division.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171006101817.htm){:target="_blank" rel="noopener"}


