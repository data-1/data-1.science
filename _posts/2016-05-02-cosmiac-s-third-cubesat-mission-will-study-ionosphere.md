---
layout: post
title: "COSMIAC's third CubeSat mission will study ionosphere"
date: 2016-05-02
categories:
author: Kim Delker, University Of New Mexico
tags: [CubeSat,Satellite,Ionosphere,Cosmic ray,Spaceflight technology,Space science,Astronomy,Spaceflight,Outer space,Spacecraft,Technology,Astronautics,Flight,Space vehicles,Science]
---


The project, called Scintillation Observations and Response of the Ionosphere to Electrodynamics (SORTIE), will involve the launch of a CubeSat to collect data to study the ionospheric F-region. The goal of the mission will be to evaluate how our current knowledge and models of some aspects of the ionosphere relates to reality. The SORTIE mission features a 6U satellite, which stands for 6 units. Craig Kief, deputy director of COSMIAC, explained that the 6U CubeSat is a relatively new format for CubeSats and there have been few 6U satellite launches, especially at university centers. This latest mission will also help establish COSMIAC, a part of the School of Engineering, as a center of expertise in this area.

<hr>

[Visit Link](http://phys.org/news350806749.html){:target="_blank" rel="noopener"}


