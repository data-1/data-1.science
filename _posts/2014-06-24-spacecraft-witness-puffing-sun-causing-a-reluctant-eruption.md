---
layout: post
title: "Spacecraft Witness 'Puffing' Sun Causing a Reluctant Eruption"
date: 2014-06-24
categories:
author: Science World Report
tags: [Stellar corona,Sun,Solar flare,Outer space,Space science,Astronomy,Physical sciences,Electromagnetism,Nature,Physical phenomena,Physics,Astrophysics,Plasma physics,Astronomical objects,Stellar astronomy,Phases of matter,Solar System,Transparent materials,Space plasmas,Bodies of the Solar System]
---


Astronomers have spotted an unusual series of eruptions form the sun, thanks to a suite of spacecraft. The outermost layer of the sun is called the corona. The big, slow structure is reluctant to erupt, and does not begin to smoothly propagate outwards until several jets have occurred. Currently, the scientists plan to further investigate this event in order to find out whether there are shock waves, formed by the jets, passing through and driving the slow eruption, or whether magnetic reconfiguration is driving the jets, allowing the structure to slowly erupt. The findings could tell scientists a bit more about the sun and the forces that drive it.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15631/20140624/spacecraft-witness-puffing-sun-causing-reluctant-eruption.htm){:target="_blank" rel="noopener"}


