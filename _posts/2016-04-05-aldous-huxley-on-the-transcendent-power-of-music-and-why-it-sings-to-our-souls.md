---
layout: post
title: "Aldous Huxley on the Transcendent Power of Music and Why It Sings to Our Souls"
date: 2016-04-05
categories:
author: Maria Popova
tags: [Music,Silence,Experience,Art,Aldous Huxley,Philosophy,Culture]
---


More than merely echoing our experience, Huxley argues, music enlarges it:  Listening to expressive music, we have, not of course the artist’s original experience (which is quite beyond us, for grapes do not grow on thistles), but the best experience in its kind of which our nature is capable — a better and completer experience than in fact we ever had before listening to the music. In a different piece from the same collection, the uncommonly breathtaking title essay “Music at Night,” Huxley revisits the subject of humanity’s most powerful medium of expression:  Moonless, this June night is all the more alive with stars. In the Benedictus Beethoven gives expression to this awareness of blessedness. Thus, the introduction to the Benedictus in the Missa Solemnis is a statement about the blessedness that is at the heart of things. If we were to start describing in our “own words” exactly what Beethoven felt about this blessedness, how he conceived it, what he thought its nature to be, we should very soon find ourselves writing lyrical nonsense… Only music, and only Beethoven’s music, and only this particular music of Beethoven, can tell us with any precision what Beethoven’s conception of the blessedness at the heart of things actually was.

<hr>

[Visit Link](https://www.brainpickings.org/2016/04/05/aldous-huxley-music-at-night/){:target="_blank" rel="noopener"}


