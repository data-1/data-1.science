---
layout: post
title: "Researchers find hidden meaning and 'speed limits' within genetic code"
date: 2016-04-17
categories:
author: Case Western Reserve University
tags: [Messenger RNA,Genetic code,Translation (biology),Ribosome,Gene,Protein,Genetics,RNA,Gene expression,Cell (biology),Cellular processes,Molecular biology,Biochemistry,Biotechnology,Biology,Life sciences,Cell biology,Molecular genetics,Branches of genetics,Chemistry,Biological processes]
---


Ribosomes translate the information contained within the mRNA and produce the instructed protein. Many codons mean the same thing, but they influence decoding rate differently. Because of this, we can change an mRNA without changing its protein sequence and cause it to be highly expressed or poorly expressed and anywhere in between, he said. They ultimately linked these observations back to the process of mRNA translation. This will both stabilize the mRNA and cause it to be translated more efficiently, Coller said.

<hr>

[Visit Link](http://phys.org/news345395872.html){:target="_blank" rel="noopener"}


