---
layout: post
title: "Scientists boost crop production by 47 percent by speeding up photorespiration"
date: 2018-06-02
categories:
author: "Carl R. Woese Institute for Genomic Biology, University of Illinois at Urbana-Champaign"
tags: [Plant,Biology,Agriculture,Life sciences,Nature]
---


Increasing production of a common, naturally occurring protein in plant leaves could boost the yields of major food crops by almost 50 percent, according to a new study led by scientists at the University of Essex published today in Plant Biotechnology Journal. This work is part of the international research project Realizing Increased Photosynthetic Efficiency (RIPE) that is supported by Bill & Melinda Gates Foundation, the Foundation for Food and Agriculture Research, and U.K. Department for International Development. Over two years of field trials, they found that increasing the H-protein in the plants' leaves increases production 27 to 47 percent. If we can translate this discovery to food crops, we can equip farmers with resilient plants capable of producing more food despite increasing temperature stress. To further increase yields, the team plans to combine this trait with others developed by the RIPE project, including a method reported in Science that boosted production by 20 percent by helping plants adapt to fluctuating light levels more quickly.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/crwi-sbc052518.php){:target="_blank" rel="noopener"}


