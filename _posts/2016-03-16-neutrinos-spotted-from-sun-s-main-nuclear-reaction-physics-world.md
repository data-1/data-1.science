---
layout: post
title: "Neutrinos spotted from Sun's main nuclear reaction – Physics World"
date: 2016-03-16
categories:
author:  
tags: [Neutrino,Borexino,Solar neutrino,Nuclear physics,Physical sciences,Physics,Chemistry,Nature,Nuclear chemistry,Atoms,Particle physics,Astronomy,Science]
---


Solace for solar physicists: Borexino results back theory  Physicists working on the Borexino experiment in Italy have successfully detected neutrinos from the main nuclear reaction that powers the Sun. It is this low-energy neutrino that physicists have now detected. While the Borexino result will bring some solace to solar physicists, a new controversy has erupted. Fortunately, 1% of the Sun’s energy arises not from the proton–proton chain but instead from the CNO cycle, in which carbon, nitrogen and oxygen nuclei catalyse the hydrogen-to-helium reaction. These reactions spawn neutrinos, too, and the more carbon, nitrogen and oxygen the Sun has, the more of these neutrinos there should be.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/UlUKE7rDZ-8/neutrinos-spotted-from-suns-main-nuclear-reaction){:target="_blank" rel="noopener"}


