---
layout: post
title: "What is the Earth's average temperature?"
date: 2016-05-27
categories:
author: Matt Williams
tags: [Earth,Planet,Sun,Atmosphere,Solar System,Mars,Climate change,Circumstellar habitable zone,Equator,Season,Planetary habitability,Jupiter,Exoplanet,Saturn,Atmosphere of Earth,Ice age,Applied and interdisciplinary physics,Planetary science,Astronomy,Space science,Physical sciences,Planets,Earth sciences,Nature,Outer space,Astronomical objects,Planets of the Solar System,Physical geography]
---


Credit: NASA  Earth is the only planet in the solar system where life is known to exists. Measurement:  The average surface temperature on Earth is approximately 7.2°C, though (as already noted) this varies. Uranus is the coldest planet in the solar system, with a lowest recorded temperature of -224°C, while temperatures in Neptune's upper atmosphere reach as low as -218°C. Variations Throughout History:  Estimates on the average surface temperature of Earth are somewhat limited due to the fact that temperatures have only been recorded for the past two hundred years. Explore further What is the average surface temperature of the planets in our solar system?

<hr>

[Visit Link](http://phys.org/news/2015-08-earth-average-temperature.html){:target="_blank" rel="noopener"}


