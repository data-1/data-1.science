---
layout: post
title: "Entanglement on a chip: Breakthrough promises secure communications and faster computers"
date: 2015-09-05
categories:
author: Optica 
tags: [Quantum entanglement,Photon,Integrated circuit,Laser,Photonics,Physics,Electromagnetic radiation,Electrical engineering,Applied and interdisciplinary physics,Optics,Electromagnetism,Technology,Science]
---


New research, reported today in The Optical Society's (OSA) new high-impact journal Optica, describes how a team of scientists has developed, for the first time, a microscopic component that is small enough to fit onto a standard silicon chip that can generate a continuous supply of entangled photons. The new design is based on an established silicon technology known as a micro-ring resonator. By tailoring the design of this resonator, the researchers created a novel source of entangled photons that is incredibly small and highly efficient, making it an ideal on-chip component. Creating Entanglement on a Chip  To bring these new technologies to fruition, however, requires a new class of entangled photon emitters: ones that can be readily incorporated into existing silicon chip technologies. As a result, this research could facilitate the adoption of quantum information technologies, particularly quantum cryptography protocols, which would ensure secure communications in ways that classical cryptography protocols cannot.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-01/tos-eoa012115.php){:target="_blank" rel="noopener"}


