---
layout: post
title: "Chile's green energy future is powered by open data analysis"
date: 2017-09-24
categories:
author: "Chris Hermansen
(Correspondent)"
tags: [Hydroelectricity,Geographic information system,Energy planning,Technology]
---


This policy addresses five key strategic issues:  security and quality of supply  energy as a driver of development  energy that is compatible with the environment  energy efficiency and education  process for followup and review of energy policy  The policy requires hydroelectric power to play an important role in providing base load, augmented by wind and solar. First, we extended and adapted the High Conservation Value methodology as a way of measuring landscape values. CC BY-SA 4.0 Data from OpenStreetMap and Chile's Ministry of Public Works  We already had the points of intake and restitution for each potential hydroelectric project in our study area, also in Shapefile format, so we linked those points to the river reaches and oriented all the reaches so that they started at upstream points and ended at downstream points, using the open source ogr2ogr, QGIS, and shell scripting. And because all the fluvial OdVs were already on the river reaches, we decided that the logical thing to do was to transfer the non-fluvial OdVs onto the same reaches. This required relatively heavy-duty scripting, for which we chose the open source Groovy programming language in general, open source GeoScript-Groovy spatial extensions, and the open source OpenCSV to read and write files that could be used in QGIS or LibreOffice Calc.

<hr>

[Visit Link](https://opensource.com/life/16/8/open-source-helps-define-chiles-future-energy-policies){:target="_blank" rel="noopener"}


