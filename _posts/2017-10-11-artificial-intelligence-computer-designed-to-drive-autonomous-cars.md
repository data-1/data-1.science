---
layout: post
title: "Artificial intelligence computer designed to drive autonomous cars"
date: 2017-10-11
categories:
author: ""
tags: [Nvidia,Nvidia Drive,Graphics processing unit,Self-driving car,Artificial intelligence,Computers,Information Age,Computer hardware,Computer engineering,Computer science,Computing,Technology]
---


The NVIDIA DRIVE PX Pegasus AI computing platform is the world’s first designed to drive fully autonomous robotaxis. The new system, codenamed Pegasus, extends the NVIDIA DRIVE PX AI computing platform to handle Level 5 driverless vehicles. NVIDIA DRIVE PX Pegasus delivers over 320 trillion operations per second—more than 10x the performance of its predecessor, NVIDIA DRIVE PX 2. Today, their trunks resemble small data centers, loaded with racks of computers with server-class NVIDIA GPUs running deep learning, computer vision and parallel computing algorithms. NVIDIA DRIVE PX Platform  The NVIDIA DRIVE PX platform scales from a single mobile processor configuration delivering Level 2+/Level 3 capabilities to a combination of multiple mobile processors and discrete GPUs for full Level 5.

<hr>

[Visit Link](https://phys.org/news/2017-10-artificial-intelligence-autonomous-cars.html){:target="_blank" rel="noopener"}


