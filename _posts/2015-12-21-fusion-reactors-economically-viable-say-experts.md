---
layout: post
title: "Fusion reactors 'economically viable' say experts"
date: 2015-12-21
categories:
author: Durham University
tags: [Fusion power,Nuclear reactor,Nuclear power,Nuclear fusion,ITER,Nuclear fission,Physical quantities,Nuclear chemistry,Radioactivity,Electric power,Technology,Sustainable technologies,Chemistry,Energy technology,Energy conversion,Forms of energy,Nuclear energy,Nuclear technology,Nuclear physics,Energy,Power (physics),Nature]
---


Researchers at Durham University and Culham Centre for Fusion Energy in Oxfordshire, have re-examined the economics of fusion, taking account of recent advances in superconductor technology for the first time. Their analysis of building, running and decommissioning a fusion power station shows the financial feasibility of fusion energy in comparison to traditional fission nuclear power. We have known about the possibility of fusion reactors for many years but many people did not believe that they would ever be built because of the technological challenges that have had to be overcome and the uncertain costs. While there are still some technological challenges to overcome we have produced a strong argument, supported by the best available data, that fusion power stations could soon be economically viable. While the analysis considers the cost of building, running and decommissioning a fusion power plant, it does not take into account the costs of disposing of radioactive waste that is associated with a fission plant.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/du-fr100215.php){:target="_blank" rel="noopener"}


