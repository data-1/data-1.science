---
layout: post
title: "Scholar finds world temperatures influence views on global warming"
date: 2014-06-24
categories:
author: Clifton B. Parker, Stanford University
tags: [Climate change,Jon Krosnick,Climatic Research Unit email controversy,Republican Party (United States),Politics]
---


It is a timely issue in light of the White House's recent proposal, the Blueprint for a Secure Energy Future, to reduce carbon emissions by the nation's biggest source of pollution blamed for global warming: power plants. Do most Americans believe global warming is happening? But our research indicates that the explanation most likely is changing average world temperatures. In our 2013 survey, only 10 percent of respondents said that the federal government should do nothing about global warming, and 79 percent said that the government should do some, quite a bit or a great deal. And in our surveys, large majorities of Americans have favored government intervention in various ways to achieve that goal.

<hr>

[Visit Link](http://phys.org/news322807235.html){:target="_blank" rel="noopener"}


