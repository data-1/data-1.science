---
layout: post
title: "Artificial leaf: New efficiency record for solar hydrogen production is 14 percent: 17 year old record value finally exceeded"
date: 2016-07-01
categories:
author: "Helmholtz-Zentrum Berlin für Materialien und Energie"
tags: [Artificial photosynthesis,Solar energy,Water splitting,Solar cell,Materials,Environmental technology,Energy development,Physical quantities,Technology,Chemistry,Energy,Sustainable technologies,Energy technology,Sustainable energy,Nature]
---


One especially interesting solution for storing this energy is artificial photosynthesis. These use the electrical power that sunlight creates in individual semiconductor components to split water into oxygen and hydrogen. Record value now exceeded  Scientific facilities worldwide have therefore been researching for many years how to break the existing record for artificial photosynthesis of 12.4 %, which has been held for 17 years by NREL in the USA. Core component: Tandem Solar Cell  Now a team from TU Ilmenau, Helmholtz-Zentrum Berlin (HZB), the California Institute of Technology as well as the Fraunhofer ISE has considerably exceeded this record value. Next goals visible  Forecasts indicate that the generation of hydrogen from sunlight using high-efficiency semiconductors could be economically competitive to fossil energy sources at efficiency levels of 15 % or more.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150915090413.htm){:target="_blank" rel="noopener"}


