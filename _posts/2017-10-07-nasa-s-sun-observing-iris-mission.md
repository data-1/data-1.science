---
layout: post
title: "NASA's Sun-observing IRIS mission"
date: 2017-10-07
categories:
author: "Lina Tran, Nasa'S Goddard Space Flight Center"
tags: [Stellar corona,Sun,Solar wind,Goddard Space Flight Center,Space science,Astronomical objects known since antiquity,Science,Bodies of the Solar System,Spaceflight,Solar System,Astronomy,Outer space]
---


NASA's Interface Region Imaging Spectrograph, or IRIS, which will continue its study of the sun thanks to a recent mission extension, watches what is known as the interface region, the lower levels of the sun's atmosphere. This particular region is also responsible for generating most of the ultraviolet emission that reaches Earth. Our space weather and environment are continuously influenced by both these emissions and the solar wind. This video from NASA’s ScienceCast explores the mystery of coronal heating – why the sun’s million-degree upper atmosphere, the corona, is several hundred times hotter than the surface below – and how scientists are using IRIS to address it. Explore further The mystery of coronal heating

<hr>

[Visit Link](http://phys.org/news/2016-12-nasa-sun-observing-iris-mission.html){:target="_blank" rel="noopener"}


