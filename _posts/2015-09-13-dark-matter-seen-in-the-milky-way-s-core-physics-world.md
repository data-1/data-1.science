---
layout: post
title: "Dark matter seen in the Milky Way's core – Physics World"
date: 2015-09-13
categories:
author: "$author"   
tags: [Milky Way,Galaxy rotation curve,Dark matter,Galaxy,Parsec,Star,Physical cosmology,Astronomy,Physics,Physical sciences,Space science,Science,Astrophysics]
---


While it is apparent that the gravitational attraction of invisible dark matter is holding galaxies together, it has proved very difficult to measure the distribution of dark matter in the core of the Milky Way. These provide information about the rotation rate of our galaxy at distances between 3–20 kpc from its centre. In this study, however, the team considered every accepted possibility in the literature, calculating the rotation curve – the rotation rate of the galaxy as a function of radius – that would be predicted by this distribution if there were no dark matter present. The team calculated the difference between the observed and theoretical rotation curves at a large number of different radii between 3–20 kpc. This result means that there are significant quantities of dark matter well inside the 8 kpc radius of the Milky Way, provided that Newtonian dynamics holds true.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/HucrJUiihCI/dark-matter-seen-in-the-milky-ways-core){:target="_blank" rel="noopener"}


