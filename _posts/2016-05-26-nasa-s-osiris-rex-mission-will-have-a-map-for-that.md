---
layout: post
title: "NASA's OSIRIS-REx mission will have a map for that"
date: 2016-05-26
categories:
author: NASA/Goddard Space Flight Center
tags: [OSIRIS-REx,101955 Bennu,Goddard Space Flight Center,Asteroid,Astronautics,Space science,Outer space,Astronomy,Spaceflight,Science,Planetary science,Solar System,Bodies of the Solar System]
---


The mission also will investigate how pressure from sunlight influences the path of this traveling asteroid. This information will be used to produce four top-level maps for identifying the site where sample will be collected. The spacecraft will slowly approach the asteroid until the sample head at the end of the arm kisses the surface. From remote observations, the team assumes that Bennu should contain water and organic - or carbon-rich - material, but they don't know yet how this material is distributed across the surface. NASA Goddard Space Flight Center in Greenbelt, Maryland, provides overall mission management, systems engineering and safety and mission assurance for OSIRIS-REx.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/nsfc-nom052516.php){:target="_blank" rel="noopener"}


