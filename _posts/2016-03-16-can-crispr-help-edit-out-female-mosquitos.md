---
layout: post
title: "Can CRISPR help edit out female mosquitos?"
date: 2016-03-16
categories:
author: Cell Press
tags: [Gene drive,CRISPR gene editing,Mosquito-borne disease,Clinical medicine,Biotechnology,Medicine,Medical specialties,Genetics,Biology,Life sciences,Health sciences]
---


Genetic strategies to control dengue fever based on the release of sterile, transgenic mosquitoes are currently underway and have been successful where attempted. This discovery sets the stage for future efforts to leverage the CRISPR-Cas9 system to drive maleness genes such as Nix into mosquito populations, thereby converting females into males or simply killing females, Tu says. Either outcome would help to reduce mosquito populations and improve sex separation procedures, which are required in any genetic strategy to prevent the accidental release of disease-transmitting females into wild populations. For example, the accidental release of just a few mosquitoes during testing may be sufficient to establish gene drive systems in the wild, and it is not clear how to remove an introduced gene from a study area if needed. Trends in Parasitology, Adelman and Tu: Control of mosquito-borne infectious diseases: Sex and gene drive http://dx.doi.org/10.1016/j.pt.2015.12.003  Trends in Parasitology (@TrendsParasitol), published by Cell Press, is a monthly review journal that reflects the global significance of medical and veterinary parasites.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/cp-cch021016.php){:target="_blank" rel="noopener"}


