---
layout: post
title: "1200 years of water balance data challenge climate models"
date: 2016-04-11
categories:
author: Swiss Federal Institute for Forest, Snow and Landscape Research WSL
tags: [Climate,News aggregator,Climate variability and change,Climate change,Earth sciences,Physical geography,Natural environment,Nature,Earth phenomena,Atmospheric sciences,Applied and interdisciplinary physics]
---


Water availability in the Northern Hemisphere has seen much larger changes during the past twelve centuries than during twentieth century global warming, a new study in Nature reports. The researchers from Sweden, Germany, and Switzerland have for the first time reconstructed the variations in water availability across the Northern Hemisphere seamless for the past twelve centuries. Test for climate models  According to Ljungqvist, the study shows the importance of a millennium-long perspective on hydroclimate changes. But unlike the climate models our reconstruction does not show any dramatic increase in hydroclimate extremes, says Ljungqvist. It could be that the global warming is not yet strong enough to trigger the changes in precipitation patterns that climate models simulate.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/04/160406165534.htm){:target="_blank" rel="noopener"}


