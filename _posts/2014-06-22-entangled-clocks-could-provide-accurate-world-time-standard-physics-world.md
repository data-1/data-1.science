---
layout: post
title: "Entangled clocks could provide accurate world time standard – Physics World"
date: 2014-06-22
categories:
author: ""         
tags: [Atomic clock,Clock,Physics,Science,Theoretical physics,Applied and interdisciplinary physics,Quantum mechanics,Physical sciences]
---


The resulting universal time standard would be more accurate than is currently possible with individual atomic clocks, and the network could also be used to do a range of fundamental and applied research, such as mapping the Earth’s gravitational field or even testing new theories of gravity. Each clock then measures this collective entangled state and, after doing so, it uses the classical link to send both its own laser frequency and the phase difference it measures between this laser frequency and the atoms to the central clock. The clocks thereby reduce their uncertainty by collectively averaging over all the atoms in all the clocks, effectively behaving as a single, super-accurate atomic clock. Finally, it might be possible to use changes in the rate of individual clocks to detect gravitational waves or even to test new theories of gravity. Far-fetched but well-reasoned  Eugene Polzik, a quantum-optics expert whose team at the Niels Bohr Institute in Copenhagen built the first atomic clock containing entangled atoms, describes the proposal as “very interesting and important”.

<hr>

[Visit Link](http://feedproxy.google.com/~r/PhysicsWorld/~3/k-wJyA_5DwM/entangled-clocks-could-provide-accurate-world-time-standard){:target="_blank" rel="noopener"}


