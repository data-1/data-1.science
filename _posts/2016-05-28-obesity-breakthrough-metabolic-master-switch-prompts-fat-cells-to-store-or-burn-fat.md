---
layout: post
title: "Obesity breakthrough: Metabolic master switch prompts fat cells to store or burn fat"
date: 2016-05-28
categories:
author: Massachusetts Institute of Technology
tags: [Adipose tissue,Obesity,Adipocyte,Brown adipose tissue,Genetics,Health sciences,Biochemistry,Biology]
---


By analyzing the cellular circuitry underlying the strongest genetic association with obesity, the researchers have unveiled a new pathway that controls human metabolism by prompting our adipocytes, or fat cells, to store fat or burn it away. However, previous studies have failed to find a mechanism to explain how genetic differences in the region lead to obesity. They found evidence of a major control switchboard in human adipocyte progenitor cells, suggesting that genetic differences may affect the functioning of human fat stores. They found that the risk version activated a major control region in adipocyte progenitor cells, which turned on two distant genes, IRX3 and IRX5. Switching the C to a T in risk individuals turned off IRX3 and IRX5, restored thermogenesis to non-risk levels, and switched off lipid storage genes.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150819211106.htm){:target="_blank" rel="noopener"}


