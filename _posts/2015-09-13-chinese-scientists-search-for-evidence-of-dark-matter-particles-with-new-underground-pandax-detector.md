---
layout: post
title: "Chinese scientists search for evidence of dark matter particles with new underground PandaX detector"
date: 2015-09-13
categories:
author: Science China Press 
tags: [XENON,PandaX,Weakly interacting massive particles,Dark matter,Matter,Chemistry,Science,Physical sciences,Physics,Particle physics,Nature,Applied and interdisciplinary physics]
---


The standard model of particle physics, which has been very successful in explaining the properties of ordinary matter, can neither explain dark matter's existence nor its properties, Professor Ji and co-authors across China and the United States write in the new study. A particle interacting with liquid xenon produces both xenon excitation states and electron-ion pairs. The nuclear recoil signal from a WIMP elastic scattering event in liquid xenon differs from that of electron recoils. A small prototype for PandaX was developed and is running in the particle physics laboratory at Shanghai Jiao Tong University. Initial results from the PandaX Dark Matter Experiment could be released late this year.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/scp-css072214.php){:target="_blank" rel="noopener"}


