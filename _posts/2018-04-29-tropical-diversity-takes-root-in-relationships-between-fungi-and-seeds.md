---
layout: post
title: "Tropical diversity takes root in relationships between fungi and seeds"
date: 2018-04-29
categories:
author: "Smithsonian Tropical Research Institute"
tags: [Fungus,Seed,Species,Plant,Tree,Organisms,Kingdoms (biology),Plants,Botany,Ecology,Nature]
---


Early pairings with a particular fungus may influence whether a seed survives and also may help explain how tropical forests remain so diverse. Depending on the species of the seed it pairs with, a fungus might kill the seed, hamper its development or, on the contrary, aid its germination, said Carolina Sarmiento, Smithsonian Tropical Research Institute fellow and lead author of the new study, published in the Proceedings of the National Academy of Sciences. The team processed more than 8,300 seeds and isolated 1,460 fungi, identifying more than 200 distinct fungal strains. The filtering effect of plant species -- even as a seed -- upon the types of fungi that it attracts provides important insights into the elusive mystery of how tropical forest diversity persists. ###  The Smithsonian Tropical Research Institute, headquartered in Panama City, Panama, is a part of the Smithsonian Institution.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/stri-tdt092817.php){:target="_blank" rel="noopener"}


