---
layout: post
title: "Next-generation dark matter experiments get the green light"
date: 2015-09-13
categories:
author: Kate Greene, Lawrence Berkeley National Laboratory
tags: [Large Underground Xenon experiment,Weakly interacting massive particles,Dark matter,XENON,Physics,Particle physics,Celestial mechanics,Nature,Astroparticle physics,Cosmology,Astronomy,Physical cosmology,Science,Physical sciences,Astrophysics]
---


These so-called Generation 2 Dark Matter Experiments include the LUX-Zeplin (LZ) experiment, an international collaboration formed in 2012, managed by DOE's Lawrence Berkeley National Lab (Berkeley Lab) and to be located at the Sanford Underground Research Facility (SURF) in South Dakota. We're looking forward to making what has been a proposal into a real, operational, first-rate experiment. The experiment will build on the current dark matter experiment at SURF called the Large Underground Xenon detector, or LUX. When one of these particles passes through the xenon detector, it should occasionally produce an observable flash of light. By picking a combination of these WIMP detection techniques that balance the potential sensitivity, the technical readiness, and the cost, the idea is to have the broadest dark-matter detection program possible, says Murdock Gil Gilchriese, LZ project scientist and physicist in Berkeley Lab's Physics Division.

<hr>

[Visit Link](http://phys.org/news324714533.html){:target="_blank" rel="noopener"}


