---
layout: post
title: "Scientists track last 2,000 years of British evolution"
date: 2016-05-18
categories:
author: Nowogrodzki, Anna Nowogrodzki, You Can Also Search For This Author In
tags: []
---


Over the past 2,000 years, British people have adapted to become taller and blonder, more likely to have blue eyes and better able to digest milk, according to researchers who have developed a technique to track very recent changes in the human genome. Frequent favourites  Jonathan Pritchard, a geneticist at Stanford University in California, and his colleagues developed the technique and tested it on the whole-genome sequences of 3,195 people collected as part of a project to study the genomes of 10,000 Britons. It has also favoured a more complex trait: increased height. Taken together, the findings hint at a larger story about how a demand for bigger brains in humans has physically shaped the species, even in modern times. “I think, for human data, it could be used to be look at any recent adaptation in any global population,” says Scheinfeldt.

<hr>

[Visit Link](http://www.nature.com/news/scientists-track-last-2-000-years-of-british-evolution-1.19917?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


