---
layout: post
title: "Core work: Iron vapor gives clues to formation of Earth and Moon"
date: 2016-04-15
categories:
author: Uc Davis
tags: [Earth,Z Pulsed Power Facility,Moon,Earth science,Physical sciences,Science]
---


The Z machine is in Albuquerque, N.M., and is part of the Pulsed Power Program, which started at Sandia National Laboratories in the 1960s. Credit: Randy Montoya  Recreating the violent conditions of Earth's formation, scientists are learning more about how iron vaporizes and how this iron rain affected the formation of the Earth and Moon. We care about when iron vaporizes because it is critical to learning how Earth's core grew, said co-author Sarah Stewart, UC Davis professor of Earth and Planetary Sciences. The researchers found that the shock pressure required to vaporize iron is much lower than expected, which means more iron was vaporized during Earth's formation than previously thought. This artist’s illustration shows a planetary scale impact on the Moon.

<hr>

[Visit Link](http://phys.org/news344512173.html){:target="_blank" rel="noopener"}


