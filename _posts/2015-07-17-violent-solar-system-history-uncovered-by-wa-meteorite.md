---
layout: post
title: "Violent solar system history uncovered by WA meteorite"
date: 2015-07-17
categories:
author: Curtin University 
tags: [Impact event,Meteorite,Desert Fireball Network,Asteroid,4 Vesta,Bunburra Rockhole (meteorite),Meteoroid,Astronomical objects,Astronomy,Solar System,Bodies of the Solar System,Space science,Planetary science,Outer space,Physical sciences,Science]
---


Associate Professor Fred Jourdan, along with colleagues Professor Phil Bland and Dr Gretchen Benedix from Curtin's Department of Applied Geology, believe the meteorite is evidence that a series of collisions of asteroids occurred more than 3.4 billion years ago. They obtained three series of ages indicating that the meteorite recorded three impact events between 3.6 billion and 3.4 billion years ago. The same impact history has also been observed from meteorites originating from Vesta with any impact activity stopping after 3.4 billion years ago. The Meteorite was captured on camera by the Desert Fireball Network. The Bunburra Rockhole meteorite was the first meteorite to be caught on camera by the Desert Fireball Network in 2007.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/cu-vss080714.php){:target="_blank" rel="noopener"}


