---
layout: post
title: "High–tech imaging stamps on Queensland dinosaur stampede theory"
date: 2015-01-20
categories:
author: University Of Queensland
tags: [Lark Quarry Dinosaur Trackways]
---


(Phys.org) —Dinosaur footprints in Central Queensland's Lark Quarry were not all caused by a dinosaur stampede, as previously thought. UQ School of Biological Sciences PhD candidate Mr Anthony Romilio said Lark Quarry, 110km south of Winton, had been famed for preserving the footprints of a carnivorous dinosaur that approached a mixed herd of chicken- and turkey-sized dinosaurs, causing them to stampede approximately 93 million years ago. All these dinosaurs were on the move, but rather than all being present during one event, the footprints were made in a series of separate events across several hours or even days, Mr Romilio said. Our results show that the large dinosaur was a herbivore that crossed the area shortly before the site was covered in shallow water, while the smaller tracks occurred hours to days later. We also know from our previous research that some of these smaller dinosaurs were swimming.

<hr>

[Visit Link](http://phys.org/news324109898.html){:target="_blank" rel="noopener"}


