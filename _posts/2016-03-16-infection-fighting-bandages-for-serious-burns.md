---
layout: post
title: "Infection-fighting bandages for serious burns"
date: 2016-03-16
categories:
author: Ecole Polytechnique Fédérale de Lausanne
tags: [Bacteria,Infection,Burn,Healing,Pseudomonas aeruginosa,Antimicrobial resistance,Medicine,Medical specialties,Clinical medicine,Health,Diseases and disorders,Health sciences,Causes of death,Health care,Microbiology]
---


Death is more commonly the result of infections that can occur several months after being hospitalized. The researchers focused on the formidable bacterium called Pseudomonas aeruginosa, the main cause of infections and death among serious burn victims. The researchers have now demonstrated that by combining these biological bandages with special molecules called dendrimers, it would be possible to not only speed up healing but also to put a stop to infections. So some dendrimers have to remain in the bandage to destroy any intruders. With the new bandages, rather than treating infections, we will be preventing them.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/epfd-ibf022416.php){:target="_blank" rel="noopener"}


