---
layout: post
title: "NASA carbon counter reaches final orbit, returns data"
date: 2015-10-29
categories:
author: "$author"  
tags: [Orbiting Carbon Observatory 2,Orbiting Carbon Observatory,NASA,Goddard Space Flight Center,Flight,Space program of the United States,Astronomy,Outer space,Space science,Astronautics,Science,Spaceflight,Spacecraft]
---


NASA's OCO-2 spacecraft collected first light data Aug. 6 over New Guinea. Credit: NASA/JPL-Caltech/NASA GSFC  (Phys.org) —Just over a month after launch, the Orbiting Carbon Observatory-2 (OCO-2)—NASA's first spacecraft dedicated to studying atmospheric carbon dioxide—has maneuvered into its final operating orbit and produced its first science data, confirming the health of its science instrument. OCO-2 will produce the most detailed picture to date of sources of carbon dioxide, as well as their natural sinks—places on Earth's surface where carbon dioxide is removed from the atmosphere. With OCO-2 in its final orbit, mission controllers began cooling the observatory's three-spectrometer instrument to its operating temperatures. The team will begin delivering calibrated OCO-2 spectra data to NASA's Goddard Earth Sciences Data and Information Services Center for distribution to the global science community and other interested parties before the end of the year.

<hr>

[Visit Link](http://phys.org/news327039998.html){:target="_blank" rel="noopener"}


