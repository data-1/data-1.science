---
layout: post
title: "D-Wave and predecessors: From simulated to quantum annealing"
date: 2014-06-24
categories:
author: World Scientific Publishing
tags: [D-Wave Systems,Quantum annealing,Quantum computing,Technology,Science,Quantum mechanics,Branches of science,Computing,Computer science,Theoretical computer science,Applied mathematics]
---


Recently, it was suggested by several research groups that a network of superconducting, D-Wave type qubits, could realize a quantum adiabatic computer and efficiently solve optimization problems. The D-wave quantum computer is hereby discussed. Its predecessors are a linage of optimization algorithms from as far as the Monte-Carlo and Metropolis algorithm, through genetic algorithm, hill-climbing, simulated annealing, quantum adiabatic algorithm and quantum annealing. Therefore, along with the discussion of the works published by the D-wave group, we present a few opposing claims, e.g., those of Smolin, regarding both the quality and quantumness of the D-wave adiabatic computer. As an application of discussed algorithms, the authors suggest a novel simulated annealing algorithm for image restoration and outline also its quantum annealing extension.

<hr>

[Visit Link](http://phys.org/news322745572.html){:target="_blank" rel="noopener"}


