---
layout: post
title: "Storing Data In DNA Brings Nature Into The Digital Universe"
date: 2017-10-20
categories:
author:  
tags: [DNA sequencing,DNA,DNA digital data storage,General Electric,Computer data storage,Technology,Backup,Hard disk drive,Information,Computing]
---


Researchers who hold the world record for storing and retrieving data in DNA explain how the building blocks of life can be used to hold digital information as well. Since we’re not going to stop taking pictures and recording movies, we need to develop new ways to save them.Over millennia, nature has evolved an incredible information storage medium – DNA. Our own group at the University of Washington and Microsoft holds the world record for the amount of data successfully stored in and retrieved from DNA – 200 megabytes.Traditional media like hard drives, thumb drives or DVDs store digital data by changing either the magnetic electrical or optical properties of a material to store 0s and 1s.To store data in DNA, the concept is the same, but the process is different. That’s because the longer a DNA strand is, the harder it is to build chemically.So we need to break the data into smaller chunks, and add to each an indicator of where in the sequence it falls. For example, today’s DNA synthesis lets us write a few hundred bytes per second ; a modern hard drive can write hundreds of millions of bytes per second .

<hr>

[Visit Link](http://www.ge.com/reports/storing-data-dna-brings-nature-digital-universe/){:target="_blank" rel="noopener"}


