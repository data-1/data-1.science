---
layout: post
title: "Closer and closer"
date: 2015-09-03
categories:
author: "$author"   
tags: [Rosetta mission,Spaceflight,Space probes,European Space Agency space probes,Missions to comets,Outer space,Orbiters (space probe),Discovery and exploration of the Solar System,Space science,Space exploration,Bodies of the Solar System,Comets]
---


Science & Exploration Closer and closer 24/07/2014 47938 views  Postcards from space as Rosetta draws closer to its destination comet

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Rosetta/Highlights/Closer_and_closer){:target="_blank" rel="noopener"}


