---
layout: post
title: "Bats use both sides of brain to listen—just like humans"
date: 2016-04-24
categories:
author: Georgetown University Medical Center
tags: [Lateralization of brain function,Human,Brain,Language,Behavioural sciences,Cognitive science,Neuroscience,Cognition,Branches of science]
---


Researchers from Georgetown University Medical Center and American University have shown that, like humans, mustached bats use the left and right sides of their brains to process different aspects of sounds. Aside from humans, no other animal that has been studied, not even monkeys or apes, has proved to use such hemispheric specialization for sound processing—meaning that the left brain is better at processing fast sounds, and the right processing slow ones. Washington says the findings of asymmetrical sound processing in both human and bat brains make evolutionary sense. Bats need to use the fast timing of the left hemisphere to distinguish communication sounds from each other, because their communication sounds have rapid changes in frequency. One downside of having such asymmetrical sound processing is that it makes in-depth scientific studies of certain communication disorders, such as aphasia—a collection of language disorders —nearly impossible, he adds.

<hr>

[Visit Link](http://phys.org/news349328701.html){:target="_blank" rel="noopener"}


