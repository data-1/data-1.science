---
layout: post
title: "Boosting the force of empty space"
date: 2015-05-22
categories:
author: Medical University Of Vienna
tags: [Virtual particle,Vacuum,Photon,Quantum fluctuation,Atom,Quantum mechanics,Energy,Force,Physical quantities,Applied and interdisciplinary physics,Scientific theories,Nature,Science,Physics,Theoretical physics,Physical sciences]
---


In fact, empty space is a bubbling soup of various virtual particles popping in and out of existence – a phenomenon called vacuum fluctuations. A team of researchers from the Weizmann Institute of Science (Rehovot, Israel) and the Vienna University of Technology has now proposed a method of amplifying these forces by several orders of magnitude using a transmission line, channelling virtual photons. Due to the uncertainty principle, virtual particles can come into existence for a brief period of time, says Igor Mazets from the Vienna University of Technology. At very short distances, vacuum fluctuations can lead to an attractive force between atoms or molecules – the Van der Waals forces. The virtual particles will be forced to go into the direction of the other atom.

<hr>

[Visit Link](http://phys.org/news325236354.html){:target="_blank" rel="noopener"}


