---
layout: post
title: "Researchers put mouse gene in cattle to make them less susceptible to TB"
date: 2016-04-16
categories:
author: Bob Yirka
tags: [Tuberculosis,Genetic engineering,Genetically modified animal,Transcription activator-like effector nuclease,Infection,Cattle,Mycobacterium tuberculosis,Mycobacterium bovis,Virus,Biotechnology,Biology,Life sciences,Genetics,Clinical medicine,Health sciences,Medical specialties,Molecular biology,Branches of genetics,Biochemistry]
---


Credit: Wikipedia  (Phys.org) —A team of researchers working at Northwest A&F University in China has found that introducing a particular mouse gene into cattle can give them better protection against tuberculosis. The team looked to mice, which are immune to Mycobacterium bovis, the virus that causes bovine TB—they have a gene (SP110) that protects them. The three normal cows all showed normal infections. In another test, the researchers housed nine of the GM cows with infected normal cows, along with nine normal cows. The team reports that introducing the SP110 gene into the cow genome did not appear to have any spillover, a term used to describe unintended changes to the genome.

<hr>

[Visit Link](http://phys.org/news344769616.html){:target="_blank" rel="noopener"}


