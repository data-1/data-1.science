---
layout: post
title: "Team identifies ancient mutation that contributed to evolution of multicellular animals"
date: 2016-01-29
categories:
author: University of Chicago Medical Center
tags: [Evolution,Cell (biology),Multicellular organism,Protein,Life,Genetics,Biology,Gene,Mutation,Enzyme,Mitosis,Spindle apparatus,Organism,Animal,Cell biology,Biotechnology,Biological evolution,Molecular biology,Nature,Life sciences,Biochemistry]
---


It acts as a kind of molecular carabiner by binding to two different partner molecules: an 'anchor' protein on the inside of the cell membrane that indicates the position of adjacent cells and a motor protein that pulls on mitotic spindle filaments. Our experiments show that the GK-PID evolved its carabiner function early, before multicellularity itself appeared, Thornton said. A deeper analysis of the proteins' structural biology suggested that the answer lies in a process that Thornton calls molecular exploitation, in which a new molecule (in this case the anchor) fortuitously binds an old protein (GK-PID) because it just happens to be structurally similar to the protein's original molecular partner. The researchers found that the ancestral GK-PID bound the anchor protein in a very similar way to how the ancestral gk enzyme bound its substrate. We hope that the approach we used -- reconstructing in detail the ancient history of protein functions -- can be applied to the evolution of other key cellular processes, revealing the whole picture of multicellular life evolving from single-celled ancestors, Thornton said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/uocm-tia010716.php){:target="_blank" rel="noopener"}


