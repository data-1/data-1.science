---
layout: post
title: "Tail as old as time—researchers trace ankylosaur's tail evolution"
date: 2015-09-02
categories:
author: North Carolina State University
tags: [Ankylosauria,Ankylosaurus,Ankylosauridae,Ornithischians]
---


Credit: Sydney Mohr  How did the ankylosaur get its tail club? These weaponized ankylosaurids lived about 66 million years ago, during the Cretaceous period. In order for an ankylosaur to be able to support the weight of a knob and swing it effectively, the tail needs to be stiff, like an ax handle, says Arbour. The knob could have evolved first, in which case you'd see ankylosaurids with osteoderms enveloping the end of the tail, but with the tail remaining flexible. Credit: Sydney Mohr  By comparing the tails of the specimens, Arbour saw that by the early Cretaceous, ankylosaurs had begun to develop stiff tails with fused vertebrae.

<hr>

[Visit Link](http://phys.org/news/2015-08-tail-timeresearchers-ankylosaur-evolution.html){:target="_blank" rel="noopener"}


