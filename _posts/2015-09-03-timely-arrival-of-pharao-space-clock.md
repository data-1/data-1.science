---
layout: post
title: "Timely arrival of Pharao space clock"
date: 2015-09-03
categories:
author: "$author"   
tags: [Atomic clock,Outer space,Astronautics,Science,Spaceflight]
---


Comparing clocks under different gravity levels allows researchers to test Einstein’s theories on space–time and other theories in fundamental physics. Internet of clocks Pharao Accurate timekeeping is vital for pinpointing our location, secure banking and fundamental science, but it is not easy to compare data from the many atomic clocks on Earth. ACES is more than just one clock in space. This clock uses hydrogen atoms as a frequency reference and offers better stability but for a shorter time. By coupling the two clocks, ACES will provide the scientists with a unique, highly stable time reference in space.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Research/Timely_arrival_of_Pharao_space_clock){:target="_blank" rel="noopener"}


