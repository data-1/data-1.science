---
layout: post
title: "New open-access journal on cannabis and cannabinoid research launching fall 2015"
date: 2016-04-24
categories:
author: Mary Ann Liebert, Inc./Genetic Engineering News
tags: [Medical cannabis,Cannabinoid,Tetrahydrocannabinol,Health,Health care,Medicine,Health sciences,Clinical medicine,Medical specialties,Medical treatments,Psychoactive drugs]
---


New Rochelle, NY, April 24, 2015 -- Cannabis and Cannabinoid Research, a new peer-reviewed, open access journal from Mary Ann Liebert, Inc., publishers (http://www.liebertpub.com/), is the only journal dedicated to the scientific, medical, and psychosocial exploration of clinical cannabis, cannabinoids, and the biochemical mechanisms of endocannabinoids. The Journal will publish under the Creative Commons Attribution 4.0 (CC BY) license to ensure broad dissemination and participation. The medical use of cannabis has become a global phenomenon. There are now 1.1 million users of legal medical cannabis in the U.S. Increasing numbers of jurisdictions in the U.S. and around the world are allowing access to herbal cannabis, and a broad new range of policy initiatives are emerging to regulate cannabis production and use to conduct high-quality research. Topics in Cannabis and Cannabinoid Research will include:  Biochemical process of the endocannabinoid system  Cannabinoid receptors and signaling  Pharmaceuticals based on cannabis and cannabinoids  Optimal dosing and drug delivery  Short- and long-term effects on the brain and behavior  Toxicological studies  Analgesic effects, including neuropathic pain and chronic nerve injury  Neurological disorders, including epilepsy, multiple sclerosis, and glaucoma  Use of cannabis as antinauseants and antispasmodics  Immune function and chronic inflammation, including HIV  Cancer and cancer-related treatment  Screening and assessment for marijuana misuse and addiction  Social, behavioral, and public health impact  Ethics, regulation, legalization, and public policy  Cannabis and Cannabinoid Research will build a central forum and repository for peer-reviewed open access papers, as more high-quality research is needed to move the field forward.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/mali-noa042415.php){:target="_blank" rel="noopener"}


