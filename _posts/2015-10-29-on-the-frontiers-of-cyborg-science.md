---
layout: post
title: "On the frontiers of cyborg science"
date: 2015-10-29
categories:
author: American Chemical Society
tags: [Cyborg,Nanoelectronics,Nanowire,Electrophysiology,Brain,Neuroscience,Technology]
---


We can make it really look and behave like smart, soft biological material, and integrate it with cells and cellular networks at the whole-tissue level. Rapid-fire cell signaling controls all of the body's movements, including breathing and swallowing, which are affected in some neurodegenerative diseases. It's hard to say where this work will take us, he says. In this presentation, the development of nanowire nanoelectronic devices and their application as powerful tools for the recording and stimulation from the level of single cells to tissue will be discussed. In addition, we will discuss extension of these nanoelectronic scaffold concepts for the development of revolutionary probes for acute and chronic brain mapping as well as their potential as future electronic therapeutics.

<hr>

[Visit Link](http://phys.org/news326867181.html){:target="_blank" rel="noopener"}


