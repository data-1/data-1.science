---
layout: post
title: "Vita liftoff replay"
date: 2017-09-23
categories:
author: ""
tags: [European Space Agency,Space research,Flight,Spacecraft,Life in space,Space exploration,Space vehicles,Human spaceflight programs,Spaceflight technology,Crewed spacecraft,Scientific exploration,Human spaceflight,Aerospace,Space program of Russia,Space-based economy,Space program of the United States,Space industry,Space science,Space agencies,NASA,Outer space,Space programs,Astronautics,Spaceflight,Rocketry]
---


ESA astronaut Paolo Nespoli, NASA astronaut Randy Bresnik and Roscosmos commander Sergey Ryazansky launched to the International Space Station on 28 July from Baikonur Cosmodrome on their Soyuz MS-05 spacecraft. Paolo, Randy and Sergey will spend five months in space working and living on the International Space Station. Follow Paolo and his mission via paolonespoli.esa.int and the mission blog for updates.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2017/07/Vita_liftoff_replay){:target="_blank" rel="noopener"}


