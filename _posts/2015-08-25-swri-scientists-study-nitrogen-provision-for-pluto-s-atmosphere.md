---
layout: post
title: "SwRI scientists study nitrogen provision for Pluto's atmosphere"
date: 2015-08-25
categories:
author: Southwest Research Institute 
tags: [Pluto,Atmosphere,New Horizons,Alan Stern,Planemos,Space science,Astronomy,Planetary science,Outer space,Bodies of the Solar System,Solar System,Astronomical objects,Planets,Planets of the Solar System,Physical sciences,Astronomical objects known since antiquity]
---


San Antonio -- August 11, 2015 -- The latest data from NASA's New Horizons spacecraft reveal diverse features on Pluto's surface and an atmosphere dominated by nitrogen gas. The Astrophysical Journal Letters accepted the paper for publication on July 15, just a day after the spacecraft's closest encounter with the icy dwarf planet (ApJ, 808, L50). Singer and Stern wondered if comets could deliver enough nitrogen to Pluto's surface to resupply what is escaping its atmosphere. They also looked at whether craters made by the comets hitting the surface could excavate enough nitrogen - but that would require a very deep layer of nitrogen ice at the surface, which is not proven. The Johns Hopkins University Applied Physics Laboratory in Laurel, Md., designed, built, and operates the New Horizons spacecraft and manages the mission for NASA's Science Mission Directorate.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/sri-sss081115.php){:target="_blank" rel="noopener"}


