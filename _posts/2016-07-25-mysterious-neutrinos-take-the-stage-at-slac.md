---
layout: post
title: "Mysterious neutrinos take the stage at SLAC"
date: 2016-07-25
categories:
author: "Slac National Accelerator Laboratory"
tags: [Neutrino,Enriched Xenon Observatory,Radioactive decay,Deep Underground Neutrino Experiment,Elementary particles,Science,Leptons,Astrophysics,Nuclear physics,Nature,Physical sciences,Physics,Particle physics]
---


To find out more about the elusive particles and their potential links to cosmic evolution, invisible dark matter and matter's dominance over antimatter in the universe, the Department of Energy's SLAC National Accelerator Laboratory is taking on key roles in four neutrino experiments: EXO, DUNE, MicroBooNE and ICARUS. Neutrinos were also the central theme of the 43rd annual SLAC Summer Institute for particle physics and astrophysics. The Enriched Xenon Observatory (EXO) is searching for a theorized rare nuclear process – neutrinoless double beta decay – that would prove that neutrinos and antineutrinos are identical. DUNE: Trio of Neutrino Masses and Matter-Antimatter Imbalance  SLAC researchers are also taking part in another neutrino experiment – the Deep Underground Neutrino Experiment (DUNE), which will be constructed by a new international collaboration hosted at the Long-Baseline Neutrino Facility (LBNF) as the centerpiece of the particle physics program in the U.S.  As part of LBNF, neutrinos and antineutrinos will be sent 800 miles through the Earth from Fermi National Accelerator Laboratory in Illinois to the DUNE detector in South Dakota – an eye for neutrinos that will eventually consist of four 10,000-ton modules of liquid argon. The future DUNE experiment will send neutrinos and antineutrinos 800 miles through the Earth to determine the relative masses of the three known neutrino types and study whether neutrinos and antineutrinos behave differently.

<hr>

[Visit Link](http://phys.org/news/2015-09-mysterious-neutrinos-stage-slac.html){:target="_blank" rel="noopener"}


