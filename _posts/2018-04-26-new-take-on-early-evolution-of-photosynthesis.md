---
layout: post
title: "New take on early evolution of photosynthesis"
date: 2018-04-26
categories:
author: "Arizona State University"
tags: [Photosynthesis,Photosynthetic reaction centre,Protein,Metabolism,Organism,Chemical reaction,Energy,Chemistry,Physical sciences,Nature,Biology,Biochemistry]
---


The molecular engines in all photosynthetic organisms that convert light energy to chemical energy are called photochemical reaction centers. The team believes that the ancestral reaction center (ARC) was simpler than the versions that exist today. This ARC was probably homodimeric and interacted with molecules in the membrane, like the modern Type II RCs (and the heliobacterial RC), instead of with soluble proteins. In essence, they focused more on the structure and function of the RCs to reconstruct the evolutionary history, starting by making predictions about the ARC's structure and function. In the lineage that led to the modern Type II RCs, the core changed from homodimeric to heterodimeric, which allowed the RC to prioritize which quinone it gave electrons to, accelerating the chemistry.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180424093718.htm){:target="_blank" rel="noopener"}


