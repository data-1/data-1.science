---
layout: post
title: "Controlling RNA in living cells"
date: 2016-04-26
categories:
author: Massachusetts Institute of Technology
tags: [Nucleic acid sequence,Protein,RNA,Messenger RNA,Translation (biology),Gene,DNA sequencing,Gene expression,Chemistry,Nucleic acids,Biomolecules,Genetics,Structural biology,Life sciences,Biology,Biotechnology,Macromolecules,Biochemistry,Molecular biology,Nucleotides,Cell biology,Molecular biophysics,Molecular genetics,Organic compounds,Natural products,Branches of genetics]
---


CAMBRIDGE, MA -- MIT researchers have devised a new set of proteins that can be customized to bind arbitrary RNA sequences, making it possible to image RNA inside living cells, monitor what a particular RNA strand is doing, and even control RNA activity. You could use these proteins to do measurements of RNA generation, for example, or of the translation of RNA to proteins, says Edward Boyden, an associate professor of biological engineering and brain and cognitive sciences at the MIT Media Lab. Whereas now, given an RNA sequence, you can specify on paper a protein to target it. This system can also be used to stimulate translation of a target mRNA. This allowed them to dramatically increase translation of an mRNA molecule that normally wouldn't be read frequently.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/miot-cri042216.php){:target="_blank" rel="noopener"}


