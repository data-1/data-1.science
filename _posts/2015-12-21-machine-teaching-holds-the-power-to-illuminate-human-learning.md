---
layout: post
title: "'Machine teaching' holds the power to illuminate human learning"
date: 2015-12-21
categories:
author: University of Wisconsin-Madison
tags: [Teaching method,Machine learning,Learning,Psychology,Science,Research,Computer science,Educational psychology,Education,Artificial intelligence,Concepts in metaphysics,Technology,Academic discipline interactions,Psychological concepts,Neuroscience,Cognitive psychology,Behavior modification,Interdisciplinary subfields,Cognitive science,Cognition,Branches of science]
---


My hope is that machine teaching has an impact on the educational world. It's quite different from how people usually think about education, says Zhu. The machine learner (the computer) is like a student. Machine teaching uses sophisticated mathematics to allow researchers to model actual human students and devise the best possible lessons for teaching them. In order for the machine teaching approach to work, it needs a good model of how the learner behaves -- that is, how the learner's behavior changes with different kinds of learning or practice experiences, Rogers says.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/uow-th081115.php){:target="_blank" rel="noopener"}


