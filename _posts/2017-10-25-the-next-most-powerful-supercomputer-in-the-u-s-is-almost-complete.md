---
layout: post
title: "The Next Most Powerful Supercomputer in the U.S. Is Almost Complete"
date: 2017-10-25
categories:
author:  
tags: [FLOPS,Supercomputer,Computer architecture,Computing,Computers,Technology,Computer science,Computer hardware,Computer engineering,Information Age,Office equipment,Electronics industry]
---


Computing for Science  Inside one of the rooms of the Oak Ridge National Laboratory (ORNL) at Tennessee, the next fastest and most powerful supercomputer in the U.S. is getting ready to solve some of science's biggest questions. When stars explode, they create all of the elements we find in the universe, everything that's part of you and me and part of this planet gets created. Radically More Powerful Supercomputer  Currently, the world's fastest and most powerful supercomputer is in China. The Sunway TaihuLight is capable of 93 peta floating-point operations per second (FLOPS) of computing power, with a theoretical peak of 125 petaFLOPS. For the first time, we were able to do a simulation of a star exploding in three dimensions and we found that it wasn't symmetric all the way around, Bland said, referring to an exploding star simulation ORNL ran on the Titan.

<hr>

[Visit Link](https://futurism.com/the-next-most-powerful-supercomputer-in-the-u-s-is-almost-complete/){:target="_blank" rel="noopener"}


