---
layout: post
title: "Big step forward for quantum computing: Quantum simulator could be used to probe material properties, new phases of matter and solve optimization problems"
date: 2018-04-26
categories:
author: "Harvard University"
tags: [Quantum mechanics,Quantum simulator,Atom,Simulation,Quantum computing,Physical sciences,Applied and interdisciplinary physics,Physics,Theoretical physics,Science,Applied mathematics]
---


Harvard researchers have developed a specialized quantum computer, known as a quantum simulator, which could be used to shed new light on a host of complex quantum processes, from the connection between quantum mechanics and material properties to investigating new phases of matter and solving complex real-world optimization problems. The reason is because these interactions are quantum mechanical in nature. It is important that we can start by simulating small systems using our machine, he said. But in order to get from the start to the end, they have to go through the complex quantum mechanical state. It's that coherent quantum state, Bernien said, that allows the system to work as a simulator, and also makes the machine a potentially valuable tool for gaining new insight into complex quantum phenomena and eventually performing useful calculations.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/11/171129131423.htm){:target="_blank" rel="noopener"}


