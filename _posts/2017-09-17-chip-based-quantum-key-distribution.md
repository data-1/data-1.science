---
layout: post
title: "Chip-based quantum key distribution"
date: 2017-09-17
categories:
author: "Sibson, H. H. Wills Physics Laboratory, Department Of Electrical, Electronic Engineering, Centre For Quantum Photonics, University Of Bristol, Bristol, Erven, Godfrey, Miki"
tags: []
---


Both devices, along with fibre coupled single photon detectors, represent the full photonic QKD system. Figure 2: Transmitter output for the BB84 states. While the key was generated unambiguously from the time of arrival of the single photon in a pair, security of the channel was determined by measuring the visibility from interfering successive photon pulses. For BB84, the QBER is derived from the timing and phase errors, while for COW the QBER is derived from the timing error and security of the channel is estimated from phase coherence between successive pulses, and finally for DPS the QBER is estimated based on the error from the phase encoded information. For BB84, using an attenuation equal to 20 km of fibre we obtained an estimated secret key rate of 345 kbps using a clock rate of 560 MHz; using mean photon number pulses of 0.45, 0.1, and 5.0 × 10−4 for the signal and two decoy states chosen with probabilities of 0.8, 0.15, and 0.05 respectively; and observed an average QBER of 1.05%.

<hr>

[Visit Link](http://www.nature.com/ncomms/2017/170209/ncomms13984/full/ncomms13984.html?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


