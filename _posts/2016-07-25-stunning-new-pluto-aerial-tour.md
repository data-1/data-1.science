---
layout: post
title: "Stunning new Pluto aerial tour"
date: 2016-07-25
categories:
author: "Stuart Robbins"
tags: [New Horizons,Pluto,Sputnik Planitia,Cthulhu Macula,Long Range Reconnaissance Imager,Charon (moon),Bodies of the Solar System,Planetary science,Astronomy,Solar System,Outer space,Planemos,Planets of the Solar System,Space science]
---


The mosaic starts with images of the heart of Pluto – informally named Tombaugh Regio – and the immediate surrounding area that are up to 400 m/px. The mosaic then includes other images of the hemisphere New Horizons flew over that are up to 800 m/px and were released last week. The latest images (as of Sept. 11, 2015) downloaded from NASA’s New Horizons spacecraft were stitched together and rendered on a sphere to make this flyover. During the animation, the altitude of the observer rises until it is about 10 times higher to show about 80% of the hemisphere New Horizons flew closest to on July 14, 2015. Credit: NASA/JHUAPL/SwRI, Stuart Robbins  Our tour starts low over the informally named Norgay Montes at a height of about 120 miles (200 kilometers). We head north over Sputnik Planum (bright area to the left) and Cthulhu Regio (dark area to the right).

<hr>

[Visit Link](http://phys.org/news/2015-09-stunning-pluto-aerial.html){:target="_blank" rel="noopener"}


