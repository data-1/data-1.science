---
layout: post
title: "Human skull evolved along with two-legged walking, study confirms"
date: 2017-03-22
categories:
author: "University of Texas at Austin"
tags: [Bipedalism,Foramen magnum,Animals]
---


While many scientists generally attribute this shift to the evolution of bipedalism and the need to balance the head directly atop the spine, others have been critical of the proposed link. Validating this connection provides another tool for researchers to determine whether a fossil hominid walked upright on two feet like humans or on four limbs like modern great apes. Controversy has centered on the association between a forward-shifted foramen magnum and bipedalism since 1925, when Raymond Dart discussed it in his description of Taung child, a 2.8 million-year-old fossil skull of the extinct South African species Australopithecus africanus. However, in a study published in the Journal of Human Evolution, UT Austin anthropology alumna Gabrielle Russo, now an assistant professor at Stony Brook University, and UT Austin anthropologist Chris Kirk built on their own prior research to show that a forward-shifted foramen magnum is found not just in humans and their bipedal fossil relatives, but is a shared feature of bipedal mammals more generally. To make their case, Russo and Kirk compared the position and orientation of the foramen magnum in 77 mammal species including marsupials, rodents and primates.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/03/170317131200.htm){:target="_blank" rel="noopener"}


