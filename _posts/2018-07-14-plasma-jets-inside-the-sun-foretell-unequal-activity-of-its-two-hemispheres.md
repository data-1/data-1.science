---
layout: post
title: "Plasma jets inside the sun foretell unequal activity of its two hemispheres"
date: 2018-07-14
categories:
author: "Center Of Excellence In Space Sciences India"
tags: [Sun,Helioseismology,Sunspot,Solar cycle,Solar phenomena,Astrophysics,Astronomical objects known since antiquity,Physical sciences,Solar System,Space plasmas,Bodies of the Solar System,Science,Astronomy,Space science]
---


Figure 1: Image shows the migration of torsional oscillation from mid-latitudes to the equator of the Sun. The study period, indicated on the x-axis covers 16 years of ground-based observations of the Sun's rotation rate variations using helioseismology. Now, a team of scientists from the Center of Excellence in Space Sciences India at IISER Kolkata and the Tata Institute of Fundamental Research in Mumbai have uncovered a hitherto unknown link between plasma jets in the sun's interior and the sunspot cycle which may foretell the unequal activity of the sun's hemispheres. Plasma material at different locations inside the sun rotates at different rates powering a dynamo mechanism that creates the magnetic sunspots. In a paper in the Astrophysical Journal of the American Astronomical Society, Lekshmi B., Dibyendu Nandi and H.M. Antia report that asymmetries in plasma jets just below the sun's surface precede asymmetries in sunspot activity by about a year, a discovery with no clear theoretical explanation yet.

<hr>

[Visit Link](https://phys.org/news/2018-07-plasma-jets-sun-unequal-hemispheres.html){:target="_blank" rel="noopener"}


