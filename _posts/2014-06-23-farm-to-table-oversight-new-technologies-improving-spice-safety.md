---
layout: post
title: "Farm to table oversight, new technologies improving spice safety"
date: 2014-06-23
categories:
author: Institute Of Food Technologists
tags: [Food,Food safety,FDA Food Safety Modernization Act,Food and drink,Health]
---


New and improved manufacturing technologies, as well as a greater focus on the individual steps of the production process, are helping to enhance spice safety in the U.S. and throughout the world, according to a June 22 panel discussion at the 2014 Institute of Food Technologists (IFT) Annual Meeting & Food Expo in New Orleans. Approximately 60 percent of spices are imported, with 12 percent containing filth— various contaminants, including microorganisms and pathogens, such as salmonella. Consumption of spices contaminated with pathogens resulted in 14 reported illness outbreaks from 1973 to 2010 around the world. The challenge is ensuring safety throughout every step of the process, which often begins on a small family farm on the other side of the world, said George C. Ziobro, PhD, a research chemist at the FDA Center for Food Safety and Applied Nutrition, the U.S. alternate delegate to the Codex Alimentarius Committee on Spices and Culinary Herbs, and one of the authors of the recent FDA draft profile, Pathogens and Risk in Spices. While one outbreak is one too many, one recall is one too many, Lawrence noted that the FDA has recorded just three contamination/illness outbreaks for spices in 37 years.

<hr>

[Visit Link](http://phys.org/news322756017.html){:target="_blank" rel="noopener"}


