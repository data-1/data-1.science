---
layout: post
title: "Why many similar species coexist within complex ecosystems"
date: 2016-04-15
categories:
author: University Of Granada
tags: [Food web,Herbivore,Biodiversity,Predation,Ecosystem,Extinction,Science,Natural environment,Ecology,Branches of science]
---


View of an Amazon rainforest  Scientists from the universities of Granada and Warwick have published an article in the journal PNAS (Proceedings of the National Academy of Sciences), in which they suggest one possible answer for the enigma of stability in complex ecosystems like the Amazon rainforest or coral reefs. For decades, scientists have been fascinated by the amount and variety of life forms that coexist in certain complex ecosystems like the Amazon rainforest or coral reefs. The authors of this article measured to what extent species are structured in levels, so that most of the preys of any predator are in a level immediately below it. Although this structuring of trophic networks in strata (called 'trophic coherence') is not perfect in natural networks (for instance, there are omnivores who feed from different levels) it is, without any doubt, much larger in actual networks than what is normally predicted or estimated by the current mathematical models employed in environmental sciences. Coherence and stability  As this research has proved, such coherence is closely related to the stability of networks: the more coherence, the more stability, Muñoz says.

<hr>

[Visit Link](http://phys.org/news344682567.html){:target="_blank" rel="noopener"}


