---
layout: post
title: "A new way to produce clean hydrogen fuel from water using sunlight"
date: 2017-10-05
categories:
author: "Osaka University"
tags: [Hydrogen,Carbon,Photocatalysis,Water,Allotropes of phosphorus,Molecule,Metal,Hydrogen production,Fuel,Energy,Chemical substances,Chemical elements,Nature,Atoms,Chemistry,Physical sciences,Physical chemistry,Materials,Artificial materials,Manufacturing]
---


Osaka -- Hydrogen is the most abundant element in the universe, and is considered by many to be a potential clean fuel of the future. Now, researchers centered at Osaka University have developed a new kind of photocatalyst for producing hydrogen from water, which is not only free of expensive metals but also absorbs a wider range of sunlight than ever before. In the past, photocatalysts based on carbon nitride have needed help from precious metals to produce hydrogen from water. They showed that their photocatalyst was effective for producing hydrogen from water using energy from different kinds of light. Lead author Tetsuro Majima says, The hydrogen economy faces a great many challenges, but our work demonstrates the potential for efficiently and cheaply producing hydrogen from water with a photocatalyst based on widely abundant elements.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/ou-anw100317.php){:target="_blank" rel="noopener"}


