---
layout: post
title: "Spotlight on Schiaparelli’s landing site"
date: 2016-08-24
categories:
author: ""
tags: [Schiaparelli EDM,Mars,Planets,Spaceflight,Terrestrial planets,Astronomical objects known since antiquity,Planetary science,Planets of the Solar System,Space science,Solar System,Discovery and exploration of the Solar System,Astronomy,Astronautics,Exploration of Mars,Outer space,Flight,Bodies of the Solar System,Space exploration,Missions to the planets,Spacecraft,Space probes,Missions to Mars]
---


Science & Exploration Spotlight on Schiaparelli’s landing site 11/08/2016 16372 views 168 likes  Schiaparelli, the Entry, Descent and Landing Demonstrator Module of the joint ESA/Roscosmos ExoMars 2016 mission, will target the Meridiani Planum region for its October landing, as seen in this mosaic created from Mars Express images. The region was chosen based on its relatively flat and smooth characteristics, as indicated in the topography map, in order to satisfy landing safety requirements for Schiaparelli. NASA’s Opportunity rover also landed within this ellipse near Endurance crater in Meridiani Planum, in 2004, and has been exploring the 22 km-wide Endeavour crater for the last five years. It will also obtain the first measurements of electric fields on the surface of Mars that, combined with measurements of the concentration of atmospheric dust, will provide new insights into the role of electric forces in dust lifting, the trigger for dust storms. Perspective view in Meridiani Planum – with Schiaparelli landing ellipse ESA’s Mars Express, which has been in orbit at the Red Planet since 2003, is among the fleet of orbiters that will act as a data relay during Schiaparelli’s short battery-powered mission on the surface.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Mars_Express/Spotlight_on_Schiaparelli_s_landing_site){:target="_blank" rel="noopener"}


