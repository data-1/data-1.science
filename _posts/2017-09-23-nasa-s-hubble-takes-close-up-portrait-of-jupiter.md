---
layout: post
title: "NASA's Hubble takes close-up portrait of Jupiter"
date: 2017-09-23
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Jupiter,Great Red Spot,Hubble Space Telescope,Goddard Space Flight Center,Planet,Atmosphere of Jupiter,Astronomy,Science,Planemos,Physical sciences,Space science,Planetary science,Outer space,Solar System,Astronomical objects,Planets,Bodies of the Solar System,Planets of the Solar System,Sky]
---


At a distance of 415 million miles (668 million kilometers) from Earth, Jupiter offered spectacular views of its colorful, roiling atmosphere, the legendary Great Red Spot, and it smaller companion at farther southern latitudes dubbed Red Spot Jr. This program provides yearly Hubble global views of the outer planets to look for changes in their storms, winds, and clouds. The team timed the Hubble observation to coincide with when NASA's space probe Juno would be near its closest point to Jupiter, so that scientists could get concurrent observations. The Hubble Space Telescope is a project of international cooperation between NASA and ESA (European Space Agency). NASA's Goddard Space Flight Center in Greenbelt, Maryland, manages the telescope.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/nsfc-nht040617.php){:target="_blank" rel="noopener"}


