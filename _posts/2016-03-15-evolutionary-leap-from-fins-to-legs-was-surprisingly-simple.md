---
layout: post
title: "Evolutionary leap from fins to legs was surprisingly simple"
date: 2016-03-15
categories:
author: University Of Lincoln
tags: [Tetrapod,Evolution,Fin,Biodiversity,Science]
---


The authors found that fish and early tetrapods developed similar levels of anatomical diversity within their fins and limbs, despite the fact that their skeletons were constructed in very different ways. Dr Marcello Ruta said: Our work investigated how quickly the first legged vertebrates blossomed out to explore new skeletal constructions, with surprising results. We might expect that early tetrapods evolved limbs that were more complex and diverse than the fins of their aquatic predecessors. Our work challenges this received wisdom, and shows that, at least in the case of the evolution of early tetrapods, key innovations did not quickly lead to greater anatomical variety. Perhaps these dual requirements limited the number of ways in which these first legs could function and evolve, thereby constraining their range of variability.

<hr>

[Visit Link](http://phys.org/news/2016-03-evolutionary-fins-legs-surprisingly-simple.html){:target="_blank" rel="noopener"}


