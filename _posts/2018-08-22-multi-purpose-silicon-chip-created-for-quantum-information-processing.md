---
layout: post
title: "Multi-purpose silicon chip created for quantum information processing"
date: 2018-08-22
categories:
author: "University of Bristol"
tags: [Computer,Silicon,Quantum computing,Photonics,Computing,Integrated circuit,Photon,Qubit,Science,Technology,Physics,Computer science,Applied and interdisciplinary physics,Quantum mechanics,Electronics,Electrical engineering,Electromagnetism]
---


An international team of researchers led by the University of Bristol have demonstrated that light can be used to implement a multi-functional quantum processor. The Bristol team has been using silicon photonic chips as a way to try to build quantum computing components on a large scale and today's result, published in the journal Nature Photonics, demonstrates it is possible to fully control two qubits of information within a single integrated chip. But what is exciting is that it the different properties of silicon photonics that can be used for making a quantum computer have been combined together in one device. And the types of devices developed in Bristol, such as the one presented today, are showing just how well quantum devices can be engineered. A consequence of the growing sophistication and functionality of these devices is that they are becoming a research tool in their own right -- we've used this device to implement several different quantum information experiments using nearly 100,000 different re-programmed settings.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/uob-msc081718.php){:target="_blank" rel="noopener"}


