---
layout: post
title: "Gravitational waves detected 100 years after Einstein's prediction: LIGO opens new window on the universe with observation of gravitational waves from colliding black holes"
date: 2016-03-15
categories:
author: LIGO Laboratory
tags: [LIGO,Gravitational wave,Virgo interferometer,Max Planck Institute for Gravitational Physics,GEO600,Gravitational-wave observatory,Physics,Gravitational-wave astronomy,Celestial mechanics,Physical cosmology,Theory of relativity,Space science,Gravity,Astrophysics,Physical sciences,Astronomy,Science,General relativity]
---


The LIGO Observatories are funded by the National Science Foundation (NSF), and were conceived, built, and are operated by Caltech and MIT. Based on the observed signals, LIGO scientists estimate that the black holes for this event were about 29 and 36 times the mass of the sun, and the event took place 1.3 billion years ago. It is these gravitational waves that LIGO has observed. The Advanced LIGO detectors are a tour de force of science and technology, made possible by a truly exceptional international team of technicians, engineers, and scientists, says David Shoemaker of MIT, the project leader for Advanced LIGO. To make this fantastic milestone possible took a global collaboration of scientists -- laser and suspension technology developed for our GEO600 detector was used to help make Advanced LIGO the most sophisticated gravitational wave detector ever created, says Sheila Rowan, professor of physics and astronomy at the University of Glasgow.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2016/02/160211103935.htm){:target="_blank" rel="noopener"}


