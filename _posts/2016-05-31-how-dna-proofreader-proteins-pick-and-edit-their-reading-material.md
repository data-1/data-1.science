---
layout: post
title: "How DNA 'proofreader' proteins pick and edit their reading material"
date: 2016-05-31
categories:
author: "North Carolina State University"
tags: [DNA mismatch repair,DNA,DNA replication,DNA repair,Biology,Genetics,Molecular biology,Biochemistry,Biotechnology,Life sciences,Macromolecules,Nucleic acids,Nucleotides,Molecular genetics,Biological processes,Biomolecules,Branches of genetics,Cellular processes,Cell biology,Chemistry]
---


Fortunately, our bodies have a system for detecting and repairing these mismatches -- a pair of proteins known as MutS and MutL. MutL puts a nick in the newly synthesized DNA strand to mark it as defective and signals a different protein to gobble up the portion of the DNA containing the error. PCNA sits at the site of a DNA split, and its physical orientation at that site indicates which part of a DNA strand is new. Somehow, MutS and MutL interact with that signal, allowing MutL to nick the strand in the proper place and indicate need for repair. Using single molecule fluorescence methods, which allowed the researchers to watch one protein moving on one piece of DNA at a time, they determined that when MutS finds an error, it changes shape in a way that allows MutL to bind with it, holding it in place at the site of the mismatch.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150821111049.htm){:target="_blank" rel="noopener"}


