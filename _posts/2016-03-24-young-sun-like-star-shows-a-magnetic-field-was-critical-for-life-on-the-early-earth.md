---
layout: post
title: "Young sun-like star shows a magnetic field was critical for life on the early Earth"
date: 2016-03-24
categories:
author: Harvard-Smithsonian Center for Astrophysics
tags: [Solar wind,Star,Planet,Earth,Sun,Atmosphere,Planetary habitability,Kappa1 Ceti,Solar analog,Sunspot,Astronomy,Astronomical objects,Physical sciences,Planetary science,Stellar astronomy,Outer space,Planets,Space science,Nature,Solar System,Stars,Sky,Bodies of the Solar System]
---


Life appeared because our planet had a rocky surface, liquid water, and a blanketing atmosphere. A new study of the young, Sun-like star Kappa Ceti shows that a magnetic field plays a key role in making a planet conducive to life. This age roughly corresponds to the time when life first appeared on Earth. As a result, studying Kappa Ceti can give us insights into the early history of our solar system. Such a fierce stellar wind would batter the atmosphere of any planet in the habitable zone, unless that planet was shielded by a magnetic field.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/hcfa-ys031616.php){:target="_blank" rel="noopener"}


