---
layout: post
title: "Optimize SSL/TLS for Maximum Security and Speed"
date: 2015-09-30
categories:
author: Michael Boelen
tags: [HTTPS,Transport Layer Security,Hypertext Transfer Protocol,Information technology,Hypertext,Computer data,Computer security,Internet architecture,Internet Standards,Communications protocols,Information technology management,Computer engineering,Software,Cyberspace,Computer networking,Computer science,Information Age,Technology,Internet protocols,Computing,Internet,Application layer protocols,Protocols]
---


Nginx Configuration  For the purpose of demonstration, we will show some snippets in this article. Instead, use the great page of Mozilla, which helps you selecting the right cipher set, tuned to modern browsers. So where possible, allow clients to cache data and do compression. Compression  First level of performance tuning is sending less data on the line, with the help of file compression. Use HTTP2  Make use of the HTTP/2 protocol to enhance the speed of HTTPS connections.

<hr>

[Visit Link](http://linux-audit.com/optimize-ssl-tls-for-maximum-security-and-speed/){:target="_blank" rel="noopener"}


