---
layout: post
title: "Artificial intelligence finds 56 new gravitational lens candidates"
date: 2017-10-24
categories:
author: Netherlands Research School For Astronomy
tags: [Gravitational lens,VLT Survey Telescope,European Southern Observatory,Space science,Technology,Science,Astronomy]
---


This picture shows a sample of the handmade photos of gravitational lenses that the astronomers used to train their neural network. The astronomers trained the neural network using millions of homemade images of gravitational lenses. Then they confronted the network with millions of images from a small patch of the sky. Credit: Carlo Enrico Petrillo, University of Groningen  Gravitational lens candidates  Initially, the neural network found 761 gravitational lens candidates. I think it will become the norm since future astronomical surveys will produce an enormous quantity of data which will be necessary to inspect.

<hr>

[Visit Link](https://phys.org/news/2017-10-artificial-intelligence-gravitational-lens-candidates.html){:target="_blank" rel="noopener"}


