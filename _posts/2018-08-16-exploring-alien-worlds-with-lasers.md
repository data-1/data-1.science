---
layout: post
title: "Exploring alien worlds with lasers"
date: 2018-08-16
categories:
author: ""
tags: [European Space Agency,Biosignature,Space science,Spaceflight,Physical sciences,Astronomy,Science,Outer space]
---


Enabling & Support Exploring alien worlds with lasers 10/01/2018 8389 views 146 likes  In everyday life we look and touch things to find out what they are made of. A powerful scientific technique does the same using lasers – and in two years’ time it will fly in space for the first time. A researcher working with ESA has been investigating how lasers might be used in future space missions. “We fire a laser at a material of interest,” explains Melissa McHugh of Leicester University in the UK, “and measure how much its colour is changed as it scatters off the surface, to identify the molecules responsible. “My research has been looking at how far we can extend the technique in future,” adds Melissa.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/Exploring_alien_worlds_with_lasers){:target="_blank" rel="noopener"}


