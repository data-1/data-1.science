---
layout: post
title: "Scientists discover new roles for viral genes in the human genome"
date: 2016-04-16
categories:
author: Winnie Lim, Agency For Science, Technology, Research, A Star
tags: [Retrovirus,Virus,Genome,Human genome,RNA,Gene,Cell (biology),Genetics,Biology,Biotechnology,Life sciences,Molecular biology,Biochemistry,Branches of genetics]
---


Research on the expression of viral DNA within the human genome furthers our understanding of human evolution and embryonic development  Singapore – The human genome is the blueprint for human life, but much of this blueprint still remains a mystery. Therefore, scientists investigate the RNAs in the cell to identify active genes. The researchers now showed that a part of the ERVs which functions as activator can be used to identify cells that show expression of these ERV families. From research with human embryonic stem cells, we know that ERVs have become essential, so it is quite likely that the ERVs described in this study contribute in a number of ways to human development. We are now developing new algorithms that will help us identify additional ERVs in the human genome, and we try to isolate cells that express these ERV-RNAs.

<hr>

[Visit Link](http://phys.org/news344760417.html){:target="_blank" rel="noopener"}


