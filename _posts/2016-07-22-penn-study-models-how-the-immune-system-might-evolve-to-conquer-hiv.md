---
layout: post
title: "Penn study models how the immune system might evolve to conquer HIV"
date: 2016-07-22
categories:
author: "University of Pennsylvania"
tags: [Neutralizing antibody,Virus,Antibody,Infection,HIV,Medical specialties,Immunology,Biology,Clinical medicine,Life sciences,Microbiology]
---


Yet the body's immune system can also evolve. Their findings, which suggest that presenting the immune system with a large diversity of viral antigens may be the best way to encourage the emergence of such potent antibodies, have implications for designing vaccines against HIV and other chronic infections. Their model calculated the average binding affinities between the entire population of viral strains and the repertoire of antibodies over time to understand how they co-evolve. We're not only looking at one virus binding to one antibody but the whole diversity of interactions that occur over the course of a chronic infection. Despite the effectiveness of broadly neutralizing antibodies, none of the patients with these antibodies has been cured of HIV, Plotkin said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/uop-psm072116.php){:target="_blank" rel="noopener"}


