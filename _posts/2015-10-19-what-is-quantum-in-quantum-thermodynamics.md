---
layout: post
title: "What is quantum in quantum thermodynamics?"
date: 2015-10-19
categories:
author: Lisa Zyga
tags: [Quantum mechanics,Heat,Physics,Theoretical physics,Science,Applied and interdisciplinary physics,Scientific theories,Scientific method,Physical sciences,Applied mathematics]
---


Physicists have shown that the three main types of engines (four-stroke, two-stroke, and continuous) are thermodynamically equivalent in a certain quantum regime, but not at the classical level. For the first time, they have shown a difference in the thermodynamics of heat machines on the quantum scale: in part of the quantum regime, the three main engine types (two-stroke, four-stroke, and continuous) are thermodynamically equivalent. This new thermodynamical equivalence principle is purely quantum, as it depends on quantum effects, and does not occur at the classical level. To the best of my knowledge, this is the first time [that a difference between quantum and classical thermodynamics has been shown] in heat machines, Uzdin told Phys.org. (Left) (a) In the quantum regime where the engine action is relatively small, all three engines generate the same amount of work after the completion of each cycle (the vertical lines indicate a complete cycle).

<hr>

[Visit Link](http://phys.org/news/2015-10-quantum-thermodynamics.html){:target="_blank" rel="noopener"}


