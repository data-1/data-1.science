---
layout: post
title: "Splitting water: Nanoscale imaging yields key insights"
date: 2018-07-19
categories:
author: "DOE/Lawrence Berkeley National Laboratory"
tags: [Artificial photosynthesis,Office of Science,Lawrence Berkeley National Laboratory,Nanotechnology,Photoelectrochemical cell,American Association for the Advancement of Science,Water splitting,Photosynthesis,Laboratory,Water,Materials,Applied and interdisciplinary physics,Technology,Science,Physical chemistry,Nature,Physical sciences,Chemistry]
---


In the quest to realize artificial photosynthesis to convert sunlight, water, and carbon dioxide into fuel - just as plants do - researchers need to not only identify materials to efficiently perform photoelectrochemical water splitting, but also to understand why a certain material may or may not work. Now scientists at Lawrence Berkeley National Laboratory (Berkeley Lab) have pioneered a technique that uses nanoscale imaging to understand how local, nanoscale properties can affect a material's macroscopic performance. This technique correlates the material's morphology to its functionality, and gives insights on the charge transport mechanism, or how the charges move inside the material, at the nanoscale, said Toma, who is also a researcher in the Joint Center for Artificial Photosynthesis, a Department of Energy Innovation Hub. This technique has already been used to analyze local charge transport and optoelectronic properties of solar cell materials but is not known to have been used to understand the charge carrier transport limitations at the nanoscale in photoelectrochemical materials. Eichhorn and Toma worked with scientists at the Molecular Foundry, a nanoscale science research facility at Berkeley Lab, on these measurements through the Foundry's user program.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/dbnl-swn071718.php){:target="_blank" rel="noopener"}


