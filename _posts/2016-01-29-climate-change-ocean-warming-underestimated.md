---
layout: post
title: "Climate change: Ocean warming underestimated"
date: 2016-01-29
categories:
author: University of Bonn
tags: [Sea level rise,Earth sciences,Physical geography,Applied and interdisciplinary physics,Oceanography,Hydrography,Hydrology,Natural environment,Environmental science,Physical oceanography,Science]
---


To date, research on the effects of climate change has underestimated the contribution of seawater expansion to sea level rise due to warming of the oceans. In the deeper parts of the ocean, even a small amount of warming is enough to create a significant rise in sea level, says Dr.-Ing. To date, we have underestimated how much the heat-related expansion of the water mass in the oceans contributes to a global rise in sea level, says Dr. Jürgen Kusche, Professor of Astronomical, Physical and Mathematical Geodesy at the University of Bonn. Effect is twice as large as the melting ice masses in Greenland  Until now, it was assumed that sea levels rose an average of 0.7 to 1.0 millimeters a year due to this thermometer effect. In addition, the sea-level rise varies strongly due to volume expansion in various ocean regions along with other effects.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/uob-cco012516.php){:target="_blank" rel="noopener"}


