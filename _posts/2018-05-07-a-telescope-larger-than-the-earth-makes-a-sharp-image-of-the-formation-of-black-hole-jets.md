---
layout: post
title: "A telescope larger than the Earth makes a sharp image of the formation of black hole jets"
date: 2018-05-07
categories:
author: "Aalto University"
tags: [Spektr-R,Black hole,Galaxy,Telescope,Radio telescope,Perseus (constellation),Astronomy,Space science,Physical sciences,Astronomical objects,Science,Astrophysics,Outer space]
---


An international team of researchers has imaged newly forming jets of plasma from a massive black hole with unprecedented accuracy. Radio images made with a combination of telescopes in space and on the ground resolve the jet structure merely a couple of hundred black hole radii or 12 light days from its launching site. The researchers were able to resolve the jet structure ten times closer to the black hole in NGC 1275 than what has been possible before with ground-based instruments, revealing unprecedented details of the jet formation region. It turned out that the observed width of the jet was significantly wider than what was expected in the currently favoured models where the jet is launched from the black hole's ergosphere - an area of space right next to a spinning black hole where space itself is dragged to a circling motion around the hole, explains Professor Gabriele Giovannini from Italian National Institute for Astrophysics, the lead author of the paper published in Nature Astronomy yesterday. Another result from the study is that the jet structure in NGC 1275 significantly differs from the jet in the very nearby galaxy Messier 87, which is the only other jet whose structure has been imaged equally close to the black hole.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-04/au-atl040318.php){:target="_blank" rel="noopener"}


