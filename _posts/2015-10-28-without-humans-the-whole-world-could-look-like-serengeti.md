---
layout: post
title: "Without humans, the whole world could look like Serengeti"
date: 2015-10-28
categories:
author: Aarhus University
tags: [Biodiversity,Human,Mammal,Africa,Natural environment,Nature,Ecology]
---


In this follow-up study, they investigate what the natural worldwide diversity patterns of mammals would be like in the absence of past and present human impacts, based on estimates of the natural distribution of each species according to its ecology, biogeography and the current natural environmental template. They provide the first estimate of how the mammal diversity world map would have appeared without the impact of modern man. The current world map of mammal diversity shows that Africa is virtually the only place with a high diversity of large mammals. Today, there is a particularly large number of mammal species in mountainous areas. The current high level of biodiversity in mountainous areas is partly due to the fact that the mountains have acted as a refuge for species in relation to hunting and habitat destruction, rather than being a purely natural pattern.

<hr>

[Visit Link](http://phys.org/news/2015-08-humans-world-serengeti.html){:target="_blank" rel="noopener"}


