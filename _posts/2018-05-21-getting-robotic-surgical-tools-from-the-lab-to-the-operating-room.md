---
layout: post
title: "Getting robotic surgical tools from the lab to the operating room"
date: 2018-05-21
categories:
author: "Vanderbilt University"
tags: [Robot-assisted surgery,Surgery,Bladder cancer,Robotics,Urology,Technology]
---


Credit: Vanderbilt University  The path from university lab to commercialization is especially complex in the biotech industry. Yet Nabil Simaan, a mechanical engineering professor who specializes in designing robots to help surgeons perform operations in areas of the body that are hard to reach, does not deter easily. IREP has gone through several development stages. IREP is licensed to Titan Medical. The latest ARMA tech—a prototype robot system to remove bladder tumors—shows great promise, having proved successful in animal studies.

<hr>

[Visit Link](https://phys.org/news/2018-05-robotic-surgical-tools-lab-room.html){:target="_blank" rel="noopener"}


