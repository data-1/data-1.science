---
layout: post
title: "An introduction to the Linux boot and startup processes"
date: 2017-09-24
categories:
author: "David Both
(Correspondent)"
tags: [GNU GRUB,Systemd,Booting,File system,Master boot record,Init,BIOS,Disk partitioning,Runlevel,Linux kernel,Boot sector,Kernel (operating system),Linux startup process,Computer hardware,Technology,Computer engineering,Computer science,Computer architecture,Operating system technology,System software,Computers,Computing,Software development,Software engineering,Software]
---


BIOS POST  The first step of the Linux boot process really has nothing whatever to do with Linux. Stage 1.5  As mentioned above, stage 1.5 of GRUB must be located in the space between the boot record itself and the first partition on the disk drive. When all of the dependencies listed in the target configuration files are loaded and running, the system is running at that target level. If so, systemd used those as configuration files to start the services described by the files. The basic target provides some additional functionality by starting units that re required for the next target.

<hr>

[Visit Link](https://opensource.com/article/17/2/linux-boot-and-startup){:target="_blank" rel="noopener"}


