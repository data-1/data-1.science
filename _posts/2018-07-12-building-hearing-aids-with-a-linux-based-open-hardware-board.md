---
layout: post
title: "Building hearing aids with a Linux-based open hardware board"
date: 2018-07-12
categories:
author: "Daniel James"
tags: [Hearing aid,BeagleBoard,Bluetooth,Computer hardware,Sound card,Information and communications technology,Electrical engineering,Information Age,Computer architecture,Manufactured goods,Electronics industry,Mass media technology,Office equipment,Electronics,Computers,Computer engineering,Computing,Technology,Computer science]
---


Since Opensource.com first published the story of the GNU/Linux hearing aid research platform in 2010, there has been an explosion in the availability of miniature system boards, including the original BeagleBone in 2011 and the Raspberry Pi in 2012. Because a hearing aid does not need a screen and a small ARM board's power consumption is far less than a typical laptop's, field trials can potentially run all day. The BeagleBone Black is open hardware finding its way into research labs. The openMHA project uses an audio cape developed by the Hearing4all project, which combines three stereo codecs to provide up to six input channels. The Cape4all might be the first multi-microphone hearing aid on an open hardware platform.

<hr>

[Visit Link](https://opensource.com/article/18/7/open-hearing-aid-platform){:target="_blank" rel="noopener"}


