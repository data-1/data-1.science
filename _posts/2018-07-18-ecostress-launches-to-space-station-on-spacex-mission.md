---
layout: post
title: "ECOSTRESS launches to space station on SpaceX mission"
date: 2018-07-18
categories:
author: "Jet Propulsion Laboratory"
tags: [SpaceX Dragon,International Space Station,ECOSTRESS,SpaceX,Mobile Servicing System,Aerospace,Science,Spaceflight technology,Human spaceflight,Space science,Space program of the United States,Space vehicles,Space programs,Flight,Spacecraft,Astronautics,Outer space,Spaceflight]
---


SpaceX launches its Falcon 9 rocket and Dragon cargo craft carrying JPL's ECOSTRESS mission from Space Launch Complex 40 at Cape Canaveral Air Force Station in Florida at 5:42 a.m. EDT June 29, 2018. Credit: NASA TV  An Earth science instrument built by NASA's Jet Propulsion Laboratory in Pasadena, California, and experiments investigating cellular biology and artificial intelligence, are among the research heading to the International Space Station following Friday's launch of a NASA-contracted SpaceX Dragon spacecraft at 5:42 a.m. EDT. Dragon lifted off on a Falcon 9 rocket from Space Launch Complex 40 at Cape Canaveral Air Force Station in Florida with more than 5,900 pounds of research, equipment, cargo and supplies that will support dozens of investigations aboard the space station. ECOSTRESS' unique orbital perch aboard the space station will allow it to observe the same spot on Earth every few days at different times of day for at least a year, giving scientists the ability to track changes in plant water use over the course of a typical day, said ECOSTRESS Principal Investigator Simon Hook of JPL. Dragon is scheduled to depart the station in August and return to Earth with more than 3,800 pounds of research, hardware and crew supplies.

<hr>

[Visit Link](https://phys.org/news/2018-07-ecostress-space-station-spacex-mission.html){:target="_blank" rel="noopener"}


