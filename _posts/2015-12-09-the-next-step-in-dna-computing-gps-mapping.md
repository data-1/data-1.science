---
layout: post
title: "The next step in DNA computing: GPS mapping?"
date: 2015-12-09
categories:
author: American Chemical Society
tags: [American Chemical Society,American Association for the Advancement of Science,Computing,Science,Technology,Branches of science,Information Age]
---


Conventional silicon-based computing, which has advanced by leaps and bounds in recent decades, is pushing against its practical limits. DNA computing could help take the digital era to the next level. They describe their advance in ACS' The Journal of Physical Chemistry B.  Jian-Jun Shu and colleagues note that Moore's law, which marked its 50th anniversary in April, posited that the number of transistors on a computer chip would double every year. The researchers built a programmable DNA-based processor that performs two computing tasks at the same time. The researchers say that in addition to cost- and time-savings over other DNA-based computers, their system could help scientists understand how the brain's internal GPS works.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/acs-tns050615.php){:target="_blank" rel="noopener"}


