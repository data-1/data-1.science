---
layout: post
title: "13 must-read books to take your DevOps skills to the next level"
date: 2018-06-28
categories:
author: "Jason Hibbets
(Advisor, Red Hat)"
tags: [DevOps,Ansible (software),Leadership,Business,Computing,Technology]
---


As many of you know, continuous learning is a critical part of DevOps. It was written by three software architects and it covers a lot of interesting subjects like DevOps requirements, virtualization and cloud computing, operations, adapting systems to work well with DevOps practices, agile methods, and test-driven development. by Jennifer Davis, Ryn Daniels (recommended by Daniel Oh)  Effective DevOps is not just a technical guide but it is also a cultural and managerial guide. Practice. I appreciate it and recommend it for developers, operation engineers, and IT managers!

<hr>

[Visit Link](https://opensource.com/article/18/6/13-books-devops-practitioners){:target="_blank" rel="noopener"}


