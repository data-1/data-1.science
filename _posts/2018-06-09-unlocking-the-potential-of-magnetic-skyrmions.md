---
layout: post
title: "Unlocking the potential of magnetic skyrmions"
date: 2018-06-09
categories:
author: "Agency For Science, Technology, Research, A Star"
tags: [Skyrmion,Thin film,Technology,Materials,Materials science,Computing,Chemistry]
---


Now A*STAR researchers have developed an innovative technique for making tunable skyrmions that could help unlock their potential. We are now examining skyrmions in nanoscale devices for their potential as bits in memory applications. The platform allows us to directly control the magnetic interactions that govern skyrmion properties by simply varying the thickness of the constituent layers, and provides skyrmion configurations tailored to the specific requirements of a range of different applications, says Soumyanarayanan. The work demonstrated, for the first time, the electrical detection of ambient skyrmions, and could lead to stable and highly scalable skyrmion-based memory and computing technologies, explained Soumyanarayanan. Explore further Researchers make a breakthrough toward the next generation of memory devices  More information: Anjan Soumyanarayanan et al. Tunable room-temperature magnetic skyrmions in Ir/Fe/Co/Pt multilayers, Nature Materials (2017).

<hr>

[Visit Link](https://phys.org/news/2017-11-potential-magnetic-skyrmions.html){:target="_blank" rel="noopener"}


