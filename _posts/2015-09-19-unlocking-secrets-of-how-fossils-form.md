---
layout: post
title: "Unlocking secrets of how fossils form"
date: 2015-09-19
categories:
author: American Chemical Society 
tags: [Fossil,Scanning electron microscope,News aggregator]
---


Fossils tell amazing stories and inspire them, too -- just think of this summer's Jurassic World blockbuster. But because some of the processes that preserve fossils are not well understood, there's still more information that they could reveal. Now scientists report in ACS' journal Analytical Chemistry a new way to probe fossils to find out how these ancient remains formed in greater detail than before. When most organisms die, they biodegrade and leave little behind. But if they get trapped in sediments that harbor few bacteria and loads of dissolved minerals, they can become fossilized and preserved for millions of years.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150916112445.htm){:target="_blank" rel="noopener"}


