---
layout: post
title: "Synthetic DNA-based enzymes"
date: 2018-08-22
categories:
author: ""
tags: [Enzyme,Protein,Metalloprotein,Hydrogenase,Catalysis,Cofactor (biochemistry),Chemical reaction,Privacy,Chemical substance,Biochemistry,Nature,Physical sciences,Chemistry]
---


Anja Hemschemeier and Thomas Happe consider DNA enzymes as ecologically and economically worthwhile – and feasible. Credit: RUB, Kramer  Enzymes perform very specific functions and require little energy – which is why biocatalysts are also of interest to the chemical industry. In a review article published in the journal Nature Reviews Chemistry, Professor Thomas Happe and Associate Professor Anja Hemschemeier from the Photobiotechnology work group at Ruhr-Universität Bochum have provided a summary on what is known about the mechanisms of enzymes in nature. That's because chemists have to artificially generate the cofactor, and the synthetic construct has to interact with the enzyme's protein part in a natural manner. Literature already provides many examples for the design of artificial proteins, elaborates Anja Hemschemeier.

<hr>

[Visit Link](https://phys.org/news/2018-08-synthetic-dna-based-enzymes.html){:target="_blank" rel="noopener"}


