---
layout: post
title: "House dust mites evolved a new way to protect their genome"
date: 2018-08-02
categories:
author: "University of Michigan"
tags: [Evolution,RNA,Transposable element,Piwi,Genome,Genetics,Organism,Mite,Non-coding DNA,Biotechnology,Biology,Molecular biology,Life sciences,Biochemistry,Nucleic acids]
---


They are tiny, free-living animals that evolved from a parasitic ancestor, which in turn evolved from free-living organisms millions of years ago. They found that house dust mites do not have Piwi proteins or the associated small RNAs that most animals use to control transposable elements. Instead, dust mites have replaced the Piwi pathway with a completely different small RNA mechanism that uses small-interfering RNAs. We believe that the evolution of this novel mechanism to protect genomes from transposable elements is linked to the unusual evolution of the dust mite, said Pavel Klimov, an associate research scientist in the U-M Department of Ecology and Evolutionary Biology and a co-author of the PLOS Genetics paper. Ancient dust mites lived in bird nests for millions of years before moving into human dwellings relatively recently.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/uom-hdm020118.php){:target="_blank" rel="noopener"}


