---
layout: post
title: "Knee-deep in spider leg evolution"
date: 2015-10-08
categories:
author: Molecular Biology and Evolution (Oxford University Press)
tags: [Evolution,Spider,Gene,Patella,Biology,News aggregator,Life sciences]
---


Authors Nikola-Michael Prpic et al., in a new study appearing in the advanced online edition of Molecular Biology and Evolution, have identified the driving force behind the evolution of a leg novelty first found in spiders: knees. With eight hairy legs and seven joints on each -- that's a lot for a spider to coordinate just to take a single step. Prpic's research team honed in on a gene called dachshund (dac). The gene was first discovered in fruit flies, and humorously named for the missing leg segments and shortened legs that result from dac mutant flies. Our work shows how a gene can be duplicated and then used during evolution to invent a new morphological feature.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151006192026.htm){:target="_blank" rel="noopener"}


