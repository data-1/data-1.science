---
layout: post
title: "The US Clean Energy Revolution In Three (or Four) Charts"
date: 2015-11-17
categories:
author: Tina Casey, U.S. Department Of Energy, Written By
tags: [Renewable energy,Wind power,Electric vehicle,Fuel,Energy development,Efficient energy use,Solar energy,Clean technology,ExxonMobil,Energy and the environment,Sustainable development,Nature,Technology,Climate change mitigation,Physical quantities,Sustainable technologies,Environmental technology,Power (physics),Sustainable energy,Electric power,Energy]
---


As stated in an Energy Department press release, Energy Secretary Ernest Moniz has timed the latest update of Revolution…Now to lead into the upcoming COP21 United Nations Climate Negotiations in Paris, which will be taking place as scheduled despite the attacks on the French capital last week. Displacing these markets with low cost clean energy will do some hurt to the fossil fuel sector, and then there’s this third chart:  This chart shows electric vehicle (EV) sales taking off after 2013, concurrent with the falling cost of EV batteries, so there goes your mobility market for fossil fuels. However, the Energy Department is still very much interested in energy efficient lighting, and the clean energy report includes this information about rise of LED (light emitting diode) technology:  Improved energy efficiency in buildings correlates to an increased potential for replacing fossil fuels with clean energy and stationary energy storage, and as a ripple effect that creates more space in the electricity generating landscape for the mobile energy storage units known as electric vehicles. Speaking Of The Paris Climate Talks  Secretary Moniz announced the updated clean energy report during a talk at the Carnegie Endowment for International Peace on November 13, almost to the hour that terrorists murdered 129 innocent people in Paris, with the group ISIS (also known by other acronyms) claiming responsibility. Or follow us on Google News Have a tip for CleanTechnica, want to advertise, or want to suggest a guest for our CleanTech Talk podcast?

<hr>

[Visit Link](http://cleantechnica.com/2015/11/16/clean-energy-revolution-three-four-charts/){:target="_blank" rel="noopener"}


