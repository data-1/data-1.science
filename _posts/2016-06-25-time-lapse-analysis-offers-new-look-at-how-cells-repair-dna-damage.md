---
layout: post
title: "Time-lapse analysis offers new look at how cells repair DNA damage"
date: 2016-06-25
categories:
author: "Lawrence Berkeley National Laboratory"
tags: [DNA,DNA repair,Cell (biology),Protein,Chromosome,Ionizing radiation,DNA sequencing,Biology]
---


Now, scientists from the Department of Energy's Lawrence Berkeley National Laboratory (Berkeley Lab) are using a similar approach to study how cells repair DNA damage. Another method tracks DNA repair in thousands of cells, but it requires removing and studying subsets of cells at different time intervals. Another component of their approach is the use of human mammary epithelial cells that are modified so that DNA repair proteins, called 53BP1, are fluorescently labeled. This modification, when combined with algorithms that can analyze thousands of cells simultaneously, enables the technique to scan multiple cells and classify areas inside each one where 53BP1 proteins cluster at DNA damage sites. The Berkeley Lab scientists have used their cell-tracking and DNA-damage-classification algorithms to analyze human mammary epithelial cells beginning 24 hours before exposure to high and low doses of radiation, and continuing until 24 hours after exposure.

<hr>

[Visit Link](http://phys.org/news/2015-09-time-lapse-analysis-cells-dna.html){:target="_blank" rel="noopener"}


