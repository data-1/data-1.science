---
layout: post
title: "World population likely to surpass 11 billion in 2100"
date: 2015-12-21
categories:
author: American Statistical Association
tags: [World population,Total fertility rate,Population growth,Population,Africa,Economy,Social issues,Environmental social science]
---


SEATTLE, WA, AUGUST 10, 2015 - The world's population will increase from today's 7.3 billion people to 9.7 billion in 2050 and 11.2 billion at century's end, John R. Wilmoth, the director of the United Nations (UN) Population Division, told a session focused on demographic forecasting at the 2015 Joint Statistical Meetings (JSM 2015) today in Seattle. Wilmoth's presentation--Populations Projections by the United Nations--was made as part of an invited session titled Better Demographic Forecasts, Better Policy Decisions held here today. For instance, in Nigeria--the continent's most-populous country--the high fertility rate would result in a more than fourfold projected increase in total population by 2100--from 182 million to 752 million people. Looking more closely at the global projections, Wilmoth said Asia, with a current population of 4.4 billion, is likely to remain the most populous continent, with its population expected to peak around the middle of the century at 5.3 billion, and then to decline to around 4.9 billion people by the end of the century. In the United States, where the median age of the population is expected to increase from today's 38.0 years to 44.7 years in 2100, the PSR is projected to decline from 4.0 to 1.9.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/asa-wpl080615.php){:target="_blank" rel="noopener"}


