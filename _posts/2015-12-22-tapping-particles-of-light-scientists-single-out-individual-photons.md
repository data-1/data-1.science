---
layout: post
title: "Tapping particles of light: Scientists single out individual photons"
date: 2015-12-22
categories:
author: Weizmann Institute Of Science
tags: [Photon,Quantum optics,Light,Science,Quantum mechanics,Theoretical physics,Atomic molecular and optical physics,Physics]
---


Capturing a single photon from a pulse of light: Devices based on the Weizmann Institute model may be the backbone of future quantum communications systems. The findings of this research, which appeared this week in Nature Photonics, bear both fundamental and practical significance: Light is the workhorse of today's communication systems, and single photons are likely to be the backbone of future quantum communication systems. Once we move over to quantum communication, information will have to be encoded in single photons, says Dr. Barak Dayan, head of the Weizmann Institute Quantum Optics group. The advantage of SPRINT, says Dayan, is that it is completely passive—it does not require any control fields, just the interaction between the atom and the optical pulse. In previous research, he and his team had employed SPRINT as a switch for single photons that sent them down different pathways, effectively turning the apparatus into a photonic router.

<hr>

[Visit Link](http://phys.org/news/2015-11-particles-scientists-individual-photons.html){:target="_blank" rel="noopener"}


