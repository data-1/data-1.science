---
layout: post
title: "Human brain recalls visual features in reverse order than it detects them: Study challenges traditional hierarchy of brain decoding; offers insight into how the brain makes perceptual judgements"
date: 2017-10-10
categories:
author: "The Zuckerman Institute at Columbia University"
tags: [Perception,Brain,Information,Memory,Encoding (memory),Science,Experience,Psychology,Cognitive science,Cognition,Psychological concepts,Neuroscience,Branches of science,Interdisciplinary subfields,Cognitive psychology,Mental processes,Concepts in metaphysics]
---


But lower-level memories -- 'this candidate said this or that' -- are not as reliable. In a second task, the researchers changed the angle of the line to 53 degrees. advertisement  Previously held models of decoding predicted that in the two-line task, people would first decode the individual angle of each line (a lower-level feature) and the use that information to decode the two lines' relationship (a higher-level feature). But during decoding, when participants were asked to report the individual angle of each line, their brains used that the lines' relationship -- which angle is greater -- to estimate the two individual angles. This was striking evidence of participants employing this reverse decoding method, said Dr. Qian.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171009154946.htm){:target="_blank" rel="noopener"}


