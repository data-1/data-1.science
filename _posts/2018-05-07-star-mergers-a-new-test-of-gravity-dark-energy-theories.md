---
layout: post
title: "Star mergers: A new test of gravity, dark energy theories"
date: 2018-05-07
categories:
author: "DOE/Lawrence Berkeley National Laboratory"
tags: [Gravitational wave,Physical cosmology,Cosmic distance ladder,Dark energy,LIGO,General relativity,Neutron star,Universe,Graviton,Gravitational-wave observatory,Star,Fermi Gamma-ray Space Telescope,Lawrence Berkeley National Laboratory,Astronomy,Physics,Science,Astrophysics,Physical sciences,Space science,Cosmology,Nature]
---


The neutron star merger created gravitational waves - a squiggly distortion in the fabric of space and time, like a tossed stone sending ripples across a pond - that traveled about 130 million light-years through space, and arrived at Earth at almost the same instant as the high-energy light that jetted out from this merger. He and Jose María Ezquiaga, who was a visiting Ph.D. researcher in the Berkeley Center for Cosmological Physics, participated in this study, which was published Dec. 18 in the journal Physical Review Letters. You need both - not just gravitational waves to help test theories of gravity and dark energy, Zumalacárregui said. Another implication for this field of research is that, by collecting gravitational waves from these and possibly other cosmological events, it may be possible to use their characteristic signatures as standard sirens for measuring the universe's expansion rate. Gathering more data from events that generate both gravitational waves and light could also help resolve different measurements of the Hubble constant - a popular gauge of the universe's expansion rate.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/dbnl-sma121817.php){:target="_blank" rel="noopener"}


