---
layout: post
title: "EU top court rules new breeding techniques count as GMOs"
date: 2018-07-27
categories:
author: "Raf Casert"
tags: [Biotechnology,Genetically modified organism,Genetic engineering,Plant breeding,Technology]
---


The European Union's top court ruled Wednesday that food produced by a series of new biotechnology breeding techniques should be considered genetically modified organisms, thus falling under the EU's strict regulations of the products. In Wednesday's ruling, the European Court of Justice said that the organisms created by the new breeding techniques come, in principle, within the scope of the GMO Directive and are subject to the obligations laid down by that directive. They say that locking them in to the strict EU GMO regulations would stifle their development. Protecting the environment and health are our top priority with new genetic technology as well, Schulze added. Serious scientific organizations such as the U.S. National Academy of Sciences, the Royal Society in Britain and the German National Academy of Sciences presented evidence that the European Court of Justice discounted, he said.

<hr>

[Visit Link](https://phys.org/news/2018-07-eu-court-techniques-gmos.html){:target="_blank" rel="noopener"}


