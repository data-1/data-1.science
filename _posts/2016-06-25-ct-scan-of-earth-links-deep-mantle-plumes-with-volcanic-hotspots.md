---
layout: post
title: "CT scan of Earth links deep mantle plumes with volcanic hotspots"
date: 2016-06-25
categories:
author: "University Of California - Berkeley"
tags: [Mantle plume,Hotspot (geology),Volcano,Nature,Terrestrial planets,Planetary science,Planets of the Solar System,Lithosphere,Applied and interdisciplinary physics,Earth sciences,Structure of the Earth,Geophysics,Geology]
---


Previous attempts to image mantle plumes have detected pockets of hot rock rising in areas where plumes have been proposed, but it was unclear whether they were connected to volcanic hotspots at the surface or the roots of the plumes at the core mantle boundary 2,900 kilometers (1,800 miles) below the surface. The new, high-resolution map of the mantle—the hot rock below Earth's crust but above the planet's iron core—not only shows these connections for many hotspots on the planet, but reveals that below about 1,000 kilometers the plumes are between 600 and 1,000 kilometers across, up to five times wider than geophysicists thought. A supercomputer simulation of plumes of hot rock rising through the mantle to form volcanic island chains. Seismologists proposed some 30 years ago that stationary plumes of hot rock in the mantle occasionally punched through the crust to produce volcanoes, which, as the crust moved, generated island chains such as the Galapagos, Cape Verde and Canary islands. Millions of hours of computer time  To create a high-resolution CT of Earth, French used very accurate numerical simulations of how seismic waves travel through the mantle, and compared their predictions to the ground motion actually measured by detectors around the globe.

<hr>

[Visit Link](http://phys.org/news/2015-09-ct-scan-earth-links-deep.html){:target="_blank" rel="noopener"}


