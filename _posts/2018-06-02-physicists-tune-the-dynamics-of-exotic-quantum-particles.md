---
layout: post
title: "Physicists tune the dynamics of exotic quantum particles"
date: 2018-06-02
categories:
author: "West Virginia University"
tags: [Spin (physics),Electron,Quasiparticle,Fermion,Weyl equation,Condensed matter physics,Scientific theories,Science,Physical sciences,Applied and interdisciplinary physics,Quantum mechanics,Theoretical physics,Physics]
---


Weyl fermions, massless quasiparticles that were predicted to exist in 1930 by mathematician H. Weyl, were first detected in solid crystals by three independent research groups in 2015. They can be moved and manipulated in a controlled manner when a coupling between the crystal symmetry and electric field is exploited. A pair of Weyl fermions forms the dancing couple. We can apply a magnet and see the response with respect to the spin and things like that, Romero said. This technology promises to yield much faster and more energy efficient devices compared to the existing electronic devices.

<hr>

[Visit Link](https://phys.org/news/2017-12-physicists-tune-dynamics-exotic-quantum.html){:target="_blank" rel="noopener"}


