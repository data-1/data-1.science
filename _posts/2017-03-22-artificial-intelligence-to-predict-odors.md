---
layout: post
title: "Artificial intelligence to predict odors"
date: 2017-03-22
categories:
author: "Friedrich-Alexander-Universität Erlangen-Nürnberg"
tags: [Odor,Neuroscience,Technology,Branches of science,Cognition,Cognitive science]
---


The Volkswagen Foundation has granted the project 96,100 euros of funding for the next 18 months as part of its 'Experiment!' Researchers at FAU's Computer Chemistry Center (CCC) are working to make undesirable smells in packaging and products a thing of the past. Understanding the molecule as a sentence  'We will be spending most of our time working at the computer, not in the laboratory,' said Dr. Thilo Bauer. It's similar to processing language; the programme is supposed to understand odour molecules as a sentence in which the molecule fragments represent the words. Funding ambitious research projects  The Volkswagen Foundation uses its funding initiative 'Experiment!'

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2016-12/uoe-ait121616.php){:target="_blank" rel="noopener"}


