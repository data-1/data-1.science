---
layout: post
title: "Satellite data to map endangered monkey populations on Earth"
date: 2017-09-10
categories:
author: "University of Leicester"
tags: [Biodiversity,Environmental DNA,Species,DNA sequencing,DNA,Extinction,Nature,Natural environment,Science,Branches of science]
---


Earth Observation technology can map human settlements to predict where monkey populations have declined through hunting  'High-throughput DNA sequencing' can tell which species live in a landscape based on the environmental DNA that they leave behind in the form of saliva, urine, faeces or blood  Mosquitoes can be caught in a trap and blended into a 'biodiversity soup' to analyse the DNA in the blood of the animals they have been feeding on  A team of scientists led by the Universities of Leicester and East Anglia are leading research to protect wildlife by using satellite data to identify monkey populations that have declined through hunting. However, satellites cannot observe small animals directly. Modern genetic fingerprinting on a massive scale, called 'high-throughput DNA sequencing', can also tell which species live in a landscape based on the environmental DNA that they leave behind in the form of saliva, urine, faeces or blood. Together, the data on the animal sounds and photos, the DNA they leave behind, and satellite observations provide a wealth of biodiversity information. These targets aim to address the underlying causes of biodiversity, reduce the pressures on biodiversity, safeguard ecosystems and species, enhance the benefits from biodiversity and ecosystem services, and enable participatory planning, knowledge management and capacity building.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-06/uol-sdt062017.php){:target="_blank" rel="noopener"}


