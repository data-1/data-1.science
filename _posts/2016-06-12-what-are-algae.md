---
layout: post
title: "What Are Algae?"
date: 2016-06-12
categories:
author: "Aparna Vidyasagar"
tags: [Algae,Cyanobacteria,Autotroph,Heterotroph,Algal bloom,Plant,Photosynthesis,Eukaryotes,Earth sciences,Organisms,Biology,Nature]
---


Algae are a diverse group of aquatic organisms that have the ability to conduct photosynthesis. By virtue of these characteristics, the general term algae includes prokaryotic organisms — cyanobacteria, also known as blue-green algae — as well as eukaryotic organisms (all other algal species). Their cells are more organized. Though they are capable of conducting oxygen-producing photosynthesis and live in many of the same environments as eukaryotic algae, cyanobacteria are gram-negative bacteria, and therefore are prokaryotes. In freshwaters, cyanobacteria are the main toxin producers, though some eukaryotic algae also cause problems.

<hr>

[Visit Link](http://www.livescience.com/54979-what-are-algae.html){:target="_blank" rel="noopener"}


