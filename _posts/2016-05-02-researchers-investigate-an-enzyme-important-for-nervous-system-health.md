---
layout: post
title: "Researchers investigate an enzyme important for nervous system health"
date: 2016-05-02
categories:
author: The Scripps Research Institute
tags: [Protein,Microtubule,Tubulin,Biochemistry,Cell biology,Molecular biology,Chemistry,Structural biology,Proteins,Biotechnology,Nutrients,Macromolecules]
---


The new image of TTLL7, from a family of proteins that has been linked to neurodegenerative diseases, provides critical information about how the protein works. Credit: Image courtesy of Elizabeth Wilson-Kubalek  Scientists from The Scripps Research Institute (TSRI), working closely with researchers at the National Institutes of Health (NIH), have mapped out the structure of an important protein involved in cellular function and nervous system development. In the new study, the researchers saw for the first time how three positively charged regions of TTLL7 interact with the microtubule substrate. A Piece of a Bigger Puzzle  The researchers were able to solve the structure of TTLL7 by combining x-ray crystallography and electron microscopy (EM). The team at NIH, led by the paper's senior author Antonina Roll-Mecak, used x-ray beams to create an atomic structure of the crystallized TTLL7 protein.

<hr>

[Visit Link](http://phys.org/news350581587.html){:target="_blank" rel="noopener"}


