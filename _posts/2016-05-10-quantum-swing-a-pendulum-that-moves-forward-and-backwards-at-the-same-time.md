---
layout: post
title: "Quantum swing—a pendulum that moves forward and backwards at the same time"
date: 2016-05-10
categories:
author: Forschungsverbund Berlin E.V., Fvb
tags: [Quantum mechanics,Coherence (physics),Terahertz radiation,Oscillation,Atom,Photon,Scientific method,Chemistry,Science,Physical chemistry,Physics,Physical sciences,Theoretical physics,Applied and interdisciplinary physics]
---


A detailed theoretical analysis shows that multiple nonlinear interactions of all three THz pulses with the InSb crystal generate strong two-phonon excitations. The two parabolas (black curves) show the potential energy surfaces of harmonic oscillators representing the oscillations of atoms in a crystalline solid around their equilibrium positions, i.e., the so called phonons. Two-phonon coherence (right panel): quantum mechanics allows also for kicking off a nonclassical state with the quantum-mechanical property that the atom can be at two positions simultaneously. The velocity of the atoms behaves also nonclassical, i.e., the atom moves at the same time both to the right and to the left. Thus, a small anharmonicity is necessary to observe the emission of a coherent electric field transient as shown in Fig.

<hr>

[Visit Link](http://phys.org/news/2016-05-quantum-swinga-pendulum.html){:target="_blank" rel="noopener"}


