---
layout: post
title: "INFOGRAPHIC: Discover the cleanest cities in the world"
date: 2015-07-04
categories:
author: Kristine Lofgren
tags: []
---


There’s a movement afoot to find ways to create cleaner cities: Birmingham, England is slashing emissions like a CO2 ninja, while Oslo boasts the honor of having not a single landfill. Copenhagen is shooting to be carbon neutral by 2025 and Chicago is covering its rooftops in luscious greenery. Check out this infographic, brought to you by Hassle, to find out which cities are the cleanest and how they are making their city – and the planet – sparkle. Continue reading below Our Featured Videos  + Hassle  The article above was submitted to us by an Inhabitat reader. Want tosee story on Inhabitat?

<hr>

[Visit Link](http://inhabitat.com/infographic-discover-the-cleanest-cities-in-the-world/){:target="_blank" rel="noopener"}


