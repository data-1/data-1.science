---
layout: post
title: "Evolutionary morphology of the lizard chemosensory system"
date: 2017-09-12
categories:
author: "Baeckens, Laboratory Of Functional Morphology, Department Of Biology, University Of Antwerp, Universiteitsplein, Wilrijk, Department Of Organismic, Evolutionary Biology, Harvard University, Cambridge"
tags: []
---


Variation in vomeronasal-lingual morphology  Squamates use their vomeronasal-lingual system to sample chemicals from the surroundings47,48. Since bifid tongues may enhance tropotactic scent-trailing19 and since a thick sensory epithelium might increase the functionality, discriminatory ability, reliability and sensitivity of the VNO47,48, it is tempting to infer this co-variation between ‘sampler’ and ‘sensor’ as a functional ‘optimization’ of chemosensory design. This might be an explanation of why the average degree of tongue bifurcation of lacertids (T FS ) is 4.2 times smaller than those of varanids (following Schwenk19). While the present study was unable to find an association between foraging activity and the chemosensory design of lacertids, it did establish a link between a lizards’ investment in chemical signalling and the morphology of their vomeronasal-lingual system. Our findings showed a relationship between tongue form and VNO sensory epithelium thickness of lacertids, and their investment in secretion production: species that carry many secretory glands and are able to produce large amounts of secretion, have on average a thin layer of VNO sensory epithelium and long, broad tongues that are only marginally forked (Fig.

<hr>

[Visit Link](http://www.nature.com/articles/s41598-017-09415-7?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


