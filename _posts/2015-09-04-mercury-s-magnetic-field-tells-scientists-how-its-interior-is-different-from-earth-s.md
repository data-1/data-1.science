---
layout: post
title: "Mercury's magnetic field tells scientists how its interior is different from Earth's"
date: 2015-09-04
categories:
author: University of California - Los Angeles 
tags: [Mercury (planet),Planetary core,Planet,Earth,Magnetic field,Planetary science,Geophysics,Magnetism,Jupiter,Outer space,Nature,Astronomical objects,Science,Planets of the Solar System,Solar System,Space science,Astronomy,Physical sciences,Planets,Bodies of the Solar System]
---


Earth and Mercury are both rocky planets with iron cores, but Mercury's interior differs from Earth's in a way that explains why the planet has such a bizarre magnetic field, UCLA planetary physicists and colleagues report. In the current research, scientists led by Hao Cao, a UCLA postdoctoral scholar working in the laboratory of Christopher T. Russell, created a model to show how the dynamics of Mercury's core contribute to this unusual phenomenon. The magnetic fields of Earth, Jupiter and Saturn show very little difference between the planets' two hemispheres. Our study of Mercury's magnetic field indicates iron is snowing throughout this fluid that is powering Mercury's magnetic field. Hao and his colleagues conducted mathematical modeling of the processes that generate Mercury's magnetic field.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/uoc--mmf072914.php){:target="_blank" rel="noopener"}


