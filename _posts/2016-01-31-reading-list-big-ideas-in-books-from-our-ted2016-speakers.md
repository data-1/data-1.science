---
layout: post
title: "Reading list: Big ideas in books from our TED2016 speakers"
date: 2016-01-31
categories:
author: Cynthia Betubiza
tags: [TED (conference)]
---


Does language shape how we think and perceive the world? She speaks in Session 6. He will speak in Session 4. Khanna will speak in Session 7; you can watch his previous TED Talk. Mogahed speaks in Session 7; you can watch her previous TED Talk.

<hr>

[Visit Link](http://blog.ted.com/books-to-get-you-ready-for-ted2016/){:target="_blank" rel="noopener"}


