---
layout: post
title: "Klekowskii penguin takes size title away from emperor"
date: 2015-10-28
categories:
author: Nancy Owano
tags: [Penguin,Seymour Island]
---


The researchers reckoned that this penguin was over six feet (2 meters) and weighed over 250 pounds (115 kilograms).The species is known as Palaeeudyptes klekowskii. Detailing Acosta Hospitaleche's work, New Scientist said, Now she has uncovered two bigger bones. The researchers reckoned this heavyweight P. klekowskii could have stayed down for 40 minutes, which indicates it was able to enjoy more time to hunt fish,  Seymour Island is in the chain of islands around the tip of the Graham Land on the Antarctic Peninsula. Many fossils have been discovered on the island. Explore further Argentine experts find giant penguin fossils in Antarctica  More information: — New giant penguin bones from Antarctica: Systematic and paleobiological significance, Comptes Rendus Palevol, In Press, — New giant penguin bones from Antarctica: Systematic and paleobiological significance,, In Press, www.sciencedirect.com/science/ … ii/S163106831400058X  — Palaeeudyptes klekowskii, the best-preserved penguin skeleton from the Eocene–Oligocene of Antarctica: Taxonomic and evolutionary remarks, Geobios, www.sciencedirect.com/science/ … ii/S0016699514000291

<hr>

[Visit Link](http://phys.org/news326394977.html){:target="_blank" rel="noopener"}


