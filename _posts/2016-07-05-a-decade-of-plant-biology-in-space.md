---
layout: post
title: "A decade of plant biology in space"
date: 2016-07-05
categories:
author: ""
tags: [Root,Plant,European Space Agency,Weightlessness,Earth,Moon,Astronaut,International Space Station,Space science,Outer space,Spaceflight,Astronautics,Astronomy,Flight]
---


Every aspect of the growing environment can be regulated – temperature, atmosphere, water and light – and two centrifuges simulate gravity up to twice Earth’s level to compare how plants respond to different degrees of gravity. Gravi-2, in 2014, showed how plants use calcium to signal root growth under a range of gravity levels. An unexpected finding was the response to red and blue light was different under simulated Moon and Mars conditions. Further experiments are under way. Will they still know which way to send their roots in reduced gravity or will they go haywire?

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Research/A_decade_of_plant_biology_in_space){:target="_blank" rel="noopener"}


