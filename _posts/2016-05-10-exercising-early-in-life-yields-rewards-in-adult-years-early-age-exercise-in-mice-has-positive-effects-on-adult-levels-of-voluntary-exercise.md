---
layout: post
title: "Exercising early in life yields rewards in adult years: Early-age exercise in mice has positive effects on adult levels of voluntary exercise"
date: 2016-05-10
categories:
author: University of California - Riverside
tags: [Exercise,Eating,News aggregator,Health]
---


They found that early-age exercise in mice has positive effects on adult levels of voluntary exercise in addition to reducing body mass -- results that may have relevance for the public policy debates concerning the importance of physical education for children. The rest of the mice were given no wheel access. They found increased adult wheel running on both the high runners and the control lines of mice during the first of the two weeks of adult testing. His team of researchers found, too, that all mice that had access to early exercise were lighter in weight than their non-exercised counterparts. A grant to Garland from the National Science Foundation supported the study.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150626083915.htm){:target="_blank" rel="noopener"}


