---
layout: post
title: "Europe’s SpaceDataHighway relays first Sentinel-1 images via laser"
date: 2016-06-02
categories:
author: ""
tags: [European Data Relay System,Technology,Outer space,Spaceflight,Spacecraft,Telecommunications,Satellites,Space science,Information and communications technology]
---


Applications Europe’s SpaceDataHighway relays first Sentinel-1 images via laser 01/06/2016 11568 views 134 likes  ESA today unveiled the first Sentinel-1 satellite images sent via the European Data Relay System’s world-leading laser technology in high orbit. EDRS will allow access to time-critical data acquired around the world. EDRS will help in disaster relief as well as for operational monitoring services like maritime surveillance by relaying the data as quickly as possible to Europe, thanks to its network of ground stations like the one in Oberpfaffenhofen. La Reunion Island via laser Magali Vaissiere, ESA Director of Telecommunications and Integrated Applications, said at the Berlin Airshow today, “With today’s first link, EDRS is close to becoming operational, providing services to the Copernicus Sentinel Satellites for the European Commission. The laser communication technology used today will be able to bridge up to 75 000 km, sending data from one node over the Asia–Pacific region (EDRS-D) to another over Europe (either EDRS-A or EDRS-C).

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Telecommunications_Integrated_Applications/EDRS/Europe_s_SpaceDataHighway_relays_first_Sentinel-1_images_via_laser){:target="_blank" rel="noopener"}


