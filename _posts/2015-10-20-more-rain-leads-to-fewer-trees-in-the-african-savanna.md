---
layout: post
title: "More rain leads to fewer trees in the African savanna"
date: 2015-10-20
categories:
author: Princeton  University 
tags: [Precipitation,Savanna,Tree,Climate,Rain,Ecosystem,Water,Ecology,Poaceae,Climate change,Herbivore,Plant,Research,Biogeochemistry,Earth phenomena,Systems ecology,Physical geography,Environmental science,Natural environment,Nature,Earth sciences]
---


But this amounts to a disadvantage for trees in periods of intense rainfall, as they are comparatively less effective at utilizing the newly abundant water. The study highlights the importance of understanding the pattern and intensity of rainfall, not just the total annual precipitation, which is where most research in this area has focused, Xu said. If the intensity changes, however, that will affect the abundance of grasses and trees. We put realistic rainfall schemes into the model, then generated corresponding grass or tree abundance, and compared the numerical results with real-world observations, Xu said. For each site, the model accurately predicted the tree abundance that the researchers observed.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/pu-mrl101915.php){:target="_blank" rel="noopener"}


