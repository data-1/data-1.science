---
layout: post
title: "First-of-its-kind material for the quantum age"
date: 2018-08-02
categories:
author: "University of Central Florida"
tags: [Quantum mechanics,Condensed matter physics,Computing,Artificial intelligence,Technology,Physics,Science,Branches of science]
---


A UCF physicist has discovered a new material that has the potential to become a building block in the new era of quantum materials, those that are composed of microscopically condensed matter and expected to change our development of technology. Researchers are entering the Quantum Age, and instead of using silicon to advance technology they are finding new quantum materials, conductors that have the ability to use and store energy at the subatomic level. Assistant Professor Madhab Neupane has spent his career learning about the quantum realm and looking for these new materials, which are expected to become the foundation of the technology to develop quantum computers and long-lasting memory devices. Microsoft has invested in its Station Q, a lab dedicated solely to studying the field of topological quantum computing. Our discovery takes us one step closer to the application of quantum materials and helps us gain a deeper understanding of the interactions between various quantum phases, Neupane said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/08/180801115037.htm){:target="_blank" rel="noopener"}


