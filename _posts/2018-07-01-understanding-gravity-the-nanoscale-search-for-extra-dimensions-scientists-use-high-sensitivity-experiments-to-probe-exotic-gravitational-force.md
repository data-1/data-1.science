---
layout: post
title: "Understanding gravity: The nanoscale search for extra dimensions: Scientists use high-sensitivity experiments to probe exotic gravitational force"
date: 2018-07-01
categories:
author: "Osaka University"
tags: [Experiment,Gravity,Inverse-square law,News aggregator,Neutron,Scientific method,Physics,Science]
---


Scientists have used a pulsed slow neutron beamline to probe the deviation of the inverse square law of gravity below the wavelength of 0.1 nm. The experiment achieved the highest sensitivity for a neutron experiment demonstrated to date, and is a significant step toward determining whether the space we live in is really limited to the three dimensions most are familiar with. The net electromagnetic neutrality of neutrons means that the experiments were not influenced by the electromagnetic background that hampers other approaches to probing short distance ISL deviations. As the performance of the world's most powerful beamlines improves, we are able to significantly enhance our knowledge and understanding in step, study corresponding author Tamaki Yoshioka of Kyushu University says. In the case of gravitational interactions we have made substantial steps towards understanding the dimensions of the space around us.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/03/180326110058.htm){:target="_blank" rel="noopener"}


