---
layout: post
title: "SwRI-led CubeSat mission selected by NASA to study solar particles and space weather"
date: 2014-06-26
categories:
author: Southwest Research Institute 
tags: [CubeSat,Satellite,Space weather,NASA,Solar System,Sky,Space vehicles,Bodies of the Solar System,Spacecraft,Flight,Science,Astronomy,Astronautics,Spaceflight,Outer space,Space science]
---


NASA has selected Southwest Research Institute (SwRI) to develop CuSPP, a CubeSat mission to study Solar Particles over the Earth's Poles. During the five-year project, engineers and scientists will design, develop and integrate a CubeSat — a nano-satellite launched as a secondary payload on another satellite mission — carrying a novel miniaturized Suprathermal Ion Sensor (SIS) developed at SwRI. CuSPP can also be used to support space weather research by measuring particles that escape ahead of powerful shock waves in the solar wind. Upon successful completion, we expect CuSPP to have achieved several key goals, such as increasing the technological readiness level and reducing the risks and costs of flying a new class of SwRI science instruments for studying heliophysics — the Sun's effects on the solar system, says Dr. Mihir Desai, CuSPP principal investigator and a staff scientist in the SwRI Space Science and Engineering Division. We also expect to provide critical measurements that shed light on the origins of hazardous charged particle populations accelerated at the Sun and interplanetary space, as well as play a major role in developing reliable nano-satellites for NASA and other sponsors.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/sri-scm061814.php){:target="_blank" rel="noopener"}


