---
layout: post
title: "John Innes Centre scientists identify protein which boosts rice yield by 50 percent"
date: 2016-06-13
categories:
author: "John Innes Centre"
tags: [Biotechnology,Soil,Rice,Science]
---


In collaboration with researchers at Nanjing Agricultural University, Dr Tony Miller from the John Innes Centre has developed rice crops with an improved ability to manage their own pH levels, enabling them to take up significantly more nitrogen, iron and phosphorous from soil and increase yield by up to 54 percent. About the John Innes Centre  The John Innes Centre is an independent, international centre of excellence in plant science and microbiology.Our mission is to generate knowledge of plants and microbes through innovative research, to train scientists for the future, to apply our knowledge of nature's diversity to benefit agriculture, the environment, human health and wellbeing, and engage with policy makers and the public. We also create new approaches, technologies and resources that enable research advances and help industry to make new products. The John Innes Centre is strategically funded by the Biotechnology and Biological Sciences Research Council (BBSRC). About the BBSRC  The Biotechnology and Biological Sciences Research Council (BBSRC) invests in world-class bioscience research and training on behalf of the UK public.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/jic-jic060716.php){:target="_blank" rel="noopener"}


