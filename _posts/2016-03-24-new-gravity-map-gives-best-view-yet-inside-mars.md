---
layout: post
title: "New gravity map gives best view yet inside Mars"
date: 2016-03-24
categories:
author: NASA/Goddard Space Flight Center
tags: [Mars,Goddard Space Flight Center,Tharsis,Mars Global Surveyor,Mars Reconnaissance Orbiter,Planet,Space science,Bodies of the Solar System,Planets of the Solar System,Solar System,Planets,Astronomical objects,Astronomy,Outer space,Planetary science,Astronomical objects known since antiquity,Terrestrial planets]
---


The map was derived using Doppler and range tracking data collected by NASA's Deep Space Network from three NASA spacecraft in orbit around Mars: Mars Global Surveyor (MGS), Mars Odyssey (ODY), and the Mars Reconnaissance Orbiter (MRO). Like all planets, Mars is lumpy, which causes the gravitational pull felt by spacecraft in orbit around it to change. The new gravity solution improved the measurement of the Martian tides, which will be used by geophysicists to improve the model of Mars' interior. Changes in Martian gravity over time have been previously measured using the MGS and ODY missions to monitor the polar ice caps. ###  The research was funded by grants from NASA's Mars Reconnaissance Orbiter mission and NASA's Mars Data Analysis Program.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/nsfc-ngm032116.php){:target="_blank" rel="noopener"}


