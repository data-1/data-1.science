---
layout: post
title: "Hippocampus: In search of memory storage"
date: 2016-05-07
categories:
author: Ruhr-Universitaet-Bochum
tags: [Memory,Hippocampus anatomy,Hippocampus,Hippocampus proper,Brain,Psychology,Interdisciplinary subfields,Cognitive psychology,Cognition,Mental processes,Cognitive science,Neuroscience]
---


From here, the neurons transmit to the Cornu Ammonus region CA3, which, in turn, projects to CA1. Functional model traces the path of memory  Based on that anatomical structure, a functional model of the hippocampus has been developed in the last years and tested with the aid of computer simulations. The hippocampus initially reduces information from the cerebral cortex, turning them into small distinctive memory pieces. Their results have demonstrated that the CA1 region is probably more strongly involved in the completion of memory stimuli than has been assumed to date. Capacities for other tasks  The RUB researchers' computer model suggests that storage and decoding of information happens for the most part between the cerebral cortex and CA1.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150603083239.htm){:target="_blank" rel="noopener"}


