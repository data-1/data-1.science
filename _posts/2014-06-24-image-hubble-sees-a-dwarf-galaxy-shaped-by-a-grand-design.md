---
layout: post
title: "Image: Hubble sees a dwarf galaxy shaped by a grand design"
date: 2014-06-24
categories:
author: ""         
tags: [Pinwheel Galaxy,Galaxy,Ursa Major,Milky Way,Sky regions,Physical cosmology,Space science,Galaxies,Extragalactic astronomy,Astronomical objects,Astronomy]
---


Credit: ESA/NASA  (Phys.org) —The subject of this Hubble image is NGC 5474, a dwarf galaxy located 21 million light-years away in the constellation of Ursa Major (The Great Bear). This beautiful image was taken with Hubble's Advanced Camera for Surveys (ACS). NGC 5474 itself is part of the Messier 101 Group. The brightest galaxy within this group is the well-known spiral Pinwheel Galaxy (also known as Messier 101). Also within this group are Messier 101's galactic neighbors.

<hr>

[Visit Link](http://phys.org/news322816778.html){:target="_blank" rel="noopener"}


