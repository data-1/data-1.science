---
layout: post
title: "Electrons moving in a magnetic field exhibit strange quantum behavior"
date: 2015-05-22
categories:
author: ""    
tags: [Electron,Quantum mechanics,Magnetism,Landau quantization,Magnetic field,Physics,Quantum Hall effect,Cyclotron,Scientific theories,Electromagnetism,Physical sciences,Theoretical physics,Applied and interdisciplinary physics,Science]
---


Yet, there is much that remains unknown about exactly how electrons behave in a magnetic field. The experimental team used a transmission electron microscope to generate nanometer-sized electron vortex beams in which the electrons had a variety of quantum angular-momentum states, and then analyzed the beam propagation to reconstruct the rotational dynamics of the electrons in different Landau states. According to classical physics, the electrons should rotate uniformly at what is called the cyclotron frequency, the frequency adopted by a charged particle moving through a magnetic field. This shows that the rotational dynamics of the electrons are more complex and intriguing than was once believed. According to Franco Nori, who leads the RIKEN team, This is a very exciting finding, and it will contribute to a better understanding of the fundamental quantum features of electrons in magnetic fields, and help us to reach a better understanding of Landau states and various related physical phenomena.

<hr>

[Visit Link](http://phys.org/news326705594.html){:target="_blank" rel="noopener"}


