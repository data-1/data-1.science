---
layout: post
title: "INFOGRAPHIC: Could the Entire World Really Run on Solar Power?"
date: 2015-07-04
categories:
author: Catherine Winter
tags: []
---


It’s pretty safe to say that we all know that we have to find an alternative to fossil fuels as soon as possible, and the most viable answer to the planet’s energy needs is visible to us any time we look upward. The amount of solar energy that hits just 1 square mile of this planet over the course of a year is equal to 4 million barrels of oil, and the energy that hits the Earth in a mere 40 minutes can fuel all of humanity’s energy needs for a year. Isn’t that incredible? Check out the infographic below to learn more about all the ways that solar power is the way to go, and how we can go about making it a reality worldwide. Continue reading below Our Featured Videos  + QuickQuid  Infographic by Neomam Studios  The article above was submitted to us by an Inhabitat reader.

<hr>

[Visit Link](http://inhabitat.com/could-the-entire-world-really-run-on-solar-power/){:target="_blank" rel="noopener"}


