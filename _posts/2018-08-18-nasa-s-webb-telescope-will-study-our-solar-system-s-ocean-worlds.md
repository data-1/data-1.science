---
layout: post
title: "NASA's Webb Telescope will study our solar system's 'ocean worlds'"
date: 2018-08-18
categories:
author: "NASA/Goddard Space Flight Center"
tags: [Europa (moon),Enceladus,James Webb Space Telescope,CassiniHuygens,Ocean world,NASA,Saturn,Planets,Planets of the Solar System,Astronomical objects,Physical sciences,Bodies of the Solar System,Solar System,Science,Outer space,Planetary science,Astronomy,Space science]
---


NASA's James Webb Space Telescope will use its infrared capabilities to study the ocean worlds of Jupiter's moon Europa and Saturn's moon Enceladus, adding to observations previously made by NASA's Galileo and Cassini orbiters. Of particular interest to the scientists are the plumes of water that breach the surface of Enceladus and Europa, and that contain a mixture of water vapor and simple organic chemicals. NASA's Cassini-Huygens and Galileo missions, and NASA's Hubble Space Telescope, previously gathered evidence that these jets are the result of geologic processes heating large subsurface oceans. Villanueva and his team plan to use Webb's near-infrared camera (NIRCam) to take high-resolution imagery of Europa, which they will use to study its surface and to search for hot surface regions indicative of plume activity and active geologic processes. The James Webb Space Telescope is the scientific complement to NASA's Hubble Space Telescope.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-08/nsfc-nwt082417.php){:target="_blank" rel="noopener"}


