---
layout: post
title: "Plant engineered for more efficient photosynthesis"
date: 2016-03-26
categories:
author: Krishna Ramanujan, Cornell University
tags: [Photosynthesis,RuBisCO,Cyanobacteria,Carboxysome,Plant,Life sciences,Biotechnology,Physical sciences,Biochemistry,Chemistry,Biology]
---


Credit: Alessandro Occhialini, Rothamsted Research  (Phys.org) —A genetically engineered tobacco plant, developed with two genes from blue-green algae (cyanobacteria), holds promise for improving the yields of many food crops. Though others have tried and failed, the Cornell and Rothamsted researchers have successfully replaced the gene for a carbon-fixing enzyme called Rubisco in a tobacco plant with two genes for a cyanobacterial version of Rubisco, which works faster than the plant's original enzyme. The Rubisco in cyanobacteria fixes carbon faster, but it is more reactive with oxygen. As a result, in cyanobacteria, Rubisco is protected in special micro-compartments (called carboxysomes) that keep oxygen out and concentrate carbon dioxide for efficient photosynthesis. In previous research, Lin, Hanson and colleagues inserted blue-green algae genes in tobacco to create carboxysomes in the plant cells.

<hr>

[Visit Link](http://phys.org/news330240481.html){:target="_blank" rel="noopener"}


