---
layout: post
title: "Magnetic reconnection research sheds light on explosive phenomena in astrophysics and fusion experiments"
date: 2017-10-08
categories:
author: "John Greenwald, Princeton Plasma Physics Laboratory"
tags: [Magnetic reconnection,Plasma (physics),Physics,Energy,Astrophysics,Electromagnetism,Nature,Science,Physical sciences,Phases of matter,Plasma physics]
---


Now physicists Masaaki Yamada of the U.S. Department of Energy's (DOE) Princeton Plasma Physics Laboratory (PPPL) and Ellen Zweibel of the University of Wisconsin-Madison have provided a major perspective on four key problems in magnetic reconnection in a paper published December 7 in the British journal Proceedings of the Royal Society A. The trigger problem. How does reconnection convert magnetic energy into explosive kinetic energy? Their paper combines data gleaned from satellite sightings and the Magnetic Reconnection Experiment (MRX) at PPPL, together with theory and computer simulation, to provide a detailed view of these puzzling processes. Characterizing the plasmoid instability in a large laboratory plasma is a goal for future research, the authors write.

<hr>

[Visit Link](http://phys.org/news/2016-12-magnetic-reconnection-explosive-phenomena-astrophysics.html){:target="_blank" rel="noopener"}


