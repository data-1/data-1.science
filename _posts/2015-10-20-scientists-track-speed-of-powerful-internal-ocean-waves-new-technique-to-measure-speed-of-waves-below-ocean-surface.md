---
layout: post
title: "Scientists track speed of powerful internal ocean waves: New technique to measure speed of waves below ocean surface"
date: 2015-10-20
categories:
author: University of Miami Rosenstiel School of Marine & Atmospheric Science
tags: [News aggregator,Ocean,TerraSAR-X,Earth sciences,Science,Technology,Applied and interdisciplinary physics]
---


For the first time researchers directly measured the speed of a wave located 80 meters below the ocean's surface from a single satellite image. This is the first time internal wave velocities could be calculated from data acquired during a single overpass of a satellite, said Roland Romeiser, associate professor of ocean sciences at the UM Rosenstiel School. Using a single satellite image collected at UM's Center for Southeastern Tropical Remote Sensing (CSTARS), the research team was able to determine that a roughly 60-meter high internal wave was traveling at a speed of three miles per hour (1.4 meters per second) near Dongsha Island in the South China Sea. This technology offers new opportunities to track the speed of ocean currents or objects moving on or below the ocean surface. A research team led by Romeiser was the first to accurately measure currents from a space shuttle platform between islands off the Dutch coast and the first to make current measurements using the radar on the TerraSAR-X satellite.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151013144106.htm){:target="_blank" rel="noopener"}


