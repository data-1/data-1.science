---
layout: post
title: "Computing at the speed of light"
date: 2015-05-19
categories:
author: ""    
tags: [Computer,Integrated circuit,Photonics,Light,Silicon photonics,Supercomputer,Computing,Computer network,Central processing unit,Intel,Information and communications technology,Technology,Computer science,Computer engineering,Information Age,Computers,Electrical engineering,Electronics]
---


The device brings researchers closer to producing silicon photonic chips that compute and shuttle data with light instead of electrons. Light is the fastest thing you can use to transmit information, says Menon. But once a data stream reaches a home or office destination, the photons of light must be converted to electrons before a router or computer can handle the information. With all light, computing can eventually be millions of times faster, says Menon. The beamsplitter would be just one of a multitude of passive devices placed on a silicon chip to direct light waves in different ways.

<hr>

[Visit Link](http://www.spacedaily.com/reports/Computing_at_the_speed_of_light_999.html){:target="_blank" rel="noopener"}


