---
layout: post
title: "Magnetically-Controlled Organic Microbots Could Change Medicine in the Next Decade"
date: 2018-06-10
categories:
author: ""
tags: [Technology,Nanorobotics,Ray Kurzweil,Technological change,Chemistry,Emerging technologies]
---


Tiny Doctors  Google's chief engineer and notable futurist Ray Kurzweil has said that nanobots or microbots will flow through our bodies by 2030. While the technology could be life-changing, the prospects for these nanobots are still limited by challenges in powering the micro devices and guiding them through the body. A team of researchers led by Li Zhang, a materials scientist from the Chinese University of Hong Kong in Shatin, may have found a solution to both problems. They degrade in days or hours, depending on how much magnetic coating they have, without harming cells — except for cancer cells. Microbots — The Future of Treatments  Miniature technologies, like these synthetic algae microbots, show potential for delivering medical treatments to every corner of the the human body.

<hr>

[Visit Link](https://futurism.com/organic-microbots-change-medicine-decade/){:target="_blank" rel="noopener"}


