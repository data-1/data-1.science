---
layout: post
title: "How succulents survive without water decoded"
date: 2018-04-19
categories:
author: ""
tags: []
---


Such plants make use of enhanced form of photosynthesis  Such plants make use of enhanced form of photosynthesis  Drought-resistant plants such as cacti and succulents, make use of an enhanced form of photosynthesis to minimise water loss, scientists say. The research, published in journal The Plant Cell , could be used to help produce new crops that can thrive in previously inhospitable, hot and dry regions across the world. Photosynthesis involves taking carbon dioxide from the atmosphere to convert into sugars using sunlight. Unlike other plants, CAM plants are able to take up CO2 during the cooler night, which reduces water loss, and store captured CO2 as malic acid inside the cell, allowing its use for photosynthesis without water loss during the next day. They found that, for CAM to work properly, the cells must switch on PPCK each night driven by internal circadian clock.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/energy-and-environment/how-succulents-survive-without-water-decoded/article20556392.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


