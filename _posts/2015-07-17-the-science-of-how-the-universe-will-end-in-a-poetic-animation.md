---
layout: post
title: "The Science of How the Universe Will End, in a Poetic Animation"
date: 2015-07-17
categories:
author: Maria Popova
tags: [Philosophy]
---


For fifteen years, it has remained free and ad-free and alive thanks to patronage from readers. If this labor has made your own life more livable in the past year (or the past decade), please consider aiding its sustenance with a one-time or loyal donation. Your support makes all the difference. MONTHLY DONATION  ♥ $3 / month ♥ $5 / month ♥ $7 / month ♥ $10 / month ♥ $25 / month  ONE-TIME DONATION  You can also become a spontaneous supporter with a one-time donation in any amount:  BITCOIN DONATION  Partial to Bitcoin? You can beam some bit-love my way: 197usDS6AsL9wDKxtGM6xaWjmR5ejgqem7  Need to cancel a recurring donation?

<hr>

[Visit Link](http://www.brainpickings.org/2015/07/16/renee-hlozek-universe-ted-ed/){:target="_blank" rel="noopener"}


