---
layout: post
title: "Environmental variation and the evolution of large brains in birds"
date: 2017-03-22
categories:
author: "Sayol, Creaf, Cerdanyola Del Vallès, Catalonia, Maspons, Lapiedra, Department Of Organismic, Evolutionary Biology, Harvard University, Cambridge"
tags: []
---


Species data collection  For each species, we also extracted information of the geographical range from BirdLife International (Supplementary Fig. Consequently, we always included body size (log-transformed and extracted from the same specimens for which brains were measured) as a co-variate when we modeled brain size as a response. However, we also re-ran the analysis with relative brain size, estimated as the residuals of a log-log PGLS of brain against body size (Supplementary Fig. In the OUMV models, an additional parameter is estimated: the rate of stochastic motion around the optima (σ2), representing the amount of brain size variation around the phenotypic optimum estimated for each group. We fitted the following OU models: (1) a simple OU model with a single optimum (θ) and the same α and σ2 parameters for all selective regimes (‘OU1’ model), (2) an ‘OUM’ model with different optima, and (3) the same OUM model, but with different σ2 for each category (‘OUMV’).

<hr>

[Visit Link](http://www.nature.com/ncomms/2016/161222/ncomms13971/full/ncomms13971.html?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


