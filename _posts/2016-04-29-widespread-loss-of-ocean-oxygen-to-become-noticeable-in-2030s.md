---
layout: post
title: "Widespread loss of ocean oxygen to become noticeable in 2030s"
date: 2016-04-29
categories:
author: National Center for Atmospheric Research/University Corporation for Atmospheric Research
tags: [Ocean deoxygenation,Ocean,Climate change,Effects of climate change,Climate variability and change,Climate,Natural environment,Nature,Oceanography,Hydrography,Earth phenomena,Hydrology,Environmental science,Atmosphere,Applied and interdisciplinary physics,Earth sciences,Physical geography]
---


BOULDER -- A reduction in the amount of oxygen dissolved in the oceans due to climate change is already discernible in some parts of the world and should be evident across large regions of the oceans between 2030 and 2040, according to a new study led by the National Center for Atmospheric Research (NCAR). Warming surface waters, however, absorb less oxygen. They also determined that more widespread detection of deoxygenation caused by climate change would be possible between 2030 and 2040. Using the same model dataset, the scientists created maps of oxygen levels in the ocean, showing which waters were oxygen-rich at the same time that others were oxygen-poor. The pattern caused by climate change also became evident in the model runs around 2030, adding confidence to the conclusion that widespread deoxygenation due to climate change will become detectable around that time.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/ncfa-wlo042716.php){:target="_blank" rel="noopener"}


