---
layout: post
title: "XENON1T, the most sensitive detector on Earth searching for WIMP dark matter, releases its first result"
date: 2017-09-21
categories:
author: "Sander Breur, Purdue University"
tags: [XENON,Dark matter,Weakly interacting massive particles,Astronomy,Particle physics,Physics,Physical sciences,Science]
---


The result from a first short 30-day run shows that this detector has a new record low radioactivity level, many orders of magnitude below surrounding materials on Earth. The latest detector of the XENON family has been in science operation at the LNGS underground laboratory since autumn 2016. A new phase in the race to detect dark matter with ultra-low background massive detectors on Earth has just began with XENON1T. Explore further Green light for next-generation dark matter detector  More information: First Dark Matter Search Results from the XENON1T Experiment. First Dark Matter Search Results from the XENON1T Experiment.

<hr>

[Visit Link](https://phys.org/news/2017-05-xenon1t-sensitive-detector-earth-wimp.html){:target="_blank" rel="noopener"}


