---
layout: post
title: "Will we ever colonize Mars?"
date: 2016-05-04
categories:
author: Matt Williams
tags: [Mars,Human mission to Mars,SpaceX Mars program,Colonization of Mars,Atmosphere,Exploration of Mars,Earth,Atmosphere of Earth,Outer space,Astronomy,Spaceflight,Planetary science,Astronautics,Physical sciences,Nature,Planets of the Solar System,Space science]
---


Let's go over them one by one…  Benefits:  As already mentioned, there are many interesting similarities between Earth and Mars that make it a viable option for colonization. Over time, planting on the native soil could also help to create a breathable atmosphere. About 95% of the planet's atmosphere is carbon dioxide, which means that in addition to producing breathable air for their habitats, settlers would also not be able to go outside without a pressure suit and bottled oxygen. Credit: Bryan Versteeg/Mars One  Proposed Missions:  NASA's proposed manned mission to Mars – which is slated to take place during the 2030s using the Orion Multi-Purpose Crew Vehicle (MPCV) and the Space Launch System (SLS) – is not the only proposal to send humans to the Red Planet. There may come a day when, after generations of terraforming and numerous waves of colonists, that Mars will begin to have a viable economy as well.

<hr>

[Visit Link](http://phys.org/news352372515.html){:target="_blank" rel="noopener"}


