---
layout: post
title: "Storm hunter in position"
date: 2018-08-16
categories:
author: ""
tags: [Lightning,International Space Station,Upper-atmospheric lightning,Gamma ray,Terrestrial gamma-ray flash,Flight,Astronautics,Science,Astronomy,Spacecraft,Spaceflight,Outer space,Space science,Space vehicles]
---


Pointing straight down at Earth, the storm hunter will observe lightning and powerful electrical bursts in the atmosphere that occur above thunderstorms, the so-called transient luminous events. The storm hunter will send data over the International Space Station network beamed via communication satellites to a ground station in White Sands, USA, then on to the Space Station mission control in Houston, under the Atlantic ocean to the Columbus Control Centre near Munich, Germany, and finally to the Belgian user operations and support centre in Brussels. Setting the levels will be a matter of trial and error – setting the trigger too low will flood the network with images that are of no use, too high and some thunderstorms will not be recorded. Other sensors are included to learn more about terrestrial gamma-ray flashes, for high and low energy x-ray and gamma-ray bursts. Each element of the storm hunter will be activated in turn and tested to ensure they are working as expected.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Human_Spaceflight/Storm_hunter_in_position){:target="_blank" rel="noopener"}


