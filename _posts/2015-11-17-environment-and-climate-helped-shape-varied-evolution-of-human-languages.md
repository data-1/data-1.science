---
layout: post
title: "Environment and climate helped shape varied evolution of human languages"
date: 2015-11-17
categories:
author: Acoustical Society Of America
tags: [Language,Consonant,Syllable,Sound,Vowel,Acoustics,Ecology,Adaptation,Natural environment,Evolution,Cognition,Linguistics]
---


It's well known that gradual adaptation to the environment shaped the development of human bodies and brains, but recent work by an international group of researchers suggests that the variations in human linguistic evolution also reflect adaptations to the local ecological conditions. This supports the hypothesis that acoustic adaptation to the environment plays a role in the evolution of human languages. Maddieson said that their findings offer support for an application of the Acoustic Adaptation Hypothesis—which argues that species adapt their acoustic signals to optimize sound transmission in the environment they live in—to human languages. Morton suggested that birds in forested areas tend to sing at lower frequencies than birds living in open areas in order to enhance the effectiveness of transmission of the signal in the specific environment they live in. That could explain why languages in areas with greater tree cover tend to be less 'consonant-heavy,' Maddieson said.

<hr>

[Visit Link](http://phys.org/news/2015-11-environment-climate-varied-evolution-human.html){:target="_blank" rel="noopener"}


