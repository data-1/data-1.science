---
layout: post
title: "Dark Energy Survey reveals most accurate measurement of dark matter structure in the universe"
date: 2017-09-22
categories:
author: "Fermi National Accelerator Laboratory"
tags: [Universe,Dark matter,Dark energy,Milky Way,Dark Energy Survey,Physical cosmology,Planck (spacecraft),Science,Astronomy,Physics,Physical sciences,Space science,Astrophysics,Nature,Cosmology]
---


Map of dark matter made from gravitational lensing measurements of 26 million galaxies in the Dark Energy Survey. The new DES result (the tree, in the above metaphor) is close to forecasts made from the Planck measurements of the distant past (the seed), allowing scientists to understand more about the ways the universe has evolved over 14 billion years. The camera was built and tested at Fermilab, the lead laboratory on the Dark Energy Survey, and is mounted on the National Science Foundation's 4-meter Blanco telescope, part of the Cerro Tololo Inter-American Observatory in Chile, a division of the National Optical Astronomy Observatory. Credit: Dark Energy Survey  DES scientists used two methods to measure dark matter. The new dark matter map is 10 times the size of the one DES released in 2015 and will eventually be three times larger than it is now.

<hr>

[Visit Link](https://phys.org/news/2017-08-dark-energy-survey-reveals-accurate.html){:target="_blank" rel="noopener"}


