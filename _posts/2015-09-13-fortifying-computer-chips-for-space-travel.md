---
layout: post
title: "Fortifying computer chips for space travel"
date: 2015-09-13
categories:
author: Lawrence Berkeley National Laboratory 
tags: [Cyclotron,Microprocessor,Integrated circuit,Radiation,Particle accelerator,Computer,Electronics,Cosmic ray,Lawrence Berkeley National Laboratory,Ion,Sun,Space science,Physical sciences,Nature,Physics]
---


It's a good thing, then, that engineers know how to make a spaceship's microprocessors more robust. In the case of Orion, which last December had its first (unmanned) test flight around Earth, a number of radiation safeguards have already been put in place. As well as keeping the ion source powerful, engineers at the cyclotron are also making improvements to the beam that blasts the microprocessors during testing. High-energy particles aren't just in space, after all. More-reliable electronics in space and on Earth: brought to you by the 88-Inch Cyclotron.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150904195347.htm){:target="_blank" rel="noopener"}


