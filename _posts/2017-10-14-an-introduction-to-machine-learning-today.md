---
layout: post
title: "An introduction to machine learning today"
date: 2017-10-14
categories:
author: "Ulrich Drepper
(Red Hat)"
tags: [Machine learning,Artificial intelligence,Training validation and test data sets,Convolutional neural network,K-nearest neighbors algorithm,Mathematical optimization,Algorithm,Regression analysis,Statistical classification,Systems science,Computer science,Computational neuroscience,Branches of science,Cognitive science,Applied mathematics,Cognition,Technology,Cybernetics,Learning]
---


Let the computer do it  What these examples have in common—what the new approach to machine learning is about and why this hasn't been done before—is that the computer does a lot of work. Successfully using machine learning requires a few things:  Choosing a method/algorithm appropriate for the problem Narrowing the search to account for the (usually) huge search space for the model's parameter Evaluating the model's precision, especially when automating the search  The latter might be easy if a model is distinguishing between cat, dog, and pudding and there are just a few pictures. This is an unsupervised method and does not require any value to be computed from the input data. The input dataset is the data that's used. It might seem like intelligence when an optimization algorithm finds a novel solution, but this is not really AI.

<hr>

[Visit Link](https://opensource.com/article/17/9/introduction-machine-learning){:target="_blank" rel="noopener"}


