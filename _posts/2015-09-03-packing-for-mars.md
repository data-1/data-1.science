---
layout: post
title: "Packing for Mars"
date: 2015-09-03
categories:
author: "$author"   
tags: [ExoMars,Schiaparelli EDM,Outer space,Flight,Space exploration,Mars,Astronomy,Astronautics,Space science,Spaceflight]
---


Enabling & Support Packing for Mars 22/10/2014 4076 views 71 likes  Like surgeons in an operating room, the technicians work gowned and masked in ESA’s ultraclean microbiology laboratory, ensuring a high-tech sensor will not contaminate the Red Planet with terrestrial microbes. This temperature sensor is destined to land on Mars as part of ESA’s Schiaparelli module in late 2016. Schiaparelli will hitch a ride to the Red Planet with the ExoMars Trace Gas Orbiter, set for launch on a Russian Proton rocket at the start of 2016. Schiaparelli will also do useful science in its own right during its estimated two to eight martian days of surface life. Technicians donned sterile ‘bunny suits’ before they could have contact with the sensor, entering the 35 sq m cleanroom through an air shower.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering/Packing_for_Mars){:target="_blank" rel="noopener"}


