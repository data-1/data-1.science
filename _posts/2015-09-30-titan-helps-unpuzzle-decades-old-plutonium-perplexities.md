---
layout: post
title: "Titan helps unpuzzle decades-old plutonium perplexities"
date: 2015-09-30
categories:
author: Jeremy Rumsey, Oak Ridge National Laboratory
tags: [Electron,Neutron,Oak Ridge National Laboratory,Magnetism,Experiment,Science,Theory,Physics,Applied and interdisciplinary physics,Scientific method,Theoretical physics,Quantum mechanics,Physical sciences]
---


Through an allocation by the DOE Office of Advanced Scientific Computing Research Leadership Computing Challenge, a team of condensed matter theorists at Rutgers University, led by Professors Gabriel Kotliar and Kristjan Haule, used nearly 10 million Titan core hours to calculate the electronic and magnetic structure of plutonium using a combination of density functional theory (DFT) calculations and the leading-edge dynamical mean field theory (DMFT) technique. With SNS's Angular-Range Chopper Spectrometer, or ARCS, Janoschek and his team performed neutron scattering experiments to obtain physical confirmation to prove once and for all that plutonium's dynamical magnetism wasn't just a theory. We successfully showed that dynamical mean field theory more or less predicted what we observed, Janoschek said. It provides a natural explanation for plutonium's complex properties and in particular the large sensitivity of its volume to small changes in temperature or pressure. Jack Wells, the OLCF's Director of Science, said, This scientific breakthrough is a terrific example of what can be achieved through collaborative research between university and national laboratory scientists in the context of DOE's scientific user facilities.

<hr>

[Visit Link](http://phys.org/news/2015-09-titan-unpuzzle-decades-old-plutonium-perplexities.html){:target="_blank" rel="noopener"}


