---
layout: post
title: "Replay of Vega liftoff VV04 with IXV"
date: 2015-09-03
categories:
author: "$author"   
tags: [Intermediate eXperimental Vehicle,Vega (rocket),Spaceflight technology,Space access,Space agencies,Aerospace,Rocketry,Spacecraft,European Space Agency,Space industry,Space organizations,Space launch vehicles,Space traffic management,Space programs,Pan-European scientific organizations,Space policy of the European Union,Space exploration,Vehicles,Space policy,Rockets and missiles,Space science,European space programmes,Space vehicles,Astronautics,Outer space,Flight,Spaceflight,Space-based economy,Commercial launch service providers,Technology,Transport authorities]
---


Replay of the liftoff of Vega VV04 with ESA's Intermediate eXperimental Vehicle, IXV, launched from Kourou, French Guiana on 11 February 2015. IXV was launched 340 km into space atop a Vega rocket, VV04, from Kourou in French Guiana on 11 February 2015. After separation from Vega, IXV coasted to 412 km before beginning a punishing glide back through the atmosphere. During its autonomous flight, IXV tested the latest technologies and critical systems to extend Europe’s capability for space exploration. More about IXV: http://www.esa.int/Our_Activities/Launchers/IXV  Connect with IXV on Twitter: twitter.com/esa_ixv

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2015/02/Replay_of_Vega_liftoff_VV04_with_IXV){:target="_blank" rel="noopener"}


