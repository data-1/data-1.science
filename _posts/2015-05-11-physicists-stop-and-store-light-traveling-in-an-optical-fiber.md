---
layout: post
title: "Physicists stop and store light traveling in an optical fiber"
date: 2015-05-11
categories:
author: Universite P. Et M. Curie
tags: [Optical fiber,Light,Quantum network,Optics,Quantum memory,Physics,Technology]
---


Light propagating in a glass fiber can be halted and reemitted later on demand via its interaction with cold cesium atoms in the vicinity of an elongated area. This device provides an all-fibered memory for light, an essential ingredient for the creation of future quantum networks. By causing interaction between the traveling light and a few thousand atoms in the vicinity, they demonstrated an all-fibered memory. Storage times of up to 5 microseconds were demonstrated, corresponding to a traveling distance of 1 km if the light had not been halted. The experiment by the Paris team also showed that even light pulses containing only one photon can be stored, with a very large signal-to-noise ratio.

<hr>

[Visit Link](http://phys.org/news350548549.html){:target="_blank" rel="noopener"}


