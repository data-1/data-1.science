---
layout: post
title: "Quantum entanglement: New study predicts a quantum Goldilocks effect"
date: 2015-09-27
categories:
author: University Of Miami
tags: [Universe,Matter,Phase transition,Quantum entanglement,Light,Physical cosmology,Phase (matter),Physics,Branches of science,Science]
---


The 'just right' structure that emerges when you drive a system containing light and matter (like the universe), neither too fast nor too slow across a quantum phase transition. A new study has translated not too hot or too cold, just right to the quantum world and the generation of quantum entanglement – the binding within and between matter and light – and suggests that the universe started neither too fast nor too slow. By studying a system that couples matter and light together, like the universe itself, researchers have now found that crossing a quantum phase transition at intermediate speeds generates the richest, most complex structure. Our findings suggest that the universe was 'cooked' at just the right speeds, said Neil Johnson, professor of physics in the University of Miami College of Arts & Sciences and one of the authors of the study. The study sheds new light on how to generate, control, and manipulate quantum entanglement, since the defects contain clusters of quantum entanglement of all sizes.

<hr>

[Visit Link](http://phys.org/news/2015-09-quantum-entanglement-goldilocks-effect.html){:target="_blank" rel="noopener"}


