---
layout: post
title: "Artificial blood vessels developed in the lab can grow with the recipient"
date: 2017-03-17
categories:
author: "University of Minnesota College of Science and Engineering"
tags: [Blood vessel,Decellularization,Medical specialties,Medicine,Clinical medicine,Biology,Health sciences,Life sciences,Biotechnology,Medical treatments]
---


In a groundbreaking new study led by University of Minnesota biomedical engineers, artificial blood vessels bioengineered in the lab and implanted in young lambs are capable of growth within the recipient. When implanted in a lamb, the tube was then repopulated by the recipient's own cells allowing it to grow. This might be the first time we have an 'off-the-shelf' material that doctors can implant in a patient, and it can grow in the body, Tranquillo said. To develop the material for this study, researchers combined sheep skin cells in a gelatin-like material, called fibrin, in the form of a tube and then rhythmically pumped in nutrients necessary for cell growth using a bioreactor for up to five weeks. What's important is that when the graft was implanted in the sheep, the cells repopulated the blood vessel tube matrix, Tranquillo said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/09/160927134842.htm){:target="_blank" rel="noopener"}


