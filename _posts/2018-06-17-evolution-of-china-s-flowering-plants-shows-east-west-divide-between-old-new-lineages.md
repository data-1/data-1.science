---
layout: post
title: "Evolution of China's flowering plants shows East-West divide between old, new lineages"
date: 2018-06-17
categories:
author: "Florida Museum of Natural History"
tags: [Biodiversity,Species,Conservation biology,Plant,Herbaceous plant,Flowering plant,Flower,China,Evolution,Phylogenetic tree,Pamela S Soltis,Tree,Nature,Organisms,Earth sciences,Natural environment,Ecology]
---


In a study led by Li-Min Lu of the Chinese Academy of Sciences, researchers produced the first dated phylogeny - a family tree of organisms showing when new species appeared - for all of China's flowering plant species, or angiosperms, and mapped their distributions using 1.4 million museum specimen records. Mean species divergence times - when species first appeared - were 22-25 million years ago in the East and 15-19 million years ago in the West. China is home to about 10 percent of the world's flowering plant species, outstripping the number of angiosperm species in the U.S. by more than 3.5-fold. The researchers used species distribution data to map China's areas of genetic richness, pinpointing several eastern provinces as home to the country's greatest genetic diversity of flowering plants: Guangdong, Guanxi, Guizhou, Hainan and Yunnan. Scientists didn't realize the importance of protecting genetic diversity - and not just rare species - until the last few decades, but we couldn't measure this type of diversity in a rigorous way until the last 10-15 years.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/fmon-eoc013118.php){:target="_blank" rel="noopener"}


