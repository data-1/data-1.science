---
layout: post
title: "Eye shape reveals whether animal is predator or prey – Physics World"
date: 2016-05-24
categories:
author:  
tags: [Pupil,Predation,Eye,Ambush predator,Tiger,Animals]
---


Ambush eyes: domestic cats have vertical pupils  A link between pupil shape and the feeding behaviour of animals has been made by studying the eyes of 214 species. Their models suggest that horizontally elongated pupils in eyes located on the sides of the head give herbivorous prey animals a panoramic view of their surroundings. “Elongation in the horizontal direction effectively optimizes the amount of light entering [the eye] and makes a better image of the ground, which is horizontal. When Love and colleagues studied vertically elongated pupils, they found that such pupils offer an advantage to ambush predators – creatures that hunt by stealth and often remain still for long periods of time. Shorter animals, like domestic and wild cats, have vertical pupils to reduce that blur, but big cats – like lions and tigers – are much taller and don’t need vertical pupils, he adds.

<hr>

[Visit Link](http://physicsworld.com/cws/article/news/2015/aug/11/eye-shape-reveals-whether-animal-is-predator-or-prey){:target="_blank" rel="noopener"}


