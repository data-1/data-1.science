---
layout: post
title: "New professorship in tissue engineering links molecular engineering, marine biology"
date: 2014-07-13
categories:
author: University of Chicago 
tags: [Marine Biological Laboratory,Science,Life sciences,Technology,Biology]
---


The University of Chicago is creating a new professorship in tissue engineering to promote innovative work at the University's Institute for Molecular Engineering and the Marine Biological Laboratory, supported by a $3.5 million donation from the Millicent and Eugene Bell Foundation. This extraordinary gift from the Millicent and Eugene Bell Foundation continues the Bell family's generous support of the Marine Biological Laboratory by providing a transformative research opportunity in our affiliation with the University of Chicago, said MBL President and Director Joan Ruderman. While a professor at MIT from 1956 to 1986, Eugene Bell founded the field of tissue engineering through efforts to generate replacement tissue for treating severe burns and other injuries. I am looking forward to the prospect of advanced research and of its medical application that will result—and am extremely pleased to see Gene's humane dreams realized. Her longtime support of the MBL includes gifts totaling $8 million to help establish the Bell Center in 2010.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/uoc-npi071114.php){:target="_blank" rel="noopener"}


