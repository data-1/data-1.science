---
layout: post
title: "The phylogeny and evolutionary history of tyrannosauroid dinosaurs"
date: 2016-02-05
categories:
author: Brusatte, Stephen L., School Of Geosciences, University Of Edinburgh, Grant Institute, Edinburgh, Carr, Thomas D., Department Of Biology, Carthage College
tags: []
---


Differences between parsimony and Bayesian topologies  In comparing our parsimony and Bayesian phylogenies, the most striking finding is that the two methods produce extremely similar consensus trees. As outlined above, there are three main differences: the position of Dryptosaurus (non-tyrannosaurid in the parsimony tree, nesting with the alioramin tyrannosaurines in the Bayesian tree), the status of Daspletosaurus (the two species form a monophyletic cluster in the parsimony analysis but are a grade on the line to more derived tyrannosaurines in the Bayesian analysis) and resolution within Tyrannosaurinae (completely resolved in the parsimony analysis, one main polytomy including Lythronax and Teratophoneus in the Bayesian analysis). Given that parsimony and Bayesian analyses of our dataset return similar results, but that our new phylogeny has key differences with the Loewen et al.13 study, it appears as if data selection (character choice and scorings) are more important drivers of topological differences than is methodology. The basal tyrannosauroids Yutyrannus and Sinotyrannus are early examples of fairly large body size in the group (~8–9 meters long and 1.5 tons in mass12,31), but these taxa are not closely related to the derived latest Cretaceous tyrannosaurids, as they are part of the early-flourishing proceratosaurid radiation and not on the direct line to T. rex and close relatives. There is high Campanian diversity in North America and long ghost lineages that extend to Asian taxa, suggesting that many tyrannosauroids probably lived in the Campanian of Asia as well, but have yet to be sampled.

<hr>

[Visit Link](http://www.nature.com/srep/2016/160202/srep20252/full/srep20252.html){:target="_blank" rel="noopener"}


