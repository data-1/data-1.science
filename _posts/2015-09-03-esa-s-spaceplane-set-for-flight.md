---
layout: post
title: "ESA’s spaceplane set for flight"
date: 2015-09-03
categories:
author: "$author"   
tags: [Intermediate eXperimental Vehicle,Spaceflight technology,Space agencies,Science,Human spaceflight,Astronomy,Technology,Vehicles,Space exploration,Rocketry,Space science,Aerospace,Spacecraft,Space vehicles,Astronautics,Outer space,Flight,Spaceflight,Space programs]
---


Instead of heading north into a polar orbit – as on previous flights – Vega will head eastwards to release the spaceplane into a suborbital path reaching all the way to the Pacific Ocean. Engineers are forging ahead with the final tests on ESA’s Intermediate eXperimental Vehicle, IXV, to check that it can withstand the demanding conditions from liftoff to separation from Vega. Launched in early November, IXV will flight test the technologies and critical systems for Europe’s future automated reentry vehicles returning from low orbit. This is a first for Europe and those working in the field are keeping a close watch. The research and industrial community have the chance to use this information for progress in atmospheric reentry, oriented towards transportation systems with applications in exploration, science, Earth observation, microgravity and clean space.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Launchers/IXV/ESA_s_spaceplane_set_for_flight){:target="_blank" rel="noopener"}


