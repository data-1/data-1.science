---
layout: post
title: "World's Oldest Peach Pits Reveal Juicy Secrets"
date: 2015-12-22
categories:
author: Megan Gannon
tags: [Peach,Fossil,Homo,Radiocarbon dating,Human evolution,Fruit]
---


Fossilized peach pits discovered in China dating back more than 2.5 million years are identical to pits found in modern varieties of the fruit. The discovery indicates peaches evolved through natural selection, long before humans arrived and domesticated the fruit. Researchers discovered the fossilized peach pits at the site of a construction project in Kunming, China. Peaches are mentioned in the Book of Songs, or Shi-Jing, China's oldest known collection of poetry, with works dating from the 11th to seventh centuries B.C. Though the researchers think the prehistoric peaches could be assigned to the same peach species living today (Prunus persica), they have proposed a new species name, Prunus kunmingensis, because they are not able to reconstruct the whole plant based on the pit alone.

<hr>

[Visit Link](http://www.livescience.com/52952-oldest-peach-fossils-discovered.html){:target="_blank" rel="noopener"}


