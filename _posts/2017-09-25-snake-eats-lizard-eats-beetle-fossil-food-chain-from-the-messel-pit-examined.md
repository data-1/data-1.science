---
layout: post
title: "Snake eats lizard eats beetle: Fossil food chain from the Messel Pit examined"
date: 2017-09-25
categories:
author: "Senckenberg Research Institute and Natural History Museum"
tags: [Messel pit,Geiseltaliellus,Snake,Lizard,News aggregator,Animals,Taxa]
---


The discovery of the approximately 48-million-year-old tripartite fossil food chain is unique for Messel; worldwide, only one single comparable piece exists. In the year 2009, we were able to recover a plate from the pit that shows an almost fully preserved snake, says Dr. Krister Smith of the Department for Messel Research at the Senckenberg Research Institute in Frankfurt, and he continues, And as if this was not enough, we discovered a fossilized lizard inside the snake, which in turn contained a fossilized beetle in its innards! Fossil food chains are extremely rarely preserved; due to the excellent level of preservation at the fossil site, leaves and grapes from the stomach of a prehistoric horse, pollen grains in a bird's intestinal tract and remains of insects in fossilized fish excrements had previously been discovered at Messel. To this day, only one other example of such fossil preservation has been found worldwide -- in a 280-million-year-old shark. Smith comments, The fossil snake is a member of Palaeophython fischeri; the lizard belongs to Geiseltaliellus maarius, which has only been found at Messel to date.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/09/160907082052.htm){:target="_blank" rel="noopener"}


