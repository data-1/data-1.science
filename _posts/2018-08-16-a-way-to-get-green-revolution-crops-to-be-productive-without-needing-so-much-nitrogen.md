---
layout: post
title: "A way to get green revolution crops to be productive without needing so much nitrogen"
date: 2018-08-16
categories:
author: "Bob Yirka"
tags: [Green Revolution,Rice,Fertilizer,Nitrogen assimilation,Food security,Plant breeding,Agriculture,Primary sector of the economy]
---


In their paper published in the journal Nature, the group describes their research efforts and the results they found when planting newly developed plant varieties. Unfortunately, DELLA proteins have also been found to be the cause of inefficient nitrogen use in the same plants—as a result, farmers used more of it to increase yields. The team then planted the varieties they had engineered and found that they required less nitrogen to produce the same yields—and they were just as stunted. DOI: 10.1038/s41586-018-0415-5  Abstract  Enhancing global food security by increasing the productivity of green revolution varieties of cereals risks increasing the collateral environmental damage produced by inorganic nitrogen fertilizers. Modulation of plant growth and metabolic co-regulation thus enables novel breeding strategies for future sustainable food security and a new green revolution.

<hr>

[Visit Link](https://phys.org/news/2018-08-green-revolution-crops-productive-nitrogen.html){:target="_blank" rel="noopener"}


