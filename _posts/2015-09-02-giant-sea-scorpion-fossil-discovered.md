---
layout: post
title: "Giant 'sea scorpion' fossil discovered"
date: 2015-09-02
categories:
author: BMC (BioMed Central) 
tags: [Pentecopterus,BioMed Central,Eurypterid]
---


The fossil of a previously unknown species of 'sea scorpion', measuring over 1.5 meters long, has been discovered in Iowa, USA, and described in the open-access journal BMC Evolutionary Biology  The fossil of a previously unknown species of 'sea scorpion', measuring over 1.5 meters long, has been discovered in Iowa, USA, and described in the open access journal BMC Evolutionary Biology. The three rearmost pairs of limbs are shorter than the front pairs, suggesting that Pentecopterus may have walked on six legs rather than eight. These form arrangements similar to swimming crabs, where they function to expand the surface area of the paddle during swimming. Research article  The oldest described eurypterid: a giant Middle Ordovician (Darriwilian) megalograptid from the Winneshiek Lagerstätte of Iowa  James C. Lamsdell, Derek E. G. Briggs, Huaibao P. Liu, Brian J. Witzke and Robert M. McKay  BMC Evolutionary Biology 2015  doi 10.1186/s12862-015-0443-9  For an embargoed copy of the research article, please contact Joel.Winston@biomedcentral.com  After embargo, article available at journal website here: http://dx.doi.org/10.1186/s12862-015-0443-9  Please name the journal in any story you write. BioMed Central is part of Springer Science+Business Media, a leading global publisher in the STM sector.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/bc-gs082715.php){:target="_blank" rel="noopener"}


