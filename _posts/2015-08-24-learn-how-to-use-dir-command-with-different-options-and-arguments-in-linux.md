---
layout: post
title: "Learn How to Use ‘dir’ Command with Different Options and Arguments in Linux"
date: 2015-08-24
categories:
author: Aaron Kili
tags: [Computer file,File system,Inode,Dir (command),Data management,Information technology,Software,Data,Computer architecture,Computing,Computers,Computer data,Information technology management,Technology,Operating system technology,Utility software,Storage software,Information retrieval,System software,Software engineering,Computer data storage,Information science,Software development,Information Age]
---


# dir [OPTION] [FILE]  dir Command Usage with Examples  Simple output of the dir command  # dir /  Output of the dir command with the /etc directory file is as follows. As you can see from the output not all files in the /etc directory are listed. # dir # dir -1  View all files in a directory including hidden files  To list all files in a directory including . In the output below, the option -d lists entries for the /etc directory. The output below shows a sorted list of files according to their sizes by using the -S option.

<hr>

[Visit Link](http://www.tecmint.com/linux-dir-command-usage-with-examples/){:target="_blank" rel="noopener"}


