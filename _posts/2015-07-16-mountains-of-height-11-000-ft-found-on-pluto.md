---
layout: post
title: "Mountains of height 11,000 ft found on Pluto"
date: 2015-07-16
categories:
author: ""   
tags: []
---


A new close-up image of a region near Pluto’s equator reveals a range of youthful mountain, in a picture released by NASA in Laurel, Maryland July 15,  NASA’s New Horizons probe discovered a stunning mountain range on Pluto, with peaks jutting as high as 11,000 feet above the surface. The data suggests that the Pluto’s surface was formed no more than 100 million years ago, a mere youngster in a 4.56-billion-year-old solar system. It also means that the close-up region, which covers about one percent of Pluto’s surface, may still be geologically active today. The probe, now heading deeper into the mysterious Kuiper Belt beyond our solar system, also clicked a new, youthful view of Pluto’s largest moon Charon. A new sneak-peak image of Hydra is the first to reveal its apparent irregular shape and its size, estimated to be about 43-33 km.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/science/new-horizon-images-of-pluto-shows-mountains-of-height-11000-ft-found/article7428833.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


