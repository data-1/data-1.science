---
layout: post
title: "Seeing Earth as an exoplanet: What signs of life are visible?"
date: 2015-10-05
categories:
author: Elizabeth Howell, Astrobio.Net
tags: [Moon,Exoplanet,Solar eclipse,European Southern Observatory,Sun,Shadow,Atmosphere of Earth,Lunar eclipse,Earth,Extraterrestrial life,Astrobiology,Atmosphere,Telescope,Astronomical transit,Outer space,Astronomy,Space science,Physical sciences,Nature,Planetary science,Astronomical objects]
---


NASA's Kepler space telescope is among a fleet of telescopes and spacecraft searching for rocky planets similar to our own. Shadow glance  Observations took place during a total lunar eclipse on Dec. 10, 2011. Credit: ESO  Ozone on other planets  If we were to look at Earth as an exoplanet, could the nitrogen dioxide be interpreted as a sign of pollution, of microbial life or of a volcano? Ozone might also be visible. One thing that future telescopes could look for is the signature of ozone in an Earth-like planet’s atmosphere.

<hr>

[Visit Link](http://phys.org/news325144601.html){:target="_blank" rel="noopener"}


