---
layout: post
title: "UK To Build World’s Largest Offshore Wind Farm, Again"
date: 2015-09-12
categories:
author: Bloomberg News Editors, Copyright Bloomberg, Root, --Ppa-Color-Scheme, --Ppa-Color-Scheme-Active
tags: [Dogger Bank Wind Farm,Wind power,Electricity,Climate change mitigation,Electrical engineering,Energy,Renewable energy,Sustainable energy,Electric power,Renewable resources,Power (physics),Nature,Sustainable technologies,Physical quantities,Energy and the environment,Renewable electricity,Sustainable development]
---


The two 1.2-gigawatt wind farms, called Dogger Bank Teesside A&B, total almost four times the capacity of the largest operational project. They won development permission from the Department of Energy and Climate Change, according to a statement e-mailed Wednesday by the Planning Inspectorate. The 2.4-gigawatt project in the North Sea matches the size of the Dogger Bank Creyke Beck development also by Forewind, which won planning consent in February. Each of the two farms at Teesside A&B will cost 3 billion to 4 billion pounds, according to Sue Vincent, a spokeswoman for the project. Britain has more installed offshore wind capacity than the rest of the world put together, according to Global Wind Energy Council figures.

<hr>

[Visit Link](http://www.renewableenergyworld.com/articles/2015/08/uk-to-build-world-s-largest-offshore-wind-farm.html){:target="_blank" rel="noopener"}


