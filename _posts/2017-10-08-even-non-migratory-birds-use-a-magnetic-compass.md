---
layout: post
title: "Even non-migratory birds use a magnetic compass"
date: 2017-10-08
categories:
author: "Lund University"
tags: [Compass,Bird migration,Magnetism,News aggregator,Bird]
---


A new study from Lund University in Sweden shows that non-migratory birds also are able to use a built-in compass to orient themselves using the Earth's magnetic field. The researchers behind the current study have received help from a group of zebra finches to study the magnetic compass of what are known as resident birds, that is, species that do not migrate according to the season. Zebra finches are popular pet birds in many homes. Our results show that the magnetic compass is more of a general mechanism found in both migratory birds and resident birds. It seems that although zebra finches do not undertake extensive migration, they still might be able to use the magnetic compass for local navigation, says Atticus Pinzón-Rodríguez.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/05/170518104124.htm){:target="_blank" rel="noopener"}


