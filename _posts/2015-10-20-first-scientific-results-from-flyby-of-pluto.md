---
layout: post
title: "First scientific results from flyby of Pluto"
date: 2015-10-20
categories:
author: American Association for the Advancement of Science (AAAS) 
tags: [Pluto,Planemos,Flight,Planets,Spaceflight,Resonant trans-Neptunian objects,Kuiper belt objects,Astronautics,Science,Solar System,Planetary science,Bodies of the Solar System,Plutinos,Dwarf planets,Outer space,Astronomy,Space science,Trans-Neptunian objects,Local Interstellar Cloud,Discoveries,Substellar objects,Astronomical objects,Missions to minor planets,Planets of the Solar System,Discovery and exploration of the Solar System,Space exploration]
---


The surface of Pluto is marked by plains, troughs and peaks that appear to have been carved out by geological processes that have been active for a very long period and continue to the present, a new study says. The study represents the first published results from the flyby of the Pluto-Charon system earlier this year. As this data arrives on Earth, scientists process and study it. Here, Alan Stern and colleagues overview some of the first results from this effort. Charon has no detectable atmosphere, the data reveal.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/aaft-fsr101315.php){:target="_blank" rel="noopener"}


