---
layout: post
title: "Habitat loss is the top threat to Australia's species"
date: 2017-10-17
categories:
author: "Brendan Wintle And Sarah Bekessy, The Conversation"
tags: [Habitat destruction,Deforestation,Extinction,Biodiversity,Habitat,Conservation biology,Systems ecology,Environmental issues,Environmental social science,Nature,Biogeochemistry,Environmental protection,Environmental conservation,Nature conservation,Ecology,Natural environment]
---


But does habitat loss through land clearing still top the list? Land clearing threat  According to an analysis of data from the International Union for the Conservation of Nature (IUCN), habitat loss is the number-one threat to biodiversity worldwide. Many more species are affected by processes such as logging and land clearing for agriculture and housing than by invasive species, disease or other threats. Credit: IUCN, Author provided  Habitat loss is the top threat here in Australia too, an assessment that is backed up by the federal government's State of the Environment Reports in 2011 and 2016. It's all about habitat loss, because habitat loss makes all other threats more acute.

<hr>

[Visit Link](https://phys.org/news/2017-10-habitat-loss-threat-australia-species.html){:target="_blank" rel="noopener"}


