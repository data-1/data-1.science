---
layout: post
title: "A photosynthetic engine for artificial cells"
date: 2018-06-02
categories:
author: "Harvard John A. Paulson School of Engineering and Applied Sciences"
tags: [Cell (biology),Cell membrane,Biology,Protein,Actin,Adenosine triphosphate,Metabolism,Photosynthesis,Organelle,Biotechnology,Molecular biology,Life sciences,Chemistry,Biochemistry,Cell biology]
---


The mechanisms we have demonstrated should be the first step in the development of multiple regulatory networks for artificial cells that can exhibit homeostasis and complex cellular behaviors, said Kwanwoo Shin, Director of the Institute of Biological Interfaces and Professor in the Chemistry Department at Sogang University, and co-principle investigator of the project. The proteins were embedded in a simple lipid membrane, along with enzymes that generate adenosine triphosphate (ATP), the essential energy carries of cells. When the membrane is illuminated with red light, a photosynthetic chemical reaction occurs, producing ATP. Being able to control and tune the production of actin allows researchers to control the shape of cell membranes and may provide a way to engineer mobile cells. This bottom-up approach could be used to build other artificial organelles, such as endoplasmic reticulum or a nucleus-like system and could be the first step towards artificial cell-like systems that can mimic the complex behaviors of biological cells.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/hjap-ape052918.php){:target="_blank" rel="noopener"}


