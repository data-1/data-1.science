---
layout: post
title: "Tiny chip mimics brain, delivers supercomputer speed"
date: 2015-10-28
categories:
author: Rob Lever
tags: [Cognitive computer,Computer,Integrated circuit,Technology,Computing,Computer science,Branches of science,Neuroscience,Cognitive science,Information Age,Computers]
---


It accomplishes this task by using a huge network of neurons and synapses, similar to how the human brain functions by using information gathered from the body's sensory organs. Sensor becomes the computer  Infographic: A brain-inspired chip to transform mobility and Internet of Things through sensory perception. Credit: IBM  This can allow a chip installed in a car or smartphone to perform supercomputer calculations in real time without connecting to the cloud or other network. Similarly, a mobile phone can take smells or visual information and interpret them in real time, without the need for a network connection. But they wrote that TrueNorth can deliver from 46 billion to 400 billion synaptic calculations per second per watt of energy.

<hr>

[Visit Link](http://phys.org/news326639392.html){:target="_blank" rel="noopener"}


