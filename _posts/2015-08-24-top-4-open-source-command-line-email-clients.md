---
layout: post
title: "Top 4 open source command-line email clients"
date: 2015-08-24
categories:
author: "Jason B"
tags: [Email client,Mutt (email client),Internet Message Access Protocol,Software development,Software,Technology,Software engineering,Computer science,Computing,System software,Computers,Computer engineering,Digital media,Information technology management,Internet,Computer architecture,Computer-mediated communication,Network service]
---


And for Linux power users who live and die by the command line, leaving the shell to use a traditional desktop or web based email client just doesn't cut it. Mutt  Many terminal enthusiasts may already have heard of or even be familiar with Mutt and Alpine, which have both been on the scene for many years. Let's first take a look at Mutt. That's okay for many Mutt users, though, who are comfortable with the interface and adhere to the project's slogan: All mail clients suck. It doesn't actually send or receive email messages on its own, and the code which enables Notmuch's super-fast searching is actually designed as a separate library which the program can call.

<hr>

[Visit Link](http://opensource.com/life/15/8/top-4-open-source-command-line-email-clients){:target="_blank" rel="noopener"}


