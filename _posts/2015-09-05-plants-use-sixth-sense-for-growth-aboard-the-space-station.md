---
layout: post
title: "Plants use sixth sense for growth aboard the Space Station"
date: 2015-09-05
categories:
author: "$author" 
tags: [Arabidopsis thaliana,Gravity,Plant,Micro-g environment,Sense]
---


The research team seeks to determine how plants sense their growth direction without gravity. In the Plant Gravity Sensing study, scientists examine whether the mechanisms of the plant that determine its growth direction -- the gravity sensor -- form in the absence of gravity. The research team does this to determine if the plants sense changes in gravitational acceleration and adapt the levels of calcium in their cells. We may design plants that respond to gravity vector changes more efficiently than wild ones, said Tatsumi. It makes sense why researchers are interested in thale cress and what it may reveal off the Earth for the Earth.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/nsc-pus040615.php){:target="_blank" rel="noopener"}


