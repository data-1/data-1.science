---
layout: post
title: "How the quantum Zeno effect impacts Schroedinger's cat"
date: 2017-09-20
categories:
author: "Washington University In St. Louis"
tags: [Schrdingers cat,Physics,Quantum mechanics,Theoretical physics,Science,Scientific theories,Applied and interdisciplinary physics]
---


Credit: Washington University in St. Louis  You've probably heard about Schrödinger's cat, which famously is trapped in a box with a mechanism that is activated if a radioactive atom decays, releasing radiation. The delay is known as the quantum Zeno effect and the acceleration as the quantum anti-Zeno effect. What is measurement, anyway? Would the atom still exhibit the Zeno and anti-Zeno effects? To test the role of measurement in the Zeno effects, they devised a new type of measurement interaction that disturbs the atom but learns nothing about its state, which they call a quasimeasurement.

<hr>

[Visit Link](https://phys.org/news/2017-06-quantum-zeno-effect-impacts-schroedinger.html){:target="_blank" rel="noopener"}


