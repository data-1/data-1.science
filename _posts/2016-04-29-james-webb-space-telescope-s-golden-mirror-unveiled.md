---
layout: post
title: "James Webb Space Telescope's golden mirror unveiled"
date: 2016-04-29
categories:
author: NASA/Goddard Space Flight Center
tags: [James Webb Space Telescope,Goddard Space Flight Center,Mirror,Astronomy,Spaceflight,Astronomical objects,Observational astronomy,Science,Physical sciences,Space science,Outer space]
---


The 18 mirrors that make up the primary mirror were individually protected with a black covers when they were assembled on the telescope structure. Currently, engineers are busy assembling and testing the other pieces of the telescope. To ensure the mirror is both strong and light, the team made the mirrors out of beryllium. This widely anticipated telescope will soon go through many rigorous tests to ensure it survives its launch into space. The James Webb Space Telescope is the scientific successor to NASA's Hubble Space Telescope.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/nsfc-jws042716.php){:target="_blank" rel="noopener"}


