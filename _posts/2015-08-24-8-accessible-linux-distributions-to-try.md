---
layout: post
title: "8 accessible Linux distributions to try"
date: 2015-08-24
categories:
author: "Kendell Clark"
tags: [Linux distribution,Ubuntu,Booting,Fedora Linux,Linux,Optical disc image,Arch Linux,Desktop environment,Unity (user interface),Trisquel,Computer keyboard,Software,Computing,Technology,Software development,Computers,Computer science,Computer architecture,Digital media,Software engineering,System software,Operating system families,Computer engineering]
---


In such a distribution, a disabled user has to both know what software they need and the commands to install it, which are often different depending on the distribution. Installing Linux  Burning and booting your Linux distribution  These are generic instructions for downloading, burning, and booting a Linux distribution. Download a distro  The first step is to download the Linux distribution you want to try. If your computer responds with a prompt telling you that it doesn't know what to do with the file, then you'll need to download and install software capable of burning iso files. To enable accessibility and the Orca screen reader on the graphical image, press and hold down the ALT and Command keys, then press S. To enable accessibility if you downloaded the network install image, press S at the boot screen of the image and press Enter.

<hr>

[Visit Link](http://opensource.com/life/15/8/accessibility-linux-blind-disabled){:target="_blank" rel="noopener"}


