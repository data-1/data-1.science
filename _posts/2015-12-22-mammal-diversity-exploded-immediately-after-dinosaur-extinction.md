---
layout: post
title: "Mammal diversity exploded immediately after dinosaur extinction"
date: 2015-12-22
categories:
author: University College London
tags: [Mammal,Dinosaur,Evolution,Placentalia,Extinction,CretaceousPaleogene extinction event,Biodiversity,Paleocene,Evolution of mammals,Species,Anjali Goswami,Life,Extinction event,Biology,Nature]
---


New analysis of the fossil record shows that placental mammals, the group that today includes nearly 5000 species including humans, became more varied in anatomy during the Paleocene epoch - the 10 million years immediately following the event. Through recent work by the same team at UCL, this issue was resolved by creating a new tree of life for placental mammals, including these early forms, which was described in a study published in Biological Reviews yesterday. First author of both papers, Dr Thomas Halliday (UCL Earth Sciences and Genetics, Evolution & Environment), said: The mass extinction that wiped out the dinosaurs 66 million years ago is traditionally acknowledged as the start of the 'Age of Mammals' because several types of mammal appear for the first time immediately afterwards. Many recent studies suggest that little changed in mammal evolution during the Paleocene but these analyses don't include fossils from that time. The team are now investigating rates of evolution in these mammals, as well as looking at body size more specifically.

<hr>

[Visit Link](http://phys.org/news/2015-12-mammal-diversity-immediately-dinosaur-extinction.html){:target="_blank" rel="noopener"}


