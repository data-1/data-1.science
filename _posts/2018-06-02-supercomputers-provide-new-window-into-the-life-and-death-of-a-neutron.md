---
layout: post
title: "Supercomputers provide new window into the life and death of a neutron"
date: 2018-06-02
categories:
author: "DOE/Lawrence Berkeley National Laboratory"
tags: [Neutron,Proton,Quantum chromodynamics,Particle physics,Standard Model,Quark,Nucleon,Beta decay,Parity (physics),Radioactive decay,Nuclear physics,Physics,Physical sciences,Theoretical physics,Quantum field theory,Science]
---


Now, a team led by scientists in the Nuclear Science Division at the Department of Energy's Lawrence Berkeley National Laboratory (Berkeley Lab) has enlisted powerful supercomputers to calculate a quantity known as the nucleon axial coupling, or gA - which is central to our understanding of a neutron's lifetime - with an unprecedented precision. And because nature breaks this symmetry, the value of gA can only be determined through experimental measurements or theoretical predictions with lattice QCD. This is because lattice QCD calculations are complicated by exceptionally noisy statistical results that had thwarted major progress in reducing uncertainties in previous gA calculations. But the calculations aren't yet precise enough to determine if new physics have been hiding in the gA and neutron lifetime measurements. The work was supported by Laboratory Directed Research and Development programs at Berkeley Lab, the U.S. Department of Energy's Office of Science, the Nuclear Physics Double Beta Decay Topical Collaboration, the DOE Early Career Award Program, the NVIDIA Corporation, the Joint Sino-German Research Projects of the German Research Foundation and National Natural Science Foundation of China, RIKEN in Japan, the Leverhulme Trust, the National Science Foundation's Kavli Institute for Theoretical Physics, DOE's Innovative and Novel Computational Impact on Theory and Experiment (INCITE) program, and the Lawrence Livermore National Laboratory Multiprogrammatic and Institutional Computing program through a Tier 1 Grand Challenge award.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/dbnl-spn052918.php){:target="_blank" rel="noopener"}


