---
layout: post
title: "Whales and dolphins have rich 'human-like' cultures and societies"
date: 2017-10-17
categories:
author: University of Manchester
tags: [Cetacea,Dolphin,Human,Brain,Animals,Behavioural sciences,Cognitive science,Science,Zoology,Branches of science]
---


The study is first of its kind to create a large dataset of cetacean brain size and social behaviours. We know whales and dolphins also have exceptionally large and anatomically sophisticated brains and, therefore, have created a similar marine based culture. The SBH and CBH are evolutionary theories originally developed to explain large brains in primates and land mammals. Dr Kieran Fox, a neuroscientist at Stanford University, added: Cetaceans have many complex social behaviours that are similar to humans and other primates. They, however, have different brain structures from us, leading some researchers to argue that whales and dolphins could not achieve higher cognitive and social skills.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uom-wad101217.php){:target="_blank" rel="noopener"}


