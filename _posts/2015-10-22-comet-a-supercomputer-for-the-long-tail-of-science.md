---
layout: post
title: "Comet: A supercomputer for the 'long tail' of science"
date: 2015-10-22
categories:
author: National Science Foundation 
tags: [San Diego Supercomputer Center,TeraGrid,Science,Research,Technology]
---


The San Diego Supercomputer Center (SDSC) at the University of California, San Diego this week formally launched Comet, a new petascale supercomputer designed to transform scientific research by expanding computational access among a larger number of researchers and across a wider range of domains. The launch of Comet marks yet another stage in SDSC's leadership in the national cyberinfrastructure ecosystem, said Jim Kurose, assistant director of the National Science Foundation for Computer and Information Science and Engineering (CISE), during remarks Oct. 14 at the SDSC event. SDSC has been at the forefront of cyberinfrastructure since the center's beginning in 1985, as one of NSF's original supercomputer centers, Kurose said. How SDSC's Comet Supercomputer Serves Science and Society  Comet is configured to help transform advanced computing by expanding access and capacity not only among research domains that typically rely on HPC--such as chemistry and biophysics--but among domains which are relatively new to supercomputers, such as genomics, finance and the social sciences. Dell compute nodes using next-generation Intel Xeon processors, 27 racks of compute nodes totaling 1,944 nodes or 46,656 cores.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/nsf-cas102015.php){:target="_blank" rel="noopener"}


