---
layout: post
title: "Looking back at the Mir space station"
date: 2016-05-08
categories:
author: Matt Williams
tags: [Mir,International Space Station,Salyut programme,Spacecraft,Astronaut,ShuttleMir program,Progress (spacecraft),Roscosmos,Docking and berthing of spacecraft,NASA,Life in space,Spaceflight technology,Astronautics,Outer space,Human spaceflight,Space program of Russia,Rocketry,Space program of the Soviet Union,Aerospace,Space science,Crewed space program of the Soviet Union,Space industry,Space stations,Space programs,Space exploration,Space-based economy,Crewed spacecraft,Space vehicles,Flight,Human spaceflight programs,Spaceflight]
---


The Mir space station in orbit, photographed by Atlantis STS-71 in 1995. Credit: NASA  The Mir Space Station was Russia's greatest space station, and the first modular space station to be assembled in orbit. At 13.1 meters (43 feet) long, the core module of the station was the main area where the cosmonauts and astronauts did their work. The Mir Space Station and Earth limb observed from the Orbiter Endeavour during NASA’s STS-89 mission in 1998. Credit: NASA  In addition to solar arrays and a docking port, the station had several facilities for orbital science. Meanwhile, the Shuttle–Mir Program was a collaborative space program between Russia and the United States, and involved American Space Shuttles visiting the space station, Russian cosmonauts flying on the shuttle, and an American astronaut flying aboard a Soyuz spacecraft to engage in long-duration expeditions aboard Mir. A view of the US Space Shuttle Atlantis and the Russian Space Station Mir during STS-71 as seen by the crew of Mir EO-19 in Soyuz TM-21.

<hr>

[Visit Link](http://phys.org/news353832556.html){:target="_blank" rel="noopener"}


