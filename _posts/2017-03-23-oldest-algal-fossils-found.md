---
layout: post
title: "Oldest algal fossils found"
date: 2017-03-23
categories:
author: ""
tags: []
---


Credit: S. Bengtson et al./PLoS Biol. (CC BY 4.0)  Fossils of the earliest algae — which are closely related to the ancestors of modern plants — are rare and, until now, the most ancient specimen was around 1.2 billion years old. Stefan Bengtson at the Swedish Museum of Natural History in Stockholm and his colleagues studied fossils from Chitrakoot in India, and found two types of multicellular colony dating to 1.6 billion years ago. One is a thread-like form (Rafatazmia chitrakootensis, pictured), whereas the other is lobe-shaped (Ramathallus lobatus). Using 3D X-ray microscopy, the team found that the colonies contained structures characteristic of red algae, including some that may have been used in photosynthesis.

<hr>

[Visit Link](http://www.nature.com/nature/journal/v543/n7646/full/543467d.html?WT.feed_name=subjects_evolution){:target="_blank" rel="noopener"}


