---
layout: post
title: "IBM Just Announced a 50-Qubit Quantum Computer"
date: 2018-04-26
categories:
author: ""
tags: [Computing,Quantum computing,Computer,Qubit,D-Wave Systems,Quantum information science,Theoretical computer science,Applied mathematics,Quantum mechanics,Technology,Computer science,Information Age,Computers,Branches of science]
---


We are really proud of this; it’s a big frickin’ deal,” IBM's director for AI and quantum computing Dario Gil, who made Friday's announcement, told the MIT Technology Review. A Step Closer  IBM has been making significant advances in quantum computing ever since their researchers helped to create the field of quantum information processing. IBM is set on making their quantum computer work, and they're expected to announce an upgrade to their quantum cloud software today. “We’re at world record pace. But we’ve got to make sure non-physicists can use this,” Gil told the MIT Tech Review.

<hr>

[Visit Link](https://futurism.com/ibm-announced-50-qubit-quantum-computer/){:target="_blank" rel="noopener"}


