---
layout: post
title: "How massive can neutron stars be?"
date: 2018-05-07
categories:
author: "Goethe University Frankfurt"
tags: [Neutron star,Star,Black hole,Science,Physical cosmology,Astronomical objects,Stellar astronomy,Astrophysics,Space science,Physical sciences,Astronomy,Physics]
---


However, there are indications that a neutron star with a maximum mass would collapse to a black hole if even just a single neutron were added. Together with his students Elias Most and Lukas Weih, Professor Luciano Rezzolla, physicist, senior fellow at the Frankfurt Institute for Advanced Studies (FIAS) and professor of Theoretical Astrophysics at Goethe University Frankfurt, has now solved the problem that had remained unanswered for 40 years: With an accuracy of a few percent, the maximum mass of non-rotating neutron stars cannot exceed 2.16 solar masses. The basis for this result was the universal relations approach developed in Frankfurt a few years ago. The researchers combined these universal relations with data on gravitational-wave signals and the subsequent electromagnetic radiation (kilonova) obtained during the observation last year of two merging neutron stars in the framework of the LIGO experiment. The result is a good example of the interaction between theoretical and experimental research.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/guf-hmc011718.php){:target="_blank" rel="noopener"}


