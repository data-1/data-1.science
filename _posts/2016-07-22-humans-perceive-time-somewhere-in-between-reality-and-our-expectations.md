---
layout: post
title: "Humans perceive time somewhere in between reality and our expectations"
date: 2016-07-22
categories:
author: "University of Birmingham"
tags: [Perception,Branches of science,Academic discipline interactions,Cognitive science,Cognition,Neuroscience,Cognitive psychology,Interdisciplinary subfields,Psychological concepts,Mental processes,Psychology,Concepts in metaphysics,Behavioural sciences,Neuropsychological assessment,Concepts in the philosophy of mind,Cognitive neuroscience,Neuropsychology,Subjective experience,Science]
---


The findings, published in Scientific Reports, displayed that participants anticipated future occurrences of the stimuli in line with the regular pattern, but the perceived accuracy of their response differed from reality when the stimuli was either accelerated or delayed. The researchers, from the universities of Birmingham and Sussex, believe their findings suggest that humans do not perceive time as it really is - rather as a mid-way between reality and their expectations. Dr Max Di Luca, from the University of Birmingham, explained, Our brain relies on past events to predict what will happen next. You have an expectation of which notes to expect and when to expect them. We use what we know about the world to inform us about when something is likely to happen.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-07/uob-hpt071316.php){:target="_blank" rel="noopener"}


