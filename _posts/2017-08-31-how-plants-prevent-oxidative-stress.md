---
layout: post
title: "How plants prevent oxidative stress"
date: 2017-08-31
categories:
author: "Kobe University"
tags: [Photosynthesis,Reactive oxygen species,P700,Plant,Chloroplast,Plants,Chemistry,Biology,Biochemistry,Physical sciences,Plant metabolism,Photochemistry,Metabolism,Organisms,Underwater diving physiology,Nature,Branches of botany]
---


When excess light energy is absorbed by plants during photosynthesis, harmful reactive oxygen species are produced. Researchers have discovered the system used by plants to prevent oxidative stress and to safely carry out photosynthesis. They discovered the P700 oxidation system, used by plants to suppress the production of reactive oxygen species. The research team also clarified two strategies that photosynthesizing organisms have acquired to enable the P700 oxidation system to function and accumulate P700+. Treatment of reactive oxygen species production using the pulse method could enable scientists to evaluate plant resistance to reactive oxygen species.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-06/ku-hpp060817.php){:target="_blank" rel="noopener"}


