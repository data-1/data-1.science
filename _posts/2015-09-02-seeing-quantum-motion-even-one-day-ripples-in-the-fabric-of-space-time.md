---
layout: post
title: "Seeing quantum motion; even one day ripples in the fabric of space-time?"
date: 2015-09-02
categories:
author: California Institute of Technology 
tags: [Gravitational wave,Gravitational-wave observatory,Quantum mechanics,Physics,Science,Physical sciences,Applied and interdisciplinary physics,Theoretical physics]
---


But we know that even at the quantum ground state, at zero-temperature, very small amplitude fluctuations--or noise--remain. Because this quantum motion, or noise, is theoretically an intrinsic part of the motion of all objects, Schwab and his colleagues designed a device that would allow them to observe this noise and then manipulate it. According to the laws of classical mechanics, the vibrating structures eventually will come to a complete rest if cooled to the ground state. Because this noisy quantum motion is always present and cannot be removed, it places a fundamental limit on how precisely one can measure the position of an object. The researchers and collaborators developed a technique to manipulate the inherent quantum noise and found that it is possible to reduce it periodically.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150828142944.htm){:target="_blank" rel="noopener"}


