---
layout: post
title: "The Galápagos Islands: Laboratory of Evolution"
date: 2018-08-06
categories:
author: "Rachel Ross"
tags: [Galpagos Islands,Evolution,Invasive species,Floreana Island,Biodiversity,Plate tectonics,Species,Galapagos penguin,Galpagos tortoise,Island,Charles Darwin,Galpagos fur seal,Hotspot (geology),Nazca Plate,Nature]
---


Threats to the survival of the giant tortoises include invasive species and climate change. The only marine iguanas in the world — in addition to three land species — are endemic to the Galápagos. Dolphins and whales also visit the islands, according to the Galapagos Conservancy. Among the large populations of marine life living in the waters around the Galápagos, approximately 20 percent of the species are endemic, according to the Galapagos Conservancy. Penal colonies were set up on Floreana and San Cristóbal in the mid-19th century and on Isabela in the 1940s, according to the Galapagos Conservancy.

<hr>

[Visit Link](https://www.livescience.com/62902-galapagos-islands.html){:target="_blank" rel="noopener"}


