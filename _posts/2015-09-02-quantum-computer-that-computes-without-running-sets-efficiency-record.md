---
layout: post
title: "Quantum computer that 'computes without running' sets efficiency record"
date: 2015-09-02
categories:
author: Lisa Zyga
tags: [Quantum mechanics,Photon,Quantum Zeno effect,Science,Physics,Technology]
---


(a) The pulse sequences for the generalized CFC scheme keep the system in its ‘off’ state. So far, however, the efficiency of this process, which is called counterfactual computation (CFC), has had an upper limit of 50%, limiting its practical applications. The main keys to achieving high-efficiency CFC include the utilization of exotic quantum features (quantum superposition, quantum measurement, and the quantum Zeno effect), as well as the use of a generalized CFC protocol, Du told Phys.org. How counterfactual computing works  By not running, the scientists mean that the computer—which can operate in either an on subspace or an off subspace—stays in its off subspace for the entire computation. To image the object with our protocol, one may imagine that the situation in which a photon is absorbed by an opaque pixel is just like the computer evolving into the 'on' subspace.

<hr>

[Visit Link](http://phys.org/news/2015-08-quantum-efficiency.html){:target="_blank" rel="noopener"}


