---
layout: post
title: "Trinity scientists make breakthrough in understanding Parkinson's disease"
date: 2016-04-11
categories:
author: Trinity College Dublin
tags: [Parkin (protein),Programmed cell death,Cell (biology),Apoptosis,Neuron,American Association for the Advancement of Science,Parkinsons disease,Cell biology]
---


The scientists showed that the Parkin protein functions to repair or destroy damaged nerve cells, depending on the degree to which they are damaged  People living with Parkinson's disease often have a mutated form of the Parkin gene, which may explain why damaged, dysfunctional nerve cells accumulate  Dublin, Ireland, November 13th, 2014 - Scientists at Trinity College Dublin have made an important breakthrough in our understanding of Parkin - a protein that regulates the repair and replacement of nerve cells within the brain. The Trinity research group, led by Smurfit Professor of Medical Genetics, Professor Seamus Martin, has just published its findings in the internationally renowned, peer-reviewed Cell Press journal, Cell Reports. Using cutting-edge research techniques, the Martin laboratory, funded by Science Foundation Ireland, found that damage to mitochondria (which function as 'cellular battery packs') activates the Parkin protein, which results in one of two different outcomes - either self-destruction or a repair mode. Importantly, these new findings suggest that one of the problems in Parkinson's disease may be the failure to clear away sick nerve cells with faulty cellular battery packs, to make way for healthy replacements. The Trinity research team is internationally recognised for its work on the regulation of cell death.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-11/tcd-tsm111314.php){:target="_blank" rel="noopener"}


