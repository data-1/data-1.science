---
layout: post
title: "Scientists discover atomic-resolution details of brain signaling"
date: 2016-05-27
categories:
author: Slac National Accelerator Laboratory
tags: [SLAC National Accelerator Laboratory,X-ray crystallography,SNARE (protein),Neurotransmitter,Exocytosis,Brain,Protein,Neuron,Thomas C Sdhof,Cell signaling,Neuroscience]
---


A protein complex at work in brain signaling. Its structure, which contains joined protein complexes known as SNARE and synaptotagmin-1, is shown in the foreground. The experiments, at the Linac Coherent Light Source (LCLS) X-ray laser at the Department of Energy's SLAC National Accelerator Laboratory, build upon decades of previous research at Stanford University, Stanford School of Medicine and SLAC. Credit: SLAC National Accelerator Laboratory  Using Crystals, Robotics and X-rays to Advance Neuroscience  To study the joined protein structure, researchers in Brunger's laboratory at the Stanford School of Medicine found a way to grow crystals of the complex. They used a robotic system developed at SSRL to study the crystals at SLAC's LCLS, an X-ray laser that is one of the brightest sources of X-rays on the planet.

<hr>

[Visit Link](http://phys.org/news/2015-08-scientists-atomic-resolution-brain.html){:target="_blank" rel="noopener"}


