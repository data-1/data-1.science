---
layout: post
title: "Life scientists streamline cutting-edge technique to edit mosquito genome"
date: 2016-04-17
categories:
author: Virginia Tech
tags: [Genome editing,Mosquito,Gene,Biology,Genetics,Life sciences,Biotechnology]
---


Life science researchers at Virginia Tech have accelerated a game-changing technology that's being used to study one of the planet's most lethal disease-carrying animals. Editing the genome of an organism allows scientists to study it by deleting certain genes to observe how the organism is affected, or even to add new genes. Medical and life scientists quickly saw the technique's potential to edit disease-causing genes in people or, in the case of mosquito research, strategically modify the genome of an animal known to carry parasites and viruses that cause malaria, dengue fever, and other high-impact diseases. Adelman, Myles, and colleagues overcame this obstacle by developing a process that silences an important mosquito DNA repair mechanism, ensuring that the introduced changes are more likely to become a permanent part of the mosquito genome, and thus increasing opportunities to figure out the new gene's role in mosquito biology. The researchers also improved the technique to increase the probability of precise insertion of DNA, making CRISPR more useful.

<hr>

[Visit Link](http://phys.org/news345725394.html){:target="_blank" rel="noopener"}


