---
layout: post
title: "Space-like gravity weakens biochemical signals in muscle formation: Microgravity conditions affect DNA methylation of muscle cells, slowing their differentiation"
date: 2018-06-07
categories:
author: "Hiroshima University"
tags: [Micro-g environment,Skeletal muscle,Cellular differentiation,DNA methylation,Cell (biology),Gene,Muscle cell,DNA,Genetics]
---


Astronauts go through many physiological changes during their time in spaceflight, including lower muscle mass and slower muscle development. They found that the process that affects gene expression of differentiating muscle cells in space also affects cells in the presence of gravity. Some cells were treated with a drug that stops DNA methylation from happening, while other cells were not. DNA methylation is a process that controls gene expression and muscle cell differentiation. The team's results can be utilized in space experiments, where muscle atrophy of astronauts uses myotubes because it is easy to understand morphologically.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/05/180523080200.htm){:target="_blank" rel="noopener"}


