---
layout: post
title: "Neuronal 'barcodes' shape complex networks in the brain"
date: 2018-07-12
categories:
author: "VIB (the Flanders Institute for Biotechnology)"
tags: [Brain,Synapse,Neuron,Cell adhesion,Hippocampus,Memory,Neuroscience]
---


Adhesion molecules  De Wit's team went looking for answers by studying adhesion molecules. In combination, the three adhesion molecules precisely define how synapses look and function. In this way, the many different adhesion molecules found in neurons across the brain allow for precise fine-tuning of the different connections they make. Understanding their role in brain connectivity is thus of vital importance, says de Wit:  Now that we understand that these adhesion molecules not only shape the number, but also the architecture and function of synapses, this may lead to a better understanding of how disease-associated mutations in the genes that encode these molecules affect circuit connectivity and function. Everyone can submit questions concerning this and other medically-oriented research directly to VIB via this address.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/vfi-ns070518.php){:target="_blank" rel="noopener"}


