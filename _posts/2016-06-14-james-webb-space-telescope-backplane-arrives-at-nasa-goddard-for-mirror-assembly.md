---
layout: post
title: "James Webb Space Telescope backplane arrives at NASA Goddard for mirror assembly"
date: 2016-06-14
categories:
author: "Laura Betz, Nasa'S Goddard Space Flight Center"
tags: [James Webb Space Telescope,Goddard Space Flight Center,NASA,Space program of the United States,Space exploration,Flight,Astronautics,Science,Astronomy,Space science,Outer space,Spaceflight]
---


The James Webb Space Telescope's spine or backplane arrived on Aug. 25 at NASA's Goddard Space Flight Center in Greenbelt, Maryland from Northrop Grumman. Credit: NASA Goddard/Chris Gunn  One of the most crucial pieces of the James Webb Space Telescope, the flight backplane, arrived on Aug. 25, on schedule for Webb's 2018 launch date at NASA's Goddard Space Flight Center in Greenbelt, Maryland, for mirror assembly. The backplane is the spine of the telescope, responsible for holding its 18 hexagonal mirrors and instruments steady while the telescope is looking into deep space. Together, those 18 mirrors make up Webb's primary mirror. Webb's components are built at room temperature, but will eventually operate at extremely cold temperatures as low as minus 389 F. While in orbit the temperature of the backplane will also vary depending on where it is pointing relative to the sun.

<hr>

[Visit Link](http://phys.org/news/2015-08-james-webb-space-telescope-backplane.html){:target="_blank" rel="noopener"}


