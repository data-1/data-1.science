---
layout: post
title: "Quantum computing: towards reality"
date: 2017-09-19
categories:
author: "Trabesinger, Andreas Trabesinger Is A Science Writer Based In Switzerland., Andreas Trabesinger, You Can Also Search For This Author In, Author Information"
tags: []
---


Now the environment exists to make prototype devices a reality. Enter quantum computers, which were first mooted in the 1980s, when it was suggested that it might be possible to construct computers based on the laws of quantum physics instead of on classical physics. But if the richness of quantum states is used to encode information, a whole new world of computational possibilities opens up. The past two decades have seen steady progress in isolating, manipulating and measuring the elements that might form the basis of a quantum computer, be they single quantum entities such as atoms, electrons or photons, or artificial systems that display quantum-mechanical behaviour, such as semiconductor structures or miniature electronic circuits. Just what a quantum computer will eventually look like, which quantum systems it will use, and which problems it will solve remain open questions.

<hr>

[Visit Link](http://www.nature.com/nature/journal/v543/n7646_supp_out/full/543S1a.html?WT.feed_name=subjects_quantum-physics){:target="_blank" rel="noopener"}


