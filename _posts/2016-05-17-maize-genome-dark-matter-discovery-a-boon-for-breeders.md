---
layout: post
title: "Maize genome 'dark matter' discovery a boon for breeders"
date: 2016-05-17
categories:
author: Cornell University
tags: [DNA,Gene,Plant breeding,Genetics,Maize,ENCODE,Genome,Biochemistry,Molecular biology,Life sciences,Biotechnology,Biology,Branches of genetics]
---


Now, plant scientists have discovered a different kind of dark matter in the maize genome: a tiny percentage of regulatory DNA that accounts for roughly half of the variation in observable traits found in corn. In a landmark finding, Cornell University and Florida State University researchers report they have identified 1 to 2 percent of the maize genome that turns genes on and off, so they may now focus their attention on these areas for more efficient plant breeding. The chromatin profiling shows you which parts of the genome are genetic switches. By comparison, regulatory open chromatin regions may be as much as 8 percent of the genome in humans. For me, it was the first time when the biology of the genome got simpler, Buckler said.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/cu-mg051616.php){:target="_blank" rel="noopener"}


