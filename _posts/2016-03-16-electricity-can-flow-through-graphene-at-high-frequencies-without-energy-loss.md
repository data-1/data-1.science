---
layout: post
title: "Electricity can flow through graphene at high frequencies without energy loss"
date: 2016-03-16
categories:
author: University of Plymouth
tags: [Graphene,Electronics,Signal,Sensor,Technology,Electrical engineering,Electricity,Electromagnetism]
---


Electrical signals transmitted at high frequencies lose none of their energy when passed through the 'wonder material' graphene, a study led by Plymouth University has shown. Discovered in 2004, graphene - which measures just an atom in thickness and is around 100 times stronger than steel - has been identified as having a range of potential uses across the engineering and health sectors. And since graphene lacks band-gap, which allows electrical signals to be switched on and off using silicon in digital electronics, academics say it seems most applicable for applications ranging from next generation high-speed transistors and amplifiers for mobile phones and satellite communications to ultra-sensitive biological sensors. The study was led by Dr Shakil Awan, a Lecturer in the School of Computing, Electronics and Mathematics at Plymouth University, alongside colleagues from Cambridge and Tohoku (Japan) Universities and Nokia Technologies (Cambridge, UK). The latter is the focus of a three-year £1million project funded by the EPSRC on developing highly-sensitive graphene bio-sensors for early detection of dementia (such as Alzheimer's disease) compared to current methods.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/uop-ecf030416.php){:target="_blank" rel="noopener"}


