---
layout: post
title: "Building a freedom-based photography community"
date: 2018-05-07
categories:
author: "Opensource.com
(Red Hat)"
tags: [Red Hat,Open source,Creative Commons license,Digital media,Computing,Technology,Intellectual works,Software,Mass media,Open-source movement,Communication,Information technology]
---


Several years ago, Pat David, an engineer, a photographer, and a member of the GIMP team, realized there was no central place for people interested in photography to learn about free and open source creative software and methods. He was also unhappy to see that most of the open source photography tutorials didn't measure up to his quality standards. Pat and Pixls.us also advocate for releasing creative content under open licenses for others to use and reuse. Watch his presentation to learn more. During the UpSCALE Lightning Talks hosted by Opensource.com at the 16th annual Southern California Linux Expo (SCALE) in March 2018, eight presenters shared quick takes on interesting open source topics, projects, and ideas.

<hr>

[Visit Link](https://opensource.com/article/18/5/pixls-us-community-photography){:target="_blank" rel="noopener"}


