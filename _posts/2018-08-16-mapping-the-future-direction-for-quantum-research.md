---
layout: post
title: "Mapping the future direction for quantum research"
date: 2018-08-16
categories:
author: "Institute Of Physics"
tags: [Branches of science,Technology,Science,Computing]
---


Credit: CC0 Public Domain  The way research in quantum technology will be taken forward has been laid out in a revised roadmap for the field. Published today in the New Journal of Physics, leading European quantum researchers summarise the field's current status, and examine its challenges and goals. In the roadmap:  Dr. Rob Thew and Professor Nicolas Gisin look at quantum communication, and when we may expect to see long-distance quantum key distribution networks  Professor Frank Wilhelm, Professor Daniel Esteve, Dr. Christopher Eichler, and Professor Andreas Wallraff examine the state of quantum computing, and the timescale for delivering large-scale quantum computational systems  Professor Jens Eisert, Professor Immanuel Bloch, Professor Maciej Lewenstein and Professor Stefan Kuhr trace quantum simulation from Richard Feynman's original theory through to the advances in technology needed to meet future challenges, and the pivotal role quantum simulators could play  Professor Fedor Jelezko , Professor Piet Schmidt, and Professor Ian Walmsley consider the field of quantum metrology, sensing and imaging, and look at a variety of physical platforms to implement this technology  Professor Frank Wilhelm and Professor Steffen Glaser examine quantum control, and the long-term goal of gaining a thorough understanding of optimal solutions, and developing a software layer enhancing the performance of quantum hardware  Professor Antonio Acín and Professor Harry Buhrman highlight the current status and future challenges of this quantum software and theory structured along three main research directions: quantum software for computing, quantum software for networks, and theory. Introducing the collection of articles, Dr. Max Riedel and Professor Tommasso Calarco note: Within the last two decades, quantum technologies have made tremendous progress, moving from Nobel Prize-winning experiments on quantum physics into a cross-disciplinary field of applied research.

<hr>

[Visit Link](https://phys.org/news/2018-08-future-quantum.html){:target="_blank" rel="noopener"}


