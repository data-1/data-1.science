---
layout: post
title: "Fruit-eating increases biodiversity"
date: 2017-10-24
categories:
author: Universiteit van Amsterdam (UVA)
tags: [Frugivore,Seed dispersal,Species,Speciation,Environmental social science,Systems ecology,Organisms,Nature,Biogeochemistry,Ecology,Natural environment,Biology,Biological evolution]
---


By dispersing the seeds of plants, fruit-eating animals contribute to the possibility of increased plant speciation and thus biodiversity. 'The aim of our study was to compare past speciation of these palms with very large fruits, to speciation of palms with smaller fruits' says Onstein. Indeed, palms dispersed by flying fruit-eating animals such as fruit pigeons and fruit bats, which can colonize isolated Asian and Pacific islands, showed the highest speciation rates, followed by Latin American palms growing in the rain forest understory, which rely on dispersal by sedentary, range-restricted understorey fruit-eating animals. These results provide important insights for the future of biodiversity. 'Our study shows that interactions among species, such as those between animal seed dispersers and their food plants, are crucial for biodiversity and the benefits that nature provides to human societies', explains Daniel Kissling.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171023150541.htm){:target="_blank" rel="noopener"}


