---
layout: post
title: "The unique ecology of human predators"
date: 2016-05-28
categories:
author: "University of Victoria"
tags: [Predation,News aggregator,Animals,Environmental social science,Ecology,Natural environment]
---


Are humans unsustainable 'super predators'? Want to see what science now calls the world's super predator? Look in the mirror. Humans hunt and kill large land carnivores such as bears, wolves and lions at nine times the rate that these predatory animals kill each other in the wild. Whereas predators primarily target the juveniles or 'reproductive interest' of populations, humans draw down the 'reproductive capital' by exploiting adult prey, says co-author Dr. Tom Reimchen, biology professor at UVic.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150820144837.htm){:target="_blank" rel="noopener"}


