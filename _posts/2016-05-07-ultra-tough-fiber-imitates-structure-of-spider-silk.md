---
layout: post
title: "Ultra-tough fiber imitates structure of spider silk"
date: 2016-05-07
categories:
author: Polytechnique Montréal
tags: [Spider,Spider silk,Composite material,Fiber,Polymer,Silk,Building engineering,Polymers,Applied and interdisciplinary physics,Goods (economics),Materials science,Manufacturing,Chemistry,Artificial materials,Materials]
---


The mechanical origin of its strength drew the interest of researchers at the Laboratory for Multiscale Mechanics in Polytechnique Montreal's Department of Mechanical Engineering. Each loop of the spring is attached to its neighbours with sacrificial bonds, chemical connections that break before the main molecular structural chain tears, explained Professor Gosselin, who, along with his colleague Daniel Therriault, is co-supervising Renaud Passieux's master's research work. This is the mechanism we're seeking to reproduce in laboratory,  Imitating nature with polymer fibres  Their project involves making micrometric-sized microstructured fibres that have mechanical properties similar to those of spider silk. Some instability patterns feature the formation of sacrificial bonds when the filament makes a loop and bonds to itself. This project aims to understand how the instability used in making the substance influences the loops' geometry and, as a result, the mechanical properties of the fibres we obtain, explained Professor Therriault.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150603124543.htm){:target="_blank" rel="noopener"}


