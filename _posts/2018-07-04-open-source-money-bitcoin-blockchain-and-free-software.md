---
layout: post
title: "Open source money: Bitcoin, blockchain, and free software"
date: 2018-07-04
categories:
author: "Heather Meeker"
tags: [Blockchain,Cryptocurrency wallet,Bitcoin,Cryptocurrency,Computer network,Open source,Information Age,Computing,Technology,Computer security,Computer science,Information technology]
---


What is blockchain? Every participant in a public ledger can access a copy of every transaction, write a new block to the chain, and validate new transactions. What is Bitcoin? When you access your Bitcoin wallet with your private key, you can transfer Bitcoins with anyone over the distributed network. The only thing that is properly called open source is open source software.

<hr>

[Visit Link](https://opensource.com/article/18/7/bitcoin-blockchain-and-open-source){:target="_blank" rel="noopener"}


