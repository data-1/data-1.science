---
layout: post
title: "First ancient genome recovered from the Mediterranean area"
date: 2015-09-07
categories:
author: Spanish National Research Council (CSIC) 
tags: [Cardium pottery,Europe,Biology,Genetics]
---


An international team of researchers has sequenced the first complete genome of an Iberian farmer, which is also the first ancient genome from the entire Mediterranean area. This new genome allows to know the distinctive genetic changes of Neolithic migration in Southern Europe which led to the abandonment of the hunter-gatherer way of life. So far, only genomic data of various individuals belonging to the inland route found in Hungary and Germany were available, but the complete genomes of the Mediterranean route were lacked. The research team, led by Carles Lalueza-Fox from the Institute of Evolutionary Biology, has sequenced the complete genome of a Neolithic woman from a tooth dated in 7400 years and from the cardial levels of the Cova Bonica cave in Vallirana, near Barcelona. Thanks to this new genome, researchers have been able to determine that farmers from the Mediterranean route and the inland route are very homogeneous and clearly derive from a common ancestral population that, most likely, is that of the first farmers who entered Europe through Anatolia.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/snrc-fag090215.php){:target="_blank" rel="noopener"}


