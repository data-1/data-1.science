---
layout: post
title: "KM3NeT unveils detailed plans for largest neutrino telescope in the world"
date: 2016-06-24
categories:
author: "IOP Publishing"
tags: [KM3NeT,Particle physics,Space science,Science,Nature,Astronomy,Physics,Physical sciences]
---


Deep-sea array will soak up signals from neutrinos traveling through the cosmos to study the evolution of the universe and to discover more about the fundamental properties of these prized sub-atomic particles  KM3NeT - a European collaboration pioneering the deployment of kilometre cubed arrays of neutrino detectors off the Mediterranean coast - has reported in detail on the scientific aims, technology and costs of its proposal in the Journal of Physics G: Nuclear and Particle Physics. Neutrinos are ideal messengers from the cosmos. Secondly, the passage of these relativistic charged particles through the medium produces so-called Cherenkov light (the typical blue light on pictures of nuclear reactors). It turns out that the deep waters in the Mediterranean are ideal, explained de Jong. In December 2015, the KM3NeT collaboration successful tested the deployment of a string of its latest optical modules.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/ip-kud062316.php){:target="_blank" rel="noopener"}


