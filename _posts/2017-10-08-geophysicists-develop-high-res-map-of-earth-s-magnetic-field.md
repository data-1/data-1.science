---
layout: post
title: "Geophysicists develop high-res map of Earth's magnetic field"
date: 2017-10-08
categories:
author: "Jenny Wells, University Of Kentucky"
tags: [Swarm (spacecraft),Satellite,Earth,Geophysics,Space science,Astronomy,Planetary science,Physical sciences,Outer space,Nature,Earth sciences,Science,Planets of the Solar System,Bodies of the Solar System,Solar System]
---


Up until now, scientists have not been able to fully map the magnetic field, but thanks to the Swarm mission, they now have a more complete understanding. It is also the first time anyone has put together a magnetic variation map from just the gradients of the field. Credit: European Space Agency (ESA)  The map also reveals the Earth's polarity flips in great detail. This new map shows striking patterns of these flipped polarities over time—solidified minerals have formed stripes on the sea floor and provide a record of Earth's magnetic history. The new map can define magnetic field features down to about 250 kilometers and will help scientists investigate geology and temperatures in Earth's lithosphere, especially in parts of the African continent that do not have detailed magnetic field variation maps.

<hr>

[Visit Link](https://phys.org/news/2017-04-geophysicists-high-res-earth-magnetic-field.html){:target="_blank" rel="noopener"}


