---
layout: post
title: "Sentinel-3A prepares for liftoff"
date: 2016-03-15
categories:
author:  
tags: [Sentinel-3,Outer space,Space science,Spacecraft,Spaceflight,Satellites,Astronautics,Bodies of the Solar System,Earth sciences]
---


This timelapse video shows Sentinel-3A, from final preparations to liftoff on a Rockot launcher from the Plesetsk Cosmodrome in northern Russia, at 17:57 GMT (18:57 CET) on 16 February 2016. Sentinel-3A is the third satellite to be launched for Europe’s Copernicus environment monitoring programme. Designed as a two-satellite constellation – Sentinel-3A and -3B – the Sentinel-3 mission carries a series of cutting-edge instruments for systematic measurements of Earth’s oceans, land, ice and atmosphere. Over oceans, Sentinel-3 measures the temperature, colour and height of the sea surface as well as the thickness of sea ice. These measurements will be used, for example, to monitor changes in sea level, marine pollution and biological productivity.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2016/03/Sentinel-3A_prepares_for_liftoff){:target="_blank" rel="noopener"}


