---
layout: post
title: "Helsinki Builds 9 Million Gallon Underground Water Tank to Cool Local Buildings"
date: 2015-07-04
categories:
author: Colin Payne
tags: []
---


The Finnish capital recently announced some major environmental initiatives aimed at making the city smarter and greener. After the water circulates through the cooling system during the day, it’s cooled in the tank overnight using heat exchangers to prepare for its next run the following day. According to a City of Helsinki press release, this latest addition to the city’s smart cooling system means it now emits 80 percent less greenhouse gasses from cooling, compared to conventional cooling systems. Continue reading below Our Featured Videos  Also on Helsinki’s list of improvements is a new intelligent public transportation system called Kutsuplus (“call plus”), an on-demand minibus service that lets riders book pickups and pay their fares on smartphones or computers. SIGN UP  Finally, Helsinki is also developing whole new areas of the city that are model districts for intelligent and eco-friendly lifestyles.

<hr>

[Visit Link](http://inhabitat.com/helsinki-builds-9-million-gallon-underground-water-tank-to-cool-local-buildings/){:target="_blank" rel="noopener"}


