---
layout: post
title: "Scientists track the brain-skull transition from dinosaurs to birds"
date: 2017-10-06
categories:
author: "Yale University"
tags: [Skull,Brain,News aggregator]
---


It is the first time scientists have tracked the link between the brain's development and the roofing bones of the skull. Across the dinosaur-bird transition, the skull transforms enormously and the brain enlarges. It was the dinosaurs most closely related to birds, as well as birds themselves, that were divergent, with enlarged brains and skulls ballooning out around them. We found a clear relationship between the frontal bones and forebrain and the parietal bones and midbrain, Bhullar said. We suggest that this relationship is found across all vertebrates with bony skulls and indicates a deep developmental relationship between the brain and the skull roof, Bhullar said.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/09/170911122701.htm){:target="_blank" rel="noopener"}


