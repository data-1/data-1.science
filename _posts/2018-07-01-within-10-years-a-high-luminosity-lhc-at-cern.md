---
layout: post
title: "Within 10 years, a high-luminosity LHC at CERN"
date: 2018-07-01
categories:
author: ""
tags: [Large Hadron Collider,High Luminosity Large Hadron Collider,Compact Muon Solenoid,ATLAS experiment,Collider,Particle accelerators,Physics institutes,Applied and interdisciplinary physics,Particle physics,Science,Accelerator physics,Experimental particle physics,Physics laboratories,Experimental physics,Physics,Particle physics facilities]
---


Credit: Cyril Fresillon/LHC/CNRS Photothèque  A new site opened on Friday, June 15, 2018, at the LHC, the Large Hadron Collider. Begun in 2011, this project aims to commission a high-luminosity LHC (HL-LHC) by 2026 that will increase the number of proton-proton collisions and gather more data. Teams at CNRS and CEA are participating in technological research and development specifically on superconducting magnets and on extending the lifespan of detectors and the accelerator. Credit: Cyril Fresillon/LHC/CNRS Photothèque  Teams from the CNRS, the CEA, and their university partners will work on the Atlas and CMS particle detectors: important improvements will be made to the detectors so that they function with better performance in much more intense conditions. Atlas, the largest particle detector ever built for particle physics (46m long and 25m high), opened during a stoppage.

<hr>

[Visit Link](https://phys.org/news/2018-06-years-high-luminosity-lhc-cern.html){:target="_blank" rel="noopener"}


