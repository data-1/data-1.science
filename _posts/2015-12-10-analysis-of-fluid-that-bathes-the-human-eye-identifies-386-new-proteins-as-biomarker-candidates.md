---
layout: post
title: "Analysis of fluid that bathes the human eye identifies 386 new proteins as biomarker candidates"
date: 2015-12-10
categories:
author: Mary Ann Liebert, Inc./Genetic Engineering News
tags: [Protein,Proteomics,Biology,Omics,Metabolism,Metabolomics,Biochemistry,Life sciences,Biotechnology,Branches of science,Chemistry,Technology]
---


These proteins could have a role in disease processes affecting the eye and serve as valuable biomarkers for the development of diagnostics and drug candidates to improve visual health, as discussed in the article Proteomics of Human Aqueous Humor, published in OMICS: A Journal of Integrative Biology, the peer-reviewed interdisciplinary journal published by Mary Ann Liebert, Inc., publishers. Others have roles in cell growth, differentiation, and proliferation. Other enzymes have a role in metabolism and the energy needs of ocular components such as the lens and cornea. ###  About the Journal  OMICS: A Journal of Integrative Biology is an authoritative peer-reviewed journal published monthly online, which covers genomics, transcriptomics, proteomics, and metabolomics innovations and omics systems diagnostics for integrative medicine. Complete tables of content and a sample issue may be viewed on the OMICS: A Journal of Integrative Biology website.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-05/mali-aof052915.php){:target="_blank" rel="noopener"}


