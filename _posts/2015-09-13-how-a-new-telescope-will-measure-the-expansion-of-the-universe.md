---
layout: post
title: "How a new telescope will measure the expansion of the universe"
date: 2015-09-13
categories:
author: Kate Greene, Lawrence Berkeley National Laboratory
tags: [Dark energy,Universe,Dark Energy Spectroscopic Instrument,National Optical Astronomy Observatory,Nicholas U Mayall Telescope,Redshift,Kitt Peak National Observatory,Astronomy,Cosmology,Physical cosmology,Physics,Science,Astrophysics,Physical sciences,Space science]
---


DS: We're actually retrofitting a 4-meter telescope at Kitt Peak in Arizona called the Mayall telescope. We'll observe these galaxies for about 20 minutes, then we will point the telescope in a new direction and 5000 little robots will rearrange these fibers to look at a new collection of up to 5000 galaxies, one fiber per galaxy. All of these projects are working together to better understand what dark energy actually is. But we only have one data point from that time. All the other data we have is when the universe's expansion was speeding up, thanks to dark energy.

<hr>

[Visit Link](http://phys.org/news349685540.html){:target="_blank" rel="noopener"}


