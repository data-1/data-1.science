---
layout: post
title: "Computer models show possible impact to world's oceans of four major stressors due to climate change"
date: 2018-08-15
categories:
author: "Bob Yirka"
tags: [Climate change,Effects of climate change,Greenhouse gas,Water,Climate]
---


Scientists have been conducting experiments designed to understand what that might mean for sea life—but as the researchers with this new effort note, to date, not much work has been done to unite current findings regarding stressors (changes to marine conditions) to predict when certain changes might come about, or if there might be some overlap. The model predicted that within just 15 years more than half of the world's oceans will be reacting to more than one of the four stressors—by 2050, that number will jump to 86 percent. These estimates were based on the status quo, meaning emissions levels remain at current levels. It also showed that changes in pH levels are likely to have the earliest impact—indeed virtually all of the oceans have already been impacted. They note that particular stressors and their degree of impact will almost certainly vary between geographical areas and types of marine life in that area.

<hr>

[Visit Link](https://phys.org/news/2017-03-impact-world-oceans-major-stressors.html){:target="_blank" rel="noopener"}


