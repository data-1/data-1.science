---
layout: post
title: "Collimators—the LHC's bodyguards"
date: 2018-07-01
categories:
author: "Iva Raynova"
tags: [Large Hadron Collider,Particle accelerator,CERN,Applied and interdisciplinary physics,Physics,Particle physics,Technology]
---


Installation of a collimator in the LHC. If even a small fraction of the circulating particles deviates from the precisely set trajectory, it can quench a super-conducting LHC magnet or even destroy parts of the accelerator. These teeth are part of special devices around the LHC, called collimators. Their jaws – moveable blocks of robust materials – close around the beam to clean it of stray particles before they come close to the collision regions. With the expected increase in the number of particle collisions in the High-Luminosity LHC, the beam intensity will be much higher.

<hr>

[Visit Link](https://phys.org/news/2018-02-collimatorsthe-lhc-bodyguards.html){:target="_blank" rel="noopener"}


