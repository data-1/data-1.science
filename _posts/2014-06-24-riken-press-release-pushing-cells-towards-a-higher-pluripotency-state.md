---
layout: post
title: "RIKEN press release: Pushing cells towards a higher pluripotency state"
date: 2014-06-24
categories:
author: RIKEN 
tags: [Cell potency,Stem cell,Induced pluripotent stem cell,CCL2,Cell (biology),Life sciences,Cell biology,Biology,Biotechnology,Medical specialties,Biochemistry,Molecular biology,Cells,Developmental biology,Biological processes]
---


Stem cells have the unique ability to become any type of cell in the body. However, some conditions that are commonly used for culturing human stem cells have the potential to introduce contaminants, thus rendering the cells unusable for clinical use. In the study, the researchers replaced basic fibroblast growth factor (bFGF), a critical component of human stem cell culture, with CCL2 and studied its effect. The work showed that CCL2 used as a replacement for bFGF activated the JAK/STAT pathway, which is known to be involved in the immune response and maintenance of mouse pluripotent stem cells. These results could potentially contribute to greater consistency of human induced pluripotent stem cells (iPSCs), which are important both for regenerative medicine and for research into diseases processes.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/r-rpr062214.php){:target="_blank" rel="noopener"}


