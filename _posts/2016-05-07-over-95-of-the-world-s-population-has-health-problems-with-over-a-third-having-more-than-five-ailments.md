---
layout: post
title: "Over 95% of the world’s population has health problems, with over a third having more than five ailments"
date: 2016-05-07
categories:
author: The Lancet
tags: [Disease burden,Disease,Health,Disability-adjusted life year,Chronic condition,Health sciences,Clinical medicine,Medicine,Diseases and disorders,Medical specialties,Epidemiology,Causes of death,Public health,Health care]
---


Just one in 20 people worldwide (4·3%) had no health problems in 2013, with a third of the world's population (2·3 billion individuals) experiencing more than five ailments, according to a major new analysis from the Global Burden of Disease Study (GBD) 2013, published in The Lancet. Low back pain, depression, iron-deficiency anemia, neck pain, and age-related hearing loss resulted in the largest overall health loss worldwide (measured in terms of YLD -- Years Lived with Disability -- ie, time spent in less than optimum health [2]) in both 1990 and 2013. The number of years lived with disability increased over the last 23 years due to population growth and aging (537·6 million to 764·8 million), while the rate (age-standardised per 1000 population) barely declined between 1990 and 2013 (115 per 1000 people to 110 per 1000 people). The main drivers of increases in the number of years lived with disability were musculoskeletal, mental, and substance abuse disorders, neurological disorders, and chronic respiratory conditions. These are worked out by combining the number of years of life lost as a result of early death and the number of years lived with disability.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150608081753.htm){:target="_blank" rel="noopener"}


