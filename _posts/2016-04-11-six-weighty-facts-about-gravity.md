---
layout: post
title: "Six weighty facts about gravity"
date: 2016-04-11
categories:
author: SLAC National Accelerator Laboratory
tags: [Gravitational wave,Graviton,Quantum gravity,Fundamental interaction,General relativity,Quantum mechanics,Gravity,Force,String theory,Physical quantities,Astrophysics,Particle physics,Applied and interdisciplinary physics,Theories of gravitation,Astronomy,Physical cosmology,Theory of relativity,Science,Physical sciences,Theoretical physics,Physics]
---


We also recognize that gravity is one of the four fundamental forces of nature, along with electromagnetism, the weak force and the strong force. The force of gravity on an astronaut is about 90 percent of the force they would experience on Earth. General relativity predicts gravitational waves. The other three fundamental forces of nature are described by quantum theories at the smallest of scales -- specifically, the Standard Model. This allows loop quantum gravity to describe the effect of gravity on a scale far smaller than the nucleus of an atom.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/04/160406170047.htm){:target="_blank" rel="noopener"}


