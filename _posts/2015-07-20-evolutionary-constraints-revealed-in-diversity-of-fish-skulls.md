---
layout: post
title: "Evolutionary constraints revealed in diversity of fish skulls"
date: 2015-07-20
categories:
author: University of California - Santa Cruz 
tags: [Aquatic feeding mechanisms,Animals,Zoology]
---


This enabled Mehta's team to look at the evolution of biting and suction feeding and compare the morphological diversity of skulls in the different lineages. The biters are three times more diverse than the suction feeders. In a 2007 paper in Nature, Mehta reported that after a moray eel captures its prey, a second set of jaws is launched from the back of the throat to grab the prey and pull it into the esophagus. In other words, suction feeders accumulated morphological changes at the same rate as biters, but those changes occurred within a much more limited morphological space. The changes that occurred in biter lineages, in contrast, led to much greater diversity in skull morphology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-11/uoc--ecr111314.php){:target="_blank" rel="noopener"}


