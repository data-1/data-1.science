---
layout: post
title: "Evolution is unpredictable and irreversible, biologists show"
date: 2016-05-07
categories:
author: University Of Pennsylvania
tags: [Natural selection,Mutation,Epistasis,Evolution,Evolutionary biology,Science,Biological evolution,Biology,Nature]
---


Using simulations of an evolving protein, they show that the genetic mutations that are accepted by evolution are typically dependent on mutations that came before, and the mutations that are accepted become increasingly difficult to reverse as time goes on. The concepts of contingency and entrenchment were well known to be present in adaptive evolution, but it came as a surprise to the researchers to find them under purifying selection. Thus mutations that are dependent upon earlier mutations will be favored. The way these substitutions occur, since they're highly contingent on what happened before, makes predictions of long-term evolution extremely difficult, Plotkin said. Gould's famous tape of life would be very different if replayed, even more different than Gould might have imagined.

<hr>

[Visit Link](http://phys.org/news353002893.html){:target="_blank" rel="noopener"}


