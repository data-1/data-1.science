---
layout: post
title: "Working out in artificial gravity"
date: 2015-12-21
categories:
author: Massachusetts Institute of Technology
tags: [Artificial gravity,Stationary bicycle,Centrifuge,Weightlessness,Astronaut,International Space Station,Exercise,Outer space,Astronautics,Spaceflight,Flight]
---


After testing the setup on healthy participants, the team found the combination of exercise and artificial gravity could significantly lessen the effects of extended weightlessness in space -- more so than exercise alone. The researchers conducted experiments to test human responses and exercise performance at varying levels of artificial gravity. Overall, Diaz found that participants tolerated the experiments well, suffering little motion sickness even while spinning at relatively high velocities. Participants only reported feelings of discomfort while initially speeding up and slowing down. That tells us that if we use artificial gravity, we're able to get higher foot forces, and we know higher foot forces are good for bones, and help you generate more bone, Diaz says.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/miot-woi070215.php){:target="_blank" rel="noopener"}


