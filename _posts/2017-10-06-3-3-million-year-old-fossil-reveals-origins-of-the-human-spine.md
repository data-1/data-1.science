---
layout: post
title: "3.3-million-year-old fossil reveals origins of the human spine"
date: 2017-10-06
categories:
author: "University of Chicago Medical Center"
tags: []
---


Analysis of a 3.3 million-year-old fossil skeleton reveals the most complete spinal column of any early human relative, including vertebrae, neck and rib cage. The fossil, known as Selam, is a nearly complete skeleton of a 2½ year-old child discovered in Dikika, Ethiopia in 2000 by Zeresenay (Zeray) Alemseged, professor of organismal biology and anatomy at the University of Chicago and senior author of the new study. Many features of the human spinal column and rib cage are shared among primates. For instance, humans have fewer rib-bearing vertebrae - bones of the back - than those of our closest primate relatives. But we have not been able to determine how many vertebrae our early ancestors had, said Carol Ward, a Curator's Distinguished Professor of Pathology and Anatomical Sciences in the University of Missouri School of Medicine, and lead author on the study.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-05/uocm-3mf051717.php){:target="_blank" rel="noopener"}


