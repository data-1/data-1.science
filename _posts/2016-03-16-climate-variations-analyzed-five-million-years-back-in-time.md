---
layout: post
title: "Climate variations analyzed five million years back in time"
date: 2016-03-16
categories:
author: Niels Bohr Institute
tags: [Ice age,Climate,Earth,Climate variability and change,Axial tilt,Global temperature record,Climate change,Natural environment,Physical sciences,Applied and interdisciplinary physics,Earth sciences,Planetary science,Nature,Physical geography]
---


Researchers from the Niels Bohr Institute have analysed the natural climate variations over the last 12,000 years, during which we have had a warm interglacial period and they have looked back 5 million years to see the major features of the Earth's climate. The research shows that the natural variations over a given period of time depends on the length of this period in the very particular way that is characteristic for fractals. Credit: Ola Jakup Joensen  Abrupt climate fluctuations during the ice age  We can see that the climate during an ice age has much greater fluctuations than the climate during an interglacial period. The astronomical factors that affect the Earth's climate are that the other planets in the solar system pull on the Earth because of their gravity. Natural and human-induced climate changes  The climate during the warm interglacial periods is more stable than the climate of ice age climate.

<hr>

[Visit Link](http://phys.org/news/2016-03-climate-variations-million-years.html){:target="_blank" rel="noopener"}


