---
layout: post
title: "UCLA physicists map the atomic structure of an alloy"
date: 2017-09-17
categories:
author: "University of California - Los Angeles"
tags: [Atom,Grain boundary,Physics,Nanotechnology,Materials science,Nanoparticle,Science,Crystallite,Ironplatinum nanoparticle,Physical sciences,Chemistry,Applied and interdisciplinary physics,Technology,Materials]
---


Now, UCLA physicists and collaborators have mapped the coordinates of more than 23,000 individual atoms in a tiny iron-platinum nanoparticle to reveal the material's defects. This new National Science Foundation-funded consortium consists of scientists at UCLA and five other colleges and universities who are using high-resolution imaging to address questions in the physical sciences, life sciences and engineering. Everything we look at, it's new, Miao said. Understanding the three-dimensional structures of grain boundaries is a major challenge in materials science because they strongly influence the properties of materials, Miao said. The researchers then used the three-dimensional coordinates of the atoms as inputs into quantum mechanics calculations to determine the magnetic properties of the iron-platinum nanoparticle.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-02/uoc--upm012617.php){:target="_blank" rel="noopener"}


