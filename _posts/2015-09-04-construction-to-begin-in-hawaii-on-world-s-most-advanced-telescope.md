---
layout: post
title: "Construction to begin in Hawaii on world's most advanced telescope"
date: 2015-09-04
categories:
author: University of California - Los Angeles 
tags: [Thirty Meter Telescope,Milky Way,Astronomy,Space science,Physical sciences,Science,Astronomical objects,Observational astronomy,Outer space]
---


He is the principal investigator for the Infrared Imaging Spectrograph (IRIS), one of three scientific instruments that will be ready for use with the TMT when the telescope begins operation. Keck telescopes — currently the world's largest optical and infrared telescopes — UCLA set up its infrared astrophysics lab to develop state-of-the-science instruments for them. The concept of a telescope three times larger and with nine times more light-gathering power than the Keck telescopes was first envisaged nearly 15 years ago, and UCLA has played a major role in defining the type of instruments needed for such a telescope. The TMT, Ghez said, will identify and map the orbits of fainter stars close to our black hole, extending our knowledge of physics with a fundamental test of Einstein's theory. In the distant universe, IRIS's ability to image and study the internal workings of early galaxies will represent a major breakthrough in the study of galaxy formation during the known peak period of star formation.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/uoc--ctb080514.php){:target="_blank" rel="noopener"}


