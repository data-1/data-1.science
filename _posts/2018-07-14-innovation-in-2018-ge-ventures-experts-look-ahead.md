---
layout: post
title: "Innovation in 2018: GE Ventures Experts Look Ahead"
date: 2018-07-14
categories:
author: ""
tags: [3D printing,General Electric,Technology,Cloud computing,Health care,Innovation,Computing,Electricity generation,Energy development,Analytics,Computer network,Edge computing,Renewable energy,Internet of things]
---


Image credit: Airbus Operations. Top and above: 2018 will usher in smart integration of breakthrough technologies, like this VR-controlled robot from GE Global Research, enabling business leaders to grow smarter and faster. It can even create “economies of one,” says Karen Kerr, executive managing director for advanced manufacturing enterprise at GE Ventures. Software “ container ” and “ edge” technologies will extend those benefits even further, according to Michael Dolbec, managing director of digital ventures for GE Ventures. And renewable energy can become a more prevalent source of power for it all.These new technologies are additive — making human resources more valuable and efficient.

<hr>

[Visit Link](https://www.ge.com/reports/innovation-2018-ge-ventures-experts-look-ahead/){:target="_blank" rel="noopener"}


