---
layout: post
title: "Emotional brains 'physically different' from rational ones"
date: 2016-05-09
categories:
author: Monash University
tags: [Empathy,Affect (psychology),News aggregator,Mental processes,Cognitive science,Cognition,Interdisciplinary subfields,Psychological concepts,Behavioural sciences,Cognitive psychology,Neuroscience,Brain,Concepts in metaphysics,Branches of science,Psychology]
---


People who are high on affective empathy are often those who get quite fearful when watching a scary movie, or start crying during a sad scene. Those who have high cognitive empathy are those who are more rational, for example a clinical psychologist counselling a client, Mr Eres said. The researchers used voxel-based morphometry (VBM) to examine the extent to which grey matter density in 176 participants predicted their scores on tests that rated their levels for cognitive empathy compared to affective -- or emotional -- empathy. The results showed that people with high scores for affective empathy had greater grey matter density in the insula, a region found right in the 'middle' of the brain. However, the discovery also raises new questions -- like whether people could train themselves to be more empathic, and would those areas of the brain become larger if they did, or whether we can lose our ability to empathise if we don't use it enough.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150618104153.htm){:target="_blank" rel="noopener"}


