---
layout: post
title: "Computer redesigns enzyme"
date: 2018-05-26
categories:
author: "University of Groningen"
tags: [Enzyme,Catalysis,Molecular biology,Biology,Biotechnology,Biochemistry,Physical sciences,Chemistry,Life sciences,Chemical reactions]
---


This successful proof of principle study was published in Nature Chemical Biology on 21 May. 'That's why a lot of effort is being put into modifying natural enzymes', explains Dick Janssen, Professor of Chemical Biotechnology at the Groningen Biomolecular and Biotechnology Institute. Aspartase is a deaminase, so the reaction was reversed. Janssen: 'In the end, some five to 20 mutants were actually made in the lab and tested to see if they functioned as expected.' Substrate conversions  The next step was to test successful mutant enzymes in a scaled-up setting.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/uog-cre051718.php){:target="_blank" rel="noopener"}


