---
layout: post
title: "Understanding the ocean's role in Greenland glacier melt"
date: 2014-06-23
categories:
author: Woods Hole Oceanographic Institution
tags: [Greenland ice sheet,Glacier,Nature,Environmental science,Earth phenomena,Natural environment,Environmental engineering,Applied and interdisciplinary physics,Oceanography,Hydrography,Hydrology,Physical geography,Earth sciences]
---


Sermilik Fjord, into which Helheim Glaciers drains, in August 2011. Credit: Nicholas Beaird  (Phys.org) —The Greenland Ice Sheet is a 1.7 million-square-kilometer, 2-mile thick layer of ice that covers Greenland. Currently, scientists think that the accelerated rate of ice sheet melt might be due to warmer ocean waters melting on the underside of the ice, where the glaciers extend into the ocean. From their analysis of the data, the researchers found rapid fluctuations in ocean temperature near the glaciers, resulting from surprisingly fast ocean currents in the fjords. These findings imply that changes in temperature in the ocean waters outside the fjord can be rapidly communicated to the glacier, through an efficient pumping of new water into the fjord. Credit: Fiamma Straneo, Woods Hole Oceanographic Institution  These observations of ocean conditions near outlet glaciers are one step towards a better understanding of submarine melting and the impact of the ocean on the Greenland Ice Sheet, Jackson said.

<hr>

[Visit Link](http://phys.org/news322737746.html){:target="_blank" rel="noopener"}


