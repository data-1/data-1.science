---
layout: post
title: "Making nanowires from protein and DNA"
date: 2016-06-29
categories:
author: "California Institute of Technology"
tags: [Protein,DNA,Transcription (biology),Transcription factor,DNA sequencing,Biology,Biomaterial,RNA,Structural biology,Cell biology,Macromolecules,Physical sciences,Genetics,Life sciences,Chemistry,Molecular biology,Biotechnology,Biochemistry,Biomolecules]
---


Leadership Chair of Caltech's Division of Biology and Biological Engineering, began with a computer program to design the type of protein and DNA that would work best as part of their hybrid material. Using the computer program, the researchers engineered a sequence of DNA that contained many of these protein-binding domains at regular intervals. Another unique attribute of this new protein-DNA nanowire is that it employs coassembly--meaning that the material will not form until both the protein components and the DNA components have been added to the solution. Although materials previously could be made out of DNA with protein added later, the use of coassembly to make the hybrid material was a first. The ability to create synthetic proteins allows researchers to develop proteins with new capabilities and functions, such as therapeutic proteins that target cancer.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/ciot-mnf090315.php){:target="_blank" rel="noopener"}


