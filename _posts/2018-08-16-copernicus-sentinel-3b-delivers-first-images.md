---
layout: post
title: "Copernicus Sentinel-3B delivers first images"
date: 2018-08-16
categories:
author: ""
tags: [Copernicus Programme,Satellite,Sentinel-3,Outer space,Space science,Earth sciences,Spaceflight,Spacecraft,Satellites,Bodies of the Solar System,Astronomy]
---


Applications Copernicus Sentinel-3B delivers first images 09/05/2018 22185 views 152 likes  Less than two weeks after it was launched, the Copernicus Sentinel-3B satellite has delivered its first images of Earth. Exceeding expectations, this first set of images include the sunset over Antarctica, sea ice in the Arctic and a view of northern Europe. As the workhorse mission for Copernicus, the two satellites carry the same suite of instruments to systematically measure Earth’s oceans, land, ice and atmosphere. Over oceans, it measures the temperature, colour and height of the sea surface as well as the thickness of sea ice. It will then be managed jointly, with ESA generating the land products and Eumetsat the marine products for application through the Copernicus services.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-3/Copernicus_Sentinel-3B_delivers_first_images){:target="_blank" rel="noopener"}


