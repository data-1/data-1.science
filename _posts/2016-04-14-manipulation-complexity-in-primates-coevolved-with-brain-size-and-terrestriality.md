---
layout: post
title: "Manipulation complexity in primates coevolved with brain size and terrestriality"
date: 2016-04-14
categories:
author: Heldstab, Sandra A., Department Of Anthropology, University Of Zurich, Zurich, Kosonen, Zaida K., Koski, Sonja E., University Of Helsinki
tags: [Tool use by animals,Primate,Brain size,Human,Hand,Bipedalism,Regression analysis,Brain,Complexity,Dependent and independent variables,Species,Bootstrapping (statistics),Animals]
---


This stage was then followed at later ages by performing actions on multiple objects, indicating that manipulating one object is less complex than manipulating two objects. Thumb opposition and finger individuation start to appear at 10–12 months indicating that manipulations with asynchronous digits are more complex than with synchronous digits45. This finding is surprising as previous studies suggested that patterns of asynchronous hand use are more complex than those of synchronous hand use within species5,6,8. Terrestriality  The primate pattern suggests that not only full terrestriality, but already a partly terrestrial habit may have positively affected the correlated evolution between manipulation complexity and brain size. That this simple categorization of manipulation complexity yields a consistent pattern of correlation between processing food and brain size obviously does not rule out that finding food may also play an important role in the evolution of cognitive abilities.

<hr>

[Visit Link](http://www.nature.com/articles/srep24528){:target="_blank" rel="noopener"}


