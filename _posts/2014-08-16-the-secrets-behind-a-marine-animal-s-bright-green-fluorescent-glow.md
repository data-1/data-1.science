---
layout: post
title: "The Secrets Behind a Marine Animal's Bright Green Fluorescent Glow"
date: 2014-08-16
categories:
author: Science World Report
tags: [Green fluorescent protein,Lancelet,Biology,Biotechnology,Chemistry,Life sciences,Biochemistry]
---


Now, scientists have taken a closer look at this ability and have managed to decipher the structural components related to fluorescence brightness, revealing how animals manage to glow. These fish-shaped creatures spend most of their time in shallow coastal regions burrowed beneath the sand. More specifically, the scientists found that only a few key structural differences at the nanoscale allows the sea creature to emit different brightness levels, and that the differences relate to changes in stiffness around the animals' chromophore pocket. This area of proteins is responsible for molecular transformation of light. We discovered that some of the amphioxus GFPs are able to transform blue light into green light with 100 percent efficiency (current engineered GFPs-traditionally rooted in the Cnidarian phylum-only reach 60 to 80 percent efficiency), which combines with other properties of light absorbance to make the amphioxus GFPs about five times brighter than current commercially available GFPs, resulting in effect to a huge difference, said Dimitri Deheyn, one of the researchers, in a news release.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15788/20140702/secrets-behind-marine-animals-bright-green-fluorescent-glow.htm){:target="_blank" rel="noopener"}


