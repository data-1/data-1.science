---
layout: post
title: "First molecules discovered using artificial intelligence head to development"
date: 2018-07-14
categories:
author: "InSilico Medicine"
tags: [Insilico Medicine,Drug discovery,Artificial intelligence,Ageing,Biotechnology,Medical research,Branches of science,Health care,Health,Health sciences,Technology,Medicine,Biology,Life sciences]
---


Jan.6, 2018, Baltimore, Maryland- Insilico Medicine, Inc. (Insilico), a Baltimore-based next-generation artificial intelligence (AI) company specializing in the application of deep learning for drug discovery, announces that Juvenescence.AI, its joint venture with Juvenescence Limited (Juvenescence), has licensed its first compound family for clinical development. Dr Greg Bailey, CEO of Juvenescence said: This has been a very exciting time for the team of drug developers at Juvenescence as we work with Insilico to change how drug are discovered. The team at Insilico Medicine is very excited to be working with Juvenescence. The company utilizes advances in genomics, big data analysis, and deep learning for in silico drug discovery and drug repurposing for ageing and age-related diseases. Juvenescence is focussed on developing therapeutics that alter ageing or age-related diseases.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/imi-fmd020618.php){:target="_blank" rel="noopener"}


