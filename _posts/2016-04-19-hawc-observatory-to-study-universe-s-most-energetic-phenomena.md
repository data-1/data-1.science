---
layout: post
title: "HAWC Observatory to study universe's most energetic phenomena"
date: 2016-04-19
categories:
author: University Of Maryland
tags: [High Altitude Water Cherenkov Experiment,Cosmic ray,Astronomy,Physical sciences,Space science,Physics,Physical phenomena,Astrophysics,Radiation,Nature,Science]
---


The complete array of HAWC detector tanks is seen here in December 2014. Credit: HAWC Collaboration  Supernovae, neutron star collisions and active galactic nuclei are among the most energetic phenomena in the known universe. These violent explosions produce high-energy gamma rays and cosmic rays, which can easily travel large distances—making it possible to see objects and events far outside our own galaxy. When gamma rays or cosmic rays reach Earth's atmosphere they set off a cascade of charged particles, and when these particles reach the water in HAWC's detectors, they produce a cone-shaped flash of light known as Cherenkov radiation. To envision how the detectors work, Goodman suggests imagining your computer keyboard as a detector array, with each key representing one tank. Credit: HAWC Collaboration  HAWC has been collecting data since August 2013 when it had only 111 detector tanks.

<hr>

[Visit Link](http://phys.org/news346078210.html){:target="_blank" rel="noopener"}


