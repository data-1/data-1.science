---
layout: post
title: "Scientists discover how brain signals travel to drive language performance: Their results pave the way for the treatment of aphasia and other language disorders"
date: 2018-07-01
categories:
author: "Drexel University"
tags: [Brain,Aphasia,Language,Inferior frontal gyrus,Transcranial magnetic stimulation,Cognition,Interdisciplinary subfields,Cognitive psychology,Behavioural sciences,Neuroscience,Branches of science,Psychology,Cognitive science]
---


advertisement  To see how the LIFG brain region is involved with different neural networks depending on various language tasks, the research team used a technique called transcranial magnetic stimulation, or TMS, which uses an external magnetic field to induce currents in parts of the brain along with implanted stimulators. In the second type of task, study participants were asked to name specific images or numerals presented to them. They were focused on how the language tasks affected two distinct network control features: modal controllability, which is the ability of a brain region to drive a network into difficult to reach states and boundary controllability, the theoretical ability of a brain region to guide distinct brain networks to communicate with each other. The researchers found that boundary controllability represented a process important for responding in the open-ended language tasks, when participants needed to retrieve and select a single word in the face of competing, alternative responses. Next, the research team is using the same type of techniques in stroke patients to see if stimulating certain areas of the brain can help them to improve their speech.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180621172459.htm){:target="_blank" rel="noopener"}


