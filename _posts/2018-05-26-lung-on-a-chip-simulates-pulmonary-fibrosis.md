---
layout: post
title: "Lung-on-a-chip simulates pulmonary fibrosis"
date: 2018-05-26
categories:
author: "University at Buffalo"
tags: [Lung,Pulmonary fibrosis,Pulmonary alveolus,Interstitial lung disease,Pirfenidone,Medicine,Life sciences,Health,Medical specialties,Health sciences,Clinical medicine]
---


One reason: it's difficult to mimic how the disease damages and scars lung tissue over time, often forcing scientists to employ a hodgepodge of time-consuming and costly techniques to assess the effectiveness of potential treatments. Obviously it's not an entire lung, but the technology can mimic the damaging effects of lung fibrosis. The department is a multidisciplinary unit formed by UB's School of Engineering and Applied Sciences and the Jacobs School of Medicine and Biomedical Sciences at UB. However, both drugs treat only one type of lung fibrosis: idiopathic pulmonary fibrosis. Using microlithography, the researchers printed tiny, flexible pillars made of a silicon-based organic polymer.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-05/uab-lsp052518.php){:target="_blank" rel="noopener"}


