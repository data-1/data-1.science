---
layout: post
title: "Quantum Dots Synthesized From Tea Leaves Could Be The Future Of Nanomedicine"
date: 2018-05-26
categories:
author: ""
tags: [Quantum dot,Cancer,Chemistry,Materials,Health sciences,Life sciences,Technology,Medicine]
---


We now have a clean, cheap way of manufacturing quantum dots, an advanced, microscopic tool that scientists are learning how to use to enhance everything from solar panels to cancer treatments. All they needed was green tea leaf extract, along with a couple other chemicals. But let’s back up, because that’s a lot to take in. In the past, synthesizing these quantum dots was cost and waste-intensive, but a team out of Wales’ Swansea University found a way to create quantum dots from Camellia sinensis leaf extract, which is the same plant from which green and black tea are brewed. Also, stopping the growth of 80 percent of cancer cells in a petri dish in one experiment does not a cancer cure make.

<hr>

[Visit Link](https://futurism.com/cheap-quantum-dots-synthesized-tea-leaves-future-nanomedicine/){:target="_blank" rel="noopener"}


