---
layout: post
title: "Neanderthals diet: 80% meat, 20% vegetables: Isotope studies shed a new light on the eating habits of the prehistoric humans"
date: 2016-03-15
categories:
author: Senckenberg Research Institute and Natural History Museum
tags: [Neanderthal,News aggregator,Mammoth,Woolly rhinoceros,Animals]
---


Scientists from the Senckenberg Center for Human Evolution and Palaeoenvironment (HEP) in Tübingen have studied the Neanderthals' diet. Based on the isotope composition in the collagen from the prehistoric humans' bones, they were able to show that, while the Neanderthals' diet consisted primarily of large plant eaters such at mammoths and rhinoceroses, it also included vegetarian food. The paleo-diet is one of the new trends among nutrition-conscious people -- but what exactly did the meal plan of our extinct ancestors include? The immediate vicinity also revealed the bones of several Neanderthals. Previously, it was assumed that the Neanderthals utilized the same food sources as their animal neighbors, explains Bocherens, and he adds, However, our results show that all predators occupy a very specific niche, preferring smaller prey as a rule, such as reindeer, wild horses or steppe bison, while the Neanderthals primarily specialized on the large plant-eaters such as mammoths and woolly rhinoceroses.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/03/160314091128.htm){:target="_blank" rel="noopener"}


