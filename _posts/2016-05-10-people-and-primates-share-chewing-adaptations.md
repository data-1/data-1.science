---
layout: post
title: "People and primates share chewing adaptations"
date: 2016-05-10
categories:
author: Liverpool John Moores University
tags: [Primate,Animals]
---


Chewing food not a primary mechanism of species differentiation  Scientists have gained insights into how primate species have evolved through space and time by studying the anatomy of their lower jaws in relation to diet. Mammals are generally described in relation to the food they eat, however anatomical structures devoted to chewing like mandibles (which form the lower jaw and hold the lower teeth in place) appear to change very little within taxonomic groups. In a recent article in the journal Evolution , Dr Meloro from Liverpool John Moores University (LJMU) and collaborators from Santa Maria Federal University and the University of Naples Federico II studied chewing adaptations in a broad range of primate species (including humans) by looking at size and shape variation of their lower jaw. Scientists find that although humans are very different from other primate species in the shape of their cranium, the lower jaw is not that different from that of our closest living relatives (the chimpanzee). In spite of its definition (from the latin mandere = to chew) mandibles change relatively little in their functional anatomy from one species to its closest ancestor suggesting that chewing food is not a primary mechanism of species differentiation .  Explore further Normal wear could explain differences in hominin jaw shapes

<hr>

[Visit Link](http://phys.org/news354343246.html){:target="_blank" rel="noopener"}


