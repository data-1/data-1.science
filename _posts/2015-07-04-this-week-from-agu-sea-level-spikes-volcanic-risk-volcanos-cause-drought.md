---
layout: post
title: "This week from AGU: Sea-level spikes, volcanic risk, volcanos cause drought"
date: 2015-07-04
categories:
author: American Geophysical Union 
tags: [Volcano,Types of volcanic eruptions,American Geophysical Union,Monsoon,Geophysics,Natural environment,Climate,Earth phenomena,Earth sciences,Physical geography,Nature,Applied and interdisciplinary physics]
---


The Volcanic Risk in Saudi Arabia (VORISA) project was developed as a multidisciplinary international research collaboration that integrates geological, geophysical, hazard, and risk studies in this important area. From AGU's journals: Large volcanic eruptions cause drought in eastern China  In most cases, the annual East Asian Monsoon brings heavy rains and widespread flooding to southeast China and drought conditions to the northeast. Understanding the effect of sulfate aerosols on monsoon behavior is particularly important now, as researchers explore aerosol seeding as a means of climate engineering. They find that the severity of the drought scales with the amount of aerosol injected into the atmosphere, and that it takes 4 to 5 years for precipitation to recover. Click here to register for access to AGU journal papers and Eos, the newspaper of the Earth and space sciences.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-08/agu-twf080514.php){:target="_blank" rel="noopener"}


