---
layout: post
title: "Analyze This: GE Apps Are Transforming Industries"
date: 2018-08-02
categories:
author: ""
tags: [General Electric,Power station,Technology,Solar power,Airline,Electrical grid,Analytics,Renewable energy,Digital twin,Computer network,Innovation,Culture,Train,Aircraft pilot,3D printing,Engine]
---


Last year, the unit brought in $5.2 billion in orders. More than 2,000 locomotives from nearly 30 customers around the world have already received similar GE overhauls. FlightPulse is GE’s mobile data and analytics software for pilots that merges flight data with crew scheduling.The app, which began as a collaboration between GE and Qantas , shows pilots data from every flight of their airline, including fuel savings and areas for improving fuel efficiency. Together, these tools can help pilots make smarter decisions about fuel use and safety, and they give airlines the insights they need to run more smoothly and efficiently. AirAsia expects this digitization to reduce fuel costs by 1 percent, which would translate to millions in dollars saved — not to mention emissions.As in other sectors, GE’s digital twins — there are some 1.2 million already working in the field — and APM — the baby aspirin of industrial software — help airlines monitor their fleets’ health and efficiency, schedule the optimal times for maintenance and keep planes in service longer.Digitization helps GE’s customers, certainly — but it’s bigger than that, according to Ruh.

<hr>

[Visit Link](https://www.ge.com/reports/sensors-sensibility-ge-apps-transforming-industries/){:target="_blank" rel="noopener"}


