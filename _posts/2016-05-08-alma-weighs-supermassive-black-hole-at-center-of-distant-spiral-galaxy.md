---
layout: post
title: "ALMA weighs supermassive black hole at center of distant spiral galaxy"
date: 2016-05-08
categories:
author: National Radio Astronomy Observatory
tags: [Galaxy,Milky Way,Supermassive black hole,Spiral galaxy,Black hole,Atacama Large Millimeter Array,European Southern Observatory,Space science,Physical sciences,Extragalactic astronomy,Galaxies,Physical cosmology,Science,Astronomical objects,Astronomy]
---


In comparison, the black hole at the center of the Milky Way is a lightweight, with a mass of just a few million times that of our Sun. A similar technique was used previously with the CARMA telescope to measure the mass of the black hole at the center of the lenticular galaxy NGC 4526. By studying the motion of two molecules, ALMA was able to determine that the supermassive black hole at the galactic center has a mass 140 million times greater than our sun. Credit: ALMA (NRAO/ESO/NAOJ), K. Onishi; NASA/ESA Hubble Space Telescope, E. Sturdivant; NRAO/AUI/NSF  While NGC 4526 is a lenticular galaxy, NGC 1097 is a barred spiral galaxy. Another technique is to track the motion of ionized gas in a galaxy's central bulge, but this technique is best suited to the study of elliptical galaxies, leaving few options when it comes to measuring the mass of supermassive black holes in spiral galaxies.

<hr>

[Visit Link](http://phys.org/news353817207.html){:target="_blank" rel="noopener"}


