---
layout: post
title: "NASA's Van Allen probes revolutionize view of radiation belts"
date: 2016-01-27
categories:
author: NASA/Goddard Space Flight Center
tags: [Van Allen radiation belt,Van Allen Probes,Earths magnetic field,Electron,Space science,Astronomy,Physical sciences,Physics,Nature,Outer space,Science,Electromagnetism]
---


The researchers found that the inner belt -- the smaller belt in the classic picture of the belts -- is much larger than the outer belt when observing electrons with low energies, while the outer belt is larger when observing electrons at higher energies. Geomagnetic storms can increase or decrease the number of energetic electrons in the radiation belts temporarily, though the belts return to their normal configuration after a time. Often, the outer electron belt expands inwards toward the inner belt during geomagnetic storms, completely filling in the slot region with lower-energy electrons and forming one huge radiation belt. The twin Van Allen Probes satellites expand the range of energetic electron data we can capture. But the Van Allen Probes measure hundreds.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-01/nsfc-nva011916.php){:target="_blank" rel="noopener"}


