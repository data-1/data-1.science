---
layout: post
title: "Machine learning finds tumor gene variants and sensitivity to drugs in The Cancer Genome Atlas"
date: 2018-07-18
categories:
author: "University of Pennsylvania School of Medicine"
tags: [Cancer,The Cancer Genome Atlas,Precision medicine,Genetics,Mutation,Neoplasm,Medical specialties,Biology,Health sciences,Life sciences,Health,Causes of death,Neoplasms,Diseases and disorders,Medicine,Clinical medicine,Biotechnology]
---


Other molecular information from patients may reveal these so-called hidden responders, according to a Penn Medicine study in Cell Reports this week. The findings are published alongside several papers in other Cell journals this week examining molecular pathways using The Cancer Genome Atlas (TCGA). Changes in the normal function of Ras proteins -- mutations which are responsible for 30 percent of all cancers -- can power cancer cells to grow and spread. This model was trained on genetic data from human tumors in The Cancer Genome Atlas and was able to predict response to certain inhibitors that affect cancers with overactive Ras signaling in an encyclopedia of cancer cell lines, Greene said. They are working together to mesh her identification of compounds that target tumors with runaway Ras activity and tumor data (analyzed by machine learning) to find patients who could benefit from these potential cancer drugs.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/04/180409161300.htm){:target="_blank" rel="noopener"}


