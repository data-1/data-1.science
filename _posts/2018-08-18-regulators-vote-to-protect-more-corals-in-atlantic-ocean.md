---
layout: post
title: "Regulators vote to protect more corals in Atlantic Ocean"
date: 2018-08-18
categories:
author: "Patrick Whittle"
tags: [Fishing,Northeast Canyons and Seamounts Marine National Monument]
---


Credit: Wikipedia, CC BY 2.5  Federal fishing regulators on Tuesday approved a compromise they said would expand the amount of coral habitat preserved in the Atlantic Ocean while also protecting fishing interests. Future actions could still take place. Northeast Canyons and Seamounts Marine National Monument, created in 2016, protected about 5,000 square miles (13,000 square kilometers) off New England, but its future is uncertain because of legal challenges and a review by President Donald Trump's administration. On Tuesday, conservation group Oceana said the newly approved protections are a step in the right direction but the council could have done more to keep fishing gear that dredges the ocean floor away from corals. Fishing groups at Tuesday's meeting said they were glad the council backed off protections that would have displaced more fishermen from the area.

<hr>

[Visit Link](https://phys.org/news/2018-01-vote-corals-atlantic-ocean.html){:target="_blank" rel="noopener"}


