---
layout: post
title: "Nietzsche on Dreams as an Evolutionary Time Machine for the Human Brain"
date: 2016-04-21
categories:
author: Maria Popova
tags: [Dream,Mind,Human,Thought,Sleep,Cognitive science,Psychological concepts,Concepts in the philosophy of mind,Mental processes,Neuroscience,Science,Psychology,Metaphysics,Metaphysics of mind,Philosophy,Cognition,Concepts in metaphysics]
---


“We feel dreamed by someone else, a sleeping counterpart,” the poet Mark Strand wrote in his beautiful ode to dreams. “The dead still live: for they appear to the living in dreams.” So reasoned mankind at one time, and through many thousands of years. This, Nietzsche argues, is how superstitions and religious mythologies may have originated:  The function of the brain which is most encroached upon in slumber is the memory; not that it is wholly suspended, but it is reduced to a state of imperfection as, in primitive ages of mankind, was probably the case with everyone, whether waking or sleeping. Just like the dreaming self contains vestiges of every self we’ve inhabited since childhood, to be resurrected in sleep, Nietzsche argues that the dreaming brain contains vestiges of the primitive stages of the human brain, when our cognitive capacity for problem-solving was far more limited and unmoored from critical thinking. What really takes place is a sort of reasoning from effect back to cause.

<hr>

[Visit Link](https://www.brainpickings.org/2016/04/21/nietzsche-on-dreams/){:target="_blank" rel="noopener"}


