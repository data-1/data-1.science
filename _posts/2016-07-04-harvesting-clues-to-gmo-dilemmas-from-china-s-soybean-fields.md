---
layout: post
title: "Harvesting clues to GMO dilemmas from China's soybean fields"
date: 2016-07-04
categories:
author: "Michigan State University"
tags: [Soybean,Genetically modified food,Agriculture,Maize,Food and drink,Primary sector of the economy,Food industry,Industries (economics),Primary industries,Natural environment,Natural resources]
---


China's struggle - mirrored across the globe -- to balance public concern over the safety of genetically modified (GM) crops with a swelling demand for affordable food crops has left a disconnect: In China's case, shrinking fields of domestic soybean - by law non-GM -- and massive imports of cheaper soybeans that are the very GM crop consumers profess to shun. Researchers at Michigan State University (MSU) take a first look at how China's soybean farmers are reacting when their crop struggles in the global market, and their choices' global environmental implications. Researchers say these farming choices may offer solutions to a national dilemma. Many studies have focused on the global expansion of GM crops. There they found farmers converting fields from soybean to corn, but not without environmental consequence.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/msu-hct091515.php){:target="_blank" rel="noopener"}


