---
layout: post
title: "NASA image: Caribbean Sea viewed from the International Space Station"
date: 2015-10-05
categories:
author: "$author"  
tags: [NASA,Privacy,International Space Station,Computing,Internet,Information technology,Technology,Cyberspace,Communication]
---


Credit: NASA  From the Earth-orbiting International Space Station, flying some 225 nautical miles above the Caribbean Sea in the early morning hours of July 15, NASA astronaut Reid Wiseman photographed this north-looking panorama. The image includes parts of Cuba, the Bahamas and Florida, and even runs into several other areas in the southeastern U.S. The long stretch of lights to the left of center frame gives the shape of Miami. Explore further Image: Arrival at the International Space Station

<hr>

[Visit Link](http://phys.org/news324885674.html){:target="_blank" rel="noopener"}


