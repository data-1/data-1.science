---
layout: post
title: "The Human Skeletal System"
date: 2016-05-31
categories:
author: "Kim Ann Zimmermann"
tags: [Bone,Skeleton,Osteoblast,Human skeleton,Osteocyte,Pelvis,Morphology (biology),Skeletal system,Anatomy,Musculoskeletal system]
---


In addition to all those bones, the human skeletal system includes a network of tendons, ligaments and cartilage that connect the bones together. The skeletal system provides the structural support for the human body and protects our organs. Bones are further classified by their shape: long, short, flat, irregular or sesamoid, according to SEER. Infant's bones contain all red bone marrow to produce enough blood cells to keep up with the youngsters' growth. There are four main types of cells within bones: Osteoblasts, osteocytes, osteoclasts and lining cells, according to the NCBI.

<hr>

[Visit Link](http://www.livescience.com/22537-skeletal-system.html){:target="_blank" rel="noopener"}


