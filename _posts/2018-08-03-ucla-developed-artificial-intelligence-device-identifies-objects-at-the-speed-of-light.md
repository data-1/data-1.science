---
layout: post
title: "UCLA-developed artificial intelligence device identifies objects at the speed of light"
date: 2018-08-03
categories:
author: "UCLA Samueli School of Engineering"
tags: [Neural network,Optics,Artificial neural network,Computer network,Computer,3D printing,Robotics,Deep learning,Technology,Engineering,Camera,Artificial intelligence,Light,Machine,Science,Computing,Branches of science]
---


A team of UCLA electrical and computer engineers has created a physical artificial neural network -- a device modeled on how the human brain works -- that can analyze large volumes of data and identify objects at the actual speed of light. Called a diffractive deep neural network, it uses the light bouncing from the object itself to identify that object in as little time as it would take for a computer to simply see the object. The researchers then trained the network using a computer to identify the objects in front of it by learning the pattern of diffracted light each object produces as the light from that object passes through the device. To do that, they placed images in front of a terahertz light source and let the device see those images through optical diffraction. Because its components can be created by a 3D printer, the artificial neural network can be made with larger and additional layers, resulting in a device with hundreds of millions of artificial neurons.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/usso-uai080218.php){:target="_blank" rel="noopener"}


