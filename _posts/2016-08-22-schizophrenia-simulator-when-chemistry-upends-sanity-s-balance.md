---
layout: post
title: "Schizophrenia simulator: When chemistry upends sanity's balance"
date: 2016-08-22
categories:
author: "Georgia Institute of Technology"
tags: [Schizophrenia,Working memory,Neurochemistry,Dopamine,Memory,Brain,Cognitive science,Neuroscience]
---


Cognitive symptoms were actually associated with schizophrenia before symptoms like hallucinations became the focus, said Eberhard Voit, a Georgia Tech biomedical engineer and a Georgia Research Alliance eminent scholar, who supervised the modeling effort. That's where Qi's and Voit's computational model comes in. Mind map  The Georgia Tech researchers collected studies on brain chemistry in schizophrenia from nearly 50 labs around the world, and mined the data. They arrived at a novel map of the brain chemistry behind working memory dysfunction in schizophrenia. With a few months' work, a graphic user interface could be constructed to allow doctors and researchers to easily use Georgia Tech's new computational model.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-08/giot-ssw080316.php){:target="_blank" rel="noopener"}


