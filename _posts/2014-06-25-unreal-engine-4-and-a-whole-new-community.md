---
layout: post
title: "Unreal Engine 4 and a whole new community"
date: 2014-06-25
categories:
author: ""        
tags: [Unreal Engine,Epic Games,Computing,Digital media,Software,Software development,Video gaming,Electronic publishing,Technology]
---


One month after Unreal Engine 4’s online launch, Epic Games released its 4.1 update, bringing PlayStation 4 and Xbox One support to subscribers at no additional cost, along with SteamOS and Linux features, mobile upgrades, and more than 100 improvements based on community feedback. The latter four platforms are still considered preview efforts for source code users at the time of writing. With the engine’s full C++ source code – and even live source code access – available to subscribers, developers are propelling Epic’s efforts forward and sharing exciting new projects at a rapid pace. “Not only in terms of the number of developers who immediately jumped in and started building, but also the amount of contributions they’ve started to make back to the engine itself via the GitHub network.”        Epic highlights noteworthy community projects – spanning games, VR apps and tools – via its weekly Twitch broadcast. Quixel’s Wiktor Öhman created a Deus Ex-inspired scene, which is coming to Marketplace for free, listed as Sci-Fi Scene.

<hr>

[Visit Link](http://linuxgamenews.com/post/89790019367){:target="_blank" rel="noopener"}


