---
layout: post
title: "Offshore wind to bring $3.4 billion to British economy"
date: 2015-06-02
categories:
author: Lisa M. Hamilton  
tags: []
---


Calgary, Alberta (UPI) Jul 16, 2013 - The dedication of a 300-megawatt wind energy project in Alberta underscores the provincial commitment to renewable energy, a provincial leader said. The 300 MW of power expected from the project will help meet the energy needs of about 140,000 average households. The Blackspring Ridge project is a joint effort between industry and government that demonstrates Alberta's commitment to renewable energy development, McQueen said in a statement. Enbridge is a leader in the project's development. The company says it's the second largest developer of wind energy in Canada.

<hr>

[Visit Link](http://feeds.importantmedia.org/~r/IM-cleantechnica/~3/M1kIdULtiro/){:target="_blank" rel="noopener"}


