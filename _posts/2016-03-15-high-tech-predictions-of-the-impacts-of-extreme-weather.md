---
layout: post
title: "High-tech predictions of the impacts of extreme weather"
date: 2016-03-15
categories:
author: Constanze Böttcher
tags: [Weather forecasting,Weather,Flood,Infrastructure,Technology,Computing]
---


When it comes to rainfall in particular, the impact of extreme weather is hard to forecast, says Herman Russchenberg, director of the TU Delft Climate Institute, the Netherlands. In the future, high-tech instruments such as the next generation of weather radars will provide more detailed data, Russchenberg hopes. To actually assess the consequences of severe weather, more knowledge, such as on the condition of infrastructure, is also necessary. Besides, you need more complex models to extrapolate this data and to assess how certain infrastructure reacts under extreme weather conditions, he adds. Satellite images are improving and drone technology will provide more detailed information on infrastructure networks.

<hr>

[Visit Link](http://phys.org/news/2016-03-high-tech-impacts-extreme-weather.html){:target="_blank" rel="noopener"}


