---
layout: post
title: "Personalized medicine will employ computer algorithms"
date: 2016-06-24
categories:
author: "ITMO University"
tags: [Microbiota,Metagenomics,DNA sequencing,Biology,Life sciences,Biotechnology,Genetics,Medical specialties,Medicine,Clinical medicine]
---


However, any living organism contains another gene sequence that is called the metagenome.It is the total DNAcontent of the many different microorganisms that inhabit the same environment - bacteria, fungi, and viruses. The software tool developed by the scientists and called MetaFast is able to conduct a rapid comparative analysis of large amounts of metagenomes. The newly developed approach allows us to do two things - find all the possible gene sequences, even if they were previously unknown (the program collects them from fragmentsof genomic reads), and at the same time identify metagenomic patterns that distinguish one patient from another, e.g. people with and without a disease, says Dmitry Alexeev, the leader of the project and head of MIPT's Laboratory of Complex Biological Systems. Our approach enables us to work with completely unknown material and still obtain results. MetaFast is not limited to detecting pathogens.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/iu-pmw061516.php){:target="_blank" rel="noopener"}


