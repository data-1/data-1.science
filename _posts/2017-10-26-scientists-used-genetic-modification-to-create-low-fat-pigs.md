---
layout: post
title: "Scientists Used Genetic Modification to Create Low-Fat Pigs"
date: 2017-10-26
categories:
author:  
tags: [Genetic engineering,Pig,CRISPR gene editing,Genetics,Health,Biology,Food and drink,Life sciences,Health sciences,Biotechnology]
---


Low-Fat Pigs  Chinese scientists have successfully used CRISPR to create twelve healthy pigs that have around 24 percent less body fat than normal. The scientists took the gene from a mouse and used CRISPR to introduce it into pig cells. These cells were used to create in excess of 2,553 cloned pig embryos. These animals were found to be completely healthy, and one even managed to mate and produce healthy offspring. Despite the fact that studies have demonstrated that genetically modified foods pose no health risks, there's still a great deal of pushback against their production and sale.

<hr>

[Visit Link](https://futurism.com/scientists-used-genetic-modification-to-create-low-fat-pigs/){:target="_blank" rel="noopener"}


