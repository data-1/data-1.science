---
layout: post
title: "The quantum Cheshire cat: Scientists separate a particle from its properties"
date: 2015-05-22
categories:
author: Chapman University 
tags: [Neutron,Quantum mechanics,Spin (physics),Magnetism,Particle,Interferometry,Magnetic field,Institut LaueLangevin,Sandu Popescu,Theoretical physics,Physics,Science,Physical sciences,Applied and interdisciplinary physics]
---


The Quantum Cheshire Cat: Can a particle be separated from its properties? On July 29, the prestigious journal, Nature Communications, published the results of the first Cheshire Cat experiment, separating a neutron from its magnetic field, conducted by Chapman University in Orange, CA, and Vienna University of Technology. The idea of the Quantum Cheshire Cat was first discovered by Chapman's Prof. Yakir Aharonov and first published by Aharonov's collaborator, Prof. Jeff Tollaksen (also at Chapman University), in 2001. At Different Places at Once  According to the law of quantum physics, particles can be in different physical states at the same time. These neutrons, which are found to have a spin parallel to its direction of motion, must clearly have traveled along the upper path – only there, the neutrons have this spin state.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/cu-tqc072814.php){:target="_blank" rel="noopener"}


