---
layout: post
title: "CO2 fertilization greening the earth"
date: 2016-04-26
categories:
author: Boston University
tags: [Climate change,Carbon dioxide,Earth,Atmosphere of Earth,United Nations Framework Convention on Climate Change,Environmental impact,Earth sciences,Nature,Natural environment,Climate variability and change,Environmental issues with fossil fuels,Physical geography,Global environmental issues,Societal collapse,Climate,Human impact on the environment,Environmental issues,Change,Earth phenomena]
---


BOSTON -- An international team of 32 authors from 24 institutions in eight countries has just published a study titled Greening of the Earth and its Drivers in the journal Nature Climate Change showing significant greening of a quarter to one-half of the Earth's vegetated lands using data from the NASA-MODIS and NOAA-AVHRR satellite sensors of the past 33 years. The greening represents an increase in leaves on plants and trees. Green leaves produce sugars using energy in the sunlight to mix carbon dioxide (CO2) drawn in from the air with water and nutrients pumped in from the ground. We were able to tie the greening largely to the fertilizing effect of rising atmospheric CO2 concentration by tasking several computer models to mimic plant growth observed in the satellite data, says co-author Prof. Ranga Myneni of the Department of Earth and Environment at Boston University, USA. The study also identified climate change, nitrogen fertilization and land management as other important reasons.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/bu-cfg042216.php){:target="_blank" rel="noopener"}


