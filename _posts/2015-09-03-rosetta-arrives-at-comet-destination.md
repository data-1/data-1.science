---
layout: post
title: "Rosetta arrives at comet destination"
date: 2015-09-03
categories:
author: "$author"   
tags: [Rosetta (spacecraft),European Space Agency,Comet,Gravity assist,Philae (spacecraft),Astronomical objects,Local Interstellar Cloud,Space exploration,Sky,Flight,Discovery and exploration of the Solar System,Space probes,Space vehicles,Spacecraft,Planetary science,Astronautics,Bodies of the Solar System,Spaceflight,Solar System,Space science,Astronomy,Outer space]
---


Science & Exploration Rosetta arrives at comet destination 06/08/2014 227480 views 724 likes  After a decade-long journey chasing its target, ESA’s Rosetta has today become the first spacecraft to rendezvous with a comet, opening a new chapter in Solar System exploration. Comet 67P/Churyumov–Gerasimenko and Rosetta now lie 405 million kilometres from Earth, about half way between the orbits of Jupiter and Mars, rushing towards the inner Solar System at nearly 55 000 kilometres per hour. Rosetta will accompany it for over a year as they swing around the Sun and back out towards Jupiter again. At the same time, more of the suite of instruments will provide a detailed scientific study of the comet, scrutinising the surface for a target site for the Philae lander. ESA has Cooperation Agreements with eight other Member States of the EU.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Rosetta/Rosetta_arrives_at_comet_destination){:target="_blank" rel="noopener"}


