---
layout: post
title: "Study reveals human body has gone through four stages of evolution"
date: 2015-09-02
categories:
author: Binghamton University
tags: [Human evolution,Archaeological site of Atapuerca]
---


SH-selected postcranial traits. Credit: PNAS, doi/10.1073/pnas.1514828112  Research into 430,000-year-old fossils collected in northern Spain found that the evolution of the human body's size and shape has gone through four main stages, according to a paper published this week. The Atapuerca humans shared many anatomical features with the later Neanderthals not present in modern humans, and analysis of their postcranial skeletons (the bones of the body other than the skull) indicated that they are closely related evolutionarily to Neanderthals. The Atapuerca fossils represent the third stage, with tall, wide and robust bodies and an exclusively terrestrial bipedalism, with no evidence of arboreal behaviors. Thus, this body form seems to have been present in the genus Homo for over a million years.

<hr>

[Visit Link](http://phys.org/news/2015-08-reveals-human-body-stages-evolution.html){:target="_blank" rel="noopener"}


