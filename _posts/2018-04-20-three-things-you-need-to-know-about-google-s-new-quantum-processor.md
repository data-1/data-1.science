---
layout: post
title: "Three Things You Need to Know About Google's New Quantum Processor"
date: 2018-04-20
categories:
author: ""
tags: [Quantum computing,Computer,Quantum supremacy,Central processing unit,Computers,Office equipment,Applied mathematics,Computing,Technology,Computer science,Information Age,Computer engineering,Computer architecture,Computer hardware,Branches of science,Information technology]
---


It Needs To Make Fewer Errors  In spite of recent advances, today's qubits are still unstable, and hardware needs to be sturdy to run them. The blog post didn't state Bristlecone's error rate, but Google has made it clear that the company strives to improve upon its previous results. It's A Serious Push For Quantum Greatness  Researchers are working so hard to bring us quantum computers because they believe the devices will inevitably outperform classical supercomputers. When it does happen, quantum supremacy will be a watershed moment for the field, according to Google's blog post on Bristlecone. Bristlecone doesn't quite get us there.

<hr>

[Visit Link](https://futurism.com/googles-new-quantum-processor/){:target="_blank" rel="noopener"}


