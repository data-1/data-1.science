---
layout: post
title: "New electron microscope method detects atomic-scale magnetism"
date: 2016-06-21
categories:
author: "Oak Ridge National Laboratory"
tags: [Microscope,Electron microscope,Electron,Microscopy,Magnetism,Chemistry,Electromagnetism,Applied and interdisciplinary physics,Electromagnetic radiation,Science,Scientific techniques,Atomic molecular and optical physics,Physical sciences,Physical chemistry,Atomic physics,Scientific method,Materials science,Optics]
---


A new counterintuitive electron microscope approach can collect magnetic signals through the introduction of aberrations. The aberrated probe (right) results in imaging and spectra with lower spatial resolution than a traditionally corrected probe but can pick up a magnetic signature. ORNL's Juan Carlos Idrobo helped develop an electron microscopy technique to measure magnetism at the atomic scale. Credit: ORNL  This is the first time someone has used aberrations to detect magnetic order in materials in electron microscopy, Idrobo said. Juan Carlos Idrobo et al. Detecting magnetic ordering with atomic size electron probes,(2016).

<hr>

[Visit Link](http://phys.org/news/2016-06-electron-microscope-method-atomic-scale-magnetism.html){:target="_blank" rel="noopener"}


