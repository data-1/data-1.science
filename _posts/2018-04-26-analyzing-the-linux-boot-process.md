---
layout: post
title: "Analyzing the Linux boot process"
date: 2018-04-26
categories:
author: "Alison Chaiken"
tags: [Linux kernel,Das U-Boot,Booting,Unified Extensible Firmware Interface,Initial ramdisk,Kernel (operating system),Advanced Configuration and Power Interface,Devicetree,Standard streams,Executable and Linkable Format,GNU GRUB,Vmlinux,File system,Intelligent Platform Management Interface,Symmetric multiprocessing,Intel Management Engine,Thread (computing),Command-line interface,GNU Debugger,Package manager,Software development,Office equipment,Software engineering,Operating system technology,Computer hardware,Software,Computer science,Computing,Computer architecture,Computers,Technology,System software,Computer engineering]
---


But how does the kernel itself get started? So the kernel must start up something like other Linux ELF binaries ... but how do userspace programs actually start? The ELF interpreter provisions a binary with the needed resources by calling _start() , a function available from the glibc source package that can be inspected via GDB. From start_kernel() to PID 1  The kernel's hardware manifest: the device-tree and ACPI tables  At boot, the kernel needs information about the hardware beyond the processor type for which it has been compiled. The kernel learns what hardware it must run at each boot by reading these files.

<hr>

[Visit Link](https://opensource.com/article/18/1/analyzing-linux-boot-process){:target="_blank" rel="noopener"}


