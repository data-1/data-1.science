---
layout: post
title: "IRIS releases new imagery of Mercury transit"
date: 2016-05-21
categories:
author: NASA/Goddard Space Flight Center
tags: [Solar Dynamics Observatory,Goddard Space Flight Center,Mercury (planet),Interface Region Imaging Spectrograph,NASA,Astronomical objects,Scientific observation,Observational astronomy,Space exploration,Space program of the United States,Solar System,Physical sciences,Space science,Astronomy,Outer space,Science,Spaceflight,Bodies of the Solar System]
---


On May 9, 2016, a NASA solar telescope called the Interface Region Imaging Spectrograph, or IRIS, observed Mercury crossing in front of the sun -- an astronomical phenomenon known as a Mercury transit. This movie shows a composite of the IRIS imagery, in the inset, and imagery from NASA's Solar Dynamics Observatory, or SDO, as the golden background with a view of Mercury's transit moving from left to right across the bottom. The thin, dark line that appears in the IRIS imagery is a slit that helps focus incoming light for an instrument called a spectrograph. Lockheed Martin designed the IRIS observatory and manages the mission for NASA. For more on Mercury transit: http://www.nasa.gov/transit

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/nsfc-irn051816.php){:target="_blank" rel="noopener"}


