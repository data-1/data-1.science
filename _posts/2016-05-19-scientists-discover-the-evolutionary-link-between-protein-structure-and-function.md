---
layout: post
title: "Scientists discover the evolutionary link between protein structure and function"
date: 2016-05-19
categories:
author: University Of Illinois At Urbana-Champaign
tags: [Protein,Protein domain,Biology,Evolution,Modularity,Organism,Cell (biology),Biological network,Biotechnology]
---


Geologists have found remnants of life preserved in rock billions of years old. For the first time, we have traced evolution onto a biological network, Caetano-Anollés notes. When loops come together, they create active sites, or molecular pockets, which give proteins their function. This recruitment is important for understanding biological diversity, Caetano-Anollés says. In our study, we are showcasing how you can do it with a very small network, Caetano-Anollés explains.

<hr>

[Visit Link](http://phys.org/news/2016-05-scientists-evolutionary-link-protein-function.html){:target="_blank" rel="noopener"}


