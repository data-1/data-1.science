---
layout: post
title: "How diet shaped human evolution: The Neanderthal rib-cage and pelvis expanded to adapt to a high-protein diet in Ice-Age Europe"
date: 2016-04-05
categories:
author: American Friends of Tel Aviv University
tags: [Neanderthal,Human evolution,News aggregator,Human,Kidney]
---


Homo sapiens, the ancestor of modern humans, shared the planet with Neanderthals, a close, heavy-set relative that dwelled almost exclusively in Ice-Age Europe, until some 40,000 years ago. According to the research, the bell-shaped Neanderthal rib-cage or thorax had to evolve to accommodate a larger liver, the organ responsible for metabolizing great quantities of protein into energy. Seeing evolution from a new angle  The anatomical differences between the thoraxes and pelvises of Homo sapiens and Neanderthals have been well-known for many years, but now we're approaching it from a new angle -- diet, said Prof. Avi Gopher. This situation triggered an evolutionary adaptation to a high-protein diet -- an enlarged liver, expanded renal system and their corresponding morphological manifestations. The solution, therefore, was to consume more fat and more carbohydrates when they were seasonally available.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/03/160329132245.htm){:target="_blank" rel="noopener"}


