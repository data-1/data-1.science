---
layout: post
title: "A Look Inside China’s Chart-Topping New Supercomputer"
date: 2016-06-21
categories:
author: "Nicole Hemsoth"
tags: [FLOPS,Supercomputer,Tianhe-2,Sunway SW26010,Information Age,Supercomputing,Information technology,Electronics industry,Classes of computers,Office equipment,Technology,Computer architecture,Computer science,Computer engineering,Computer hardware,Computing,Computers]
---


The SW26010 and the Sunway TaihuLight system has been engineered for super-efficient floating point performance. This creates a total of 260 cores per unit and it’s built from there. Four of those go in a cabinet, and the full system stretches to forty cabinets total with an interconnect that’s built into the chip (which is referred to as the custom ‘network on a chip” interconnect) and also an interconnect for hooking everything together to form a supernode. “Sunway has built their own interconnect. The efficiency figures below are for the LINPACK benchmark and count processor, memory, and the interconnect.

<hr>

[Visit Link](http://www.nextplatform.com/2016/06/20/look-inside-chinas-chart-topping-new-supercomputer/){:target="_blank" rel="noopener"}


