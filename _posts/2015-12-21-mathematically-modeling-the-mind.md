---
layout: post
title: "Mathematically modeling the mind"
date: 2015-12-21
categories:
author: American Institute of Physics
tags: [Mind,Memory,Cognition,Mental disorder,Thought,Chaos theory,American Association for the Advancement of Science,Attention,Psychological concepts,Neuropsychology,Branches of science,Concepts in metaphysics,Behavioural sciences,Mental processes,Cognitive science,Interdisciplinary subfields,Neuroscience,Psychology,Cognitive psychology]
---


New model described in the journal CHAOS represents how the mind processes sequential memory and may help understand psychiatric disorders  WASHINGTON, D.C., October 20, 2015 -- Try to remember a phone number, and you're using what's called your sequential memory. In particular, he and a group of researchers have now mathematically modeled how the mind switches among different ways of thinking about a sequence of objects, events, or ideas, which are based on the activity of so-called cognitive modes. When these perspectives describe cognitive modes, they're called modalities. When the mind has sequential thoughts, the cognitive modes underlying neural activity switches among different modalities. This switching is called a binding process, because the mind binds each cognitive mode to a certain modality.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-10/aiop-mmt101615.php){:target="_blank" rel="noopener"}


