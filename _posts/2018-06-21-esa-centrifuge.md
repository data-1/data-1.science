---
layout: post
title: "ESA centrifuge"
date: 2018-06-21
categories:
author: ""
tags: [Centrifuge,European Space Agency,International Space Station,Astronomy,Human spaceflight,Spacecraft,Flight,Science,Astronautics,Outer space,Spaceflight,Space science]
---


A decade ago, as Europe’s Columbus laboratory module was attached to the International Space Station for microgravity research, ESA’s Large Diameter Centrifuge began offering lengthy experiments in hypergravity. The principle is simple: the 8 m-diameter four-arm centrifuge is set spinning at up to 67 revs per minute, creating up to 20 times normal Earth gravity for weeks or even months at a time. As part of ESA’s Life and Physical Sciences Instrumentation Laboratory at the Agency’s technical centre in the Netherlands, the centrifuge’s development was supported by the Dutch government and its use is encouraged by the European Low Gravity Research Association. For the last decade it has been a place of pilgrimage for researchers, including student experimenters on regular Spin Your Thesis campaigns. Tomorrow sees a celebration of the centrifuge’s first decade, giving its team the opportunity to hear from their users about desired upgrades and new research ideas.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2018/01/ESA_centrifuge){:target="_blank" rel="noopener"}


