---
layout: post
title: "How climate change will affect western groundwater"
date: 2016-03-15
categories:
author: University of Arizona
tags: [Groundwater recharge,Groundwater,Aquifer,Nature,Environmental technology,Earth phenomena,Transparent materials,Solvents,Water and the environment,Lubricants,Hydroxides,Natural environment,Refrigerants,Environment,Environmental engineering,Environmental science,Liquid dielectrics,Water,Hydrology,Physical geography,Earth sciences]
---


By 2050 climate change will increase the groundwater deficit even more for four economically important aquifers in the western U.S., reports a University of Arizona-led team of scientists. Aquifers in the southern tier of the West are all expected to see slight-to-significant decreases in recharge as the climate warms, Meixner said. To synthesize existing knowledge and predict how climate change would affect western groundwater, Meixner gathered 16 experts in climate change and in hydrology of the western U.S. The team studied eight economically important western aquifers for which studies about their groundwater recharge budgets existed. Much of the San Pedro's current recharge comes from mountain-system recharge, which the scientists expect will dwindle as more precipitation falls in the mountains as rain rather than snow and as the region dries.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-02/uoa-hcc021716.php){:target="_blank" rel="noopener"}


