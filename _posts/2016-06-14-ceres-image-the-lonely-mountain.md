---
layout: post
title: "Ceres image: The lonely mountain"
date: 2016-06-14
categories:
author: ""
tags: [Dawn (spacecraft),Ceres (dwarf planet),Privacy,NASA,Technology]
---


Credit: NASA/JPL-Caltech/UCLA/MPS/DLR/IDA  NASA's Dawn spacecraft spotted this tall, conical mountain on Ceres from a distance of 915 miles (1,470 kilometers). Its perimeter is sharply defined, with almost no accumulated debris at the base of the brightly streaked slope. The image was taken on August 19, 2015. The resolution of the image is 450 feet (140 meters) per pixel. Explore further Dawn spirals closer to Ceres, returns a new view

<hr>

[Visit Link](http://phys.org/news/2015-08-ceres-image-lonely-mountain.html){:target="_blank" rel="noopener"}


