---
layout: post
title: "Nano-beaker offers insight into the condensation of atoms"
date: 2015-07-07
categories:
author: ""   
tags: [Atom,Chemical bond,Gas,Nanotechnology,Microscope,News,Xenon,Scanning tunneling microscope,Quantum tunnelling,Noble gas,Chemistry,Physical sciences,Applied and interdisciplinary physics,Technology,Materials]
---


Led by the Swiss Nanoscience Institute and the Department of Physics at the University of Basel, the team was able to monitor for the first time how xenon atoms condensate in microscopic measuring beakers, or quantum wells, thereby enabling key conclusions to be drawn as to the nature of atomic bonding. The researchers published their results in the journal Nature Communications. The researchers allowed atoms of the noble gas xenon to condensate in quantum wells and monitored the resulting accumulations using a scanning tunneling microscope. Conclusions about the nature of bonding  The images and structures of nano-condensates recorded for the first time allow key conclusions to be drawn as to the nature of the physical bonds formed by the xenon atoms. We can also use it to research other atoms and the way that they bond.

<hr>

[Visit Link](http://www.nanodaily.com/reports/Nano_beaker_offers_insight_into_the_condensation_of_atoms_999.html){:target="_blank" rel="noopener"}


