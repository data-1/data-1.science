---
layout: post
title: "Scientists isolate genes that delay Alzheimer's disease"
date: 2016-03-16
categories:
author:  
tags: []
---


Olga Deacon, who has dementia, speaks with her granddaughter, Chris Boyce, in a replica 1940s kitchen, Friday, Nov. 6, 2015, at The Easton Home in Easton, Pa. Nursing homes and assisted living facilities are increasingly using sight, sound and other sensory cues to stimulate memory in people with Alzheimer's disease and other forms of dementia. (AP Photo/Matt Rourke)  A team of researchers have identified a network of nine genes that play a key role in the onset of Alzheimer’s disease and these findings could help scientists develop new treatments to delay the onset of the disease in a study of a family of 5,000 people in Columbia, scientists identified genes that delayed the disease and others that accelerated it, and by how much. Arcos-Burgos at the John Curtin School of Medical Research said that if they could work out how to decelerate the disease, then they could have a profound impact. The study is published in the Journal Molecular Psychiatry.

<hr>

[Visit Link](http://www.thehindu.com/sci-tech/technology/scientists-isolate-genes-that-delay-alzheimers-disease/article7941215.ece?utm_source=RSS_Feed&utm_medium=RSS&utm_campaign=RSS_Syndication){:target="_blank" rel="noopener"}


