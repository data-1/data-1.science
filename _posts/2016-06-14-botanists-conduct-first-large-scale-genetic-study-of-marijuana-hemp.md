---
layout: post
title: "Botanists conduct first large-scale genetic study of marijuana, hemp"
date: 2016-06-14
categories:
author: "University Of British Columbia"
tags: [Cannabis,Cannabis strain,Cannabaceae]
---


Credit: Jonathan Page  A study by Canadian researchers is providing a clearer picture of the evolutionary history and genetic organization of cannabis, a step that could have agricultural, medical and legal implications for this valuable crop. Even though hemp and marijuana are important crops, knowledge about cannabis is lacking because of its status as a controlled drug, said Jonathan Page, a University of British Columbia botanist who co-led the first large-scale study of the genetic diversity of cannabis. For example, a sample of Jamaican Lambs Bread, which is classified as C. sativa, was almost identical at a genetic level to a C. indica strain from Afghanistan. Cannabis breeders and growers often indicate the percentage of Sativa or Indica in a cannabis strain, but they are not very accurate, Page explained. Explore further Study explains why hemp and marijuana are different

<hr>

[Visit Link](http://phys.org/news/2015-08-botanists-large-scale-genetic-marijuana-hemp.html){:target="_blank" rel="noopener"}


