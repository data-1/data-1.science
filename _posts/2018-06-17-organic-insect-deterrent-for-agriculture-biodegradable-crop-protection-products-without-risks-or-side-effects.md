---
layout: post
title: "Organic insect deterrent for agriculture: Biodegradable crop protection products without risks or side effects"
date: 2018-06-17
categories:
author: "Technical University of Munich (TUM)"
tags: [Insecticide,Pest (organism),News aggregator,Bacteria,Insect]
---


Traditional insecticides are killers: they not only kill pests, they also endanger bees and other beneficial insects, as well as affecting biodiversity in soils, lakes, rivers and seas. Repelling instead of poisoning  Brück and his team have now found an alternative: The insect repellent they have developed is biodegradable and ecologically harmless. The plant uses this molecule to protect itself from pests. Using synthetic biotechnology tools, Professor Brück's team isolated the sections of the tobacco plant genome responsible for the formation of the CBTol molecules. The solution was centrifugal separation chromatography: a highly efficient process that works equally well on an industrial scale, but hitherto had never been used to separate products from fermentation processes.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2018/06/180606132729.htm){:target="_blank" rel="noopener"}


