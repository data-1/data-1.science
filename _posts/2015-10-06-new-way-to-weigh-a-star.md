---
layout: post
title: "New way to weigh a star"
date: 2015-10-06
categories:
author: University of Southampton
tags: [Pulsar,Star,Mass,Neutron star,Planet,Physics,Astronomy,Physical sciences,Space science,Nature,Astronomical objects,Science]
---


However, in the case of young pulsars, mathematicians at Southampton have now found a new way to measure their mass, even if a star exists on its own in space. Dr Wynn Ho, of Mathematical Sciences at the University of Southampton, who led the research says: For pulsars, we have been able to use principles of nuclear physics, rather than gravity, to work out what their mass is -- an exciting breakthrough which has the potential to revolutionise the way we make this kind of calculation. Collaborator Dr Cristobal Espinoza of the Pontificia Universidad Catolica de Chile goes on to explain: All previous precise measurements of pulsar masses have been made for stars that orbit another object, using the same techniques that were used to measure the mass of the Earth or Moon, or discover the first extrasolar planets. Professor of Applied Mathematics at Southampton, Nils Andersson explains, Imagine the pulsar as a bowl of soup, with the bowl spinning at one speed and the soup spinning faster. Dr Ho has collaborated with his colleague Professor Andersson and external researchers Dr Espinoza and Dr Danai Antonopoulou of the University of Amsterdam, to use new radio and X-ray data to develop a novel mathematical model that can be used to measure the mass of pulsars that glitch.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/10/151005082537.htm){:target="_blank" rel="noopener"}


