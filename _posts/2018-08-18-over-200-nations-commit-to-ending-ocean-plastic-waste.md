---
layout: post
title: "Over 200 nations commit to ending ocean plastic waste"
date: 2018-08-18
categories:
author: "Greg Beach"
tags: []
---


Over 200 countries just signed a United Nations resolution in Nairobi, Kenya to eliminate plastic waste in the world’s oceans. The resolution is an important step towards establishing a legally binding treaty that would deal with the plastic pollution problem afflicting the world’s oceans. According to the United Nations Environment Programme (UNEP), there will be more plastic by weight in the world’s oceans than fish by 2050 if current trends continue. “We now have an agreement to explore a legally binding instrument and other measures and that will be done at the international level over the next 18 months.”  Join Our Newsletter Receive the latest in global news and designs building a better future. SIGN UP  Although plastic pollution is a global problem, Norway was the country that initiated the UN resolution.

<hr>

[Visit Link](https://inhabitat.com/over-200-nations-commit-to-ending-ocean-plastic-waste){:target="_blank" rel="noopener"}


