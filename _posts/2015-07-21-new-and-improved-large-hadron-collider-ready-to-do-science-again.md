---
layout: post
title: "New and improved Large Hadron Collider ready to do science again"
date: 2015-07-21
categories:
author: ""   
tags: [Large Hadron Collider,Subatomic particle,Matter,Universe,Dark matter,Collider,Physics,Physical sciences,Particle physics,Nature,Science]
---


New and improved Large Hadron Collider ready to do science again    by Brooks Hays    San Jose, Calif. (UPI) Feb 19, 2015    Researchers hope the Large Hadron Collider, set to resume scientific work in March after two years of improvements, can help them confirm the existence of dark matter particles. Dark matter makes up 27 percent of the universe. Dark energy -- the all-encompassing term used to describe the strange properties exhibited by the emptiness of space -- makes up 68 percent of the universe. In other words, more than 95 percent of the universe remains largely a mystery. With the LHC now twice as powerful, scientists hope even more energy-packed collisions will reveal new subatomic particles -- like dark matter.

<hr>

[Visit Link](http://www.spacedaily.com/reports/New_and_improved_Large_Hadron_Collider_ready_to_do_science_again_999.html){:target="_blank" rel="noopener"}


