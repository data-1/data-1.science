---
layout: post
title: "Researchers use laser to levitate, glowing nanodiamonds in vacuum"
date: 2015-09-08
categories:
author: University Of Rochester
tags: [Nanodiamond,Spin (physics),Quantum mechanics,Energy level,Diamond,Laser,Electron,Energy,Photoluminescence,Light,Photon,Motion,Optics,Nitrogen-vacancy center,Magnetic field,Electromagnetism,Physical sciences,Atomic molecular and optical physics,Science,Physical chemistry,Chemistry,Applied and interdisciplinary physics,Theoretical physics,Physics]
---


The team have now continued the research to use nanodiamonds with single vacancies and to do the experiments in vacuum. This, Neukirch stated, is their long-term goal: to damp the diamond's motion until it is in the ground state of the system, which would make the system behave as a quantum mechanical oscillator. With a single spin in the NV center, and the system functioning as a quantum mechanical oscillator, the researchers would be able to affect the spin state of the tiny defect inside the nanodiamond by exerting mechanical control on the entire nanodiamond. For this to be possible, the system has to be in vacuum, at even lower pressures that the researchers were able to achieve. Photoluminescence allows the researchers to understand by the energy of the emitted photon what the energy structure of the system is, as well as exert control and be able to change the energy of the system.

<hr>

[Visit Link](http://phys.org/news/2015-09-laser-levitate-nanodiamonds-vacuum.html){:target="_blank" rel="noopener"}


