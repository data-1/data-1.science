---
layout: post
title: "Sentinel-5P liftoff"
date: 2017-10-13
categories:
author: ""
tags: [Sentinel-5,Atmosphere,Chemistry,Nature,Earth sciences,Gases,Societal collapse]
---


Replay of the Sentinel-5P liftoff on a Rockot from the Plesetsk Cosmodrome in northern Russia at 09:27 GMT (11:27 CEST) on 13 October 2017. Sentinel-5P – the ‘P’ standing for ‘Precursor’ – is the first Copernicus mission dedicated to monitoring our atmosphere. The satellite carries the state-of-the-art Tropomi instrument to map a multitude of trace gases such as nitrogen dioxide, ozone, formaldehyde, sulphur dioxide, methane, carbon monoxide and aerosols – all of which affect the air we breathe and therefore our health, and our climate. Watch the full replay of the Sentinel-5P launch coverage  Watch the replay of the Sentinel-5P launch event at ESTEC

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2017/10/Sentinel-5P_liftoff){:target="_blank" rel="noopener"}


