---
layout: post
title: "Superabsorbing ring could make light work of snaps"
date: 2015-05-22
categories:
author: Oxford University
tags: [Photon,Light,Laser,Energy,Atom,Optics,Atomic molecular and optical physics,Physics,Electromagnetic radiation,Physical sciences,Science,Physical chemistry,Technology,Chemistry]
---


For this the team propose exploiting a key property of the ring structure: each time it absorbs a photon, it becomes receptive to photons of a slightly higher energy. 'Let's say it starts by absorbing red light from the laser,' said Kieran, 'once it is charged to 50% it now has an appetite for yellow photons, which are higher energy. This can be achieved by embedding the device into a special crystal that suppresses red light: it makes it harder for the ring to release its existing energy, so trapping it in the 50% charged state. The final ingredient of the design is a molecular 'wire' that draws off the energy of newly absorbed photons. And it could pave the way for camera technology that would exceed the human eye's ability to see clearly both in dark conditions and in bright sunlight.'

<hr>

[Visit Link](http://phys.org/news327916156.html){:target="_blank" rel="noopener"}


