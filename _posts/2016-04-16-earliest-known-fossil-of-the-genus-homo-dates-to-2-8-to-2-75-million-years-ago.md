---
layout: post
title: "Earliest known fossil of the genus Homo dates to 2.8 to 2.75 million years ago"
date: 2016-04-16
categories:
author: Pennsylvania State University
tags: [Fossil,Homo,LD 350-1,Human evolution,Hominidae,Sedimentary rock,Geology,Earth sciences]
---


The researchers dated the recently discovered Ledi-Geraru fossil mandible, known by its catalog number LD 350-1, by dating various layers of volcanic ash or tuff using argon40 argon39 dating, a method that measures the different isotopes of argon and determines the age of the eruption that created the sample. Credit: Erin DiMaggio, Penn State  The area of Ethiopia where LD 350-1 was found is part of the East African Rift System, an area that undergoes tectonic extension, which enabled the 2.8 million-year-old rocks to be deposited and then exposed through erosion, according to DiMaggio. In most areas in Afar, Ethiopia, rocks dating to 3 to 2.5 million years ago are incomplete or have eroded away, so dating those layers and the fossils they held is impossible. Geologists Dr. Erin DiMaggio (PSU, left) and Dominique Garello (ASU, right) sample a tuff near the early Homo site in the Ledi-Geraru project area. Explore further Discoverer of Lucy skeleton hopes to find what made us human  More information: Early Homo at 2.8 Ma from Ledi-Geraru, Afar, Ethiopia, Science, www.sciencemag.org/lookup/doi/ … 1126/science.aaa1343 Early Homo at 2.8 Ma from Ledi-Geraru, Afar, Ethiopia,  Late Pliocene Fossiliferous Sedimentary Record and the Environmental Context of early Homo from Afar, Ethiopia, Science, www.sciencemag.org/lookup/doi/ … 1126/science.aaa1415  Related paper:Nature, DOI: 10.1038/nature14224

<hr>

[Visit Link](http://phys.org/news344673764.html){:target="_blank" rel="noopener"}


