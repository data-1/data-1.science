---
layout: post
title: "Dinosaur evolution: Lumbering giants had agile ancestors"
date: 2017-09-21
categories:
author: "Ludwig-Maximilians-Universität München"
tags: [Dinosaur,Sauropoda,Sauropodomorpha,Taxa]
---


The best known sauropod dinosaurs were huge herbivorous creatures, whose brain structures were markedly different from those of their evolutionary predecessors, for the earliest representatives of the group were small, lithe carnivores. For a start, they were carnivores - like Saturnalia tupiniquim, an early sauropod dinosaur that was about the same size as a modern wolf. Bronzati, Rauhut and their co-authors therefore argue that these features enabled S. tupiniquim to adopt a predatory lifestyle. Their findings strongly suggest that, in contrast to the true sauropods, it had a bipedal gait. With the aid of CT-based reconstruction of the surface anatomy of the brain, the researchers now hope to retrace other stages in the evolution of the sauropodomorphs.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-09/lm-del092017.php){:target="_blank" rel="noopener"}


