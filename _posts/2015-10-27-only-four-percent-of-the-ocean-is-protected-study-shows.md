---
layout: post
title: "Only four percent of the ocean is protected, study shows"
date: 2015-10-27
categories:
author: University Of British Columbia
tags: [Marine protected area,Protected area]
---


The Chagos Marine Reserve protects the ocean from fishing and other activities. UBC Institute for Ocean and Fisheries researchers found that major swaths of the ocean must still be protected to reach even the most basic global targets. The countries committed to protecting at least 10 per cent of the ocean by 2020. Boonzaier believes that not only do countries need to create more MPAs, they need to improve the protection they afford biodiversity by making a greater percentage of them no-take and enforcing them as such. Only 16 per cent of the area that is protected or 0.5 per cent of the global ocean—is designated as no take.

<hr>

[Visit Link](http://phys.org/news/2015-10-percent-ocean.html){:target="_blank" rel="noopener"}


