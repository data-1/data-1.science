---
layout: post
title: "ESA commits to next stage of UK revolutionary rocket engine"
date: 2016-07-21
categories:
author: ""
tags: [SABRE (rocket engine),Reaction Engines,Transport,Rocket propulsion,Space programs,Engines,Space industry,Astronautics,Spacecraft,Space access,Technology,Outer space,Rocketry,Space vehicles,Vehicles,Spaceflight,Flight,Aerospace,Spaceflight technology,Propulsion,Aerospace technologies,Jet engines]
---


Enabling & Support ESA commits to next stage of UK revolutionary rocket engine 12/07/2016 25643 views 295 likes  The UK’s Farnborough airshow today saw ESA’s commitment to the next step in developing a revolutionary air-breathing rocket engine that could begin test firings in about four years. The UK’s Reaction Engines Ltd has been working on SABRE for many years. Success could lead to single-stage-to-orbit spaceplanes. Precooler testing for SABRE engine A number of research and development projects followed through ESA, helping to demonstrate the feasibility of other elements, such as the novel rocket nozzles, air intake design and thrust chamber cooling. Today saw the contract signing by Franco Ongaro, ESA’s Director of Technical and Quality Management, and Mark Thomas, Chief Executive Officer of Reaction Engines Ltd, to commit the next stage of ESA funding towards SABRE.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/ESA_commits_to_next_stage_of_UK_revolutionary_rocket_engine){:target="_blank" rel="noopener"}


