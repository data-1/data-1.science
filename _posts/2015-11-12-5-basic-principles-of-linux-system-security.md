---
layout: post
title: "5 Basic Principles of Linux System Security"
date: 2015-11-12
categories:
author: Michael Boelen
tags: [Linux,Lynis,Vulnerability (computing),Image scanner,Cyberwarfare,Computer security,Information Age,Systems engineering,Computing,Computer science,Computer engineering,Digital media,Information technology,Software development,Technology,Information technology management,Software,Software engineering]
---


Know your system(s)  The first principle is about knowing what your system is supposed to do. What is its primary role, what software packages does it need and who needs access? Security Measures:  Use minimal/basic installation  Only allow access to people who really need it  3. Perform Defense in Depth  Protect the system by applying several layers of security. Know your Enemy  You can only protect a system the right way, if you know what threats you are facing.

<hr>

[Visit Link](http://linux-audit.com/5-basic-principles-of-linux-system-security/){:target="_blank" rel="noopener"}


