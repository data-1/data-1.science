---
layout: post
title: "Advanced artificial limbs mapped in the brain"
date: 2017-10-27
categories:
author: Ecole Polytechnique Fédérale de Lausanne
tags: [Prosthesis,Amputation,Somatosensory system,Perception,Clinical medicine,Brain,Cognitive science,Nervous system,Neuroscience]
---


Targeted motor and sensory reinnervation (TMSR) is a surgical procedure on patients with amputations that reroutes residual limb nerves towards intact muscles and skin in order to fit them with a limb prosthesis allowing unprecedented control. Now, EPFL scientists have used ultra-high field 7 Tesla fMRI to show how TMSR affects upper-limb representations in the brains of patients with amputations, in particular in primary motor cortex and the somatosensory cortex and regions processing more complex brain functions. But how does the brain encode and integrate such artificial touch and movements of the prosthetic limb? The study also showed that TMSR is still in need of improvement: the connections between the primary sensory and motor cortex with the higher-level embodiment regions in fronto-parietal cortex were as weak in the TMSR patients as in the non-TMSR patients, and differed with respect to healthy subjects. The findings provide the first detailed neuroimaging investigation in patients with bionic limbs based on the TMSR prosthesis, and show that ultra-high field 7 Tesla fMRI is an exceptional tool for studying the upper-limb maps of the motor and somatosensory cortex following amputation.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/epfd-aal102617.php){:target="_blank" rel="noopener"}


