---
layout: post
title: "How Neanderthals influenced human genetics at the crossroads of Asia and Europe"
date: 2017-10-25
categories:
author: University at Buffalo
tags: [Neanderthal,Interbreeding between archaic and modern humans,Human,Recent African origin of modern humans,Human populations,Genetics,Biology,Biological anthropology,Human evolution]
---


BUFFALO, N.Y. -- When the ancestors of modern humans migrated out of Africa, they passed through the Middle East and Turkey before heading deeper into Asia and Europe. The research, published on Oct. 13 in Genome Biology and Evolution, analyzes the genetic material of people living in the region today, identifying DNA sequences inherited from Neanderthals. Early contact with Neanderthals, but relatively little Neanderthal DNA  In addition to exploring the specific functions of genetic material that the Turkish population inherited from Neanderthals, the study also examined the Neanderthals' influence on human populations in Western Asia more broadly. People who live in Europe, Central Asia and East Asia today may be descended from human populations that treated Western Asia as a waystation: These human populations lived there temporarily, mating with the region's Neanderthals before moving on to other destinations. The first was a constant influx of genetic material from ancient Africans, who had no Neanderthal DNA and who continued to pass through Western Asia for thousands of years as human societies grew in Europe and Asia.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uab-hni102417.php){:target="_blank" rel="noopener"}


