---
layout: post
title: "The current state of Linux video editing 2018"
date: 2018-04-22
categories:
author: "Seth Kenlon
(Team, Red Hat)"
tags: [Blender (software),Application software,Lightworks,Pitivi,Edit decision list,Motion graphics,AppImage,Cinelerra,Filmmaking,FFmpeg,Software engineering,Computer engineering,Mass media technology,Software development,Digital media,Software,Computing,Technology]
---


Independent  An independent or hobbyist editor with simple needs will find OpenShot perfect. Independent  Many of the obvious things a hobbyist would expect from a video editor just don't happen in Flowblade. It's a robust application with some serious pro features, such as timeline effects, codec support, lots of export formats, and a unique but efficient interface. Independent  Resolve is probably overkill for hobbyists, but its interface is flexible and allows for several editing styles. Professional integration  Da Vinci exports to several exchange formats as well as video, audio, and image sequences.

<hr>

[Visit Link](https://opensource.com/article/18/4/new-state-video-editing-linux){:target="_blank" rel="noopener"}


