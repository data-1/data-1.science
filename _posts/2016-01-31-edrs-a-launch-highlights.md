---
layout: post
title: "EDRS-A launch highlights"
date: 2016-01-31
categories:
author: "$author" 
tags: [European Data Relay System,Technology,Spacecraft,Outer space,Spaceflight,Satellites,Space vehicles,Astronautics,Flight,Space science,Spaceflight technology,Rocketry,Space programs,Aerospace]
---


The first laser node of the European Data Relay System was launched from Baikonur, Kazakhstan atop a Proton rocket on 29 January at 22:20 GMT. Dubbed the ‘SpaceDataHighway’, EDRS will uniquely provide near-realtime Big Data relay services using cutting-edge laser technology. It will dramatically improve access to time-critical data, aiding disaster response by emergency services and maritime surveillance. EDRS is a public–private partnership between ESA and Airbus Defence and Space. Watch Launch of EDRS-A, the first SpaceDataHighway laser node.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Videos/2016/01/EDRS-A_launch_highlights){:target="_blank" rel="noopener"}


