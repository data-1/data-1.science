---
layout: post
title: "African subterranean animal exhibits 'extraordinary' cancer resistance"
date: 2016-06-24
categories:
author: "Hokkaido University"
tags: [Induced pluripotent stem cell,Stem cell,Cell potency,Naked mole-rat,Cell biology,Biology,Biotechnology,Life sciences,Biological processes,Cells,Medical specialties]
---


Mole-rats live up to 30 years, 10 times longer than mice, and captured colonies almost never show any type of cancer. When the mole-rats' iPSCs were inserted into the testes of mice with extremely weak immune systems, the team discovered that they didn't form tumours in contrast to human iPSCs and mouse iPSCs. The team also found that ERAS, a tumorigenic gene expressed in mouse embryonic stem cells and iPSCs, was mutated and dysfunctional in the mole-rat iPSCs. When researchers suppressed the ARF gene in mole-rat cells during the reprogramming process to iPSCs, the cells stopped proliferation with sign of cellular senescence, while the opposite happens with mouse cells. Further research into the detailed mechanisms underlying ASIS in naked mole-rats may shed new light on cancer resistance in the mole-rats and contribute to the generation of non-tumorigenic human-iPSCs, enabling safer cell-based therapeutics, said Kyoko Miura, an assistant professor at Hokkaido University.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/hu-asa061616.php){:target="_blank" rel="noopener"}


