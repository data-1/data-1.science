---
layout: post
title: "How does an experiment at the Large Hadron Collider work?"
date: 2016-05-07
categories:
author: Gavin Hesketh, The Conversation
tags: [Particle physics,Large Hadron Collider,CERN,Standard Model,Particle accelerator,Science,Nature,Theoretical physics,Applied and interdisciplinary physics,Physical sciences,Physics]
---


The LHC is the world's largest particle accelerator and lies in a tunnel below CERN, the European physics lab just outside Geneva. Ramp: Once the LHC is fully loaded, its two proton beams are slowly accelerated up to collision energy, now a world-record 6.5TeV per beam. The LHC Run 1 (2010-2013) provided enough data to test the Standard Model to new levels of precision and discover the Higgs boson. Run 2 will collide protons at 60% higher energies than Run 1 by pushing the magnets and accelerators to the limit. And with the LHC Run 2 we hope to make it in the lab for the first time.

<hr>

[Visit Link](http://phys.org/news352965519.html){:target="_blank" rel="noopener"}


