---
layout: post
title: "Large Hadron Collider resumes collisions after upgrade"
date: 2015-05-06
categories:
author: ""    
tags: [Large Hadron Collider,CERN,Collider,Nature,Applied and interdisciplinary physics,Physical sciences,Science,Particle physics,Physics]
---


The LHC Magnet Facility, which is used to train engineers and technicians, at the European Organisation for Nuclear Research (CERN) in Meyrin  The world's largest particle smasher resumed colliding protons Tuesday as it gradually reboots following a two-year upgrade, Europe's physics lab CERN said. The low-energy collisions took place in the Large Hadron Collider (LHC) Tuesday morning, CERN said in a statement. Experiments at the collider are aimed at unlocking clues as to how the universe came into existence by studying fundamental particles, the building blocks of all matter, and the forces that control them. Tuesday's collisions at the giant lab, housed in a 27-kilometre (17-mile) tunnel straddling the French-Swiss border, are part of preparations for the next experiments to delve into the mysteries of the universe. During the next phase of the LHC programme, researchers will probe a conceptual frontier called new physics, including antimatter and dark matter.

<hr>

[Visit Link](http://phys.org/news350053188.html){:target="_blank" rel="noopener"}


