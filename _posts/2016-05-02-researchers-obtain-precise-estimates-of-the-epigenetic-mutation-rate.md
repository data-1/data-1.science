---
layout: post
title: "Researchers obtain precise estimates of the epigenetic mutation rate"
date: 2016-05-02
categories:
author: University Of Groningen
tags: [Epigenetics,Evolution,DNA methylation,Mutation,Gene,Genetics,Bioinformatics,Mutation rate,Biological evolution,Biology,Life sciences,Biotechnology,Evolutionary biology]
---


It is as yet not known how important epigenetic marks are in plant evolution. In close collaboration with researchers from the European Research Institute for the Biology of Ageing (University of Groningen and University Medical Center Groningen) and the University of Georgia (USA), the Johannes group used material from an inbred line of the plant Arabidopsis thaliana (thale cress), which is often used in genetic research. Epigenetic mutation rate  'We chose this mark because we know it is heritable between generations and can affect gene expression', Johannes explains. 'Basically, our analysis shows that epigenetic mutations are about 100,000 times more likely than DNA sequence mutations. But using our epigenetic mutation rates, we can now look at natural populations with fresh eyes and ask whether any selection on these epigenetic mutations is present over evolutionary timescales.

<hr>

[Visit Link](http://phys.org/news350561594.html){:target="_blank" rel="noopener"}


