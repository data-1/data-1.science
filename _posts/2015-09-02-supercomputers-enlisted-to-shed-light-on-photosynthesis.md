---
layout: post
title: "Supercomputers enlisted to shed light on photosynthesis"
date: 2015-09-02
categories:
author: University of the Basque Country  
tags: [American Association for the Advancement of Science,Light,Applied and interdisciplinary physics,Science,Chemistry,Physics,Physical sciences,Technology,Physical chemistry,Branches of science,Quantum mechanics]
---


Researchers at the UPV/EHU-University of the Basque Country are using high-performance computing to simulate the processes that take place during the first moments of photosynthesis  Computing --the creation of supercomputers, above all-- enables scientists and engineers to analyse highly complex physical processes using simulation techniques. The molecule that carries out photosynthesis in plants is the LHC-II (Light Harvesting Complex II), comprising over 17,000 atoms. The aim of this thesis by the researcher Joseba Alberdi was to optimize the OCTOPUS code and to achieve high performance to be able to obtain the right acceleration factors in the calculations that are made in supercomputers. He wrote up his thesis entitled High-performance computing for electron dynamics in complex systems, in the ALDAPA group of the Department of Computer Architecture and Technology (Faculty of Computing) and in the Nano-Bio Spectroscopy group in the Department of Materials Physics (Faculty of Chemistry); his thesis supervisors were Javier Muguerza and Angel Rubio. Oliveira, P. García-Risueño, F. Nogueira, J. Muguerza, A. Arruabarrena, A. Rubio.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/uotb-set082615.php){:target="_blank" rel="noopener"}


