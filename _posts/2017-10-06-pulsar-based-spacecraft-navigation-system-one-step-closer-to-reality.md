---
layout: post
title: "Pulsar-based spacecraft navigation system one step closer to reality"
date: 2017-10-06
categories:
author: "Tomasz Nowakowski, Astrowatch.Net"
tags: [Pulsar,PSR B193721,Telescope,Space science,Physical sciences,Outer space,Science,Astronomy]
---


An enhancement to the NICER mission, the Station Explorer for X-ray Timing and Navigation Technology (SEXTANT) will perform the first space demonstration of pulsar-based navigation of spacecraft. The simulations made by the team provided crucial measurements regarding the future development of the XNAV method. The dashed lines represent a given pulse phase of a signal from the pulsar arriving at the true spacecraft position and an initial estimated position at two instants in time separated by an interval Δt. The researchers propose that this instrument could be further developed as a practical telescope for the XNAV system. However, major challenges still need to be overcome to develop this system as a 'GPS' in space, including the availability of a practical system, for steering the telescope to sufficient accuracy and reducing further the required pulsar observation times and the craft positioning errors.

<hr>

[Visit Link](http://phys.org/news/2016-08-pulsar-based-spacecraft-closer-reality.html){:target="_blank" rel="noopener"}


