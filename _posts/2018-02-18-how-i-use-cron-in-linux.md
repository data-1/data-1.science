---
layout: post
title: "How I use cron in Linux"
date: 2018-02-18
categories:
author: "David Both
(Correspondent)"
tags: [Cron,Information technology management,Computing,Software,Technology,System software,Computers,Computer architecture,Operating system technology,Software development,Computer engineering,Computer science,Utility software,Software engineering,Computer programming]
---


The cron service checks for files in the /var/spool/cron and /etc/cron.d directories and the /etc/anacrontab file. This line is set to run at 5:03 a.m. (03 05) every day. */ 5 08- 18 / 2 * * * / usr / local / bin / mycronjob.sh  This cron job runs every five minutes during every hour between 8 a.m. and 5:58 p.m.  One thing to note: The division expressions must result in a remainder of zero for the job to run. For example, the job will not run at all from 9 p.m. to 9:59 a.m. The following example shows a job definition that runs a job as the user student:  04 07 * * * student / usr / local / bin / mycronjob.sh  If no user is specified, the job is run as the user that owns the crontab file, root in this case.

<hr>

[Visit Link](https://opensource.com/article/17/11/how-use-cron-linux){:target="_blank" rel="noopener"}


