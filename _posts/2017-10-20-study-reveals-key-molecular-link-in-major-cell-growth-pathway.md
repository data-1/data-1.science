---
layout: post
title: "Study reveals key molecular link in major cell growth pathway"
date: 2017-10-20
categories:
author: Whitehead Institute for Biomedical Research
tags: [Protein,Zinc transporter ZIP9,Lysosome,Cell (biology),MTOR,Amino acid,Cell biology,Biochemistry,Biotechnology,Biology,Molecular biology,Life sciences,Chemistry]
---


Their work, which involves a critical cellular growth pathway known as mTOR, sheds light on a key aspect of cells' metabolism that involves tiny cellular compartments, called lysosomes, and harnesses a sophisticated technology for probing their biochemical content. SLC38A9 is a really elegant protein that ties together two critical functions: activating a key pathway that controls cell growth and releasing the substrates, namely amino acids, needed for that growth, says senior author David Sabatini, a Member of Whitehead Institute, a professor of biology at Massachusetts Institute of Technology, and investigator with the Howard Hughes Medical Institute. To clarify how SLC38A9 works, the researchers, including the study's first authors Gregory Wyant and Monther Abu-Remaileh, eliminated or knocked out its function in cells. When SLC38A9 function was absent, the levels of these essential amino acids in lysosomes went up. Pancreatic cancer cells are known to be highly dependent on the flow of amino acids from the lysosome.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/wifb-srk101917.php){:target="_blank" rel="noopener"}


