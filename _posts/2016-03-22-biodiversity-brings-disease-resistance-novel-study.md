---
layout: post
title: "Biodiversity brings disease resistance: Novel study"
date: 2016-03-22
categories:
author: University of Adelaide
tags: [Biodiversity,Meadow,Species,Experiment,Ecosystem,Environmental social science,Natural environment,Ecology,Biogeochemistry,Systems ecology,Nature,Earth sciences,Environmental science,Environmental conservation,Organisms]
---


They investigated the impacts of levels of biodiversity on the severity of a fungal disease. Our experiments, on the other hand, used natural communities and species with similar abundance so we could control for confounding effects of species richness and abundance. The result was rather astounding, says Professor Bradshaw. We showed unequivocally that greater biodiversity among the meadow plants reduced the overall incidence of fungal disease, even though there were more pathogens. Changing the delicate balance of a healthy community not only resulted in more pathogens but weakened the overall community's resistance to disease.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-03/uoa-bbd032116.php){:target="_blank" rel="noopener"}


