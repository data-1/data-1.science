---
layout: post
title: "10-Million-Year-Old Snake Revealed in Living Color"
date: 2016-04-05
categories:
author: Mindy Weisberger
tags: [Chromatophore,Fossil]
---


Preserved pigment cells in a snake fossil — the cream-colored material in the image is fossilized skin — allowed scientists to determine the ancient snake's color in life. (Image credit: Jim Robbins, artist)  A snake of a different color  Modern snakes have three different types of pigment cells, or chromatophores, arranged in layers in their skin: iridophores at the top, then xanthophores, and melanophores at the bottom, with each containing a different type of granule related to color. In previous studies of color extraction from fossils, scientists had reconstructed pigments from traces of melanin (produced by melanophores) preserved in both feathers and skin, McNamara told Live Science. And as McNamara and her colleagues discovered, that mineralization left behind a fossil that retained the shapes of cells linked to skin color. Knowing that mineralized fossils could retain a lot more color information than scientists had previously suspected could be an important part of answering questions about how snakes evolved and use their color — today and millions of years in the past.

<hr>

[Visit Link](http://www.livescience.com/54244-pigment-cells-in-fossil-snakeskin.html){:target="_blank" rel="noopener"}


