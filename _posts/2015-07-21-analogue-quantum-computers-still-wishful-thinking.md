---
layout: post
title: "Analogue quantum computers: Still wishful thinking?"
date: 2015-07-21
categories:
author: ""   
tags: [Quantum annealing,D-Wave Systems,Quantum mechanics,Qubit,Technology,Branches of science,Science,Applied mathematics,Computing,Computer science,Theoretical physics]
---


Analogue quantum computers: Still wishful thinking? by Staff Writers    Saitama, Japan (SPX) Feb 15, 2015    Traditional computational tools are simply not powerful enough to solve some complex optimisation problems, like, for example, protein folding. Quantum annealing, a potentially successful implementation of analogue quantum computing, would bring about an ultra-performant computational method. A series of reviews in this topical issue of EPJ ST, guest-edited by Sei Suzuki from Saitama Medical University, Japan, and Arnab Das from the Indian Association for the Cultivation of Science, Kolkota, India, focuses on the state of the art and challenges in quantum annealing. This approach, if proven viable, could greatly boost the capabilities of large-scale simulations and revolutionise several research fields, from biology to economics, medicine and material science.

<hr>

[Visit Link](http://www.spacedaily.com/reports/Analogue_quantum_computers_Still_wishful_thinking_999.html){:target="_blank" rel="noopener"}


