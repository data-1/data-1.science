---
layout: post
title: "What's so sexy about math?"
date: 2016-06-13
categories:
author: "Cédric Villani"
tags: [TED (conference),Mathematics,Branches of science,Science]
---


Hidden truths permeate our world; they're inaccessible to our senses, but math allows us to go beyond our intuition to uncover their mysteries. In this survey of mathematical breakthroughs, Fields Medal winner Cédric Villani speaks to the thrill of discovery and details the sometimes perplexing life of a mathematician. Beautiful mathematical explanations are not only for our pleasure, he says. They change our vision of the world.

<hr>

[Visit Link](http://www.ted.com/talks/cedric_villani_what_s_so_sexy_about_math){:target="_blank" rel="noopener"}


