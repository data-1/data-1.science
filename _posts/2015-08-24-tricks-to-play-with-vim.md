---
layout: post
title: "Tricks to play with vim"
date: 2015-08-24
categories:
author: Sandra Henry-Stocker
tags: []
---


bananas yogurt spinach chicken bread bananas cheese :sort u  Once you press the enter key, your grocery list will look like this:  bananas bread cheese chicken spinach yogurt  And, of course, you can still undo your changes, returning to the original list if you like by pressing u.  Inserting the output of commands into your file  If you want to insert the current date and time into your file, open a new line to hold the data and then type :.! Replace the date command shown above with the command of your choosing -- like who or uptime. Converting numbers from hexadecimal number to decimal and back again  To convert a hexadecimal number to decimal without leaving vim, press colon followed by the echo command and your hex number in the 0x format shown below. :echo printf ('%d',0xffff)  In both cases, you'll see the decimal result, though vim won't insert the number into your text. From vim, you can issue a sudo command (assuming sudo is set up to permit you to run commands as root) and still save your changes -- without having to save the file to a different location (e.g., /tmp) and then use additional commands to get it back to where it needs to be.

<hr>

[Visit Link](http://lxer.com/module/newswire/ext_link.php?rid=217913){:target="_blank" rel="noopener"}


