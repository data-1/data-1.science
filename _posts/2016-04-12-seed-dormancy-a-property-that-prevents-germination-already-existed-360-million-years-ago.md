---
layout: post
title: "Seed dormancy, a property that prevents germination, already existed 360 million years ago"
date: 2016-04-12
categories:
author: University Of Granada
tags: [Seed,Germination,Seed dormancy,Dormancy,Botany,Plants]
---


Seed dormancy is a phenomenon that has intrigued naturalists for decades, since it conditions the dynamics of natural vegetation and agricultural cycles. Credit: UGR  An international team of scientists, coordinated by a researcher from the U. of Granada, has found that seed dormancy (a property that prevents germination under non-favourable conditions) was a feature already present in the first seeds, 360 million years ago. 'Of all possible types of dormancy, the oldest one already featured very sophisticated adjustments to environmental conditions, according to the coordinator of this project, Rafael Rubio de Casas, a researcher from the Environment Department at the University of Granada, and the only Spaniard involved in this research. Actually, many species of plants simply germinate at the moment when their seeds are exposed to favourable conditions. Explore further Plants with dormant seeds give rise to more species

<hr>

[Visit Link](http://phys.org/news335610663.html){:target="_blank" rel="noopener"}


