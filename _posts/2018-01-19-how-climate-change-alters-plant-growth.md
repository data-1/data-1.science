---
layout: post
title: "How climate change alters plant growth"
date: 2018-01-19
categories:
author: "Martin-Luther-Universität Halle-Wittenberg"
tags: [Arabidopsis thaliana,Plant,Biology]
---


At higher temperatures, PIF4 activates growth-promoting genes and the plant grows taller. In the lab, the scientists identified plants with a gene defect which still only formed short stems at 28 degrees. They discovered a hormone that activates the PIF4 gene at high temperatures, thus producing the protein. We have now discovered the role of this special hormone in the signalling pathway and have found a mechanism through which the growth process is positively regulated at higher temperatures, Quint explains. The findings of the research group from Halle may help to breed plants in the future that remain stable even at high temperatures and are able to produce sufficient yields.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-01/mh-hc011218.php){:target="_blank" rel="noopener"}


