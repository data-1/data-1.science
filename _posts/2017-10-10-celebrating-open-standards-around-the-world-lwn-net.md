---
layout: post
title: "Celebrating open standards around the world [LWN.net]"
date: 2017-10-10
categories:
author: "Posted October, Ris"
tags: [Free software websites,Linux websites,Computing,System software,Open-source movement,Linux organizations,Finnish families of Swedish ancestry,Software development,Free software programmers,Kernel programmers,Free content,Free software people,Unix people,Intellectual works,Computer architecture,Software engineering,Computers,Technology,Software,Linux,Unix variants,Finnish computer programmers,Torvalds family,Linux kernel programmers,Finnish computer scientists,Free system software,Linus Torvalds,Linux people,Unix,Operating system families,Free software,Linux Foundation]
---


[Announcements] Posted Oct 14, 2016 19:04 UTC (Fri) by ris  Opensource.com celebrates World Standards Day on October 14. Whether in the world of software, where without standards we would have been unable to connect the world through the Internet and the World Wide Web, or the physical world, where standards make nearly everything you buy easier, more useful, and safer, the world would be a difficult place to navigate without standards. And critical to the useful of standards is making them available to all in an accessible, free format, unencumbered by legal or other hurdles. Comments (9 posted)

<hr>

[Visit Link](http://lwn.net/Articles/703630/rss){:target="_blank" rel="noopener"}


