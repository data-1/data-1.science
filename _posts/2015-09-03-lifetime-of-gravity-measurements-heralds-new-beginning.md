---
layout: post
title: "Lifetime of gravity measurements heralds new beginning"
date: 2015-09-03
categories:
author: "$author"   
tags: [Gravity Field and Steady-State Ocean Circulation Explorer,Geoid,Earth,Satellite,Space science,Planetary science,Astronomy,Nature,Applied and interdisciplinary physics,Science,Outer space,Physical sciences,Planets of the Solar System,Bodies of the Solar System,Earth sciences]
---


GOCE’s four years in orbit resulted in a series of four gravity models, each more accurate than the last. These models have been used to generate corresponding ‘geoids’ – the surface of a global ocean moulded by gravity alone. The satellite was designed to orbit at an extremely low altitude of 255 km to gain the best possible gravity measurements. Although the satellite is no longer in orbit, scientists now have the best possible information to hand about Earth’s gravity, effectively a new beginning for the mission. The ultimate geoid model and gravity data will be used for years to come for a deeper understanding of Earth.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/GOCE/Lifetime_of_gravity_measurements_heralds_new_beginning){:target="_blank" rel="noopener"}


