---
layout: post
title: "Gravitational waves detected from second pair of colliding black holes"
date: 2016-06-21
categories:
author: "Massachusetts Institute of Technology"
tags: [LIGO,Gravitational wave,Virgo interferometer,Gravitational-wave observatory,Max Planck Institute for Gravitational Physics,Physics,Statics,Theories of gravitation,Theory of relativity,Celestial mechanics,Gravitational-wave astronomy,Physical cosmology,Astronomy,General relativity,Physical sciences,Space science,Gravity,Astrophysics,Science]
---


Gravitational waves carry information about their origins and about the nature of gravity that cannot otherwise be obtained, and physicists have concluded that these gravitational waves were produced during the final moments of the merger of two black holes--14 and 8 times the mass of the sun--to produce a single, more massive spinning black hole that is 21 times the mass of the sun. It is very significant that these black holes were much less massive than those observed in the first detection, says Gabriela González, LIGO Scientific Collaboration (LSC) spokesperson and professor of physics and astronomy at Louisiana State University. With detections of two strong events in the four months of our first observing run, we can begin to make predictions about how often we might be hearing gravitational waves in the future. The LSC detector network includes the LIGO interferometers and the GEO600 detector. Several universities designed, built, and tested key components and techniques for Advanced LIGO: The Australian National University, the University of Adelaide, the University of Western Australia, the University of Florida, Stanford University, Columbia University in the City of New York, and Louisiana State University.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/miot-gwd061316.php){:target="_blank" rel="noopener"}


