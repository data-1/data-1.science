---
layout: post
title: "Plans for world’s first self-sufficient floating city advance to next phase"
date: 2015-07-09
categories:
author: Cat Distasio
tags: []
---


The Seasteading Institute is on target to launch the world’s first floating city by 2020. The Floating City Project is set to have political autonomy, although the concept involves an integrated relationship with a “host nation.” The self-sufficient floating community is planned to have residences, tourism, aquaculture, a business park, a research institute, and a power plant to sell energy and clean water back to the host nation. The first phase involved a feasibility report, crowdfunding $27,000 toward the design, and identifying potential customers. DeltaSync’s vision was selected as the most viable option, which is comprised of modular platforms in either squares or pentagons. The actual design of the floating city remains to be seen, following the forthcoming announcement of the design contest winner.

<hr>

[Visit Link](http://inhabitat.com/plans-for-worlds-first-self-sufficient-floating-city-advance-to-next-phase/){:target="_blank" rel="noopener"}


