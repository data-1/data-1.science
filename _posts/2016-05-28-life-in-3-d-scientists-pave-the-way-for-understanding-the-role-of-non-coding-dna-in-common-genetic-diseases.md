---
layout: post
title: "Life in 3-D: Scientists pave the way for understanding the role of non-coding DNA in common genetic diseases"
date: 2016-05-28
categories:
author: European Molecular Biology Laboratory
tags: [Enhancer (genetics),Gene,Genetics,Promoter (genetics),Genome,Non-coding DNA,Gene expression,Mutation,Proteinprotein interaction,Biochemistry,Molecular biology,Life sciences,Molecular genetics,Branches of genetics,Biotechnology,Biology]
---


Enhancers, in contrast, can be far away from their target gene in terms of genomic location and might require physical interaction with the promoter of a gene to propagate the activity signal. In this study, the team has generated molecular profiles from 75 human individuals that were sequenced as part of the 1000 Genomes Project - an international collaboration to produce an extensive catalogue of human genetic variation. As well as charting the specific interactions between promoters and enhancers using genotype information, the team were able to find genetic associations between physically interacting regions of the genome, thus providing evidence for functional interactions between enhancers and promoters. An unexpected finding was that often it was not only genetic variants in enhancers that were associated with gene expression, but also regulatory elements in promoters of a distal genes that were physically and genetically connected to the gene of interest. The approach we used enables us to map links between genes and their regulatory elements, says Fabian Grubert, who led the work at Stanford University, in Michael Snyder's lab.

<hr>

[Visit Link](http://phys.org/news/2015-08-life-d-scientists-pave-role.html){:target="_blank" rel="noopener"}


