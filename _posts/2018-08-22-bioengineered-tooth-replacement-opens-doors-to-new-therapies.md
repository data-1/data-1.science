---
layout: post
title: "Bioengineered tooth replacement opens doors to new therapies"
date: 2018-08-22
categories:
author: "International Association for Dental Research"
tags: [Dentistry,Human tooth development,Dental implant,Tooth loss,Health,Medicine,Health care,Clinical medicine,Medical specialties,Health sciences]
---


While artificial dental implants are the existing standard tooth replacement therapy, they do not exhibit many properties of natural teeth and can be associated with complications leading to implant failure. Two articles published in the September 2018 issue of the Journal of Dental Research share recent advances in bioengineering teeth. This is the first report that describes the use of postnatal dental cells to create bioengineered tooth buds that exhibit evidence of these features of natural tooth development, pointing to future bioengineered tooth buds as a promising, clinically relevant tooth replacement therapy. ###  About the Journal of Dental Research  The IADR/AADR Journal of Dental Research (JDR) is a multidisciplinary journal dedicated to the dissemination of new knowledge in all sciences relevant to dentistry and the oral cavity and associated structures in health and disease. About the International and American Associations for Dental Research  The International Association for Dental Research (IADR) is a nonprofit organization with over 11,000 individual members worldwide, dedicated to: (1) advancing research and increasing knowledge for the improvement of oral health worldwide, (2) supporting and representing the oral health research community, and (3) facilitating the communication and application of research findings.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-08/iaa-btr082018.php){:target="_blank" rel="noopener"}


