---
layout: post
title: "Quantum mechanic frequency filter for atomic clocks"
date: 2015-07-26
categories:
author: ""   
tags: [Electron,Atomic clock,Light,Atom,Laser,Resonator,Clock,Energy level,Filter (signal processing),Mirror,Wavelength,Electromagnetic radiation,Electrical engineering,Quantum mechanics,Physics,Chemistry,Science,Physical chemistry,Physical sciences,Electromagnetism,Atomic molecular and optical physics,Applied and interdisciplinary physics]
---


In an atomic clock, electrons jumping from one orbit to another decides the clock's frequency. To get the electrons to jump, researchers shine light on the atoms using stabilised laser light. However, the laser light has to have a very precise frequency to trigger very precise electron jumps. The problem with making it more precise is controlling the laser light, so that the light has exactly the wavelength that hits the atoms' electrons and gets them to oscillate very precisely and very accurately. The filter consisted of a vacuum chamber with ultra cold strontium atoms between the two mirrors.

<hr>

[Visit Link](http://www.spacedaily.com/reports/Quantum_mechanic_frequency_filter_for_atomic_clocks_999.html){:target="_blank" rel="noopener"}


