---
layout: post
title: "Imaging of scar tissue formation: Noninvasive magnetic resonance imaging of lung fibrogenesis with an amino acid targeted probe"
date: 2017-10-08
categories:
author: "Wiley"
tags: [Medical imaging,Magnetic resonance imaging,Scar,Fibrosis,Resonance,Healing,News aggregator,Wound healing,Chemistry,Medicine,Clinical medicine,Medical specialties,Diseases and disorders]
---


Organs respond to injuries with the formation of new fibrous tissue, which can result in scarring. This process called fibrogenesis can now be monitored noninvasively on a molecular level, as American scientists report in the journal Angewandte Chemie. Natural wound healing and tissue injury involves the formation of collagen-based fibrous tissue to close the wound. To monitor this process, Peter Caravan and collaborators from Massachusetts General Hospital and Harvard Medical School, USA, sought a molecular probe that could specifically recognize the components involved in fibrogenesis. They have created a functionalized gadolinium chelate as a probe for magnetic resonance imaging (MRI).

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/07/170719173710.htm){:target="_blank" rel="noopener"}


