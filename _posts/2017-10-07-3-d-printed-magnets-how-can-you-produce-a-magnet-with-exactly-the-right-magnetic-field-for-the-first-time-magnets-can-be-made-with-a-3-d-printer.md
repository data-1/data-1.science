---
layout: post
title: "3-D-printed magnets: How can you produce a magnet with exactly the right magnetic field? For the first time, magnets can be made with a 3-D printer"
date: 2017-10-07
categories:
author: "Vienna University of Technology"
tags: [3D printing,News aggregator,Physics,Electrical engineering,Electromagnetism,Technology]
---


It is, however, difficult to produce a permanent magnet with a magnetic field of a specific pre-determined shape. That is, until now, thanks to the new solution devised at TU Wien: for the first time ever, permanent magnets can be produced using a 3D printer. Designed on a computer  The strength of a magnetic field is not the only factor, says Dieter Süss, Head of the Christian-Doppler Advanced Magnetic Sensing and Materials laboratory at TU Wien. A magnet can be designed on a computer, adjusting its shape until all requirements for its magnetic field are met, explains Christian Huber, a doctoral student in Dieter Süss' team. But once you have the desired geometric shape, how do you go about implementing the design?

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2016/10/161025115757.htm){:target="_blank" rel="noopener"}


