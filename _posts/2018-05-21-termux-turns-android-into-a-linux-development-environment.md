---
layout: post
title: "Termux turns Android into a Linux development environment"
date: 2018-05-21
categories:
author: "Paul Bailey"
tags: [Termux,Android (operating system),Chroot,Linux,Linux distribution,Package manager,Hard coding,Chromebook,Tablet computer,Intellectual works,Operating system families,Digital media,Information technology management,Computer engineering,Computer science,Computer architecture,System software,Computers,Software engineering,Software development,Technology,Software,Computing]
---


What is Termux? Termux is Linux, but it is based on Android and runs in a container. Since you don't have root access in Termux, you can't just create a symlink to fix path issues. You can also make chroots of different Linux distributions. I'd never started these services manually before; normally they start and stop for me automatically, and I had to do a little digging to find out how to start my favorite services.

<hr>

[Visit Link](https://opensource.com/article/18/5/termux){:target="_blank" rel="noopener"}


