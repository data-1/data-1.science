---
layout: post
title: "Skin tough"
date: 2015-12-09
categories:
author: DOE/Lawrence Berkeley National Laboratory
tags: [Skin,Fibril,Collagen,Office of Science,Deformation (engineering),Lawrence Berkeley National Laboratory,Dermis,Laboratory,Fracture,American Association for the Advancement of Science,Applied and interdisciplinary physics,Materials science,Materials,Chemistry]
---


Collagen fibrils and fibers rotate, straighten, stretch and slide to carry load and reduce the stresses at the tip of any tear in the skin, says Robert Ritchie of Berkeley Lab's Materials Sciences Division, co-leader of this study along with UC San Diego's Marc Meyers. Ritchie and Meyers are the corresponding authors of a paper in Nature Communications that describes this study. Mechanical properties are largely determined in the dermis, which is the thickest layer and is made up primarily of collagen and elastin proteins. Collagen provides for mechanical resistance to extension, while elastin allows for deformation in response to low strains. The University of California manages Berkeley Lab for the U.S. Department of Energy's Office of Science.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/dbnl-st033115.php){:target="_blank" rel="noopener"}


