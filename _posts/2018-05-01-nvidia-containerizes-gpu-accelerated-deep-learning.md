---
layout: post
title: "Nvidia Containerizes GPU-Accelerated Deep Learning"
date: 2018-05-01
categories:
author: ""
tags: []
---


Learn More. Nvidia will certify configurations for other public clouds in the future. Microsoft may be next in supporting NGC on Azure. A D V E R T I S E M E N T  The open source version of NGC’s underlying containers and the developer tools are available to anyone running at least an Nvidia Titan-level GPU card and a Linux host operating system. These DIY developers then can migrate to Nvidia’s certified instances on major public clouds as their needs grow.

<hr>

[Visit Link](http://www.linuxinsider.com/story/84928.html?rss=1){:target="_blank" rel="noopener"}


