---
layout: post
title: "Better together: Merged microscope offers unprecedented look at biological processes"
date: 2018-05-07
categories:
author: "National Institutes Of Health"
tags: [Microscopy,Total internal reflection fluorescence microscope,Super-resolution microscopy,Optics,Scientific techniques,Electromagnetic radiation,Laboratory techniques,Science,Chemistry,Natural philosophy,Atomic molecular and optical physics,Laboratories,Scientific method,Technology]
---


This process creates very high contrast images because it eliminates much of the background, out-of-focus, light that conventional microscopes pick up. While TIRF microscopy has been used in cell biology for decades, it produces blurry images of small features within cells. Instant structured illumination microscopy (iSIM), developed by the Shroff lab in 2013, can capture video at 100 frames per second, which is more than 3 times faster than most movies or internet videos. Our method improves the spatial resolution of TIRF microscopy without compromising speed—something that no other microscope can do. Attached to molecular cargo that are transported around the cell, these particles move so fast that they are blurred when imaged by other microscopes.

<hr>

[Visit Link](https://phys.org/news/2018-05-merged-microscope-unprecedented-biological.html){:target="_blank" rel="noopener"}


