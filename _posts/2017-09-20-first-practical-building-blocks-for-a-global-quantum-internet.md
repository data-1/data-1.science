---
layout: post
title: "First practical building blocks for a global quantum internet"
date: 2017-09-20
categories:
author: "Centre For Quantum Computation, Communication Technology"
tags: [Quantum network,Quantum memory,Technology,Computing]
---


Erbium, a rare earth ion, has unique quantum properties such that it operates in the same band as existing fibre optic networks, eliminating the need for a conversion process. We've shown that erbium ions in a crystal can store quantum information for more than a second, which is 10,000 times longer than other attempts, and is long enough to one day send quantum information throughout a global network. Sellars said the new technology can also be operated as a quantum light source or used as an optical link for solid-state quantum computing devices, connecting them to the quantum internet. Dr Rose Ahlefeldt and A. Prof Matthew Sellars operating a superconducting magnet cryostat, used in the experiment to generate a high magnetic field and extremely low temperatures Credit: ANU/cqc2t.org  Not only is our material compatible with existing fibre optics, but it's versatility means it will be able to connect with many types of quantum computers including CQC2T's silicon qubits, and superconducting qubits such as those being developed by Google and IBM, said Sellars. This result is so exciting to me because it allows us to take a lot of the in-principle work we've demonstrated and turn it into practical devices for a full-scale quantum internet.

<hr>

[Visit Link](https://phys.org/news/2017-09-blocks-global-quantum-internet.html){:target="_blank" rel="noopener"}


