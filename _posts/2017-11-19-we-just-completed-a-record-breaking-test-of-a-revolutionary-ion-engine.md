---
layout: post
title: "We Just Completed a Record-Breaking Test of a Revolutionary Ion Engine"
date: 2017-11-19
categories:
author: ""
tags: [Hall-effect thruster,Spacecraft propulsion,Rocket engine,Ion thruster,Aerospace,Spaceflight,Flight,Outer space,Technology,Spacecraft,Space vehicles,Space science,Propulsion,Astronautics]
---


This thruster, which is being developed by NASA’s Glenn Research Center in conjunction with the US Air Force and the University of Michigan, is a scaled-up model of the kinds of thrusters used by the Dawn spacecraft. During a recent test, this thruster shattered the previous record for a Hall-effect thruster, achieving higher power and superior thrust. The test was carried about by Scott Hall and Hani Kamhawi at the NASA Glenn Research Center in Cleveland. To conduct the test, the team relied on NASA Glenn’s vacuum chamber, which is currently the only chamber in the US that can handle the X3 thruster. Credit: NASA  The X3’s power supplies are also being developed by Aerojet Rocketdyne, the Sacramento-based rocket and missile propulsion manufacturer that is also the lead on the propulsion system grant from NASA.

<hr>

[Visit Link](https://futurism.com/we-just-completed-a-record-breaking-test-of-a-revolutionary-ion-engine/){:target="_blank" rel="noopener"}


