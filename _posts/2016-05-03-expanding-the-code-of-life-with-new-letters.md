---
layout: post
title: "Expanding the code of life with new 'letters'"
date: 2016-05-03
categories:
author: American Chemical Society
tags: [DNA,Nucleotide,Nucleobase,Life sciences,Molecular biology,Genetics,Biochemistry,Biology,Biotechnology,Nucleic acids,Macromolecules,Nucleotides,Chemistry]
---


The DNA encoding all life on Earth is made of four building blocks called nucleotides, commonly known as letters, that line up in pairs and twist into a double helix. Now, two groups of scientists are reporting for the first time that two new nucleotides can do the same thing—raising the possibility that entirely new proteins could be created for medical uses. The researchers found that multiple Z-P pairs can contribute to a double helix, just as C-G and A-T pairs do, with the same combination of flexibility and rigidity required for natural DNA to function. Structural Basis for a Six Nucleotide Genetic Alphabet,., Article ASAP. DOI: 10.1021/jacs.5b03482  Evolution of Functional Six-Nucleotide DNA, J.

<hr>

[Visit Link](http://phys.org/news351943756.html){:target="_blank" rel="noopener"}


