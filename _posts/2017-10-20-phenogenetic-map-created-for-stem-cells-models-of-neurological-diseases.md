---
layout: post
title: "Phenogenetic map created for stem cells models of neurological diseases"
date: 2017-10-20
categories:
author: The Ohio State University Wexner Medical Center
tags: [Cell potency,Induced pluripotent stem cell,Neurology,Phenotype,Neurodegenerative disease,Research,Amyotrophic lateral sclerosis,Gene,Stem cell,Clinical medicine,Medicine,Biology,Biotechnology,Genetics,Neuroscience,Health sciences,Medical specialties,Life sciences]
---


In an effort to better understand neurological diseases like Alzheimer's, Parkinson's and ALS -- and develop new ways to treat them -- researchers at The Ohio State University Wexner Medical Center have performed the first meta-analysis of all induced pluripotent stem cell models for neurological and neurodegenerative diseases, and created an atlas of how cell characteristics are linked to their genotype. Synthesizing this information to understand the phenotypic role of disease-promoting genes and identifying the limitations of our current practices will be crucial steps toward achieving the great translational potential of induced pluripotent stem cell models of neurological diseases, said Dr. Jaime Imitola, director of the Progressive Multiple Sclerosis Multidisciplinary Clinic and Translational Research Program at Ohio State's Wexner Medical Center. As they analyzed the correlation of 663 neuronal phenotypes with genotypic data from 243 patients and 214 controls -- and examined research practices and reporting bias in neurological disease models -- they found that there is no established standard for the reporting of methods nor a defined minimal number of cell lines. This work also showed that alterations in patient-derived cells at the level of gene expression correlate with the reported cellular phenotypes, and these dysregulated genes are highly expressed in specific regions of disease in the human brain. Our phenogenetic map can be used to build new hypotheses in the field of neurological disease modeling, and to identify potential new opportunities to design novel drug strategies, said first author Ethan W. Hollingsworth, a neural stem cell research assistant at Ohio State's Wexner Medical Center and a hematology/oncology clinical research intern at Nationwide Children's Hospital.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171019100938.htm){:target="_blank" rel="noopener"}


