---
layout: post
title: "Gravitational wave detectors to search for dark matter"
date: 2018-08-16
categories:
author: "Lisa Zyga"
tags: [Gravitational-wave observatory,Gravitational wave,LIGO,Dark matter,Laser Interferometer Space Antenna,Photon,Physics,Science,Astrophysics,Astronomy,Physical sciences]
---


According to a new study, they could also potentially detect dark matter, if dark matter is composed of a particular kind of particle called a dark photon. Without any modifications, a gravitational wave detector can be used as a very sensitive direct dark matter detector, with the potential for a five-sigma discovery of dark matter. Gravitational wave detectors could potentially detect these oscillations because the oscillations may affect test objects placed in the gravitational wave detectors. In the future, the scientists plan to work on further developing the new dark matter search method and determining exactly what kind of signal a gravitational wave detector would receive if a dark photon were nearby. We plan to push this work well beyond a theoretical proposal, Zhao said.

<hr>

[Visit Link](https://phys.org/news/2018-08-gravitational-detectors-dark.html){:target="_blank" rel="noopener"}


