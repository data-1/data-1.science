---
layout: post
title: "New study shows the importance of jellyfish falls to deep-sea ecosystem"
date: 2015-07-04
categories:
author: University of Hawaii author:#8209; SOEST 
tags: [Deep sea,Jellyfish,Ocean,Marine food web,University of Hawaii at Mnoa,Scavenger,Seabed,Deep sea community,Earth sciences,Environmental engineering,Biogeochemistry,Nature,Hydrology,Ecology,Applied and interdisciplinary physics,Hydrography,Systems ecology,Environmental science,Natural environment,Physical geography,Oceanography]
---


This week, researchers from University of Hawai'i, Norway, and the UK have shown with innovative experiments that a rise in jellyfish blooms near the ocean's surface may lead to jellyfish falls that are rapidly consumed by voracious deep-sea scavengers. This latest research shows that the accumulation of dead jellyfish lakes may be unusual, with jellyfish carcasses normally being rapidly consumed by a host of typical deep-sea scavengers such as hagfish and crabs. Smith (13 seconds)  We've only been able to do these experiments in one location. Rapid scavenging of jellyfish carcasses reveals the importance of gelatinous material to deep-sea food webs. Proceedings of the Royal Society B 281: 20142210. http://dx.doi.org/10.1098/rspb.2014.2210  The School of Ocean and Earth Science and Technology at the University of Hawaii at Manoa was established by the Board of Regents of the University of Hawai'i in 1988 in recognition of the need to realign and further strengthen the excellent education and research resources available within the University.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-10/uoh-nss101514.php){:target="_blank" rel="noopener"}


