---
layout: post
title: "New forensic ID technology introduced: Fingerprint Molecular Identification"
date: 2015-12-21
categories:
author: Barry Teater, Self-employed PR consultant
tags: [Fingerprint,Forensic science,Forensic identification,DNA profiling,Science]
---


Can reveal gender, exposure to explosives, use of tobacco, medicines and illicit drugs  ArroGen Group, an integrated advanced forensic solutions company, will unveil its new Fingerprint Molecular Identification™ (FMID) technology Aug. 2-8 at the International Association for Identification's educational conference in Sacramento, Calif. By analyzing chemical residues on fingerprints taken directly from a suspect or from fingerprints or other latent prints left at crime scenes, FMID can reveal a suspect's gender, use of tobacco, medicines and illicit drugs, and exposure to explosives. This unprecedented technology will empower their investigations and intelligence-gathering with indisputable scientific evidence, saving time and money. FMID, a non-invasive process, can detect gender biomarkers, nicotine use by tobacco smokers, chewers or snuffers, and chemicals used in improvised explosives. It can also detect illicit drugs such as cocaine, heroin, methamphetamines, temazepam, marijuana and ecstasy, as well as legitimate medicines.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/btsp-nfi080315.php){:target="_blank" rel="noopener"}


