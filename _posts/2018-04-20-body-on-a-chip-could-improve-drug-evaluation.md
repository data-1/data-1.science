---
layout: post
title: "“Body on a chip” could improve drug evaluation"
date: 2018-04-20
categories:
author: "Anne Trafton"
tags: [Organ (biology),Organ-on-a-chip,Microfluidics,Gastrointestinal tract,Medicine,Life sciences,Medical specialties,Health sciences,Biology,Clinical medicine]
---


MIT engineers have developed new technology that could be used to evaluate new drugs and detect possible side effects before the drugs are tested in humans. Using a microfluidic platform that connects engineered tissues from up to 10 organs, the researchers can accurately replicate human organ interactions for weeks at a time, allowing them to measure the effects of drugs on different parts of the body. “With our chip, you can distribute a drug and then look for the effects on other tissues, and measure the exposure and how it is metabolized.”  These chips could also be used to evaluate antibody drugs and other immunotherapies, which are difficult to test thoroughly in animals because they are designed to interact with the human immune system. Preclinical testing in animals can offer information about a drug’s safety and effectiveness before human testing begins, but those tests may not reveal potential side effects, Griffith says. Her lab is now developing a model system for Parkinson’s disease that includes brain, liver, and gastrointestinal tissue, which she plans to use to investigate the hypothesis that bacteria found in the gut can influence the development of Parkinson’s disease.

<hr>

[Visit Link](http://news.mit.edu/2018/body-chip-could-improve-drug-evaluation-0314){:target="_blank" rel="noopener"}


