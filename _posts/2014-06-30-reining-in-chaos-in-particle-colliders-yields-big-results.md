---
layout: post
title: "Reining in chaos in particle colliders yields big results"
date: 2014-06-30
categories:
author: American Institute Of Physics
tags: [Particle physics,Large Hadron Collider,Particle accelerator,CERN,Physics,Chaos theory,Physical sciences,Science,Branches of science,Applied and interdisciplinary physics]
---


A method to correct tiny defects in the LHC's superconducting magnets (example shown above) was crucial to the discovery of the Higgs boson, which was announced in 2012. Credit: CERN  When beams with trillions of particles go zipping around at near light speed, there's bound to be some chaos. In a special focus issue of the journal Chaos, from AIP Publishing, a physicist at the European Organization for Nuclear Research (CERN) details an important method of detecting and correcting unwanted chaotic behavior in particle colliders. In these machines, powerful electric and magnetic fields accelerate and guide beams containing trillions of particles. From previous work in astronomy, Yannis Papaphilippou, a physicist at CERN, knew of a method called frequency map analysis that relates the frequencies at which objects oscillate to their chaotic behavior. By modeling the extent to which tiny defects in the LHC's superconducting magnets cause protons traveling in the collider's rings to behave chaotically, Papaphilippou and his colleagues helped magnet builders design and produce these magnets within strict tolerance limits.

<hr>

[Visit Link](http://phys.org/news323354543.html){:target="_blank" rel="noopener"}


