---
layout: post
title: "New technique can capture images of ultrafast energy-time entangled photon pairs"
date: 2018-07-03
categories:
author: "University Of Waterloo"
tags: [Ultrafast laser spectroscopy,Quantum entanglement,Quantum mechanics,Photon,Quantum cryptography,Optics,Technology,Science,Physics]
---


In the last 10 to 20 years, researchers have been interested in exploring and exploiting energy-time entanglement for communication, said MacLean. By being able to measure ultrafast entangled photons, our measurement technique opens the door to exploiting entanglement in a whole new regime. Energy-time entanglement is a feature of quantum light. Scientists have been interested in exploiting energy-time entanglement for quantum information, but until now, they lacked the resolution in both energy and time to directly observe it. The new apparatus brings a tool frequently relied upon in classical optics research to the quantum world.

<hr>

[Visit Link](https://phys.org/news/2018-02-technique-capture-images-ultrafast-energy-time.html){:target="_blank" rel="noopener"}


