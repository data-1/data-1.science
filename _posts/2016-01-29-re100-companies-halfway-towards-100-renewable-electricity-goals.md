---
layout: post
title: "RE100 Companies Halfway Towards 100% Renewable Electricity Goals"
date: 2016-01-29
categories:
author: Joshua S Hill, Carolyn Fortuna, Guest Contributor, Zachary Shahan, Written By
tags: [100 renewable energy,Renewable energy,The Climate Group,Climate change mitigation,Sustainable energy,Physical quantities,Renewable resources,Energy and the environment,Economy,Sustainable development,Power (physics),Technology,Sustainable technologies,Nature,Energy]
---


This, according to the new RE100 annual report published by The Climate Group and CDP this week, which publishes the latest available data (which is from 2014) on 45 companies partaking in the RE100 campaign to commit to 100% renewable electricity. Specifically, the report shows that the retail sector of RE100 companies have switched over 10 GW to renewables, while those in the ICT sector are on average 64% of the way towards their 100% electricity goals. Many European companies are also making the most of opportunities to use Power Purchase Agreements for renewable energy directly from the grid. “This report shows us that business corporations around the world are stepping up in making commitments to renewable energy and working with RE100 to drive forward a global market for renewable energy,” said Roberto Zanchi, Technical Manager, Renewable Energy at CDP. Advertisement  Appreciate CleanTechnica’s originality and cleantech news coverage?

<hr>

[Visit Link](http://cleantechnica.com/2016/01/22/re100-companies-halfway-towards-100-renewable-electricity-goals/){:target="_blank" rel="noopener"}


