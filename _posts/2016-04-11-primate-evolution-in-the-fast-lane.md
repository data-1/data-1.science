---
layout: post
title: "Primate evolution in the fast lane"
date: 2016-04-11
categories:
author: Cornell University
tags: [Mutation,DNA,Virus,APOBEC3G,Gene,Genome,Biology,Evolution,Enzyme,Cancer,Genetics,Life sciences,Molecular biology,Biotechnology,Biochemistry]
---


The discovery raises questions about the accuracy of using the more typical mutation process as an estimate to date when two species diverged, as well as the extent to which this and related enzymes played a role in primate evolution. Alon Keinan, associate professor of Biological Statistics and Computational Biology at Cornell, and Erez Levanon, co-senior author and an associate professor with the Mina and Everard Goodman Faculty of Life Sciences at Bar-Ilan University in Israel, describe the novel, and rare, process triggered by a member of the APOBEC family of virus-fighting enzymes in the journal Genome Research. To the enzyme, they look the same. They knew that the enzyme recognizes a specific motif in the DNA and it targets only one of the DNA bases for mutation. What is appealing is that it's an accelerated evolutionary mechanism that could generate a large change in a gene in a single generation, said Levanon.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-04/cu-pei040716.php){:target="_blank" rel="noopener"}


