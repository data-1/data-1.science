---
layout: post
title: "Revealed—the single event that made complex life possible in our oceans"
date: 2015-12-20
categories:
author: University Of Bristol
tags: [Cyanobacteria,Phytoplankton,Life,Ocean,Natural environment,Earth sciences,Nature,Oceanography,Environmental science,Physical geography,Systems ecology]
---


Scientists have been trying to work out why it took so long for the Earth's atmosphere to reach modern concentrations of oxygen, when photosynthesis had already evolved by around 2,700 million years ago. Patricia Sánchez-Baracaldo, Royal Society Research Fellow, from the School of Geographical Sciences at the University of Bristol, used genomic data to trace back the origin of these crucial and transformative marine planktonic cyanobacteria. Her research, published in Scientific Reports, revealed that various different types of marine planktonic forms evolved relatively late – between 800 to 500 million years ago, arising from freshwater and/or marine benthic ancestors. Dr Sánchez-Baracaldo said: 'The results of this large-scale phylogenomic study imply that, early on, terrestrial cyanobacteria capable of building microbial mats dominated the ecology of the Early Earth'  'Rather surprisingly, marine planktonic cyanobacteria are relatively young, only evolving just prior to the origin of complex life – animals. These biological events are linked - they help explain why it took so long for complex life to evolve on our planet.

<hr>

[Visit Link](http://phys.org/news/2015-12-revealedthe-event-complex-life-oceans.html){:target="_blank" rel="noopener"}


