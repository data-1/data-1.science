---
layout: post
title: "California needs 11 trillion gallons of water: NASA"
date: 2016-04-12
categories:
author:  
tags: [NASA,GRACE and GRACE-FO,Earth sciences]
---


A vehicle raises a large dust cloud as it drives on a parched farm field in Los Banos, California on September 23, 2014  California needs 11 trillion gallons of water to recover from its three-year drought, the US space agency said Tuesday after studying water resources by using satellite data. California has experienced rainstorms in recent days but, while welcome, scientists warn that they are not enough to end the drought. Spaceborne and airborne measurements of Earth's changing shape, surface height and gravity field now allow us to measure and analyze key features of droughts better than ever before, including determining precisely when they begin and end and what their magnitude is at any moment in time. That's an incredible advance and something that would be impossible using only ground-based observations. Meanwhile, other NASA satellite data showed that so far this year, the snowpack in California's Sierra Nevada range is only half previous estimates.

<hr>

[Visit Link](http://phys.org/news337971795.html){:target="_blank" rel="noopener"}


