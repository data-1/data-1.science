---
layout: post
title: "A step closer to quantum computers: NUS researchers show how to directly observe quantum spin effects"
date: 2018-07-17
categories:
author: "National University of Singapore"
tags: [Quantum mechanics,Computing,Electron,Computer,Quantum computing,Topological insulator,Spin (physics),Microscope,Electricity,Technology,Science,Physics,Branches of science,Applied and interdisciplinary physics]
---


The NUS team, together with our collaborators from Rutgers, The State University of New Jersey in the United States and RMIT University in Australia, showed a practical way to observe and examine the quantum effects of electrons in topological insulators and heavy metals which could later pave the way for the development of advanced quantum computing components and devices, explained Assoc Prof Yang. These spinning electron states replace the ones and zeros used as the basis for traditional computers, and because they can exist in many spin states simultaneously, this allows for much more complex computing to be performed. An applied electrical current influenced the electron spin at the quantum level for all of these materials and the scientists were able to directly visualise this change using polarised light from the microscope. This means that developing better devices for quantum computers will become easier now that these phenomena can be directly observed in this way. The team hopes to work with industry partners to further explore the various applications of this unique technique, with a focus on developing the devices used in future quantum computers.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/nuos-asc071618.php){:target="_blank" rel="noopener"}


