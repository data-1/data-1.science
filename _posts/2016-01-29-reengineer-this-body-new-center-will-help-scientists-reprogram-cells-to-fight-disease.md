---
layout: post
title: "Reengineer This Body: New Center Will Help Scientists Reprogram Cells To Fight Disease"
date: 2016-01-29
categories:
author: "$author" 
tags: [General Electric,Biopharmaceutical,Technology,Regenerative medicine,Innovation,Cell therapy,Cancer]
---


The reprogrammed cell itself becomes the treatment that gets reintroduced to the patient. “We need to integrate existing technology to break through manufacturing bottlenecks and make cell therapy affordable and available,” says CCRM President and CEO Michael May. GE makes bioreactors as well as other technology required to produce these drugs. “In the biopharmaceutical industry, we use cells in bioreactors to make these therapeutic proteins,” GE’s Vanek says. In many ways, GE Healthcare’s cell therapy business is the perfect example of what the company calls the GE Store — the ability to share knowledge across many domains .

<hr>

[Visit Link](http://www.gereports.com/from-bloodstream-to-mainstream-new-cell-therapy-center-seeks-to-revolutionize-personalized-medicine/){:target="_blank" rel="noopener"}


