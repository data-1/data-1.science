---
layout: post
title: "Timelapse from space reveals glacier in motion"
date: 2015-12-22
categories:
author: "$author" 
tags: [Glacier,Karakoram,Satellite,Satellite imagery,Time-lapse photography,Climate,False color,Technology,Earth sciences]
---


Applications Timelapse from space reveals glacier in motion 26/11/2015 17055 views 97 likes  Animations that compress 25 years of satellite images into just one second reveal the complex behaviour and flow of glaciers in the Karakoram mountain range in Asia. Since global change is having a direct effect on the environment and society at large, it is more important than ever to understand exactly what is happening to our planet so that informed decisions can be made – as will be highlighted even more at the upcoming COP21 conference on climate change. The animations are a very practical way to get a better overview and follow the changes through time,” added Dr Paul. The Baltoro animation, for example, highlights how fast and steadily the glacier is flowing without changing the position of its front, while the Panmah image sequence shows several surging glaciers flowing into each other. As detailed in The Cryosphere, Dr Paul created the animations in simple gif format using satellite images freely available from the US Geological Survey.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Timelapse_from_space_reveals_glacier_in_motion){:target="_blank" rel="noopener"}


