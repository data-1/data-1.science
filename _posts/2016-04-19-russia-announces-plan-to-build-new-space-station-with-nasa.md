---
layout: post
title: "Russia announces plan to build new space station with NASA"
date: 2016-04-19
categories:
author:  
tags: [International Space Station,Roscosmos,NASA,Space exploration,Spacecraft,Life in space,Human spaceflight,Flight,Space programs,Outer space,Astronautics,Spaceflight]
---


NASA astronauts at the International Space Station (ISS)  Russia on Saturday announced initial plans to build a new orbital space station together with NASA to replace the International Space Station (ISS), which is set to operate until 2024. Komarov made the announcement flanked by NASA administrator Charles Bolden at Russia's Baikonur launchpad in Kazakhstan. We agreed that the group of countries taking part in the ISS project will work on the future project of a new orbital station, Komarov said. Roscosmos and NASA do not rule out that the station's flight could be extended, Komarov added. The Russian government will study the results of the talks between Roscosmos and NASA.

<hr>

[Visit Link](http://phys.org/news346754256.html){:target="_blank" rel="noopener"}


