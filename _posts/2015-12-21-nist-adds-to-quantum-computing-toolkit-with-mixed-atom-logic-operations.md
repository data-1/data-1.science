---
layout: post
title: "NIST adds to quantum computing toolkit with mixed-atom logic operations"
date: 2015-12-21
categories:
author: National Institute of Standards and Technology (NIST)
tags: [Quantum mechanics,Quantum logic gate,Quantum computing,Trapped ion quantum computer,Computing,Branches of science,Physics,Theoretical physics,Science,Applied mathematics,Theoretical computer science,Quantum information science,Technology,Computer science]
---


The scientists used two sets of laser beams to entangle the two ions--establishing a special quantum link between their properties--and to perform two types of logic operations, a controlled NOT (CNOT) gate and a SWAP gate. As in classical computing, a quantum bit (qubit) can have a value of 0 or 1. The same NIST group has demonstrated many other building blocks for quantum computers based on trapped ions. For example, the group demonstrated the first quantum logic gate (a CNOT gate) on individual qubits in 1995 using a single beryllium ion. A universal set of quantum gates is one of the so-called DiVincenzo criteria (see http://arxiv.org/pdf/quant-ph/0002077.pdf), which describe the elements needed to build a practical quantum computer.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-12/nios-nat121415.php){:target="_blank" rel="noopener"}


