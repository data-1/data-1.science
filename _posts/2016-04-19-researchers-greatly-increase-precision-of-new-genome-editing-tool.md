---
layout: post
title: "Researchers greatly increase precision of new genome editing tool"
date: 2016-04-19
categories:
author: Max Planck Society
tags: [CRISPR gene editing,DNA repair,Gene,Non-homologous end joining,Induced pluripotent stem cell,Genetics,DNA,Genome editing,Biotechnology,Cell biology,Biological engineering,Modification of genetic information,Molecular genetics,Branches of genetics,Biochemistry,Molecular biology,Biology,Life sciences]
---


Many researchers, including Van Trung Chu, Klaus Rajewsky and Ralf Kühn, are seeking to promote the HDR repair pathway to make gene modification in the laboratory more precise in order to avoid editing errors and to increase efficiency. The MDC researchers succeeded in increasing the efficiency of the more precisely working HDR repair system by temporarily inhibiting the most dominant repair protein of NHEJ, the enzyme DNA Ligase IV. The expertise of Ralf Kühn is very important for gene research at MDC and especially for my research group, Klaus Rajewsky said. That is, researchers are able to use the new tool to introduce patient-derived mutations into the genome of iPS cells for studying the onset of human diseases. Another future goal, however, is to use CRISPR-Cas9 for somatic gene therapy in humans with severe diseases, Klaus Rajewsky pointed out.

<hr>

[Visit Link](http://phys.org/news346497144.html){:target="_blank" rel="noopener"}


