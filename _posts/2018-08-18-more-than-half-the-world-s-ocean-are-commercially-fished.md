---
layout: post
title: "More than half the world's ocean are commercially fished"
date: 2018-08-18
categories:
author: "American Association for the Advancement of Science (AAAS)"
tags: [American Association for the Advancement of Science,Supraorganizations,Social economy,Occupational organizations,United States,Economy of the United States,Learned societies,Washington DC,Non-profit organizations,Clubs and societies,Professional associations,Science,Learned societies of the United States,Science and technology,Scientific societies,Scientific organizations,Scientific supraorganizations]
---


Peak fishing activity, the study goes on to report, is more affected by cultural and political events such as holidays and closures than by changes in economic factors, or environmental ones. Given the vastness of the world's oceans, which cover roughly 70% of the Earth's surface, quantifying the extent of human fishing activity globally has been a challenge. However, David A. Kroodsma and colleagues saw a prime opportunity to better study fishing patterns as more and more vessels adopted automatic identification system (AIS) technology. AIS technology - which tracks a ship's identity, position, speed, and turning angle every few seconds - was originally meant to help ships avoid collisions. But here, Kroodsma et al. used the data from this system to study the global footprint of industrial fishing, processing 22 billion automatic identification system messages obtained from more than 70,000 industrial fishing vessels between 2012 and 2016.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/aaft-mth022118.php){:target="_blank" rel="noopener"}


