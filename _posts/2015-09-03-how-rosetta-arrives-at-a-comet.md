---
layout: post
title: "How Rosetta arrives at a comet"
date: 2015-09-03
categories:
author: "$author"   
tags: [Rosetta (spacecraft),Comet,Coma (cometary),Spaceflight,Celestial mechanics,Planetary science,Spacecraft,Sky,Astronomical objects,Local Interstellar Cloud,Bodies of the Solar System,Astronautics,Flight,Outer space,Astronomy,Space science,Solar System]
---


But how does a spacecraft actually arrive at a comet? Since early May, Rosetta’s controllers have been pacing it through a tightly planned series of manoeuvres designed to slow its speed with respect to the comet by about 2800 km/h, or 775 m/s, to ensure its arrival on 6 August. Flight dynamics experts play crucial role Rosetta’s orbit around comet  ESA’s experts are playing a crucial role, having worked extensively behind the scenes to develop a series of ten orbit-correction manoeuvres that use Rosetta’s thrusters to match the spacecraft’s speed and direction with that of the comet. “Our team is responsible for predicting and determining Rosetta’s orbit, and we work with the flight controllers to plan the thruster burns,” says Frank Dreger, Head of Flight Dynamics at ESA’s Space Operations Centre, ESOC, in Darmstadt, Germany. Throughout July, the burns were made on a weekly basis, and will culminate in two short orbit insertion burns set for 3 and 6 August.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Operations/How_Rosetta_arrives_at_a_comet){:target="_blank" rel="noopener"}


