---
layout: post
title: "3 Reasons Why It's a Good Idea to Resurrect a Species"
date: 2018-04-19
categories:
author: ""
tags: [De-extinction,Biodiversity,Extinction,Species,Biology,Nature,Natural environment,Ecology]
---


Assembling the complete genome of the moa — and those of other long-gone creatures like the woolly mammoth, passenger pigeons, or even our Neanderthal cousins — is the first step in bringing those species back to life. Using modern technology to revive extinct species is known simply as de-extinction, though the process is not what you'd call simple. With an accurate assembly of an extinct species' genetic data, researchers can inject them into an egg of a living species that's genetically close to the one they're attempting to bring back to life. And the genomes researchers are reconstructing now could be useful for assembling the DNA of other species in the future. Reviving extinct keystone species, then, could help us preserve biodiversity, and, possibly, the ecosystems as a whole.

<hr>

[Visit Link](https://futurism.com/reasons-why-resurrect-species/){:target="_blank" rel="noopener"}


