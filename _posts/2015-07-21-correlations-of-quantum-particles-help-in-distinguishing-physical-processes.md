---
layout: post
title: "Correlations of quantum particles help in distinguishing physical processes"
date: 2015-07-21
categories:
author: ""   
tags: [Quantum mechanics,Quantum entanglement,Physics,Science,Theoretical physics,Branches of science]
---


Correlations of quantum particles help in distinguishing physical processes    by Staff Writers    Glasgow, UK (SPX) Feb 17, 2015    Communication security and metrology could be enhanced through a study of the role of quantum correlations in the distinguishability of physical processes, by researchers at the Universities of Strathclyde and Waterloo. The study authors devised a method for both precisely quantifying steering's impact and relating it to the task of distinguishing physical processes. Dr Piani said: Quantum particles can be in a particular state known as `entangled'. We now know that steering is a crucial and real quantum effect; however, knowledge about what steering is actually useful for has remained limited. In our research, we related steering to the discrimination of physical processes, which seeks to answer questions about what happens in time to physical systems of interest, like microscopic particles.

<hr>

[Visit Link](http://www.spacedaily.com/reports/Correlations_of_quantum_particles_help_in_distinguishing_physical_processes_999.html){:target="_blank" rel="noopener"}


