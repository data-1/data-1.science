---
layout: post
title: "First comprehensive inventory of Neotropical snakes"
date: 2017-11-30
categories:
author: "Senckenberg Research Institute, Natural History Museum"
tags: [Biodiversity,Neotropical realm,Privacy,Taxonomy (biology),Science]
---


This database is made up of museum collections from the past 150 years and demonstrates that some Neotropical regions, such as the Cerrado in the central Brazil, contain a disproportionately high diversity. About 10,500 species of reptiles (animals such as lizards and snakes) are found around the world and about 150 to 200 new species are also discovered every year. The international group of scientists have collected data about snake collections of the Neotropics—South and Central America, the West Indies and the southern part of Mexico and Florida—to record the diversity of snake species, their distribution, as well as their threats. The result is a unique database with 147,515 entries for 886 snake species from 12 families. Senior author of the study Alexandre Antonelli from the University of Gothenburg says, We have published one of the largest and most detailed surveys on the distribution of snakes—one of the most species-rich reptile groups in the world.

<hr>

[Visit Link](https://phys.org/news/2017-11-comprehensive-neotropical-snakes.html){:target="_blank" rel="noopener"}


