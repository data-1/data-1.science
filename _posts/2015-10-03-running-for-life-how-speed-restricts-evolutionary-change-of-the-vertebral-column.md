---
layout: post
title: "Running for life: How speed restricts evolutionary change of the vertebral column"
date: 2015-10-03
categories:
author: Naturalis Biodiversity Center 
tags: [Vertebral column,Vertebra]
---


One of the riddles of mammal evolution explained: the strong conservation of the number of trunk vertebrae. They measured variation in vertebrae of 774 individual mammal skeletons of both fast and slow running species. The researchers found that a combination of developmental and biomechanical problems prevents evolutionary change in the number of trunk vertebrae in fast running and agile mammals. The stiffness of the back of a mammal is key to whether evolutionary change is possible or not, said Frietson Galis, researcher at Naturalis Biodiversity Center and one of the authors of the study. Photograph 2  Title: Authors Frietson Galis (left) and Clara ten Broek investigating the vertebral column of a Thomson's gazelle.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-07/nbc-rfl071114.php){:target="_blank" rel="noopener"}


