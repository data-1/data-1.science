---
layout: post
title: "Nearest ancestor of living bears discovered from Gansu, China"
date: 2015-10-28
categories:
author: Chinese Academy Of Sciences
tags: [Sinotherium,Ursavus,Molar (tooth)]
---


While establishing a new genus, Agriarctos, based on some isolated lower cheek teeth from the latest Miocene deposits in Hungary, Kretzoi (1942) transferred the type specimen of Ursavus depereti to his new genus Agriarctos, and tended to associate it with his smaller species, Agriarctos vighi. Based mainly on the diagnostic characters of the m1, i.e., the tooth being short and wide, with strong, anteriorly shifted metaconid, thus forming a rather closed trigonid, Agriarctos was considered by Kretzoi an intermediate form between Ursavus and the Agriotherium-Indarctos lineage. As thus defined, the primitive members of the Ursidae include: Ballusia (3 species), Ursavus (8 species), Agriarctos (3 species), Ailurarctos lufengensis and Kretzoiarctos beatrix, altogether 5 genera and 16 species. Being one of the latest occurred (~8 Ma) and highly advanced member of the early ancestral ursids, the Huaigou skull may be quite different from the majority of the species in Early-Middle Miocene and early Late Miocene in possessing more advanced characters leading to the living bears. Credit: Deng Tao  Taken as a whole, there is no doubt that Ursavus tedfordi is the latest and the most advanced among known ancestral forms of the living ursine bears.

<hr>

[Visit Link](http://phys.org/news326447899.html){:target="_blank" rel="noopener"}


