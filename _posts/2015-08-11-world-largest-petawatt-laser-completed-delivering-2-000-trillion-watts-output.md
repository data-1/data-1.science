---
layout: post
title: "World-largest petawatt laser completed, delivering 2,000 trillion watts output"
date: 2015-08-11
categories:
author: Osaka University 
tags: [Laser,Watt,Diffraction,Diffraction grating,Optics,Electrical engineering,Physics,Technology,Electromagnetism,Electromagnetic radiation,Science,Electricity]
---


High power output implemented by a 4-beam amplifier technology and the world's highest performance dielectric multilayer diffraction grating of large diameter [2]. Background and output of research  Petawatt lasers are used for study of basic science, generating such high-energy quantum beams as neutrons and ions, but only a few facilities in the world have Petawatt laser. This high output has been implemented by a 4-beam amplifier technology and the world's highest performance dielectric multilayer diffraction grating of a large diameter. LFEX laser enables us to generate high energy pulses of quantum beams with large current, and one can expect such medical applications as particle beam cancer therapy and non-destructive inspection for bridges and etc. Furthermore, it is expected that the fast ignition scheme proposed by ILE, Osaka, will produce fusion energy with a relatively small laser system.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-08/ou-wpl080615.php){:target="_blank" rel="noopener"}


