---
layout: post
title: "Integral’s orbits 2002–17"
date: 2018-08-16
categories:
author: ""
tags: [INTEGRAL,Physical phenomena,Astronomy,Space science,Outer space,Spaceflight,Physical sciences,Astronautics,Flight,Sky,Spacecraft,Astronomical objects,Science,Nature,Solar System]
---


ESA’s Integral space observatory has been orbiting Earth for 15 years, observing the ever-changing, powerful and violent cosmos in gamma rays, X-rays and visible light. Studying stars exploding as supernovas, monster black holes and, more recently, even gamma-rays that were associated with gravitational waves, Integral continues to broaden our understanding of the high-energy Universe. This image visualises the orbits of the spacecraft since its launch on 17 October 2002, until October of this year. Integral travels in a highly eccentric orbit. The orbit brought it to within 2756 km of Earth at its closest, on 25 October 2011, to 159 967 km at the furthest, two days later.

<hr>

[Visit Link](http://www.esa.int/ESA_Multimedia/Images/2017/11/Integral_s_orbits_2002_17){:target="_blank" rel="noopener"}


