---
layout: post
title: "Gorilla origins of the last two AIDS virus lineages confirmed"
date: 2016-04-15
categories:
author: University Of Pennsylvania School Of Medicine
tags: [Beatrice Hahn,HIV,Western lowland gorilla,Health sciences,Infectious diseases,Medical specialties,Microbiology,Epidemiology]
---


Scanning electron micrograph of an HIV-infected H9 T cell. Credit: NIAID  Two of the four known groups of human AIDS viruses (HIV-1 groups O and P) have originated in western lowland gorillas, according to an international team of scientists from the Perelman School of Medicine at the University of Pennsylvania, the University of Montpellier, the University of Edinburgh, and others. Beatrice Hahn, MD, a professor of Medicine and Microbiology, and others from Penn were part of the team, whose findings appear online this week in the Proceedings of the National Academy of Sciences. HIV-1, the virus that causes AIDS, has jumped species to infect humans on at least four separate occasions, generating four HIV-1 lineages—groups M, N, O, and P. Previous research from this team found that groups M and N originated in geographically distinct chimpanzee communities in southern Cameroon, but the origins of groups O and P remained uncertain. Two of the gorilla virus lineages were particularly closely related to HIV-1 groups O and P. This told us that these two groups originated in western lowland gorillas.

<hr>

[Visit Link](http://phys.org/news344520638.html){:target="_blank" rel="noopener"}


