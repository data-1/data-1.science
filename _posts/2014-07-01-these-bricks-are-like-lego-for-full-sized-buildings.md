---
layout: post
title: "These bricks are like Lego for full-sized buildings"
date: 2014-07-01
categories:
author: Condé Nast, Olivia Solon
tags: [Brick,Building insulation,Concrete,Wall,Technology,Materials,Architectural design,Building,Civil engineering,Artificial materials,Building materials,Manufacturing,Architecture,Construction,Building technology,Building engineering,Economic sectors]
---


Read more: Gallery: These bricks are like Lego for full-sized buildings  The bricks feature open internal spaces for insulation, which means that buildings made with the bricks require less energy for heating and cooling. Removable panels allow for easy access to these infrastructure elements so that portions of walls don't need to be torn down for maintenance. The bricks can be used to make floors, walls and ceilings and the company says that if it constructs the average five storey building using the bricks it can save around 30 percent energy compared with traditional construction methods. Kite Bricks was set up by Ronnie Zohar, who has another company that applies layers of film over windows in order to improve insulation. It's been in development for three years, with one of the most tricky elements being developing a formulation of concrete that's light and strong like steel.

<hr>

[Visit Link](http://www.wired.co.uk/news/archive/2014-07/01/kite-bricks){:target="_blank" rel="noopener"}


