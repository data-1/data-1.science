---
layout: post
title: "Study reveals the genetic start-up of a human embryo"
date: 2016-06-29
categories:
author: "Karolinska Institutet"
tags: [Gene,Zygote,Embryo,Fertilisation,Development of the human body,Egg,Cell biology,Molecular genetics,Branches of genetics,Developmental biology,Reproduction,Biochemistry,Biological processes,Molecular biology,Biotechnology,Biology,Life sciences,Genetics]
---


The study, which is being published in the journal Nature Communications, provides an in-depth understanding of early embryonic development in human -- and scientists now hope that the results will help finding for example new therapies against infertility. One day after fertilization there are two cells, after two days four, after three days eight and so on, until there are billions of cells at birth. In the current study, scientists found that only 32 of these genes are switched on two days after fertilization, and by day three there are 129 activated genes. The researchers had to develop a new way of analyzing the results in order to find the new genes. In the current study, the researchers show that the newly identified genes can interact with the 'junk DNA', and that this is essential to the start of development.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150903081200.htm){:target="_blank" rel="noopener"}


