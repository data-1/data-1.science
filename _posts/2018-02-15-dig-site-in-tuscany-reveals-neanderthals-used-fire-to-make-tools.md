---
layout: post
title: "Dig site in Tuscany reveals Neanderthals used fire to make tools"
date: 2018-02-15
categories:
author: "Bob Yirka"
tags: [Straight-tusked elephant,Neanderthal]
---


Wood, as the researchers note, has always been a popular material for crafting tools and weapons. The sticks were found at a site in Tuscany, Italy, called Poggetti Vecchi—an area that has previously given up Neanderthal artifacts. In studying the sticks, the researchers found them to be made from boxwood, a particularly hard wood. The team notes that modern hunter-gatherers use roughly the same technique in making their digging sticks. Wooden tools and fire technology in the early Neanderthal site of Poggetti Vecchi (Italy),(2018).

<hr>

[Visit Link](https://phys.org/news/2018-02-site-tuscany-reveals-neanderthals-tools.html){:target="_blank" rel="noopener"}


