---
layout: post
title: "Burning like the Sun"
date: 2016-03-16
categories:
author:  
tags: [ITER,Nuclear fusion,Fusion power,Composite material,Nature,Technology,Chemistry,Energy,Materials,Physical sciences]
---


Applications Burning like the Sun 16/03/2016 16001 views 163 likes  Engineers building parts of a new type of power plant for generating green energy with nuclear fusion are using their expertise from building rockets like Europe’s Ariane 5 to create the super-strong structures to cope with conditions similar to those inside the Sun. A technique for building launcher and satellite components has turned out to be the best way for constructing rings to support the powerful magnetic coils inside the machine. We have to make a special composite material which is durable and lightweight, non-conductive and never changes shape.” At their centre of excellence in Spain with its track record in composites for space applications, CASA Espacio has been at the forefront of developing a technique for embedding carbon fibres in resin to create a strong, lightweight material. With a diameter of 5 m and a solid cross-section of 30x30 cm, ITER’s compression rings will hold the giant magnets in place. A commercial successor for generating electricity is not predicted before 2050.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Engineering_Technology/TTP2/Burning_like_the_Sun){:target="_blank" rel="noopener"}


