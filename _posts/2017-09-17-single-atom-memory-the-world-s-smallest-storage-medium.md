---
layout: post
title: "Single atom memory: The world's smallest storage medium"
date: 2017-09-17
categories:
author: "Institute for Basic Science"
tags: [Computer data storage,Bit,Atom,Moores law,Quantum mechanics,Center for Quantum Nanoscience,Technology,Computing,Science]
---


One bit of digital information can now be successfully stored in an individual atom, according to a study just published in Nature. A certain direction of magnetization corresponds to the 0 bit, the other direction to the 1 bit. Then, when Heinrich's team of researchers tried to use two holmium atoms instead of one, they made another surprising discovery. In this way, the scientists could build a two bit device with four possible types of memory: 1-1, 0-0, 1-0 and 0-1 clearly distinguished by the iron sensor. This research may spur innovation in commercial storage media that will expand the possibilities of miniaturizing data storage.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-03/ifbs-sam030917.php){:target="_blank" rel="noopener"}


