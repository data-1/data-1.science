---
layout: post
title: "Gene circuit design strategy to advance synthetic biology"
date: 2017-10-04
categories:
author: "University of Illinois College of Engineering"
tags: [Synthetic biology,Gene,Synthetic biological circuit,Escherichia coli,News aggregator,Biology,Gene regulatory network,Life sciences,Biotechnology,Branches of science,Technology]
---


With the increase of circuit complexity, the lack of predictive design guidelines has become a major challenge in realizing the potential of synthetic biology, said Lu, who also is affiliated with the Carl R. Woese Institute for Genomic Biology and Physics Department at Illinois. Lu and his graduate students, Chen Liao and Andrew Blanchard, recently addressed the challenge by constructing an integrated modeling framework for quantitatively describing and predicting gene circuit behaviors. Although Lu's framework was established using E. coli as the model host, it has the potential to be generalized for describing multiple host organisms. According to Lu, this work advances the quantitative understanding of gene circuit behaviors, and facilitates the transformation of gene network design from trial-and-error construction to rational forward engineering. By systematically illustrating key cellular processes and multilayered circuit-host interactions, it further sheds light on quantitative biology towards a better understanding of complex bacterial physiology.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/09/170926162303.htm){:target="_blank" rel="noopener"}


