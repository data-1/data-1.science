---
layout: post
title: "Why public libraries need to support open source"
date: 2017-09-24
categories:
author: "Phil Shapiro
(Alumni)"
tags: [Open source,Library,Red Hat,Linux,Public library,Technology,Computing]
---


What can we do to make this better so that more people can turn to their public library to learn about open source software, hardware, and principles? Find out where to point someone if you don't know the answer to their question. Learn all about it from Opensource.com's resource page, and I have a few videos related to installing Linux on refurbished computers:  Learning about Raspberry Pi  Open source is at the heart of the maker movement. How to help your public library  One thing you can do is go talk to them. How to build X (thing) with Y (open source tool)  Libraries are changing.

<hr>

[Visit Link](https://opensource.com/life/16/9/public-library-open-source){:target="_blank" rel="noopener"}


