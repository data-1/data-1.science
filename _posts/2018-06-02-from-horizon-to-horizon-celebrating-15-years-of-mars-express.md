---
layout: post
title: "From horizon to horizon: Celebrating 15 years of Mars Express"
date: 2018-06-02
categories:
author: ""
tags: [Tharsis,Mars,Valles Marineris,Ascraeus Mons,Planets of the Solar System,Solar System,Astronomy,Space science,Geology,Outer space,Bodies of the Solar System,Astronomical objects known since antiquity,Planetary science,Terrestrial planets]
---


Fifteen years ago, ESA’s Mars Express was launched to investigate the Red Planet. On 2 June 2003, the Mars Express spacecraft lifted off from Baikonur, Kazakhstan, on a journey to explore our red-hued neighbouring planet. It sweeps from the planet’s upper horizon — marked by the faint blue haze at the top of the frame — down across a web of pale fissures named Noctis Labyrinthus (a part of Valles Marineris stretching to the upper left corner of the image), Ascraeus and Pavonis Mons (two of Tharsis’ four great volcanoes at more than 20 km high), and finishes at the planet’s northern polar ice cap (in this perspective, North is to the lower left). Elevation on Mars is defined relative to where the gravity is the same as the average at the martian equator. Most of Tharsis is higher than average, at between 2 and 10 km high.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Mars_Express/From_horizon_to_horizon_Celebrating_15_years_of_Mars_Express){:target="_blank" rel="noopener"}


