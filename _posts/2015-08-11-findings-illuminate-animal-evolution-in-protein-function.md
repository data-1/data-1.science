---
layout: post
title: "Findings illuminate animal evolution in protein function"
date: 2015-08-11
categories:
author: Eric Beidel, Virginia Commonwealth University
tags: [Cell (biology),Evolution,Ion channel,Phosphatidylinositol 45-bisphosphate,Biology,Inward-rectifier potassium channel,Potassium channel,Biophysics,Biotechnology,Molecular biology,Biochemistry,Cell biology]
---


Virginia Commonwealth University and University of Richmond researchers recently teamed up to explore the inner workings of cells and shed light on the 400–600 million years of evolution between humans and early animals such as sponges. For the most part, human and sponge ion channels are the same. The recent paper explores one key difference that researchers now believe developed about the time the first animals evolved. The sponge Kir channel, however, does not share this high affinity with PIP2 as it lacks two amino acids necessary for the interaction. Researchers compared amino acids in both sponge and mammal Kir channels.

<hr>

[Visit Link](http://phys.org/news/2015-07-illuminate-animal-evolution-protein-function.html){:target="_blank" rel="noopener"}


