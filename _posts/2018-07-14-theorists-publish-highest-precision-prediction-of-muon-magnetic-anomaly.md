---
layout: post
title: "Theorists publish highest-precision prediction of muon magnetic anomaly"
date: 2018-07-14
categories:
author: "Brookhaven National Laboratory"
tags: [Muon,Particle physics,Muon g-2,Fermilab,Standard Model,Fundamental interaction,Physics,Spin (physics),Physical sciences,Theoretical physics,Science]
---


Credit: Fermilab  Theoretical physicists at the U.S. Department of Energy's (DOE's) Brookhaven National Laboratory and their collaborators have just released the most precise prediction of how subatomic particles called muons—heavy cousins of electrons—wobble off their path in a powerful magnetic field. The new experiment at Fermilab, combined with the higher-precision calculations, will provide a more stringent test of the Standard Model, the reigning theory of particle physics. If there's another particle that pops into existence and interacts with the muon before it interacts with the magnetic field, that could explain the difference between the experimental measurement and our theoretical prediction, said Christoph Lehner, one of the Brookhaven Lab theorists involved in the latest calculations. The path to reduced uncertainty  By everything he means how all the known particles of the Standard Model affect muons via nature's four fundamental forces—gravity, electromagnetism, the strong nuclear force, and the electroweak force. This has been the least understood part of the theory, and therefore the greatest source of uncertainty in the overall prediction.

<hr>

[Visit Link](https://phys.org/news/2018-07-theorists-publish-highest-precision-muon-magnetic.html){:target="_blank" rel="noopener"}


