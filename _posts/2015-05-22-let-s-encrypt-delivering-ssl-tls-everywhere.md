---
layout: post
title: "Let’s Encrypt: Delivering SSL/TLS Everywhere"
date: 2015-05-22
categories:
author: ""    
tags: [Public key certificate,Lets Encrypt,Certificate authority,Application layer protocols,Internet Standards,Software,Security engineering,Computer science,Digital rights,Cryptography,Computing,Technology,Information Age,Computer networking,Cyberspace,Computer security,Telecommunications,Secure communication,Information technology,Cyberwarfare,Internet,Internet protocols]
---


Then why don’t we use TLS (the successor to SSL) everywhere? Every server in every data center supports it. Let’s Encrypt is a new free certificate authority, built on a foundation of cooperation and openness, that lets everyone be up and running with basic server certificates for their domains through a simple one-click process. The ISRG welcomes other organizations dedicated to the same ideal of ubiquitous, open Internet security. The entire enrollment process for certificates occurs painlessly during the server’s native installation or configuration process, while renewal occurs automatically in the background.

<hr>

[Visit Link](https://letsencrypt.org//2014/11/18/announcing-lets-encrypt.html){:target="_blank" rel="noopener"}


