---
layout: post
title: "You don't know Bash: An introduction to Bash arrays"
date: 2018-06-02
categories:
author: "Robert Aboukhalil"
tags: [Bash (Unix shell),Array data type,Command-line interface,Array data structure,Pipeline (Unix),Parameter (computer programming),Bracket,Control flow,String interpolation,Computer programming,Software engineering,Computing,Software development,Computers,Technology,Computer science,Information technology management,Software,Computer engineering,Programming interfaces]
---


As a first step, you want to do a parameter sweep to evaluate how well the pipeline makes use of threads. In turn, this allows us to specify the index to access, e.g., echo ${allThreads[1]} returns the second element of the array. Looping through arrays  Although in the examples above we used integer indices in our arrays, let's consider two occasions when that won't be the case: First, if we wanted the $i -th element of the array, where $i is a variable containing the index of interest, we can retrieve that element using: echo ${allThreads[$i]} . Some useful syntax  But before diving into the code, we need to introduce some more syntax. First, we need to be able to retrieve the output of a Bash command.

<hr>

[Visit Link](https://opensource.com/article/18/5/you-dont-know-bash-intro-bash-arrays){:target="_blank" rel="noopener"}


