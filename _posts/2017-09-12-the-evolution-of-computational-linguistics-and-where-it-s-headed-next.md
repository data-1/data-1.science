---
layout: post
title: "The evolution of computational linguistics and where it's headed next"
date: 2017-09-12
categories:
author: "Andrew Myers, Stanford University"
tags: [Language,Natural language processing,Artificial intelligence,Translation,Machine translation,Grammar,Linguistics,Cognition,Systems science,Science,Communication,Branches of linguistics,Interdisciplinary subfields,Computing,Technology,Human communication,Cognitive science,Branches of science]
---


When I was younger, I was totally interested in computer science and artificial intelligence but I also found human languages fascinating with their grammatical structures and complexity. In natural language processing, you need the computer to understand the world. Natural language processing, even if trained in the earlier meaning, can look at a word like terrific and see the positive context. Recently, there has been a big leap in translation quality from deep learning or neural machine translation approaches, which have been explored at Stanford and also at the University of Montreal. I'm sure people will still be working on this in 20 years.

<hr>

[Visit Link](https://phys.org/news/2017-05-evolution-linguistics.html){:target="_blank" rel="noopener"}


