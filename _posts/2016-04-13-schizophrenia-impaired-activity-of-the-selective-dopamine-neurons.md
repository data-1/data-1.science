---
layout: post
title: "Schizophrenia: Impaired activity of the selective dopamine neurons"
date: 2016-04-13
categories:
author: Goethe University Frankfurt
tags: [Schizophrenia,Dopamine,Health,Clinical medicine,Diseases and disorders,Mental disorders,Human diseases and disorders,Causes of death,Mental health,Abnormal psychology,Medicine,Neuroscience]
---


Schizophrenia is not only associated with positive symptoms such as hallucinations and delusions, but also with negative symptoms e.g. cognitive deficits and impairments of the emotional drive. However, the underlying neurophysiological impairments of dopamine neurons remained unresolved. Now, Prof. Eleanor Simpson and Prof. Jochen Roeper, in cooperation with the mathematician Prof. Gaby Schneider of the Goethe University and the physiologist Prof. Birgit Liss of the University of Ulm have succeeded in defining the neurophysiological impairments with the dopamine system. They were able to show, with single cell recordings in the intact brain of mice, that those dopamine midbrain neurons responsible for emotional and cognitive processing displayed altered patterns and frequencies of electrical activity. Our results show that altered neuronal activity of selective dopamine neurons is crucial for schizophrenia, Jochen Roeper summarises the importance of the research work.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-02/guf-sia021715.php){:target="_blank" rel="noopener"}


