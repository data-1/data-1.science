---
layout: post
title: "Calibre 1.42 Is the Best eBook Editing Tool on Linux"
date: 2014-06-27
categories:
author: Silviu Stahie, Jun
tags: [Calibre (software),HTML,EPUB,Technology,Application software,Intellectual works,Information Age,Computing,Software,Software development,Software engineering,Computer science,Digital media,Information technology management,Computer engineering,Computers,Computer programming,System software,Digital technology]
---


A new version of the Calibre eBook reader, editor, and library management software, has been released and features even more options to edit books. One of the latest features added to the application is the ability to edit books, which is something that very few other apps can do. As you can imagine, editing books is actually a very complete operation and the Calibre dev has already made numerous fixes and changes to this feature. Fortunately, the developer also provides a complete list of dependencies, if you feel brave enough. If you don't want to compile Calibre on your own, the developer offers a simple command line that takes care of the entire installation, making the whole process a lot smoother.

<hr>

[Visit Link](http://news.softpedia.com/news/Calibre-1-42-Is-the-Best-eBook-Editing-Tool-on-Linux-448563.shtml){:target="_blank" rel="noopener"}


