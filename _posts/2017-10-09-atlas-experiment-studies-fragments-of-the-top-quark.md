---
layout: post
title: "ATLAS experiment studies fragments of the top quark"
date: 2017-10-09
categories:
author: "Atlas Experiment"
tags: [Top quark,Quark,ATLAS experiment,Elementary particle,Quantum field theory,Subatomic particles,Nuclear physics,Physics,Standard Model,Theoretical physics,Quantum mechanics,Elementary particles,Quantum chromodynamics,Particle physics,Science]
---


Measured values of the top quark mass determined from the differential production rates. In order to measure the production rates of top quark pairs, the ATLAS Experiment examined events with an electron, muon, and one or two jets that were likely to have originated from bottom quarks. This method was used to explore the kinematics of the electrons and muons originating from top quark decays. The result also allowed ATLAS to make a precise new determination of the top quark mass. The use of electron and muon kinematics has helped to reduce these ambiguities, and the new ATLAS measurement of the top quark mass has a smaller theoretical ambiguity than other measurements that examine jets from bottom and lighter quarks.

<hr>

[Visit Link](https://phys.org/news/2017-10-atlas-fragments-quark.html){:target="_blank" rel="noopener"}


