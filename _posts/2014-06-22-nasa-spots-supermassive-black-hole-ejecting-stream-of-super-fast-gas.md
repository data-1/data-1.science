---
layout: post
title: "NASA Spots Supermassive Black Hole Ejecting Stream of Super Fast Gas"
date: 2014-06-22
categories:
author: Science World Report
tags: [Galaxy,Active galactic nucleus,Black hole,Supermassive black hole,Milky Way,NGC 5548,Space science,Astronomy,Astronomical objects,Physical sciences,Outer space,Astrophysics,Sky]
---


Scientists have made a surprising discovery when it comes to a supermassive black hole at the heart of the galaxy, NGC 5548. They've found that this black hole is emitting a stream of rapidly flowing gas that blocks about 90 percent of its emitted X-rays, an extremely unusual behavior that's telling scientists a bit more about how supermassive black holes interact with their host galaxies. Using NASA's Hubble Space Telescope and data from other spacecraft, the scientists combined their findings. These new winds reach speeds of up to 3,107 miles per second, but is much closer to the nucleus than the persistent wind, said Jelle Kaastra, one of the researchers, in a news release. This is the first time we've seen a stream like this move into our line of sight.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15562/20140620/nasa-spots-supermassive-black-hole-ejecting-stream-super-fast-gas.htm){:target="_blank" rel="noopener"}


