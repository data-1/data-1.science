---
layout: post
title: "Scientists 'squeeze' light one particle at a time: Scientists have measured a bizarre effect in quantum physics, in which individual particles of light are said to have been 'squeezed'"
date: 2015-09-02
categories:
author: St John's College, University of Cambridge 
tags: [Photon,Light,Quantum mechanics,Physics,Atom,Quantum fluctuation,Vacuum,Laser,Scientific theories,Science,Physical sciences,Theoretical physics,Applied and interdisciplinary physics,Atomic molecular and optical physics,Physical chemistry,Optics]
---


A team of scientists has successfully measured particles of light being squeezed, in an experiment that had been written off in physics textbooks as impossible to observe. This involves exciting a single atom with just a tiny amount of light. Even in a situation where there is no light, electromagnetic noise still exists. Even lasers, the most perfect light source known, carry this level of fluctuating noise. By scattering faint laser light from the quantum dot, the noise of part of the electromagnetic field was reduced to an extremely precise and low level, below the standard baseline of vacuum fluctuations.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/08/150831120304.htm){:target="_blank" rel="noopener"}


