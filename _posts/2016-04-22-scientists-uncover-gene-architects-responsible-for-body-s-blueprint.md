---
layout: post
title: "Scientists uncover gene 'architects' responsible for body's blueprint"
date: 2016-04-22
categories:
author: Walter, Eliza Hall Institute
tags: [Embryo,Hox gene,Spinal cord,Birth defect,Biology,Life sciences]
---


Associate Professor Anne Voss (right) and Associate Professor Tim Thomas led the research team from the Walter and Eliza Hall Institute in Melbourne, Australia.Previous work by the research team from the Walter and Eliza Hall Institute showed that the protein MOZ could relay external 'messages' to the developing embryo, revealing a mechanism for how the environment could affect development in very early pregnancy. Dr Bilal Sheikh, Associate Professor Tim Thomas, Associate Professor Anne Voss and colleagues have now discovered that MOZ and the protein BMI1 play opposing roles in giving developing embryos the set of instructions needed to ensure that body segments including the spine, nerves and blood vessels develop correctly and in the right place. Associate Professor Voss said the study revealed that the proteins tightly regulated Hox gene expression in early embryonic development. We discovered that MOZ and BMI1 were important for initiating and correctly timing Hox gene expression, ensuring the genes were activated at the right time and in the right place, she said. We know that Hox genes can be directly affected by too much vitamin A, which can cause severe deformities in the embryo, Associate Professor Voss said.

<hr>

[Visit Link](http://phys.org/news348143304.html){:target="_blank" rel="noopener"}


