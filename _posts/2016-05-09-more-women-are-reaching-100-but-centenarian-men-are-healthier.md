---
layout: post
title: "More women are reaching 100 but centenarian men are healthier"
date: 2016-05-09
categories:
author: King's College London
tags: [Chronic condition,Health,News aggregator,Disease,Health care,Health sciences,Medicine,Clinical medicine,Medical specialties,Causes of death]
---


New research conducted by a team at King's College London has found an increasing trend in the number of people in the UK reaching age 100 over the past two decades. The study also found that, whilst women were far more likely to reach 100 than men, males tended to be healthier and had fewer diagnosed chronic illnesses compared to women. Findings suggested a 50 per cent increase in the number of females reaching the age of 100 years between 1990 and 2013 and that women were four times more likely to reach age 100 than men. The increase in the overall number of centenarians and conditions associated with reaching this age suggest that the utilisation of health care services by the elderly may increase substantially and could also have an impact on the associated health care costs. However, further research is needed to understand why some people reach extreme old age without severe health problems and others do not.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/06/150622101047.htm){:target="_blank" rel="noopener"}


