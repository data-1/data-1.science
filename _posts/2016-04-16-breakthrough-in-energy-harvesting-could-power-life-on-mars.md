---
layout: post
title: "Breakthrough in energy harvesting could power life on Mars"
date: 2016-04-16
categories:
author: Northumbria University
tags: [Mars,Mars Reconnaissance Orbiter,Carbon dioxide,Leidenfrost effect,Engine,Earth,Nature,Physical sciences]
---


Leidenfrost Engine  Martian colonists could use an innovative new technique to harvest energy from carbon dioxide thanks to research pioneered at Northumbria University, Newcastle. Northumbria's research proposes using the vapour created by this effect to power an engine. This is the first time the Leidenfrost effect has been adapted as a way of harvesting energy. If utilised in a Leidenfrost-based engine dry-ice deposits could provide the means to create future power stations on the surface of Mars. Mars manned mission  One of the co-authors of Northumbria's research, Dr Rodrigo Ledesma-Aguilar, said: Carbon dioxide plays a similar role on Mars as water does on Earth.

<hr>

[Visit Link](http://phys.org/news344767948.html){:target="_blank" rel="noopener"}


