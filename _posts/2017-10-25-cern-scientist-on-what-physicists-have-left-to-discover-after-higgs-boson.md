---
layout: post
title: "CERN Scientist on What Physicists Have Left to Discover After Higgs Boson"
date: 2017-10-25
categories:
author:  
tags: [Higgs boson,Elementary particle,Physics,CERN,Boson,Standard Model,Nuclear physics,Quantum field theory,Quantum mechanics,Theoretical physics,Particle physics,Science,Physical sciences]
---


They had observed the Higgs boson, an elementary particle predicted by one of the best models for physics in the universe. Those in the know might have been anticipating this observation for decades, but that didn't dampen the excitement. But this discovery of a new particle, predicted by many and finally observed after so much work and anticipation — was different. So for us, there was no celebration downtime, Beacham said. There are still particles that physicists have predicted, but which no one has yet observed.

<hr>

[Visit Link](https://futurism.com/cern-scientist-on-life-after-the-higgs-boson/){:target="_blank" rel="noopener"}


