---
layout: post
title: "Modular brains help organisms learn new skills without forgetting old skills"
date: 2015-12-09
categories:
author: PLOS
tags: [Artificial intelligence,Neural network,PLOS,Catastrophic interference,Computational biology,Intelligence,Cognition,Cybernetics,Neuroscience,Learning,Interdisciplinary subfields,Computing,Science,Technology,Branches of science,Cognitive science]
---


The findings--published this week in PLOS Computational Biology--not only shed light on the evolution of intelligence in natural animals, but will also accelerate attempts to create artificial intelligence (AI). Kai Olav Ellefsen (Norwegian University of Science and Technology), Jean-Baptiste Mouret (Pierre & Marie Curie University) and Jeff Clune (University of Wyoming) used simulations of evolving computational brain models called artificial neural networks to show that more modular brains learn more and forget less. The researchers found that modularity significantly reduced such catastrophic forgetting in these computer brains. Jeff Clune adds: The ultimate goal of artificial intelligence research is to produce AI that can learn many different skills and get better at each of them over time, just as humans and animals do. Image Link: http://www.plos.org/wp-content/uploads/2015/03/robot_two_skills.png  Image Credit: Dreamstime LLC  Image Link: http://www.plos.org/wp-content/uploads/2015/03/brainToNeuronZoom-HighRes.jpg  All works published in PLOS Computational Biology are Open Access, which means that all content is immediately and freely available.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/p-mbh032615.php){:target="_blank" rel="noopener"}


