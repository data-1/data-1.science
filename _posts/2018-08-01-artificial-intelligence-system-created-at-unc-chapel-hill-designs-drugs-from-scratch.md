---
layout: post
title: "Artificial intelligence system created at UNC-Chapel Hill designs drugs from scratch"
date: 2018-08-01
categories:
author: "University of North Carolina at Chapel Hill"
tags: [University of North Carolina at Chapel Hill,Virtual screening,Drug discovery,American Association for the Advancement of Science,University,Branches of science,Science,Cognitive science,Technology,Education,Cognition]
---


An artificial-intelligence approach created at the University of North Carolina at Chapel Hill Eshelman School of Pharmacy can teach itself to design new drug molecules from scratch and has the potential to dramatically accelerate the design of new drug candidates. Virtual screening allows scientists to evaluate existing large chemical libraries, but the method only works for known chemicals. The team has used ReLeaSE to generate molecules with properties that they specified, such as desired bioactivity and safety profiles. ###  About the University of North Carolina at Chapel Hill  The University of North Carolina at Chapel Hill, the nation's first public university, is a global higher education leader known for innovative teaching, research and public service. More than 169,00 live in North Carolina.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-07/uonc-ais073118.php){:target="_blank" rel="noopener"}


