---
layout: post
title: "New research maps areas most vulnerable to ocean acidification"
date: 2015-10-20
categories:
author: Noaa Headquarters
tags: [Ocean acidification,Ocean,Earth sciences,Natural environment,Environmental science,Applied and interdisciplinary physics,Nature,Oceanography,Hydrography,Hydrology,Chemistry,Physical sciences,Physical geography]
---


This map shows the global distribution of aragonite saturation at 50 meters depth. The graphic shows areas that are most vulnerable to ocean acidification since they are regions where the saturation of aragonite is lower. Aragonite is a calcium carbonate mineral that shellfish use to build their shells. The saturation state of seawater for a mineral such as aragonite is a measure of the potential for the mineral to form or to dissolve. In these areas, deep ocean waters that are naturally rich in carbon dioxide are upwelling and mixing with surface waters that are absorbing carbon dioxide from the atmosphere.

<hr>

[Visit Link](http://phys.org/news/2015-10-areas-vulnerable-ocean-acidification.html){:target="_blank" rel="noopener"}


