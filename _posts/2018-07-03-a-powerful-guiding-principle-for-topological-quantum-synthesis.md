---
layout: post
title: "A powerful guiding principle for topological quantum synthesis"
date: 2018-07-03
categories:
author: "Science China Press"
tags: [Topological insulator,Science,Topology,Materials,Quantum mechanics,Condensed matter,Materials science,Physical chemistry,Chemical product engineering,Condensed matter physics,Applied and interdisciplinary physics,Chemistry,Physical sciences,Physics]
---


Topological materials are a new, rapidly expanding family of quantum matter, which can be classified into topological insulators (TIs), topological crystalline insulators, topological Dirac semimetals, topological Weyl semimetals, topological nodal-line semimetals, and so on. For a given compound system, identification of its topological nature is generally complex, demanding specific determination of the appropriate topological invariant through detailed electronic structure and Berry curvature calculations. The topologically nontrivial nature is tied to the appearance of inverted bands in the electronic structure. For most topological materials, band inversions have been demonstrated to be induced by delicate synergistic effects of different physical factors, including chemical bonding, crystal field and, most notably, spin-orbit coupling (SOC). Recently, several so-called high-throughput methods were successfully developed for predicting TIs.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/scp-apg122517.php){:target="_blank" rel="noopener"}


