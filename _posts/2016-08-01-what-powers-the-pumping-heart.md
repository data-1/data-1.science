---
layout: post
title: "What powers the pumping heart?"
date: 2016-08-01
categories:
author: "University of Toronto"
tags: [Heart,Protein,Arrhythmia,Muscle contraction,Cell (biology),Cell biology,Biology,Life sciences,Biotechnology,Physiology,Biochemistry]
---


In addition to providing a new understanding of what makes our hearts pump, these findings could also help researchers uncover new information about how heart disease affects the signal pathways in our hearts. In this study, our focus was on Tmem65, but there are 555 proteins that we identified and showed that they are present throughout many species and are conserved throughout evolution-- at least in the mouse and the human -- in the heart's membrane-enriched contractile cells. Tmem65 was only the number-one candidate in our study, but theoretically, we have 554 other proteins to work through, says Gramolini. Gramolini says the findings are essential for understanding cardiac biology and hopes they open the door for further study into health and disease in his lab and others. These are molecules that haven't been studied, but must play some role in heart function.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-09/uot-wpt092515.php){:target="_blank" rel="noopener"}


