---
layout: post
title: "Magnetic 'wormhole' connecting two regions of space created for the first time"
date: 2015-09-03
categories:
author: Autonomous University of Barcelona 
tags: [News aggregator,Physics,Electromagnetism,Physical sciences,Applied and interdisciplinary physics,Electrical engineering,Science]
---


In electromagnetism, however, advances in metamaterials and invisibility have allowed researchers to put forward several designs to achieve this. Scientists in the Department of Physics at the Universitat Autònoma de Barcelona have designed and created in the laboratory the first experimental 'wormhole' that can connect two regions of space magnetically. This consists of a tunnel that transfers the magnetic field from one point to the other while keeping it undetectable -- invisible -- all the way. The sphere is made in such a way as to be magnetically undetectable -- invisible, in magnetic field terms -- from the exterior. These same researchers had already built a magnetic fibre in 2014: a device capable of transporting the magnetic field from one end to the other.

<hr>

[Visit Link](http://www.sciencedaily.com/releases/2015/09/150903081506.htm){:target="_blank" rel="noopener"}


