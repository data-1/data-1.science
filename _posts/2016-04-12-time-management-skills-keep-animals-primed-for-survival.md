---
layout: post
title: "Time management skills keep animals primed for survival"
date: 2016-04-12
categories:
author: PLOS
tags: [PLOS,Decision-making,American Association for the Advancement of Science,Trade-off,Computational biology,Open access,Branches of science,Science,Cognitive science]
---


Many animals may have a previously under-appreciated ability to make up for lost time with more effort, according to new research publishing this week in PLOS Computational Biology. Researchers from Princeton University challenge the conventional view that animals face a simple trade-off between the speed and the accuracy of their decisions. Image Credit: Daniel Rubenstein  Image Link: http://www.plos.org/wp-content/uploads/2014/12/18-Dec-de-Froment-Lion.jpg  All works published in PLOS Computational Biology are Open Access, which means that all content is immediately and freely available. Use this URL in your coverage to provide readers access to the paper upon publication: http://www.ploscompbiol.org/article/info:doi/10.1371/journal.pcbi.1003937  Press-only preview: http://www.plos.org/wp-content/uploads/2014/12/plcb-10-12-de-Froment.pdf  Contact: Adrian de Froment  Address: Princeton University  Ecology and Evolutionary Biology  106a Guyot Hall  Princeton, NJ 8544  UNITED STATES  Phone: 001-609-258-6880  Email: adriandefroment@gmail.com  Citation: de Froment AJ, Rubenstein DI, Levin SA (2014) An Extra Dimension to Decision-Making in Animals: The Three-way Trade-off between Speed, Effort per-Unit-Time and Accuracy. PLoS Comput Biol 10(12): e1003937.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-12/p-tms121114.php){:target="_blank" rel="noopener"}


