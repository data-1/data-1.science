---
layout: post
title: "Past and present genomes tell the story of Native American biological origins"
date: 2015-09-07
categories:
author: Carl R. Woese Institute for Genomic Biology, University of Illinois at Urbana-Champaign 
tags: [Settlement of the Americas,Genetics]
---


The study was led by the Centre for GeoGenetics at the University of Copenhagen; more than 80 researchers contributed sequence data and analyses of key ancient individuals, and from living individuals in the Americas and possible ancestral regions, including Siberia and Oceania. This finding supported the idea that a single group migrated to the Americas, and subsequently split into two distinct populations. The current study suggests that the migrating population remained isolated in this area for about 8,000 years, before moving further into the Americas. Instead, the Central and South American remains examined were more closely related to other Native American groups. There were traces of genetic similarity to East Asian and Australo-Melanesian populations found in some of the Native American groups examined; analyses suggested this similarity was produced through sporadic contact between Native American and Eurasian populations, rather than two independent migration events.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-07/crwi-pap072715.php){:target="_blank" rel="noopener"}


