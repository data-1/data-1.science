---
layout: post
title: "Elasmosaur Fossil Discovered in Alaska Mountain Range Reveals New Marine Reptile"
date: 2015-08-11
categories:
author: Science World Report
tags: [Elasmosaurus,Plesiosaur,Taxa,Paleontology]
---


They have extremely long necks and two pairs of paddle-like limbs that they use to swim underwater. Picture the mythical Loch Ness monster and you have a pretty good idea what it looked like, said Patrick Druckenmiller, one of the researchers, in a news release. This is an exciting find because it is the first time an elasmosaur has ever been discovered in Alaska. Curvin Metzler, an Anchorage-based fossil collector, found several vertebrae in the area while hiking. He then went to the location again in June with three others and began excavating the skeleton.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/28202/20150727/elasmosaur-fossil-discovered-alaska-mountain-range-reveals-new-marine-reptile.htm){:target="_blank" rel="noopener"}


