---
layout: post
title: "Particle accelerator on a microchip"
date: 2015-12-21
categories:
author: Deutsches Elektronen-Synchrotron DESY
tags: [DESY,Particle accelerator,Electron,Laser,Particle physics,X-ray,Radiation,Science,Technology,Physics,Applied and interdisciplinary physics,Electromagnetism]
---


Within five years, they hope to produce a working prototype of an 'accelerator-on-a-chip'. For decades, particle accelerators have been an indispensable tool in countless areas of research -- from fundamental research in physics to examining the structure of biomolecules in order to develop new drugs. The advantage is that everything is up to fifty times smaller, explains Franz Kärtner who is a Leading Scientist at DESY, as well as a professor at the University of Hamburg and the Massachusetts Institute of Technology (MIT) in the US, and a member of Hamburg's Centre for Ultrafast Imaging (CUI), and who heads a similar project in Hamburg, funded by the European Research Council. Among other things, DESY is working on a high-precision electron source to feed the elementary particles into the accelerator modules, a powerful laser for accelerating them, and an electron undulator for creating X-rays. In addition, the interaction between the miniature components is not yet a routine matter, especially not when it comes to joining up several accelerator modules.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-11/ded-pao111915.php){:target="_blank" rel="noopener"}


