---
layout: post
title: "How the Arctic Ocean became saline"
date: 2018-08-18
categories:
author: "Alfred Wegener Institute, Helmholtz Centre for Polar and Marine Research"
tags: [Ocean,Arctic Ocean,Atlantic Ocean,Natural environment,Applied and interdisciplinary physics,Hydrography,Oceanography,Hydrology,Physical geography,Earth sciences,Nature,Environmental engineering,Geography,Climate,Earth phenomena,Environmental science,Water]
---


Every year, ca. Only once the land bridge between Greenland and Scotland disappeared did the first ocean passages emerge, connecting the Arctic with the North Atlantic and making water exchange possible. Only when the oceanic ridge lies below the surface mixed layer can the heavier saline water of the North Atlantic flow into the Arctic with relatively little hindrance, explains Stärz. Once the ocean passage between Greenland and Scotland had reached this critical depth, the saline Arctic Ocean as we know it today was created. What was once a land bridge now lies ca.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-06/awih-hta060617.php){:target="_blank" rel="noopener"}


