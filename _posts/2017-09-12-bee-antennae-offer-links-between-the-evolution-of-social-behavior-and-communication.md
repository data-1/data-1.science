---
layout: post
title: "Bee antennae offer links between the evolution of social behavior and communication"
date: 2017-09-12
categories:
author: "Princeton  University"
tags: [Sociality,Eusociality,Evolution,Bee,Insect,Species,Zoology,Biology,Biological evolution,Evolutionary biology]
---


Kocher and her colleagues imaged the antennae of adult females from 36 species that Kocher netted in the wild, mostly in France, or secured from specimens from the Museum of Comparative Zoology in the Department of Organismic and Evolutionary Biology at Harvard University and the American Museum of Natural History in New York. Using a scanning electron microscope at Princeton, they obtained information about the antennae's surface topography and composition and observed convergent changes in both sensilla structures and the chemical signals of the groups as sociality was gained and lost. These repeated gains and losses make the species one of the most behaviorally diverse social insects on the planet, and good candidates for studying sociality, according to Kocher. Relatively little is known about the evolutionary transition between solitary and social living, according to Kocher. In the paper, the researchers also noted that ancestrally solitary halictid bees -- those bees that had never evolved social behaviors -- had sensilla densities similar to eusocial species, while secondarily solitary halictid bees -- those bees that evolved from social to solitary and back -- exhibited decreases in sensilla density.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/06/170615142656.htm){:target="_blank" rel="noopener"}


