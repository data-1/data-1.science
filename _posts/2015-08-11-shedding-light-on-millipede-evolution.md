---
layout: post
title: "Shedding light on millipede evolution"
date: 2015-08-11
categories:
author: National Science Foundation
tags: [Motyxia,Bioluminescence,Green fluorescent protein,Aequorin,Aequorea victoria,Protein,Chemistry,Biology]
---


My goal is to find millipedes that are bioluminescent—meaning they produce light through a chemical reaction. My research team, which is funded by NSF, explores the evolution of bioluminescence in a genus known as Motyxia, the only millipedes in North America that are known to be bioluminescent. Our analyses of the chemical reaction used by M. bistipita to generate its faint glow suggests that this species might not have originally acquired bioluminescence as a defense mechanism. According to my explanation for the origins of bioluminescence in millipedes, as their evolution progressed and Motyxia colonized higher elevations that support more predators, this millipede repurposed and intensified its glow as a way to warn predators of its greater toxicity. Those include:  Helping to revolutionize our understanding of cancer by literally shining a light on what researchers could not see any other way.

<hr>

[Visit Link](http://phys.org/news/2015-08-millipede-evolution.html){:target="_blank" rel="noopener"}


