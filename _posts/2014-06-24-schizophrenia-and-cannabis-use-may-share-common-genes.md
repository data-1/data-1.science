---
layout: post
title: "Schizophrenia and cannabis use may share common genes"
date: 2014-06-24
categories:
author: King's College London 
tags: [Cannabis (drug),Schizophrenia,Auditory hallucination,Mental disorder,Kings College London,Health,Health care,Mental health,Medicine,Clinical medicine,Health sciences]
---


Genes that increase the risk of developing schizophrenia may also increase the likelihood of using cannabis, according to a new study led by King's College London, published today in Molecular Psychiatry. Previous studies have identified a number of genetic risk variants associated with schizophrenia, each of these slightly increasing an individual's risk of developing the disorder. The researchers found that people genetically pre-disposed to schizophrenia were more likely to use cannabis, and use it in greater quantities than those who did not possess schizophrenia risk genes. ###  For a copy of the paper, or interview with the author, please contact Seil Collins, Press Officer, Institute of Psychiatry, King's College London seil.collins@kcl.ac.uk / (+44) 0207 848 5377 / (+44) 07718 697 176  Additional funding was provided by the National Institutes of Health, Australian National health and Medical Research Council, Australian Research Council, GenomEUtwin Project, Centre for Research Excellence on Suicide Prevention in Australia, the National Institute for Health Research Biomedical Research Centre (NIHR BRC) at the South London and Maudsley NHS Foundation Trust and King's College London and the Netherlands Organization for Health Research and Development. King's Health Partners Academic Health Sciences Centre (AHSC) is a pioneering global collaboration between one of the world's leading research-led universities and three of London's most successful NHS Foundation Trusts, including leading teaching hospitals and comprehensive mental health services.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2014-06/kcl-sac062314.php){:target="_blank" rel="noopener"}


