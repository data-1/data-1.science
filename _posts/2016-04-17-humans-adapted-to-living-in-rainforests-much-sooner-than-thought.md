---
layout: post
title: "Humans adapted to living in rainforests much sooner than thought"
date: 2016-04-17
categories:
author: Oxford University
tags: [Human,Rainforest,Research]
---


The researchers from Oxford University, working with a team from Sri Lanka and the University of Bradford, analysed the carbon and oxygen isotopes in the teeth of 26 individuals, with the oldest dating back 20,000 years. The researchers studied the fossilised teeth of 26 humans of a range of dates - from 20,000 to 3,000 years ago. All of the teeth were excavated from three archaeological sites in Sri Lanka, which are today surrounded by either dense rainforest or more open terrain. The new evidence published in this paper argues this shows just how adaptable our earliest ancestors were. The results are significant in showing that early humans in Sri Lanka were able to live almost entirely on food found in the rainforest without the need to move into other environments.

<hr>

[Visit Link](http://phys.org/news345374563.html){:target="_blank" rel="noopener"}


