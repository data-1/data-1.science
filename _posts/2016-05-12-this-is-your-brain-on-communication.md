---
layout: post
title: "This is your brain on communication"
date: 2016-05-12
categories:
author: Uri Hasson
tags: [TED (conference),Brain,Memory,Neuroscience,Psychology,Linguistics,Language,Human communication,Interdisciplinary subfields,Cognitive psychology,Cognition,Cognitive science,Mental processes,Communication,Branches of science,Concepts in metaphysics]
---


Neuroscientist Uri Hasson researches the basis of human communication, and experiments from his lab reveal that even across different languages, our brains show similar activity, or become aligned, when we hear the same idea or story. This amazing neural mechanism allows us to transmit brain patterns, sharing memories and knowledge. We can communicate because we have a common code that presents meaning, Hasson says.

<hr>

[Visit Link](http://www.ted.com/talks/uri_hasson_this_is_your_brain_on_communication){:target="_blank" rel="noopener"}


