---
layout: post
title: "How to measure the impact of your open source project"
date: 2018-05-26
categories:
author: "Vinod Ahuja"
tags: [Free and open-source software,H-index,JQuery,Bootstrap (front-end framework),Software quality,Software,Technology,Computing,Software engineering]
---


Open source project impact  An effective way to understand an open source project’s impact is through its software libraries. Within the open source ecosystem, jQuery is an upstream project to Bootstrap, which itself is an upstream project to many websites and web frameworks, as shown below:  opensource.com  Measuring impact  Many metrics are being developed to measure the impact of an open source project. We propose, therefore, that a project’s impact in an open source ecosystem can be determined by downstream dependencies (i.e., how many downstream open source projects use them and how often those downstream projects are themselves used). Impact metrics measure the impact on users and other projects. The V-index was developed to measure impact metrics.

<hr>

[Visit Link](https://opensource.com/article/18/5/metrics-project-success){:target="_blank" rel="noopener"}


