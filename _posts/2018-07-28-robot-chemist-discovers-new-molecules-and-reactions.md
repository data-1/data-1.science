---
layout: post
title: "Robot chemist discovers new molecules and reactions"
date: 2018-07-28
categories:
author: "University Of Glasgow"
tags: [Chemistry,Chemical substance,Chemical reaction,Robot,Chemist,Digitization,Technology,Computing,Branches of science]
---


Credit: CC0 Public Domain  A new type of artificial-intelligence-driven chemistry could revolutionise the way molecules are discovered, scientists claim. Their 'self-driving' system, underpinned by machine learning algorithms, can find new reactions and molecules, allowing a digital-chemical data-driven approach to locating new molecules of interest, rather than being confined to a known database and the normal rules of organic synthesis. After exploring only around 100, or 10 percent, of the possible reactions, the robot was able to predict with over 80 percent accuracy which combinations of starting chemicals should be explored to create new reactions and molecules. The approach was designed and developed by the team lead by Professor Leroy (Lee) Cronin, the University of Glasgow's Regius Chair of Chemistry. Professor Cronin said: This approach is a key step in the digitisation of chemistry, and will allow the real time searching of chemical space leading to new discoveries of drugs, interesting molecules with valuable applications, and cutting cost, time, and crucially improving safety, reducing waste, and helping chemistry enter a new digital era.

<hr>

[Visit Link](https://phys.org/news/2018-07-robot-chemist-molecules-reactions.html){:target="_blank" rel="noopener"}


