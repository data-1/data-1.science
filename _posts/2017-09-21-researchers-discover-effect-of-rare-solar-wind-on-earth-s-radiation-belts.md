---
layout: post
title: "Researchers discover effect of rare solar wind on Earth's radiation belts"
date: 2017-09-21
categories:
author: "University Of New Hampshire"
tags: [Van Allen radiation belt,Solar wind,Earth,Earth analog,Space science,Astronomy,Outer space,Physical sciences,Science,Nature,Solar System,Astronomical objects,Spaceflight,Planetary science]
---


Credit: NASA  Researchers from the University of New Hampshire have captured unique measurements of the Van Allen radiation belts, which circle the Earth, during an extremely rare solar wind event. The findings, which have never been reported before, may be helpful in protecting orbiting telecommunication and navigational satellites, and possibly future astronauts, by helping to more accurately predict space conditions near Earth, as well as around more remote planets. The UNH researchers used data from more than 10 spacecrafts, including information from a UNH-led instrument on board NASA's Van Allen Probes twin satellites, to get measurements of both the Earth's inner and outer Van Allen radiation belts, two donut-shaped regions of high-energy particles trapped by Earth's magnetic field, during the uncommon solar wind conditions. This is the first time detailed measurements of the Earth's radiation belts have ever been recorded during such rare conditions, said Harlan Spence, director of EOS at UNH and a co-author of the study. When the Van Allen belts were first discovered in the 1950s they were thought to be relatively stable structures, but subsequent observations have shown they are dynamic and mysterious.

<hr>

[Visit Link](http://phys.org/news/2016-10-effect-rare-solar-earth-belts.html){:target="_blank" rel="noopener"}


