---
layout: post
title: "How molecular vibrations make photosynthesis efficient"
date: 2015-07-26
categories:
author: Vienna University Of Technology
tags: [Light,Photosynthesis,Quantum mechanics,Privacy,Science]
---


It was shown that the hotly debated quantum phenomena can be understood as a delicate interplay between vibrations and electrons of the involved molecules. The resulting theoretical model explains the experiments perfectly. The studied artificial light harvester is a supramolecule, consisting of hundreds of thousands of light absorbing molecules, arranged in close proximity to one another and in an orderly fashion. The research team employed polarized light to isolate the desired quantum-dynamical effects. Studying such ordered systems does not only further our understanding of natural photosynthesis, it also helps us to appreciate the physical mechanisms necessary for energy-efficient, cheaper, more flexible and lighter photovoltaic cells.

<hr>

[Visit Link](http://phys.org/news/2015-07-molecular-vibrations-photosynthesis-efficient.html){:target="_blank" rel="noopener"}


