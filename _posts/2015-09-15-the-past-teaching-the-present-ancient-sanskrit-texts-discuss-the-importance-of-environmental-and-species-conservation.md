---
layout: post
title: "The Past Teaching the Present: Ancient Sanskrit Texts Discuss the Importance of Environmental and Species Conservation"
date: 2015-09-15
categories:
author: "$author"   
tags: [Hinduism,Religion,Buddhism,Reincarnation,Kraken]
---


Nevertheless, this concept has been around for a much longer period of time, and can even be found in Sanskrit texts from ancient India. The Environment and Human Connection  Lessons about environmental conservation can be found within the teachings of Hinduism. The link between the senses and the elements forms the basis for the bond between human beings and the natural world. Furthermore, protection is given to many animals that are sacred in Hinduism and the killing of certain animals, including cats, snakes, monkeys and various birds, is a sin, and is punishable. Phajoding Gonpa, Thimphu, Bhutan (Wikimedia Commons )  Using Ancient Teachings in Today’s World  These are some of the messages passed down from the ancient Indians regarding environmental and species conservation.

<hr>

[Visit Link](http://www.ancient-origins.net/history-ancient-traditions/past-teaching-present-ancient-sanskrit-texts-discuss-importance-020527){:target="_blank" rel="noopener"}


