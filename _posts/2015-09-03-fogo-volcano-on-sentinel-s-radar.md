---
layout: post
title: "Fogo volcano on Sentinel’s radar"
date: 2015-09-03
categories:
author: "$author"   
tags: [Sentinel-1A,Sentinel-1,Deformation (volcanology),Volcano,Satellite,Outer space,Earth sciences,Space science,Geology]
---


Applications Fogo volcano on Sentinel’s radar 02/12/2014 13462 views 101 likes  Radar images from the Sentinel-1A satellite are helping to monitor ground movements of the recently erupted Fogo volcano. Lava flows are threatening nearby villages, and local residents have been evacuated. Deformation on the ground causes changes in radar signals that appear as the rainbow-coloured patterns. Mapping for emergency response Scientists can use the deformation patterns to understand the subsurface pathways of molten rock moving towards the surface. In this case, the radar shows that the magma travelled along a crack at least 1 km wide.

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Observing_the_Earth/Copernicus/Sentinel-1/Fogo_volcano_on_Sentinel_s_radar){:target="_blank" rel="noopener"}


