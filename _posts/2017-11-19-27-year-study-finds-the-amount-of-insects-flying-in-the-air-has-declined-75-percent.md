---
layout: post
title: "27-Year Study Finds the Amount of Insects Flying in the Air Has Declined 75 Percent"
date: 2017-11-19
categories:
author: ""
tags: [Insect,Agriculture,Climate change,Living Planet Index,Ecology,Natural environment,Nature,Earth sciences]
---


New data suggests the total population of flying insects there has declined a whopping 75% in the past 27 years. A study released Wednesday in the journal PLOS ONE details a longitudinal study by German researchers to measure flying insect biomass — the weight of all flying bugs — in 63 protected spots around the country. They were expecting to find some population decreases, but this extreme decline, they said, is alarming. At the peak of summer heat, when there are usually more bugs out than in the spring and fall, the drop was even more pronounced, and bug counts were down 82%, — that's 7% more than the average decline over the 27-year period. German birds are feeling the squeeze on their food supply — new research published Thursday shows that Germany lost 15% of its non-endangered bird population in the past 12 years.

<hr>

[Visit Link](https://futurism.com/insects-flying-declined-75-percent/){:target="_blank" rel="noopener"}


