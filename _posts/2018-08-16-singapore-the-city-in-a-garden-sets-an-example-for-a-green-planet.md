---
layout: post
title: "Singapore, the City in a Garden, sets an example for a green planet"
date: 2018-08-16
categories:
author: "Luciana Pricop"
tags: []
---


Related: A rainforest-like green heart grows within Singapore’s Marina One  Join Our Newsletter Receive the latest in global news and designs building a better future. “We merely wanted to rise above the region we found ourselves in,” Lim Liang Jim, group director of the National Biodiversity Centre at the National Parks Board, said in an interview with UN Environment. Keep us clean. Other ongoing projects include urban planning and zoning as well as policy changes and public awareness campaigns focused on a smaller carbon footprint and zero waste, among other goals. With its visionary leadership, Singapore’s long-term plan includes a phase of sustainable development found in its Sustainable Singapore Blueprint 2015, which underlines improvement in sectors that include all 17 of the UN Sustainable Development Goals through 2030.

<hr>

[Visit Link](https://inhabitat.com/singapore-the-city-in-a-garden-sets-an-example-for-a-green-planet){:target="_blank" rel="noopener"}


