---
layout: post
title: "What are asteroids?"
date: 2016-06-30
categories:
author: "Nancy Atkinson"
tags: [Asteroid,Near-Earth object,Impact event,Asteroid belt,Solar System,Jupiter trojan,Ceres (dwarf planet),Potentially hazardous object,Comet,Planet,Earth,Chelyabinsk meteor,Jupiter,Sun,4 Vesta,Planetary science,Planets,Physical sciences,Astronomy,Asteroids,Local Interstellar Cloud,Space science,Outer space,Astronomical objects,Bodies of the Solar System]
---


According to NASA, the average distance between objects in the asteroid belt is greater than 1-3 million km. Even though there may be millions of asteroids in the asteroid belt, most are small. The largest asteroid is asteroid Ceres at about 952 km (592 miles) in diameter, and Ceres is so large that it is also categorized as a dwarf planet. Near-Earth Asteroids: These objects have orbits that pass close by that of Earth. Here are some important dates in the history of our knowledge and study of asteroids, including spacecraft missions that flew by or landed on asteroids:  1801: Giuseppe Piazzi discovers the first and largest asteroid, Ceres, orbiting between Mars and Jupiter.

<hr>

[Visit Link](http://phys.org/news/2015-09-asteroids.html){:target="_blank" rel="noopener"}


