---
layout: post
title: "Artificial intelligence and accessible theater for the deaf and blind"
date: 2018-07-14
categories:
author: "Universidad Carlos III de Madrid"
tags: [Accessibility,Augmented reality,Sign language,Cognitive science,Technology,Cognition,Branches of science,Communication]
---


This new technology from UC3M's SoftLab research group has been implemented to give accessibility to the Broadway musical comedy The Addams Family at the Teatro Calderón of Madrid. The creative agency C&W, COMUNICAdos and Escena Global also collaborated on the project, which received support from the Consejería de Educación e Innovación (Council for Education and Innovation) of the Autonomous Community of Madrid. This means that over two millions deaf and blind people in Spain can, for the first time in their lives, attend an important, high-quality musical such as this. This innovation, developed by this research group from UC3M's Instituto de Desarrollo Tecnológico y Promoción de la Innovación Pedro Juan de Lastanosa (Pedro Juan de Lastanosa Institute for Technological Development and Innovation), is based on software that uses an augmented reality system to allow members of the audience to individually see adapted subtitles and a sign language interpreter, and listen the audio description. Using deep learning techniques combined with audio processing the software achieves the perfect synchronization of the performance and accessibility elements.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-12/ucid-aia121117.php){:target="_blank" rel="noopener"}


