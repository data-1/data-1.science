---
layout: post
title: "Crocodile ancestor was top predator before dinosaurs roamed North America"
date: 2015-07-21
categories:
author: North Carolina State University 
tags: [Carnufex,Dinosaur,Triassic,Crocodylomorpha,Taxa,Paleontology]
---


Paleontologists from North Carolina State University and the North Carolina Museum of Natural Sciences recovered parts of Carnufex's skull, spine and upper forelimb from the Pekin Formation in Chatham County, North Carolina. The Pekin Formation contains sediments deposited 231 million years ago in the beginning of the Late Triassic (the Carnian), when what is now North Carolina was a wet, warm equatorial region beginning to break apart from the supercontinent Pangea. Typical predators roaming Pangea included large-bodied rauisuchids and poposauroids, fearsome cousins of ancient crocodiles that went extinct in the Triassic Period. However, the discovery of Carnufex indicates that in the north, large-bodied crocodylomorphs, not dinosaurs, were adding to the diversity of top predator niches. Early crocodylomorph increases top tier predator diversity during rise of dinosaurs  DOI: 10.1038/srep09276  Authors: Lindsay Zanno, Susan Drymala, NC State University and the NC Museum of Natural Sciences; Vincent Schneider, NC Museum of Natural Sciences; Sterling Nesbitt, Virginia Polytechnic Institute  Published: March 19, 2015 in Scientific Reports  Abstract:  Triassic predatory guild evolution reflects a period of ecological flux spurred by the catastrophic end-Permian mass extinction and terminating with the global ecological dominance of dinosaurs in the early Jurassic.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-03/ncsu-caw031715.php){:target="_blank" rel="noopener"}


