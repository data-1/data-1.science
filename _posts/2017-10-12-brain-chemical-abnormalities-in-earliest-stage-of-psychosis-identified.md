---
layout: post
title: "Brain chemical abnormalities in earliest stage of psychosis identified"
date: 2017-10-12
categories:
author: "Elsevier"
tags: [Psychosis,NMDA receptor,Antipsychotic,Schizophrenia,Mental disorders,Clinical medicine,Neuroscience,Diseases and disorders,Health,Mental health,Causes of death,Human diseases and disorders,Abnormal psychology,Medicine]
---


Published in Biological Psychiatry, the study led by Dr. Dost Öngür of Harvard Medical School provides the first ever measurement of glycine levels in patients with psychotic disorders. Abnormal brain activity in psychotic disorders, such as schizophrenia and bipolar disorder, is thought to stem in part from impaired function of the NMDA receptor. But first author Dr. Sang-Young Kim and colleagues applied a new method of the brain imaging technique called MR spectroscopy to suppress the interfering signal and reveal the hidden glycine signal. Our findings suggest that glycine abnormalities may play a role in the earliest phases of psychotic disorders, said Dr. Öngür. The researchers also measured increased glutamate levels in patients, which lines up with strong support for elevated glutamate reported in other studies of first-episode psychosis.

<hr>

[Visit Link](https://www.sciencedaily.com/releases/2017/10/171011120339.htm){:target="_blank" rel="noopener"}


