---
layout: post
title: "DNA reveals the origins of modern Europeans"
date: 2016-04-19
categories:
author: Alan Cooper And Wolfgang Haak, The Conversation
tags: [Yamnaya culture]
---


Along with our colleagues, we have been using genome sequencing technology to analyse the vast array of ancient skeletons recovered from across Europe, ranging from the original hunter-gatherer inhabitants to the first farmers who appear around 8,000 years ago, and right up to the early Bronze Age 3,500 years ago. It also reveals the mysterious source for the Indo-European languages. The genetic results have answered a number of contentious and long-standing questions in European history. This group of pastoralists, with domestic horses and oxen-drawn wheeled carts, appear to be responsible for up to 75% of the genomic DNA seen in central European cultures 4,500 years ago, known as the Corded Ware Culture. Archaeologists had two major hypotheses: the language family came with either the invading Near East farming wave more than 8,000 years ago, or some form of steppe population sometime much later.

<hr>

[Visit Link](http://phys.org/news346314892.html){:target="_blank" rel="noopener"}


