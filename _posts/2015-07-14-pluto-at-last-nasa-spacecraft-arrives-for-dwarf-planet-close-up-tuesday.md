---
layout: post
title: "Pluto at Last! NASA Spacecraft Arrives for Dwarf Planet Close-Up Tuesday"
date: 2015-07-14
categories:
author: Mike Wall
tags: [New Horizons,Pluto,Charon (moon),Dwarf planet,Planet,Astronautics,Spacecraft,Physical sciences,Space exploration,Flight,Planets of the Solar System,Local Interstellar Cloud,Planets,Solar System,Space science,Astronomy,Outer space,Science,Planetary science,Bodies of the Solar System,Astronomical objects,Spaceflight,Planemos]
---


On Tuesday morning (July 14) — nine and a half years after launching, and a quarter-century after its mission began to take shape — NASA's New Horizons spacecraft will perform history's first flyby of Pluto. [See the Latest Pluto Photos by New Horizons]  The spacecraft is performing very well. On Sunday, NASA and New Horizons scientists unveiled the latest photos from New Horizons, including a tantalizing view of what appears to be canyons and craters on Pluto's moon Charon, and a new view of the dwarf planet itself. (The dwarf planet orbits about 39 times farther from the sun than Earth does, on average.) That flyby a generation ago marked the last time humanity got its first up-close look at a planet — and left Pluto as the only planet in the solar system yet to receive a spacecraft visit.

<hr>

[Visit Link](http://www.livescience.com/51529-pluto-flyby-new-horizons-closeup-tuesday.html){:target="_blank" rel="noopener"}


