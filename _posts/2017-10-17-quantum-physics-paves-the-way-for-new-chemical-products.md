---
layout: post
title: "Quantum physics paves the way for new chemical products"
date: 2017-10-17
categories:
author: Open University
tags: [Electron,Physics,Chemistry,Privacy,Molecule,Technology,Science,Physical sciences]
---


Credit: Open University  Research by an OU molecular physicist has discovered that electrons can control chemical reactions in experiments leading to purer, cheaper chemical products. Observing these low energy electrons, physicists from both The Open University and Tata Institute of Fundamental Research, Mumbai, have discovered that chemical reactions can be controlled using electrons – rather than lasers, which are currently used – providing a much cheaper alternative to controlling chemical reactions. Professor of Molecular Physics in the School of Physical Sciences at The Open University, Nigel Mason, is leading on the research; he said:  The ability to control chemical reactions is one of the major goals in chemistry; it would enable scientists and the manufacturing industry to reduce production costs and waste by targeting only the chemicals they want. Momentum images of H from H2 and D from D2 at different electron energies. The one at 4 eV for H is symmetric, while those above 14 eV are strongly asymmetric.

<hr>

[Visit Link](https://phys.org/news/2017-10-quantum-physics-paves-chemical-products.html){:target="_blank" rel="noopener"}


