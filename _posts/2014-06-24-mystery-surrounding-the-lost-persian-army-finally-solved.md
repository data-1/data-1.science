---
layout: post
title: "Mystery Surrounding the Lost Persian Army Finally Solved"
date: 2014-06-24
categories:
author: Science World Report
tags: [Petubastis III,Achaemenid Empire,Cambyses II,Ancient Egypt]
---


The long quest to dig out the reason behind the disappearance of a troop of 50,000 Persian soldiers in the Egyptian desert around 524 BC has ended. They were not swallowed up by a sandstorm as ancient historians would like us to believe but very likely defeated by a rebel army and to cover up the ignominious defeat, subsequent rulers supported the fable of a great sandstorm swallowing an army, according to an Egyptologist Olaf Kaper, professor at Leiden University  The Greek historian Herodotus wrote that the entire troop was swallowed by a sand storm. His research suggests that the army was proceeding to the Dachla Oasis in the desert, where the Egyptian rebel leader Petubastis III was waiting with his troops. To cover up the defeat of his predecessor he put out the story of the great sandstorm. Professor Kaper unearthed an ancient temple block that had the entire list of titles of Petubastis III.

<hr>

[Visit Link](http://www.scienceworldreport.com/articles/15620/20140624/mystery-surrounding-the-lost-persian-army-finally-solved.htm){:target="_blank" rel="noopener"}


