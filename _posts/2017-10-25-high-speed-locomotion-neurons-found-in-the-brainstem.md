---
layout: post
title: "High-speed locomotion neurons found in the brainstem"
date: 2017-10-25
categories:
author: University of Basel
tags: [Brainstem,Neuron,Brain,Neurophysiology,Anatomy,Neuroanatomy,Organs (anatomy),Nervous system,Physiology,Neuroscience]
---


A clearly defined subpopulation of neurons in the brainstem is essential to execute locomotion at high speeds. Brainstem plays important role in control of movement  Spinal circuits receive key instructions from the brain about when and how to perform a movement. Strikingly, if all of these neurons were studied together, no clear pattern with respect to the elicited motor program emerged. Neurons with distinct locomotor functions are intermingled  Paolo Capelli, PhD student in Arber's group and first author of the study, remembers that the most exciting breakthrough of the project was when he started to study the identified neuronal cell types separately: When we activated neurons releasing the excitatory neurotransmitter glutamate in one small region of the brainstem called Lateral Paragigantocellular nucleus (LPGi), but not in other neighboring regions, we reliably induced full body locomotion at short latency. Disentangling neuron types crucial for insights  These findings provide an important step forward for a better understanding of the neuronal underpinnings at work in the brainstem during the control of movement.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-10/uob-hln102417.php){:target="_blank" rel="noopener"}


