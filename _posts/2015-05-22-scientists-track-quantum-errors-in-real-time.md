---
layout: post
title: "Scientists track quantum errors in real time"
date: 2015-05-22
categories:
author: Holly Lauridsen, Yale University
tags: [Quantum error correction,Quantum computing,Photon,Qubit,Quantum mechanics,Physics,Theoretical physics,Science,Branches of science]
---


To combat errors, physicists must be able to detect that an error has occurred and then correct it in real time, a process known as quantum error correction. To determine if an error occurred, Schoelkopf and his team relied on an ancilla, or a more stable reporter atom, which detected errors without destroying the state and relayed that information back to the scientists on a computer. So the ancilla reported only the photon parity—whether an even or odd number of quantum photons were present in the box—in real time. The team found success in their first experiment and demonstrated for the first time the tracking of naturally occurring errors, in real time, as would be needed for a real quantum computer. The Yale team is now studying how to fix errors, the second step in quantum error correction and an essential capability for functional quantum computers.

<hr>

[Visit Link](http://phys.org/news324551043.html){:target="_blank" rel="noopener"}


