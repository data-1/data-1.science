---
layout: post
title: "The 101 of ELF files on Linux: Understanding and Analysis - Linux..."
date: 2015-09-30
categories:
author: Michael Boelen
tags: [Executable and Linkable Format,Executable,Endianness,Library (computing),Computer program,Linux kernel,Linker (computing),Computers,Computer engineering,Software engineering,Computer programming,Computer architecture,Software development,Computer science,Software,Technology,Information technology management,Operating system technology,System software,Information Age,Systems engineering,Computer data,Computing]
---


By reading this guide, you will learn:  Why ELF is used and for what kind of files  Understand the structure of ELF and the details of the format  How to read and analyze an ELF file such as a binary  Which tools can be used for binary analysis  What is an ELF file? An ELF file consists of:  ELF header File data  With the readelf command, we can look at the structure of a file and it will look something like this:  ELF header  As can be seen in this screenshot, the ELF header starts with some magic. It shows a formatted output very similar to the ELF header file. File data  Besides the ELF header, ELF files consist of three parts. Sections are viewed by the linker to create executable code or shared objects.

<hr>

[Visit Link](http://linux-audit.com/elf-binaries-on-linux-understanding-and-analysis/){:target="_blank" rel="noopener"}


