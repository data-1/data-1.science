---
layout: post
title: "From Light To Bright: San Diego Is Building The World's Largest Municipal Internet Of Things"
date: 2017-10-19
categories:
author:  
tags: [General Electric,Parking,Technology,Street light,Computer network]
---


It just degrades over time.”As GE replaced the lights, San Diego asked for a better way to monitor the LEDs. “Now we know exactly how much energy a streetlight is using,” Graham says. San Diego agreed to be the first city to pilot GE’s intelligent cities platform — enabled by adding nodes holding multiple sensors to streetlights. “With sensored street lights, you can have more efficient use of your on-street parking,” Graham says. “This is highly valuable information to help cities manage and mitigate crime.”The city is replacing 14,000 lights with LED fixtures, and 3,600 will be equipped with the new intelligent nodes.

<hr>

[Visit Link](http://www.gereports.com/light-bright-san-diego-leads-way-future-smart-cities/){:target="_blank" rel="noopener"}


