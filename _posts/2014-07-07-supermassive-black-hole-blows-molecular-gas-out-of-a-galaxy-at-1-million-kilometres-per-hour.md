---
layout: post
title: "Supermassive black hole blows molecular gas out of a galaxy at 1 million kilometres per hour"
date: 2014-07-07
categories:
author: University Of Sheffield
tags: [Galaxy,Astronomy,Milky Way,Astrophysical jet,Supermassive black hole,Black hole,Physical sciences,Space science,Astronomical objects,Astrophysics,Physics,Science]
---


Credit: NASA  (Phys.org)—New research by academics at the University of Sheffield has solved a long-standing mystery surrounding the evolution of galaxies, deepening our understanding of the future of the Milky Way. The supermassive black holes in the cores of some galaxies drive massive outflows of molecular hydrogen gas. As a result, most of the cold gas is expelled from the galaxies. As a result of this collision, gas will become concentrated at the centre of the system, fuelling its supermassive black hole, and potentially leading to the formation of jets that will then eject the remaining gas from the galaxy – just as we already observe in IC5063. It is extraordinary that the molecular gas can survive being accelerated by jets of electrons moving at close to the speed of light.

<hr>

[Visit Link](http://phys.org/news323946368.html){:target="_blank" rel="noopener"}


