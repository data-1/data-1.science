---
layout: post
title: "A guide to CRISPR gene activation"
date: 2016-05-24
categories:
author: Wyss Institute for Biologically Inspired Engineering at Harvard
tags: [Cas9,CRISPR gene editing,Synthetic biology,DanaFarber Cancer Institute,Gene,Genetics,Technology,Health sciences,Biological engineering,Branches of genetics,Molecular genetics,Biochemistry,Molecular biology,Biotechnology,Biology,Life sciences]
---


To that end, they have built a number of synthetic gene activating Cas9 proteins to study gene functions or to compensate for insufficient gene expression in potential therapeutic approaches. The main challenge was that all had been uniquely designed and tested in different settings; there was no side-by-side comparison of their relative potentials, said George Church, Ph.D., who is Core Faculty Member at the Wyss Institute for Biologically Inspired Engineering at Harvard University, leader of its Synthetic Biology Platform, and Professor of Genetics at Harvard Medical School. Three of them, provided much higher gene activation than the other candidates while maintaining high specificities toward their target genes, said Marcelle Tuttle, Research Fellow at the Wyss and a co-lead author of the study. This study provides valuable new design criteria that will help enable synthetic biologists and bioengineers to develop more effective targeted genome engineering technologies in the future, said Wyss Institute Founding Director Donald Ingber, M.D., Ph.D., who is the Judah Folkman Professor of Vascular Biology at Harvard Medical School and the Vascular Biology Program at Boston Children's Hospital, and also Professor of Bioengineering at the Harvard John A. Paulson School of Engineering and Applied Sciences. The Wyss Institute creates transformative technological breakthroughs by engaging in high risk research, and crosses disciplinary and institutional barriers, working as an alliance that includes Harvard's Schools of Medicine, Engineering, Arts & Sciences and Design, and in partnership with Beth Israel Deaconess Medical Center, Brigham and Women's Hospital, Boston Children's Hospital, Dana-Farber Cancer Institute, Massachusetts General Hospital, the University of Massachusetts Medical School, Spaulding Rehabilitation Hospital, Boston University, Tufts University, Charité - Universitätsmedizin Berlin, University of Zurich and Massachusetts Institute of Technology.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-05/wifb-agt052216.php){:target="_blank" rel="noopener"}


