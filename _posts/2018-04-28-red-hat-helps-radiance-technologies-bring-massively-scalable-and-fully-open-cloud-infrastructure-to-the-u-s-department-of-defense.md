---
layout: post
title: "Red Hat Helps Radiance Technologies Bring Massively-Scalable and Fully-Open Cloud Infrastructure to the U.S. Department of Defense"
date: 2018-04-28
categories:
author: ""
tags: [Red Hat,Cloud computing,OpenStack,Information Age,Computer science,Information technology,Technology,Computing]
---


SYDNEY – OpenStack Summit Sydney 2017 - November 7, 2017 —  Red Hat, Inc. (NYSE: RHT), the world's leading provider of open source solutions, is today announcing that Radiance Technologies, Inc., an employee owned small business offering a variety of services and products for government and commercial customers, has deployed a tailored solution on behalf of the Department of Defense (DoD) using Red Hat Cloud Suite, Red Hat’s hybrid cloud offering that enables organizations to more easily build and manage cloud-native applications through a single platform. Additionally, Radiance had only 90 days to provision and deploy this new solution from scratch. To meet these needs, Radiance, supported by Red Hat, used many of the technologies comprising Red Hat Cloud Suite, including:  Red Hat OpenStack Platform , supported by Red Hat Ceph Storage , to enable a massively scalable, highly agile private cloud infrastructure along with enabling rapid provisioning of virtual machines. With the technology requirements and turnaround time, Red Hat Consulting also assisted Radiance, tailoring this particular build and environment to the unique needs of the DoD. Supporting Quotes  Paul Smith, senior vice president and general manager, U.S. Public Sector, Red Hat  “Given the breadth and complexity often involved in defense IT, the capability to more easily build cloud-native applications, deploy them and manage these workloads in a single platform can be important.

<hr>

[Visit Link](https://www.redhat.com/en/about/press-releases/red-hat-helps-radiance-technologies-bring-massively-scalable-and-fully-open-cloud-infrastructure-us-department-defense){:target="_blank" rel="noopener"}


