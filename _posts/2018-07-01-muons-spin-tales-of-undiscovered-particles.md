---
layout: post
title: "Muons spin tales of undiscovered particles"
date: 2018-07-01
categories:
author: "Savannah Mitchem, Argonne National Laboratory"
tags: [G-factor (physics),Muon,Particle physics,Spin (physics),Fermilab,Physics,Science,Physical sciences,Applied and interdisciplinary physics]
---


The original experiment measured the spin precession of the muon — i.e., the speed at which its spin changes direction — to be different from the theoretical predictions. Their experiment could point to the existence of physics beyond our current understanding, including undiscovered particles. The experiment follows one that began in 1999 at the DOE's Brookhaven National Laboratory in which scientists measured the spin precession of the muon—i.e., the speed at which its spin changes direction—to be different from the theoretical predictions. Once they calibrated the probes, the scientists placed 17 of them on a circular trolley that moves about the ring at Fermilab. The trolley measures the field at around 10,000 points, creating a map of the field strength everywhere in the ring.

<hr>

[Visit Link](https://phys.org/news/2018-04-muons-tales-undiscovered-particles.html){:target="_blank" rel="noopener"}


