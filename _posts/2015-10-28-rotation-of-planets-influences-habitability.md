---
layout: post
title: "Rotation of planets influences habitability"
date: 2015-10-28
categories:
author: Amanda Doyle, Astrobio.Net
tags: [Exoplanet,Planetary habitability,Circumstellar habitable zone,Venus,Runaway greenhouse effect,Atmosphere of Earth,Star,Greenhouse effect,Atmosphere,Atmospheric circulation,Sun,Physical sciences,Nature,Outer space,Planetary science,Space science,Planets,Astronomy,Planemos,Exoplanetology,Stellar astronomy,Astronomical objects]
---


In astronomy, a habitable zone is a region of space around a star where conditions are favorable for life as it may be found on Earth. Air circulation and rotation rates  The radiation that the Earth receives from the Sun is strongest at the equator. Image Credit: NASA  If, on the other hand, the planet is a slow rotator, then the Hadley cells can expand to encompass the entire world. The high albedo clouds can allow a planet to remain habitable even at levels of radiation that were previously thought to be too high, so that the inner edge of the habitable zone is pushed much closer to the star. This research emphasises the importance of looking beyond the traditional habitable zone for planets that could host life, and it turns out that planets we once thought were too hot might actually be just right for life.

<hr>

[Visit Link](http://phys.org/news326700691.html){:target="_blank" rel="noopener"}


