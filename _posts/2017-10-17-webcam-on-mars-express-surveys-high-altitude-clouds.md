---
layout: post
title: "Webcam on Mars Express surveys high-altitude clouds"
date: 2017-10-17
categories:
author:  
tags: [Cloud,Mars,Atmosphere,Ice,Water vapor,Earth sciences,Planetary science,Outer space,Astronomy,Nature,Planets of the Solar System,Sky,Solar System,Bodies of the Solar System,Applied and interdisciplinary physics,Planets,Space science]
---


Science & Exploration Webcam on Mars Express surveys high-altitude clouds 17/10/2017 8193 views 104 likes  An unprecedented catalogue of more than 21 000 images taken by a webcam on ESA’s Mars Express is proving its worth as a science instrument, providing a global survey of unusual high-altitude cloud features on the Red Planet. Now, the first paper has been published, on detached, high-altitude cloud features and dust storms over the edge, or ‘limb’, of the planet. From the 18 studied in depth, most were concluded to be water-ice clouds, and one was attributed to a dust storm. Limb observations by the webcam indicated the altitude was about 65 km. “We will continue to maintain the database with systematic observations from the webcam to provide wide views of atmospheric phenomena.”  Notes for editors “Limb clouds and dust on Mars from images obtained by the Visual Monitoring Camera (VMC) onboard Mars Express,” A. Sánchez-Lavega et al is published in Icarus 299 (2018).

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Mars_Express/Webcam_on_Mars_Express_surveys_high-altitude_clouds){:target="_blank" rel="noopener"}


