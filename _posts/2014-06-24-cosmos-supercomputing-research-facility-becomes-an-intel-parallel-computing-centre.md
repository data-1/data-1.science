---
layout: post
title: "COSMOS supercomputing research facility becomes an Intel Parallel Computing Centre"
date: 2014-06-24
categories:
author: University Of Cambridge
tags: [Xeon Phi,Supercomputer,Intel,Coprocessor,Microprocessor,Xeon,Parallel computing,Multi-core processor,Computing,Computer science,Computer engineering,Computer architecture,Technology,Office equipment,Information Age,Computer hardware,Computers]
---


As an IPCC, the COSMOS research facility will receive enhanced Intel support from its applications and engineering teams, as well as early access to future Intel Xeon Phi and other Intel products aimed at high-performance computing. IPCC status will allow COSMOS to better focus on delivering computing advances to the scientific community it serves and also highlight the efforts Intel has put into advancing high-performance computing. The research centre has already developed Xeon Phi for use in Planck Satellite analysis of the cosmic microwave sky and for simulations of the very early Universe. I am very pleased that the COSMOS supercomputer centre has been selected among the vanguard of Intel Parallel Computing Centres worldwide, said Professor Stephen Hawking, founder of the COSMOS Consortium. Intel Parallel Computing Centres are collaborations to modernise key applications to unlock performance gains that come through parallelism, enabling the way for the next leap in discovery.

<hr>

[Visit Link](http://phys.org/news322806960.html){:target="_blank" rel="noopener"}


