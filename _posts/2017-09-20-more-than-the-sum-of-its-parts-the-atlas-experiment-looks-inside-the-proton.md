---
layout: post
title: "More than the sum of its parts: the ATLAS Experiment looks inside the proton"
date: 2017-09-20
categories:
author: "Atlas Experiment"
tags: [ATLAS experiment,Jet (particle physics),Gluon,Quark,Proton,Quantum chromodynamics,Hadron,Elementary particle,Large Hadron Collider,Theoretical physics,Physics,Particle physics,Quantum mechanics,Quantum field theory,Science,Applied and interdisciplinary physics,Standard Model,Nuclear physics]
---


Measured inclusive jet cross section as a function of the jet transverse momentum. It depicts the proton (and other hadrons) as a system of elementary particles, in this case quarks and gluons. QCD explains how these quarks and gluons interact and, consequently, what emerges from high-energy proton–proton collisions at the LHC. One of the remarkable features of QCD is that quarks and gluons cannot be observed as free particles. QCD also predicts that jets of hadrons produced in LHC collisions will fly away from the interaction point in a few distinct directions.

<hr>

[Visit Link](https://phys.org/news/2017-06-sum-atlas-proton.html){:target="_blank" rel="noopener"}


