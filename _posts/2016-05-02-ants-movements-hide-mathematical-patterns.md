---
layout: post
title: "Ants' movements hide mathematical patterns"
date: 2016-05-02
categories:
author: Spanish Foundation For Science, Technology, Fecyt
tags: [Ant,Partial differential equation,Argentine ant,Mathematics,Research,Science,Branches of science]
---


The authors, whose study has been published in the journal Mathematical Biosciences, started by observing the behaviour of ants individually and subsequently as a collective group. Concentration of pheromone (more in red color) after 10,000 time steps (6.67min. Credit: S. Garnier, M. Vela-Pérez et al.  Now, with this data they have been able to create the model describing the collective movement of the ants on a surface. From individual to collective dynamics in Argentine ants (Linepithema humile). Journal of Mathematical Analysis and Applications 425 (1): 1-19, 2015.

<hr>

[Visit Link](http://phys.org/news350645539.html){:target="_blank" rel="noopener"}


