---
layout: post
title: "Van Allen probes catch rare glimpse of supercharged radiation belt"
date: 2017-09-21
categories:
author: "Lina Tran, Nasa'S Goddard Space Flight Center"
tags: [Van Allen radiation belt,Van Allen Probes,Space weather,Magnetosphere,Electron,Coronal mass ejection,Geomagnetic storm,Interplanetary spaceflight,Space science,Outer space,Astronomy,Nature,Physical phenomena,Sky,Science,Spaceflight,Physics,Physical sciences]
---


NASA's Van Allen Probes were there to watch it. There are multiple ways electrons in the radiation belts can be energized or accelerated: radially, locally or by way of a shock. One can liken the process of shock acceleration, as observed by the Van Allen Probe, to pushing a swing. The result: a peak in electron energy measured by the Van Allen Probe five days later. With luck, the Van Allen Probes may be in the right position in their orbit to observe the radiation belt response to more geomagnetic storms in the future.

<hr>

[Visit Link](http://phys.org/news/2016-08-van-allen-probes-rare-glimpse.html){:target="_blank" rel="noopener"}


