---
layout: post
title: "Watch Gaia first data release media briefing"
date: 2017-09-22
categories:
author: ""
tags: [Gaia (spacecraft),Stellar astronomy,European space programmes,Outer space,Space agencies,Physical sciences,Scientific observation,European Space Agency spacecraft,Uncrewed spacecraft,Space telescopes,Space policy of the European Union,Space astrometry missions,Space science experiments,Observational astronomy,Spacecraft using Lissajous orbits,Artificial satellites at Earth-Sun Lagrange points,Spaceflight,Astronomical observatories,European Space Agency,Pan-European scientific organizations,Science,Space science,Astronomy,Astronomy projects,Space exploration,European Space Agency space probes,Satellites orbiting Lagrange points]
---


Science & Exploration Watch Gaia first data release media briefing 21725 views 146 likes  Replay of the media briefing on the first data release from ESA’s Gaia mission on 14 September. The media briefing provided examples of the performance of the satellite and its science data, and highlighted the research that can be done with this first data release. Programme outline    09:30–09:40 GMT/11:30–11:40 CEST  Alvaro Gimenez, Director of Science, ESA:  Astrometry with Gaia at the very core of ESA’s Science Programme    09:40–09:50 GMT/11:40–11:50 CEST  Fred Jansen, ESA Gaia Mission Manager:  Operating at the limits of precision    09:50–10:00 GMT/11:50–12:00 CEST  Timo Prusti, ESA Gaia Project Scientist:  Gaia on the way to the most precise map of our galaxy    10:00–10:10 GMT/12:00–12:10 CEST  Anthony Brown, Gaia Data Processing and Analysis Consortium, Leiden University:  A first exploration of the Gaia sky    10:10–10:20 GMT/12:10-12:20 CEST  Antonella Vallenari, Gaia Data Processing and Analysis Consortium, Istituto Nazionale di Astrofisica (INAF), Astronomical Observatory of Padua:  Gaia’s view of the nearby star clusters    10:20–10:30 GMT/12:20-12:30 CEST  Gisella Clementini, Gaia Data Processing and Analysis Consortium Member of Coordination Unit 7, Istituto Nazionale di Astrofisica (INAF), Astronomical Observatory of Bologna:  Gaia and the distance ladder    10:30–11:00 GMT/12:30–13:00 CEST  Question and Answer sessions and opportunity for individual interviews

<hr>

[Visit Link](http://www.esa.int/Our_Activities/Space_Science/Gaia/Watch_Gaia_first_data_release_media_briefing){:target="_blank" rel="noopener"}


