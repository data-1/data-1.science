---
layout: post
title: "Genetic evidence points to nocturnal early mammals"
date: 2017-10-06
categories:
author: "Stanford University"
tags: [Evolution,Mammal,Reptile,Nocturnality,Diurnality,Animals,Biology,Evolutionary biology]
---


The team, led by Liz Hadly, professor of biology and senior author on the paper, examined genes involved in night vision in animals throughout the evolutionary tree, looking for places where those genes became enhanced. From this, they deduced that the earliest common ancestor did not have good night vision and was instead active during the day. Filling in our history  The methods used by these researchers could be applied to different areas of the animal evolutionary tree to learn more about the evolution of vision, including how humans made the switch to bright-light vision. This study is also an example of how little information we have about the first mammals, compared to what we know about our ancient and more compelling reptile cousins, the dinosaurs. We want to know better what the mammals were like then.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2017-04/su-gep042017.php){:target="_blank" rel="noopener"}


