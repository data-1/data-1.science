---
layout: post
title: "'Expansion entropy': A new litmus test for chaos?"
date: 2016-05-16
categories:
author: University Of Maryland
tags: [Chaos theory,Butterfly effect,Systems theory,Systems science,Applied mathematics,Science,Branches of science]
---


Now, researchers from the University of Maryland have described a new definition of chaos that applies more broadly than previous definitions. This new definition is compact, can be easily approximated by numerical methods and works for a wide variety of chaotic systems. Fittingly, the chaotic solution to Lorenz's equations looks like two wings of a butterfly. For example, Hunt explains, two identical chaotic systems with different initial conditions may evolve completely differently, but if the systems are forced by external inputs they may start to synchronize. By applying the expansion entropy definition of chaos and characterizing whether the original systems respond chaotically to inputs, researchers can tell whether they can wrestle some control over the chaos through inputs to the system.

<hr>

[Visit Link](http://phys.org/news/2015-07-expansion-entropy-litmus-chaos.html){:target="_blank" rel="noopener"}


