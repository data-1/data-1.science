---
layout: post
title: "Swirling ammonia lies below Jupiter's thick clouds"
date: 2016-06-03
categories:
author: "American Association for the Advancement of Science (AAAS)"
tags: [Jupiter,Cloud,Great Red Spot,Physical sciences,Solar System,Astronomy,Space science,Planetary science,Outer space,Planets,Astronomical objects,Bodies of the Solar System,Planets of the Solar System,Science]
---


Using radio waves, astronomers have been able to peer through Jupiter's thick clouds, gaining insights into the gas giant's atmosphere, a new study reports. Previous radio studies of the planet have been limited to analyzing its properties at specific latitudes, but the new observations offer a widespread, comprehensive view of activity below the clouds. To acquire such detailed data, Imke de Pater and colleagues used the recently upgraded Jansky Very Large Array (VLA) observatory, detecting a range of radio frequencies from Jupiter's atmosphere. This revealed a number of hot spots, dry regions that are devoid of clouds and condensable gases, particularly opaque billows of ammonia. Analysis of the new VLA data suggests that areas where this ammonia is concentrated extend right up to the base of where Jupiter's clouds form.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2016-06/aaft-sal053116.php){:target="_blank" rel="noopener"}


