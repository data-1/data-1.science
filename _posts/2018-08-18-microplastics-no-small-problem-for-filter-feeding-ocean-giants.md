---
layout: post
title: "Microplastics: No small problem for filter-feeding ocean giants"
date: 2018-08-18
categories:
author: "Sharks, Rays And Whales Impacted Microplastic Pollution And Associated Toxins, Marine Megafauna Foundation"
tags: [Microplastics,Plastic pollution,Filter feeder,Whale,Shark,Whale shark,Natural environment,Environmental science,Ecology,Earth sciences,Systems ecology,Nature]
---


Microplastic pollution is a major threat to filter-feeding animals such as manta rays, whale sharks and baleen whales, according to a new study published in the journal Trends in Ecology & Evolution. These iconic animals are at risk of exposure to microplastic contamination and associated toxins. It is vital to understand the effects of microplastic pollution on ocean giants since nearly half of the mobulid rays, two thirds of filter-feeding sharks and over one quarter of baleen whales are listed by the IUCN as globally threatened species and are prioritized for conservation, she adds. Our studies on whale sharks in the Sea of Cortez and on fin whales in the Mediterranean Sea confirmed exposure to toxic chemicals, indicating that these filter feeders are taking up microplastics in their feeding grounds. Elitza Germanov, Andrea Marshall, Lars Bejder, Maria Cristina Fossi and Neil R Loneragan 'Microplastics: No small problem for filter feeding megafauna' is published on 6 February 2018 and available here: DOI: 10.1016/j.tree.2018.01.005  Explore further International investigation begins on the effect of microplastics on manta rays  More information: Germanov, Elitza S. et al. (2018) Microplastics: No Small Problem for Filter-Feeding Megafauna.

<hr>

[Visit Link](https://phys.org/news/2018-02-microplastics-small-problem-filter-feeding-ocean.html){:target="_blank" rel="noopener"}


