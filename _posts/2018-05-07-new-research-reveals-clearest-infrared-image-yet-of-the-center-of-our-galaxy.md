---
layout: post
title: "New research reveals clearest infrared image yet of the center of our galaxy"
date: 2018-05-07
categories:
author: "University of Texas at San Antonio"
tags: [Astronomy,Galaxy,Black hole,Milky Way,Infrared,Supermassive black hole,Galactic Center,Light,Physics,Astrophysics,Science,Physical sciences,Astronomical objects,Space science]
---


It reveals a new high resolution map of the magnetic field lines in gas and dust swirling around the supermassive black hole at the center of our galaxy. This work, in addition to its scientific relevance, is very important to the advancement of the Ph.D. physics and astronomy program here at UTSA, since it involves students in cutting edge research, said Miguel Jose Yacaman, professor and Lutcher Brown Endowed Chair of the UTSA Department of Physics and Astronomy. Centered on the supermassive black hole, the new infrared map covers a region about one light year on each side. Elsewhere the magnetic field is less clearly aligned with the filaments. We're now able to watch material race around a black hole 25,000 light years away, and for the first time see magnetic fields there in detail.

<hr>

[Visit Link](https://www.eurekalert.org/pub_releases/2018-02/uota-nrr022318.php){:target="_blank" rel="noopener"}


