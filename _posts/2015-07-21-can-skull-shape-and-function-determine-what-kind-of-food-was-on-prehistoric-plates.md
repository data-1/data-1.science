---
layout: post
title: "Can skull shape and function determine what kind of food was on prehistoric plates?"
date: 2015-07-21
categories:
author: American Museum of Natural History 
tags: [Carnivore,Tooth,Predation,Animals]
---


Flynn and Z. Jack Tseng, a National Science Foundation and Frick Postdoctoral Fellow in the Museum's Division of Paleontology, looked at the relationship between skull shape and function of five different modern carnivore species, including meat-eating hypercarnivore specialists such as wolves and leopards, and more omnivorous generalists such as mongooses, skunks, and raccoons. Animals with the same diets and biomechanical demands, like wolves and leopards--both hypercarnivores--were not linking together, Tseng said. But once Tseng and Flynn accounted for the strong effects of ancestry and skull size on the models, hypercarnivores and generalists still could be distinguished based on biomechanics, in particular by looking at where along the tooth row the skull is strongest. Meat specialist skulls are stiffest when hunting with front teeth and/or slicing or crushing with back teeth, whereas skulls of generalists show incrementally increasing stiffness when biting sequentially from the front to the back of the tooth row. We are applying similar types of skull shape and biomechanical analyses to reconstructed hypothetical ancestor skulls of Carnivora and their relatives to map out and better understand the long history of feeding adaptation of living top predators.

<hr>

[Visit Link](http://www.eurekalert.org/pub_releases/2015-04/amon-css042815.php){:target="_blank" rel="noopener"}


