---
layout: post
title: "Japan space scientists make wireless energy breakthrough"
date: 2016-04-17
categories:
author:  
tags: [Space-based solar power,Energy,Nature,Technology]
---


Electricity gained from solar panels in space could one day be beamed to earth  Japanese scientists have succeeded in transmitting energy wirelessly, in a key step that could one day make solar power generation in space a possibility, an official said Thursday. Researchers used microwaves to deliver 1.8 kilowatts of power—enough to run an electric kettle—through the air with pinpoint accuracy to a receiver 55 metres (170 feet) away. While the distance was not huge, the technology could pave the way for mankind to eventually tap the vast amount of solar energy available in space and use it here on Earth, a spokesman for The Japan Aerospace Exploration Agency (JAXA) said. This was the first time anyone has managed to send a high output of nearly two kilowatts of electric power via microwaves to a small target, using a delicate directivity control device, he said. The idea, said the JAXA spokesman, would be for microwave-transmitting solar satellites—which would have sunlight-gathering panels and antennae—to be set up about 36,000 kilometres (22,300 miles) from the earth.

<hr>

[Visit Link](http://phys.org/news345351820.html){:target="_blank" rel="noopener"}


