---
layout: post
title: "Top 4 open source augmented reality SDKs"
date: 2018-06-20
categories:
author: "Dr. Michael J. Garbade"
tags: [Augmented reality,Software development kit,Android (operating system),Mobile app,ARToolKit,Application software,IOS,Computers,Computer science,Computing,Intellectual works,Software,Technology,Computer engineering,System software,Software development,Software engineering,Digital media]
---


If you're interested in joining Swizec and the other developers creating innovative AR projects, take a look at this list of the top four free and open source AR software SDKs you can use for powering your applications. Google's platform comes with various APIs, and some are available on both Android and iOS devices to support shared augmented reality experiences. ARToolKit  ARToolKit is a popular open source SDK used for creating enthralling AR apps that overlay virtual imagery on the physical environment. One of the main problems when building AR apps is effectively tracking the user's viewpoint. Get busy  Augmented reality technology is quickly taking the world by storm, and these open source AR SDKs will help you enhance your apps' capabilities.

<hr>

[Visit Link](https://opensource.com/article/18/6/open-source-augmented-reality-sdks){:target="_blank" rel="noopener"}


